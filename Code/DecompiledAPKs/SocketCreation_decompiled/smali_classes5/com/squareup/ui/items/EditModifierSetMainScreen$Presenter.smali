.class Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;
.super Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.source "EditModifierSetMainScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditModifierSetMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/BaseEditObjectViewPresenter<",
        "Lcom/squareup/ui/items/EditModifierSetMainView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final data:Lcom/squareup/ui/items/ModifierSetEditState;

.field private final flow:Lflow/Flow;

.field private final libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

.field private final modifierSetEditStateObservable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/items/EditModifierSetScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditModifierSetScopeRunner;Lcom/squareup/ui/items/ModifierSetEditState;Lcom/squareup/util/Res;Lrx/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            "Lcom/squareup/util/Res;",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/ui/items/LibraryDeleter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lflow/Flow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0, p10, p9, p6}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->modifierSetEditStateObservable:Lrx/Observable;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 88
    iput-object p7, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 89
    iput-object p8, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    .line 90
    iput-object p9, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 91
    iput-object p11, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private finish()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public static synthetic lambda$UaWjfCWRmFtNlnv9YGpEhBRUQ6I(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->finish()V

    return-void
.end method


# virtual methods
.method applySetToItemsClicked()V
    .locals 3

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method createNewModifierOption(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 3

    .line 295
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getModifierOptions()Ljava/util/List;

    move-result-object v0

    .line 296
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v1

    .line 297
    new-instance v2, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    .line 298
    invoke-virtual {v2, v1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v2

    .line 299
    invoke-virtual {v2, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p1

    .line 300
    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p1

    .line 297
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method deleteClicked(Lcom/squareup/api/items/ItemModifierOption$Builder;)V
    .locals 1

    .line 290
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getModifierOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addRemovedOptionId(Ljava/lang/String;)V

    return-void
.end method

.method deleteModifierSet()V
    .locals 4

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v0

    .line 255
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$Presenter$UaWjfCWRmFtNlnv9YGpEhBRUQ6I;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$Presenter$UaWjfCWRmFtNlnv9YGpEhBRUQ6I;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/items/LibraryDeleter;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getModifierOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemModifierOption$Builder;",
            ">;"
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierOptions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getNumberOfItemsInSet()I
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSetItemCount()I

    move-result v0

    return v0
.end method

.method public isNewObject()Z
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->isNewModifierSet()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onLoad$0$EditModifierSetMainScreen$Presenter(Lcom/squareup/ui/items/ModifierSetEditState;)V
    .locals 0

    if-eqz p1, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditModifierSetMainView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetMainView;->hideSpinner()V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->modifierSetEditStateObservable:Lrx/Observable;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$Presenter$t1234ThKCRnq9mlPgdchVia6FG0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetMainScreen$Presenter$t1234ThKCRnq9mlPgdchVia6FG0;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->addToCompositeSubscription(Lrx/Subscription;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->isNewModifierSet()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->create_modifier_set:I

    .line 106
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_edit_modifier_set:I

    .line 109
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->updatePrimaryButtonState()V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->secondary_button_save:I

    .line 113
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$cvqn-8cZGuDNLk37rXPFqNfM6XY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$cvqn-8cZGuDNLk37rXPFqNfM6XY;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$jIXYzOcuOVJNgzgren_d_qPMpI4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$jIXYzOcuOVJNgzgren_d_qPMpI4;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onProgressHidden()V
    .locals 1

    .line 281
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->updatePrimaryButtonState()V

    .line 282
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditModifierSetMainView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditModifierSetMainView;->showContent()V

    return-void
.end method

.method public save()V
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/registerlib/R$string;->category_name_required_warning_title:I

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_set_name_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 128
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->getModifierSetData()Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->hasNoModifierOptions()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldDisplayModifierInsteadOfOption()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/itemsapplet/R$string;->modifier_required_warning_title:I

    sget v2, Lcom/squareup/itemsapplet/R$string;->modifier_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/registerlib/R$string;->modifier_option_required_warning_title:I

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_option_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 140
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 144
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierOptions()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 145
    iget-object v2, v2, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 147
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/registerlib/R$string;->category_name_required_warning_title:I

    sget v2, Lcom/squareup/registerlib/R$string;->modifier_option_name_required_warning_message:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v2, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 155
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->isNewObject()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 156
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 157
    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v3

    .line 156
    invoke-virtual {v1, v2, v3}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    goto :goto_1

    .line 159
    :cond_5
    sget-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->getModifierSet()Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 162
    :goto_1
    sget-object v1, Lsquareup/items/merchant/CatalogObjectType;->ITEM_MODIFIER_LIST:Lsquareup/items/merchant/CatalogObjectType;

    invoke-virtual {v1}, Lsquareup/items/merchant/CatalogObjectType;->name()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->logEditCatalogObjectEvent(Ljava/lang/String;Z)V

    .line 164
    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v2, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter$1;-><init>(Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 248
    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v0

    .line 164
    invoke-interface {v1, v2, v0}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    .line 250
    invoke-direct {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->finish()V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierSetEditState;->setName(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->updatePrimaryButtonState()V

    return-void
.end method

.method showConfirmDiscardDialogOrFinish()V
    .locals 3

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->data:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->hasModifierSetDataChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->scopeRunner:Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->getModifierSetId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/ConfirmDiscardModifierSetChangesDialogScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 321
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->finish()V

    :goto_0
    return-void
.end method

.method updateOption(Lcom/squareup/api/items/ItemModifierOption$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 305
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->getModifierOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 306
    iget-object v2, v1, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    invoke-virtual {v1, p2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-virtual {p1, p3}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    return-void

    .line 314
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Attempting to update an option that does not exist."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
