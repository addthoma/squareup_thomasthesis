.class public Lcom/squareup/ui/items/EditItemMainScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "EditItemMainScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 71
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 72
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 73
    check-cast p2, Lcom/squareup/ui/items/EditItemMainScreen;

    .line 74
    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/EditItemMainScreen$Module;-><init>(Lcom/squareup/ui/items/EditItemMainScreen;)V

    .line 75
    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemScope$Component;->editItemMain(Lcom/squareup/ui/items/EditItemMainScreen$Module;)Lcom/squareup/ui/items/EditItemMainScreen$Component;

    move-result-object p1

    return-object p1
.end method
