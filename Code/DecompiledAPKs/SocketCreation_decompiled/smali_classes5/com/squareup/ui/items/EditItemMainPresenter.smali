.class Lcom/squareup/ui/items/EditItemMainPresenter;
.super Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.source "EditItemMainPresenter.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;
.implements Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/BaseEditObjectViewPresenter<",
        "Lcom/squareup/ui/items/EditItemMainView;",
        ">;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

.field private final durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

.field private durationToken:J

.field private final editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

.field private final editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

.field private final editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

.field private final employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

.field private final errorPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

.field private extraTimeToken:J

.field private final features:Lcom/squareup/settings/server/Features;

.field private finalDurationToken:J

.field private final flow:Lflow/Flow;

.field private gapDurationToken:J

.field private final inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private initialDurationToken:J

.field private final intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

.field private final itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

.field private final libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final onlineStoreAnalytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final res:Lcom/squareup/util/Res;

.field private final scannedSkuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private screen:Lcom/squareup/ui/items/EditItemMainScreen;

.field private final server:Lcom/squareup/http/Server;

.field private final servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final state:Lcom/squareup/ui/items/EditItemState;

.field private final stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

.field private userHasSquareOnlineStoreAccount:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/LibraryDeleter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/ItemEditingStringIds;Lflow/Flow;Lcom/squareup/ui/items/DuplicateSkuValidator;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/ui/items/EmployeeTextHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/librarylist/ItemSuggestionsManager;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/http/Server;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;Lcom/squareup/items/editoption/EditOptionEventLogger;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p5

    move-object v2, p6

    move-object v3, p9

    move-object v4, p10

    .line 189
    invoke-direct {p0, p10, p9, p5}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V

    move-object v4, p1

    .line 190
    iput-object v4, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    move-object v4, p2

    .line 191
    iput-object v4, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    move-object v4, p3

    .line 192
    iput-object v4, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    move-object v4, p4

    .line 193
    iput-object v4, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    .line 194
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 195
    iput-object v2, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->errorPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    move-object v1, p7

    .line 196
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    move-object v1, p8

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-object/from16 v1, p11

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

    move-object/from16 v1, p12

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    move-object/from16 v1, p13

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    move-object/from16 v1, p14

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object/from16 v1, p15

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    move-object/from16 v1, p16

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object/from16 v1, p17

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-object/from16 v1, p18

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    move-object/from16 v1, p19

    .line 206
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    move-object/from16 v1, p20

    .line 207
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    move-object/from16 v1, p21

    .line 208
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p22

    .line 209
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 210
    iput-object v3, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p23

    .line 211
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    move-object/from16 v1, p24

    .line 212
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

    move-object/from16 v1, p25

    .line 213
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    move-object/from16 v1, p26

    .line 214
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

    move-object/from16 v1, p27

    .line 215
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    move-object/from16 v1, p28

    .line 216
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    move-object/from16 v1, p29

    .line 217
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    move-object/from16 v1, p30

    .line 218
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->server:Lcom/squareup/http/Server;

    move-object/from16 v1, p31

    .line 219
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    move-object/from16 v1, p32

    .line 220
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    move-object/from16 v1, p33

    .line 221
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->onlineStoreAnalytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-object/from16 v1, p34

    .line 222
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

    .line 224
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->scannedSkuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 225
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    .line 226
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->userHasSquareOnlineStoreAccount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    .line 229
    invoke-virtual {p6, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method private checkUserHasSosAccount()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 1005
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->weeblySquareSyncService:Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;->getMerchant(Ljava/lang/String;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 1006
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$78k8gP2TJzLgpa1yGUlWpQIENhI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$78k8gP2TJzLgpa1yGUlWpQIENhI;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 1007
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method private defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    .locals 1

    .line 655
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private dismissSaveSpinner()V
    .locals 2

    .line 718
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->show()V

    .line 719
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemMainView;->showSaveSpinnerView(Z)V

    return-void
.end method

.method private editItemMainScreenData()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;",
            ">;"
        }
    .end annotation

    .line 855
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v0}, Lcom/squareup/appointmentsapi/ServicesCustomization;->businessInfo()Lio/reactivex/Observable;

    move-result-object v0

    .line 856
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    invoke-interface {v1}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v1

    .line 857
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemMainScreenRefreshNeeded()Lrx/Observable;

    move-result-object v2

    invoke-static {v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->userHasSquareOnlineStoreAccount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v4, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$zVYCPEvbDW5QHLPhlExWTwxKXbs;

    invoke-direct {v4, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$zVYCPEvbDW5QHLPhlExWTwxKXbs;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v2, v0, v1, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private getCheckoutLinkUrlResIdBasedOnEndpoint()I
    .locals 1

    .line 673
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    sget v0, Lcom/squareup/edititem/R$string;->checkout_link_with_item_id:I

    return v0

    .line 676
    :cond_0
    sget v0, Lcom/squareup/edititem/R$string;->checkout_link_staging_with_item_id:I

    return v0
.end method

.method private getInclusiveTaxForDefaultVariation()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 791
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 792
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/EditItemState;->calculateIncludedTax(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private getItemOptionAndValuesList(Lcom/squareup/ui/items/EditItemState$ItemData;Lcom/squareup/ui/items/ItemOptionData;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemState$ItemData;",
            "Lcom/squareup/ui/items/ItemOptionData;",
            ")",
            "Ljava/util/List<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1034
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1036
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getItemOptionValueIdsByOptionIdsUsedInItem()Ljava/util/LinkedHashMap;

    move-result-object p1

    .line 1039
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1040
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1041
    invoke-virtual {p1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    if-eqz v4, :cond_0

    .line 1043
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    invoke-static {v5}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 1045
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1046
    invoke-virtual {p2}, Lcom/squareup/ui/items/ItemOptionData;->getAllItemOptionValuesByOptionIdsAndValueIds()Ljava/util/Map;

    move-result-object v6

    .line 1047
    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 1048
    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    .line 1049
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1050
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1052
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 1054
    invoke-virtual {p2}, Lcom/squareup/ui/items/ItemOptionData;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    .line 1055
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1056
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1057
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1060
    :cond_2
    new-instance v2, Landroid/util/Pair;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v4, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    return-object v0
.end method

.method private goToScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 651
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private isCheckoutLinksFeatureEnabled()Z
    .locals 2

    .line 823
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-boolean v0, v0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 825
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    .line 826
    invoke-interface {v0}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isCheckoutLinksBuyButtonEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isUsingImplicitPriceForServices()Z
    .locals 2

    .line 1014
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public static synthetic lambda$eS_Y-s4telJeDDwr6-9y1paU6M0(Lcom/squareup/ui/items/EditItemMainPresenter;)Lio/reactivex/disposables/Disposable;
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->checkUserHasSosAccount()Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$12(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 388
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 389
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/appointmentsapi/StaffInfo;

    .line 390
    invoke-virtual {v1}, Lcom/squareup/appointmentsapi/StaffInfo;->getEmployeeId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic lambda$shouldShowRedSkuForVariation$16(Ljava/lang/String;Ljava/util/Set;)Ljava/lang/Boolean;
    .locals 0

    .line 850
    invoke-interface {p1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private populateErrorsBarForVariationList()V
    .locals 2

    .line 796
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 798
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->errorPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    .line 799
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DuplicateSkuValidator;->validateSkuAndUpdateErrorsBarForAllVariations()V

    :cond_1
    return-void
.end method

.method private shouldChooseGlobalOrLocalSaveThroughV3()Z
    .locals 1

    .line 734
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldSaveItemVariationsUsingV3()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 738
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasItemEverUsedItemOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 744
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->buildModifiedVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v0

    .line 745
    invoke-virtual {v0}, Lcom/squareup/catalog/EditItemVariationsState;->hasExistingVariationOverrideFieldChanged()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method private shouldSaveItemVariationsUsingV3()Z
    .locals 3

    .line 723
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->buildModifiedVariations()Lcom/squareup/catalog/EditItemVariationsState;

    move-result-object v0

    .line 724
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->useMultiUnitEditingUI()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 725
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getAppliedLocationCount()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 726
    invoke-virtual {v0}, Lcom/squareup/catalog/EditItemVariationsState;->hasOverrideFieldChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private shouldShowTooManyVariationsError()Z
    .locals 2

    .line 806
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LIMIT_VARIATIONS_PER_ITEM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 807
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xfa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private showSaveSpinner()V
    .locals 2

    .line 711
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    .line 712
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->hide()V

    .line 713
    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v1, 0x1

    .line 714
    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemMainView;->showSaveSpinnerView(Z)V

    return-void
.end method


# virtual methods
.method abbreviationChanged(Ljava/lang/String;)V
    .locals 1

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setAbbreviation(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    return-void
.end method

.method addVariationClicked()V
    .locals 2

    .line 478
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldShowTooManyVariationsError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->showTooManyVariationsErrorDialog()V

    goto :goto_0

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->isItemAssignedWithIemOptions()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 484
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasLoallyDisabledVariation()Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    if-ne v0, v1, :cond_1

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    .line 486
    invoke-virtual {v0}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedAddVariationWithOptionsAndLocallyDisabledVariationDialog()V

    goto :goto_0

    .line 488
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->gotoAddVariationWithOptionsWorkflow()V

    goto :goto_0

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedVariationAddErrorDialog()V

    goto :goto_0

    .line 494
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lcom/squareup/ui/items/EditVariationRunner;->createOrEditVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    :goto_0
    return-void
.end method

.method assignEmployeesClicked()V
    .locals 2

    .line 416
    new-instance v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method attachStockCountRowActionToStockCountRow(Lcom/squareup/ui/items/widgets/StockCountRow;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILjava/lang/String;)Lrx/Subscription;
    .locals 6

    .line 763
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    .line 764
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v2

    .line 766
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    .line 767
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemScopeRunner;->createStockCountRowAction(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/items/widgets/StockCountRow;ILjava/lang/String;)Lcom/squareup/ui/items/StockCountRowAction;

    move-result-object p1

    .line 770
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditInventoryStateController;->editInventoryState()Lrx/Observable;

    move-result-object p2

    invoke-virtual {p2, p1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public barcodeScanned(Ljava/lang/String;)V
    .locals 2

    .line 753
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    .line 754
    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/items/EditItemMainScreen;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 757
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scannedSkuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public blockExtraTimeToggled(Z)Lkotlin/Unit;
    .locals 2

    if-eqz p1, :cond_0

    const-wide/16 v0, 0xf

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 950
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    .line 951
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    .line 952
    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setTransitionTime(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 953
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 954
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method bookableByCustomerToggled(Z)Lkotlin/Unit;
    .locals 1

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setAvailableOnOnlineBookingSite(Z)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    if-eqz p1, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->setAllStaffAsAssignable()V

    goto :goto_0

    .line 407
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 408
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->bookableChange()V

    .line 409
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->updatePrimaryButtonState()V

    .line 412
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method cancel()V
    .locals 2

    .line 578
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DIRTY:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    if-ne v0, v1, :cond_0

    .line 579
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->resetBitmap()V

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->exitEditItemFlow()V

    .line 582
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item Changes Discarded"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method categoryButtonClicked()V
    .locals 2

    .line 659
    new-instance v0, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method checkoutLinkClicked(Ljava/lang/String;)V
    .locals 9

    .line 1018
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->onlineStoreAnalytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEventName;->SHARE_LINK:Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/ItemCatalogCheckoutLinkTapEventName;)V

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 1019
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    .line 1020
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->share:I

    .line 1021
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->checkout_link_share_subject:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v8, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->EDIT_ITEM_SCREEN:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-object v7, p1

    .line 1019
    invoke-interface/range {v3 .. v8}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;->openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V

    return-void
.end method

.method checkoutLinksToggleChanged(Z)V
    .locals 1

    .line 830
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isCheckoutLinksFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setEcomAvailable(Z)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    :cond_0
    return-void
.end method

.method defaultSkuChanged(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 472
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setSku(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 473
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    .line 474
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getName()Ljava/lang/String;

    move-result-object v2

    .line 473
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/items/DuplicateSkuValidator;->validateSkuAndUpdateErrorsBarForSingleVariation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method deleteItemClicked()V
    .locals 4

    .line 553
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-boolean v0, v0, Lcom/squareup/ui/items/EditItemScope;->isFromItemSuggestion:Z

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/ItemSuggestionsManager;->deleteItemSuggestion(Ljava/lang/String;)V

    .line 555
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 557
    :cond_0
    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItem;

    move-result-object v0

    .line 560
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->libraryDeleter:Lcom/squareup/ui/items/LibraryDeleter;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {v1, v0, v3}, Lcom/squareup/ui/items/LibraryDeleter;->delete(Lcom/squareup/shared/catalog/models/CatalogObject;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method descriptionChanged(Ljava/lang/String;)V
    .locals 1

    .line 545
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    return-void
.end method

.method public dropView(Lcom/squareup/ui/items/EditItemMainView;)V
    .locals 1

    .line 294
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 297
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 120
    check-cast p1, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->dropView(Lcom/squareup/ui/items/EditItemMainView;)V

    return-void
.end method

.method public durationRowClicked(Lorg/threeten/bp/Duration;)V
    .locals 5

    .line 917
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationToken:J

    .line 918
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    sget v1, Lcom/squareup/widgets/pos/R$string;->duration_title:I

    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationToken:J

    invoke-direct {v2, p1, v3, v4}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    const/4 p1, 0x1

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void
.end method

.method editItemShown()V
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Item Shown"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method editLabelClicked()V
    .locals 2

    .line 344
    new-instance v0, Lcom/squareup/ui/items/EditItemLabelScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemLabelScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method existingVariationClicked(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 2

    .line 500
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SERVICES_TAP_VARIATION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditVariationRunner;->createOrEditVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method

.method public finalDurationChanged()V
    .locals 3

    .line 943
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 944
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getFinalDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 945
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/squareup/intermission/IntermissionHelper;->startDurationPicker(Lorg/threeten/bp/Duration;Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->finalDurationToken:J

    return-void
.end method

.method public gapDurationChanged()V
    .locals 3

    .line 937
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 938
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getGapDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 939
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/squareup/intermission/IntermissionHelper;->startDurationPicker(Lorg/threeten/bp/Duration;Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->gapDurationToken:J

    return-void
.end method

.method public gapTimeToggled(Z)Lkotlin/Unit;
    .locals 1

    .line 923
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->gapTimeToggled(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Z)V

    .line 924
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 925
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->maybeShowGapTimeEducation()V

    .line 926
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->GAP_TIME_TOGGLE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 927
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public getCurrentName()Ljava/lang/String;
    .locals 1

    .line 644
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPriceHelpText()Ljava/lang/String;
    .locals 7

    .line 774
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasInclusiveTaxesApplied()Z

    move-result v2

    .line 775
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 776
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v0

    .line 777
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 778
    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 779
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    .line 780
    invoke-static {v3, v0}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v6

    .line 782
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getInclusiveTaxForDefaultVariation()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemMainView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 781
    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/items/PriceHelpTextHelper;->buildPriceHelpTextForItem(ZZLcom/squareup/protos/common/Money;Landroid/content/res/Resources;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getTextForCheckoutLink()Ljava/lang/String;
    .locals 3

    .line 663
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 665
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getCheckoutLinkUrlResIdBasedOnEndpoint()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "item_id"

    .line 666
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 667
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 668
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method inclusiveTaxPossiblyChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 841
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public initialDurationChanged()V
    .locals 3

    .line 931
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 932
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/intermission/IntermissionHelperKt;->getInitialDuration(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 933
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/squareup/intermission/IntermissionHelper;->startDurationPicker(Lorg/threeten/bp/Duration;Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->initialDurationToken:J

    return-void
.end method

.method isDefaultAbbreviation(Ljava/lang/String;)Z
    .locals 1

    .line 549
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public isNewObject()Z
    .locals 1

    .line 636
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-boolean v0, v0, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    return v0
.end method

.method public isService()Z
    .locals 2

    .line 640
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$checkUserHasSosAccount$20$EditItemMainPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1007
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$dxV0WeEOuY-tRXzZVNpHtNawCVo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$dxV0WeEOuY-tRXzZVNpHtNawCVo;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$_0uidEdfszJHEfQjU96UPMhWr9s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$_0uidEdfszJHEfQjU96UPMhWr9s;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$editItemMainScreenData$17$EditItemMainPresenter(Lkotlin/Unit;Lcom/squareup/appointmentsapi/BusinessInfo;Ljava/util/List;Ljava/lang/Boolean;)Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;
    .locals 37
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 860
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->useMultiUnitEditingUI()Z

    move-result v2

    .line 861
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v3

    .line 862
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v4

    .line 863
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v5, v1, Lcom/squareup/ui/items/EditItemState;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 864
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getAbbreviation()Ljava/lang/String;

    move-result-object v6

    .line 865
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getColor()Ljava/lang/String;

    move-result-object v7

    .line 866
    invoke-static {v6}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v4}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_0

    :cond_0
    move-object v8, v6

    .line 867
    :goto_0
    new-instance v9, Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 868
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 869
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v10

    .line 870
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v12, 0x1

    if-ne v1, v12, :cond_1

    const/4 v14, 0x1

    goto :goto_1

    :cond_1
    const/4 v14, 0x0

    .line 871
    :goto_1
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-boolean v15, v1, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    .line 872
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v13

    .line 873
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getCategoryName()Ljava/lang/String;

    move-result-object v16

    .line 874
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getDescription()Ljava/lang/String;

    move-result-object v17

    .line 875
    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    const/16 v18, 0x0

    if-ne v13, v1, :cond_2

    move-object/from16 v19, v18

    goto :goto_2

    :cond_2
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    iget-object v11, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v11, v11, Lcom/squareup/ui/items/ItemEditingStringIds;->readOnlyDetailsId:I

    .line 876
    invoke-interface {v1, v11}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v19, v1

    .line 877
    :goto_2
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$TaxStates;->getStates()Ljava/util/List;

    move-result-object v20

    .line 878
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->getStates()Ljava/util/List;

    move-result-object v21

    .line 879
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 880
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v11, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    if-eqz v15, :cond_4

    .line 882
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-boolean v1, v1, Lcom/squareup/ui/items/EditItemScope;->isFromItemSuggestion:Z

    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    move-object/from16 v22, v18

    goto :goto_5

    .line 884
    :cond_4
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->useMultiUnitEditingUI()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    iget-object v12, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v12, v12, Lcom/squareup/ui/items/ItemEditingStringIds;->deleteMultiunitResId:I

    invoke-interface {v1, v12}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_5
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    iget-object v12, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v12, v12, Lcom/squareup/ui/items/ItemEditingStringIds;->deleteResId:I

    .line 885
    invoke-interface {v1, v12}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    move-object/from16 v22, v1

    .line 887
    :goto_5
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->errorPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {v1}, Lcom/squareup/ui/ErrorsBarPresenter;->getErrorKeys()Ljava/util/Set;

    move-result-object v23

    .line 888
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/appointmentsapi/BusinessInfo;->getNoShowProtectionEnabled()Z

    move-result v24

    .line 889
    invoke-virtual {v0, v9}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldNameTakeExtraSpace(Ljava/util/List;)Z

    move-result v25

    .line 890
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->employeeTextHelper:Lcom/squareup/ui/items/EmployeeTextHelper;

    .line 891
    invoke-virtual {v10}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getEmployeeTokens()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    move-object/from16 v26, v11

    .line 892
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v11

    .line 891
    invoke-virtual {v1, v12, v11}, Lcom/squareup/ui/items/EmployeeTextHelper;->format(II)Ljava/lang/String;

    move-result-object v27

    .line 893
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->isItemAssignedWithIemOptions()Z

    move-result v1

    const/4 v11, 0x1

    xor-int/lit8 v28, v1, 0x1

    .line 894
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v12, Lcom/squareup/settings/server/Features$Feature;->ENABLE_RATE_BASED_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v12}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v29

    .line 895
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->canEditItemWithItemOptions()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v30, 0x1

    goto :goto_6

    :cond_6
    const/16 v30, 0x0

    :goto_6
    if-eqz v30, :cond_7

    .line 896
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 897
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v12, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v12}, Lcom/squareup/ui/items/EditItemState;->getItemOptionData()Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v12

    invoke-direct {v0, v1, v12}, Lcom/squareup/ui/items/EditItemMainPresenter;->getItemOptionAndValuesList(Lcom/squareup/ui/items/EditItemState$ItemData;Lcom/squareup/ui/items/ItemOptionData;)Ljava/util/List;

    move-result-object v1

    goto :goto_7

    .line 898
    :cond_7
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_7
    move-object/from16 v31, v1

    .line 899
    invoke-direct/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isCheckoutLinksFeatureEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v32, 0x1

    goto :goto_8

    :cond_8
    const/16 v32, 0x0

    .line 900
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getTextForCheckoutLink()Ljava/lang/String;

    move-result-object v33

    .line 901
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->isEcomAvailable()Z

    move-result v34

    .line 902
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 903
    invoke-interface {v1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemMainPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v12, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    .line 904
    invoke-interface {v1, v12}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v35, 0x1

    goto :goto_9

    :cond_9
    const/16 v35, 0x0

    .line 905
    :goto_9
    new-instance v36, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    move-object/from16 v1, v36

    .line 907
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result v11

    move-object/from16 v18, v13

    move v13, v11

    move v11, v14

    move v12, v15

    move-object/from16 v14, v18

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    move-object/from16 v17, v19

    move-object/from16 v18, v20

    move-object/from16 v19, v21

    move-object/from16 v20, v22

    move-object/from16 v21, v23

    move/from16 v22, v24

    move/from16 v23, v25

    move-object/from16 v24, v26

    move-object/from16 v25, v27

    move/from16 v26, v28

    move/from16 v27, v29

    move/from16 v28, v30

    move-object/from16 v29, v31

    move/from16 v30, v32

    move-object/from16 v31, v33

    move/from16 v32, v34

    move/from16 v33, v35

    invoke-direct/range {v1 .. v33}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;-><init>(ZZLjava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ZZZLcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/Set;ZZLjava/util/Map;Ljava/lang/String;ZZZLjava/util/List;ZLjava/lang/String;ZZ)V

    return-object v36
.end method

.method public synthetic lambda$null$0$EditItemMainPresenter(Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;)V
    .locals 1

    .line 241
    sget-object v0, Lcom/squareup/ui/items/EditItemMainPresenter$1;->$SwitchMap$com$squareup$ui$items$EditItemScopeRunner$SaveState:[I

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner$SaveState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 253
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown Save State"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 250
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->dismissSaveSpinner()V

    return-void

    .line 246
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->showSaveSpinner()V

    goto :goto_1

    .line 243
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->show()V

    :goto_1
    return-void
.end method

.method public synthetic lambda$null$13$EditItemMainPresenter(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 395
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setEmployeeTokens(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->bookableChange()V

    .line 397
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->updatePrimaryButtonState()V

    return-void
.end method

.method public synthetic lambda$null$18$EditItemMainPresenter(Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1008
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->userHasSquareOnlineStoreAccount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;->getInitialSyncFinished()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$19$EditItemMainPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1009
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->userHasSquareOnlineStoreAccount:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$2$EditItemMainPresenter(Landroid/os/Bundle;Lcom/squareup/ui/items/EditItemState;)V
    .locals 0

    .line 262
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->stateLoaded(Landroid/os/Bundle;)V

    .line 263
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {p1, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    return-void
.end method

.method public synthetic lambda$null$4$EditItemMainPresenter(Ljava/util/Set;)V
    .locals 0

    .line 267
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->updatePrimaryButtonState()V

    return-void
.end method

.method public synthetic lambda$null$6$EditItemMainPresenter(Lkotlin/Unit;)V
    .locals 0

    .line 271
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->populateErrorsBarForVariationList()V

    return-void
.end method

.method public synthetic lambda$null$8$EditItemMainPresenter(Lcom/squareup/ui/items/EditItemMainView;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 279
    iget-boolean v0, p2, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p2, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isNewItem:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 280
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getEmployeeTokens()Ljava/util/List;

    move-result-object v0

    .line 283
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->setAllStaffAsAssignable()V

    .line 286
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainView;->initializeView(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$11$EditItemMainPresenter(Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;)V
    .locals 7

    .line 306
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getToken()J

    move-result-wide v0

    .line 307
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 308
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 309
    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v2

    .line 311
    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationToken:J

    cmp-long v5, v0, v3

    if-nez v5, :cond_0

    .line 312
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 313
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 316
    :cond_0
    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->initialDurationToken:J

    cmp-long v5, v0, v3

    if-nez v5, :cond_1

    .line 317
    invoke-static {v2, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateInitialTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 318
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 319
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 322
    :cond_1
    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->gapDurationToken:J

    cmp-long v5, v0, v3

    if-nez v5, :cond_2

    .line 323
    invoke-static {v2, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateGapTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 324
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 325
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 328
    :cond_2
    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->finalDurationToken:J

    cmp-long v5, v0, v3

    if-nez v5, :cond_3

    .line 329
    invoke-static {v2, p1}, Lcom/squareup/intermission/IntermissionHelperKt;->updateFinalTime(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lorg/threeten/bp/Duration;)V

    .line 330
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v3}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    .line 331
    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->GAP_TIME_MODIFIED_SERVICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 334
    :cond_3
    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->extraTimeToken:J

    cmp-long v5, v0, v3

    if-nez v5, :cond_4

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/items/ExtraTimeActionEvent;

    .line 336
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/ui/items/ExtraTimeActionEvent;-><init>(Ljava/lang/String;)V

    .line 335
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 337
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMillis()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setTransitionTime(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 338
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->durationChange()V

    :cond_4
    return-void
.end method

.method public synthetic lambda$onLoad$1$EditItemMainPresenter()Lrx/Subscription;
    .locals 2

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveState()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$PUtahMxksK9nrLQB9H1WJ8eio9Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$PUtahMxksK9nrLQB9H1WJ8eio9Q;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 240
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$10$EditItemMainPresenter(Lcom/squareup/ui/items/EditItemMainView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 290
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->editItemMainScreenData()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$venCYPjVsN7Iz5_EmchRfx2xkVY;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/-$$Lambda$venCYPjVsN7Iz5_EmchRfx2xkVY;-><init>(Lcom/squareup/ui/items/EditItemMainView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$EditItemMainPresenter(Landroid/os/Bundle;)Lrx/Subscription;
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->editItemStateLoaded()Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$8S--YIsJMl5x0QkrxzHO4g2Gjmw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$8S--YIsJMl5x0QkrxzHO4g2Gjmw;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;Landroid/os/Bundle;)V

    .line 261
    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$EditItemMainPresenter()Lrx/Subscription;
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSku()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$GdW9flng09tGbVWkglHshSZCQbA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$GdW9flng09tGbVWkglHshSZCQbA;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 267
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$7$EditItemMainPresenter()Lrx/Subscription;
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editVariationRunner:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditVariationRunner;->variationEditingFinished()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$hsbOij-mE1FCa3TanbmTxF-_oNw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$hsbOij-mE1FCa3TanbmTxF-_oNw;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 271
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$9$EditItemMainPresenter(Lcom/squareup/ui/items/EditItemMainView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 274
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->editItemMainScreenData()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$wSV3gRKE7PKH2g78kqMMM0fOw28;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$wSV3gRKE7PKH2g78kqMMM0fOw28;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/ui/items/EditItemMainView;)V

    .line 275
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setAllStaffAsAssignable$14$EditItemMainPresenter()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->servicesCustomization:Lcom/squareup/appointmentsapi/ServicesCustomization;

    .line 385
    invoke-interface {v0}, Lcom/squareup/appointmentsapi/ServicesCustomization;->allStaffInfo()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 386
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$xcpmEE3fyOWBfTmdQLu0_h1-hA8;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$xcpmEE3fyOWBfTmdQLu0_h1-hA8;

    .line 387
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$lDbonOgjpVaKUYgoeg7SKFJeeJ0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$lDbonOgjpVaKUYgoeg7SKFJeeJ0;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 394
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$stateLoaded$15$EditItemMainPresenter()V
    .locals 2

    .line 594
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldSaveItemVariationsUsingV3()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 595
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldChooseGlobalOrLocalSaveThroughV3()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->showPriceChangedDialog()V

    goto :goto_0

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_VIA_ITEM_V3_GLOBALLY:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChanges(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V

    goto :goto_0

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    sget-object v1, Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;->SAVE_VARIATIONS_WITH_COGS:Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/EditItemScopeRunner;->saveChanges(Lcom/squareup/ui/items/EditItemScopeRunner$VariationSavePath;)V

    .line 603
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Edit Item Save Pressed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-boolean v0, v0, Lcom/squareup/ui/items/EditItemScope;->isFromItemSuggestion:Z

    if-eqz v0, :cond_2

    .line 608
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->itemSuggestionsManager:Lcom/squareup/librarylist/ItemSuggestionsManager;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/librarylist/ItemSuggestionsManager;->markItemSuggestionAsSaved(Ljava/lang/String;)V

    goto :goto_1

    .line 612
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_1
    return-void
.end method

.method manageItemOptionClicked()V
    .locals 9

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasLoallyDisabledVariation()Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->YES:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    if-ne v0, v1, :cond_0

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    .line 351
    invoke-virtual {v0}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedEditOptionWithLocallyDisabledVariationsDialog()V

    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->isItemAssignedWithIemOptions()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->unsupportedItemOptionActionDialogRunner:Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    .line 358
    invoke-virtual {v0}, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->showUnsupportedAddOptionWithMultipleFlatVariationsDialog()V

    return-void

    .line 362
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->variations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 364
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasLoallyDisabledVariation()Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;->NO:Lcom/squareup/ui/items/EditItemState$HasLocallyDisabledVariation;

    if-ne v0, v2, :cond_3

    const/4 v5, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 367
    :goto_1
    new-instance v0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v2, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 370
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 371
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 374
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getDeletedVariationCatalogTokens()Ljava/util/List;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 375
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemOptionData()Lcom/squareup/ui/items/ItemOptionData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/ItemOptionData;->getAllItemOptionsByIds()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 367
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 446
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemScopeRunner;->maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method modifierCheckChanged(Ljava/lang/String;Z)V
    .locals 1

    .line 574
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->modifiers:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditItemState$ModifierStates;->setApplied(Ljava/lang/String;Z)V

    return-void
.end method

.method nameChanged(Ljava/lang/String;)V
    .locals 1

    .line 507
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    .line 508
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 509
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Item Name Entered"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 511
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->updatePrimaryButtonState()V

    return-void
.end method

.method nameFieldFocusChanged(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 516
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Item Name Focused"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 517
    sget-object p1, Lcom/squareup/tutorialv2/RequestLayoutTimeout;->FOCUS_CHANGE:Lcom/squareup/tutorialv2/RequestLayoutTimeout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V

    :cond_0
    return-void
.end method

.method onCancellationFeeChanged(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 454
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getNoShowFee()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 456
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setNoShowFee(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method

.method onDisplayPriceChanged(Ljava/lang/CharSequence;)V
    .locals 1

    .line 462
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPriceDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 464
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPriceDescription(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    :cond_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 301
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainScreen;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    .line 304
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->datePicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$Vs8xq6_Cokq0xbJGNPA5fMpooTo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$Vs8xq6_Cokq0xbJGNPA5fMpooTo;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    .line 305
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 303
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method onLearnMoreOnItemOptionsClicked()V
    .locals 2

    .line 1000
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editOptionEventLogger:Lcom/squareup/items/editoption/EditOptionEventLogger;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 1001
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1000
    invoke-virtual {v0, v1}, Lcom/squareup/items/editoption/EditOptionEventLogger;->logClickOnLearnMoreAboutItemOptionsLink(Ljava/lang/String;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 233
    invoke-super {p0, p1}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 235
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    .line 239
    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$vxa8Ca9j_KWSm_IRC1oK_JyRCfw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$vxa8Ca9j_KWSm_IRC1oK_JyRCfw;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 260
    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$JpYvCLw8Hnp0u5DjxfkevsSyFCk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$JpYvCLw8Hnp0u5DjxfkevsSyFCk;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;Landroid/os/Bundle;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 266
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$6Y32SK-bJO08sOiVEX_QrObn4T0;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$6Y32SK-bJO08sOiVEX_QrObn4T0;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 269
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$rflNmQXETJYFcfMMZvUWZnLRzAg;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$rflNmQXETJYFcfMMZvUWZnLRzAg;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v0, p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 273
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$2O0hxZr0KNUTZi8si3rG5I4uAqI;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$2O0hxZr0KNUTZi8si3rG5I4uAqI;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/ui/items/EditItemMainView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 289
    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$-vuhz8z-IPQefmeovBB5fPhjUjs;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$-vuhz8z-IPQefmeovBB5fPhjUjs;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/ui/items/EditItemMainView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onPriceChanged(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 420
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 422
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrClearPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 423
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method onServicePriceChanged(Lcom/squareup/protos/common/Money;)V
    .locals 3

    .line 429
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isUsingImplicitPriceForServices()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->onPriceChanged(Lcom/squareup/protos/common/Money;)V

    return-void

    .line 434
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    .line 437
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_0

    .line 439
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setOrClearPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 441
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public onUnitAssignedToVariation(Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;",
            ">;)V"
        }
    .end annotation

    .line 966
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v4

    .line 967
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 968
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 970
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v3

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setCatalogMeasurementUnitToken(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 971
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 972
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 975
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    move-object p2, v3

    goto :goto_2

    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 977
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 978
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->eventLogger:Lcom/squareup/ui/items/EditItemEventLogger;

    .line 979
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v2

    move-object v3, v0

    move-object v5, p2

    move-object v6, p1

    .line 978
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/items/EditItemEventLogger;->logMeasurementUnitAssignment(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/lang/String;)V

    .line 986
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result p1

    if-eqz p1, :cond_4

    if-eqz p2, :cond_3

    .line 989
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    goto :goto_3

    :cond_3
    if-eqz v0, :cond_4

    .line 992
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState$ItemData;->getDefaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object p1

    const-wide/32 v0, 0x1b7740

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->setDuration(J)Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 996
    :cond_4
    :goto_3
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scopeRunner:Lcom/squareup/ui/items/EditItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemScopeRunner;->defaultVariationUnitTypeChanged()V

    return-void
.end method

.method priceFieldFocusChanged(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 523
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Item Price Focused"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 524
    sget-object p1, Lcom/squareup/tutorialv2/RequestLayoutTimeout;->FOCUS_CHANGE:Lcom/squareup/tutorialv2/RequestLayoutTimeout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V

    goto :goto_0

    .line 526
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Edit Item Price Entered"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method priceTypeButtonClicked()V
    .locals 2

    .line 698
    new-instance v0, Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditServicePriceTypeSelectionScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V
    .locals 1

    .line 531
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainView;->requestLayoutsForTutorial(Lcom/squareup/tutorialv2/RequestLayoutTimeout;)V

    return-void
.end method

.method scannedSku()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 845
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->scannedSkuRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method setAllStaffAsAssignable()V
    .locals 2

    .line 383
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$nj72kaU4jguUPk2Yr37oE_pPSqI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$nj72kaU4jguUPk2Yr37oE_pPSqI;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method shouldNameTakeExtraSpace(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
            ">;)Z"
        }
    .end annotation

    .line 814
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 815
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getSku()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method shouldShowBitmap()Z
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->itemImageState:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    sget-object v1, Lcom/squareup/ui/items/EditItemState$ItemImageState;->DELETED:Lcom/squareup/ui/items/EditItemState$ItemImageState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method shouldShowRedSkuForVariation(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 849
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->duplicateSkuValidator:Lcom/squareup/ui/items/DuplicateSkuValidator;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DuplicateSkuValidator;->variationIdsWithRedSku()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$sl73O2Sd34Q6YJarrDuYBfwhqjQ;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$sl73O2Sd34Q6YJarrDuYBfwhqjQ;-><init>(Ljava/lang/String;)V

    .line 850
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 851
    invoke-virtual {p1}, Lrx/Observable;->asObservable()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method showConfirmDiscardDialogOrFinish()V
    .locals 3

    .line 702
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->hasItemChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 703
    invoke-virtual {v0}, Lcom/squareup/ui/items/EditInventoryStateController;->hasPendingInventoryAssignments()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 706
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->cancel()V

    goto :goto_1

    .line 704
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v2, v2, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/ConfirmDiscardItemChangesDialogScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method stateLoaded(Landroid/os/Bundle;)V
    .locals 4

    .line 586
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->populateErrorsBarForVariationList()V

    .line 588
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$P2LV6YxfyZzBYxaBNki4AO6-vQs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$P2LV6YxfyZzBYxaBNki4AO6-vQs;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 589
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-boolean v2, v2, Lcom/squareup/ui/items/EditItemState;->isNewItem:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    .line 590
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isService()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Lcom/squareup/registerlib/R$string;->create_service:I

    goto :goto_0

    :cond_0
    sget v3, Lcom/squareup/registerlib/R$string;->create_item:I

    :goto_0
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->stringIds:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget v3, v3, Lcom/squareup/ui/items/ItemEditingStringIds;->screenTitleResId:I

    .line 592
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 589
    :goto_1
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 593
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$LdLrE6FmX0gfo2TKDDoDLj68ZWw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$LdLrE6FmX0gfo2TKDDoDLj68ZWw;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 615
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 616
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->updatePrimaryButtonState()V

    .line 618
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->isCheckoutLinksFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$eS_Y-s4telJeDDwr6-9y1paU6M0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemMainPresenter$eS_Y-s4telJeDDwr6-9y1paU6M0;-><init>(Lcom/squareup/ui/items/EditItemMainPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_2
    if-nez p1, :cond_3

    .line 623
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainView;->requestInitialFocus()V

    :cond_3
    return-void
.end method

.method taxSwitchChanged(Ljava/lang/String;Z)V
    .locals 1

    .line 569
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState;->taxes:Lcom/squareup/ui/items/EditItemState$TaxStates;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditItemState$TaxStates;->setApplied(Ljava/lang/String;Z)V

    .line 570
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->inclusiveTaxPossiblyChanged:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public transitionTimeChanged(Lorg/threeten/bp/Duration;)V
    .locals 5

    .line 958
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->extraTimeToken:J

    .line 959
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->durationPickerRunner:Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    sget v1, Lcom/squareup/edititem/R$string;->edit_service_extra_time:I

    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    iget-wide v3, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->extraTimeToken:J

    invoke-direct {v2, p1, v3, v4}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void
.end method

.method unitPriceTypeButtonClicked()V
    .locals 9

    .line 681
    sget-object v0, Lcom/squareup/ui/items/unit/ItemType;->ITEM:Lcom/squareup/ui/items/unit/ItemType;

    .line 682
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v1, v2, :cond_0

    .line 683
    sget-object v0, Lcom/squareup/ui/items/unit/ItemType;->SERVICE:Lcom/squareup/ui/items/unit/ItemType;

    :cond_0
    move-object v2, v0

    .line 686
    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$FullSheetBootstrapScreen;

    .line 688
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 689
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/items/EditItemState$ItemData;->measurementUnits:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v6, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->editInventoryStateController:Lcom/squareup/ui/items/EditInventoryStateController;

    .line 692
    invoke-direct {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->defaultVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/squareup/ui/items/EditInventoryStateController;->doesVariationHaveStockCount(Ljava/lang/String;)Z

    move-result v7

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    .line 693
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditItemState;->shouldShowDefaultUnits()Z

    move-result v8

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$FullSheetBootstrapScreen;-><init>(Lcom/squareup/ui/items/unit/ItemType;Ljava/lang/String;Ljava/util/List;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/util/Res;ZZ)V

    .line 686
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditItemMainPresenter;->goToScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public updatePrimaryButtonState()V
    .locals 2

    .line 535
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 536
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->locationCountUnavailable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 535
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public useMultiUnitEditingUI()Z
    .locals 2

    .line 628
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->screen:Lcom/squareup/ui/items/EditItemMainScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 629
    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 632
    :cond_0
    invoke-super {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->useMultiUnitEditingUI()Z

    move-result v0

    return v0
.end method

.method variationOrderChanged(II)V
    .locals 1

    .line 787
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainPresenter;->state:Lcom/squareup/ui/items/EditItemState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditItemState$ItemData;->reorderVariation(II)V

    return-void
.end method
