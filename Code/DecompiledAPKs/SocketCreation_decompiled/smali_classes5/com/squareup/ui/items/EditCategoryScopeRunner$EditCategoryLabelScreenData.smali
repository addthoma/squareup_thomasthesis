.class Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;
.super Ljava/lang/Object;
.source "EditCategoryScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditCategoryLabelScreenData"
.end annotation


# instance fields
.field final abbreviation:Ljava/lang/String;

.field final categoryName:Ljava/lang/String;

.field final isTextTile:Z

.field final labelColor:Ljava/lang/String;


# direct methods
.method constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-boolean p1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->isTextTile:Z

    .line 161
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->categoryName:Ljava/lang/String;

    .line 162
    iput-object p3, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->abbreviation:Ljava/lang/String;

    .line 163
    iput-object p4, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->labelColor:Ljava/lang/String;

    return-void
.end method
