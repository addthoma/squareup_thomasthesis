.class public Lcom/squareup/ui/items/ItemsAppletScopeRunner;
.super Ljava/lang/Object;
.source "ItemsAppletScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

.field private final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field private final flow:Lflow/Flow;

.field private final itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/cogs/Cogs;Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/sku/DuplicateSkuResultController;Lcom/squareup/ui/items/EditItemGateway;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->flow:Lflow/Flow;

    .line 47
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    .line 48
    iput-object p5, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 49
    iput-object p6, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    .line 50
    iput-object p7, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    return-void
.end method

.method private advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private varargs discardUnsavedChangesAndGoBackPast([Ljava/lang/Class;)V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->flow:Lflow/Flow;

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private onVariationsInMultipleItemsMatchingBarcode(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;)V"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    .line 118
    invoke-interface {v1}, Lcom/squareup/sku/DuplicateSkuResultController;->variationSelection()Lio/reactivex/Observable;

    move-result-object v1

    .line 119
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/-$$Lambda$hM66tjlM5LNZSSfzkGAk9VOo9kE;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$hM66tjlM5LNZSSfzkGAk9VOo9kE;

    .line 120
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/items/-$$Lambda$0aZYHUwyUbxJxGAHU0p3XUhjTVk;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$0aZYHUwyUbxJxGAHU0p3XUhjTVk;

    .line 121
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$ItemsAppletScopeRunner$ssZTBdfuZXUTPHGPYgxv7uCHx8E;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$ItemsAppletScopeRunner$ssZTBdfuZXUTPHGPYgxv7uCHx8E;-><init>(Lcom/squareup/ui/items/ItemsAppletScopeRunner;)V

    .line 122
    invoke-virtual {v1, v2}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 117
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->duplicateSkuResultController:Lcom/squareup/sku/DuplicateSkuResultController;

    invoke-interface {v0, p1}, Lcom/squareup/sku/DuplicateSkuResultController;->showDuplicateSkuResult(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method discardCategoryUnsavedChanges()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 63
    const-class v1, Lcom/squareup/ui/items/InEditCategoryScope;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->discardUnsavedChangesAndGoBackPast([Ljava/lang/Class;)V

    return-void
.end method

.method discardModifierSetChanges()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 67
    const-class v1, Lcom/squareup/ui/items/InEditModifierSetScope;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->discardUnsavedChangesAndGoBackPast([Ljava/lang/Class;)V

    return-void
.end method

.method goToDiscountTooComplicatedDialog()V
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/DiscountTooComplicatedErrorDialogScreen;-><init>()V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public synthetic lambda$onVariationsInMultipleItemsMatchingBarcode$0$ItemsAppletScopeRunner(Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    .line 123
    invoke-virtual {p1}, Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;->getItemId()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/sku/DuplicateSkuResultController$VariationSelection;->getType()Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    const/4 v2, 0x0

    .line 122
    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 54
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemsApplet;->select()V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->variationSelectionDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method onResultOfVariationsMatchingBarcode(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 75
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {p1, p2}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowWithSku(Ljava/lang/String;)V

    return-void

    .line 79
    :cond_0
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 80
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 81
    iget-object v1, v1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    :cond_1
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_3

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->onVariationsInMultipleItemsMatchingBarcode(Ljava/util/List;)V

    goto :goto_2

    .line 87
    :cond_3
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    .line 88
    iget-object p2, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    iget-object v0, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->type:Lcom/squareup/api/items/Item$Type;

    const/4 v1, 0x0

    invoke-interface {p2, v0, p1, v1}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    :goto_2
    return-void
.end method

.method resumeEditing()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method startEditCategoryFlow(Ljava/lang/String;)V
    .locals 0

    .line 97
    invoke-static {p1}, Lcom/squareup/ui/items/EditCategoryScreen;->toEditCategory(Ljava/lang/String;)Lcom/squareup/ui/items/EditCategoryScreen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method startEditModifierSetFlow(Ljava/lang/String;)V
    .locals 1

    .line 105
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetMainScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/EditModifierSetMainScreen;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method startNewCategoryFlow()V
    .locals 1

    .line 93
    invoke-static {}, Lcom/squareup/ui/items/EditCategoryScreen;->forNewCategory()Lcom/squareup/ui/items/EditCategoryScreen;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method startNewModifierSetFlow()V
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetMainScreen;

    sget-object v1, Lcom/squareup/ui/items/EditModifierSetScope;->NEW_MODIFIER_SET:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditModifierSetMainScreen;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/ItemsAppletScopeRunner;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method
