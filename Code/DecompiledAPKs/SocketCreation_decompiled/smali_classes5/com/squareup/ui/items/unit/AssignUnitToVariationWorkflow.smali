.class public final Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "AssignUnitToVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAssignUnitToVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AssignUnitToVariationWorkflow.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,320:1\n149#2,5:321\n149#2,5:326\n149#2,5:331\n149#2,5:336\n149#2,5:341\n149#2,5:346\n149#2,5:351\n747#3:356\n769#3:357\n1360#3:358\n1429#3,3:359\n770#3:362\n*E\n*S KotlinDebug\n*F\n+ 1 AssignUnitToVariationWorkflow.kt\ncom/squareup/ui/items/unit/AssignUnitToVariationWorkflow\n*L\n178#1,5:321\n185#1,5:326\n195#1,5:331\n230#1,5:336\n269#1,5:341\n279#1,5:346\n292#1,5:351\n303#1:356\n303#1:357\n303#1:358\n303#1,3:359\n303#1:362\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B\'\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u001a\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016JN\u0010\u0017\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0003H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
        "",
        "Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "editUnitWorkflow",
        "Lcom/squareup/items/unit/EditUnitWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "defaultStandardUnits",
        "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/items/unit/EditUnitWorkflow;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/DefaultStandardUnits;Lcom/squareup/settings/server/Features;)V",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultStandardUnits:Lcom/squareup/ui/items/unit/DefaultStandardUnits;

.field private final editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

.field private features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/EditUnitWorkflow;Lcom/squareup/util/Res;Lcom/squareup/ui/items/unit/DefaultStandardUnits;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editUnitWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultStandardUnits"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->defaultStandardUnits:Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    iput-object p4, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;)Lcom/squareup/util/Res;
    .locals 0

    .line 112
    iget-object p0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState;
    .locals 7

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getShouldShowDefaultStandardUnits()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->defaultStandardUnits:Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/unit/DefaultStandardUnits;->units(Lcom/squareup/ui/items/unit/ItemType;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 357
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 305
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getSelectableUnits()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 358
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 359
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 360
    check-cast v6, Lcom/squareup/items/unit/SelectableUnit;

    .line 305
    invoke-virtual {v6}, Lcom/squareup/items/unit/SelectableUnit;->getBackingUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 361
    :cond_1
    check-cast v5, Ljava/util/List;

    .line 305
    invoke-interface {v5, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 362
    :cond_2
    check-cast v1, Ljava/util/List;

    goto :goto_2

    .line 307
    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_2
    if-eqz p2, :cond_4

    .line 309
    sget-object v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    move-result-object p2

    if-eqz p2, :cond_4

    goto :goto_3

    .line 310
    :cond_4
    new-instance p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    .line 311
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v0

    .line 312
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getSelectableUnits()Ljava/util/List;

    move-result-object p1

    .line 313
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 310
    invoke-direct {p2, v0, p1, v2, v1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    check-cast p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->initialState(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;

    check-cast p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->render(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationState;",
            "-",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    instance-of v0, p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 133
    move-object v2, p2

    check-cast v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object v3

    .line 135
    new-instance v4, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;

    invoke-direct {v4, p2, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$1;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 132
    invoke-direct {v0, v2, v3, p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/ui/items/unit/ItemType;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 322
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 323
    const-class p2, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 324
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 322
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 178
    sget-object p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 180
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    if-eqz v0, :cond_1

    sget-object v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;

    .line 181
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 182
    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;

    invoke-virtual {v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnInventoryNotUpdatedOnUnitChange;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v3

    .line 183
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object p1

    .line 184
    sget-object v4, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$3;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 181
    invoke-direct {v0, v3, p1, v4}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/ui/items/unit/ItemType;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 327
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 328
    const-class p1, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 329
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 327
    invoke-direct {v3, p1, v0, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    .line 186
    new-instance p1, Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen;

    new-instance v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$4;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$4;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 332
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 333
    const-class p2, Lcom/squareup/ui/items/unit/InventoryCountUpdateAlertScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 334
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 332
    invoke-direct {v5, p2, p1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 180
    invoke-static/range {v2 .. v7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;->stack$default(Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 198
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    if-eqz v0, :cond_2

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 201
    new-instance v0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    .line 202
    move-object v10, p2

    check-cast v10, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;

    invoke-virtual {v10}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectableUnits()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v5

    .line 203
    sget-object v6, Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;->QUICK_ADD:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v0

    .line 201
    invoke-direct/range {v4 .. v9}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;-><init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x0

    .line 205
    new-instance v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$1;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V

    move-object v6, v2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 199
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    .line 223
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/Screen;

    .line 224
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/legacy/Screen;

    .line 226
    sget-object v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;

    .line 227
    new-instance v3, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 228
    invoke-virtual {v10}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$CreateUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v4

    .line 229
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object p1

    .line 230
    sget-object v5, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$5;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$5;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 227
    invoke-direct {v3, v4, p1, p3}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/ui/items/unit/ItemType;Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 337
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 338
    const-class p3, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 339
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 337
    invoke-direct {p1, p3, v3, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 226
    invoke-virtual {v2, p1, v0, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 236
    :cond_2
    instance-of v0, p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->editUnitWorkflow:Lcom/squareup/items/unit/EditUnitWorkflow;

    move-object v3, v0

    check-cast v3, Lcom/squareup/workflow/Workflow;

    .line 239
    new-instance v4, Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;

    .line 240
    move-object v0, p2

    check-cast v0, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;->getDefaultStandardUnitToAdd()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v2

    .line 241
    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectableUnits()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v5

    .line 242
    sget-object v6, Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;->QUICK_ADD:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    .line 239
    invoke-direct {v4, v2, v5, v6}, Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V

    const/4 v5, 0x0

    .line 244
    new-instance v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$2;

    invoke-direct {v2, p0, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$renderingOfUnitCreationOnUnitSelection$2;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)V

    move-object v6, v2

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    .line 237
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;

    .line 262
    sget-object v2, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/legacy/Screen;

    .line 263
    sget-object v3, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/legacy/Screen;

    .line 265
    sget-object v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;

    .line 266
    new-instance v4, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 267
    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$AddDefaultStandardUnit;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    .line 268
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object p1

    .line 269
    sget-object v5, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$6;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$6;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 266
    invoke-direct {v4, v0, p1, p3}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/ui/items/unit/ItemType;Lkotlin/jvm/functions/Function1;)V

    check-cast v4, Lcom/squareup/workflow/legacy/V2Screen;

    .line 342
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 343
    const-class p3, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 344
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 342
    invoke-direct {p1, p3, v4, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 265
    invoke-virtual {v3, p1, v2, p2}, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 275
    :cond_3
    instance-of v0, p2, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;

    if-eqz v0, :cond_4

    sget-object v2, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack;->Companion:Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;

    .line 276
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    .line 277
    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;

    invoke-virtual {v3}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$WarnUnsavedUnitSelectionChange;->getCurrentSelectUnit()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v3

    .line 278
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;->getItemType()Lcom/squareup/ui/items/unit/ItemType;

    move-result-object v4

    .line 279
    sget-object v5, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$7;->INSTANCE:Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$7;

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    .line 276
    invoke-direct {v0, v3, v4, v5}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;Lcom/squareup/ui/items/unit/ItemType;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 347
    new-instance v3, Lcom/squareup/workflow/legacy/Screen;

    .line 348
    const-class v4, Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 349
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 347
    invoke-direct {v3, v4, v0, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v4, 0x0

    .line 280
    new-instance v0, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;

    new-instance v5, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;

    invoke-direct {v5, p2, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow$render$8;-><init>(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;Lcom/squareup/ui/items/unit/AssignUnitToVariationInput;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 352
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 353
    const-class p1, Lcom/squareup/ui/items/unit/WarnUnsavedUnitSelectionChangeAlertScreen;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    .line 354
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 352
    invoke-direct {v5, p1, v0, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 275
    invoke-static/range {v2 .. v7}, Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;->stack$default(Lcom/squareup/ui/items/unit/AssignUnitToVariationStack$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/items/unit/AssignUnitToVariationState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflow;->snapshotState(Lcom/squareup/ui/items/unit/AssignUnitToVariationState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
