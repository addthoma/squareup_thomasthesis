.class final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->apply(Lkotlin/Unit;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "kotlin.jvm.PlatformType",
        "local",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 5

    .line 45
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 44
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$searchTerm:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v2, v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$itemTypes:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllObjectsOfTypeFromLibraryTable(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_0
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v2, v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v3, v3, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$analytics:Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v4, v4, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$searchTerm:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->access$logSearch(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/models/CatalogObjectType;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$searchTerm:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v2, v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v3, v3, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$itemTypes:Ljava/util/List;

    const/4 v4, 0x0

    .line 52
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixSearchByNameAndTypes(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v0

    .line 57
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$objectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    sget-object v2, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    return-object v0

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    iget-object v1, v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;->$discountCursorDecoration:Lkotlin/jvm/functions/Function2;

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v2, "libraryCursor"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "local"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    return-object p1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1
.end method
