.class public final enum Lcom/squareup/ui/items/EditDiscountScreen$RuleType;
.super Ljava/lang/Enum;
.source "EditDiscountScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RuleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/EditDiscountScreen$RuleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

.field public static final enum DATE_RANGE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

.field public static final enum DISCOUNTED_PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

.field public static final enum PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

.field public static final enum SCHEDULE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 501
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    const/4 v1, 0x0

    const-string v2, "PRODUCT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    .line 502
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    const/4 v2, 0x1

    const-string v3, "DISCOUNTED_PRODUCT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DISCOUNTED_PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    .line 503
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    const/4 v3, 0x2

    const-string v4, "SCHEDULE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->SCHEDULE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    .line 504
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    const/4 v4, 0x3

    const-string v5, "DATE_RANGE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DATE_RANGE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    .line 500
    sget-object v5, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DISCOUNTED_PRODUCT:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->SCHEDULE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->DATE_RANGE:Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->$VALUES:[Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 500
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/EditDiscountScreen$RuleType;
    .locals 1

    .line 500
    const-class v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/EditDiscountScreen$RuleType;
    .locals 1

    .line 500
    sget-object v0, Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->$VALUES:[Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/EditDiscountScreen$RuleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/EditDiscountScreen$RuleType;

    return-object v0
.end method
