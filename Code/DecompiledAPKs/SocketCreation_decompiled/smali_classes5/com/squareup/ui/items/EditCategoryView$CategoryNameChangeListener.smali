.class public interface abstract Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;
.super Ljava/lang/Object;
.source "EditCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CategoryNameChangeListener"
.end annotation


# virtual methods
.method public abstract onCurrentCategoryNameChanged(Ljava/lang/String;)V
.end method
