.class public final Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;
.super Ljava/lang/Object;
.source "EditItemEditDetailsView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/items/EditItemEditDetailsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemMainPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/items/EditItemEditDetailsView;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectCatalogIntegrationController(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/items/EditItemEditDetailsView;Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/ui/items/EditItemMainPresenter;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectToastFactory(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/util/ToastFactory;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->toastFactory:Lcom/squareup/util/ToastFactory;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/items/EditItemEditDetailsView;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectPresenter(Lcom/squareup/ui/items/EditItemEditDetailsView;Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectToastFactory(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/util/ToastFactory;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectRes(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/util/Res;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectFeatures(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/settings/server/Features;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectCatalogIntegrationController(Lcom/squareup/ui/items/EditItemEditDetailsView;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/items/EditItemEditDetailsView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemEditDetailsView_MembersInjector;->injectMembers(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    return-void
.end method
