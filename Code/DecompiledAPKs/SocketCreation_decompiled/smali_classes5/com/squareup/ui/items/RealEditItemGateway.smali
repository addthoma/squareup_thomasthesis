.class public final Lcom/squareup/ui/items/RealEditItemGateway;
.super Ljava/lang/Object;
.source "RealEditItemGateway.kt"

# interfaces
.implements Lcom/squareup/ui/items/EditItemGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\rH\u0016J\"\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J$\u0010\u0014\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0015\u001a\u0004\u0018\u00010\r2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\rH\u0016J \u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\r2\u0006\u0010\u0019\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\rH\u0016J\u0008\u0010\u001e\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/items/RealEditItemGateway;",
        "Lcom/squareup/ui/items/EditItemGateway;",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "getFlow",
        "()Lflow/Flow;",
        "advanceToScreen",
        "",
        "screen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "startEditDiscount",
        "id",
        "",
        "startEditItemFlow",
        "itemId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "imageUrl",
        "startEditNewItemFlow",
        "startEditNewItemFlowInCategory",
        "categoryId",
        "categoryName",
        "startEditNewItemFlowWithNameAndAmount",
        "itemAbbreviation",
        "itemName",
        "itemAmount",
        "",
        "startEditNewItemFlowWithSku",
        "sku",
        "startNewDiscount",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/RealEditItemGateway;->flow:Lflow/Flow;

    return-void
.end method

.method private final advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/RealEditItemGateway;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getFlow()Lflow/Flow;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/ui/items/RealEditItemGateway;->flow:Lflow/Flow;

    return-object v0
.end method

.method public startEditDiscount(Ljava/lang/String;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/EditDiscountScreen;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V
    .locals 8

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/ui/items/EditItemScope;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    new-instance p1, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {p1, v0}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    check-cast p1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-virtual {p0, p1, v0, v0}, Lcom/squareup/ui/items/RealEditItemGateway;->startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    new-instance p1, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {p1, v0}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    check-cast p1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public startEditNewItemFlowWithNameAndAmount(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 13

    const-string v0, "itemAbbreviation"

    move-object v9, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemName"

    move-object v10, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/ui/items/EditItemScope;

    .line 53
    sget-object v2, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, v0

    move-wide/from16 v11, p3

    .line 52
    invoke-direct/range {v1 .. v12}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V

    .line 64
    new-instance v1, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    move-object v0, p0

    invoke-direct {p0, v1}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public startEditNewItemFlowWithSku(Ljava/lang/String;)V
    .locals 2

    const-string v0, "sku"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen;

    new-instance v1, Lcom/squareup/ui/items/EditItemScope;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/EditItemScope;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public startNewDiscount()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditDiscountScreen;-><init>()V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/RealEditItemGateway;->advanceToScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method
