.class final Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;
.super Ljava/lang/Object;
.source "EditVariationRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditVariationRunner;->createOrEditItemVariation(ZLcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/ui/items/EditInventoryState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/items/EditInventoryState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

.field final synthetic $variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

.field final synthetic $variationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/items/EditVariationRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditVariationRunner;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$variationId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iput-object p4, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/ui/items/EditInventoryState;)V
    .locals 4

    const-string v0, "it"

    .line 215
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryAssignments()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$variationId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->count:Ljava/math/BigDecimal;

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 217
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditInventoryState;->getInventoryAssignments()Ljava/util/Map;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$variationId:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;->unitCost:Lcom/squareup/protos/common/Money;

    .line 219
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->this$0:Lcom/squareup/ui/items/EditVariationRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner;->getEditItemVariationState()Lcom/squareup/ui/items/EditVariationState;

    move-result-object p1

    .line 220
    iget-object v2, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    iget-object v3, p0, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->$measurementUnit:Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 219
    invoke-virtual {p1, v2, v0, v1, v3}, Lcom/squareup/ui/items/EditVariationState;->startEditingExistingVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/ui/items/EditInventoryState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditVariationRunner$createOrEditItemVariation$1;->call(Lcom/squareup/ui/items/EditInventoryState;)V

    return-void
.end method
