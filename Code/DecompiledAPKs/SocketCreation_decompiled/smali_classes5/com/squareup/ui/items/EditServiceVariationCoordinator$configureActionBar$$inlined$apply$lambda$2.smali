.class final Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;
.super Ljava/lang/Object;
.source "EditServiceVariationCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditServiceVariationCoordinator;->configureActionBar(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData$inlined:Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;

.field final synthetic $view$inlined:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;->$screenData$inlined:Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;

    iput-object p3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;->$view$inlined:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$getEditServiceVariationRunner$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;->doneClickedWhenEditingItemVariation()V

    return-void
.end method
