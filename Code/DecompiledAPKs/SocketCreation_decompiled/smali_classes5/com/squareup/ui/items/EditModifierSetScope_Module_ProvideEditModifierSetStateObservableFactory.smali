.class public final Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;
.super Ljava/lang/Object;
.source "EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/Observable<",
        "Lcom/squareup/ui/items/ModifierSetEditState;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;->scopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEditModifierSetStateObservable(Lcom/squareup/ui/items/EditModifierSetScopeRunner;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditModifierSetScopeRunner;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/items/EditModifierSetScope$Module;->provideEditModifierSetStateObservable(Lcom/squareup/ui/items/EditModifierSetScopeRunner;)Lrx/Observable;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/Observable;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;->get()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/EditModifierSetScope_Module_ProvideEditModifierSetStateObservableFactory;->provideEditModifierSetStateObservable(Lcom/squareup/ui/items/EditModifierSetScopeRunner;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
