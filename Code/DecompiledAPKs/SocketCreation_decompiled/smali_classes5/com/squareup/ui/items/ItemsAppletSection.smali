.class public abstract Lcom/squareup/ui/items/ItemsAppletSection;
.super Lcom/squareup/applet/AppletSection;
.source "ItemsAppletSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ItemsAppletSection$Units;,
        Lcom/squareup/ui/items/ItemsAppletSection$Discounts;,
        Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;,
        Lcom/squareup/ui/items/ItemsAppletSection$Categories;,
        Lcom/squareup/ui/items/ItemsAppletSection$AllServices;,
        Lcom/squareup/ui/items/ItemsAppletSection$AllItems;
    }
.end annotation


# static fields
.field private static final ALWAYS_AVAILABLE:Lcom/squareup/applet/SectionAccess;


# instance fields
.field private initialScreen:Lcom/squareup/container/ContainerTreeKey;

.field final tapName:Lcom/squareup/analytics/RegisterTapName;

.field final titleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {v0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/ItemsAppletSection;->ALWAYS_AVAILABLE:Lcom/squareup/applet/SectionAccess;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;)V
    .locals 0

    .line 24
    invoke-direct {p0, p2}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/items/ItemsAppletSection;->initialScreen:Lcom/squareup/container/ContainerTreeKey;

    .line 26
    iput p3, p0, Lcom/squareup/ui/items/ItemsAppletSection;->titleResId:I

    .line 27
    iput-object p4, p0, Lcom/squareup/ui/items/ItemsAppletSection;->tapName:Lcom/squareup/analytics/RegisterTapName;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;Lcom/squareup/ui/items/ItemsAppletSection$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/items/ItemsAppletSection;-><init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/applet/SectionAccess;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/ui/items/ItemsAppletSection;->ALWAYS_AVAILABLE:Lcom/squareup/applet/SectionAccess;

    return-object v0
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/items/ItemsAppletSection;->initialScreen:Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method
