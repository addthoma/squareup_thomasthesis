.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2$$special$$inlined$sortedBy$1;
.super Ljava/lang/Object;
.source "Comparisons.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;->invoke(Lcom/squareup/cycler/Update;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nComparisons.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Comparisons.kt\nkotlin/comparisons/ComparisonsKt__ComparisonsKt$compareBy$2\n+ 2 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2\n*L\n1#1,319:1\n101#2,5:320\n101#2,5:325\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0006\n\u0002\u0008\u0007\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u000e\u0010\u0003\u001a\n \u0004*\u0004\u0018\u0001H\u0002H\u00022\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u0001H\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "a",
        "kotlin.jvm.PlatformType",
        "b",
        "compare",
        "(Ljava/lang/Object;Ljava/lang/Object;)I",
        "kotlin/comparisons/ComparisonsKt__ComparisonsKt$compareBy$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .line 102
    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    .line 321
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    const-string v1, "Unexpected row in the list: "

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;->getSelectableUnit()Lcom/squareup/items/unit/SelectableUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/SelectableUnit;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 322
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;->getLocalizedUnitName()Ljava/lang/String;

    move-result-object p1

    .line 324
    :goto_0
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    .line 326
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;->getSelectableUnit()Lcom/squareup/items/unit/SelectableUnit;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/unit/SelectableUnit;->getName()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 327
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;->getLocalizedUnitName()Ljava/lang/String;

    move-result-object p2

    .line 329
    :goto_1
    check-cast p2, Ljava/lang/Comparable;

    invoke-static {p1, p2}, Lkotlin/comparisons/ComparisonsKt;->compareValues(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result p1

    return p1

    .line 328
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 323
    :cond_3
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
