.class public final Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditServiceVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditServiceVariationCoordinator;->configureAPoSInputFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 283
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$getEditServiceVariationRunner$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;->priceDescriptionChanged(Ljava/lang/String;)V

    return-void
.end method
