.class public interface abstract Lcom/squareup/ui/items/EditItemScope$Component;
.super Ljava/lang/Object;
.source "EditItemScope.java"

# interfaces
.implements Lcom/squareup/ui/inventory/AdjustInventoryScope$ParentComponent;
.implements Lcom/squareup/ui/items/option/AssignOptionToItemScope$ParentComponent;
.implements Lcom/squareup/ui/items/option/AddSingleVariationWithOptionsScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsModule;,
        Lcom/squareup/items/assignitemoptions/AssignItemOptionsModule;,
        Lcom/squareup/catalog/online/CatalogOnlineModule;,
        Lcom/squareup/items/unit/EditUnitModule;,
        Lcom/squareup/ui/items/EditItemScope$Module;,
        Lcom/squareup/ui/items/EditItemScope$VariationRunnerModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editItemCategorySelection()Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Component;
.end method

.method public abstract editItemLabel()Lcom/squareup/ui/items/EditItemLabelScreen$Component;
.end method

.method public abstract editItemMain(Lcom/squareup/ui/items/EditItemMainScreen$Module;)Lcom/squareup/ui/items/EditItemMainScreen$Component;
.end method

.method public abstract editItemPhoto()Lcom/squareup/ui/items/EditItemPhotoScreen$Component;
.end method

.method public abstract editItemVariation()Lcom/squareup/ui/items/EditItemVariationScreen$Component;
.end method

.method public abstract editServiceVariation()Lcom/squareup/ui/items/EditServiceVariationScreen$Component;
.end method

.method public abstract editVariationRunner()Lcom/squareup/ui/items/EditVariationRunner;
.end method

.method public abstract newCategoryName()Lcom/squareup/ui/items/NewCategoryNameScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;
.end method
