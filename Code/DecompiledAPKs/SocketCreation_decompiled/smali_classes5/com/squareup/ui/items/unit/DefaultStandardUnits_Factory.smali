.class public final Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;
.super Ljava/lang/Object;
.source "DefaultStandardUnits_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/unit/DefaultStandardUnits;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Locale;)Lcom/squareup/ui/items/unit/DefaultStandardUnits;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/unit/DefaultStandardUnits;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/unit/DefaultStandardUnits;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;->newInstance(Ljava/util/Locale;)Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/unit/DefaultStandardUnits_Factory;->get()Lcom/squareup/ui/items/unit/DefaultStandardUnits;

    move-result-object v0

    return-object v0
.end method
