.class public interface abstract Lcom/squareup/ui/items/ItemsAppletMasterScreen$Component;
.super Ljava/lang/Object;
.source "ItemsAppletMasterScreen.java"

# interfaces
.implements Lcom/squareup/applet/AppletSectionsListView$Component;
.implements Lcom/squareup/applet/AppletMasterView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/items/ItemsAppletMasterScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
