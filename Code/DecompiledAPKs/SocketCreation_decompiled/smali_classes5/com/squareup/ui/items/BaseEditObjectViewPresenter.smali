.class public abstract Lcom/squareup/ui/items/BaseEditObjectViewPresenter;
.super Lmortar/ViewPresenter;
.source "BaseEditObjectViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "Lcom/squareup/ui/items/BaseEditObjectView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TV;>;"
    }
.end annotation


# static fields
.field public static final GLOBAL_SAVE:Z = true

.field public static final LOCATION_OVERRIDE:Z


# instance fields
.field protected actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private appliedLocationCountInfo:Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final subs:Lrx/subscriptions/CompositeSubscription;


# direct methods
.method public constructor <init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 28
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    .line 29
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private isMultiUnitItemActiveAtCurrentLocation()Z
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountInfo:Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    .line 72
    invoke-virtual {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->getActiveAtCurrentLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private subscribeToAppliedLocationsCount()V
    .locals 3

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v1}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getAppliedLocationCount()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$BaseEditObjectViewPresenter$ft5RkX3CWdfh4s657XUH71tzEhw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$BaseEditObjectViewPresenter$ft5RkX3CWdfh4s657XUH71tzEhw;-><init>(Lcom/squareup/ui/items/BaseEditObjectViewPresenter;)V

    .line 95
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public addToCompositeSubscription(Lrx/Subscription;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method protected getAppliedLocationCount()I
    .locals 3

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->locationCountUnavailable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountInfo:Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

    instance-of v1, v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    if-eqz v1, :cond_0

    .line 86
    check-cast v0, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;

    invoke-virtual {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCount$Data;->getAppliedLocationCount()I

    move-result v0

    return v0

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expect CatalogAppliedLocationCount.Data, but get "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountInfo:Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method must not be called when applied location count is not available."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract getCurrentName()Ljava/lang/String;
.end method

.method public abstract isNewObject()Z
.end method

.method public synthetic lambda$subscribeToAppliedLocationsCount$0$BaseEditObjectViewPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCount;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountInfo:Lcom/squareup/catalog/online/CatalogAppliedLocationCount;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->updatePrimaryButtonState()V

    return-void
.end method

.method public locationCountUnavailable()Z
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->useMultiUnitEditingUI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->isMultiUnitItemActiveAtCurrentLocation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public logEditCatalogObjectEvent(Ljava/lang/String;Z)V
    .locals 4

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/items/EditCatalogObjectEvent;

    iget-object v2, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 108
    invoke-interface {v2}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v2

    xor-int/lit8 p2, p2, 0x1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->isNewObject()Z

    move-result v3

    invoke-direct {v1, v2, p1, p2, v3}, Lcom/squareup/log/items/EditCatalogObjectEvent;-><init>(ILjava/lang/String;ZZ)V

    .line 107
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 42
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->useMultiUnitEditingUI()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    check-cast p1, Lcom/squareup/ui/items/BaseEditObjectView;

    invoke-interface {p1}, Lcom/squareup/ui/items/BaseEditObjectView;->showMultiUnitContent()V

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->subscribeToAppliedLocationsCount()V

    :cond_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->subs:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 53
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method public updatePrimaryButtonState()V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->locationCountUnavailable()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->getCurrentName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public useMultiUnitEditingUI()Z
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/items/BaseEditObjectViewPresenter;->isNewObject()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
