.class Lcom/squareup/ui/items/NewCategoryNameView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "NewCategoryNameView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/NewCategoryNameView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/NewCategoryNameView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/NewCategoryNameView;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/items/NewCategoryNameView$1;->this$0:Lcom/squareup/ui/items/NewCategoryNameView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 31
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView$1;->this$0:Lcom/squareup/ui/items/NewCategoryNameView;

    iget-object v0, v0, Lcom/squareup/ui/items/NewCategoryNameView;->presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->updateCategoryName(Ljava/lang/String;)V

    return-void
.end method
