.class final enum Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;
.super Ljava/lang/Enum;
.source "DuplicateSkuValidator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DuplicateSkuValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DuplicateSkuValidationTarget"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

.field public static final enum FOR_ITEM:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

.field public static final enum FOR_VARIATION:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 345
    new-instance v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    const/4 v1, 0x0

    const-string v2, "FOR_ITEM"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_ITEM:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    .line 346
    new-instance v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    const/4 v2, 0x1

    const-string v3, "FOR_VARIATION"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_VARIATION:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    .line 344
    sget-object v3, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_ITEM:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->FOR_VARIATION:Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->$VALUES:[Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 344
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;
    .locals 1

    .line 344
    const-class v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;
    .locals 1

    .line 344
    sget-object v0, Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->$VALUES:[Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/DuplicateSkuValidator$DuplicateSkuValidationTarget;

    return-object v0
.end method
