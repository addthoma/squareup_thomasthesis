.class public Lcom/squareup/ui/items/ItemsAppletMasterScreen$Module;
.super Ljava/lang/Object;
.source "ItemsAppletMasterScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAppletMasterViewPresenter(Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/applet/AppletSelection;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/applet/AppletMasterViewPresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 68
    invoke-interface {p3}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result p3

    if-eqz p3, :cond_0

    sget p3, Lcom/squareup/common/strings/R$string;->items_and_services:I

    goto :goto_0

    :cond_0
    sget p3, Lcom/squareup/common/strings/R$string;->items:I

    .line 71
    :goto_0
    new-instance v0, Lcom/squareup/applet/AppletMasterViewPresenter;

    .line 72
    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/applet/AppletMasterViewPresenter;-><init>(Lcom/squareup/applet/ActionBarNavigationHelper;Ljava/lang/String;Lcom/squareup/applet/AppletSelection;)V

    return-object v0
.end method

.method static provideAppletSectionsListPresenter(Lcom/squareup/ui/items/ItemsApplet;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)Lcom/squareup/applet/AppletSectionsListPresenter;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemsApplet;->getSections()Lcom/squareup/ui/items/ItemsAppletSectionList;

    move-result-object v1

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/ItemsAppletMasterScreen$ItemsAppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)V

    return-object v6
.end method
