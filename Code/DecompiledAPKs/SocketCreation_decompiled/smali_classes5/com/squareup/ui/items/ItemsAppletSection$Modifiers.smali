.class public final Lcom/squareup/ui/items/ItemsAppletSection$Modifiers;
.super Lcom/squareup/ui/items/ItemsAppletSection;
.source "ItemsAppletSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ItemsAppletSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Modifiers"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    sget-object v1, Lcom/squareup/ui/items/ModifiersDetailScreen;->INSTANCE:Lcom/squareup/ui/items/ModifiersDetailScreen;

    invoke-static {}, Lcom/squareup/ui/items/ItemsAppletSection;->access$000()Lcom/squareup/applet/SectionAccess;

    move-result-object v2

    sget v3, Lcom/squareup/itemsapplet/R$string;->items_applet_modifiers_title:I

    sget-object v4, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_MODIFIER_SETS:Lcom/squareup/analytics/RegisterTapName;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/ItemsAppletSection;-><init>(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/applet/SectionAccess;ILcom/squareup/analytics/RegisterTapName;Lcom/squareup/ui/items/ItemsAppletSection$1;)V

    return-void
.end method
