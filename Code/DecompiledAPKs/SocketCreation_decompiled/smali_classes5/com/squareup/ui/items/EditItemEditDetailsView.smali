.class public Lcom/squareup/ui/items/EditItemEditDetailsView;
.super Landroid/widget/LinearLayout;
.source "EditItemEditDetailsView.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# instance fields
.field catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final categoryButton:Lcom/squareup/noho/NohoRow;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final helpText:Landroid/widget/TextView;

.field private final isNewItem:Z

.field private final itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

.field private itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private final itemTextTile:Lcom/squareup/orderentry/TextTile;

.field private final labelHeight:I

.field private final labelWidth:I

.field private final name:Landroid/widget/EditText;

.field private nameTextWatcher:Landroid/text/TextWatcher;

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field toastFactory:Lcom/squareup/util/ToastFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemEditDetailsView;->getContext()Landroid/content/Context;

    move-result-object p2

    const-class v0, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p2, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    .line 71
    sget p2, Lcom/squareup/edititem/R$layout;->edit_item_edit_details:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/items/EditItemEditDetailsView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 73
    sget p1, Lcom/squareup/edititem/R$id;->edit_item_details_item_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    .line 74
    sget p1, Lcom/squareup/edititem/R$id;->item_category:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    .line 75
    sget p1, Lcom/squareup/edititem/R$id;->item_image_tile:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 76
    sget p1, Lcom/squareup/edititem/R$id;->tile_help_text:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->helpText:Landroid/widget/TextView;

    .line 77
    sget p1, Lcom/squareup/edititem/R$id;->item_text_tile:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/TextTile;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    const/4 p1, 0x1

    .line 78
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemEditDetailsView;->setOrientation(I)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemEditDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 81
    sget p2, Lcom/squareup/registerlib/R$dimen;->edit_entry_label_width:I

    .line 82
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->labelWidth:I

    .line 83
    sget p2, Lcom/squareup/registerlib/R$dimen;->edit_entry_label_height:I

    .line 84
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->labelHeight:I

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->isNewObject()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->isNewItem:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditItemEditDetailsView;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->isNewItem:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/orderentry/TextTile;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditItemEditDetailsView;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$setContent$0$EditItemEditDetailsView(Landroid/view/View;Z)V
    .locals 0

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->nameFieldFocusChanged(Z)V

    return-void
.end method

.method public synthetic lambda$setContent$1$EditItemEditDetailsView(Landroid/view/View;)V
    .locals 0

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->categoryButtonClicked()V

    return-void
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v0, Lcom/squareup/edititem/R$string;->edit_item_image_load_failed:I

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/squareup/util/ToastFactory;->showText(II)V

    .line 184
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showItemBitmapError()V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 0

    .line 173
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldShowBitmap()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 174
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setItemBitmap(Landroid/graphics/Bitmap;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showImage()V

    :cond_0
    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public setContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto;Ljava/lang/String;ZZ)V
    .locals 3

    if-eqz p7, :cond_0

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->new_service:I

    .line 91
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/widgets/pos/R$string;->new_item:I

    .line 92
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    iget-boolean v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->isNewItem:Z

    if-eqz v1, :cond_1

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, p1

    :goto_1
    if-eqz p6, :cond_2

    const/4 p6, 0x5

    goto :goto_2

    :cond_2
    const/4 p6, 0x6

    .line 100
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->nameTextWatcher:Landroid/text/TextWatcher;

    if-eqz v1, :cond_3

    .line 101
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 104
    new-instance p1, Lcom/squareup/ui/items/EditItemEditDetailsView$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/EditItemEditDetailsView$1;-><init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->nameTextWatcher:Landroid/text/TextWatcher;

    .line 122
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->nameTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditItemEditDetailsView$b6RdkX8QyKgWRIZXTKpbUfrVurE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemEditDetailsView$b6RdkX8QyKgWRIZXTKpbUfrVurE;-><init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->name:Landroid/widget/EditText;

    const/high16 v1, 0x10000000

    or-int/2addr p6, v1

    invoke-virtual {p1, p6}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance p6, Lcom/squareup/ui/items/EditItemEditDetailsView$2;

    invoke-direct {p6, p0}, Lcom/squareup/ui/items/EditItemEditDetailsView$2;-><init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    invoke-virtual {p1, p6}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    if-eqz p1, :cond_4

    .line 134
    invoke-virtual {p1, v0, p2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    new-instance p6, Lcom/squareup/ui/items/EditItemEditDetailsView$3;

    invoke-direct {p6, p0}, Lcom/squareup/ui/items/EditItemEditDetailsView$3;-><init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    invoke-virtual {p1, p6}, Lcom/squareup/orderentry/TextTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1, p3}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    if-eqz p4, :cond_6

    .line 147
    invoke-virtual {p4}, Lcom/squareup/ui/photo/ItemPhoto;->hasUrl()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->shouldShowBitmap()Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_3

    .line 150
    :cond_5
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    iget p2, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->labelWidth:I

    iget p3, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->labelHeight:I

    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-virtual {p1, p2, p0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    goto :goto_4

    .line 148
    :cond_6
    :goto_3
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    :goto_4
    if-eqz p7, :cond_7

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {p1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result p1

    if-nez p1, :cond_7

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 157
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    if-eqz p1, :cond_9

    .line 158
    invoke-static {p5}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    .line 160
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemEditDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/edititem/R$string;->edit_item_default_category_button_text:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 159
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 162
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, p5}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 164
    :goto_5
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->categoryButton:Lcom/squareup/noho/NohoRow;

    new-instance p2, Lcom/squareup/ui/items/-$$Lambda$EditItemEditDetailsView$-ccyOVYB0sn9eGSpDEfiVv-tSZA;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemEditDetailsView$-ccyOVYB0sn9eGSpDEfiVv-tSZA;-><init>(Lcom/squareup/ui/items/EditItemEditDetailsView;)V

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    return-void
.end method

.method setShowTextTile(Z)V
    .locals 3

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemTextTile:Lcom/squareup/orderentry/TextTile;

    if-eqz v0, :cond_0

    .line 190
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->helpText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 193
    iget-object v1, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_1

    sget v2, Lcom/squareup/edititem/R$string;->tap_to_set_color:I

    goto :goto_0

    :cond_1
    sget v2, Lcom/squareup/edititem/R$string;->tap_to_edit:I

    .line 194
    :goto_0
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEditDetailsView;->itemImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
