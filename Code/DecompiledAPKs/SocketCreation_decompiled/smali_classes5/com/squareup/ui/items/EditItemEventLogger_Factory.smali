.class public final Lcom/squareup/ui/items/EditItemEventLogger_Factory;
.super Ljava/lang/Object;
.source "EditItemEventLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditItemEventLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemEventLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemEventLogger_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemEventLogger_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/items/EditItemEventLogger_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemEventLogger_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/items/EditItemEventLogger;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/items/EditItemEventLogger;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemEventLogger;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditItemEventLogger;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemEventLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemEventLogger_Factory;->newInstance(Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/items/EditItemEventLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemEventLogger_Factory;->get()Lcom/squareup/ui/items/EditItemEventLogger;

    move-result-object v0

    return-object v0
.end method
