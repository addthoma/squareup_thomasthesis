.class public interface abstract Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;
.super Ljava/lang/Object;
.source "EditItemVariationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemVariationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EditItemVariationRunner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H&J\n\u0010\u0008\u001a\u0004\u0018\u00010\tH&J\u0008\u0010\n\u001a\u00020\u0003H&J\u000e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0006H&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000cH&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u000cH&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u000cH&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemVariationScreen$EditItemVariationRunner;",
        "",
        "backClickedWhenEditingItemVariation",
        "",
        "doneClickedWhenEditingItemVariation",
        "editItemVariationScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
        "getIncludedTax",
        "Lcom/squareup/protos/common/Money;",
        "removeButtonClickedWhenEditingItemVariation",
        "scannedBarcode",
        "",
        "unitTypeSelectClicked",
        "variationNameChanged",
        "newName",
        "variationPriceChanged",
        "newPrice",
        "variationSkuChanged",
        "newSku",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract backClickedWhenEditingItemVariation()V
.end method

.method public abstract doneClickedWhenEditingItemVariation()V
.end method

.method public abstract editItemVariationScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditItemVariationScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getIncludedTax()Lcom/squareup/protos/common/Money;
.end method

.method public abstract removeButtonClickedWhenEditingItemVariation()V
.end method

.method public abstract scannedBarcode()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract unitTypeSelectClicked()V
.end method

.method public abstract variationNameChanged(Ljava/lang/String;)V
.end method

.method public abstract variationPriceChanged(Ljava/lang/String;)V
.end method

.method public abstract variationSkuChanged(Ljava/lang/String;)V
.end method
