.class public Lcom/squareup/ui/items/EditCategoryLabelCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditCategoryLabelCoordinator.java"


# instance fields
.field private categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

.field private categoryTextTile:Lcom/squareup/orderentry/TextTile;

.field private colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

.field private helpText:Landroid/view/View;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditCategoryScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->res:Lcom/squareup/util/Res;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->stopEditingLabel()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)Lcom/squareup/register/widgets/EditCatalogObjectLabel;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)Lcom/squareup/ui/items/EditCategoryScopeRunner;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->editLabelClicked()V

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 111
    sget v0, Lcom/squareup/itemsapplet/R$id;->edit_category_label_color_grid:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/PickColorGrid;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    .line 112
    sget v0, Lcom/squareup/edititem/R$id;->edit_label_image_tile:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    .line 113
    sget v0, Lcom/squareup/edititem/R$id;->edit_label_text_tile:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/TextTile;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryTextTile:Lcom/squareup/orderentry/TextTile;

    .line 114
    sget v0, Lcom/squareup/edititem/R$id;->image_tile_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->helpText:Landroid/view/View;

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/PickColorGrid;->addColorsForCatalogObjects()V

    return-void
.end method

.method private colorChanged(Ljava/lang/String;Z)V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->stopEditingLabel()V

    if-eqz p2, :cond_0

    .line 130
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryTextTile:Lcom/squareup/orderentry/TextTile;

    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/TextTile;->setColor(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p2, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 134
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->labelColorChanged(Ljava/lang/String;)V

    return-void
.end method

.method private editLabelClicked()V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->enableAbbreviationEditing()V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showSoftInput()V

    return-void
.end method

.method private getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 102
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->edit_category_tile:I

    .line 104
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 105
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/items/-$$Lambda$Wnc7fIYNKPSN3vhxDBaIr2rQFU8;

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/-$$Lambda$Wnc7fIYNKPSN3vhxDBaIr2rQFU8;-><init>(Lcom/squareup/ui/items/EditCategoryScopeRunner;)V

    .line 106
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 107
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method private showImageTile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0, p3}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 145
    iget-object p3, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p3, p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setName(Ljava/lang/String;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setLabelColor(Ljava/lang/String;)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->showAbbreviation()V

    return-void
.end method

.method private showTextTile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryTextTile:Lcom/squareup/orderentry/TextTile;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/TextTile;->setContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryTextTile:Lcom/squareup/orderentry/TextTile;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/TextTile;->setVisibility(I)V

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->helpText:Landroid/view/View;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setVisibility(I)V

    return-void
.end method

.method private stopEditingLabel()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->getAbbreviationOrDefault()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setAbbreviationText(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->disableAbbreviationEditing()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->bindViews(Landroid/view/View;)V

    .line 50
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 54
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$Tt5KVpzJpNVN9Dgvf6xkSjrai3o;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$Tt5KVpzJpNVN9Dgvf6xkSjrai3o;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$2$EditCategoryLabelCoordinator()Lrx/Subscription;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->scopeRunner:Lcom/squareup/ui/items/EditCategoryScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryLabelScreenData()Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$sp9sOdct_tS5Ruhe064Ht4dBp4Q;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$sp9sOdct_tS5Ruhe064Ht4dBp4Q;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    .line 55
    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$EditCategoryLabelCoordinator(ZLjava/lang/String;)V
    .locals 0

    .line 64
    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->colorChanged(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$null$1$EditCategoryLabelCoordinator(Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;)V
    .locals 5

    .line 56
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->isTextTile:Z

    .line 57
    iget-object v1, p1, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->categoryName:Ljava/lang/String;

    .line 58
    iget-object v2, p1, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->labelColor:Ljava/lang/String;

    .line 59
    iget-object p1, p1, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;->abbreviation:Ljava/lang/String;

    .line 61
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    invoke-virtual {v3, v2}, Lcom/squareup/register/widgets/PickColorGrid;->setSelectedColor(Ljava/lang/String;)V

    .line 63
    iget-object v3, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->colorGrid:Lcom/squareup/register/widgets/PickColorGrid;

    new-instance v4, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$2f40w-XX5ynuVTWEIG3084f9NUg;

    invoke-direct {v4, p0, v0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryLabelCoordinator$2f40w-XX5ynuVTWEIG3084f9NUg;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;Z)V

    invoke-virtual {v3, v4}, Lcom/squareup/register/widgets/PickColorGrid;->setOnColorChangeListener(Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;)V

    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->showTextTile(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 72
    :cond_0
    invoke-direct {p0, v1, v2, p1}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->showImageTile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$1;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$2;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator;->categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    new-instance v0, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditCategoryLabelCoordinator$3;-><init>(Lcom/squareup/ui/items/EditCategoryLabelCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/EditCatalogObjectLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
