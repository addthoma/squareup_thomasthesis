.class final Lcom/squareup/ui/items/EditItemState$ModifierState$1;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState$ModifierState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditItemState$ModifierState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$ModifierState;
    .locals 9

    .line 1217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1218
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1219
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 1220
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 1221
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 1222
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_1

    const/4 v6, 0x1

    .line 1223
    :cond_1
    new-instance p1, Lcom/squareup/ui/items/EditItemState$ModifierState;

    const/4 v8, 0x0

    move-object v0, p1

    move v5, v7

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/items/EditItemState$ModifierState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[BZZLcom/squareup/ui/items/EditItemState$1;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1215
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$ModifierState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$ModifierState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditItemState$ModifierState;
    .locals 0

    .line 1227
    new-array p1, p1, [Lcom/squareup/ui/items/EditItemState$ModifierState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1215
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$ModifierState$1;->newArray(I)[Lcom/squareup/ui/items/EditItemState$ModifierState;

    move-result-object p1

    return-object p1
.end method
