.class public final Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;
.super Ljava/lang/Object;
.source "EditItemScope_Module_ProvideCatalogServiceEndpointFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/catalog/CatalogServiceEndpoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogService;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->catalogServiceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/catalog/CatalogService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCatalogServiceEndpoint(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)Lcom/squareup/catalog/CatalogServiceEndpoint;
    .locals 0

    .line 48
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/items/EditItemScope$Module;->provideCatalogServiceEndpoint(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)Lcom/squareup/catalog/CatalogServiceEndpoint;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/catalog/CatalogServiceEndpoint;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/catalog/CatalogServiceEndpoint;
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->catalogServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/catalog/CatalogService;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->provideCatalogServiceEndpoint(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/server/catalog/CatalogService;Lcom/squareup/analytics/Analytics;)Lcom/squareup/catalog/CatalogServiceEndpoint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScope_Module_ProvideCatalogServiceEndpointFactory;->get()Lcom/squareup/catalog/CatalogServiceEndpoint;

    move-result-object v0

    return-object v0
.end method
