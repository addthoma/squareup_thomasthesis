.class public final Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListCogsObjectListHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListCogsObjectListHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListCogsObjectListHelper.kt\ncom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion\n*L\n1#1,112:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jd\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0010\u0008\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0012\u0018\u00010\u00112\u001c\u0008\u0002\u0010\u0013\u001a\u0016\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0014J \u0010\u0018\u001a\u00020\u00192\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0012\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001b*\u00020\u0015H\u0002J\u0017\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u001e*\u00020\u000bH\u0002\u00a2\u0006\u0002\u0010\u001f\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;",
        "",
        "()V",
        "listCogsObjects",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "objectType",
        "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
        "searchTerm",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "itemTypes",
        "",
        "Lcom/squareup/api/items/Item$Type;",
        "discountCursorDecoration",
        "Lkotlin/Function2;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "Lcom/squareup/prices/DiscountRulesLibraryCursor;",
        "logSearch",
        "",
        "toDataSourceOfDetailSearchableListObject",
        "Lcom/squareup/cycler/DataSource;",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "updateEventTarget",
        "",
        "(Lcom/squareup/shared/catalog/models/CatalogObjectType;)[Lcom/squareup/shared/catalog/models/CatalogObjectType;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$logSearch(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/models/CatalogObjectType;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->logSearch(Lcom/squareup/shared/catalog/models/CatalogObjectType;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$toDataSourceOfDetailSearchableListObject(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->toDataSourceOfDetailSearchableListObject(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateEventTarget(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/shared/catalog/models/CatalogObjectType;)[Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->updateEventTarget(Lcom/squareup/shared/catalog/models/CatalogObjectType;)[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic listCogsObjects$default(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lio/reactivex/Observable;
    .locals 10

    and-int/lit8 v0, p8, 0x20

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 35
    move-object v0, v1

    check-cast v0, Ljava/util/List;

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p6

    :goto_0
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_1

    .line 36
    move-object v0, v1

    check-cast v0, Lkotlin/jvm/functions/Function2;

    move-object v9, v0

    goto :goto_1

    :cond_1
    move-object/from16 v9, p7

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v2 .. v9}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private final logSearch(Lcom/squareup/shared/catalog/models/CatalogObjectType;Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    return-void

    .line 100
    :cond_0
    new-instance p1, Lcom/squareup/ui/items/log/DiscountSearchEvent;

    invoke-direct {p1, p3}, Lcom/squareup/ui/items/log/DiscountSearchEvent;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private final toDataSourceOfDetailSearchableListObject(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ")",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
            ">;"
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$toDataSourceOfDetailSearchableListObject$1;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    check-cast v0, Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method private final updateEventTarget(Lcom/squareup/shared/catalog/models/CatalogObjectType;)[Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 4

    .line 105
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq p1, v2, :cond_1

    if-ne p1, v1, :cond_0

    new-array p1, v2, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 107
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v1, p1, v0

    goto :goto_0

    .line 108
    :cond_0
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "An operation is not implemented: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "CATALOG-8676: refactor old lists to use the workflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    const/4 p1, 0x3

    new-array p1, p1, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 106
    sget-object v3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v3, p1, v0

    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v0, p1, v2

    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v0, p1, v1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final listCogsObjects(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/lang/String;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/api/items/Item$Type;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "-",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "+",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "cogs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "objectType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchTerm"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    const-class v0, Lcom/squareup/cogs/CatalogUpdateEvent;

    .line 37
    invoke-virtual {p2, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p2

    .line 39
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;

    invoke-direct {v0, p3}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$1;-><init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;)V

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p2

    .line 40
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$2;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    .line 41
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p2

    .line 42
    new-instance v7, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p6

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion$listCogsObjects$3;-><init>(Lcom/squareup/cogs/Cogs;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;Lcom/squareup/analytics/Analytics;Lkotlin/jvm/functions/Function2;)V

    check-cast v7, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v7}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "badBus.events(\n        C\u2026  .toV2Single()\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
