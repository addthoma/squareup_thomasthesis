.class Lcom/squareup/ui/items/ModifierOptionRow$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "ModifierOptionRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/ModifierOptionRow;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/ModifierOptionRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/ModifierOptionRow;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierOptionRow$1;->this$0:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x2

    if-eq p2, p1, :cond_1

    const/4 p1, 0x5

    if-eq p2, p1, :cond_1

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    .line 48
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/items/ModifierOptionRow$1;->this$0:Lcom/squareup/ui/items/ModifierOptionRow;

    invoke-static {p1}, Lcom/squareup/ui/items/ModifierOptionRow;->access$000(Lcom/squareup/ui/items/ModifierOptionRow;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    const/4 p1, 0x1

    return p1
.end method
