.class public Lcom/squareup/ui/ReverseOvershootInterpolator;
.super Ljava/lang/Object;
.source "ReverseOvershootInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr p1, v0

    neg-float v1, p1

    mul-float v1, v1, p1

    const v2, 0x3ff47ae1    # 1.91f

    mul-float v2, v2, p1

    const v3, 0x3f8eb852    # 1.115f

    add-float/2addr v2, v3

    mul-float v1, v1, v2

    const v2, 0x3fe66666    # 1.8f

    mul-float p1, p1, v2

    add-float/2addr v1, p1

    add-float/2addr v1, v0

    return v1
.end method
