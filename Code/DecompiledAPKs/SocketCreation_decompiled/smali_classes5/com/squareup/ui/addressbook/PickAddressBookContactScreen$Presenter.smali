.class Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PickAddressBookContactScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/addressbook/PickAddressBookContactView;",
        ">;"
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final contactsLoader:Lcom/squareup/ui/addressbook/AddressBookLoader;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/ui/addressbook/AddressBookLoader;Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->application:Landroid/app/Application;

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->contactsLoader:Lcom/squareup/ui/addressbook/AddressBookLoader;

    .line 75
    iput-object p3, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->runner:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method constructor <init>(Landroid/app/Application;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 6
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 66
    new-instance v2, Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-direct {v2, p2, p3, p7}, Lcom/squareup/ui/addressbook/AddressBookLoader;-><init>(Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;-><init>(Landroid/app/Application;Lcom/squareup/ui/addressbook/AddressBookLoader;Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-void
.end method


# virtual methods
.method invoiceContactClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->runner:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;->closePickAddressBookContactScreen(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$PickAddressBookContactScreen$Presenter(Landroid/database/MatrixCursor;)V
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/addressbook/PickAddressBookContactView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 95
    invoke-virtual {p1}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    .line 96
    invoke-virtual {v0, v1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->updateSearchButton(Z)V

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCodeOrNull()Lcom/squareup/CountryCode;

    move-result-object v1

    .line 99
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->contactsLoaded(Landroid/database/MatrixCursor;Lcom/squareup/CountryCode;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 81
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/addressbook/PickAddressBookContactView;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->invoice_choose_contact:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 85
    invoke-virtual {v2, v3, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->runner:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/addressbook/-$$Lambda$igdjxnGVLQXp4P-M78z6bNNSqLc;

    invoke-direct {v3, v2}, Lcom/squareup/ui/addressbook/-$$Lambda$igdjxnGVLQXp4P-M78z6bNNSqLc;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;)V

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 84
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    const/4 v0, 0x0

    .line 89
    invoke-virtual {p1, v0}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->updateSearchButton(Z)V

    .line 91
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->application:Landroid/app/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->contactsLoader:Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-virtual {p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactView;->getSearchText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactScreen$Presenter$x7jkERBBs4Pfd1J3gk1Nl6xHjJU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/addressbook/-$$Lambda$PickAddressBookContactScreen$Presenter$x7jkERBBs4Pfd1J3gk1Nl6xHjJU;-><init>(Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;)V

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/ui/addressbook/AddressBookLoader;->load(Landroid/content/Context;Ljava/lang/String;Lcom/squareup/ui/addressbook/AddressBookLoader$Callback;)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->runner:Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;->closePickAddressBookContactScreen()V

    :goto_0
    return-void
.end method

.method runSearchQuery(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Presenter;->contactsLoader:Lcom/squareup/ui/addressbook/AddressBookLoader;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/addressbook/AddressBookLoader;->loadSync(Landroid/content/Context;Ljava/lang/String;)Landroid/database/MatrixCursor;

    move-result-object p1

    return-object p1
.end method
