.class public Lcom/squareup/ui/Interpolators;
.super Ljava/lang/Object;
.source "Interpolators.java"


# static fields
.field public static final CubicEaseIn:Landroid/view/animation/Interpolator;

.field public static final CubicEaseInEaseOut:Landroid/view/animation/Interpolator;

.field public static final CubicEaseOut:Landroid/view/animation/Interpolator;

.field public static final QintEaseIn:Landroid/view/animation/Interpolator;

.field public static final QintEaseInEaseOut:Landroid/view/animation/Interpolator;

.field public static final QintEaseOut:Landroid/view/animation/Interpolator;

.field public static final QuadEaseIn:Landroid/view/animation/Interpolator;

.field public static final QuadEaseInEaseOut:Landroid/view/animation/Interpolator;

.field public static final QuadEaseOut:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/ui/Interpolators$1;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->CubicEaseIn:Landroid/view/animation/Interpolator;

    .line 14
    new-instance v0, Lcom/squareup/ui/Interpolators$2;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->CubicEaseOut:Landroid/view/animation/Interpolator;

    .line 20
    new-instance v0, Lcom/squareup/ui/Interpolators$3;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->CubicEaseInEaseOut:Landroid/view/animation/Interpolator;

    .line 27
    new-instance v0, Lcom/squareup/ui/Interpolators$4;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$4;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QuadEaseInEaseOut:Landroid/view/animation/Interpolator;

    .line 34
    new-instance v0, Lcom/squareup/ui/Interpolators$5;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$5;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QuadEaseIn:Landroid/view/animation/Interpolator;

    .line 40
    new-instance v0, Lcom/squareup/ui/Interpolators$6;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$6;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QuadEaseOut:Landroid/view/animation/Interpolator;

    .line 46
    new-instance v0, Lcom/squareup/ui/Interpolators$7;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$7;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QintEaseInEaseOut:Landroid/view/animation/Interpolator;

    .line 53
    new-instance v0, Lcom/squareup/ui/Interpolators$8;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$8;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QintEaseIn:Landroid/view/animation/Interpolator;

    .line 59
    new-instance v0, Lcom/squareup/ui/Interpolators$9;

    invoke-direct {v0}, Lcom/squareup/ui/Interpolators$9;-><init>()V

    sput-object v0, Lcom/squareup/ui/Interpolators;->QintEaseOut:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
