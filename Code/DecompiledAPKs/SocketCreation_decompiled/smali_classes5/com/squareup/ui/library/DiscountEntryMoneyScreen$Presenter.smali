.class Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "DiscountEntryMoneyScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/DiscountEntryMoneyScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/library/DiscountEntryMoneyView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;"
        }
    .end annotation
.end field

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

.field private final features:Lcom/squareup/settings/server/Features;

.field protected listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;

.field private state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final vibrator:Landroid/os/Vibrator;

.field private final workingDiscountBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/ui/library/DiscountEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/library/DiscountEntryScreenRunner;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/Employee;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 80
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 81
    iput-object p2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 83
    iput-object p3, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 84
    iput-object p4, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 85
    iput-object p5, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    .line 86
    iput-object p6, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    .line 87
    iput-object p7, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 88
    iput-object p8, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 89
    iput-object p9, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 90
    iput-object p10, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    .line 91
    iput-object p11, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    .line 92
    iput-object p12, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->displayAmount()V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)Landroid/os/Vibrator;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    return-object p0
.end method

.method private displayAmount()V
    .locals 5

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/DiscountEntryMoneyView;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v2}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$500(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/DiscountEntryMoneyView;->setAmountText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$500(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->updateDiscountValueColor(Z)V

    return-void
.end method

.method private updateDiscountValueColor(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryMoneyView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryMoneyView;->setAmountColor(I)V

    goto :goto_0

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryMoneyView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryMoneyView;->setAmountColor(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public onCancelSelected()V
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->finishDiscountEntryMoney()V

    return-void
.end method

.method public onCommitSelected()Z
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_MONEY:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v2}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$600(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$700(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;Z)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->discountEntryScreenRunner:Lcom/squareup/ui/library/DiscountEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/DiscountEntryScreenRunner;->finishDiscountEntryMoney()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 96
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;

    .line 97
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;->access$000(Lcom/squareup/ui/library/DiscountEntryMoneyScreen;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;->access$000(Lcom/squareup/ui/library/DiscountEntryMoneyScreen;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    .line 99
    invoke-static {p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen;->access$100(Lcom/squareup/ui/library/DiscountEntryMoneyScreen;)Lcom/squareup/protos/client/Employee;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    .line 98
    invoke-static {v0, v1, v2, p1, v3}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->buildLoadedWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/BundleKey;Lcom/squareup/protos/client/Employee;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    goto :goto_0

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->authorizingEmployeeBundleKey:Lcom/squareup/BundleKey;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->buildEmptyWorkingDiscountState(Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 107
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$200(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;Landroid/os/Bundle;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$300(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$pnPX2ScWZIwYtMZVj1dnwMng_L0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$pnPX2ScWZIwYtMZVj1dnwMng_L0;-><init>(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->apply:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$s1DJWbwQ-Mf3vJBC_Fg8q2OVEcw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$s1DJWbwQ-Mf3vJBC_Fg8q2OVEcw;-><init>(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 115
    new-instance p1, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;

    invoke-direct {p1, p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter$MoneyEntryKeypadListener;-><init>(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    .line 116
    invoke-direct {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->displayAmount()V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/DiscountEntryMoneyView;

    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->listener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/DiscountEntryMoneyView;->setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$400(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;Landroid/os/Bundle;)V

    .line 122
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method onStartVisualTransition()V
    .locals 3

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_DISCOUNT_ENTRY_MONEY:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$Presenter;->state:Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;

    .line 152
    invoke-static {v2}, Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;->access$600(Lcom/squareup/ui/library/DiscountEntryMoneyScreen$MoneyWorkingDiscountState;)Lcom/squareup/configure/item/WorkingDiscount;

    move-result-object v2

    .line 151
    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logDiscountEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingDiscount;)V

    return-void
.end method
