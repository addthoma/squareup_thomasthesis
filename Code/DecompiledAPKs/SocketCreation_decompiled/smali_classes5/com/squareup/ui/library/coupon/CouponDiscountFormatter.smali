.class public Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;
.super Ljava/lang/Object;
.source "CouponDiscountFormatter.java"


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private isFreeItem(Lcom/squareup/protos/client/coupons/Coupon;)Z
    .locals 6

    .line 149
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    sget-object v2, Lcom/squareup/protos/client/coupons/Scope;->ITEM:Lcom/squareup/protos/client/coupons/Scope;

    if-eq v0, v2, :cond_0

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v0, v0, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 159
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpl-double p1, v2, v4

    if-nez p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method

.method private tryFormatAmount(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;
    .locals 1

    .line 133
    iget-object v0, p1, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private tryFormatPercentage(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;
    .locals 1

    .line 141
    iget-object v0, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public formatCouponAlreadyRedeemedDate(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/String;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->localeProvider:Ljavax/inject/Provider;

    .line 180
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    .line 179
    invoke-static {v0, p1, v1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponAlreadyRedeemedDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatCouponExpirationDate(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/String;
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->localeProvider:Ljavax/inject/Provider;

    .line 175
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    .line 174
    invoke-static {v0, p1, v1}, Lcom/squareup/crm/DateTimeFormatHelper;->formatCouponExpirationDate(Lcom/squareup/util/Res;Lcom/squareup/protos/client/ISO8601Date;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public formatCouponExpirationDate(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;
    .locals 1

    .line 166
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    .line 167
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatCouponExpirationDate(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public formatLongerDescription(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;
    .locals 5

    .line 55
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    const-string v1, "coupon.discount"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->isFreeItem(Lcom/squareup/protos/client/coupons/Coupon;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkout/R$string;->coupon_free_item:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 61
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->tryFormatAmount(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 62
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->tryFormatPercentage(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "amount"

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->coupon_discount_description_format_amount:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 66
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v2, "percentage"

    if-nez p1, :cond_2

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->coupon_discount_description_format_percentage:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 70
    invoke-virtual {p1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 73
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkout/R$string;->coupon_discount_description_format_percentage_and_amount:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 74
    invoke-virtual {v3, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;
    .locals 6

    .line 84
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    const-string v1, "coupon.discount"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    .line 86
    iget-object v1, v0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 87
    iget-object p1, v0, Lcom/squareup/api/items/Discount;->name:Ljava/lang/String;

    return-object p1

    .line 90
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->isFreeItem(Lcom/squareup/protos/client/coupons/Coupon;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkout/R$string;->coupon_free_item:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 94
    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->tryFormatAmount(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 95
    invoke-direct {p0, v0}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->tryFormatPercentage(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 97
    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    const-string v3, "percentage"

    const-string v4, "amount"

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    sget-object v5, Lcom/squareup/protos/client/coupons/Scope;->CART:Lcom/squareup/protos/client/coupons/Scope;

    if-ne v2, v5, :cond_2

    goto :goto_0

    .line 112
    :cond_2
    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    sget-object v5, Lcom/squareup/protos/client/coupons/Scope;->ITEM:Lcom/squareup/protos/client/coupons/Scope;

    if-ne v2, v5, :cond_5

    if-nez v0, :cond_3

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkout/R$string;->coupon_name_item_scope_format_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 115
    invoke-static {v1, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_3
    if-nez v1, :cond_4

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->coupon_name_item_scope_format_percentage:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 120
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 122
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->coupon_name_item_scope_format_percentage_and_amount:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 123
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 124
    invoke-virtual {p1, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 128
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "coupon.reward.scope: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_0
    if-nez v0, :cond_7

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkout/R$string;->coupon_name_cart_scope_format_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 100
    invoke-static {v1, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    :cond_7
    if-nez v1, :cond_8

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$string;->coupon_name_cart_scope_format_percentage:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 104
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 105
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 107
    :cond_8
    iget-object p1, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkout/R$string;->coupon_name_cart_scope_format_percentage_and_amount:I

    invoke-interface {p1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 108
    invoke-virtual {p1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 109
    invoke-virtual {p1, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public formatShortestDescription(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;
    .locals 1

    .line 44
    iget-object v0, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/api/items/Discount;->percentage:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
