.class public final Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;
.super Ljava/lang/Object;
.source "GiftCardActivationView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/library/giftcard/GiftCardActivationView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/PriceLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationView;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectCurrencyCode(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 76
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    return-void
.end method

.method public static injectPriceLocaleHelper(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-void
.end method

.method public static injectThirdPartyGiftCardPanValidationStrategyProvider(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/library/giftcard/GiftCardActivationView;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectPresenter(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->priceLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectPriceLocaleHelper(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/money/PriceLocaleHelper;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/text/Formatter;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectCurrencyCode(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->thirdPartyGiftCardPanValidationStrategyProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectThirdPartyGiftCardPanValidationStrategyProvider(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;Ljavax/inject/Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView_MembersInjector;->injectMembers(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V

    return-void
.end method
