.class Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "GiftCardActivationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->access$000(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    invoke-static {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->access$100(Lcom/squareup/ui/library/giftcard/GiftCardActivationView;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/library/giftcard/GiftCardActivationView$1;->this$0:Lcom/squareup/ui/library/giftcard/GiftCardActivationView;

    iget-object p1, p1, Lcom/squareup/ui/library/giftcard/GiftCardActivationView;->presenter:Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/library/giftcard/GiftCardActivationPresenter;->onAmountChanged()V

    :cond_0
    return-void
.end method
