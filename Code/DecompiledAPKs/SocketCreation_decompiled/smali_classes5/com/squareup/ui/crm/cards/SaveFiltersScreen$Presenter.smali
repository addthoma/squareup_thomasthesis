.class Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SaveFiltersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveFiltersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/SaveFiltersCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final group:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;"
        }
    .end annotation
.end field

.field private final onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 83
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 77
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 78
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    .line 79
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;

    .line 85
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 86
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 87
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method private createGroupV2(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/GroupV2;"
        }
    .end annotation

    .line 161
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 163
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 164
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->SMART:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    .line 165
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type(Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    .line 166
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object p1

    .line 167
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$10(Lcom/squareup/protos/client/rolodex/GroupV2;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 150
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$11(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$0(Lkotlin/Unit;Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$null$1$SaveFiltersScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$2$SaveFiltersScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$4$SaveFiltersScreen$Presenter(Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;->closeSaveFiltersScreen(Lcom/squareup/protos/client/rolodex/GroupV2;)V

    return-void
.end method

.method public synthetic lambda$null$5$SaveFiltersScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 104
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;

    const-string p2, ""

    if-eqz p1, :cond_0

    .line 105
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Response;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_failed_to_save_group:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$8$SaveFiltersScreen$Presenter(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/GroupV2;->newBuilder()Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v1

    .line 142
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p1

    .line 141
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$3$SaveFiltersScreen$Presenter(Lcom/squareup/protos/client/rolodex/GroupV2;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertSmartGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$0S7J6NCXkrmVFDgEdp28NX4MS_M;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$0S7J6NCXkrmVFDgEdp28NX4MS_M;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    .line 98
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$ffrfLB-Y7T51h3sWYDSQZYT6H70;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$ffrfLB-Y7T51h3sWYDSQZYT6H70;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    .line 99
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$6$SaveFiltersScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 101
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$HyJGD7KYt-MR5LLj3z3mAUtoPgk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$HyJGD7KYt-MR5LLj3z3mAUtoPgk;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$COjM6gpbJWnFEgZHW0kQLrtkU2g;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$COjM6gpbJWnFEgZHW0kQLrtkU2g;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onLoad$12$SaveFiltersScreen$Presenter(Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$U52W3NgdncKNBwjfY_ZIdDngBxE;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$U52W3NgdncKNBwjfY_ZIdDngBxE;

    .line 150
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$0M27_s5Hc_F6idBWD-KGlkrNjl0;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$0M27_s5Hc_F6idBWD-KGlkrNjl0;

    .line 148
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SxET--V_t5eSk3B_3IRcq3lnTMA;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 152
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$SaveFiltersScreen$Presenter()V
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$9$SaveFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveFiltersCardView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->onGroupNameChanged()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$t3ceff4ksSqAZy9Dd9Mhp5mnowA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$t3ceff4ksSqAZy9Dd9Mhp5mnowA;

    .line 138
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$WAgGo5T7PciTlbHqCM1CfnAC9zk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$WAgGo5T7PciTlbHqCM1CfnAC9zk;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    .line 140
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 91
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$rw8sKrTl34_Dlwa2gCS8-buJjVc;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$rw8sKrTl34_Dlwa2gCS8-buJjVc;

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$sNHVMomrpwXtbvsXooD6g5CsbO4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$sNHVMomrpwXtbvsXooD6g5CsbO4;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$4n7J6Ll9SMY6p9xzBMga2fHfodk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$4n7J6Ll9SMY6p9xzBMga2fHfodk;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    .line 101
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 93
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 114
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;

    .line 116
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 118
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_save_filters_title:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 119
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$HEUAXY2CjEaiIpum2I9pqJTqU0o;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$HEUAXY2CjEaiIpum2I9pqJTqU0o;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 121
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 122
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$10O3jtl-3pgPIH08D7lBdHR0Bxo;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$10O3jtl-3pgPIH08D7lBdHR0Bxo;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 124
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Controller;->getFiltersForSaveFiltersScreen()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->createGroupV2(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object v2

    if-eqz p1, :cond_1

    .line 127
    sget-object v3, Lcom/squareup/protos/client/rolodex/GroupV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v4, "group"

    invoke-static {v3, p1, v4}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    .line 128
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, v2

    :goto_0
    invoke-virtual {v3, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1

    .line 130
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 133
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GroupV2;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->showGroupName(Ljava/lang/String;)V

    .line 136
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$33ApbgwzEHY26pxzKYz40a2gSDs;

    invoke-direct {p1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$33ApbgwzEHY26pxzKYz40a2gSDs;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveFiltersCardView;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 147
    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$S4DugVtfdogVOyYzUsomvGvX3M8;

    invoke-direct {p1, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveFiltersScreen$Presenter$S4DugVtfdogVOyYzUsomvGvX3M8;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 156
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "group"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method
