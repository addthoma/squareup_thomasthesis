.class public Lcom/squareup/ui/crm/rows/AddCouponRow;
.super Landroid/widget/LinearLayout;
.source "AddCouponRow.java"


# instance fields
.field private closeButton:Landroid/widget/ImageView;

.field private onCloseClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    sget p2, Lcom/squareup/crm/R$layout;->crm_add_coupon_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/rows/AddCouponRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onCloseClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->onCloseClicked:Lrx/Observable;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/crm/R$id;->crm_remove_add_coupon_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->closeButton:Landroid/widget/ImageView;

    .line 32
    sget v0, Lcom/squareup/crm/R$id;->crm_add_coupon_row_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->title:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->BOLD:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->closeButton:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->onCloseClicked:Lrx/Observable;

    const/16 v0, 0x8

    .line 37
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setVisibility(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AddCouponRow;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
