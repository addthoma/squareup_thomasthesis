.class public interface abstract Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;
.super Ljava/lang/Object;
.source "ResolveDuplicatesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract closeResolveDuplicatesScreen()V
.end method

.method public abstract showMergingDuplicatesScreen(I)V
.end method
