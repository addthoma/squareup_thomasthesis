.class public interface abstract Lcom/squareup/ui/crm/v2/profile/InvoicesSectionRunner;
.super Ljava/lang/Object;
.source "InvoicesSectionRunner.java"


# virtual methods
.method public abstract getInvoiceMetrics(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMetricQueries()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setInvoiceLoaderToken(Ljava/lang/String;)V
.end method

.method public abstract showCustomerInvoiceScreen()V
.end method
