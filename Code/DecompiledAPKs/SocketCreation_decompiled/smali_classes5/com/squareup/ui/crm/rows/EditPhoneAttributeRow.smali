.class public Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;
.super Lcom/squareup/ui/XableEditText;
.source "EditPhoneAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private onPhoneChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/XableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->onPhoneChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->onPhoneChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 50
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 57
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v0

    .line 56
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public onPhoneChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->onPhoneChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/text/InsertingScrubber;)V
    .locals 1

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 32
    new-instance v0, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow$1;

    invoke-direct {v0, p0, p3, p0}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow$1;-><init>(Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 39
    iget-object p3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, p3}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->setHint(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 40
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const/16 p2, 0x8

    :goto_1
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->setVisibility(I)V

    .line 42
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditPhoneAttributeRow;->setFocusable(Z)V

    return-void
.end method
