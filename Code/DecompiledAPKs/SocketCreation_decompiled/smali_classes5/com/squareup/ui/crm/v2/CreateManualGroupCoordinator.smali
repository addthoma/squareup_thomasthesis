.class public Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CreateManualGroupCoordinator.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;
    }
.end annotation


# instance fields
.field private final busy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    const/4 v0, 0x0

    .line 43
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/XableEditText;Landroid/widget/ProgressBar;Ljava/lang/Boolean;)V
    .locals 1

    .line 79
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 81
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 80
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 82
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-static {p2, p0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 52
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 56
    sget v1, Lcom/squareup/crm/applet/R$id;->crm_group_name_field:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    .line 58
    sget v2, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marin/widgets/ActionBarView;

    .line 59
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    .line 60
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    .line 61
    invoke-interface {v4}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;->isFirstGroup()Z

    move-result v4

    if-eqz v4, :cond_0

    sget v4, Lcom/squareup/crm/applet/R$string;->crm_create_manual_group_first:I

    goto :goto_0

    :cond_0
    sget v4, Lcom/squareup/crm/applet/R$string;->crm_create_manual_group:I

    .line 60
    :goto_0
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 63
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/crm/v2/-$$Lambda$T87WiSgdZYvbzPo6oVGyWe7GHWg;

    invoke-direct {v4, v3}, Lcom/squareup/ui/crm/v2/-$$Lambda$T87WiSgdZYvbzPo6oVGyWe7GHWg;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;)V

    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 64
    sget v3, Lcom/squareup/crm/applet/R$string;->crm_save:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 65
    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 66
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$Khm0LctP5z3feUHyMrBHS6vx7ek;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$Khm0LctP5z3feUHyMrBHS6vx7ek;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;Lcom/squareup/ui/XableEditText;)V

    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 69
    new-instance v0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;

    invoke-direct {v0, p0, v2}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$1;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 75
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 76
    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$2s08z-hRA24rsrj_khUOYnfHBzM;

    invoke-direct {v3, p0, v2, v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$2s08z-hRA24rsrj_khUOYnfHBzM;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/XableEditText;Landroid/widget/ProgressBar;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$UK0VUnFNnHMmCc3OPL9IGPg4XCM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$UK0VUnFNnHMmCc3OPL9IGPg4XCM;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 91
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$attach$0$CreateManualGroupCoordinator(Lcom/squareup/ui/XableEditText;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;->saveManualGroup(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$attach$2$CreateManualGroupCoordinator(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/XableEditText;Landroid/widget/ProgressBar;)Lrx/Subscription;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$V3xsYCyDftvly_nfg8kNmjOMohM;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$V3xsYCyDftvly_nfg8kNmjOMohM;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/XableEditText;Landroid/widget/ProgressBar;)V

    .line 78
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$4$CreateManualGroupCoordinator()Lrx/Subscription;
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->runner:Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;->onCreateGroupError()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$YkIEq_eplxHUyneDX-0pt_2k2hA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$CreateManualGroupCoordinator$YkIEq_eplxHUyneDX-0pt_2k2hA;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$3$CreateManualGroupCoordinator(Ljava/lang/String;)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const-string v1, ""

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
