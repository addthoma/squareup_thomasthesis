.class public abstract Lcom/squareup/ui/crm/applet/UpdateGroup2Scope$Module;
.super Ljava/lang/Object;
.source "UpdateGroup2Scope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/UpdateGroup2Scope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideUpdateGroup2ScreenController(Lcom/squareup/ui/crm/flow/UpdateGroup2Flow;)Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
