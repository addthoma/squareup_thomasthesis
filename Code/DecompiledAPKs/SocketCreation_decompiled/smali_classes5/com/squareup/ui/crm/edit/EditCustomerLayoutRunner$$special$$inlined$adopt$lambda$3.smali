.class final Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;",
        "Lcom/squareup/noho/NohoInputBox;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditCustomerLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$6$1\n+ 2 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n+ 3 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,283:1\n172#2,2:284\n64#3,2:286\n*E\n*S KotlinDebug\n*F\n+ 1 EditCustomerLayoutRunner.kt\ncom/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$6$1\n*L\n169#1,2:284\n173#1,2:286\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;",
        "Lcom/squareup/noho/NohoInputBox;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$recycler$1$6$1",
        "com/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$row$lambda$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;->this$0:Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
            "Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;",
            "Lcom/squareup/noho/NohoInputBox;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/squareup/noho/NohoInputBox;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 169
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoInputBox;

    .line 284
    invoke-virtual {v0}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    .line 170
    new-instance v8, Lcom/squareup/noho/ClearPlugin;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v8, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, v8}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 286
    new-instance p2, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3$1;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$$special$$inlined$adopt$lambda$3;Lcom/squareup/cycler/StandardRowSpec$Creator;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
