.class public interface abstract Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;
.super Ljava/lang/Object;
.source "AbstractViewCustomersListCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract appliedFilters()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract commitMultiSelectAction()V
.end method

.method public abstract deleteIndividualFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
.end method

.method public abstract getAppliedFilters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getInitialScrollPosition()I
.end method

.method public abstract getMultiSelectMode()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/MultiSelectMode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract saveScrollPosition(I)V
.end method

.method public abstract setMultiSelectContactSet(Lkotlin/Pair;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;)V"
        }
    .end annotation
.end method

.method public abstract setMultiSelectCount(I)V
.end method

.method public abstract setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
.end method

.method public abstract showChooseFiltersScreen()V
.end method

.method public abstract showContactDetail(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract showCreateCustomerScreen(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V
.end method
