.class public interface abstract Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;
.super Ljava/lang/Object;
.source "ChooseEnumAttributeScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeChooseEnumAttributeScreen(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
.end method

.method public abstract getEnumAttribute()Lcom/squareup/ui/crm/rows/EnumAttribute;
.end method
