.class public Lcom/squareup/ui/crm/cards/MergingCustomersScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "MergingCustomersScreen.java"

# interfaces
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Factory;,
        Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;,
        Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Component;
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGING_CUSTOMERS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
