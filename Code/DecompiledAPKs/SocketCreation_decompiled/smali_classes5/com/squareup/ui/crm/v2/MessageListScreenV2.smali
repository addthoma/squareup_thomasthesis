.class public Lcom/squareup/ui/crm/v2/MessageListScreenV2;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "MessageListScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/ui/crm/applet/CustomersApplet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/MessageListScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;,
        Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/MessageListScreenV2;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    invoke-direct {v0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    .line 154
    sget-object v0, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    .line 155
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 158
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_message_list_view:I

    return v0
.end method
