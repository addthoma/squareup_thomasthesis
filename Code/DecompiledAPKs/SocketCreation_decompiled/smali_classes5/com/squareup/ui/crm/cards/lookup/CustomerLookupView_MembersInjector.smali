.class public final Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;
.super Ljava/lang/Object;
.source "CustomerLookupView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;",
        ">;"
    }
.end annotation


# instance fields
.field private final phoneNumberScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPhoneNumberScrubber(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->phoneNumberScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->injectPhoneNumberScrubber(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Lcom/squareup/text/InsertingScrubber;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->injectRes(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V

    return-void
.end method
