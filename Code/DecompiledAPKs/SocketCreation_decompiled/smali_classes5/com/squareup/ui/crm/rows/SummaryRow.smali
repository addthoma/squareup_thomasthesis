.class public Lcom/squareup/ui/crm/rows/SummaryRow;
.super Landroid/widget/LinearLayout;
.source "SummaryRow.java"


# instance fields
.field private final title:Landroid/widget/TextView;

.field private final value:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 19
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 20
    sget v0, Lcom/squareup/crm/R$layout;->crm_summary_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/rows/SummaryRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 22
    sget p1, Lcom/squareup/crm/R$id;->crm_summary_title:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/SummaryRow;->title:Landroid/widget/TextView;

    .line 23
    sget p1, Lcom/squareup/crm/R$id;->crm_summary_value:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/SummaryRow;->value:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public showTitle(Ljava/lang/String;)V
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SummaryRow;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showValue(Ljava/lang/String;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SummaryRow;->value:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
