.class public final Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmSendLoyaltyStatusDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 50
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;->confirmSendLoyaltyStatus()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 42
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 44
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->confirmSendLoyaltyStatusDialogScreen()Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Component;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Component;->runner()Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_send_status:I

    .line 47
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;->getConfirmSendLoyaltyStatusCopy()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_send_status_confirm:I

    new-instance v2, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmSendLoyaltyStatusDialogScreen$Factory$1YQJkAJyCv2neVC6KA-21tmxuyM;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmSendLoyaltyStatusDialogScreen$Factory$1YQJkAJyCv2neVC6KA-21tmxuyM;-><init>(Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;)V

    .line 49
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 53
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 46
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
