.class public interface abstract Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;
.super Ljava/lang/Object;
.source "CreateGroupScreen.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/crm/cards/group/GroupEditView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CreateGroupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/CreateGroupView;)V
.end method
