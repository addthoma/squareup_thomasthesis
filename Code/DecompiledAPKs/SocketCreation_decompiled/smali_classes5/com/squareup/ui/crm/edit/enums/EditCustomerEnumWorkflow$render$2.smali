.class final Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditCustomerEnumWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->render(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

.field final synthetic $state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->this$0:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$state:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;

    iget-object v3, p0, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow$render$2;->$props:Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;

    invoke-static {v1, v2, v3}, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;->access$closeAction(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumState;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumProps;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
