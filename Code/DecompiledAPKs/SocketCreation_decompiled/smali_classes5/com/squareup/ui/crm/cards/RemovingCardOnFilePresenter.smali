.class Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;
.super Lmortar/Presenter;
.source "RemovingCardOnFilePresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final response:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->res:Lcom/squareup/util/Res;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 51
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 55
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$RemovingCardOnFilePresenter(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 83
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->hideProgress()V

    .line 85
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_cardonfile_unlink_success:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->showText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;->successRemoveCardOnFile()V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$2$RemovingCardOnFilePresenter(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->hideProgress()V

    .line 95
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 96
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_cardonfile_unlink_failed:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->showText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$3$RemovingCardOnFilePresenter(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 81
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$RMhzTjiTjMDCImqGuyzf7Jt7vCg;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$RMhzTjiTjMDCImqGuyzf7Jt7vCg;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$XfMhKmRSD-I6IKKsonJYZkp4I7k;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$XfMhKmRSD-I6IKKsonJYZkp4I7k;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$5$RemovingCardOnFilePresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;->closeRemovingCardOnFileScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$4$RemovingCardOnFilePresenter(Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$EVuzfEem-mENvxylgcRiu_IFawI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$EVuzfEem-mENvxylgcRiu_IFawI;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$RemovingCardOnFilePresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$R0uIYgp9YRHaNHyWhPvKDVfpNJY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$R0uIYgp9YRHaNHyWhPvKDVfpNJY;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;)V

    .line 105
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 59
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;

    .line 64
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;->getContactTokenForUnlinkInstrumentDialog()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->runner:Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;

    .line 65
    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;->getInstrumentTokenForUnlinkInstrumentDialog()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->unlinkInstrumentOnFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 68
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Single;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$-ZUcFZ_o6ORfzKid_bGJY7q0Ed0;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$-ZUcFZ_o6ORfzKid_bGJY7q0Ed0;

    .line 67
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->zipWith(Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 74
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_cardonfile_unlink_loading:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->showText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$qCt-J44DC4B0zCiizmZKFtg0BPA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$qCt-J44DC4B0zCiizmZKFtg0BPA;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$Yra-Vyc0TmU05f_oZd_eVi-uurU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$RemovingCardOnFilePresenter$Yra-Vyc0TmU05f_oZd_eVi-uurU;-><init>(Lcom/squareup/ui/crm/cards/RemovingCardOnFilePresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
