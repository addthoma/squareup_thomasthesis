.class Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ProfileAttachmentsUploadBottomDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;

.field final synthetic val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;->val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog$Factory$2;->val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->openGallery()V

    return-void
.end method
