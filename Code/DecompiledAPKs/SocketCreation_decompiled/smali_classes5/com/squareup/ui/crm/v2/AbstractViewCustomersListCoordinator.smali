.class public abstract Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AbstractViewCustomersListCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field protected contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field protected customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

.field protected final defaultActionBarTitle:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultDropDown:Landroid/view/View;

.field private final device:Lcom/squareup/util/Device;

.field protected dropDownContainer:Lcom/squareup/ui/DropDownContainer;

.field private emptyDirectoryMessage:Landroid/view/View;

.field protected final features:Lcom/squareup/settings/server/Features;

.field private filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

.field protected final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private multiSelectActionBarView:Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

.field private multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

.field private multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

.field private multiSelectDropDown:Landroid/view/View;

.field private noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

.field private progressBar:Landroid/widget/ProgressBar;

.field protected final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

.field private visualState:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            ">;"
        }
    .end annotation
.end field

.field private warningMessage:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/AppletSelection;)V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    const-string v0, ""

    .line 100
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->defaultActionBarTitle:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 142
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    .line 143
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 144
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 145
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 146
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 147
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    .line 148
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->features:Lcom/squareup/settings/server/Features;

    .line 149
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->device:Lcom/squareup/util/Device;

    .line 150
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    .line 151
    iput-object p10, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 263
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_customer_lookup:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    .line 264
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filters_layout:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/FiltersLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    .line 265
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_contact_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 266
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_progress:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    .line 267
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_empty_directory_message_phone:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    .line 268
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_warning_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    .line 269
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_create_customer_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    .line 271
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 273
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_multiselect_actionbar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectActionBarView:Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

    .line 274
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_default:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->defaultDropDown:Landroid/view/View;

    .line 275
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_multiselect_dropdown:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectDropDown:Landroid/view/View;

    .line 276
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_multiselect_button_blue:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

    .line 277
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_multiselect_button_red:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private buildDeselectedContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 3

    .line 556
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, v0, v2, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->buildContactSet(ZLjava/lang/String;ILjava/util/Collection;)Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    return-object v0
.end method

.method private buildSelectedContactSet(I)Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 3

    .line 560
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, v0, p1, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->buildContactSet(ZLjava/lang/String;ILjava/util/Collection;)Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p1

    return-object p1
.end method

.method private varargs createMultiSelectEvent(Lcom/squareup/ui/crm/v2/MultiSelectMode;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;
    .locals 0

    .line 566
    iget-object p1, p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->analyticsName:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createMultiSelectEvent(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$CVz2a8Kb1pxd2N14V2m2rOxCzf8(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->updateMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method static synthetic lambda$bindDropDownAction$42(Lrx/Observable;Landroid/view/View;)Lrx/Subscription;
    .locals 1

    .line 409
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$-zgxqVGHwry-1O7EGLiPN5cj5CU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$-zgxqVGHwry-1O7EGLiPN5cj5CU;-><init>(Landroid/view/View;)V

    .line 410
    invoke-virtual {p0, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$22(Lkotlin/Unit;Lcom/squareup/ui/crm/v2/MultiSelectMode;)Lcom/squareup/ui/crm/v2/MultiSelectMode;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$null$27(Lkotlin/Unit;Lcom/squareup/ui/crm/v2/MultiSelectMode;)Lcom/squareup/ui/crm/v2/MultiSelectMode;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$null$37(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 1

    .line 380
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$9(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$setupActionBar$18(Landroid/widget/ImageView;Landroid/content/Context;)Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$setupActionBar$21(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)Ljava/lang/Boolean;
    .locals 1

    .line 314
    sget-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupActionBar$31(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 0

    .line 355
    iget-object p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/crm/RolodexContactLoader$Input;

    invoke-static {p0}, Lcom/squareup/crm/RolodexContactLoaderHelper;->canDetermineTotalContactCount(Lcom/squareup/crm/RolodexContactLoader$Input;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupActionBar$32(Lcom/squareup/ui/crm/v2/MultiSelectMode;Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 1

    .line 358
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq p0, v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupActionBar$33(Lcom/jakewharton/rxrelay/PublishRelay;)V
    .locals 1

    const/4 v0, 0x1

    .line 359
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$setupActionBar$34(Lcom/squareup/ui/crm/v2/MultiSelectMode;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 1

    .line 368
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    if-eq p0, v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupActionBar$35(Lcom/jakewharton/rxrelay/PublishRelay;)V
    .locals 1

    const/4 v0, 0x0

    .line 369
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$setupActionBar$36(Lrx/subscriptions/CompositeSubscription;)Lrx/Subscription;
    .locals 0

    return-object p0
.end method

.method private setupActionBar(Landroid/view/View;)V
    .locals 9

    .line 282
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_drop_down_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/DropDownContainer;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    .line 284
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 285
    sget v1, Lcom/squareup/crm/R$drawable;->crm_action_overflow:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 286
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 288
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$SWe9xZvwyZ9qRcO8kPHxxFnllxo;

    invoke-direct {v3, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$SWe9xZvwyZ9qRcO8kPHxxFnllxo;-><init>(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->open_menu:I

    .line 290
    invoke-interface {v0, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-virtual {v2, v3, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$N2lr1VXOkwWrM3-jBtlpC2fmDlA;-><init>(Lcom/squareup/ui/DropDownContainer;)V

    .line 291
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 288
    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 295
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$lCo8nd3WKlWy64XCwZEBNkOmRO8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$lCo8nd3WKlWy64XCwZEBNkOmRO8;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->visualState:Lrx/Observable;

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$caMxJFpKPKOXU8Re932IKwl4ze0;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$caMxJFpKPKOXU8Re932IKwl4ze0;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 316
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    .line 319
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$dnbaFB0HzGfZzXNR1bN-OUCJkm8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$dnbaFB0HzGfZzXNR1bN-OUCJkm8;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 328
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$Hv4FoIqkS6WgcPjrI7V7wBXg7lE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$Hv4FoIqkS6WgcPjrI7V7wBXg7lE;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 332
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$pm4i6F7DbCmcLSqQI5Bsr0dF2cQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$pm4i6F7DbCmcLSqQI5Bsr0dF2cQ;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 342
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    .line 345
    sget v2, Lcom/squareup/crm/applet/R$id;->crm_multiselect_menu_select_all:I

    .line 346
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 351
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 353
    invoke-virtual {v3}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$V-Qb4dfuXcAuFapmLIgwxbJDsqw;

    invoke-direct {v4, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$V-Qb4dfuXcAuFapmLIgwxbJDsqw;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 354
    invoke-virtual {v3, v4}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$Ok-0ZEcxQ8R7AtTZepNjcmmbAcw;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$Ok-0ZEcxQ8R7AtTZepNjcmmbAcw;

    .line 355
    invoke-virtual {v3, v4}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v3

    .line 356
    invoke-virtual {v3}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v3

    sget-object v4, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$gSuLcrHgr4La12JlImNMfuEPvFI;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$gSuLcrHgr4La12JlImNMfuEPvFI;

    .line 350
    invoke-static {v2, v0, v3, v4}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v7

    new-instance v8, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$WOJTA0Af7WXSRZLHk51U6OOh-ew;

    invoke-direct {v8, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$WOJTA0Af7WXSRZLHk51U6OOh-ew;-><init>(Lcom/jakewharton/rxrelay/PublishRelay;)V

    const/4 v6, 0x1

    move-object v3, p0

    move-object v4, p1

    .line 345
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 362
    sget v2, Lcom/squareup/crm/applet/R$id;->crm_multiselect_menu_deselect_all:I

    .line 363
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 366
    invoke-interface {v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$DflLXeu8_rtID6rmlI1ozrm-UQU;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$DflLXeu8_rtID6rmlI1ozrm-UQU;

    invoke-static {v2, v0, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v7

    new-instance v8, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$TtUm9Ny9X9khqq8uIdheYBgOLQk;

    invoke-direct {v8, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$TtUm9Ny9X9khqq8uIdheYBgOLQk;-><init>(Lcom/jakewharton/rxrelay/PublishRelay;)V

    move-object v3, p0

    .line 362
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 373
    new-instance v2, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v2}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    .line 374
    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$TJ5NLvNm5dkpwIfVGm_wPFl0VZg;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$TJ5NLvNm5dkpwIfVGm_wPFl0VZg;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 377
    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$D_xLGxpSDb0PhtZ9U6VE3X1nC-s;

    invoke-direct {v3, p0, v1, v2}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$D_xLGxpSDb0PhtZ9U6VE3X1nC-s;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;Lcom/jakewharton/rxrelay/PublishRelay;Lrx/subscriptions/CompositeSubscription;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 389
    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$_w6Tt4yg8opOrcHiYpo6JtFBHgs;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$_w6Tt4yg8opOrcHiYpo6JtFBHgs;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;Lcom/jakewharton/rxrelay/PublishRelay;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 396
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->setupDropDownActions(Landroid/view/View;Lrx/Observable;)V

    return-void
.end method

.method private showMessage(ZLjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 535
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    const/high16 v0, 0x10e0000

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 538
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 539
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->warningMessage:Lcom/squareup/widgets/MessageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private showProgress(Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 527
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    const/high16 v1, 0x10e0000

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 529
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private updateMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
    .locals 5

    .line 422
    sget-object v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 424
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-static {v3, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 425
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getSupportsFilters()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    :goto_1
    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_2

    .line 426
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getSupportsFilters()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 428
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v4}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getAppliedFilters()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/crm/v2/FiltersLayout;->setFilters(Ljava/util/List;)V

    .line 430
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->defaultDropDown:Landroid/view/View;

    invoke-static {v3, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 432
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectActionBarView:Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

    xor-int/lit8 v4, v0, 0x1

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 433
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectDropDown:Landroid/view/View;

    xor-int/lit8 v4, v0, 0x1

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 434
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_3

    iget-boolean v4, p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 435
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_4

    iget-boolean v4, p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    if-nez v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :goto_3
    invoke-static {v3, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_5

    .line 438
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->show()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 440
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 438
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 442
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->buildDeselectedContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V

    goto :goto_4

    .line 445
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 446
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hide()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 447
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 445
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 450
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

    iget v1, p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->commitLabel:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 451
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

    iget p1, p1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->commitLabel:I

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 454
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->buildDeselectedContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V

    :goto_4
    return-void
.end method

.method private updateRolodexState(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;ZZ)V
    .locals 3

    .line 461
    sget-object v0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$1;->$SwitchMap$com$squareup$crm$RolodexContactLoaderHelper$VisualState:[I

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    .line 516
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 517
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 518
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 519
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 520
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 495
    :pswitch_1
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 496
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 500
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    if-eqz p3, :cond_0

    iget-object p3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->device:Lcom/squareup/util/Device;

    .line 501
    invoke-interface {p3}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p3

    if-nez p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 500
    :goto_0
    invoke-static {p1, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p2, :cond_1

    .line 504
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 505
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    goto :goto_1

    .line 509
    :cond_1
    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    .line 510
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->device:Lcom/squareup/util/Device;

    .line 511
    invoke-interface {p2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p2

    .line 510
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_1

    .line 487
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 488
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 489
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 490
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 491
    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    goto :goto_1

    .line 479
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 480
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 481
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 482
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 483
    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    goto :goto_1

    .line 471
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 472
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 473
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 474
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 475
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    goto :goto_1

    .line 463
    :pswitch_5
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showProgress(Z)V

    .line 464
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 465
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->emptyDirectoryMessage:Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 466
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 467
    invoke-direct {p0, v2, v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showMessage(ZLjava/lang/String;)V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 155
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 156
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->bindViews(Landroid/view/View;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->visualState:Lrx/Observable;

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->setupActionBar(Landroid/view/View;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setGroupToken(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    sget-object v1, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 165
    invoke-interface {v3}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getInitialScrollPosition()I

    move-result v3

    const/4 v4, 0x0

    .line 164
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->initForMultiSelect(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;IZ)V

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setShowGreyHeaderRows(Z)V

    .line 169
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$1Kd6teQ9L6EqlqUB4Z5zadBTN7s;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$1Kd6teQ9L6EqlqUB4Z5zadBTN7s;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 174
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$OkRNex59sqHGJGX6ZRWnGeKgvlc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$OkRNex59sqHGJGX6ZRWnGeKgvlc;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 179
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VEhQELh_lJqI05qsP9i6pMoQDiU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VEhQELh_lJqI05qsP9i6pMoQDiU;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 183
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$ZWQTqyc3Tk_0S8UY_ijSYG0ZBm8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$ZWQTqyc3Tk_0S8UY_ijSYG0ZBm8;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 188
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$XBCLrjb2TZ0VwM0cUY-fQZnPcjg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$XBCLrjb2TZ0VwM0cUY-fQZnPcjg;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 193
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$V7-iN2qnbBxTCxNlJXU2cq8_FPg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$V7-iN2qnbBxTCxNlJXU2cq8_FPg;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 206
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$2UOyeCBGImfZ34G60tvlqv8pw-I;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$2UOyeCBGImfZ34G60tvlqv8pw-I;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 212
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VZofBESJ1kcV4n7H0wOLLiRTsII;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VZofBESJ1kcV4n7H0wOLLiRTsII;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 222
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$911KK2ks7VW-T-YCdFAv08tAQOg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$911KK2ks7VW-T-YCdFAv08tAQOg;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 226
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getSupportsFilters()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$FJ_4U3w1GmXeTvFUkoazztYyDQo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$FJ_4U3w1GmXeTvFUkoazztYyDQo;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 239
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$7Ua1cGtgsuM-RdVEGCFQX-XrGNY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$7Ua1cGtgsuM-RdVEGCFQX-XrGNY;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 243
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$IET-a2LSYijLoNl6H7H899hiPfU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$IET-a2LSYijLoNl6H7H899hiPfU;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 251
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/crm/RolodexContactLoader;->setFilterList(Ljava/util/List;)V

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/FiltersLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method protected bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lrx/functions/Action0;",
            ")V"
        }
    .end annotation

    .line 407
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p3, :cond_0

    .line 409
    new-instance p3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$I6F-MsU-NETVw4WxNtrJ7-06XAI;

    invoke-direct {p3, p4, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$I6F-MsU-NETVw4WxNtrJ7-06XAI;-><init>(Lrx/Observable;Landroid/view/View;)V

    invoke-static {p1, p3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 412
    new-instance p3, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$hcFV9SlaRQ_HVFRAtTdIzPy8m5s;

    invoke-direct {p3, p0, p2, p5}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$hcFV9SlaRQ_HVFRAtTdIzPy8m5s;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;Landroid/view/View;Lrx/functions/Action0;)V

    invoke-static {p1, p3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    .line 257
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    .line 259
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1, v0}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    return-void
.end method

.method protected abstract getGroupToken()Ljava/lang/String;
.end method

.method protected abstract getSupportsFilters()Z
.end method

.method public synthetic lambda$attach$0$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchTerm()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onSearchClicked()Lrx/Observable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupHelper;->contactLoaderSearchTerm(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$raW7PjDsZrIp-L_lCXeGebchZgg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$raW7PjDsZrIp-L_lCXeGebchZgg;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    .line 171
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$8vdQUlic20o-2iw3JRqfpwcB2rA;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$8vdQUlic20o-2iw3JRqfpwcB2rA;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;)V

    .line 176
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$10$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->noResultsCreateCustomerButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    .line 214
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchTerm()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$6sCXhbRAdlhBGYiqjPJCjt-jFfE;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$6sCXhbRAdlhBGYiqjPJCjt-jFfE;

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 215
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 216
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/crm/RolodexGroupLoader;->success(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 217
    invoke-static {v1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v1

    .line 218
    :goto_0
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    .line 215
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$TOvDyT0ZiPI_lTVgyG2vLaAoJ-A;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$TOvDyT0ZiPI_lTVgyG2vLaAoJ-A;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;)V

    .line 219
    invoke-static {v2}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$11$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$CVz2a8Kb1pxd2N14V2m2rOxCzf8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$CVz2a8Kb1pxd2N14V2m2rOxCzf8;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$13$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->appliedFilters()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$MZJuB262U7luRiOwslX4aK64Qh0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$MZJuB262U7luRiOwslX4aK64Qh0;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 229
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$15$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/FiltersLayout;->onFilterClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$1E40aBWbMRUtXyP1P9vEO8oLrFM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$1E40aBWbMRUtXyP1P9vEO8oLrFM;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 241
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$17$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/FiltersLayout;->onDeleteClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$o9SThr1k6LfOmNwrbw2IQrQdnSw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$o9SThr1k6LfOmNwrbw2IQrQdnSw;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 245
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$2$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$yeuOdjLPXyAk1enkJ1Q-omt2Y4A;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$yeuOdjLPXyAk1enkJ1Q-omt2Y4A;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;)V

    .line 181
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$3$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->getMultiSelectedContactSet()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$jhbCiHDz__3d8IDbhWJ2jcn5P1M;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$jhbCiHDz__3d8IDbhWJ2jcn5P1M;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;)V

    .line 185
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$4$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->selectedContactCount()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$F7vQL-1s1XgilOPbfPpICkCv4DQ;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$F7vQL-1s1XgilOPbfPpICkCv4DQ;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;)V

    .line 190
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$6$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->selectedContactCount()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$5zInO7rLf9wAKSyaowFvkAfGv4w;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$5zInO7rLf9wAKSyaowFvkAfGv4w;

    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$c1L-wOLHDsdzpp_pD5qRAyUdwrY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$c1L-wOLHDsdzpp_pD5qRAyUdwrY;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 195
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$8$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->visualState:Lrx/Observable;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->showCreateCustomerOnNoResults()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$aL0XlOUwBsrtyZoP7J7dg6zWpzs;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$aL0XlOUwBsrtyZoP7J7dg6zWpzs;

    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$QbouFuKpdobpvDBSmD21Emuue-Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$QbouFuKpdobpvDBSmD21Emuue-Y;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 208
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bindDropDownAction$44$AbstractViewCustomersListCoordinator(Landroid/view/View;Lrx/functions/Action0;)Lrx/Subscription;
    .locals 1

    .line 413
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$H66yzIMX5Nty9mAk3lsKW-5sWPY;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$H66yzIMX5Nty9mAk3lsKW-5sWPY;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;Lrx/functions/Action0;)V

    .line 414
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$12$AbstractViewCustomersListCoordinator(Ljava/util/List;)V
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/RolodexContactLoader;->setFilterList(Ljava/util/List;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->filtersLayout:Lcom/squareup/ui/crm/v2/FiltersLayout;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/FiltersLayout;->setFilters(Ljava/util/List;)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-static {p1}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_search_customers_hint:I

    .line 235
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_search_customers_hint_filtering:I

    .line 236
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 234
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$14$AbstractViewCustomersListCoordinator(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 0

    .line 241
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->showChooseFiltersScreen()V

    return-void
.end method

.method public synthetic lambda$null$16$AbstractViewCustomersListCoordinator(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->deleteIndividualFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 247
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_FILTER_REMOVE_FILTER_PILL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public synthetic lambda$null$19$AbstractViewCustomersListCoordinator(Ljava/lang/String;Lcom/squareup/applet/Applet;)V
    .locals 2

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->upButtonAction()Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 302
    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 303
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->upButtonAction()Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 304
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 301
    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    goto :goto_0

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->marinActionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1, p2}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$23$AbstractViewCustomersListCoordinator(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Close"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->createMultiSelectEvent(Lcom/squareup/ui/crm/v2/MultiSelectMode;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$null$25$AbstractViewCustomersListCoordinator(Lkotlin/Unit;)V
    .locals 0

    .line 329
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {p1}, Lcom/squareup/ui/DropDownContainer;->toggleDropDown()V

    return-void
.end method

.method public synthetic lambda$null$28$AbstractViewCustomersListCoordinator(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->commitMultiSelectAction()V

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    const-string v1, "Done"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->createMultiSelectEvent(Lcom/squareup/ui/crm/v2/MultiSelectMode;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$null$38$AbstractViewCustomersListCoordinator(Lkotlin/Pair;)V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 384
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->buildSelectedContactSet(I)Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p1

    goto :goto_0

    .line 386
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->buildDeselectedContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object p1

    :goto_0
    const/4 v1, 0x1

    .line 383
    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setMultiSelect(ZLcom/squareup/protos/client/rolodex/ContactSet;)V

    return-void
.end method

.method public synthetic lambda$null$40$AbstractViewCustomersListCoordinator(Lkotlin/Pair;)V
    .locals 4

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "Select All"

    goto :goto_0

    :cond_0
    const-string p1, "Deselect All"

    :goto_0
    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->createMultiSelectEvent(Lcom/squareup/ui/crm/v2/MultiSelectMode;[Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$null$43$AbstractViewCustomersListCoordinator(Lrx/functions/Action0;Lkotlin/Unit;)V
    .locals 0

    .line 415
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {p2}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    .line 416
    invoke-interface {p1}, Lrx/functions/Action0;->call()V

    return-void
.end method

.method public synthetic lambda$null$5$AbstractViewCustomersListCoordinator(Lkotlin/Pair;)V
    .locals 5

    .line 196
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;

    .line 197
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 199
    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->NONE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    iget v1, v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->minSelectionAmount:I

    if-gt v1, p1, :cond_1

    iget v1, v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->maxSelectionAmount:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    iget v1, v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->maxSelectionAmount:I

    if-gt p1, v1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 201
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

    iget-boolean v4, v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v1, v4}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

    iget-boolean v0, v0, Lcom/squareup/ui/crm/v2/MultiSelectMode;->blue:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$null$7$AbstractViewCustomersListCoordinator(Lkotlin/Pair;)V
    .locals 2

    .line 209
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 208
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->updateRolodexState(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;ZZ)V

    return-void
.end method

.method public synthetic lambda$setupActionBar$20$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->defaultActionBarTitle:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    .line 297
    invoke-interface {v1}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    .line 296
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VzX8ehdWvRRPhgXT4BkT3JOa3L8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$VzX8ehdWvRRPhgXT4BkT3JOa3L8;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 298
    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setupActionBar$24$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectActionBarView:Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->onXClicked()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 321
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$O9XyL5eDM6TPXvUtqTCAXcS2N7E;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$O9XyL5eDM6TPXvUtqTCAXcS2N7E;

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$_wejXBqBxwdzn79lHWfR40HLShc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$_wejXBqBxwdzn79lHWfR40HLShc;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 322
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setupActionBar$26$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectActionBarView:Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->onDropDownClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$HStdEigmvgF-UaA5M7i44xeLKos;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$HStdEigmvgF-UaA5M7i44xeLKos;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 329
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setupActionBar$29$AbstractViewCustomersListCoordinator()Lrx/Subscription;
    .locals 3

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonBlue:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->multiSelectCommitButtonRed:Lcom/squareup/marketfont/MarketButton;

    .line 334
    invoke-static {v1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->mergeWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 335
    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$nTsyMXfweT23eMqOfVQYKuC5yO0;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$nTsyMXfweT23eMqOfVQYKuC5yO0;

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$oXOXJTpYfxdD0ttuYkHoZMPYQ_c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$oXOXJTpYfxdD0ttuYkHoZMPYQ_c;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 336
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setupActionBar$30$AbstractViewCustomersListCoordinator(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 1

    .line 354
    iget-object p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object p1, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setupActionBar$39$AbstractViewCustomersListCoordinator(Lcom/jakewharton/rxrelay/PublishRelay;Lrx/subscriptions/CompositeSubscription;)Lrx/Subscription;
    .locals 4

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 379
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->getGroupToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;

    invoke-direct {v3, p2}, Lcom/squareup/ui/crm/v2/-$$Lambda$LCFL_O8Smpcyj8Fwyue7A61jTJc;-><init>(Lrx/subscriptions/CompositeSubscription;)V

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/crm/RolodexContactLoaderHelper;->getTotalCountFound(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Ljava/lang/String;Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$KhRbjDlu8rRErznIwrhD_MG8FOk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$KhRbjDlu8rRErznIwrhD_MG8FOk;

    .line 380
    invoke-virtual {p2, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$paTqZCrNvO2fw3U97s04Y8S583M;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$paTqZCrNvO2fw3U97s04Y8S583M;

    .line 378
    invoke-virtual {p1, p2, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$LM_YgMUtYPo111pSS-RsEC6NqLI;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$LM_YgMUtYPo111pSS-RsEC6NqLI;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 382
    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setupActionBar$41$AbstractViewCustomersListCoordinator(Lcom/jakewharton/rxrelay/PublishRelay;)Lrx/Subscription;
    .locals 2

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->runner:Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;

    .line 390
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;->getMultiSelectMode()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$77ns_gktboTwkAhCnR3Idoie6nk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$77ns_gktboTwkAhCnR3Idoie6nk;

    invoke-virtual {p1, v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$4JHkhpd2TRUNpsjt36p3Y31Sa7k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$AbstractViewCustomersListCoordinator$4JHkhpd2TRUNpsjt36p3Y31Sa7k;-><init>(Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;)V

    .line 391
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected abstract setupDropDownActions(Landroid/view/View;Lrx/Observable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation
.end method

.method protected abstract showCreateCustomerOnNoResults()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract upButtonAction()Ljava/lang/Runnable;
.end method
