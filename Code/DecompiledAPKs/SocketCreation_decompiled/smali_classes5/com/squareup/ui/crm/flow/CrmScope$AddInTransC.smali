.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$AddInTransC;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;


# annotations
.annotation runtime Lcom/squareup/crm/RolodexAttachmentLoader$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/crm/RolodexEventLoader$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/crm/RolodexRecentContactLoader$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AddInTransC"
.end annotation


# virtual methods
.method public abstract chooseCustomer2ScreenV2()Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Component;
.end method

.method public abstract chooseCustomerToSaveCardScreenV2()Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;
.end method
