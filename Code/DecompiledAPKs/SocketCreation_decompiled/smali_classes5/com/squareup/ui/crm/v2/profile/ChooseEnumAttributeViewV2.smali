.class public Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;
.super Landroid/widget/LinearLayout;
.source "ChooseEnumAttributeViewV2.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private enumOptions:Lcom/squareup/widgets/CheckableGroup;

.field presenter:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private suppressCheckChangeEvent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-class p2, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Component;->inject(Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;)V

    return-void
.end method


# virtual methods
.method addEnumOptions(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 75
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 76
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v0, v3, v2, v4, v1}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$ChooseEnumAttributeViewV2(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 45
    iget-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->suppressCheckChangeEvent:Z

    if-nez p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->presenter:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->onEnumSelected(I)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$ChooseEnumAttributeViewV2(Lcom/squareup/widgets/CheckableGroup;I)Z
    .locals 1

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->presenter:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->onEnumReselected(I)V

    const/4 p1, 0x1

    .line 52
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->suppressCheckChangeEvent:Z

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/CheckableGroup;->uncheck(I)V

    const/4 p2, 0x0

    .line 54
    iput-boolean p2, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->suppressCheckChangeEvent:Z

    return p1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->presenter:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->presenter:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->dropView(Ljava/lang/Object;)V

    .line 66
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 42
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_enum_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeViewV2$oS6TA1jiSpsq7gMLp8nPJ6aoSNo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeViewV2$oS6TA1jiSpsq7gMLp8nPJ6aoSNo;-><init>(Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeViewV2$CDERGgDyN25pZ9wk2tYJRlL2w0s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeViewV2$CDERGgDyN25pZ9wk2tYJRlL2w0s;-><init>(Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedClickListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;)V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setCheckedOption(I)V
    .locals 1

    const/4 v0, 0x1

    .line 82
    iput-boolean v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->suppressCheckChangeEvent:Z

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    const/4 p1, 0x0

    .line 84
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->suppressCheckChangeEvent:Z

    return-void
.end method

.method setSingleChoiceMode(Z)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->enumOptions:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->setSingleChoiceMode(Z)V

    return-void
.end method
