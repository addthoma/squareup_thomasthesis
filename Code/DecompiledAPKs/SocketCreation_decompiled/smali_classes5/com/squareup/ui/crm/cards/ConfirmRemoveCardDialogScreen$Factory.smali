.class public final Lcom/squareup/ui/crm/cards/ConfirmRemoveCardDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmRemoveCardDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ConfirmRemoveCardDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 48
    invoke-interface {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->unlinkInstrumentConfirmClicked()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 31
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 32
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;

    .line 34
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;->viewCustomerCardScreen()Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;->runner()Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getInstrumentForUnlinkInstrumentDialog()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v1

    .line 36
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getContactForUnlinkInstrumentDialog()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_cardonfile_unlink_confirm_body:I

    invoke-static {v3, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v1, v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    .line 39
    invoke-static {v1}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->formatNameAndNumber(Lcom/squareup/protos/client/instruments/CardSummary;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string v4, "card_name"

    invoke-virtual {v3, v4, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    const-string v3, "customer_name"

    .line 40
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 42
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/crmviewcustomer/R$string;->crm_cardonfile_unlink_confirm:I

    .line 45
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->confirm:I

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ConfirmRemoveCardDialogScreen$Factory$_NPcdWGErPXzFEWYY9eCuCVpvSQ;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ConfirmRemoveCardDialogScreen$Factory$_NPcdWGErPXzFEWYY9eCuCVpvSQ;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;)V

    .line 47
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 49
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 50
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 44
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
