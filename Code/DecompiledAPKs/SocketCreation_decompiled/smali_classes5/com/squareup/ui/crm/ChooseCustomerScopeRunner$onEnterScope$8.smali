.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;->invoke(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 4

    .line 165
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0, v2, p1, v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$sendResult(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;Z)V

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$8;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getFlow$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lflow/Flow;

    move-result-object v0

    sget-object v2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v3, 0x1

    new-array v3, v3, [Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v3, v1

    invoke-static {v0, v2, v3}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method
