.class Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "ProfileAttachmentsRenameFileDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;

.field final synthetic val$dialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;Landroid/app/AlertDialog;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;->val$dialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 73
    invoke-super {p0, p1}, Lcom/squareup/debounce/DebouncedTextWatcher;->doAfterTextChanged(Landroid/text/Editable;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRenameFileDialog$Factory$2;->val$dialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    .line 75
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 76
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
