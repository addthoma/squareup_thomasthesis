.class public interface abstract Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator$Runner;
.super Ljava/lang/Object;
.source "AllCustomersMasterCoordinator.java"

# interfaces
.implements Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/AllCustomersMasterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract allCustomersListHardwareBackButton()V
.end method

.method public abstract showResolveDuplicatesScreen()V
.end method

.method public abstract viewFeedbackV2()V
.end method

.method public abstract viewGroupsList()V
.end method
