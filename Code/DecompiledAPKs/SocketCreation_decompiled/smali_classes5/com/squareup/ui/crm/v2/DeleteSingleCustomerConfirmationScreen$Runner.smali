.class public interface abstract Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Runner;
.super Ljava/lang/Object;
.source "DeleteSingleCustomerConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelDeleteSingleCustomer()V
.end method

.method public abstract deleteSingleCustomer()V
.end method

.method public abstract getDeleteProfileConfirmationText()Ljava/lang/String;
.end method
