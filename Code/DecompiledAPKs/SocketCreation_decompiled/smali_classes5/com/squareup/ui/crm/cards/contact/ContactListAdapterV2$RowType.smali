.class final enum Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;
.super Ljava/lang/Enum;
.source "ContactListAdapterV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

.field public static final enum BOTTOM_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

.field public static final enum CONTACT:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

.field public static final enum TOP2_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

.field public static final enum TOP_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 22
    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    const/4 v1, 0x0

    const-string v2, "TOP_ROW"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    const/4 v2, 0x1

    const-string v3, "TOP2_ROW"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP2_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    const/4 v3, 0x2

    const-string v4, "CONTACT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->CONTACT:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    new-instance v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    const/4 v4, 0x3

    const-string v5, "BOTTOM_ROW"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->BOTTOM_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    .line 21
    sget-object v5, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP2_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->CONTACT:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->BOTTOM_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->$VALUES:[Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;
    .locals 1

    .line 21
    const-class v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->$VALUES:[Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object v0
.end method
