.class public Lcom/squareup/ui/crm/cards/EditFilterScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "EditFilterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;,
        Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;,
        Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/EditFilterScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 214
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$4iJ6U0c1oNDfdsH3cPaMeF83Uv4;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$4iJ6U0c1oNDfdsH3cPaMeF83Uv4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/EditFilterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/EditFilterScreen;
    .locals 1

    .line 215
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 216
    new-instance v0, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 210
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_EDIT_FILTER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_edit_filter_card_view:I

    return v0
.end method
