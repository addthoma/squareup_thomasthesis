.class public Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;
.super Ljava/lang/Object;
.source "ChooseDateAttributeFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen$Runner;


# instance fields
.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

.field private final flow:Lflow/Flow;

.field private final locale:Ljava/util/Locale;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lflow/Flow;Ljava/util/Locale;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 38
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    .line 39
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->flow:Lflow/Flow;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->locale:Ljava/util/Locale;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-void
.end method


# virtual methods
.method public getDateAttrViewData()Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 114
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    .line 115
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "dateAttrViewData"

    .line 76
    invoke-static {p1, v0}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->load(Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    const-string v1, "dateAttrViewData"

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->save(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;Landroid/os/Bundle;Ljava/lang/String;)V

    return-void
.end method

.method public onSave(Lcom/squareup/protos/common/time/DateTime;)V
    .locals 5

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 96
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    iget-object v1, v1, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 98
    invoke-virtual {v2, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object p1

    .line 97
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    goto :goto_1

    :cond_1
    move-object p1, v0

    .line 103
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    iget-object v3, v3, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    iget v4, v4, Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;->index:I

    invoke-static {v2, v3, p1, v4}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpdatedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 104
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    .line 105
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V
    .locals 0

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->dateAttr:Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 52
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->flow:Lflow/Flow;

    new-instance p3, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;

    invoke-direct {p3, p1}, Lcom/squareup/ui/crm/v2/ChooseDateAttributeDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {p2, p3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->sellerCreatingCustomer()Z

    return-void
.end method
