.class public Lcom/squareup/ui/crm/rows/CustomerCardView;
.super Landroid/widget/LinearLayout;
.source "CustomerCardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;
    }
.end annotation


# instance fields
.field private final cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

.field private final cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private final onCardEntryDone:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onCardInvalid:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/Card$PanWarning;",
            ">;"
        }
    .end annotation
.end field

.field private final onCardValid:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end field

.field private final removeButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardValid:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 29
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardInvalid:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 30
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardEntryDone:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 p2, 0x1

    .line 38
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/CustomerCardView;->setOrientation(I)V

    .line 40
    sget p2, Lcom/squareup/ui/crm/R$layout;->crm_customer_card_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 42
    sget p1, Lcom/squareup/ui/crm/R$id;->crm_card_input_row_editor:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/CardEditor;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    .line 44
    sget p1, Lcom/squareup/ui/crm/R$id;->crm_save_card_name_number:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 45
    sget p1, Lcom/squareup/ui/crm/R$id;->crm_save_card_remove_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->removeButton:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardEntryDone:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardInvalid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public initCardEditor(Lcom/squareup/CountryCode;)V
    .locals 3

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method public onCardEntryDone()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardEntryDone:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onCardInvalid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card$PanWarning;",
            ">;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardInvalid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onCardValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public setCardData(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->clear()V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->removeButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->removeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/ui/crm/rows/CustomerCardView$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/rows/CustomerCardView$1;-><init>(Lcom/squareup/ui/crm/rows/CustomerCardView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setHint(I)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(I)V

    return-void
.end method

.method public showCardEntry()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->hasCard()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->onCardValid:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/CardEditor;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    new-instance v2, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;-><init>(Lcom/squareup/ui/crm/rows/CustomerCardView;Lcom/squareup/ui/crm/rows/CustomerCardView$1;)V

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/CardEditor;->setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->cardNameNumberTextView:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView;->removeButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void
.end method
