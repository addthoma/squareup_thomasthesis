.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;
.super Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;
.source "ProfileAttachmentsScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 120
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsScreen$peECuHu_k6n3TxZdQbh2AVfWL5M;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsScreen$peECuHu_k6n3TxZdQbh2AVfWL5M;

    .line 121
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;
    .locals 1

    .line 122
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    .line 123
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    .line 124
    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 116
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;->profileAttachmentsPath:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 48
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 38
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 39
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 40
    invoke-interface {p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->profileAttachmentsCoordinator()Lcom/squareup/ui/crm/cards/ProfileAttachmentsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 44
    sget v0, Lcom/squareup/profileattachments/R$layout;->crm_profile_attachments_view:I

    return v0
.end method
