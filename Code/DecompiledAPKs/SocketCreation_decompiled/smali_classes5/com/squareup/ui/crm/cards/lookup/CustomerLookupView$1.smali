.class Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "CustomerLookupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$1;->this$0:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView$1;->this$0:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->presenter:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupPresenter;->onSearchTermChanged(Ljava/lang/String;)V

    return-void
.end method
