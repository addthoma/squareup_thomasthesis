.class Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "DippedCardSpinnerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method private shouldProcessDip()Z
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 104
    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;->cancelDippedCardSpinnerScreen()V

    const/4 v0, 0x1

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 65
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;

    .line 67
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/crm/R$string;->crm_cardonfile_savecard_header:I

    .line 69
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$sOlL_AHVbYsSdUYIJzWo6T6FYxc;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$sOlL_AHVbYsSdUYIJzWo6T6FYxc;-><init>(Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;)V

    .line 70
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 71
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    .line 72
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 73
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 75
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->shouldProcessDip()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {p1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->processDip()V

    goto :goto_0

    .line 81
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Presenter;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {p1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->cancel()V

    :goto_0
    return-void
.end method
