.class public final Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ExpiringPointsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;,
        Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;,
        Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 87
    const-class v0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;

    .line 88
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 83
    sget v0, Lcom/squareup/crmscreens/R$layout;->crm_v2_expiring_points_view:I

    return v0
.end method
