.class public interface abstract Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;
.super Ljava/lang/Object;
.source "CustomerActivityScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerActivityScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeCustomerActivityScreen()V
.end method

.method public abstract getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
.end method

.method public abstract showBillHistoryScreen(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract showConversationScreen(Ljava/lang/String;)V
.end method
