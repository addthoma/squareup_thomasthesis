.class public interface abstract Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;
.super Ljava/lang/Object;
.source "SelectCustomersScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/SelectCustomersScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/SelectCustomersScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract addCustomersToGroupScreen()Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;
.end method

.method public abstract addingCustomersToGroupScreen()Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Component;
.end method

.method public abstract createGroupScreen()Lcom/squareup/ui/crm/cards/CreateGroupScreen$Component;
.end method

.method public abstract deleteCustomersConfirmation()Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Component;
.end method

.method public abstract deletingCustomers()Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Component;
.end method

.method public abstract mergeCustomersScreen()Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;
.end method

.method public abstract mergingCustomersScreen()Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Component;
.end method

.method public abstract selectLoyaltyPhoneCoordinator()Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;
.end method
