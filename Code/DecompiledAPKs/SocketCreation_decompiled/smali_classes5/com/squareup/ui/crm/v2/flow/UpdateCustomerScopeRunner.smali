.class public Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;
.super Ljava/lang/Object;
.source "UpdateCustomerScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;
.implements Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Runner;
.implements Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;
.implements Lcom/squareup/ui/addressbook/PickAddressBookContactScreen$Runner;


# static fields
.field private static final PERMISSION_REQUEST_CODE:I = 0x64


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private analyticsPathName:Ljava/lang/String;

.field private busy:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseGroupsFlow:Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final contactChangedByDialogScreen:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

.field private crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field private enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

.field private errorString:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final maybeContactAddressBook:Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;

.field private final permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

.field private final phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation
.end field

.field private type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

.field private uniqueKey:Ljava/util/UUID;

.field private updateContactDisposable:Lio/reactivex/disposables/Disposable;

.field private final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

.field private updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field private updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

.field private updateCustomerScreenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final updateDateFlow:Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;Lcom/squareup/util/Res;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 2
    .param p15    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateContactDisposable:Lio/reactivex/disposables/Disposable;

    .line 130
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->uniqueKey:Ljava/util/UUID;

    const/4 v1, 0x0

    .line 140
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const-string v1, ""

    .line 141
    invoke-static {v1}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    .line 146
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactChangedByDialogScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    move-object v1, p1

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    move-object v1, p2

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p3

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    move-object v1, p4

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p5

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object v1, p6

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object v1, p7

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->chooseGroupsFlow:Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    move-object v1, p8

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateDateFlow:Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    move-object v1, p9

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p10

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-object v1, p11

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    move-object v1, p12

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->maybeContactAddressBook:Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;

    move-object v1, p13

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    move-object/from16 v1, p14

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    move-object/from16 v1, p15

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method private complete(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 5

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->goBackInCustomerFlow()Z

    .line 419
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    sget-object v4, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$sWsQ3bN6X8IcmQiXYvXiPFDQGV8;->INSTANCE:Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$sWsQ3bN6X8IcmQiXYvXiPFDQGV8;

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v0, v1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->emitResult(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V

    return-void
.end method

.method private static isValidCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)Ljava/lang/Integer;
    .locals 1

    .line 432
    sget-object v0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner$1;->$SwitchMap$com$squareup$updatecustomerapi$UpdateCustomerFlow$ContactValidationType:[I

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 453
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez p0, :cond_5

    .line 454
    sget p0, Lcom/squareup/crmupdatecustomer/R$string;->crm_add_customer_first_name_validation_error:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    .line 434
    :cond_1
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-nez p1, :cond_2

    .line 435
    sget p0, Lcom/squareup/crmupdatecustomer/R$string;->crm_add_customer_first_name_validation_error:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    .line 438
    :cond_2
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->given_name:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 439
    sget p0, Lcom/squareup/crmupdatecustomer/R$string;->crm_add_customer_first_name_validation_error:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    .line 442
    :cond_3
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->surname:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 443
    sget p0, Lcom/squareup/crmupdatecustomer/R$string;->crm_add_customer_last_name_validation_error:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    .line 446
    :cond_4
    iget-object p1, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    .line 447
    invoke-static {p0}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_5

    goto :goto_1

    :cond_5
    :goto_0
    const/4 p0, 0x0

    return-object p0

    .line 448
    :cond_6
    :goto_1
    sget p0, Lcom/squareup/crmupdatecustomer/R$string;->crm_add_customer_email_address_validation_error:I

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$complete$9(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 424
    const-class v1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$3(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 240
    new-instance v6, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    xor-int/lit8 v3, p0, 0x1

    .line 241
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    move-object v0, v6

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;-><init>(ZZZZLjava/lang/String;)V

    return-object v6
.end method

.method private onSavingResponse(Lcom/squareup/protos/client/rolodex/UpsertContactResponse;)V
    .locals 4

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne v0, v1, :cond_3

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->APPOINTMENT_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_CREATE_CONTACT:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_CREATE_CONTACT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    goto :goto_1

    .line 375
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICE_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    goto :goto_1

    .line 382
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_EDIT_CONTACT_SAVED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/crm/events/CrmEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CRM_EDIT_CONTACT_SAVED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2}, Lcom/squareup/crm/events/CrmEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    const-string v3, "Save"

    invoke-static {v1, v2, v3}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createEditContactEvent(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 388
    :goto_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_4

    .line 390
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->complete(Lcom/squareup/protos/client/rolodex/Contact;)V

    goto :goto_2

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public X2displayCustomerDetailsOnBran(Lcom/squareup/x2/customers/CustomerInfoWithState$State;)V
    .locals 1

    .line 311
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayCustomerDetails(Lcom/squareup/protos/client/rolodex/Contact;)Z

    return-void
.end method

.method public addressBookClicked()V
    .locals 4

    .line 466
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 467
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v0

    .line 468
    sget-object v1, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner$1;->$SwitchMap$com$squareup$ui$systempermissions$SystemPermissionsPresenter$Status:[I

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 480
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown permission status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    const/16 v1, 0x64

    sget-object v2, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(ILcom/squareup/systempermissions/SystemPermission;)V

    goto :goto_0

    .line 471
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/permissions/RequestContactsPermissionDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 483
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public analyticsPathName()Ljava/lang/String;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    return-object v0
.end method

.method public closeChooseEnumAttributeScreen(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 3

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    iget-object v1, v1, Lcom/squareup/ui/crm/rows/EnumAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    iget v2, v2, Lcom/squareup/ui/crm/rows/EnumAttribute;->index:I

    .line 304
    invoke-static {v0, v1, p1, v2}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpdatedAttribute(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;I)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    const/4 p1, 0x0

    .line 306
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    .line 307
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closePickAddressBookContactScreen()V
    .locals 2

    .line 399
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closePickAddressBookContactScreen(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 0

    .line 404
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_0

    .line 405
    iget-object p4, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {p4, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withName(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 407
    :cond_0
    invoke-static {p2}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 408
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->withEmail(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 410
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->phoneNumberHelper:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {p1, p3}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 411
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {p1, p3}, Lcom/squareup/crm/util/RolodexContactHelper;->withPhone(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 413
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    const-class p2, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    invoke-static {p1, p2}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeUpdateCustomerScreen()V
    .locals 4

    .line 315
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    const-string v3, "Close"

    invoke-static {v1, v2, v3}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createEditContactEvent(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v0, 0x0

    .line 317
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->complete(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public contactChangedByDialogScreen()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactChangedByDialogScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getEnumAttribute()Lcom/squareup/ui/crm/rows/EnumAttribute;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 249
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;",
            ">;"
        }
    .end annotation

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScreenData:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->crm_create_new_customer_title:I

    .line 365
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    .line 366
    invoke-static {v0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    return-object v0
.end method

.method public synthetic lambda$null$6$UpdateCustomerScopeRunner(Lcom/squareup/protos/client/rolodex/UpsertContactResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 341
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->onSavingResponse(Lcom/squareup/protos/client/rolodex/UpsertContactResponse;)V

    return-void
.end method

.method public synthetic lambda$null$7$UpdateCustomerScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 343
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 344
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_0

    .line 345
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    .line 347
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 350
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->crm_contact_saving_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$UpdateCustomerScopeRunner(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->onContactUpdated(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 203
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    const-string v2, "Group"

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createEditContactEvent(Ljava/lang/String;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$UpdateCustomerScopeRunner(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 211
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->onContactUpdated(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 212
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactChangedByDialogScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$UpdateCustomerScopeRunner(Lcom/squareup/x2/customers/CustomerInfoWithState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p1, v0}, Lcom/squareup/x2/customers/CustomerInfoWithState;->updateContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->onContactUpdated(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public synthetic lambda$saveContact$4$UpdateCustomerScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 331
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 332
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    const-string v0, ""

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveContact$5$UpdateCustomerScopeRunner()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveContact$8$UpdateCustomerScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 339
    new-instance v0, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$l6tv59i38c6JiJM85PoFPbe6k1Y;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$l6tv59i38c6JiJM85PoFPbe6k1Y;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$Hogn89HBsi8AW7t1QhrGNCKbYnE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$Hogn89HBsi8AW7t1QhrGNCKbYnE;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onContactUpdated(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 273
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 275
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne p1, v0, :cond_1

    .line 276
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 277
    invoke-static {v0, v1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->isValidCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 276
    :goto_0
    invoke-interface {p1, v0}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->invoiceCustomerUpdated(Z)V

    :cond_1
    return-void
.end method

.method public onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 173
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->baseContact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eqz v0, :cond_1

    .line 182
    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->saveCustomerContact(Lcom/squareup/protos/client/rolodex/Contact;)Z

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->getUpdateCustomerAnalyticsPathType(Lcom/squareup/ui/crm/flow/CrmScopeType;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "Customers Applet"

    .line 187
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->analyticsPathName:Ljava/lang/String;

    .line 191
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->maybeRegister(Lmortar/MortarScope;Lmortar/Scoped;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexGroupLoader;->setIncludeCounts(Z)V

    .line 194
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->shouldShowAddressBookButton()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->chooseGroupsFlow:Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    .line 199
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$GJY2S8d4eAP3MiRGdHfJ9sSVNEo;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$GJY2S8d4eAP3MiRGdHfJ9sSVNEo;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 200
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 198
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateDateFlow:Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$7Zsy587oLfC3Kp-IRtQIzRa6MRQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$7Zsy587oLfC3Kp-IRtQIzRa6MRQ;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 209
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 207
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 222
    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onBuyerContactEntered()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$JymyukWcgG6bJsYuQfwwGgsrUSw;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$JymyukWcgG6bJsYuQfwwGgsrUSw;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 223
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 221
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    .line 233
    invoke-virtual {p1}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    .line 234
    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 236
    invoke-interface {v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onCanDisplayDetailsToBuyer()Lio/reactivex/Observable;

    move-result-object v2

    .line 237
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$VcwY0Vm-t16yf7xp3L5RrNnpDaQ;->INSTANCE:Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$VcwY0Vm-t16yf7xp3L5RrNnpDaQ;

    .line 232
    invoke-static {p1, v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScreenData:Lio/reactivex/Observable;

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v3, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne v2, v3, :cond_4

    sget-object v2, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->CREATING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/squareup/x2/customers/CustomerInfoWithState$State;->EDITING:Lcom/squareup/x2/customers/CustomerInfoWithState$State;

    :goto_1
    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onCreateOrEditCustomer(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/x2/customers/CustomerInfoWithState$State;)Z

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 253
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->shouldShowAddressBookButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateContactDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method public onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 1

    .line 488
    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->CONTACTS:Lcom/squareup/systempermissions/SystemPermission;

    if-ne p2, v0, :cond_1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    goto :goto_0

    .line 493
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {p2, v0}, Lcom/squareup/ui/addressbook/PickAddressBookContactScreen;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    nop

    :cond_1
    :goto_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 505
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contact"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 506
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const-string v1, "busy"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 507
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    const-string v1, "errorString"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    const-string v0, "enumAttribute"

    .line 508
    invoke-static {p1, v0}, Lcom/squareup/ui/crm/rows/EnumAttribute;->load(Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/ui/crm/rows/EnumAttribute;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    const-string/jumbo v0, "uniqueKey"

    .line 509
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->uniqueKey:Ljava/util/UUID;

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "busy"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 515
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "errorString"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    const-string v1, "enumAttribute"

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/crm/rows/EnumAttribute;->save(Lcom/squareup/ui/crm/rows/EnumAttribute;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public saveContact()V
    .locals 3

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->isValidCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->errorString:Lio/reactivex/subjects/BehaviorSubject;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->busy:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateContactDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->uniqueKey:Ljava/util/UUID;

    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->upsertContact(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$P58otQ_8cItdlpWWEE7nOHKKyuk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$P58otQ_8cItdlpWWEE7nOHKKyuk;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 330
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$sLdi5GAi60q-1AjNiu14N7iVGH4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$sLdi5GAi60q-1AjNiu14N7iVGH4;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 335
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$qO_fE-eueVMNrJLABQi3j53NW1w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$UpdateCustomerScopeRunner$qO_fE-eueVMNrJLABQi3j53NW1w;-><init>(Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;)V

    .line 339
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateContactDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public shouldShowAddressBookButton()Z
    .locals 2

    .line 462
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->maybeContactAddressBook:Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;

    invoke-interface {v0}, Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public showChooseCustomDateAttributeScreen(Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateDateFlow:Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/crm/v2/flow/ChooseDateAttributeFlow;->showFirstScreen(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/rows/EditDateAttributeRow$DateAttribute;)V

    return-void
.end method

.method public showChooseEnumAttributeScreen(Lcom/squareup/ui/crm/rows/EnumAttribute;)V
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_CUSTOM_ATTRIBUTES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 293
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->enumAttribute:Lcom/squareup/ui/crm/rows/EnumAttribute;

    .line 294
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showChooseGroupsScreen()V
    .locals 3

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->chooseGroupsFlow:Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;->showFirstScreen(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
