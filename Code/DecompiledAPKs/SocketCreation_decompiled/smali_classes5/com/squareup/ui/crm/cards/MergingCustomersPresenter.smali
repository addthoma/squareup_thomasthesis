.class Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;
.super Lmortar/Presenter;
.source "MergingCustomersPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/MergingCustomersDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final contactResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 49
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->contactResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->res:Lcom/squareup/util/Res;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 59
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method private formatMergedCustomersTitle(I)Ljava/lang/String;
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_merged_customers_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 147
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatMergingCustomersTitle(I)Ljava/lang/String;
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_merging_customers_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 141
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 142
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    instance-of v0, p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0

    .line 96
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$3(Lcom/squareup/util/Optional;Ljava/lang/Long;)Lcom/squareup/util/Optional;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/MergingCustomersDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 63
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/MergingCustomersDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$4$MergingCustomersPresenter(Lcom/squareup/ui/crm/cards/MergingCustomersDialog;Lcom/squareup/util/Optional;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 118
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->hideProgress()V

    .line 120
    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->formatMergedCustomersTitle(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->successMergedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    goto :goto_0

    .line 126
    :cond_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 127
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_merging_customers_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    .line 130
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const-wide/16 v0, 0x3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$6$MergingCustomersPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->closeMergingCustomersScreen()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$MergingCustomersPresenter(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/SingleSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    sget-object v0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    .line 75
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    .line 74
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/crm/RolodexServiceHelper;->manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 77
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$2$MergingCustomersPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ManualMergeContactsResponse;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$lnevmBE2zdwu67Burb6uoU4aemE;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$lnevmBE2zdwu67Burb6uoU4aemE;

    .line 92
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 100
    :cond_0
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$MergingCustomersPresenter(Lcom/squareup/ui/crm/cards/MergingCustomersDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->contactResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$JICrnAqbFx7KSBGb4lVMlkVD3Io;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$JICrnAqbFx7KSBGb4lVMlkVD3Io;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;Lcom/squareup/ui/crm/cards/MergingCustomersDialog;)V

    .line 117
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$MergingCustomersPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$uMTjcYZJh23XOYWm3FAPjf_DceQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$uMTjcYZJh23XOYWm3FAPjf_DceQ;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;)V

    .line 136
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 67
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->supportsLoyaltyDirectoryIntegration()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$AXb6zmi-5gnD7G32elpZl6WmghE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$AXb6zmi-5gnD7G32elpZl6WmghE;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;)V

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    .line 82
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/crm/RolodexServiceHelper;->manualMergeContacts(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    .line 87
    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$2XV1jvrOPzit_XXGLciquEB5P1o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$2XV1jvrOPzit_XXGLciquEB5P1o;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;)V

    .line 89
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 104
    invoke-static {v1, v2, v3, v4}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$lzE2fyKLUBFDMOFLFzVrGfkwyDA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$lzE2fyKLUBFDMOFLFzVrGfkwyDA;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->contactResponse:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 105
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 109
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->runner:Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;->formatMergingCustomersTitle(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->showText(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$YksfOD7jJz6BDMmO_XQ3y7KZSgw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$YksfOD7jJz6BDMmO_XQ3y7KZSgw;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;Lcom/squareup/ui/crm/cards/MergingCustomersDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingCustomersDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$VM9oWmwRacMc2ao9DKrsI2kfgH4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingCustomersPresenter$VM9oWmwRacMc2ao9DKrsI2kfgH4;-><init>(Lcom/squareup/ui/crm/cards/MergingCustomersPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
