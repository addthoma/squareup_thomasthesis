.class public interface abstract Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;
.super Ljava/lang/Object;
.source "ExpiringPointsScreen.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract coordinator()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;
.end method

.method public abstract runner()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;
.end method
