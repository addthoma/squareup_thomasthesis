.class Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "AbstractCrmScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showManageCouponsAndRewardsScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V
    .locals 0

    .line 1035
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 1037
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->access$100(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;->this$0:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->showFirstScreen(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V

    return-void
.end method
