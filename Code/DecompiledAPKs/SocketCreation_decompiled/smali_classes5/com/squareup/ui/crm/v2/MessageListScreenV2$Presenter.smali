.class Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "MessageListScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/MessageListScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/MessageListViewV2;",
        ">;"
    }
.end annotation


# static fields
.field private static final MESSAGES_NEXT_PAGE_SIZE:I = 0x32


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private conversationListItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final conversationLoader:Lcom/squareup/crm/ConversationLoader;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final locale:Ljava/util/Locale;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/ConversationLoader;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;Ljava/util/Locale;Ljava/text/DateFormat;Ljava/text/DateFormat;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 69
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationListItems:Ljava/util/List;

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->locale:Ljava/util/Locale;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 76
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->dateFormatter:Ljava/text/DateFormat;

    return-void
.end method

.method private createActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 3

    .line 123
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_feedback_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 125
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/-$$Lambda$J4SkAv4FXb7fZ6BlM8yr16-mj-8;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/-$$Lambda$J4SkAv4FXb7fZ6BlM8yr16-mj-8;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 126
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/squareup/protos/client/dialogue/ConversationListItem;)Ljava/lang/String;
    .locals 0

    .line 118
    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/ConversationListItem;->token:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method formatCreatorTimestamp(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 141
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 142
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getConversations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/dialogue/ConversationListItem;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationListItems:Ljava/util/List;

    return-object v0
.end method

.method public synthetic lambda$null$0$MessageListScreenV2$Presenter(Lcom/squareup/ui/crm/v2/MessageListViewV2;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 92
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 96
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 99
    :cond_0
    iput-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationListItems:Ljava/util/List;

    .line 101
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->refresh()V

    return-void
.end method

.method public synthetic lambda$null$2$MessageListScreenV2$Presenter(Ljava/lang/Integer;)V
    .locals 1

    .line 108
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 110
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->getConversations()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/crm/ConversationLoader;->loadMore(Ljava/lang/Integer;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$1$MessageListScreenV2$Presenter(Lcom/squareup/ui/crm/v2/MessageListViewV2;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/ConversationLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$019qgITjVHAJnBGbpFzo_UnrpAU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$019qgITjVHAJnBGbpFzo_UnrpAU;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$MessageListScreenV2$Presenter(Lcom/squareup/ui/crm/v2/MessageListViewV2;)Lrx/Subscription;
    .locals 1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->endOfListIndex()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$lh4pAyyrOT4z1jsfzhICRl-oqM0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$lh4pAyyrOT4z1jsfzhICRl-oqM0;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;)V

    .line 107
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$MessageListScreenV2$Presenter(Lcom/squareup/ui/crm/v2/MessageListViewV2;)Lrx/Subscription;
    .locals 2

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->selectConversationListItem()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$4c0lYoO0POMCseK5xZ01wcnd2RE;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$4c0lYoO0POMCseK5xZ01wcnd2RE;

    .line 118
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$LRC8i63K2Cu7rs0Wq1hkE--dTgY;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$LRC8i63K2Cu7rs0Wq1hkE--dTgY;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;)V

    .line 119
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MESSAGING_SHOW_CONVERSATION_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->conversationLoader:Lcom/squareup/crm/ConversationLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/ConversationLoader;->refresh()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 81
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/MessageListViewV2;

    .line 84
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->createActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/MessageListViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$J4SkAv4FXb7fZ6BlM8yr16-mj-8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$J4SkAv4FXb7fZ6BlM8yr16-mj-8;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 89
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$Q4UElljlPj1ng-fBqfxP3GwyVqI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$Q4UElljlPj1ng-fBqfxP3GwyVqI;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 105
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$VwNAstrYeW1wR6R6Sx-NN64-3x4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$VwNAstrYeW1wR6R6Sx-NN64-3x4;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 116
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$3iD9cAd2-0lgjLen6JwJfHOlfB8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$MessageListScreenV2$Presenter$3iD9cAd2-0lgjLen6JwJfHOlfB8;-><init>(Lcom/squareup/ui/crm/v2/MessageListScreenV2$Presenter;Lcom/squareup/ui/crm/v2/MessageListViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
