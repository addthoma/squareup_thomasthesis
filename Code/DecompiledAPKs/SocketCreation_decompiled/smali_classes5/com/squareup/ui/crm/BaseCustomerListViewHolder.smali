.class public abstract Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;,
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;,
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$LoadingViewHolder;,
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ErrorViewHolder;,
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;,
        Lcom/squareup/ui/crm/BaseCustomerListViewHolder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \t2\u00020\u0001:\u0006\t\n\u000b\u000c\r\u000eB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u0082\u0001\u0005\u000f\u0010\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "Companion",
        "ContactViewHolder",
        "CreateCustomerViewHolder",
        "ErrorViewHolder",
        "HeaderViewHolder",
        "LoadingViewHolder",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$LoadingViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ErrorViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CONTACT_ROW:I = 0x1

.field public static final CREATE_CUSTOMER_ROW:I = 0x3

.field public static final Companion:Lcom/squareup/ui/crm/BaseCustomerListViewHolder$Companion;

.field public static final ERROR_ROW:I = 0x4

.field public static final HEADER_ROW:I = 0x0

.field public static final LOADING_ROW:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;->Companion:Lcom/squareup/ui/crm/BaseCustomerListViewHolder$Companion;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public abstract bind(Lcom/squareup/ui/crm/BaseCustomerListItem;)V
.end method
