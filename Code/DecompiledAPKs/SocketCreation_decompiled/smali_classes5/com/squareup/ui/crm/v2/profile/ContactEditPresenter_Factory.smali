.class public final Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;
.super Ljava/lang/Object;
.source "ContactEditPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final birthdayFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatCustomAttrProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexMerchantLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->birthdayFormatterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->rolodexMerchantLoaderProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->dateFormatCustomAttrProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;"
        }
    .end annotation

    .line 64
    new-instance v8, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Ljava/text/DateFormat;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;
    .locals 9

    .line 71
    new-instance v8, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;-><init>(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Ljava/text/DateFormat;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->birthdayFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->rolodexMerchantLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crm/RolodexMerchantLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->dateFormatCustomAttrProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->newInstance(Lcom/squareup/ui/crm/cards/birthday/BirthdayFormatter;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Ljava/text/DateFormat;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter_Factory;->get()Lcom/squareup/ui/crm/v2/profile/ContactEditPresenter;

    move-result-object v0

    return-object v0
.end method
