.class Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "MergeProposalListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->getItemCount()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 38
    new-instance p2, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 42
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo p2, "viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 34
    :cond_1
    new-instance p2, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;-><init>(Landroid/content/Context;)V

    .line 45
    :goto_0
    new-instance p1, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    new-instance p1, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter$1;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;Landroid/view/View;)V

    return-object p1
.end method

.method public onViewAttachedToWindow(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 2

    .line 58
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast p1, Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->bind(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V

    goto :goto_0

    .line 68
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo v0, "viewType"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListAdapter;->presenter:Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v1, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->bind(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;I)V

    :goto_0
    return-void
.end method
