.class public final Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;
.super Lcom/squareup/ui/crm/edit/EditCustomerState$Editing;
.source "EditCustomerState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/edit/EditCustomerState$Editing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditingGroups"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B=\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0007\u00a2\u0006\u0002\u0010\u000cJ\u0015\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J\u000f\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0007H\u00c6\u0003JI\u0010\u0018\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0007H\u00c6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u001aH\u00d6\u0001J\t\u0010 \u001a\u00020\u0004H\u00d6\u0001J\u0019\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u001aH\u00d6\u0001R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R \u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000e\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState$Editing;",
        "data",
        "",
        "",
        "Lcom/squareup/crm/model/ContactAttribute;",
        "keys",
        "",
        "attribute",
        "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
        "allGroups",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "(Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)V",
        "getAllGroups",
        "()Ljava/util/List;",
        "getAttribute",
        "()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
        "getData",
        "()Ljava/util/Map;",
        "getKeys",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final allGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field

.field private final attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

.field private final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private final keys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups$Creator;

    invoke-direct {v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups$Creator;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keys"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attribute"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allGroups"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->data:Ljava/util/Map;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->keys:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    iput-object p4, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->copy(Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keys"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attribute"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allGroups"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;-><init>(Ljava/util/Map;Ljava/util/List;Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;Ljava/util/List;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    iget-object v1, p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllGroups()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    return-object v0
.end method

.method public final getAttribute()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    return-object v0
.end method

.method public getData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/crm/model/ContactAttribute;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->data:Ljava/util/Map;

    return-object v0
.end method

.method public getKeys()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->keys:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditingGroups(data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getData()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", keys="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getKeys()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", attribute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allGroups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->data:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crm/model/ContactAttribute;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->keys:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->attribute:Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->allGroups:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_1

    :cond_1
    return-void
.end method
