.class final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SeeAllRewardTiersCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->call(Ljava/lang/Integer;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;->invoke(Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;->this$0:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/CompactLoyaltyRewardRow;->getTierProto()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/loyalty/ui/RewardAdapterKt;->toRewardTier(Lcom/squareup/server/account/protos/RewardTier;)Lcom/squareup/protos/client/loyalty/RewardTier;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->unredeemRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)V

    return-void
.end method
