.class public final Lcom/squareup/ui/crm/applet/CustomersAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CustomersAppletScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;,
        Lcom/squareup/ui/crm/applet/CustomersAppletScope$ParentComponent;,
        Lcom/squareup/ui/crm/applet/CustomersAppletScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/CustomersAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    .line 151
    sget-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->INSTANCE:Lcom/squareup/ui/crm/applet/CustomersAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 3

    .line 46
    const-class v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;

    .line 47
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v1

    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;->scopeRunner()Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 48
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/CustomersAppletScope$Component;->mergeCustomersFlow()Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
