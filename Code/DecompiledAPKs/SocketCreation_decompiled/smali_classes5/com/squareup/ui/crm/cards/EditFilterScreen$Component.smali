.class public interface abstract Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;
.super Ljava/lang/Object;
.source "EditFilterScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/EditFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/EditFilterCardView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V
.end method
