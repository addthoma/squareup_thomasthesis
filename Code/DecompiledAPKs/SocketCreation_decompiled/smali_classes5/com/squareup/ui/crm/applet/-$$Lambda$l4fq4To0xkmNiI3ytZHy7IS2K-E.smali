.class public final synthetic Lcom/squareup/ui/crm/applet/-$$Lambda$l4fq4To0xkmNiI3ytZHy7IS2K-E;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func6;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/applet/-$$Lambda$l4fq4To0xkmNiI3ytZHy7IS2K-E;->f$0:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/-$$Lambda$l4fq4To0xkmNiI3ytZHy7IS2K-E;->f$0:Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    check-cast p3, Ljava/lang/Integer;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    check-cast p4, Ljava/lang/Boolean;

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    check-cast p5, Ljava/lang/Integer;

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    check-cast p6, Ljava/lang/Boolean;

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/RightPaneDataRenderer;->createRightPaneData(IZIZIZ)Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;

    move-result-object p1

    return-object p1
.end method
