.class public interface abstract Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Component;
.super Ljava/lang/Object;
.source "ResolveDuplicatesScreen.java"

# interfaces
.implements Lcom/squareup/ui/ErrorsBarView$Component;
.implements Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/ResolveDuplicatesCardView;)V
.end method
