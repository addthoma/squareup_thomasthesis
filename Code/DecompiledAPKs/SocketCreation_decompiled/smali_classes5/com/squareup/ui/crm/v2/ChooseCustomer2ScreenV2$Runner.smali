.class public interface abstract Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;
.super Ljava/lang/Object;
.source "ChooseCustomer2ScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeChooseCustomerScreen()V
.end method

.method public abstract closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract getContactLoaderForSearch()Lcom/squareup/crm/RolodexContactLoader;
.end method

.method public abstract getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
.end method

.method public abstract getSearchTerm()Ljava/lang/String;
.end method

.method public abstract setSearchTerm(Ljava/lang/String;)V
.end method

.method public abstract showCreateCustomerScreen()V
.end method
