.class public Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;
.super Landroid/widget/LinearLayout;
.source "SelectableLoyaltyPhoneRow.java"


# instance fields
.field private checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private contactName:Landroid/widget/TextView;

.field private loyaltyAccountMappingToken:Ljava/lang/String;

.field private phoneNumber:Landroid/widget/TextView;

.field private piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getChecked()Z
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public getLoyaltyAccountMappingToken()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->loyaltyAccountMappingToken:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Landroid/widget/TextView;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->phoneNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 27
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 29
    sget v0, Lcom/squareup/crm/R$id;->crm_loyalty_phone_check:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    .line 30
    sget v0, Lcom/squareup/crm/R$id;->crm_customer_display_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->contactName:Landroid/widget/TextView;

    .line 31
    sget v0, Lcom/squareup/crm/R$id;->crm_loyalty_phone_number:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->phoneNumber:Landroid/widget/TextView;

    .line 32
    sget v0, Lcom/squareup/crm/R$id;->crm_customer_name_pii_wrapper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/accessibility/AccessibilityScrubber;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->checkBox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    return-void
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->contactName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLoyaltyAccountMappingToken(Ljava/lang/String;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->loyaltyAccountMappingToken:Ljava/lang/String;

    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->phoneNumber:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setPiiNameScrubberContentDescription(I)V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/SelectableLoyaltyPhoneRow;->piiNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    sget v1, Lcom/squareup/crm/R$string;->crm_customer_name_row_content_description:I

    .line 37
    invoke-static {p0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "index"

    .line 38
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 36
    invoke-virtual {v0, p1}, Lcom/squareup/accessibility/AccessibilityScrubber;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
