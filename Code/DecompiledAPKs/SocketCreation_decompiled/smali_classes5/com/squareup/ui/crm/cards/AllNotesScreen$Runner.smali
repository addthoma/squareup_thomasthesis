.class public interface abstract Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;
.super Ljava/lang/Object;
.source "AllNotesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllNotesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeAllNotesScreen()V
.end method

.method public abstract getContactForAllNotesScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract showViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V
.end method
