.class public final enum Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;
.super Ljava/lang/Enum;
.source "ViewCustomerCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExposedAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum AddToSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum NewAppointment:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum NewSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum RemoveFromEstimate:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum RemoveFromInvoice:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum RemoveFromSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

.field public static final enum TransferLoyalty:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;


# instance fields
.field public longTitle:I

.field public registerActionName:Lcom/squareup/analytics/RegisterActionName;

.field public shortTitle:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 905
    new-instance v6, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->new_sale_with_customer:I

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->new_sale_with_customer_shortened:I

    sget-object v5, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_NEW_SALE:Lcom/squareup/analytics/RegisterActionName;

    const-string v1, "NewSale"

    const/4 v2, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v6, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 907
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v10, Lcom/squareup/crmviewcustomer/R$string;->book_new_appointment_with_customer:I

    sget v11, Lcom/squareup/crmviewcustomer/R$string;->book_new_appointment_with_customer_shortened:I

    sget-object v12, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_APPOINTMENT:Lcom/squareup/analytics/RegisterActionName;

    const-string v8, "NewAppointment"

    const/4 v9, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewAppointment:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 910
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_add_customer_title:I

    sget v5, Lcom/squareup/crmviewcustomer/R$string;->crm_add_to_sale:I

    sget-object v6, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_ADD_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    const-string v2, "AddToSale"

    const/4 v3, 0x2

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->AddToSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 912
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v10, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_sale:I

    sget v11, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_sale_shortened:I

    sget-object v12, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_SALE:Lcom/squareup/analytics/RegisterActionName;

    const-string v8, "RemoveFromSale"

    const/4 v9, 0x3

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 915
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_invoice:I

    sget v5, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_invoice_shortened:I

    sget-object v6, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    const-string v2, "RemoveFromInvoice"

    const/4 v3, 0x4

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromInvoice:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 918
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v10, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_estimate:I

    sget v11, Lcom/squareup/crmviewcustomer/R$string;->crm_remove_customer_from_estimate_shortened:I

    sget-object v12, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_MENU_REMOVE_FROM_ESTIMATE:Lcom/squareup/analytics/RegisterActionName;

    const-string v8, "RemoveFromEstimate"

    const/4 v9, 0x5

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromEstimate:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 921
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transfer_account:I

    sget v5, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transfer_account_short:I

    sget-object v6, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_VIEW_PROFILE_TRANSFER_LOYALTY:Lcom/squareup/analytics/RegisterActionName;

    const-string v2, "TransferLoyalty"

    const/4 v3, 0x6

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;-><init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->TransferLoyalty:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    .line 904
    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->NewAppointment:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->AddToSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromSale:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromInvoice:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->RemoveFromEstimate:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->TransferLoyalty:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->$VALUES:[Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/squareup/analytics/RegisterActionName;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/squareup/analytics/RegisterActionName;",
            ")V"
        }
    .end annotation

    .line 931
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 932
    iput p3, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->longTitle:I

    .line 933
    iput p4, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->shortTitle:I

    .line 934
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->registerActionName:Lcom/squareup/analytics/RegisterActionName;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;
    .locals 1

    .line 904
    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;
    .locals 1

    .line 904
    sget-object v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->$VALUES:[Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$ExposedAction;

    return-object v0
.end method
