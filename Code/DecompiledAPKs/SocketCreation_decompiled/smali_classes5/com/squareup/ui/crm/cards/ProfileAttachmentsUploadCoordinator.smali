.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ProfileAttachmentsUploadCoordinator.java"


# instance fields
.field private failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private inProgressView:Landroid/widget/ProgressBar;

.field private final runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

.field private successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private uploadText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    return-void
.end method

.method private setFailure()V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    sget-object v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->FAILURE:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-interface {v1, v0, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->getUploadStatusText(Landroid/view/View;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->inProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->uploadText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private setInProgress()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->inProgressView:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void
.end method

.method private setSuccess()V
    .locals 3

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    sget-object v2, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->SUCCESS:Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;

    invoke-interface {v1, v0, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->getUploadStatusText(Landroid/view/View;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->inProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->uploadText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 34
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 36
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 37
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 38
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$SfMpr1ZJ-kZJsv4hXXz2zWZ2uAg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SfMpr1ZJ-kZJsv4hXXz2zWZ2uAg;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 40
    sget v0, Lcom/squareup/profileattachments/R$id;->success_glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->successView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 41
    sget v0, Lcom/squareup/profileattachments/R$id;->failed_glyph_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->failView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 42
    sget v0, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_upload_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->inProgressView:Landroid/widget/ProgressBar;

    .line 44
    sget v0, Lcom/squareup/profileattachments/R$id;->crm_profile_attachments_upload_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->uploadText:Landroid/widget/TextView;

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->uploadText:Landroid/widget/TextView;

    sget v1, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_upload_in_progress:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 47
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadCoordinator$aoCv0XNqNeeTmpdOAOguunpkUiM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadCoordinator$aoCv0XNqNeeTmpdOAOguunpkUiM;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->uploadFile(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ProfileAttachmentsUploadCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->runner:Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->getUploadState()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadCoordinator$8zXvBknN8TPbjxYgqh4ZLGrGfQQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsUploadCoordinator$8zXvBknN8TPbjxYgqh4ZLGrGfQQ;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;)V

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ProfileAttachmentsUploadCoordinator(Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    sget-object v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator$1;->$SwitchMap$com$squareup$ui$crm$cards$ProfileAttachmentsUploadState$UploadState:[I

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState$UploadState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->setInProgress()V

    goto :goto_0

    .line 54
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->setFailure()V

    goto :goto_0

    .line 51
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadCoordinator;->setSuccess()V

    :goto_0
    return-void
.end method
