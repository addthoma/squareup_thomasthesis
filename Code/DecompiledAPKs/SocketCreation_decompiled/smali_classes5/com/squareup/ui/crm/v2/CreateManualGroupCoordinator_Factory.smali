.class public final Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;
.super Ljava/lang/Object;
.source "CreateManualGroupCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;Lcom/squareup/ui/ErrorsBarPresenter;)Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;-><init>(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;Lcom/squareup/ui/ErrorsBarPresenter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->newInstance(Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator$Runner;Lcom/squareup/ui/ErrorsBarPresenter;)Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator_Factory;->get()Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

    move-result-object v0

    return-object v0
.end method
