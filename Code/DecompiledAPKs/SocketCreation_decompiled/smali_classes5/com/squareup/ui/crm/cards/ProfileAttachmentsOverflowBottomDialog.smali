.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;
.super Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;
.source "ProfileAttachmentsOverflowBottomDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsOverflowBottomDialog$i-LJYLtspOC-WyzLrL71TeWt7rY;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsOverflowBottomDialog$i-LJYLtspOC-WyzLrL71TeWt7rY;

    .line 82
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;
    .locals 1

    .line 83
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 84
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 77
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InProfileAttachmentsScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;->profileAttachmentsPath:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
