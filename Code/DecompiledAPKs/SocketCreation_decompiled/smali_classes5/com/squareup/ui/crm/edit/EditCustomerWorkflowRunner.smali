.class public interface abstract Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;
.super Ljava/lang/Object;
.source "EditCustomerWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00072\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "start",
        "",
        "initialProps",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;->Companion:Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract start(Lcom/squareup/ui/crm/edit/EditCustomerProps;)V
.end method
