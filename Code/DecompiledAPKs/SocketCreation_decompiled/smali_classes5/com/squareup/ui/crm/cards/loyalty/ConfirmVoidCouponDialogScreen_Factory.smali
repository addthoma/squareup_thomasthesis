.class public final Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;
.super Ljava/lang/Object;
.source "ConfirmVoidCouponDialogScreen_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;",
        ">;"
    }
.end annotation


# instance fields
.field private final crmPathProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CrmScope;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CrmScope;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;->crmPathProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CrmScope;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/flow/CrmScope;)Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;->crmPathProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;->newInstance(Lcom/squareup/ui/crm/flow/CrmScope;)Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen_Factory;->get()Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;

    move-result-object v0

    return-object v0
.end method
