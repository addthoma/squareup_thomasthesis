.class final Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;
.super Ljava/lang/Object;
.source "PosCofDippedCardInfoProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/EmvListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/PinRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)V
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onCardError()V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onCardRemovedDuringPayment()V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 0

    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    .line 315
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 0

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onMagFallbackSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 276
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "We should not be configured such that onMagFallbackSwipeFailed will be called."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onMagFallbackSwipeSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 271
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "We should not be configured such that onMagFallbackSwipeSuccess will be called."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    return-void
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 297
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "onPaymentDeclined should not be called."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 290
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "onPaymentReversed should not be called."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 304
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    .line 309
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onSigRequested()V
    .locals 2

    .line 258
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onSigRequested should not be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 0

    .line 321
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    return-void
.end method

.method public onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
    .locals 1

    .line 240
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "We should not be configured such that onSwipeForFallback will be called."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onUseChipCardDuringFallback()V
    .locals 2

    .line 235
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We should not be configured such that onUseChipCardDuringFallback will be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 263
    iget-object p2, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    .line 264
    invoke-static {p2}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->access$200(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p2

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;->this$0:Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;

    invoke-virtual {v0, p3, p1, p2}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipSuccess(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;)V

    return-void
.end method
