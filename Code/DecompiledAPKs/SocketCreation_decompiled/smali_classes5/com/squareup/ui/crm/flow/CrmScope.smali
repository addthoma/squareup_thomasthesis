.class public final Lcom/squareup/ui/crm/flow/CrmScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/crm/flow/CrmScope$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/CrmScope$ViewInTransC;,
        Lcom/squareup/ui/crm/flow/CrmScope$ViewCustomerFromServicesComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ViewInRetailDetailC;,
        Lcom/squareup/ui/crm/flow/CrmScope$ViewInInvoiceOnDetailC;,
        Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$ApiCardOnFileBootstrapComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ViewInAppletC;,
        Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$AddInTransC;,
        Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerC;,
        Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$BaseAddCustomerToSaleModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$TransactionsHistoryRefundHelperModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$BillHistoryModule;,
        Lcom/squareup/ui/crm/flow/CrmScope$ComponentFactory;,
        Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ParentRetailHomeAppletScopeComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ParentInvoicesAppletScopeComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$ParentCustomersAppletComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;,
        Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;,
        Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/flow/CrmScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final billPayment:Lcom/squareup/payment/BillPayment;

.field public final contact:Lcom/squareup/protos/client/rolodex/Contact;

.field public final holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field public final holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

.field public final maybeSourceContactToTransferFrom:Lcom/squareup/protos/client/rolodex/Contact;

.field public final parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

.field public final readOnly:Z

.field public final type:Lcom/squareup/ui/crm/flow/CrmScopeType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1153
    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$CrmScope$JlXcOnlm6chcmmJVWqS85mLYhck;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$CrmScope$JlXcOnlm6chcmmJVWqS85mLYhck;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/CrmScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 390
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 391
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 392
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 393
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 394
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 395
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/CrmScope;->billPayment:Lcom/squareup/payment/BillPayment;

    .line 396
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    .line 397
    iput-boolean p7, p0, Lcom/squareup/ui/crm/flow/CrmScope;->readOnly:Z

    .line 398
    iput-object p8, p0, Lcom/squareup/ui/crm/flow/CrmScope;->maybeSourceContactToTransferFrom:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method public static firstPostTransactionAddCardScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/payment/BillPayment;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 10

    .line 217
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 220
    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 223
    new-instance p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object p0

    .line 225
    :cond_0
    new-instance p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object p0
.end method

.method public static firstPostTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/PaymentReceipt;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 9

    .line 202
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v3

    .line 203
    invoke-interface {v3}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 204
    new-instance p1, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 205
    invoke-static {v3}, Lcom/squareup/ui/crm/flow/CrmScope;->getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 206
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0

    .line 209
    :cond_0
    new-instance p1, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 212
    new-instance p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static firstX2PostTransactionAddCardScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Payment;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 10

    .line 231
    instance-of v0, p1, Lcom/squareup/payment/BillPayment;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/BillPayment;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v6, v0

    .line 232
    new-instance v0, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 235
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 238
    new-instance p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object p0

    .line 240
    :cond_1
    new-instance p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object p0
.end method

.method public static firstX2PostTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Payment;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 10

    .line 247
    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 249
    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScope;->getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 250
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0

    .line 252
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_ADD_CUSTOMER_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v4, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 256
    new-instance p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static forAddingCustomerInSplitTicketScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 10

    .line 155
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 158
    new-instance p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static forAppointmentsCustomerInDetail(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 10

    .line 261
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_APPOINTMENT:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 264
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static forInvoiceCreateFromDraftContact(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Order;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)Lcom/squareup/container/ContainerTreeKey;
    .locals 10

    .line 311
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 315
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->builderForNameString(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 316
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    .line 317
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile;->newBuilder()Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 318
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    .line 317
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object p1

    .line 318
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object p1

    .line 316
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object p0

    .line 319
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v6

    .line 321
    sget-object v2, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CREATE_FOR_INVOICE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    sget-object v3, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_FIRST_LAST_EMAIL:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    sget-object v5, Lcom/squareup/ui/crm/flow/CrmScopeType;->CREATE_CUSTOMER_FOR_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-object v0, p2

    move-object v1, v9

    invoke-interface/range {v0 .. v6}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p0

    return-object p0
.end method

.method public static forInvoiceCustomerInDetail(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 10

    .line 271
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p2

    move-object v4, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 274
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static forInvoiceLikeInEditViewCustomer(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Order;Z)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 9

    if-eqz p2, :cond_0

    .line 291
    sget-object p2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    goto :goto_0

    .line 293
    :cond_0
    sget-object p2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    :goto_0
    move-object v2, p2

    .line 296
    new-instance p2, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 297
    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScope;->getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p2

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 299
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static forViewingCustomerInSplitTicketScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 10

    .line 163
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 165
    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScope;->getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 166
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, v9}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method private static getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 1136
    invoke-interface {p0}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1137
    invoke-interface {p0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1138
    invoke-interface {p0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    goto :goto_0

    .line 1139
    :cond_0
    invoke-interface {p0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->toContact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/flow/CrmScope;
    .locals 10

    .line 1154
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 1155
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/CrmScopeType;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v3

    .line 1156
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/ProtosPure;->decodeOrNull(Lcom/squareup/wire/ProtoAdapter;[B)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/protos/client/rolodex/Contact;

    .line 1157
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    const/4 v8, 0x0

    .line 1158
    :goto_0
    new-instance p0, Lcom/squareup/ui/crm/flow/CrmScope;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    return-object p0
.end method

.method public static newApiCrmScope(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/crm/flow/CrmScope;
    .locals 10

    .line 355
    new-instance v9, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CARD_TO_CUSTOMER:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    return-object v9
.end method

.method public static newReadOnlyCrmScopeCopy(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 11

    .line 364
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    new-instance v10, Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/CrmScope;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v1, v3, :cond_0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    :goto_0
    move-object v3, v1

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v6, p0, Lcom/squareup/ui/crm/flow/CrmScope;->billPayment:Lcom/squareup/payment/BillPayment;

    iget-object v7, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v1, v10

    move-object v5, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method

.method public static newTransferLoyaltyCustomerCard(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 9

    .line 378
    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->inAddCustomerToSale(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    :goto_0
    move-object v2, p1

    .line 380
    new-instance p1, Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v5, p0, Lcom/squareup/ui/crm/flow/CrmScope;->billPayment:Lcom/squareup/payment/BillPayment;

    iget-object v6, p0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    const/4 v7, 0x0

    move-object v0, p1

    move-object v4, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    .line 384
    new-instance p0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object p0
.end method

.method public static newViewCustomerCardScreenInApplet(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 11

    .line 349
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    new-instance v10, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v2, p0

    move-object v4, p2

    move-object v5, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method

.method public static newViewCustomerDetailScreenInAppletV2(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 11

    .line 333
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;

    new-instance v10, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v2, p0

    move-object v4, p2

    move-object v5, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v10}, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method

.method public static newViewCustomerDetailScreenInRetailHomeApplet(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 11

    .line 341
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;

    new-instance v10, Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v2, p0

    move-object v4, p2

    move-object v5, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v10}, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method

.method public static newViewInTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 1

    .line 175
    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScope;->getBaseContactFromHoldsCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-static {p0, v0, p1, p2}, Lcom/squareup/ui/crm/flow/CrmScope;->newViewInTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object p0

    return-object p0
.end method

.method public static newViewInTransactionCrmScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/crm/flow/InCrmScope;
    .locals 11

    .line 186
    new-instance v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    new-instance v10, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 188
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    :goto_0
    move-object v3, v1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v10

    move-object v2, p0

    move-object v4, p2

    move-object v5, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/crm/flow/CrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/BillPayment;Lcom/squareup/checkout/HoldsCoupons;ZLcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v10}, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 1147
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CrmScope;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1148
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/CrmScopeType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1149
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {p2}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1150
    iget-boolean p2, p0, Lcom/squareup/ui/crm/flow/CrmScope;->readOnly:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 1119
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_1

    .line 1122
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CrmScope;->maybeSourceContactToTransferFrom:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_0

    .line 1123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/TransferLoyalty/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1125
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1128
    :cond_1
    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 407
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CrmScope;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 402
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;

    .line 403
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;->baseRunner()Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
