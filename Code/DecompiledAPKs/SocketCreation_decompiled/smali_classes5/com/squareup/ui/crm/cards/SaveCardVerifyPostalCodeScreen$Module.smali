.class public Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Module;
.super Ljava/lang/Object;
.source "SaveCardVerifyPostalCodeScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providesEmailAppAvailable(Landroid/app/Application;)Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "mailto:"

    .line 190
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/util/Intents;->hasLinkableAppToUri(Landroid/app/Application;Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method
