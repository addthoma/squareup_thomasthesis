.class public final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealEditCustomerWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;",
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0003H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;",
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "viewFactory",
        "Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;",
        "workflow",
        "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "getWorkflow",
        "()Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "initialProps",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget-object v0, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;->Companion:Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 18
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 19
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 16
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->workflow:Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->workflow:Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->getWorkflow()Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public start(Lcom/squareup/ui/crm/edit/EditCustomerProps;)V
    .locals 1

    const-string v0, "initialProps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
