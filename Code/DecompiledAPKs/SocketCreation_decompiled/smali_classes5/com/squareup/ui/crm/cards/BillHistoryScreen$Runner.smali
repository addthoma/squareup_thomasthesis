.class public interface abstract Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;
.super Ljava/lang/Object;
.source "BillHistoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/BillHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract billHistoryOrServerError()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;",
            ">;"
        }
    .end annotation
.end method

.method public abstract closeBillHistoryCard()V
.end method

.method public abstract closeBillHistoryCardAndShowWarning(Lcom/squareup/widgets/warning/Warning;)V
.end method

.method public abstract getTitleForBillHistoryCard()Ljava/lang/String;
.end method

.method public abstract reprintTicket()V
.end method

.method public abstract showFirstIssueReceiptScreen()V
.end method

.method public abstract showFirstIssueRefundScreen()V
.end method

.method public abstract startPrintGiftReceiptFlow()V
.end method
