.class public interface abstract Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;
.super Ljava/lang/Object;
.source "DeletingLoyaltyAccountScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeDeletingLoyaltyAccountScreen()V
.end method

.method public abstract getLoyaltyAccountToken()Ljava/lang/String;
.end method

.method public abstract onDeletingLoyaltyAccountSuccess()V
.end method
