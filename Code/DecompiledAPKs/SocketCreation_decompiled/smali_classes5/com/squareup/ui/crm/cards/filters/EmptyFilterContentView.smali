.class public Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;
.super Landroid/widget/LinearLayout;
.source "EmptyFilterContentView.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;


# instance fields
.field presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const-class p2, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;)V

    return-void
.end method


# virtual methods
.method public filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;->filter()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;->isValid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;->dropView(Ljava/lang/Object;)V

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 25
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    return-void
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentPresenter;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method
