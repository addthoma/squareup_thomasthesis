.class public Lcom/squareup/ui/crm/cards/SaveFiltersCardView;
.super Landroid/widget/LinearLayout;
.source "SaveFiltersCardView.java"


# instance fields
.field private groupName:Lcom/squareup/ui/XableEditText;

.field private final onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 31
    const-class p2, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Component;->inject(Lcom/squareup/ui/crm/cards/SaveFiltersCardView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/SaveFiltersCardView;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 57
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->presenter:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->presenter:Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveFiltersScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_group_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->groupName:Lcom/squareup/ui/XableEditText;

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->groupName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveFiltersCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveFiltersCardView$1;-><init>(Lcom/squareup/ui/crm/cards/SaveFiltersCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method onGroupNameChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method showGroupName(Ljava/lang/String;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveFiltersCardView;->groupName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
