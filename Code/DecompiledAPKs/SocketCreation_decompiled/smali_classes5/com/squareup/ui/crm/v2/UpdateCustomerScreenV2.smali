.class public Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;
.super Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;
.source "UpdateCustomerScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$ScreenData;,
        Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 124
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerScreenV2$uLdQzMXupUdHstiY8BYxiYrMXs0;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$UpdateCustomerScreenV2$uLdQzMXupUdHstiY8BYxiYrMXs0;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;
    .locals 1

    .line 125
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 126
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 127
    new-instance v0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 120
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InUpdateCustomerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;->updateCustomerScope:Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_CREATE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_V2_DIRECTORY_EDIT_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    :goto_0
    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 67
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 62
    const-class v0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;

    .line 63
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2$Component;->coordinator()Lcom/squareup/ui/crm/v2/UpdateCustomerCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 58
    sget v0, Lcom/squareup/crmupdatecustomer/R$layout;->crm_v2_update_customer_view:I

    return v0
.end method
