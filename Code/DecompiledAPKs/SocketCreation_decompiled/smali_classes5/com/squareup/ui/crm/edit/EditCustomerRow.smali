.class public abstract Lcom/squareup/ui/crm/edit/EditCustomerRow;
.super Ljava/lang/Object;
.source "EditCustomerRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/edit/EditCustomerRow$BooleanRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$NumberRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$EnumRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$GroupsRow;,
        Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0008\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerRow;",
        "",
        "()V",
        "key",
        "",
        "getKey",
        "()Ljava/lang/String;",
        "AddressRow",
        "BooleanRow",
        "EmailRow",
        "EnumRow",
        "GroupsRow",
        "NameRow",
        "NumberRow",
        "TextRow",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$BooleanRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$EmailRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$TextRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$NumberRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$NameRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$EnumRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$GroupsRow;",
        "Lcom/squareup/ui/crm/edit/EditCustomerRow$AddressRow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/ui/crm/edit/EditCustomerRow;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getKey()Ljava/lang/String;
.end method
