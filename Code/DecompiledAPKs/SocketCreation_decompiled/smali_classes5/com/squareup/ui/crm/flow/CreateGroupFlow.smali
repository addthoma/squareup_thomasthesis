.class public Lcom/squareup/ui/crm/flow/CreateGroupFlow;
.super Ljava/lang/Object;
.source "CreateGroupFlow.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/CreateGroupFlow$Component;
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public closeCreateGroupScreen(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/CreateGroupScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/CreateGroupScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/CreateGroupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
