.class Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "BillHistoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/BillHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/BillHistoryCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistoryOrServerError:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;",
            ">;"
        }
    .end annotation
.end field

.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

.field private screen:Lcom/squareup/ui/crm/cards/BillHistoryScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 99
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 92
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 94
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->billHistoryOrServerError:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$11$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;Lkotlin/Unit;)V
    .locals 0

    .line 159
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->temporarilyDisablePrintGiftReceiptButton()V

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->startPrintGiftReceiptFlow()V

    return-void
.end method

.method public synthetic lambda$null$3$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->hasBillHistory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object p2, p2, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    goto :goto_0

    .line 131
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->hasError()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    iget-object p2, p2, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->serverError:Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;

    .line 133
    invoke-virtual {p2}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;->toWarningStrings()Lcom/squareup/widgets/warning/WarningStrings;

    move-result-object p2

    .line 132
    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->closeBillHistoryCardAndShowWarning(Lcom/squareup/widgets/warning/Warning;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$null$5$BillHistoryScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->showFirstIssueRefundScreen()V

    return-void
.end method

.method public synthetic lambda$null$7$BillHistoryScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->showFirstIssueReceiptScreen()V

    return-void
.end method

.method public synthetic lambda$null$9$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;Lkotlin/Unit;)V
    .locals 0

    .line 151
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->temporarilyDisableReprintTicketButton()V

    .line 152
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->reprintTicket()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$BillHistoryScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 109
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$BillHistoryScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$10$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lrx/Subscription;
    .locals 2

    .line 149
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->onReprintTicketButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$tOe-JTwxQIvyIeHs50odg8-CxhY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$tOe-JTwxQIvyIeHs50odg8-CxhY;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    .line 150
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$12$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lrx/Subscription;
    .locals 2

    .line 157
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$wpAu_oPSyOaB0hr36FhQzs7U3pQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$wpAu_oPSyOaB0hr36FhQzs7U3pQ;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    .line 158
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$RUwmO7kWbuDxVMIy-zEYl4uLNt0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$RUwmO7kWbuDxVMIy-zEYl4uLNt0;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->billHistoryOrServerError:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$gI1CaeJu21cCP9POpSS9vlfez-s;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$gI1CaeJu21cCP9POpSS9vlfez-s;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    .line 128
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lrx/Subscription;
    .locals 1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$1uYgoErD8v7VISqeoRJ8J-HVvvY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$1uYgoErD8v7VISqeoRJ8J-HVvvY;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;)V

    .line 140
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$BillHistoryScreen$Presenter(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)Lrx/Subscription;
    .locals 1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$TvKxAoGTWBllue7GQO9BAU3iqKg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$TvKxAoGTWBllue7GQO9BAU3iqKg;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;)V

    .line 145
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 104
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 105
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->screen:Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    .line 108
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->billHistoryOrServerError()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$6QR5wgvP0VlvHDCgGuyjy7DMZZc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$6QR5wgvP0VlvHDCgGuyjy7DMZZc;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;)V

    .line 109
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$N22YOE_SYDxiTev5ZS7N-N1CB6M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$N22YOE_SYDxiTev5ZS7N-N1CB6M;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;)V

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->billHistoryOrServerError:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 111
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 115
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/BillHistoryCardView;

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->screen:Lcom/squareup/ui/crm/cards/BillHistoryScreen;

    invoke-static {v1}, Lcom/squareup/ui/crm/cards/BillHistoryScreen;->access$000(Lcom/squareup/ui/crm/cards/BillHistoryScreen;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    .line 120
    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;->getTitleForBillHistoryCard()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$FVh4-hDd0_P1OPqsrvDnks7-kj0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$FVh4-hDd0_P1OPqsrvDnks7-kj0;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 124
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$wphAi45n73ZjJBcL_xenomKaR6g;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$wphAi45n73ZjJBcL_xenomKaR6g;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 127
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$jxgNDMytVppPGaWsqNKT3HzVJi8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$jxgNDMytVppPGaWsqNKT3HzVJi8;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 138
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$gibw5eY_O9zQK2lHIHDdT1Oz_14;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$gibw5eY_O9zQK2lHIHDdT1Oz_14;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 143
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$ZIFoE1CvzlF_w9CUwA2sMXm8bRw;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$ZIFoE1CvzlF_w9CUwA2sMXm8bRw;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 148
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$lEt5LjZfqueFWR_Yxtq2zVN6H94;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$lEt5LjZfqueFWR_Yxtq2zVN6H94;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 156
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$NIMbfrnMNqj8u6wd_WAbhUmbkJQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$BillHistoryScreen$Presenter$NIMbfrnMNqj8u6wd_WAbhUmbkJQ;-><init>(Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
