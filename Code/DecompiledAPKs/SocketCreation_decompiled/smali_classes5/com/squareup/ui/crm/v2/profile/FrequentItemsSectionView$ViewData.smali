.class public Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;
.super Ljava/lang/Object;
.source "FrequentItemsSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewData"
.end annotation


# instance fields
.field public final frequentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;"
        }
    .end annotation
.end field

.field public final showAll:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/FrequentItem;",
            ">;)V"
        }
    .end annotation

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-boolean p1, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;->showAll:Z

    .line 97
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;->frequentItems:Ljava/util/List;

    return-void
.end method
