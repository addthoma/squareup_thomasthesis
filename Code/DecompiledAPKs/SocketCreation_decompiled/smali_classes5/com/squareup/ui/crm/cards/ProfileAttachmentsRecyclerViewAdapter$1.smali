.class Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ProfileAttachmentsRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;

.field final synthetic val$holder:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;->val$holder:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;

    invoke-static {p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->access$000(Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;)Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;

    iget-object v0, v0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter;->attachments:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$1;->val$holder:Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsRecyclerViewAdapter$ProfileAttachmentViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen$Runner;->showPreviewScreen(Lcom/squareup/protos/client/rolodex/Attachment;)V

    return-void
.end method
