.class public Lcom/squareup/ui/crm/rows/AppointmentRow;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "AppointmentRow.java"


# static fields
.field private static final NO_COLOR:I

.field private static final NO_IMAGE:I


# instance fields
.field private icon:Landroid/widget/FrameLayout;

.field private iconImageView:Landroid/widget/ImageView;

.field private iconTextView:Landroid/widget/TextView;

.field private subtitleField:Landroid/widget/TextView;

.field private titleField:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getIconColor(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/ui/crm/rows/AppointmentRow$1;->$SwitchMap$com$squareup$crm$viewcustomerconfiguration$api$CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType:[I

    invoke-virtual {p1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    return p1

    .line 80
    :pswitch_0
    sget p1, Lcom/squareup/crm/R$color;->appointment_row_upcoming:I

    return p1

    .line 77
    :pswitch_1
    sget p1, Lcom/squareup/crm/R$color;->appointment_row_cancelled:I

    return p1

    .line 74
    :pswitch_2
    sget p1, Lcom/squareup/crm/R$color;->appointment_row_past:I

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getIconImage(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I
    .locals 1

    .line 87
    sget-object v0, Lcom/squareup/ui/crm/rows/AppointmentRow$1;->$SwitchMap$com$squareup$crm$viewcustomerconfiguration$api$CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType:[I

    invoke-virtual {p1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 91
    :cond_0
    sget p1, Lcom/squareup/crm/R$drawable;->crm_appointment_no_show:I

    return p1

    .line 89
    :cond_1
    sget p1, Lcom/squareup/crm/R$drawable;->crm_appointment_cancelled:I

    return p1
.end method

.method public static inflateAppointmentRow(Landroid/view/ViewGroup;)Lcom/squareup/ui/crm/rows/AppointmentRow;
    .locals 3

    .line 30
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/squareup/crm/R$layout;->crm_appointment_row:I

    const/4 v2, 0x0

    .line 31
    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/rows/AppointmentRow;

    return-object p0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .line 39
    invoke-super {p0}, Landroidx/constraintlayout/widget/ConstraintLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/crm/R$id;->icon:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->icon:Landroid/widget/FrameLayout;

    .line 42
    sget v0, Lcom/squareup/crm/R$id;->icon_image:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconImageView:Landroid/widget/ImageView;

    .line 43
    sget v0, Lcom/squareup/crm/R$id;->icon_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconTextView:Landroid/widget/TextView;

    .line 44
    sget v0, Lcom/squareup/crm/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->titleField:Landroid/widget/TextView;

    .line 45
    sget v0, Lcom/squareup/crm/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->subtitleField:Landroid/widget/TextView;

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_row_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->setMinHeight(I)V

    return-void
.end method

.method public setViewData(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;)V
    .locals 3

    .line 52
    iget-object v0, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->type:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getIconImage(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->type:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getIconImage(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->iconTextView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->iconText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    :goto_0
    iget-object v0, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->type:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getIconColor(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->icon:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->type:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/rows/AppointmentRow;->getIconColor(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel$AppointmentType;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->titleField:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/AppointmentRow;->subtitleField:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
