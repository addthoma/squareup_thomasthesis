.class public interface abstract Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;
.super Ljava/lang/Object;
.source "ConversationCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ConversationCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeConversationScreen()V
.end method

.method public abstract getConversationTokenForConversationScreen()Ljava/lang/String;
.end method

.method public abstract showAddCouponScreen()V
.end method

.method public abstract showBillHistoryScreen(Lcom/squareup/billhistory/model/BillHistory;)V
.end method
