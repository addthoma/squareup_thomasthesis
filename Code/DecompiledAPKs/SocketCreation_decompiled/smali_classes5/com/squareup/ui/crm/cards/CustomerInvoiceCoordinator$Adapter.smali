.class Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "CustomerInvoiceCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;,
        Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final INVOICE_ROW_TYPE:I


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$200(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 172
    check-cast p1, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->onBindViewHolder(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;I)V
    .locals 0

    .line 207
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;->bind(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 172
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$ViewHolder;
    .locals 0

    if-nez p2, :cond_0

    .line 191
    invoke-static {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 192
    new-instance p2, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$1;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    new-instance p2, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter$InvoiceViewHolder;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;Landroid/view/View;)V

    return-object p2

    .line 202
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
