.class public Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;
.super Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;
.source "ViewGroupMasterCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;
    }
.end annotation


# instance fields
.field private isSmartGroup:Z

.field private final runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;Lcom/squareup/analytics/Analytics;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/applet/AppletSelection;Lcom/squareup/applet/BadgePresenter;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p10

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object v9, p3

    move-object/from16 v10, p9

    .line 59
    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator$Runner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;Lcom/squareup/applet/AppletSelection;)V

    move-object v1, p2

    .line 61
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    return-void
.end method

.method static synthetic lambda$null$4(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$setupDropDownActions$11(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Boolean;
    .locals 1

    .line 139
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexGroupHelper;->getGroupMembershipCount(Lcom/squareup/protos/client/rolodex/Group;)I

    move-result p0

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$12(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 140
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$2(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Boolean;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-eq v0, v1, :cond_1

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->audience_type:Lcom/squareup/protos/client/rolodex/AudienceType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/AudienceType;->GROUP_V2_SMART:Lcom/squareup/protos/client/rolodex/AudienceType;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$8(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Boolean;
    .locals 1

    .line 126
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$setupDropDownActions$9(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 127
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$showCreateCustomerOnNoResults$15(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Boolean;
    .locals 1

    .line 161
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private logAction(Ljava/lang/String;)V
    .locals 4

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iget-boolean v1, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->isSmartGroup:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v2

    .line 167
    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createSmartGroupEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v2

    .line 168
    invoke-static {v1}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createManualGroupEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object p1

    .line 165
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/v2/AbstractViewCustomersListCoordinator;->attach(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$nQahx8lCyxAjVmB_Jcvh1Y0ZPGQ;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$nQahx8lCyxAjVmB_Jcvh1Y0ZPGQ;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 73
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$mi_15N4H1tuJNQQ0PmGzyYAJNH4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$mi_15N4H1tuJNQQ0PmGzyYAJNH4;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected getGroupToken()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupTokenForViewGroupScreen()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSupportsFilters()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$attach$1$ViewGroupMasterCoordinator()Lrx/Subscription;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$D1gQShV41mycAd9VM8XU3Tavmiw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$D1gQShV41mycAd9VM8XU3Tavmiw;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ViewGroupMasterCoordinator(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 2

    .line 75
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->isSmartGroup:Z

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->defaultActionBarTitle:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$5$ViewGroupMasterCoordinator(Lkotlin/Pair;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    const-string v0, "Menu:Create Customer"

    .line 106
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->showCreateCustomerScreen(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$10$ViewGroupMasterCoordinator()V
    .locals 2

    const-string v0, "Menu:Remove From Group"

    .line 129
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->REMOVE_FROM_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$13$ViewGroupMasterCoordinator()V
    .locals 2

    const-string v0, "Menu:Merge Customers"

    .line 142
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->MERGE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$14$ViewGroupMasterCoordinator()V
    .locals 2

    const-string v0, "Menu:Bulk Delete"

    .line 151
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->BULK_DELETE:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$3$ViewGroupMasterCoordinator()V
    .locals 1

    const-string v0, "Menu:Edit Group"

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->editGroup()V

    return-void
.end method

.method public synthetic lambda$setupDropDownActions$6$ViewGroupMasterCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 2

    .line 101
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->customerLookup:Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;

    .line 102
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupView;->searchTerm()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$__hAx8yI9zB7PaHH-g1RzCeMjSk;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$__hAx8yI9zB7PaHH-g1RzCeMjSk;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    .line 103
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Rf7mVlDeP4CDVSs8qE24uuwuruQ;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$Rf7mVlDeP4CDVSs8qE24uuwuruQ;

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$ci9Gd3i7y4o8bzffmNLwIcScQnQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$ci9Gd3i7y4o8bzffmNLwIcScQnQ;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    .line 104
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setupDropDownActions$7$ViewGroupMasterCoordinator()V
    .locals 2

    const-string v0, "Menu:Add to Group"

    .line 115
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->logAction(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    sget-object v1, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ADD_TO_ANOTHER_MANUAL_GROUP:Lcom/squareup/ui/crm/v2/MultiSelectMode;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->setMultiSelectMode(Lcom/squareup/ui/crm/v2/MultiSelectMode;)V

    return-void
.end method

.method protected setupDropDownActions(Landroid/view/View;Lrx/Observable;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 88
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_edit_group:I

    .line 89
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    .line 90
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$n-GlgS2HnJNf-VOH__THg8-jXnE;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$n-GlgS2HnJNf-VOH__THg8-jXnE;

    .line 91
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$2W9WSmKgbnwOMfRZQxymVpde3V4;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$2W9WSmKgbnwOMfRZQxymVpde3V4;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    .line 88
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 99
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_create_customer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$Xzqdpa_LMmF7B7PHIzgBgpojrlY;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$Xzqdpa_LMmF7B7PHIzgBgpojrlY;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;Landroid/view/View;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 111
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_add_to_manual_group:I

    .line 112
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$nxhSEF48IWk5ImaqBSJ6b4XT35w;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$nxhSEF48IWk5ImaqBSJ6b4XT35w;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    move-object v1, p0

    move-object v5, p2

    .line 111
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 121
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_remove_from_manual_group:I

    .line 122
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    .line 126
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$-h69LWHs6A6o7ycVA6sVNK1gXso;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$-h69LWHs6A6o7ycVA6sVNK1gXso;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$3NnsE-TaqOsWriZ1FKiL9FOnLro;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$3NnsE-TaqOsWriZ1FKiL9FOnLro;

    .line 125
    invoke-virtual {p2, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$QxkJAyAeYERIAaEOtqz3Rmizv7s;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$QxkJAyAeYERIAaEOtqz3Rmizv7s;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    const/4 v4, 0x0

    move-object v1, p0

    .line 121
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 134
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_merge_customers:I

    .line 135
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    .line 138
    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$JBg7Y6qxVDttZpkFOWsyDLF9GhQ;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$JBg7Y6qxVDttZpkFOWsyDLF9GhQ;

    .line 139
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$yz03BTy5_DR8ARwkC2NkcnDS6q8;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$yz03BTy5_DR8ARwkC2NkcnDS6q8;

    .line 137
    invoke-static {p2, v0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v5

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$5m3TNMgI6HaV4gtdX3dy8FwlZq8;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$5m3TNMgI6HaV4gtdX3dy8FwlZq8;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    const/4 v4, 0x1

    move-object v1, p0

    .line 134
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    .line 147
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_master_menu_delete_customers:I

    .line 148
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    new-instance v6, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$cDB8F8qZYfnjlnzCFb2kyDj4WG4;

    invoke-direct {v6, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$cDB8F8qZYfnjlnzCFb2kyDj4WG4;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;)V

    move-object v5, p2

    .line 147
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->bindDropDownAction(Landroid/view/View;Landroid/view/View;ZLrx/Observable;Lrx/functions/Action0;)V

    return-void
.end method

.method protected showCreateCustomerOnNoResults()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;->getGroupForViewGroupScreen()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$uAHBiaiRsCtVOWVSkHCeRbKwaQQ;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ViewGroupMasterCoordinator$uAHBiaiRsCtVOWVSkHCeRbKwaQQ;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected upButtonAction()Ljava/lang/Runnable;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator;->runner:Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$-x0Xrq6w4QzspWic17WdC2x4rpY;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$-x0Xrq6w4QzspWic17WdC2x4rpY;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupMasterCoordinator$Runner;)V

    return-object v1
.end method
