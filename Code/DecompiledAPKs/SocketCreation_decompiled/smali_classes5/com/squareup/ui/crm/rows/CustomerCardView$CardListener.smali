.class Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;
.super Ljava/lang/Object;
.source "CustomerCardView.java"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnCardListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/rows/CustomerCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/crm/rows/CustomerCardView;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;->this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/crm/rows/CustomerCardView;Lcom/squareup/ui/crm/rows/CustomerCardView$1;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;-><init>(Lcom/squareup/ui/crm/rows/CustomerCardView;)V

    return-void
.end method


# virtual methods
.method public onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
    .locals 0

    return-void
.end method

.method public onCardInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;->this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-static {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->access$300(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onCardValid(Lcom/squareup/Card;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;->this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-static {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->access$100(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onChargeCard(Lcom/squareup/Card;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;->this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-static {v0}, Lcom/squareup/ui/crm/rows/CustomerCardView;->access$100(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerCardView$CardListener;->this$0:Lcom/squareup/ui/crm/rows/CustomerCardView;

    invoke-static {p1}, Lcom/squareup/ui/crm/rows/CustomerCardView;->access$200(Lcom/squareup/ui/crm/rows/CustomerCardView;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;Z)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
