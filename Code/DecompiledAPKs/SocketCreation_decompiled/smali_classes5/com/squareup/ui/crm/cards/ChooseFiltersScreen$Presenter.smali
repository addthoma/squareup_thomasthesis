.class Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseFiltersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseFiltersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final filterHelper:Lcom/squareup/crm/filters/FilterHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    .line 80
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 81
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    .line 82
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;I)V
    .locals 1

    .line 163
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 164
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 167
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Z8Bh7Kvy5pGgNOUuI7zoDpeohao;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Z8Bh7Kvy5pGgNOUuI7zoDpeohao;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 176
    new-instance p2, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$OSwciAoslajlHSAiy4KihhDtgig;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$OSwciAoslajlHSAiy4KihhDtgig;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/account/view/SmartLineRow;I)V

    invoke-static {p1, p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method static synthetic lambda$null$0(Ljava/util/List;Ljava/util/List;)Ljava/lang/Boolean;
    .locals 0

    .line 105
    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$12(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 0

    .line 152
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$16(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V
    .locals 0

    .line 171
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 172
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method static synthetic lambda$null$18(ILkotlin/Unit;)Ljava/lang/Integer;
    .locals 0

    .line 178
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 0

    .line 125
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$8(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 1

    .line 137
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/16 v0, 0xa

    if-ge p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$bind$17$ChooseFiltersScreen$Presenter(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/account/view/SmartLineRow;)Lrx/Subscription;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/filters/FilterHelper;->displayValueOf(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;

    move-result-object p1

    .line 169
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$XHwNVXejWGIJhJ7u9C_ZSj5Z1CU;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$XHwNVXejWGIJhJ7u9C_ZSj5Z1CU;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    .line 170
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$19$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/account/view/SmartLineRow;I)Lrx/Subscription;
    .locals 1

    .line 177
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$TRZgfn4-YI64XjfMPBBa8ivlloQ;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$TRZgfn4-YI64XjfMPBBa8ivlloQ;-><init>(I)V

    .line 178
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$-lQza-hdMvW158nLqKmNooSm5aw;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$-lQza-hdMvW158nLqKmNooSm5aw;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;)V

    .line 179
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$10$ChooseFiltersScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->clearModifiedFilters()V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->commitChooseFiltersScreen()V

    return-void
.end method

.method public synthetic lambda$null$14$ChooseFiltersScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->showSaveFiltersScreen()V

    return-void
.end method

.method public synthetic lambda$null$2$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;Ljava/util/List;)V
    .locals 3

    .line 113
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->removeAllFilterRows()V

    .line 116
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 117
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v2

    invoke-direct {p0, v2, v1, v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$6$ChooseFiltersScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->showCreateFilterScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ChooseFiltersScreen$Presenter(Lcom/squareup/marin/widgets/MarinActionBar;)Lrx/Subscription;
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    .line 103
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->originalFilters()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    .line 104
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->modifiedFilters()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$8C-IcT5ZNmqWsrOiN5aq2GX_4N0;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$8C-IcT5ZNmqWsrOiN5aq2GX_4N0;

    .line 102
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 106
    invoke-static {v1}, Lcom/squareup/util/rx/RxTransformers;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$Mg33pw2ykhDYYi1B8X_NkLhFaZs;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$Mg33pw2ykhDYYi1B8X_NkLhFaZs;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 107
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$11$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->onRemoveAllClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Ry1HCQA3lhmwQtZAfmqroO2-SI0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Ry1HCQA3lhmwQtZAfmqroO2-SI0;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;)V

    .line 144
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$13$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->modifiedFilters()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$8C7VKtAdU7yZjRZRkaswY38qpDo;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$8C7VKtAdU7yZjRZRkaswY38qpDo;

    .line 152
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 153
    invoke-static {v1}, Lcom/squareup/util/rx/RxTransformers;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$Y-gGtLJHLWLIlhY7jfFDvIixAPE;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$Y-gGtLJHLWLIlhY7jfFDvIixAPE;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    .line 154
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$15$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 1

    .line 158
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->onSaveFiltersClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$3LfC4zFYdNu2pXzc5w0UroFa9xs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$3LfC4zFYdNu2pXzc5w0UroFa9xs;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;)V

    .line 159
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->modifiedFilters()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$vQnqsB2RE6V6IwmpaUsAkSrpmP4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$vQnqsB2RE6V6IwmpaUsAkSrpmP4;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    .line 112
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->modifiedFilters()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$XnFTShQtWzuAADwxjcJYQ_5yq4M;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$XnFTShQtWzuAADwxjcJYQ_5yq4M;

    .line 125
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 126
    invoke-static {v1}, Lcom/squareup/util/rx/RxTransformers;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$wNDPml52iUxyatPCWNWLi-xc6YE;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$wNDPml52iUxyatPCWNWLi-xc6YE;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    .line 127
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 1

    .line 131
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->onAddFilterClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$la2q8ofGS0Y1ZUP3cu64QF7fhOw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$la2q8ofGS0Y1ZUP3cu64QF7fhOw;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;)V

    .line 132
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$ChooseFiltersScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)Lrx/Subscription;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;->modifiedFilters()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$u4MOeSfT0EzMJzu_4aU_XNiAP24;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$u4MOeSfT0EzMJzu_4aU_XNiAP24;

    .line 137
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 138
    invoke-static {v1}, Lcom/squareup/util/rx/RxTransformers;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$9jMlrydn3bGmwoZFgOMZO4VMOg0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$9jMlrydn3bGmwoZFgOMZO4VMOg0;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    .line 139
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 86
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 90
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 94
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/applet/R$string;->crm_filter_customers_title:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$IGCTzsCoqDNB0CkxgXUVdB4hXyU;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$IGCTzsCoqDNB0CkxgXUVdB4hXyU;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crm/applet/R$string;->crm_apply_filters_label:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$sy1N4sFS2blGcIObKTKMjikfeXI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$sy1N4sFS2blGcIObKTKMjikfeXI;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 101
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$-TA8CTBqS9l9W-hAWJuZ9nF-YgM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$-TA8CTBqS9l9W-hAWJuZ9nF-YgM;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 110
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$W4NUuBL1jUUARQ0i9zucItl85ak;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$W4NUuBL1jUUARQ0i9zucItl85ak;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 123
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$I17mxrksl0LNwYytsBPutV2pkMo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$I17mxrksl0LNwYytsBPutV2pkMo;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 130
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$dKVlvkPRrqqwpGN98VtAAjjYQ6A;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$dKVlvkPRrqqwpGN98VtAAjjYQ6A;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 135
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$2QvPFTyEuq17-KnnqJpQJbKqwEM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$2QvPFTyEuq17-KnnqJpQJbKqwEM;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 142
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$J-zys1tN5J_07Qu8dzsGufelKiQ;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$J-zys1tN5J_07Qu8dzsGufelKiQ;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Qxkn4dvnvq4Yrbx7_TJAoHVhhNs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$Qxkn4dvnvq4Yrbx7_TJAoHVhhNs;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 157
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$51iZf2CcDbJqzzFkjoPq3bDnJA4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFiltersScreen$Presenter$51iZf2CcDbJqzzFkjoPq3bDnJA4;-><init>(Lcom/squareup/ui/crm/cards/ChooseFiltersScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFiltersCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
