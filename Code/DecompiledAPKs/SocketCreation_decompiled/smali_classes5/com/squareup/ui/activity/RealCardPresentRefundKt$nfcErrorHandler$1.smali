.class public final Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/RealCardPresentRefundKt;->nfcErrorHandler(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u000c*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016J\u0008\u0010\u0005\u001a\u00020\u0003H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016J\u0008\u0010\u0007\u001a\u00020\u0003H\u0016J\u0008\u0010\u0008\u001a\u00020\u0003H\u0016J\u0008\u0010\t\u001a\u00020\u0003H\u0016J\u0008\u0010\n\u001a\u00020\u0003H\u0016J\u0008\u0010\u000b\u001a\u00020\u0003H\u0016J\u0008\u0010\u000c\u001a\u00020\u0003H\u0016J\u0008\u0010\r\u001a\u00020\u0003H\u0016J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "com/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1",
        "Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;",
        "handleActionRequired",
        "",
        "handleCardBlocked",
        "handleCardDeclined",
        "handleCardTapAgain",
        "handleCollisionDetected",
        "handleInterfaceUnavailable",
        "handleLimitExceededInsertCard",
        "handleLimitExceededTryAnotherCard",
        "handleOnRequestTapCard",
        "handleProcessingError",
        "handleTryAnotherCard",
        "handleUnlockDevice",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/CardPresentRefundEventHandler;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleActionRequired()V
    .locals 3

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofActionRequired()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleCardBlocked()V
    .locals 3

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleCardDeclined()V
    .locals 3

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleCardTapAgain()V
    .locals 3

    .line 232
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofTapAgain()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleCollisionDetected()V
    .locals 3

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofNfcCollision()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleInterfaceUnavailable()V
    .locals 3

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleLimitExceededInsertCard()V
    .locals 3

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleLimitExceededTryAnotherCard()V
    .locals 3

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleOnRequestTapCard()V
    .locals 3

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofTapNow()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleProcessingError()V
    .locals 3

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleTryAnotherCard()V
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->fatalError()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public handleUnlockDevice()V
    .locals 3

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/activity/RealCardPresentRefundKt$nfcErrorHandler$1;->$handler:Lcom/squareup/ui/activity/CardPresentRefundEventHandler;

    sget-object v1, Lcom/squareup/ui/activity/ReaderResult;->Companion:Lcom/squareup/ui/activity/ReaderResult$Companion;

    sget-object v2, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->ofUnlockDevice()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/activity/ReaderResult$Companion;->errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefundEventHandler;->onReaderResult(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method
