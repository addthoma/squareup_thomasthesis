.class public final Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;
.super Ljava/lang/Object;
.source "TransactionsHistoryView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/activity/TransactionsHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final legacyPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/activity/TransactionsHistoryView;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static injectActionBarNavigationHelper(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/ActionBarNavigationHelper;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    return-void
.end method

.method public static injectAppletSelection(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/AppletSelection;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->appletSelection:Lcom/squareup/applet/AppletSelection;

    return-void
.end method

.method public static injectBadgePresenter(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/BadgePresenter;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLegacyPresenter(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/activity/TransactionsHistoryView;->legacyPresenter:Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/activity/TransactionsHistoryView;)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectBadgePresenter(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/BadgePresenter;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->legacyPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectLegacyPresenter(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/ui/activity/LegacyTransactionsHistoryPresenter;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/ActionBarNavigationHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectActionBarNavigationHelper(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/ActionBarNavigationHelper;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSelection;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectAppletSelection(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/applet/AppletSelection;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectFeatures(Lcom/squareup/ui/activity/TransactionsHistoryView;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/activity/TransactionsHistoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/TransactionsHistoryView_MembersInjector;->injectMembers(Lcom/squareup/ui/activity/TransactionsHistoryView;)V

    return-void
.end method
