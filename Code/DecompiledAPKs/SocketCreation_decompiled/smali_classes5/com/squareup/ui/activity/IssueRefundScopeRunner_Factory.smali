.class public final Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;
.super Ljava/lang/Object;
.source "IssueRefundScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/activity/IssueRefundScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final activeCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final activityAppletGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final billListServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final billRefundServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/BillRefundService;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cardPresentRefundFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/CardPresentRefundFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currentSecureTouchModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final exchangesHostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final inventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchFeature;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionsHistoryRefundHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SwipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/BillRefundService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/CardPresentRefundFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchFeature;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionsHistoryRefundHelperProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->userTokenProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->billListServiceHelperProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->billRefundServiceProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardPresentRefundFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->activityAppletGatewayProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->x2SwipeBusProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->secureTouchFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->exchangesHostProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/BillRefundService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/CardPresentRefundFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchFeature;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ExchangesHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/CurrentSecureTouchMode;",
            ">;)",
            "Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    .line 216
    new-instance v36, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;

    move-object/from16 v0, v36

    invoke-direct/range {v0 .. v35}, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v36
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;Lcom/squareup/settings/server/Features;Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/server/payment/BillRefundService;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/activity/CardPresentRefundFactory;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/activity/ActivityAppletGateway;Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/badbus/BadBus;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/securetouch/CurrentSecureTouchMode;)Lcom/squareup/ui/activity/IssueRefundScopeRunner;
    .locals 37

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    .line 236
    new-instance v36, Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-object/from16 v0, v36

    invoke-direct/range {v0 .. v35}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;Lcom/squareup/settings/server/Features;Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/server/payment/BillRefundService;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/activity/CardPresentRefundFactory;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/activity/ActivityAppletGateway;Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/badbus/BadBus;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V

    return-object v36
.end method


# virtual methods
.method public get()Lcom/squareup/ui/activity/IssueRefundScopeRunner;
    .locals 37

    move-object/from16 v0, p0

    .line 184
    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionsHistoryRefundHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->billListServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->billRefundServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/server/payment/BillRefundService;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cashDrawerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardPresentRefundFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/activity/CardPresentRefundFactory;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->activityAppletGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/activity/ActivityAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/server/inventory/InventoryService;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->activeCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/cardreader/dipper/ActiveCardReader;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->x2SwipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/giftcard/GiftCards;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->secureTouchFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/securetouch/SecureTouchFeature;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->exchangesHostProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/ui/activity/ExchangesHost;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->secureTouchWorkflowLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->currentSecureTouchModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v36, v1

    check-cast v36, Lcom/squareup/securetouch/CurrentSecureTouchMode;

    invoke-static/range {v2 .. v36}, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;Lcom/squareup/settings/server/Features;Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/server/payment/BillRefundService;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/activity/CardPresentRefundFactory;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/activity/ActivityAppletGateway;Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/badbus/BadBus;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/securetouch/CurrentSecureTouchMode;)Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner_Factory;->get()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object v0

    return-object v0
.end method
