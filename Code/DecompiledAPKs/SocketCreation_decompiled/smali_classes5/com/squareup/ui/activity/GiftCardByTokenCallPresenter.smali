.class public Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "GiftCardByTokenCallPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

.field private final giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final lastRequest:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;",
            ">;"
        }
    .end annotation
.end field

.field private progressSpinnerSubscription:Lrx/Subscription;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 40
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->lastRequest:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->flow:Lflow/Flow;

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    .line 52
    iput-object p4, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 53
    iput-object p5, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object p2, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->lastRequest:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/mortar/Popup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    if-eqz v0, :cond_0

    .line 118
    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    const/4 v0, 0x0

    .line 119
    iput-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    .line 121
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method public fetch(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;)V
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$GiftCardByTokenCallPresenter(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 83
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/GiftCard;->state:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    sget-object v1, Lcom/squareup/protos/client/giftcards/GiftCard$State;->ACTIVE:Lcom/squareup/protos/client/giftcards/GiftCard$State;

    if-ne v0, v1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    sget-object v2, Lcom/squareup/ui/activity/ActivityAppletScope;->INSTANCE:Lcom/squareup/ui/activity/ActivityAppletScope;

    invoke-interface {v1, p1, v2}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->checkBalanceFor(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->gift_card_checking_failure_not_active_title:I

    .line 88
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->gift_card_checking_failure_not_active_desc:I

    .line 89
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 90
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-static {p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$GiftCardByTokenCallPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getRetryable()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->network_error_title:I

    .line 97
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->network_error_message:I

    .line 98
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 99
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->retry:I

    .line 100
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/billhistoryui/R$string;->gift_card_checking_failure:I

    .line 103
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->server_error_message:I

    .line 104
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->dismiss:I

    .line 105
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 102
    invoke-static {p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->show(Landroid/os/Parcelable;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$0$GiftCardByTokenCallPresenter(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;)Lrx/Observable;
    .locals 5

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;->gift_card_server_token:Ljava/lang/String;

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->checkBalanceByServerToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 74
    invoke-static {p1, v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    sget v1, Lcom/squareup/billhistoryui/R$string;->gift_card_checking:I

    sget-object v2, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x320

    .line 77
    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(IJLjava/util/concurrent/TimeUnit;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$3$GiftCardByTokenCallPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$-pNz_jR0tp8XslSdQZ0msB_pDVY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$-pNz_jR0tp8XslSdQZ0msB_pDVY;-><init>(Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$BIrQKbFxQJEK0qzCBzDfJ77Uds4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$BIrQKbFxQJEK0qzCBzDfJ77Uds4;-><init>(Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$I_airZWE5YfFssnXL2-OsyyOPuM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$I_airZWE5YfFssnXL2-OsyyOPuM;-><init>(Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V

    .line 73
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$dkQWyMbTY9SkZ0Tnr3Hvwhkn1C4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$GiftCardByTokenCallPresenter$dkQWyMbTY9SkZ0Tnr3Hvwhkn1C4;-><init>(Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V

    .line 80
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 72
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 112
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->progressSpinnerSubscription:Lrx/Subscription;

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 64
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->lastRequest:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->take(I)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->loadCardRelay:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
