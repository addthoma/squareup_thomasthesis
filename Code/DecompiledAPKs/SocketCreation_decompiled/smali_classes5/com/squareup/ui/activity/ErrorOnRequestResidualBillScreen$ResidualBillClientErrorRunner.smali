.class public interface abstract Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;
.super Ljava/lang/Object;
.source "ErrorOnRequestResidualBillScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResidualBillClientErrorRunner"
.end annotation


# virtual methods
.method public abstract dismissAfterResidualBillClientError()V
.end method

.method public abstract failureMessage()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation
.end method
