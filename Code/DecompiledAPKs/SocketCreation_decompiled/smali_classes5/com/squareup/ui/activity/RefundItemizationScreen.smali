.class public final Lcom/squareup/ui/activity/RefundItemizationScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "RefundItemizationScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/buyer/PaymentExempt;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/RefundItemizationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isFirstScreen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$RefundItemizationScreen$H4Ds_3Bfc5oU3OgrQtOZdY9qcfM;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$RefundItemizationScreen$H4Ds_3Bfc5oU3OgrQtOZdY9qcfM;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/RefundItemizationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 0

    .line 33
    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    .line 34
    iput-boolean p1, p0, Lcom/squareup/ui/activity/RefundItemizationScreen;->isFirstScreen:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/RefundItemizationScreen;
    .locals 3

    .line 68
    const-class v0, Lcom/squareup/ui/activity/RefundItemizationScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 70
    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 71
    new-instance v0, Lcom/squareup/ui/activity/RefundItemizationScreen;

    invoke-direct {v0, v2, p0}, Lcom/squareup/ui/activity/RefundItemizationScreen;-><init>(ZLcom/squareup/ui/activity/IssueRefundScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 63
    iget-boolean v0, p0, Lcom/squareup/ui/activity/RefundItemizationScreen;->isFirstScreen:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundItemizationScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/ui/activity/RefundItemizationScreen;->isFirstScreen:Z

    if-eqz v0, :cond_1

    .line 47
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 2

    .line 52
    const-class v0, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/IssueRefundScope$Component;

    .line 53
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->refundItemizationsCoordinator()Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;

    move-result-object v0

    .line 54
    invoke-interface {p1}, Lcom/squareup/ui/activity/IssueRefundScope$Component;->issueRefundScopeRunner()Lcom/squareup/ui/activity/IssueRefundScopeRunner;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->screenData()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$Factory;->create(Lio/reactivex/Observable;Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;)Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 59
    sget v0, Lcom/squareup/activity/R$layout;->activity_applet_refund_itemizations_view:I

    return v0
.end method
