.class public interface abstract Lcom/squareup/ui/activity/TransactionsHistoryScreen$Component;
.super Ljava/lang/Object;
.source "TransactionsHistoryScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/TransactionsHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/activity/BulkSettleButton;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/InstantDepositRowView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/TransactionsHistoryListView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/activity/TransactionsHistoryView;)V
.end method
