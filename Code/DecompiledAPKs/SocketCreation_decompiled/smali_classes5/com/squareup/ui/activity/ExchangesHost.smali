.class public interface abstract Lcom/squareup/ui/activity/ExchangesHost;
.super Ljava/lang/Object;
.source "ExchangesHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/ExchangesHost$NoExchangesHost;
    }
.end annotation


# virtual methods
.method public abstract exchangesEnabled()Z
.end method

.method public abstract getRefundContext()Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;
.end method

.method public abstract onReturnItemsAddedToCart(Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;)V
.end method

.method public abstract reset()V
.end method
