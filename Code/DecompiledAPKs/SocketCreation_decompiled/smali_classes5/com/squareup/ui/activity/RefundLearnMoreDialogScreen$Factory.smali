.class public Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen$Factory;
.super Ljava/lang/Object;
.source "RefundLearnMoreDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/Context;Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 35
    invoke-static {p1}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 29
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;

    .line 31
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-static {v1}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object v3

    iget v3, v3, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 33
    invoke-static {v1}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object v3

    iget v3, v3, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 34
    invoke-static {v1}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object v3

    iget v3, v3, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/squareup/ui/activity/-$$Lambda$RefundLearnMoreDialogScreen$Factory$4sY_Q8YCC8VI8mA2Qd8uD4Mvs4I;

    invoke-direct {v4, p1, v1}, Lcom/squareup/ui/activity/-$$Lambda$RefundLearnMoreDialogScreen$Factory$4sY_Q8YCC8VI8mA2Qd8uD4Mvs4I;-><init>(Landroid/content/Context;Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)V

    invoke-virtual {v2, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 37
    invoke-static {v1}, Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;->access$000(Lcom/squareup/ui/activity/RefundLearnMoreDialogScreen;)Lcom/squareup/register/widgets/LearnMoreMessages;

    move-result-object v1

    iget v1, v1, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 38
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 31
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
