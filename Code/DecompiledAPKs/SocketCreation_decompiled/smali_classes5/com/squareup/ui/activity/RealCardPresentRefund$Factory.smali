.class public final Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;
.super Ljava/lang/Object;
.source "RealCardPresentRefund.kt"

# interfaces
.implements Lcom/squareup/ui/activity/CardPresentRefundFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/RealCardPresentRefund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;",
        "Lcom/squareup/ui/activity/CardPresentRefundFactory;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/CardReaderListeners;",
        "(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;)V",
        "cardPresentRefund",
        "Lcom/squareup/ui/activity/RealCardPresentRefund;",
        "scope",
        "Lmortar/MortarScope;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "nfcProcessor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderListeners"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object p2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-void
.end method


# virtual methods
.method public bridge synthetic cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/CardPresentRefund;
    .locals 0

    .line 90
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;->cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/RealCardPresentRefund;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/activity/CardPresentRefund;

    return-object p1
.end method

.method public cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/RealCardPresentRefund;
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/ui/activity/RealCardPresentRefund;

    iget-object v1, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iget-object v2, p0, Lcom/squareup/ui/activity/RealCardPresentRefund$Factory;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/ui/activity/RealCardPresentRefund;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/CardReaderListeners;Lmortar/MortarScope;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
