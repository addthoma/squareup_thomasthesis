.class public Lcom/squareup/ui/activity/IssueRefundScopeRunner;
.super Ljava/lang/Object;
.source "IssueRefundScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen$ResidualBillClientErrorRunner;
.implements Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;
.implements Lcom/squareup/activity/refund/IssueRefundCoordinator$IssueRefundEventHandler;
.implements Lcom/squareup/activity/refund/RefundErrorCoordinator$RefundErrorEventRunner;
.implements Lcom/squareup/activity/refund/RefundCardPresenceCoordinator$RefundCardPresenceEventHandler;
.implements Lcom/squareup/activity/refund/RefundDoneCoordinator$RefundDoneHandler;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/activity/refund/RestockOnItemizedRefundCoordinator$EventHandler;
.implements Lcom/squareup/cardreader/PinRequestListener;
.implements Lcom/squareup/securetouch/SecureTouchWorkflowResultRelay;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/IssueRefundScopeRunner$ActiveCardReaderNullException;
    }
.end annotation


# instance fields
.field private final EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final billListServiceHelper:Lcom/squareup/server/bills/BillListServiceHelper;

.field private final billRefundService:Lcom/squareup/server/payment/BillRefundService;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

.field private final cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private cardPresentRefund:Lcom/squareup/ui/activity/CardPresentRefund;

.field private final cardPresentRefundFactory:Lcom/squareup/ui/activity/CardPresentRefundFactory;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private emvListener:Lcom/squareup/cardreader/EmvListener;

.field private final exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

.field private final failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final inventoryService:Lcom/squareup/server/inventory/InventoryService;

.field private issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

.field private lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

.field private readerCapabilities:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation
.end field

.field private readerGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

.field private final secureTouchResultRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation
.end field

.field private final secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

.field private final swipeBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

.field private final userToken:Ljava/lang/String;

.field private final x2SwipeBus:Lcom/squareup/badbus/BadBus;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;Lcom/squareup/settings/server/Features;Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/server/payment/BillRefundService;Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/activity/CardPresentRefundFactory;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/activity/ActivityAppletGateway;Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/badbus/BadBus;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/securetouch/SecureTouchFeature;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/activity/ExchangesHost;Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/securetouch/CurrentSecureTouchMode;)V
    .locals 2
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    .line 182
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 183
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 184
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 185
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 186
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 220
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 221
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 226
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    .line 227
    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->UNKNOWN:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 228
    const-class v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    .line 229
    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerCapabilities:Ljava/util/EnumSet;

    .line 230
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 1071
    new-instance v1, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner$2;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    .line 1097
    new-instance v1, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner$3;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvListener:Lcom/squareup/cardreader/EmvListener;

    move-object v1, p1

    .line 249
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    move-object v1, p2

    .line 250
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p3

    .line 251
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, p4

    .line 252
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    move-object v1, p5

    .line 253
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p6

    .line 254
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->userToken:Ljava/lang/String;

    move-object v1, p7

    .line 255
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p8

    .line 256
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billListServiceHelper:Lcom/squareup/server/bills/BillListServiceHelper;

    move-object v1, p9

    .line 257
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billRefundService:Lcom/squareup/server/payment/BillRefundService;

    move-object v1, p10

    .line 258
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p11

    .line 259
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-object v1, p12

    .line 260
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p13

    .line 261
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-object/from16 v1, p14

    .line 262
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-object/from16 v1, p15

    .line 263
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentRefundFactory:Lcom/squareup/ui/activity/CardPresentRefundFactory;

    move-object/from16 v1, p16

    .line 264
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-object/from16 v1, p17

    .line 265
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    move-object/from16 v1, p18

    .line 266
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-object/from16 v1, p19

    .line 267
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    move-object/from16 v1, p20

    .line 268
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

    move-object/from16 v1, p21

    .line 269
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    move-object/from16 v1, p22

    .line 270
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move-object/from16 v1, p23

    .line 271
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object/from16 v1, p24

    .line 272
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object/from16 v1, p25

    .line 273
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v1, p26

    .line 274
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    move-object/from16 v1, p27

    .line 275
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->x2SwipeBus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v1, p28

    .line 276
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    move-object/from16 v1, p29

    .line 277
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p30

    .line 278
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    move-object/from16 v1, p31

    .line 279
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    move-object/from16 v1, p32

    .line 280
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p33

    .line 281
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    move-object/from16 v1, p34

    .line 282
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    move-object/from16 v1, p35

    .line 283
    iput-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 172
    iget-object p0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V
    .locals 0

    .line 172
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCardPresentCardDataThenRequestRefund()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 172
    iget-object p0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/ui/activity/ReaderResult;)V
    .locals 0

    .line 172
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderResultReceived(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method private addExchangesOrIssueRefund()V
    .locals 7

    .line 1014
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/activity/refund/RefundData;

    .line 1017
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    invoke-interface {v0}, Lcom/squareup/ui/activity/ExchangesHost;->exchangesEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    if-ne v0, v1, :cond_0

    .line 1018
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget-object v3, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    iget-object v4, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 1020
    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    .line 1019
    invoke-virtual {v2, v1, v3, v4}, Lcom/squareup/activity/refund/RefundData;->createCartReturnData(Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;

    move-result-object v1

    .line 1018
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->setReturnCart(Lcom/squareup/checkout/ReturnCart;)V

    .line 1021
    new-instance v0, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 1023
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/billhistory/model/BillHistory;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v4, v1, Lcom/squareup/ui/activity/IssueRefundScope;->authorizedEmployeeToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-boolean v5, v1, Lcom/squareup/ui/activity/IssueRefundScope;->skipsRestock:Z

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v6, v1, Lcom/squareup/ui/activity/IssueRefundScope;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;-><init>(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V

    .line 1028
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    invoke-interface {v1, v0}, Lcom/squareup/ui/activity/ExchangesHost;->onReturnItemsAddedToCart(Lcom/squareup/ui/activity/IssueRefundScope$RefundContext;)V

    goto :goto_0

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueRefundScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/IssueRefundScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private cardReaderResultReceived(Lcom/squareup/ui/activity/ReaderResult;)V
    .locals 4

    .line 723
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    .line 724
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getMessageResources()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    .line 729
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getMessageResources()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-eq v2, v3, :cond_0

    .line 730
    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 731
    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    invoke-virtual {v3}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copyForScreenComplete()Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 734
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getSuccess()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 735
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    .line 736
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getAuthorizationBytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithAuthorizationData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 735
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 738
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCardPresentCardDataThenRequestRefund()V

    goto :goto_0

    .line 743
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v2, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 745
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getMessageResources()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->getErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->RETRY:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-ne v2, v3, :cond_2

    .line 746
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 747
    invoke-virtual {p1}, Lcom/squareup/ui/activity/ReaderResult;->getMessageResources()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, p1, v2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copyForRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object p1

    .line 746
    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 748
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showNextCardPresenceRequiredScreen()V

    .line 749
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->subscribeToCardPresentRefundResult()V

    goto :goto_0

    .line 751
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copyForFatalError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 752
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showNextCardPresenceRequiredScreen()V

    :goto_0
    return-void
.end method

.method private emitRefundDataWithCashDrawerShift(Lcom/squareup/activity/refund/RefundData;)V
    .locals 2

    .line 659
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->cashManagementEnabledAndIsOpenShift()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    .line 660
    invoke-interface {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/activity/refund/RefundData;->copyWithCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 659
    :cond_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private errorInContactlessWorkflow(Ljava/lang/Throwable;)V
    .locals 1

    .line 878
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private exitFromItemizedRefundFlow()V
    .locals 4

    .line 950
    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->UNKNOWN:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 951
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundDoneEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundDoneEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 952
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/InIssueRefundScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private getCardPresentCardDataThenRequestRefund()V
    .locals 4

    .line 669
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->hasTenderRequiringCardAuthorization()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 670
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->isCardInsertedOnAnyContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPayment()V

    .line 682
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->awaitingCardRemoval(Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 683
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 698
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 700
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->hasTenderRequiringCardAuthorization()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerCapabilities:Ljava/util/EnumSet;

    sget-object v1, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RefundNoCardReaderScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/RefundNoCardReaderScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 705
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    .line 706
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v2, v3}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->fromRefundData(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 707
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showNextCardPresenceRequiredScreen()V

    .line 708
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->subscribeToCardPresentRefundResult()V

    goto :goto_0

    .line 710
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->requestRefund()V

    :goto_0
    return-void
.end method

.method private getCreatorDetailsForRefund()Lcom/squareup/protos/client/CreatorDetails;
    .locals 3

    .line 909
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/activity/refund/CreatorDetailsHelper;->forEmployeeToken(Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;)Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    return-object v0
.end method

.method private static ignoresDips()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
    .locals 1

    .line 976
    new-instance v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$1;

    invoke-direct {v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner$1;-><init>()V

    return-object v0
.end method

.method private isInCardPresentRefund()Z
    .locals 1

    .line 610
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static synthetic lambda$0EdGj-b9i3yCC_7ydLvliqnzkH8(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onSuccessfulRefund(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V

    return-void
.end method

.method public static synthetic lambda$CM6EIRdlP_WsE47VsfSscEEPdW8(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/ui/activity/ReaderResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderResultReceived(Lcom/squareup/ui/activity/ReaderResult;)V

    return-void
.end method

.method public static synthetic lambda$DYteP2X-6gpJDw7NqZ1ZZgJgp1A(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->errorInContactlessWorkflow(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic lambda$jTmW3xN0CgofZEVAMCsIxGoRV8M(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onSecureTouchResult(Lcom/squareup/securetouch/SecureTouchResult;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefundScopeRunner: secure touch event for reader: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$2(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 510
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefundScopeRunner: secure touch event for app: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$3(Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefundScopeRunner: isSecureTouchEnabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$7(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Lcom/squareup/Card;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 537
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    return-object p0
.end method

.method static synthetic lambda$onRefundError$17(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 837
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/IssueRefundsResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method static synthetic lambda$onResidualBillError$16(Lcom/squareup/protos/client/bills/GetResidualBillResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 0

    .line 827
    new-instance p0, Lcom/squareup/receiving/FailureMessage$Parts;

    invoke-direct {p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>()V

    return-object p0
.end method

.method public static synthetic lambda$vAuYcFevRqO82sfvEtFuT6eMn6U(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method public static synthetic lambda$xKJ2DOdlBd3cZ37YFHlFcbOF8Uw(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onResidualBillError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method private logRestockOnItemizedRefundActions(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V
    .locals 4

    .line 988
    iget-object p1, p1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;->adjustments:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/InventoryAdjustment;

    .line 989
    iget-object v1, v0, Lcom/squareup/protos/client/InventoryAdjustment;->variation_token:Ljava/lang/String;

    .line 990
    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 991
    invoke-virtual {v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v3}, Lcom/squareup/activity/refund/RefundData;->getItemTokensByVariationTokens()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 990
    invoke-static {v0, v1}, Lcom/squareup/log/inventory/InventoryStockActionEvent;->restockOnItemizedRefund(Lcom/squareup/protos/client/InventoryAdjustment;Ljava/lang/String;)Lcom/squareup/log/inventory/InventoryStockActionEvent;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private onRefundDone()V
    .locals 3

    .line 859
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueRefundDoneScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/IssueRefundDoneScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onRefundError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/bills/IssueRefundsResponse;",
            ">;)V"
        }
    .end annotation

    .line 835
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/activity/R$string;->refund_failed:I

    sget-object v2, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$PwVw9HylydRmzlDZ664H03N84i8;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$PwVw9HylydRmzlDZ664H03N84i8;

    .line 836
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 838
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/activity/refund/RefundEvent;->refundErrorEvent(Ljava/lang/String;)Lcom/squareup/activity/refund/RefundErrorEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 839
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showErrorForRequestRefundFailure(Lcom/squareup/receiving/FailureMessage;)V

    return-void
.end method

.method private onResidualBillError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            ">;)V"
        }
    .end annotation

    .line 825
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/activity/R$string;->refund_failed:I

    sget-object v2, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$ZqajvkLwgmhV_hIQb0JCW_C-xN8;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$ZqajvkLwgmhV_hIQb0JCW_C-xN8;

    .line 826
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 828
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/activity/refund/RefundEvent;->refundErrorEvent(Ljava/lang/String;)Lcom/squareup/activity/refund/RefundErrorEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 830
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 831
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/ErrorOnRequestResidualBillScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method private onRestockRetryableError()V
    .locals 4

    .line 843
    new-instance v0, Lcom/squareup/receiving/FailureMessage;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/activity/R$string;->failed_restock_attempt:I

    .line 844
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/activity/R$string;->restock_recoverable_failure_message:I

    .line 845
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 847
    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showRetryableError(Lcom/squareup/receiving/FailureMessage;)V

    return-void
.end method

.method private onSecureTouchResult(Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 3

    .line 935
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private onSuccessfulRefund(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V
    .locals 4

    .line 789
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    .line 790
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/activity/refund/RefundEvent;->issueRefundEvent(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 791
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 792
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 793
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exchangesHost:Lcom/squareup/ui/activity/ExchangesHost;

    invoke-interface {v1}, Lcom/squareup/ui/activity/ExchangesHost;->reset()V

    .line 797
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 799
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 800
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/BillHistory;

    .line 799
    invoke-virtual {v1, v2, p1}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;->ingestRefundsResponse(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    .line 804
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 805
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isSingleTender()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 806
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v2

    sget-object v3, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eq v2, v3, :cond_2

    .line 807
    :cond_1
    invoke-virtual {p0, v1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->performCashDrawerFunctions(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 811
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getRestockIndices()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 812
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundDone()V

    return-void

    .line 817
    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->userToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/IssueRefundsResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 819
    invoke-static {v2, p1, v0}, Lcom/squareup/activity/refund/IssueRefundsRequests;->createRestockRequest(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object p1

    .line 818
    invoke-virtual {v0, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithRestockRequest(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 817
    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 821
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->performRestockOnItemizedRefund()V

    return-void
.end method

.method private performRestockOnItemizedRefund()V
    .locals 4

    .line 956
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-boolean v0, v0, Lcom/squareup/ui/activity/IssueRefundScope;->skipsRestock:Z

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Cannot perform restock on itemized refund when the refund scope says to skip restock."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 959
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 960
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getRestockRequest()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-result-object v0

    const-string v1, "Restock request must have been created before performing restock actions."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    .line 962
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->inventoryService:Lcom/squareup/server/inventory/InventoryService;

    invoke-interface {v2, v0}, Lcom/squareup/server/inventory/InventoryService;->batchAdjust(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v2

    .line 963
    invoke-virtual {v2}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$7Xf1pXjYimUpHT0xFenDQke9-_U;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$7Xf1pXjYimUpHT0xFenDQke9-_U;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V

    .line 964
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 962
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private requestRefund()V
    .locals 6

    .line 761
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    .line 762
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/activity/refund/RefundSelectedTendersEvent;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getTenderDetails()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/activity/refund/RefundSelectedTendersEvent;-><init>(I)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 764
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-ltz v5, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 765
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestRefund refund amount is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "but should not be negative"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 768
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v2, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v2, v3}, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 770
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/activity/refund/RefundProcessingEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundProcessingEvent;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 773
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 774
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->userToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCreatorDetailsForRefund()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 775
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    .line 774
    invoke-static {v1, v2, v0, v3}, Lcom/squareup/activity/refund/IssueRefundsRequests;->createExchangeRequest(Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/activity/refund/RefundData;Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object v0

    goto :goto_1

    .line 777
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->userToken:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCreatorDetailsForRefund()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/squareup/activity/refund/IssueRefundsRequests;->createIssueRefundRequest(Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/protos/client/bills/IssueRefundsRequest;

    move-result-object v0

    .line 780
    :goto_1
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v1, v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logIssueRefundsRequest(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V

    .line 782
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billRefundService:Lcom/squareup/server/payment/BillRefundService;

    invoke-interface {v2, v0}, Lcom/squareup/server/payment/BillRefundService;->issueRefunds(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 783
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$v3daUHfuI8ZsMLLHfc0SSNeQ18E;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$v3daUHfuI8ZsMLLHfc0SSNeQ18E;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 784
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 782
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private requestResidualBill(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 615
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billListServiceHelper:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 616
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getSourceBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/bills/BillListServiceHelper;->getResidualBill(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$LK6JG3ssgywbgtl-wuj-0g2yOKA;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$LK6JG3ssgywbgtl-wuj-0g2yOKA;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V

    .line 617
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method private returnToIssueRefundScreen()V
    .locals 3

    .line 945
    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->UNKNOWN:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 946
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueRefundScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/IssueRefundScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private setInvalidGiftCardForRefund()V
    .locals 3

    .line 996
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-virtual {v1, v2}, Lcom/squareup/activity/refund/RefundData;->copyWithInvalidGiftCard(Lcom/squareup/Card;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private shouldShowRefundToGiftCard()Z
    .locals 2

    .line 1188
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private showErrorForRequestRefundFailure(Lcom/squareup/receiving/FailureMessage;)V
    .locals 1

    .line 851
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showRetryableError(Lcom/squareup/receiving/FailureMessage;)V

    goto :goto_0

    .line 854
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showFatalError(Lcom/squareup/receiving/FailureMessage;)V

    :goto_0
    return-void
.end method

.method private showFatalError(Lcom/squareup/receiving/FailureMessage;)V
    .locals 2

    .line 926
    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->FATAL:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 928
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 929
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 930
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/RefundFailureScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/RefundFailureScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private showNextCardPresenceRequiredScreen()V
    .locals 3

    .line 882
    sget-object v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$4;->$SwitchMap$com$squareup$ui$activity$CardPresentRefundErrorState:[I

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->getCardPresentRefundErrorState()Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 890
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copyForScreenComplete()Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 891
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->toFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 892
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RefundFailureScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/RefundFailureScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 895
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown error state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 887
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 884
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/RefundCardPresenceScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/RefundCardPresenceScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private showRetryableError(Lcom/squareup/receiving/FailureMessage;)V
    .locals 2

    .line 873
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 874
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/RefundFailureScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/RefundFailureScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;Z)V
    .locals 7

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1203
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;ZZLjava/util/Map;)V

    return-void
.end method

.method public static startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;ZZLjava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lflow/Flow;",
            "ZZ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;)V"
        }
    .end annotation

    .line 1225
    new-instance v6, Lcom/squareup/ui/activity/IssueRefundScope;

    move-object v0, v6

    move-object v1, p2

    move-object v2, p0

    move-object v3, p1

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/activity/IssueRefundScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ZLjava/util/Map;)V

    .line 1228
    new-instance p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;

    invoke-direct {p0, v6, p4}, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;Z)V

    invoke-virtual {p3, p0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private subscribeToCardPresentRefundResult()V
    .locals 3

    .line 863
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentRefund:Lcom/squareup/ui/activity/CardPresentRefund;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 864
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundData;->getFirstTenderRequiringCardAuthorization()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 863
    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/CardPresentRefund;->requestContactlessRefund(Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$CM6EIRdlP_WsE47VsfSscEEPdW8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$CM6EIRdlP_WsE47VsfSscEEPdW8;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$DYteP2X-6gpJDw7NqZ1ZZgJgp1A;

    invoke-direct {v2, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$DYteP2X-6gpJDw7NqZ1ZZgJgp1A;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 865
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    .line 869
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method public cancelAfterError()V
    .locals 3

    .line 407
    sget-object v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$4;->$SwitchMap$com$squareup$activity$refund$RefundErrorCoordinator$ItemizedRefundStage:[I

    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 414
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundDone()V

    goto :goto_0

    .line 417
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no such stage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " in ItemizedRefundStage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundCancelAfterErrorEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundCancelAfterErrorEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 410
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;->canceledRefundFlow()V

    .line 411
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exitFromItemizedRefundFlow()V

    :goto_0
    return-void
.end method

.method public cancelCardPresenceScreen()V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentAuthSubscription:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 394
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundData;->copyClearingAuthorizationData()Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 395
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->returnToIssueRefundScreen()V

    return-void
.end method

.method cardPresentData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/CardPresentRefundScreenData;",
            ">;"
        }
    .end annotation

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 294
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 295
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public clearCard(Z)V
    .locals 2

    .line 1179
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1180
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundToGiftCard(ZLcom/squareup/Card;Lokio/ByteString;)V

    return-void
.end method

.method public dismissAfterClientError()V
    .locals 3

    .line 423
    sget-object v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$4;->$SwitchMap$com$squareup$activity$refund$RefundErrorCoordinator$ItemizedRefundStage:[I

    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 429
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundDone()V

    goto :goto_0

    .line 432
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no such stage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " in ItemizedRefundStage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundCancelAfterErrorEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundCancelAfterErrorEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 426
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueRefundScreen;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/IssueRefundScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public dismissAfterResidualBillClientError()V
    .locals 4

    .line 311
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;->canceledRefundFlow()V

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/InIssueRefundScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public failureMessage()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->failureMessageData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 300
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 301
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public finishWithResult(Lcom/squareup/securetouch/SecureTouchResult;)V
    .locals 1

    .line 1184
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method getPinListener()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 1

    .line 1068
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->pinListener:Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-object v0
.end method

.method public hasCard()Z
    .locals 2

    .line 1175
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;
    .locals 1

    .line 453
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->isProcessingRestock()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->REQUESTING_RESTOCK:Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->REQUESTING_REFUND:Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    :goto_0
    return-object v0
.end method

.method public synthetic lambda$null$12$IssueRefundScopeRunner(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/protos/client/bills/GetResidualBillResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 619
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 618
    invoke-virtual {p0, p1, p3, p2, v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->showRefundItemizationScreen(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$18$IssueRefundScopeRunner(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Lcom/squareup/protos/client/BatchAdjustVariationInventoryResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 968
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->logRestockOnItemizedRefundActions(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V

    .line 969
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundDone()V

    return-void
.end method

.method public synthetic lambda$null$19$IssueRefundScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 971
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRestockRetryableError()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$IssueRefundScopeRunner(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 500
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 503
    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReader;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    goto :goto_0

    .line 505
    :cond_0
    new-instance v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$ActiveCardReaderNullException;

    invoke-direct {v0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner$ActiveCardReaderNullException;-><init>(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$10$IssueRefundScopeRunner(Lcom/squareup/Card;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 550
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$onEnterScope$11$IssueRefundScopeRunner(Lcom/squareup/Card;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 552
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->activitySearchCardRead()Z

    .line 556
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 557
    invoke-virtual {p1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    .line 558
    invoke-virtual {p0, v1, p1, v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundToGiftCard(ZLcom/squareup/Card;Lokio/ByteString;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 560
    invoke-virtual {p0, v1, p1, v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundToGiftCard(ZLcom/squareup/Card;Lokio/ByteString;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$4$IssueRefundScopeRunner(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 517
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 518
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchEnabled()V

    goto :goto_0

    .line 520
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {p1}, Lcom/squareup/securetouch/SecureTouchFeature;->onSecureTouchDisabled()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$5$IssueRefundScopeRunner(Ljava/util/EnumSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 527
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerCapabilities:Ljava/util/EnumSet;

    return-void
.end method

.method public synthetic lambda$onEnterScope$6$IssueRefundScopeRunner(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 531
    iput-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-void
.end method

.method public synthetic lambda$onEnterScope$8$IssueRefundScopeRunner(Lcom/squareup/Card;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 539
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/RefundData;->isSingleTender()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$9$IssueRefundScopeRunner(Lcom/squareup/Card;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 541
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->setInvalidGiftCardForRefund()V

    goto :goto_0

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$performRestockOnItemizedRefund$20$IssueRefundScopeRunner(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 965
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$kO4LpibCs25ag0FxTS9W9P_lj14;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$kO4LpibCs25ag0FxTS9W9P_lj14;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)V

    new-instance p1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$kzvmSFvmHYtbDQrxhcmHyiDyhKM;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$kzvmSFvmHYtbDQrxhcmHyiDyhKM;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$requestRefund$15$IssueRefundScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 784
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$0EdGj-b9i3yCC_7ydLvliqnzkH8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$0EdGj-b9i3yCC_7ydLvliqnzkH8;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$vAuYcFevRqO82sfvEtFuT6eMn6U;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$vAuYcFevRqO82sfvEtFuT6eMn6U;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$requestResidualBill$13$IssueRefundScopeRunner(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 617
    new-instance v0, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$n9Q1s1c2XvZWbgDSbE9Sg1LEbEA;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$xKJ2DOdlBd3cZ37YFHlFcbOF8Uw;

    invoke-direct {p1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$xKJ2DOdlBd3cZ37YFHlFcbOF8Uw;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    invoke-virtual {p3, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$showRefundItemizationScreen$14$IssueRefundScopeRunner(Lcom/squareup/activity/refund/RefundData;)V
    .locals 2

    .line 648
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emitRefundDataWithCashDrawerShift(Lcom/squareup/activity/refund/RefundData;)V

    .line 649
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/activity/refund/RefundChooseRefundEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundChooseRefundEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 650
    new-instance p1, Lcom/squareup/ui/activity/RefundItemizationScreen;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    const/4 v1, 0x0

    invoke-direct {p1, v1, v0}, Lcom/squareup/ui/activity/RefundItemizationScreen;-><init>(ZLcom/squareup/ui/activity/IssueRefundScope;)V

    .line 652
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, p1, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public maybeShowDeprecatingInventoryApiScreen()Z
    .locals 2

    .line 1036
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->isInventoryApiDisallowed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/ActivityAppletGateway;->goToDisallowInventoryApiDialog(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .line 322
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->shouldShowRefundToGiftCard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onCancelRefundPressed()V
    .locals 4

    .line 316
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundCancelEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundCancelEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;->canceledRefundFlow()V

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/activity/InIssueRefundScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public onDonePressed()V
    .locals 1

    .line 939
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->transactionsHistoryRefundHelper:Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;->finishedRefundFlow()V

    .line 940
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->exitFromItemizedRefundFlow()V

    const/4 v0, 0x0

    .line 941
    invoke-virtual {p0, v0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->clearCard(Z)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 457
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueRefundScope;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    .line 459
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v0, v0, Lcom/squareup/ui/activity/IssueRefundScope;->bill:Lcom/squareup/billhistory/model/BillHistory;

    .line 460
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->billHistory:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 461
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v1, v1, Lcom/squareup/ui/activity/IssueRefundScope;->inProgressRefundData:Lcom/squareup/activity/refund/RefundData;

    if-eqz v1, :cond_0

    .line 463
    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundData;->isExchange()Z

    move-result v0

    const-string v2, "Refund has not been prepared for exchange."

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 466
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v1, v1, Lcom/squareup/ui/activity/IssueRefundScope;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->requestResidualBill(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 483
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-static {}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->ignoresDips()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 488
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 490
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentRefundFactory:Lcom/squareup/ui/activity/CardPresentRefundFactory;

    invoke-interface {v0, p1}, Lcom/squareup/ui/activity/CardPresentRefundFactory;->cardPresentRefund(Lmortar/MortarScope;)Lcom/squareup/ui/activity/CardPresentRefund;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentRefund:Lcom/squareup/ui/activity/CardPresentRefund;

    .line 491
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 492
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvListener:Lcom/squareup/cardreader/EmvListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setEmvListener(Lcom/squareup/cardreader/EmvListener;)V

    .line 493
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->currentSecureTouchMode:Lcom/squareup/securetouch/CurrentSecureTouchMode;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 495
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchResultRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$jTmW3xN0CgofZEVAMCsIxGoRV8M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$jTmW3xN0CgofZEVAMCsIxGoRV8M;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 497
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-interface {v0}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsForReader()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$FMEDhG8KDE-I0Q8qaQN4BNEq2M4;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$FMEDhG8KDE-I0Q8qaQN4BNEq2M4;

    .line 498
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$yzw2TZSzicQWsSs4M00VZMcWEkU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$yzw2TZSzicQWsSs4M00VZMcWEkU;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 499
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 497
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 509
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->secureTouchEvents()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$IeV8ZOPRj8SC6CYc75071v63BSU;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$IeV8ZOPRj8SC6CYc75071v63BSU;

    .line 510
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchFeature:Lcom/squareup/securetouch/SecureTouchFeature;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/activity/-$$Lambda$LyRV8dNknUbVYrBPWdGMOe5dU4Q;

    invoke-direct {v2, v1}, Lcom/squareup/ui/activity/-$$Lambda$LyRV8dNknUbVYrBPWdGMOe5dU4Q;-><init>(Lcom/squareup/securetouch/SecureTouchFeature;)V

    .line 511
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 509
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 513
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->isSecureTouchEnabled()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$wyQMhrrfhFdxvIzGPS_OE4NJIHI;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$wyQMhrrfhFdxvIzGPS_OE4NJIHI;

    .line 514
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$3C-_oqY4n3gFe3dEM1-wh3faZZs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$3C-_oqY4n3gFe3dEM1-wh3faZZs;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 516
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 513
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 524
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 525
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->asCapabilities(Lcom/squareup/settings/server/Features;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 526
    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$O7LdD9vkP4RGgYZjMcHRlQwkNhw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$O7LdD9vkP4RGgYZjMcHRlQwkNhw;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 527
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 526
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 530
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHubUtils;->glyphForAttachedReaders()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$XVP5Cn7frd_MAPHaywRxObKjNNQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$XVP5Cn7frd_MAPHaywRxObKjNNQ;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 531
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 530
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 535
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 537
    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$5RJ8NwvE0lMY5TyqUK2U6OzjjP8;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$5RJ8NwvE0lMY5TyqUK2U6OzjjP8;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->x2SwipeBus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/x2/X2SwipedGiftCard;

    .line 538
    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/activity/-$$Lambda$Lyt8gnZzm4Ub_ultck2lQcBWJZw;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$Lyt8gnZzm4Ub_ultck2lQcBWJZw;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 537
    invoke-static {v0, v1}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$1yzFMf5RhpwuK-u2MfkFKCKOb6s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$1yzFMf5RhpwuK-u2MfkFKCKOb6s;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 539
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$leWC7CA94iPxqg6ome1QCjU0Uag;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$leWC7CA94iPxqg6ome1QCjU0Uag;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 540
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 536
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 549
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$1XRtXSKzhbmQTpINI53AqA0irXE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$1XRtXSKzhbmQTpINI53AqA0irXE;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 550
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$zYSn61LhmbUE9QZpu0pzlR0y6wY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$zYSn61LhmbUE9QZpu0pzlR0y6wY;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 551
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 549
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    :cond_1
    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 568
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getEmvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->emvListener:Lcom/squareup/cardreader/EmvListener;

    if-ne v0, v1, :cond_0

    .line 569
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetEmvListener()V

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPinRequestListener()V

    :cond_1
    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 1

    .line 1048
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1052
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->secureTouchWorkflowLauncher:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->launch(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V

    return-void
.end method

.method public onItemSelectionForRestockChanged(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1000
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithSelectedItemForRestockIndices(Ljava/util/List;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onItemsSelected(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithSelectedItemIndices(Ljava/util/List;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onNext(Lcom/squareup/Card;)V
    .locals 2

    .line 1155
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isSquareGiftCard(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 1158
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->setInvalidGiftCardForRefund()V

    :goto_0
    return-void
.end method

.method public onNextPressed()V
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-boolean v0, v0, Lcom/squareup/ui/activity/IssueRefundScope;->skipsRestock:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 330
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getIndicesOfRestockableSelectedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-interface {v0, v1}, Lcom/squareup/ui/activity/ActivityAppletGateway;->goToRestockOnItemizedRefundScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    goto :goto_0

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 334
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->addExchangesOrIssueRefund()V

    :goto_0
    return-void
.end method

.method public onNextPressedOnRestockOnItemizedRefundScreen()V
    .locals 2

    .line 1009
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1010
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->addExchangesOrIssueRefund()V

    return-void
.end method

.method public onPanInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 1171
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;)V
    .locals 2

    .line 1163
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/giftcard/GiftCards;->isSquareGiftCard(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 1166
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->setInvalidGiftCardForRefund()V

    :goto_0
    return-void
.end method

.method public onRefundAmountChanged(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onRefundModeSelected(Lcom/squareup/activity/refund/RefundMode;)V
    .locals 2

    .line 339
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/activity/refund/RefundEvent;->refundModeEvent(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithRefundMode(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onRefundPressed()V
    .locals 3

    .line 399
    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->NONE:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    iput-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getRefundMode()Lcom/squareup/activity/refund/RefundMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v0, v1}, Lcom/squareup/activity/refund/RefundMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v2}, Lcom/squareup/activity/refund/RefundData;->getItemNames()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/log/itemizedrefund/ItemizedRefundActionEvent;-><init>([Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 403
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCardPresentCardDataThenRequestRefund()V

    return-void
.end method

.method public onRefundReasonSelected(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)V
    .locals 8

    .line 371
    sget-object v0, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-ne p2, v0, :cond_0

    .line 373
    new-instance p1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    sget v2, Lcom/squareup/flowlegacy/R$layout;->editor_dialog:I

    sget v3, Lcom/squareup/flowlegacy/R$id;->editor:I

    const/16 v4, 0x2000

    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/activity/R$string;->refund_reason_other_title:I

    .line 376
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 377
    invoke-virtual {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {p2}, Lcom/squareup/activity/refund/RefundData;->getOtherReason()Ljava/lang/String;

    move-result-object v6

    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/activity/R$string;->refund_reason_other_hint:I

    .line 378
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/register/widgets/EditTextDialogFactory;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/EditOtherReasonScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/activity/EditOtherReasonScreen;-><init>(Lcom/squareup/register/widgets/EditTextDialogFactory;Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/activity/refund/RefundReasonEvent;

    invoke-direct {v1, p1}, Lcom/squareup/activity/refund/RefundReasonEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 382
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1, p2}, Lcom/squareup/activity/refund/RefundData;->copyWithRefundReason(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onRefundToGiftCard(ZLcom/squareup/Card;Lokio/ByteString;)V
    .locals 2

    if-eqz p3, :cond_0

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    .line 359
    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/activity/refund/RefundData;->copyWithIsGiftCardAndSwipe(ZLcom/squareup/Card;Lokio/ByteString;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 358
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 360
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-virtual {p3, p2}, Lcom/squareup/Card;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 361
    iget-object p3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 362
    invoke-virtual {p3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/activity/refund/RefundData;->copyWithGiftCard(ZLcom/squareup/Card;Z)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 361
    invoke-virtual {p3, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 364
    :cond_1
    iget-object p3, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 365
    invoke-virtual {p3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/RefundData;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/activity/refund/RefundData;->copyWithGiftCard(ZLcom/squareup/Card;Z)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 364
    invoke-virtual {p3, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onSkipRestockPressed()V
    .locals 2

    .line 1004
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1005
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->addExchangesOrIssueRefund()V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 3

    .line 1058
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 1062
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/RefundPinDialog;

    new-instance v1, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;

    .line 1064
    invoke-virtual {p2}, Lcom/squareup/cardreader/PinRequestData;->getCardInfo()Lcom/squareup/cardreader/CardInfo;

    move-result-object p2

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2, v2, v2}, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;-><init>(Lcom/squareup/cardreader/CardInfo;ZZZ)V

    iget-object p2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/activity/RefundPinDialog;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$Params;Lcom/squareup/ui/activity/IssueRefundScope;)V

    .line 1062
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onTenderDetailChanged(Lcom/squareup/activity/refund/TenderDetails;)V
    .locals 2

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    invoke-virtual {v1, p1}, Lcom/squareup/activity/refund/RefundData;->copyWithTenderDetails(Lcom/squareup/activity/refund/TenderDetails;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method onUpdateAppClicked()V
    .locals 3

    .line 606
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/billhistoryui/R$string;->google_play_square_pos_url:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method

.method performCashDrawerFunctions(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 901
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-static {p1, v0}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;->addRefundTendersToManagedCashDrawer(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V

    .line 903
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;->shouldOpenDrawerForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 904
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    :cond_0
    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 3

    .line 578
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->isInCardPresentRefund()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 581
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->UNKNOWN:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-ne p1, v0, :cond_1

    .line 582
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->ofRemoveCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->readerGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->fromRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/glyph/GlyphTypeface$Glyph;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 589
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cardPresentData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 590
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    invoke-static {}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->ofPleaseTapCard()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/activity/refund/CardPresentRefundScreenData;->copyForRetryableError(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;Lcom/squareup/util/Res;)Lcom/squareup/activity/refund/CardPresentRefundScreenData;

    move-result-object v0

    .line 589
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 592
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/activity/RefundCardPresenceRetryScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 596
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->lastCardPresentError:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    sget-object v0, Lcom/squareup/ui/activity/CardPresentRefundErrorState;->UNKNOWN:Lcom/squareup/ui/activity/CardPresentRefundErrorState;

    if-ne p1, v0, :cond_0

    .line 598
    iget-object p1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    goto :goto_0

    .line 601
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->getCardPresentCardDataThenRequestRefund()V

    :goto_0
    return-void
.end method

.method public retryAfterClientError()V
    .locals 3

    .line 438
    sget-object v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner$4;->$SwitchMap$com$squareup$activity$refund$RefundErrorCoordinator$ItemizedRefundStage:[I

    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 444
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->performRestockOnItemizedRefund()V

    goto :goto_0

    .line 447
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no such stage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->itemizedRefundStage()Lcom/squareup/activity/refund/RefundErrorCoordinator$ItemizedRefundStage;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " in ItemizedRefundStage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/activity/refund/RefundRetryEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundRetryEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 441
    invoke-virtual {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->onRefundPressed()V

    :goto_0
    return-void
.end method

.method screenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 288
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 289
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method showRefundItemizationScreen(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    move-object v0, p0

    .line 643
    iget-object v1, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->compositeDisposable:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 645
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v9, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v10, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 646
    invoke-direct {p0}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->shouldShowRefundToGiftCard()Z

    move-result v11

    iget-object v2, v0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->issueRefundScope:Lcom/squareup/ui/activity/IssueRefundScope;

    iget-object v12, v2, Lcom/squareup/ui/activity/IssueRefundScope;->itemizationMaxReturnableQuantity:Ljava/util/Map;

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v5, p1

    move-object v6, p2

    .line 643
    invoke-static/range {v3 .. v12}, Lcom/squareup/activity/refund/RefundData;->fromResidualBillAndCatalog(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/cogs/Cogs;ZLjava/util/Map;)Lrx/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$QrUwYdX0j6HozPz8y8l5f9q6Txk;

    invoke-direct {v3, p0}, Lcom/squareup/ui/activity/-$$Lambda$IssueRefundScopeRunner$QrUwYdX0j6HozPz8y8l5f9q6Txk;-><init>(Lcom/squareup/ui/activity/IssueRefundScopeRunner;)V

    .line 647
    invoke-virtual {v2, v3}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v2

    .line 643
    invoke-static {v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public swipedCard()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/Card;",
            ">;"
        }
    .end annotation

    .line 1147
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->REFUND_TO_GIFT_CARD:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1148
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->mergedSwipedCards:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0

    .line 1150
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->EMPTY_PLACEHOLDER_CARD:Lcom/squareup/Card;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method updateOtherReason(Ljava/lang/CharSequence;)V
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/activity/refund/RefundReasonEvent;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/activity/refund/RefundReasonEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 306
    iget-object v0, p0, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->refundData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/RefundData;

    .line 307
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-virtual {v1, p1, v2}, Lcom/squareup/activity/refund/RefundData;->copyWithRefundReason(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)Lcom/squareup/activity/refund/RefundData;

    move-result-object p1

    .line 306
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
