.class Lcom/squareup/ui/activity/TenderRowViewHolder$1;
.super Ljava/lang/Object;
.source "TenderRowViewHolder.java"

# interfaces
.implements Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/TenderRowViewHolder;->onBind(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TenderRowViewHolder;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;->this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyboardActionNextClicked()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;->this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->access$000(Lcom/squareup/ui/activity/TenderRowViewHolder;)Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;->this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/TenderRowViewHolder;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->onKeyboardActionNextClicked(I)V

    return-void
.end method

.method public onTipAmountChanged(Ljava/lang/Long;)V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;->this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-static {v0}, Lcom/squareup/ui/activity/TenderRowViewHolder;->access$000(Lcom/squareup/ui/activity/TenderRowViewHolder;)Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/activity/TenderRowViewHolder$1;->this$0:Lcom/squareup/ui/activity/TenderRowViewHolder;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/activity/BulkSettleView$TendersRecyclerAdapter;->onTipAmountChanged(Lcom/squareup/ui/activity/TenderRowViewHolder;Ljava/lang/Long;)V

    return-void
.end method
