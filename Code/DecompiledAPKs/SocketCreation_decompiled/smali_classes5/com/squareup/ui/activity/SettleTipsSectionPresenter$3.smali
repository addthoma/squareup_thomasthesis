.class Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;
.super Ljava/lang/Object;
.source "SettleTipsSectionPresenter.java"

# interfaces
.implements Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->doSettle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

.field final synthetic val$tenderState:Lcom/squareup/ui/activity/TenderEditState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/ui/activity/TenderEditState;)V
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->this$0:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iput-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->val$tenderState:Lcom/squareup/ui/activity/TenderEditState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 263
    iget-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->this$0:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    iget-object p2, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->val$tenderState:Lcom/squareup/ui/activity/TenderEditState;

    iget-object p2, p2, Lcom/squareup/ui/activity/TenderEditState;->tenderHistory:Lcom/squareup/billhistory/model/TenderHistory;

    invoke-static {p1, p2}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->access$100(Lcom/squareup/ui/activity/SettleTipsSectionPresenter;Lcom/squareup/billhistory/model/TenderHistory;)V

    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 257
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->onSuccess(Ljava/lang/Void;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Void;)V
    .locals 0

    .line 259
    iget-object p1, p0, Lcom/squareup/ui/activity/SettleTipsSectionPresenter$3;->this$0:Lcom/squareup/ui/activity/SettleTipsSectionPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/activity/SettleTipsSectionPresenter;->onSettleSuccess()V

    return-void
.end method
