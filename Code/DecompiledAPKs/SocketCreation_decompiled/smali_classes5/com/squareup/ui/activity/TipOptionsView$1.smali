.class Lcom/squareup/ui/activity/TipOptionsView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TipOptionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/activity/TipOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/activity/TipOptionsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/activity/TipOptionsView;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/activity/TipOptionsView$1;->this$0:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView$1;->this$0:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-static {v0}, Lcom/squareup/ui/activity/TipOptionsView;->access$000(Lcom/squareup/ui/activity/TipOptionsView;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView$1;->this$0:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-static {v0}, Lcom/squareup/ui/activity/TipOptionsView;->access$100(Lcom/squareup/ui/activity/TipOptionsView;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 71
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/activity/TipOptionsView$1;->this$0:Lcom/squareup/ui/activity/TipOptionsView;

    invoke-static {v0, p1}, Lcom/squareup/ui/activity/TipOptionsView;->access$200(Lcom/squareup/ui/activity/TipOptionsView;Ljava/lang/Long;)V

    return-void
.end method
