.class public Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
.super Landroid/view/ViewGroup;
.source "BillHistoryEntryRow.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;
    }
.end annotation


# static fields
.field private static final DRAW_BOTTOM_BORDER:Z = true


# instance fields
.field private final amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private final colorStrip:Landroid/view/View;

.field private final colorStripWidth:I

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final isTextTile:Z

.field private final marinMediumGray:I

.field private final minHeight:I

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final subtitle:Landroid/widget/TextView;

.field private final thumbnail:Lcom/squareup/glyph/SquareGlyphView;

.field private final titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

.field private final titleAndSubtitleContainer:Landroid/widget/LinearLayout;

.field private final transitionTime:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)V
    .locals 0

    .line 270
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 271
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->res:Lcom/squareup/util/Res;

    .line 272
    iput-object p3, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 273
    iput-boolean p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->isTextTile:Z

    .line 274
    iput-object p5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->countryCode:Lcom/squareup/CountryCode;

    const/4 p3, 0x0

    .line 277
    invoke-virtual {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setClipChildren(Z)V

    .line 278
    invoke-virtual {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setClipToPadding(Z)V

    .line 279
    sget p5, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-interface {p2, p5}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p5

    .line 280
    invoke-virtual {p0, p5, p3, p5, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setPadding(IIII)V

    .line 282
    sget p3, Lcom/squareup/billhistoryui/R$layout;->bill_history_entry_row_contents:I

    invoke-static {p1, p3, p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/high16 p1, 0x10e0000

    .line 284
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->transitionTime:I

    .line 285
    sget p1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->marinMediumGray:I

    .line 286
    sget p1, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->minHeight:I

    .line 288
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_thumbnail:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 289
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_color_strip:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStrip:Landroid/view/View;

    .line 290
    sget p1, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    .line 291
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStripWidth:I

    .line 293
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_title_and_subtitle_container:I

    .line 294
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndSubtitleContainer:Landroid/widget/LinearLayout;

    .line 295
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_title_and_quantity:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    .line 296
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_subtitle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    .line 298
    sget p1, Lcom/squareup/billhistoryui/R$id;->receipt_list_row_amount:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 300
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStrip:Landroid/view/View;

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 301
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    xor-int/lit8 p2, p4, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$1;)V
    .locals 0

    .line 232
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)V

    return-void
.end method

.method private getColor(I)I
    .locals 1

    .line 741
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method private hideTitle()V
    .locals 2

    .line 561
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PreservedLabelView;->setVisibility(I)V

    return-void
.end method

.method private inParentheses(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    .line 733
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/billhistoryui/R$string;->parenthesis:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "content"

    .line 734
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 735
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private setImage(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/marin/R$drawable;->marin_clear_border_bottom_dim_translucent_1px:I

    invoke-static {p2, p1, v0}, Lcom/squareup/util/Views;->createOverlaidDrawable(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object p1

    .line 714
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
    .locals 1

    .line 718
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    if-eqz p2, :cond_0

    .line 720
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    sget p2, Lcom/squareup/marin/R$drawable;->marin_medium_gray_border_bottom_medium_gray_pressed_1px:I

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundResource(I)V

    goto :goto_0

    .line 723
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    iget p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->marinMediumGray:I

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setBackgroundColor(I)V

    :goto_0
    return-void
.end method

.method private showInclusiveTax()V
    .locals 2

    .line 728
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    sget v1, Lcom/squareup/billhistoryui/R$string;->tax_included:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(I)V

    .line 729
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method private showTitle(I)V
    .locals 1

    .line 481
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantity(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private showTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    .line 485
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantity(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private showTitleWithPercentage(Ljava/lang/String;Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/OrderAdjustment;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 490
    iget-object v0, p2, Lcom/squareup/payment/OrderAdjustment;->discountApplicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v1, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-ne v0, v1, :cond_0

    .line 491
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/checkout/R$string;->comped_with_reason:I

    invoke-static {p2, p3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p3, "reason"

    .line 492
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 493
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 494
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 495
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 496
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    goto :goto_0

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 499
    invoke-virtual {p2, p3}, Lcom/squareup/payment/OrderAdjustment;->getPercentage(Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 500
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 501
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->inParentheses(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 503
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    .line 506
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method private showTitleWithQuantity(Ljava/lang/CharSequence;I)V
    .locals 2

    .line 510
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 511
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    .line 512
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    if-le p2, v1, :cond_0

    .line 514
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/checkout/R$string;->configure_item_summary_quantity_only:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 516
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "quantity"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 517
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 518
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 519
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->marinMediumGray:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 522
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->hidePreservedLabel()V

    :goto_0
    return-void
.end method


# virtual methods
.method enableCheckGiftCardBalance(Landroid/view/View$OnClickListener;)V
    .locals 3

    .line 701
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_blue:I

    .line 702
    invoke-direct {p0, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getColor(I)I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 701
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;Landroid/content/res/ColorStateList;)V

    .line 703
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method hideAmount()V
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method hideSubtitle()V
    .locals 2

    .line 580
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 581
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 3

    .line 401
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_clear_border_bottom_dim_translucent_1px:I

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Views;->createOverlaidDrawable(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object p1

    .line 405
    sget-object v0, Lcom/squareup/picasso/Picasso$LoadedFrom;->MEMORY:Lcom/squareup/picasso/Picasso$LoadedFrom;

    if-eq p2, v0, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 410
    :cond_0
    new-instance p2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 411
    invoke-virtual {v2}, Lcom/squareup/glyph/SquareGlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-direct {p2, v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 412
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 413
    iget p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->transitionTime:I

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    return-void

    .line 406
    :cond_1
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .line 347
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getMeasuredWidth()I

    move-result p1

    .line 348
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getMeasuredHeight()I

    move-result p2

    .line 349
    div-int/lit8 p3, p2, 0x2

    .line 350
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getPaddingLeft()I

    move-result p4

    .line 351
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getPaddingRight()I

    move-result p5

    .line 353
    iget-boolean v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->isTextTile:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/squareup/glyph/SquareGlyphView;->layout(IIII)V

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStrip:Landroid/view/View;

    iget v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStripWidth:I

    add-int/2addr v2, p4

    invoke-virtual {v0, p4, v1, v2, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 364
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p4

    .line 361
    invoke-virtual {v0, p4, v1, v2, p2}, Lcom/squareup/glyph/SquareGlyphView;->layout(IIII)V

    .line 368
    :goto_0
    iget-boolean p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->isTextTile:Z

    if-eqz p2, :cond_1

    iget p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStripWidth:I

    add-int/2addr p4, p2

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p2}, Lcom/squareup/glyph/SquareGlyphView;->getRight()I

    move-result p4

    .line 370
    :goto_1
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p2}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p2

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    div-int/lit8 p2, p2, 0x2

    sub-int v1, p3, p2

    .line 374
    invoke-virtual {v0}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p4

    add-int/2addr p2, p3

    .line 371
    invoke-virtual {v0, p4, v1, v2, p2}, Lcom/squareup/widgets/PreservedLabelView;->layout(IIII)V

    .line 377
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p2

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    div-int/lit8 p2, p2, 0x2

    sub-int v1, p3, p2

    .line 381
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p4

    add-int/2addr p2, p3

    .line 378
    invoke-virtual {v0, p4, v1, v2, p2}, Landroid/widget/TextView;->layout(IIII)V

    .line 384
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndSubtitleContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p2

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndSubtitleContainer:Landroid/widget/LinearLayout;

    div-int/lit8 p2, p2, 0x2

    sub-int v1, p3, p2

    .line 388
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p4

    add-int/2addr p2, p3

    .line 385
    invoke-virtual {v0, p4, v1, v2, p2}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 391
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredHeight()I

    move-result p2

    .line 392
    iget-object p4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    sub-int/2addr p1, p5

    .line 393
    invoke-virtual {p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result p5

    sub-int p5, p1, p5

    div-int/lit8 p2, p2, 0x2

    sub-int v0, p3, p2

    add-int/2addr p3, p2

    .line 392
    invoke-virtual {p4, p5, v0, p1, p3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .line 305
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 307
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 308
    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v3, 0x0

    invoke-static {p1, v3, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 309
    invoke-virtual {v4}, Lcom/squareup/glyph/SquareGlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v3, v4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result v4

    .line 307
    invoke-virtual {v1, v2, v4}, Lcom/squareup/glyph/SquareGlyphView;->measure(II)V

    .line 311
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 312
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v3, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result p1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 313
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v3, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result v2

    .line 311
    invoke-virtual {v1, p1, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->measure(II)V

    .line 317
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getPaddingLeft()I

    move-result p1

    sub-int p1, v0, p1

    .line 318
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->thumbnail:Lcom/squareup/glyph/SquareGlyphView;

    .line 319
    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 320
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 322
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    const/high16 v2, -0x80000000

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    .line 323
    invoke-virtual {v5}, Lcom/squareup/widgets/PreservedLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v3, v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result v5

    .line 322
    invoke-virtual {v1, v4, v5}, Lcom/squareup/widgets/PreservedLabelView;->measure(II)V

    .line 325
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    .line 326
    invoke-virtual {v5}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {p2, v3, v5}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result v5

    .line 325
    invoke-virtual {v1, v4, v5}, Landroid/widget/TextView;->measure(II)V

    .line 328
    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndSubtitleContainer:Landroid/widget/LinearLayout;

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    iget-object v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndSubtitleContainer:Landroid/widget/LinearLayout;

    .line 330
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 329
    invoke-static {p2, v3, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getChildMeasureSpec(III)I

    move-result p2

    .line 328
    invoke-virtual {v1, p1, p2}, Landroid/widget/LinearLayout;->measure(II)V

    .line 334
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {p1}, Lcom/squareup/widgets/PreservedLabelView;->getMeasuredHeight()I

    move-result p1

    .line 335
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p2

    mul-int/lit8 v1, p1, 0x2

    if-lt p2, v1, :cond_0

    add-int/2addr p2, p1

    :cond_0
    add-int/2addr p1, p2

    .line 343
    iget p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->minHeight:I

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setMeasuredDimension(II)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method setColorStripBackgroundColor(Ljava/lang/String;)V
    .locals 2

    .line 425
    iget-boolean v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->isTextTile:Z

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->colorStrip:Landroid/view/View;

    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-static {p1, v1}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    return-void
.end method

.method setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 565
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 568
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->subtitle:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showAdjustment(Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderAdjustment;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 586
    iget-object v0, p1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    .line 587
    sget-object v1, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    .line 588
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    goto :goto_0

    .line 590
    :cond_0
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 593
    :goto_0
    sget-object v1, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    if-ne v0, v1, :cond_1

    goto :goto_1

    .line 598
    :cond_1
    sget-object p3, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    if-ne v0, p3, :cond_2

    .line 599
    iget-object p3, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-direct {p0, p3, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithPercentage(Ljava/lang/String;Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;)V

    goto :goto_3

    .line 601
    :cond_2
    iget-object p2, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 594
    :cond_3
    :goto_1
    iget-object p2, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_4

    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->tax_name_default:I

    .line 595
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_4
    iget-object p2, p1, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    .line 597
    :goto_2
    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithPercentage(Ljava/lang/String;Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;)V

    .line 604
    :goto_3
    sget-object p2, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    if-ne v0, p2, :cond_5

    .line 605
    invoke-direct {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showInclusiveTax()V

    goto :goto_4

    .line 607
    :cond_5
    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1, p4}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 609
    :goto_4
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 455
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 456
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-void
.end method

.method showAmountForItem(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 466
    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 467
    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 468
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 470
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 473
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setEnabled(Z)V

    return-void
.end method

.method showCustomAmountImage()V
    .locals 3

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    .line 443
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 442
    invoke-static {v0}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 444
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->minHeight:I

    .line 441
    invoke-static {v0, v1, v2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forCustomItem(Ljava/lang/String;Landroid/content/Context;I)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object v0

    const/4 v1, 0x1

    .line 445
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setImage(Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method showDiscountLineItem(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;Z)V"
        }
    .end annotation

    .line 614
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 615
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 617
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 619
    invoke-static {v0}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v0

    .line 618
    invoke-interface {p3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->inParentheses(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p3

    .line 620
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p3}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz p4, :cond_1

    .line 623
    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 624
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 626
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    return-void
.end method

.method showFeeLineItem(Lcom/squareup/protos/client/bills/FeeLineItem;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;Z)V"
        }
    .end annotation

    .line 631
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 632
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    .line 635
    invoke-static {v1}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 634
    invoke-interface {p3, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->inParentheses(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/squareup/widgets/PreservedLabelView;->setPreservedLabel(Ljava/lang/CharSequence;)V

    .line 637
    iget-object p3, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iget-object p3, p3, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne p3, v0, :cond_1

    if-eqz p4, :cond_0

    .line 638
    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 639
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 641
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    goto :goto_1

    .line 643
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showInclusiveTax()V

    :goto_1
    return-void
.end method

.method showGiftCardImage(Ljava/lang/String;)V
    .locals 1

    .line 449
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v0, 0x1

    .line 450
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setImage(Landroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method showItemImage(Lcom/squareup/checkout/CartItem;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 4

    .line 432
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemAbbreviation:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->color:Ljava/lang/String;

    iget v2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->minHeight:I

    .line 433
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object v0

    const/4 v1, 0x1

    .line 434
    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setImage(Landroid/graphics/drawable/Drawable;Z)V

    .line 435
    invoke-virtual {p2, p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Lcom/squareup/checkout/CartItem;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    iget p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->minHeight:I

    invoke-virtual {p1, p2, p0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    return-void
.end method

.method showSubtitle(Ljava/lang/CharSequence;)V
    .locals 0

    .line 574
    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 575
    invoke-direct {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideTitle()V

    .line 576
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideAmount()V

    return-void
.end method

.method showSubtotal(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 685
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_EQUALS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 686
    sget v0, Lcom/squareup/utilities/R$string;->subtotal:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(I)V

    .line 687
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 688
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showSurcharge(Lcom/squareup/checkout/Surcharge;Lcom/squareup/text/Formatter;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/Surcharge;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z)V"
        }
    .end annotation

    .line 659
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 660
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->countryCode:Lcom/squareup/CountryCode;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lcom/squareup/checkout/Surcharge;->formattedName(Lcom/squareup/util/Res;ZLcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 661
    invoke-virtual {p1}, Lcom/squareup/checkout/Surcharge;->getHistoricalAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 662
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 663
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showSwedishRounding(Lcom/squareup/payment/OrderAdjustment;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderAdjustment;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 674
    iget-object p1, p1, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showSwedishRounding(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    return-void
.end method

.method showSwedishRounding(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 678
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PENNIES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 679
    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_rounding:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(I)V

    .line 680
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 681
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showTaxMultipleAdjustment()V
    .locals 2

    .line 652
    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_tax_multiple_included:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(I)V

    .line 653
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 654
    invoke-direct {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showInclusiveTax()V

    .line 655
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showTip(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;Z)V"
        }
    .end annotation

    .line 667
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    if-eqz p3, :cond_0

    .line 668
    sget p3, Lcom/squareup/billhistoryui/R$string;->additional_tip:I

    goto :goto_0

    :cond_0
    sget p3, Lcom/squareup/activity/R$string;->tip:I

    :goto_0
    invoke-direct {p0, p3}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(I)V

    .line 669
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 670
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method

.method showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 531
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;I)V

    return-void
.end method

.method showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;I)V
    .locals 3

    .line 537
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 544
    :cond_0
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    goto :goto_0

    .line 547
    :cond_1
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 549
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "How did an item with no name get here? %s"

    .line 550
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 552
    :cond_2
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 556
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantity(Ljava/lang/CharSequence;I)V

    .line 557
    iget-object p2, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p2, v0, p1}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/checkout/CartItem;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->setSubtitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showTitleWithSubtitle(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    const/4 v0, 0x0

    .line 527
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitleWithQuantityAndSubtitle(Lcom/squareup/checkout/CartItem;I)V

    return-void
.end method

.method showTotal(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 692
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_EQUALS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showGlyphWithBackground(Lcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    .line 693
    sget v0, Lcom/squareup/billhistoryui/R$string;->receipt_detail_total:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showTitle(I)V

    .line 694
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->showAmount(Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;)V

    .line 695
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->amount:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 696
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->titleAndQuantity:Lcom/squareup/widgets/PreservedLabelView;

    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/PreservedLabelView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 697
    invoke-virtual {p0}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;->hideSubtitle()V

    return-void
.end method
