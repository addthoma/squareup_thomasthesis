.class public interface abstract Lcom/squareup/ui/activity/QuickTipEditorPresenter$TipAmountListener;
.super Ljava/lang/Object;
.source "QuickTipEditorPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/QuickTipEditorPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TipAmountListener"
.end annotation


# virtual methods
.method public abstract onKeyboardActionNextClicked()V
.end method

.method public abstract onTipAmountChanged(Ljava/lang/Long;)V
.end method
