.class public interface abstract Lcom/squareup/ui/activity/ActivitySearchPaymentStarter$SearchListener;
.super Ljava/lang/Object;
.source "ActivitySearchPaymentStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SearchListener"
.end annotation


# virtual methods
.method public abstract onCardError()V
.end method

.method public abstract onCardInserted()V
.end method

.method public abstract onCardMustBeReInserted()V
.end method

.method public abstract onCardRemoved()V
.end method

.method public abstract onCardRemovedDuringPayment()V
.end method

.method public abstract onFieldTimeout()V
.end method

.method public abstract onInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
.end method

.method public abstract onPaymentTerminated(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
.end method

.method public abstract onPaymentTerminatedDueToSwipe(Lcom/squareup/Card;)V
.end method

.method public abstract onSearching()V
.end method
