.class public Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$Factory;
.super Ljava/lang/Object;
.source "BillHistoryEntryRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;)Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;
    .locals 8

    .line 263
    new-instance v7, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLcom/squareup/CountryCode;Lcom/squareup/ui/activity/billhistory/BillHistoryEntryRow$1;)V

    return-object v7
.end method
