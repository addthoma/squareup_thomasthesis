.class public Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;
.super Ljava/lang/Object;
.source "GiftCardCheckBalanceStarter.java"


# instance fields
.field private final giftCardByTokenCallPresenter:Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->giftCardByTokenCallPresenter:Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/caller/FailurePopup;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->giftCardByTokenCallPresenter:Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method public requestGiftCardBalance(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    .line 26
    iget-object p1, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->giftCardByTokenCallPresenter:Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

    new-instance v0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 27
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    move-result-object p2

    .line 29
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;

    move-result-object p2

    .line 26
    invoke-virtual {p1, p2}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->fetch(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;)V

    return-void
.end method

.method public takeView(Lcom/squareup/caller/FailurePopup;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/activity/billhistory/GiftCardCheckBalanceStarter;->giftCardByTokenCallPresenter:Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/GiftCardByTokenCallPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
