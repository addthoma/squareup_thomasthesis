.class public Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;
.super Lcom/squareup/ui/activity/InIssueRefundScope;
.source "RefundProgressSpinnerScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/activity/RefundProgressSpinnerScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/activity/RefundProgressSpinnerScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final showSpinner:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/ui/activity/-$$Lambda$RefundProgressSpinnerScreen$2MfW3vWD0PLqtPb_nmmg-02zvzg;->INSTANCE:Lcom/squareup/ui/activity/-$$Lambda$RefundProgressSpinnerScreen$2MfW3vWD0PLqtPb_nmmg-02zvzg;

    .line 51
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;)V
    .locals 1

    const/4 v0, 0x1

    .line 22
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/activity/IssueRefundScope;Z)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/activity/InIssueRefundScope;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;)V

    .line 27
    iput-boolean p2, p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->showSpinner:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->showSpinner:Z

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;
    .locals 2

    .line 52
    const-class v0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/IssueRefundScope;

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 55
    :goto_0
    new-instance p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;-><init>(Lcom/squareup/ui/activity/IssueRefundScope;Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->parentKey:Lcom/squareup/ui/activity/IssueRefundScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 47
    iget-boolean p2, p0, Lcom/squareup/ui/activity/RefundProgressSpinnerScreen;->showSpinner:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 31
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ISSUE_REFUND:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method
