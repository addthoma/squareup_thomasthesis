.class public final Lcom/squareup/ui/activity/ReaderResult$Companion;
.super Ljava/lang/Object;
.source "CardPresentRefund.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ReaderResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/activity/ReaderResult$Companion;",
        "",
        "()V",
        "authorizationOf",
        "Lcom/squareup/ui/activity/ReaderResult;",
        "readerType",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "authorizationByteArray",
        "",
        "errorOf",
        "messageResources",
        "Lcom/squareup/ui/activity/CardPresentRefundMessageResources;",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/activity/ReaderResult$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final authorizationOf(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/ui/activity/ReaderResult;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "readerType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorizationByteArray"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/squareup/ui/activity/ReaderResult;

    .line 31
    sget-object v1, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    array-length v2, p2

    invoke-static {p2, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p2

    invoke-virtual {v1, p2}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object p2

    .line 32
    sget-object v1, Lcom/squareup/ui/activity/CardPresentRefundMessageResources;->Companion:Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/activity/CardPresentRefundMessageResources$Companion;->empty()Lcom/squareup/ui/activity/CardPresentRefundMessageResources;

    move-result-object v1

    const/4 v2, 0x1

    .line 28
    invoke-direct {v0, v2, p1, p2, v1}, Lcom/squareup/ui/activity/ReaderResult;-><init>(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)V

    return-object v0
.end method

.method public final errorOf(Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)Lcom/squareup/ui/activity/ReaderResult;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "messageResources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/ui/activity/ReaderResult;

    .line 38
    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 39
    sget-object v2, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v3, 0x0

    .line 36
    invoke-direct {v0, v3, v1, v2, p1}, Lcom/squareup/ui/activity/ReaderResult;-><init>(ZLcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/ui/activity/CardPresentRefundMessageResources;)V

    return-object v0
.end method
