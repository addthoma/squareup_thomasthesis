.class public abstract Lcom/squareup/ui/activity/ActivityAppletScope$Module;
.super Ljava/lang/Object;
.source "ActivityAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/activity/ActivityAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideController(Lcom/squareup/ui/activity/BillHistoryRunner;)Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideLinkDebitCardWorkflowResultHandler(Lcom/squareup/ui/activity/ActivityAppletScopeRunner;)Lcom/squareup/debitcard/LinkDebitCardWorkflowRunner$LinkDebitCardWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePriceChangeRunner(Lcom/squareup/ui/activity/ActivityAppletScopeRunner;)Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderWithCustomerInfoCache(Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransactionsHistoryRefundHelper(Lcom/squareup/ui/activity/RealActivityTransactionsHistoryRefundHelper;)Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
