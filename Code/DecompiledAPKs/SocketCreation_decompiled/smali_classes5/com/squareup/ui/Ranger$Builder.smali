.class public Lcom/squareup/ui/Ranger$Builder;
.super Ljava/lang/Object;
.source "Ranger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/Ranger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Enum;",
        ":",
        "Lcom/squareup/ui/Ranger$RowType<",
        "TH;>;H:",
        "Ljava/lang/Enum;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private nextIndex:I

.field private orderedRanges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;"
        }
    .end annotation
.end field

.field private rangesByType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TR;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/Ranger$Builder;->orderedRanges:Ljava/util/List;

    .line 79
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/Ranger$Builder;->rangesByType:Ljava/util/Map;

    const/4 v0, 0x0

    .line 80
    iput v0, p0, Lcom/squareup/ui/Ranger$Builder;->nextIndex:I

    return-void
.end method


# virtual methods
.method public addOne(Ljava/lang/Enum;)Lcom/squareup/ui/Ranger$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Lcom/squareup/ui/Ranger$Builder<",
            "TR;TH;>;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 84
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/Ranger$Builder;->addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;

    return-object p0
.end method

.method public addRange(Ljava/lang/Enum;I)Lcom/squareup/ui/Ranger$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;I)",
            "Lcom/squareup/ui/Ranger$Builder<",
            "TR;TH;>;"
        }
    .end annotation

    if-lez p2, :cond_1

    .line 93
    new-instance v0, Lcom/squareup/ui/Ranger$Range;

    iget v1, p0, Lcom/squareup/ui/Ranger$Builder;->nextIndex:I

    add-int v2, v1, p2

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/ui/Ranger$Range;-><init>(IILjava/lang/Enum;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/Ranger$Builder;->orderedRanges:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/Ranger$Builder;->rangesByType:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    .line 97
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 98
    iget-object v2, p0, Lcom/squareup/ui/Ranger$Builder;->rangesByType:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    iget p1, p0, Lcom/squareup/ui/Ranger$Builder;->nextIndex:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/ui/Ranger$Builder;->nextIndex:I

    return-object p0

    .line 90
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 91
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v0, v1

    const-string p2, "Must provide a count greater than 0! Provided count of size %d"

    .line 90
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public build()Lcom/squareup/ui/Ranger;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ui/Ranger<",
            "TR;TH;>;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/squareup/ui/Ranger;

    iget-object v1, p0, Lcom/squareup/ui/Ranger$Builder;->orderedRanges:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/ui/Ranger$Builder;->rangesByType:Ljava/util/Map;

    iget v3, p0, Lcom/squareup/ui/Ranger$Builder;->nextIndex:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/Ranger;-><init>(Ljava/util/List;Ljava/util/Map;I)V

    return-object v0
.end method
