.class Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;
.super Ljava/lang/Object;
.source "ReorderableRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ReorderableRecyclerViewAdapter;

.field final synthetic val$viewHolder:Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ReorderableRecyclerViewAdapter;Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;)V
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;->this$0:Lcom/squareup/ui/ReorderableRecyclerViewAdapter;

    iput-object p2, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;->val$viewHolder:Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 186
    invoke-static {p2}, Landroidx/core/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result p1

    if-nez p1, :cond_0

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;->this$0:Lcom/squareup/ui/ReorderableRecyclerViewAdapter;

    iget-object p2, p0, Lcom/squareup/ui/ReorderableRecyclerViewAdapter$1;->val$viewHolder:Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->startDrag(Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
