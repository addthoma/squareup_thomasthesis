.class public Lcom/squareup/ui/account/view/MultiLineRow;
.super Landroid/widget/LinearLayout;
.source "MultiLineRow.java"


# instance fields
.field private lineRowToCopy:Lcom/squareup/ui/account/view/LineRow;

.field private paddingVertical:I

.field private titleView:Lcom/squareup/widgets/PreservedLabelView;

.field private valuesLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x0

    .line 31
    iput p2, p0, Lcom/squareup/ui/account/view/MultiLineRow;->paddingVertical:I

    .line 35
    sget p2, Lcom/squareup/widgets/pos/R$layout;->multi_line_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/account/view/MultiLineRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 37
    sget p1, Lcom/squareup/widgets/pos/R$id;->preserved_label:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PreservedLabelView;

    iput-object p1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->titleView:Lcom/squareup/widgets/PreservedLabelView;

    .line 38
    sget p1, Lcom/squareup/widgets/pos/R$id;->values:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->valuesLayout:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public addValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/account/view/MultiLineRow;->valuesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/MultiLineRow;->makeTextView(Ljava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object p0
.end method

.method public addValues(Ljava/util/List;)Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;)",
            "Lcom/squareup/ui/account/view/MultiLineRow;"
        }
    .end annotation

    .line 60
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 61
    invoke-virtual {p0, v0}, Lcom/squareup/ui/account/view/MultiLineRow;->addValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/MultiLineRow;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public clearValues()Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/account/view/MultiLineRow;->valuesLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-object p0
.end method

.method public makeTextView(Ljava/lang/CharSequence;)Landroid/widget/TextView;
    .locals 3

    .line 42
    new-instance v0, Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/account/view/MultiLineRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x5

    .line 44
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 45
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public setContainerPadding()Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/account/view/MultiLineRow;->lineRowToCopy:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0}, Lcom/squareup/ui/account/view/LineRow;->getApparentPadding()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/account/view/MultiLineRow;->setContainerPadding(I)Lcom/squareup/ui/account/view/MultiLineRow;

    move-result-object v0

    return-object v0
.end method

.method public setContainerPadding(I)Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 3

    .line 84
    iput p1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->paddingVertical:I

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->lineRowToCopy:Lcom/squareup/ui/account/view/LineRow;

    .line 86
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow;->getPaddingLeft()I

    move-result p1

    iget v0, p0, Lcom/squareup/ui/account/view/MultiLineRow;->paddingVertical:I

    iget-object v1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->lineRowToCopy:Lcom/squareup/ui/account/view/LineRow;

    .line 88
    invoke-virtual {v1}, Lcom/squareup/ui/account/view/LineRow;->getPaddingRight()I

    move-result v1

    iget v2, p0, Lcom/squareup/ui/account/view/MultiLineRow;->paddingVertical:I

    .line 85
    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/squareup/ui/account/view/MultiLineRow;->setPadding(IIII)V

    return-object p0
.end method

.method public setLineRowToCopy(Lcom/squareup/ui/account/view/LineRow;)Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/account/view/MultiLineRow;->lineRowToCopy:Lcom/squareup/ui/account/view/LineRow;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/MultiLineRow;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/account/view/MultiLineRow;->titleView:Lcom/squareup/widgets/PreservedLabelView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/PreservedLabelView;->setTitle(Ljava/lang/CharSequence;)V

    return-object p0
.end method
