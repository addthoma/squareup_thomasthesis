.class public final Lcom/squareup/ui/ActivityScopeCleaner;
.super Ljava/lang/Object;
.source "ActivityScopeCleaner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nActivityScopeCleaner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ActivityScopeCleaner.kt\ncom/squareup/ui/ActivityScopeCleaner\n*L\n1#1,43:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0006H\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/ActivityScopeCleaner;",
        "Lmortar/Scoped;",
        "activity",
        "Landroid/app/Activity;",
        "(Landroid/app/Activity;)V",
        "forget",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/ui/ActivityScopeCleaner;->activity:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public final forget()V
    .locals 1

    const/4 v0, 0x0

    .line 26
    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/squareup/ui/ActivityScopeCleaner;->activity:Landroid/app/Activity;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/ActivityScopeCleaner;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    const v1, 0x1020002

    .line 36
    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 38
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 39
    sget v1, Lcom/squareup/marin/R$drawable;->window_background_logo:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :cond_0
    return-void
.end method
