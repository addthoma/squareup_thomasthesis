.class public final Lcom/squareup/ui/LinkSpanModel;
.super Ljava/lang/Object;
.source "LinkSpanModel.kt"

# interfaces
.implements Lcom/squareup/resources/TextModel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/LinkSpanModel$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/resources/TextModel<",
        "Landroid/text/Spannable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0004H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00042\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0004H\u00c6\u0001J\t\u0010\u0010\u001a\u00020\u0004H\u00d6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\u0010\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\t\u0010\u0018\u001a\u00020\u0004H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0004H\u00d6\u0001R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\t\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/LinkSpanModel;",
        "Lcom/squareup/resources/TextModel;",
        "Landroid/text/Spannable;",
        "clickableText",
        "",
        "url",
        "linkColor",
        "(III)V",
        "getClickableText",
        "()I",
        "getLinkColor",
        "getUrl",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "evaluate",
        "context",
        "Landroid/content/Context;",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final clickableText:I

.field private final linkColor:I

.field private final url:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/LinkSpanModel$Creator;

    invoke-direct {v0}, Lcom/squareup/ui/LinkSpanModel$Creator;-><init>()V

    sput-object v0, Lcom/squareup/ui/LinkSpanModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    iput p2, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    iput p3, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    return-void
.end method

.method public synthetic constructor <init>(IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 20
    sget p3, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/LinkSpanModel;IIIILjava/lang/Object;)Lcom/squareup/ui/LinkSpanModel;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/LinkSpanModel;->copy(III)Lcom/squareup/ui/LinkSpanModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    return v0
.end method

.method public final copy(III)Lcom/squareup/ui/LinkSpanModel;
    .locals 1

    new-instance v0, Lcom/squareup/ui/LinkSpanModel;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/LinkSpanModel;-><init>(III)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/LinkSpanModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/LinkSpanModel;

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    iget v1, p1, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    iget v1, p1, Lcom/squareup/ui/LinkSpanModel;->url:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    iget p1, p1, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public evaluate(Landroid/content/Context;)Landroid/text/Spannable;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 24
    iget p1, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    invoke-virtual {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 25
    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 26
    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object p1

    const-string v0, "LinkSpan.Builder(context\u2026r)\n        .asSpannable()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/ui/LinkSpanModel;->evaluate(Landroid/content/Context;)Landroid/text/Spannable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public final getClickableText()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    return v0
.end method

.method public final getLinkColor()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    return v0
.end method

.method public final getUrl()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LinkSpanModel(clickableText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", linkColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/ui/LinkSpanModel;->clickableText:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/ui/LinkSpanModel;->url:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/ui/LinkSpanModel;->linkColor:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
