.class public interface abstract Lcom/squareup/ui/balance/BalanceAppletScope$Component;
.super Ljava/lang/Object;
.source "BalanceAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardScope$ParentComponent;
.implements Lcom/squareup/transferreports/TransferReportsScope$ParentComponent;
.implements Lcom/squareup/ui/balance/addmoney/AddMoneyScope$ParentComponent;
.implements Lcom/squareup/ui/balance/bizbanking/capital/CapitalScope$ParentComponent;
.implements Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityScope$ParentComponent;
.implements Lcom/squareup/instantdeposit/PriceChangeDialog$ParentComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract balanceAppletScopeRunner()Lcom/squareup/ui/balance/BalanceAppletScopeRunner;
.end method

.method public abstract balanceMasterScreenComponent()Lcom/squareup/ui/balance/BalanceMasterScreen$Component;
.end method

.method public abstract balanceScopeRunner()Lcom/squareup/ui/balance/bizbanking/BalanceScopeRunner;
.end method

.method public abstract balanceTransactionDetailCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionDetailCoordinator;
.end method

.method public abstract balanceTransactionsCoordinator()Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
.end method

.method public abstract confirmTransferCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
.end method

.method public abstract features()Lcom/squareup/settings/server/Features;
.end method

.method public abstract instantDepositResultCoordinator()Lcom/squareup/ui/balance/bizbanking/InstantDepositResultCoordinator;
.end method

.method public abstract linkBankAccountDialogRunner()Lcom/squareup/ui/balance/bizbanking/transfer/LinkBankAccountDialog$Runner;
.end method

.method public abstract linkDebitCardDialogRunner()Lcom/squareup/ui/balance/bizbanking/transfer/LinkDebitCardDialog$Runner;
.end method

.method public abstract transferResultCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;
.end method

.method public abstract transferToBankCoordinator()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;
.end method
