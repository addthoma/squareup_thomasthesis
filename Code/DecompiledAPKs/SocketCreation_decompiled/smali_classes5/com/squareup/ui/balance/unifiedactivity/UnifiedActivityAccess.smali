.class public final Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "UnifiedActivityAccess.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "determineVisibility",
        "",
        "isSquareCardSeller",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private final isSquareCardSeller()Z
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getBusinessBankingSettings()Lcom/squareup/settings/server/BusinessBankingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/BusinessBankingSettings;->showCardSpend()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_UNIFIED_ACTIVITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/balance/unifiedactivity/UnifiedActivityAccess;->isSquareCardSeller()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
