.class final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;
.super Ljava/lang/Object;
.source "SquareCardActivityRequester.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->updateTransactionCategory(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            ">;)V"
        }
    .end annotation

    .line 125
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;

    invoke-static {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$onUpdateTransactionCategorySuccess(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;)V

    goto :goto_0

    .line 126
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$onUpdateTransactionCategoryFailed(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
