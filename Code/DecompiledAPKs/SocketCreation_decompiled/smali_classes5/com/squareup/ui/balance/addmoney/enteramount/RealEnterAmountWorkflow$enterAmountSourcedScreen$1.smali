.class final Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEnterAmountWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow;->enterAmountSourcedScreen(Lcom/squareup/protos/client/deposits/CardInfo;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/common/Money;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/common/Money;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/deposits/CardInfo;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/protos/client/deposits/CardInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;->$card:Lcom/squareup/protos/client/deposits/CardInfo;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;->invoke(Lcom/squareup/protos/common/Money;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/common/Money;)V
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;

    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$enterAmountSourcedScreen$1;->$card:Lcom/squareup/protos/client/deposits/CardInfo;

    invoke-direct {v1, p1, v2}, Lcom/squareup/ui/balance/addmoney/enteramount/RealEnterAmountWorkflow$Action$Submit;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/CardInfo;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
