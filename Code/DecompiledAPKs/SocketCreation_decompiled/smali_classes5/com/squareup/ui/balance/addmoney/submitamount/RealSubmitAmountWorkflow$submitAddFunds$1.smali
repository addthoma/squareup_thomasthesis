.class final Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;
.super Ljava/lang/Object;
.source "RealSubmitAmountWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->submitAddFunds(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;->this$0:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/deposits/CreateTransferResponse;",
            ">;)",
            "Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Success;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    iget-object p1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    iget-object p1, p1, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    const-string v1, "result.response.balance_activity.amount"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult$Success;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    goto :goto_0

    .line 175
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;->this$0:Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;->access$mapFailure(Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow;Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$submitAddFunds$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/balance/addmoney/submitamount/RealSubmitAmountWorkflow$TransfersResult;

    move-result-object p1

    return-object p1
.end method
