.class public final Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "AddMoneyWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;,
        Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyProps;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddMoneyWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddMoneyWorkflowRunner.kt\ncom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner\n+ 2 PosContainer.kt\ncom/squareup/ui/main/PosContainers\n*L\n1#1,56:1\n152#2:57\n*E\n*S KotlinDebug\n*F\n+ 1 AddMoneyWorkflowRunner.kt\ncom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner\n*L\n38#1:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00162\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0015\u0016B\'\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003H\u0002J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014H\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyProps;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        "viewFactory",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;",
        "workflow",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "resultHandler",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;",
        "(Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;)V",
        "getWorkflow",
        "()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;",
        "onAddMoneyResult",
        "",
        "result",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "AddMoneyWorkflowResultHandler",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final resultHandler:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;

.field private final workflow:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->Companion:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$Companion;

    .line 42
    const-class v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AddMoneyWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/addmoney/AddMoneyViewFactory;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultHandler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget-object v2, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->NAME:Ljava/lang/String;

    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 16
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->workflow:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->resultHandler:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;

    return-void
.end method

.method public static final synthetic access$ensureWorkflow(Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;)V
    .locals 0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->ensureWorkflow()V

    return-void
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getProps$p(Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;)Lcom/squareup/ui/balance/addmoney/AddMoneyProps;
    .locals 0

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->getProps()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/balance/addmoney/AddMoneyProps;

    return-object p0
.end method

.method public static final synthetic access$onAddMoneyResult(Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->onAddMoneyResult(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V

    return-void
.end method

.method public static final synthetic access$setProps$p(Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;Lcom/squareup/ui/balance/addmoney/AddMoneyProps;)V
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->setProps(Ljava/lang/Object;)V

    return-void
.end method

.method private final onAddMoneyResult(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V
    .locals 3

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->resultHandler:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;

    invoke-interface {v0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$AddMoneyWorkflowResultHandler;->onAddMoneyResult(Lcom/squareup/ui/balance/addmoney/AddMoneyResult;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 57
    const-class v1, Lcom/squareup/ui/balance/addmoney/AddMoneyScope;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPast([Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->workflow:Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->getWorkflow()Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;

    invoke-direct {v1, v2}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
