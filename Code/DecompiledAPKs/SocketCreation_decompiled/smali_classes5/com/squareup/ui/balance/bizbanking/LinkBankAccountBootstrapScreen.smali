.class public final Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "LinkBankAccountBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "()V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;->INSTANCE:Lcom/squareup/ui/balance/bizbanking/LinkBankAccountBootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner;->Companion:Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$Companion;

    sget-object v1, Lcom/squareup/banklinking/LinkBankAccountProps$StartFromFetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/LinkBankAccountProps$StartFromFetchingBankAccount;

    check-cast v1, Lcom/squareup/banklinking/LinkBankAccountProps;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/banklinking/LinkBankAccountWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/banklinking/LinkBankAccountProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 3

    .line 13
    new-instance v0, Lcom/squareup/banklinking/LinkBankAccountScope;

    sget-object v1, Lcom/squareup/ui/balance/BalanceAppletScope;->INSTANCE:Lcom/squareup/ui/balance/BalanceAppletScope;

    const-string v2, "BalanceAppletScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/LinkBankAccountScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method
