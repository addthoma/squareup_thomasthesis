.class public final Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;
.super Lcom/squareup/balance/squarecard/ManageSquareCardSection;
.source "RealManageSquareCardSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSection;",
        "manageSquareCardSectionAccess",
        "Lcom/squareup/balance/squarecard/ManageSquareCardSectionAccess;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/balance/squarecard/ManageSquareCardSectionAccess;Lcom/squareup/util/Device;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardBootstrapScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/ManageSquareCardSectionAccess;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "manageSquareCardSectionAccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    check-cast p1, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/ManageSquareCardSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;->getInitialScreen()Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardBootstrapScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardBootstrapScreen;
    .locals 2

    .line 14
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardBootstrapScreen;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/squarecard/RealManageSquareCardSection;->device:Lcom/squareup/util/Device;

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/squarecard/ManageSquareCardBootstrapScreen;-><init>(Lcom/squareup/util/Device;)V

    return-object v0
.end method
