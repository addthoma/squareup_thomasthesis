.class final Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;
.super Lcom/squareup/ui/LinkSpan;
.source "EnterAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DepositSettingsLinkSpan"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;",
        "Lcom/squareup/ui/LinkSpan;",
        "context",
        "Landroid/content/Context;",
        "screen",
        "Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "(Landroid/content/Context;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V",
        "getScreen",
        "()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    sget v0, Lcom/squareup/noho/R$color;->noho_color_selector_row_value:I

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;->screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    return-void
.end method


# virtual methods
.method public final getScreen()Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;->screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountCoordinator$DepositSettingsLinkSpan;->screen:Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/enteramount/EnterAmountScreen;->getOnDepositSettingsTapped()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method
