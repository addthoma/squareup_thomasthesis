.class public abstract Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;
.super Ljava/lang/Object;
.source "BalanceTransactionsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TransactionRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionDateRow;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactions;,
        Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;",
        "",
        "()V",
        "id",
        "",
        "getId",
        "()J",
        "CardTransactionDateRow",
        "CardTransactionRow",
        "LoadMoreTransactions",
        "LoadMoreTransactionsFailed",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionDateRow;",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$CardTransactionRow;",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactions;",
        "Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow$LoadMoreTransactionsFailed;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 247
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/BalanceTransactionsCoordinator$TransactionRow;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getId()J
.end method
