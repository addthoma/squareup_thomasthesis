.class public Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
.super Ljava/lang/Object;
.source "TransferResultScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation


# static fields
.field public static final SENTINEL:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;


# instance fields
.field public final accountNumberSuffix:Ljava/lang/String;

.field public final bankName:Ljava/lang/String;

.field public final brandNameId:I

.field public final desiredDeposit:Lcom/squareup/protos/common/Money;

.field public final instantDepositRequiresLinkedCard:Z

.field public final isInstant:Z

.field public final message:Ljava/lang/String;

.field public final panSuffix:Ljava/lang/String;

.field public final requestState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

.field public final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    invoke-static {}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->asSentinel()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->SENTINEL:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->requestState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    .line 75
    iput-boolean p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->isInstant:Z

    .line 76
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    .line 77
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->title:Ljava/lang/String;

    .line 78
    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->message:Ljava/lang/String;

    .line 79
    iput-boolean p6, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->instantDepositRequiresLinkedCard:Z

    const/4 p1, 0x0

    if-nez p7, :cond_0

    move-object p2, p1

    goto :goto_0

    .line 80
    :cond_0
    iget-object p2, p7, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    :goto_0
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->bankName:Ljava/lang/String;

    if-nez p7, :cond_1

    move-object p2, p1

    goto :goto_1

    .line 81
    :cond_1
    iget-object p2, p7, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    :goto_1
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->accountNumberSuffix:Ljava/lang/String;

    if-nez p8, :cond_2

    const/4 p2, -0x1

    goto :goto_2

    .line 82
    :cond_2
    iget-object p2, p8, Lcom/squareup/protos/client/deposits/CardInfo;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 83
    invoke-static {p2}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p2

    iget p2, p2, Lcom/squareup/text/CardBrandResources;->brandNameId:I

    :goto_2
    iput p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->brandNameId:I

    if-nez p8, :cond_3

    goto :goto_3

    .line 84
    :cond_3
    iget-object p1, p8, Lcom/squareup/protos/client/deposits/CardInfo;->pan_suffix:Ljava/lang/String;

    :goto_3
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->panSuffix:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;)V
    .locals 9

    .line 67
    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->depositStatus:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    iget-object v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferSpeed:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    sget-object v2, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;->INSTANT:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$TransferSpeed;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->transferAmount:Lcom/squareup/protos/common/Money;

    iget-object v4, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->title:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$State;->message:Ljava/lang/String;

    move-object v0, p0

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;)V

    return-void
.end method

.method private static asSentinel()Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;
    .locals 10

    .line 88
    new-instance v9, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;

    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->BUILDING:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    sget-object v0, Lcom/squareup/protos/common/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v2, 0x0

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;)V

    return-object v9
.end method
