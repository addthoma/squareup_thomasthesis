.class abstract Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action;
.super Ljava/lang/Object;
.source "AddMoneyWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;,
        Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "FinishWith",
        "SubmitEnteredAmount",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;",
        "Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/AddMoneyResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
            ">;)",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;

    if-eqz v0, :cond_0

    move-object p1, p0

    check-cast p1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;

    invoke-virtual {p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$FinishWith;->getResult()Lcom/squareup/ui/balance/addmoney/AddMoneyResult;

    move-result-object p1

    goto :goto_0

    .line 134
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;

    if-eqz v0, :cond_1

    .line 135
    new-instance v0, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;

    invoke-virtual {v1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action$SubmitEnteredAmount;->getCardInfo()Lcom/squareup/protos/client/deposits/CardInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/deposits/CardInfo;->instrument_token:Ljava/lang/String;

    const-string v3, "cardInfo.instrument_token"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/balance/addmoney/AddMoneyState$SubmittingAmount;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    :goto_0
    return-object p1

    .line 136
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 130
    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/addmoney/AddMoneyWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/ui/balance/addmoney/AddMoneyResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyState;",
            "-",
            "Lcom/squareup/ui/balance/addmoney/AddMoneyResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
