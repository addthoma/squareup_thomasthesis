.class final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivityRequester.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "+",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivityRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivityRequester.kt\ncom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,251:1\n1360#2:252\n1429#2,3:253\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardActivityRequester.kt\ncom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1\n*L\n57#1:252\n57#1,3:253\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    .line 56
    invoke-virtual {v1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getCardActivities()Ljava/util/List;

    move-result-object p1

    .line 57
    check-cast p1, Ljava/lang/Iterable;

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 253
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 254
    check-cast v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    .line 58
    iget-object v4, v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;->transaction_token:Ljava/lang/String;

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 59
    invoke-virtual {v3}, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->newBuilder()Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;

    move-result-object v3

    .line 60
    iget-object v4, v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;->is_personal_expense:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;

    move-result-object v3

    .line 61
    invoke-virtual {v3}, Lcom/squareup/protos/client/bizbank/CardActivityEvent$Builder;->build()Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    move-result-object v3

    .line 64
    :cond_0
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 255
    :cond_1
    move-object v5, v2

    check-cast v5, Ljava/util/List;

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;->this$0:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->access$getCardActivityState$p(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->copy$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
