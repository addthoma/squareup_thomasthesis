.class public Lcom/squareup/ui/buyercart/BuyerCartAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "BuyerCartAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/buyercart/BuyerCartViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final displayItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
            ">;"
        }
    .end annotation
.end field

.field private lastFooter:Landroid/view/View;

.field private lastHeader:Landroid/view/View;

.field private lastHeaderLoc:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 28
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 24
    iput-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeaderLoc:[I

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    const/4 v0, 0x1

    .line 29
    invoke-virtual {p0, v0}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemDataPositionById(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    .line 91
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;

    .line 94
    invoke-virtual {v1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem$AbstractBuyerCartItem;->getDisplayItem()Lcom/squareup/comms/protos/seller/DisplayItem;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/comms/protos/seller/DisplayItem;->client_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public getItemId(I)J
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;->getId()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;

    invoke-virtual {p1}, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;->getLayoutId()I

    move-result p1

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->onBindViewHolder(Lcom/squareup/ui/buyercart/BuyerCartViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/buyercart/BuyerCartViewHolder;I)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;->bind(Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;)V

    .line 62
    instance-of p2, p1, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;

    if-eqz p2, :cond_0

    .line 63
    iget-object p1, p1, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;->itemView:Landroid/view/View;

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeader:Landroid/view/View;

    goto :goto_0

    .line 64
    :cond_0
    instance-of p2, p1, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartFooterViewHolder;

    if-eqz p2, :cond_1

    .line 65
    iget-object p1, p1, Lcom/squareup/ui/buyercart/BuyerCartViewHolder;->itemView:Landroid/view/View;

    iput-object p1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastFooter:Landroid/view/View;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->tweakFooterVisibility()V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/buyercart/BuyerCartViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/buyercart/BuyerCartViewHolder;
    .locals 2

    .line 41
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    .line 42
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 43
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_footer:I

    if-ne p2, v0, :cond_0

    .line 44
    new-instance p2, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartFooterViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartFooterViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 45
    :cond_0
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_header:I

    if-ne p2, v0, :cond_1

    .line 46
    new-instance p2, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranCartHeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 47
    :cond_1
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_list_item:I

    if-ne p2, v0, :cond_2

    .line 48
    new-instance p2, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder$BranCartItemViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 49
    :cond_2
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->bran_cart_section_header:I

    if-ne p2, v0, :cond_3

    .line 50
    new-instance p2, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranSectionHeaderViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BranSectionHeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 51
    :cond_3
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->buyer_cart_list_header:I

    if-ne p2, v0, :cond_4

    .line 52
    new-instance p2, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BuyerCartHeaderViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/BuyerCartViewHolder$BuyerCartHeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 53
    :cond_4
    sget v0, Lcom/squareup/ui/buyercart/R$layout;->buyer_cart_list_item:I

    if-ne p2, v0, :cond_5

    .line 54
    new-instance p2, Lcom/squareup/ui/buyercart/CartItemViewHolder$BuyerCartItemViewHolder;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyercart/CartItemViewHolder$BuyerCartItemViewHolder;-><init>(Landroid/view/View;)V

    return-object p2

    .line 56
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "wrong id"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDisplayItems(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/buyercart/BuyerCartAdapterItem;",
            ">;)V"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/buyercart/BuyerCartDiff;

    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/buyercart/BuyerCartDiff;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 34
    invoke-static {v0}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 36
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->displayItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 37
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method

.method public tweakFooterVisibility()V
    .locals 3

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastFooter:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeader:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeaderLoc:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeader:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastHeaderLoc:[I

    aget v0, v0, v2

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 86
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyercart/BuyerCartAdapter;->lastFooter:Landroid/view/View;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return-void
.end method
