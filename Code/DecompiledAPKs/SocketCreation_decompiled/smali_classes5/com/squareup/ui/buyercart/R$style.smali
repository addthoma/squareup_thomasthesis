.class public final Lcom/squareup/ui/buyercart/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyercart/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final Display:I = 0x7f1300f5

.field public static final Label:I = 0x7f130128

.field public static final Label_LightGray:I = 0x7f130129

.field public static final Label_LightGray_BuyerCart:I = 0x7f13012a

.field public static final Label_White:I = 0x7f13012b

.field public static final LightDisplay:I = 0x7f13012d

.field public static final LightDisplay_DarkGray:I = 0x7f13012e

.field public static final LightDisplay_LightGray:I = 0x7f13012f

.field public static final LightDisplay_White:I = 0x7f130130

.field public static final LightDisplay_White_Light:I = 0x7f130131

.field public static final LightTitle:I = 0x7f130132

.field public static final LightTitle_DarkGray:I = 0x7f130133

.field public static final LightTitle_White:I = 0x7f130134

.field public static final MediumTitle:I = 0x7f130142

.field public static final MediumTitle_Blue:I = 0x7f130143

.field public static final MediumTitle_DarkGray:I = 0x7f130144

.field public static final MediumTitle_DarkGray_LightGrayHint:I = 0x7f130145

.field public static final MediumTitle_LightGray:I = 0x7f130146

.field public static final MediumTitle_White:I = 0x7f130147

.field public static final RegularDisplay:I = 0x7f130170

.field public static final RegularDisplay_DarkGray:I = 0x7f130171

.field public static final RegularDisplay_LightGray:I = 0x7f130172

.field public static final RegularDisplay_White:I = 0x7f130173

.field public static final RegularTitle:I = 0x7f130174

.field public static final RegularTitle_DarkGray:I = 0x7f130175

.field public static final RegularTitle_DarkGray_LightGrayHint:I = 0x7f130176

.field public static final RegularTitle_LightGray:I = 0x7f130177

.field public static final RegularTitle_White:I = 0x7f130178

.field public static final RegularTitle_White_BuyerCart:I = 0x7f130179

.field public static final Title:I = 0x7f130371


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
