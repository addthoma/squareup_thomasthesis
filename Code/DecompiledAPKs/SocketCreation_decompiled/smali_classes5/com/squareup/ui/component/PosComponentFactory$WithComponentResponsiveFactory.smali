.class final Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;
.super Ljava/lang/Object;
.source "PosComponentFactory.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/component/PosComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WithComponentResponsiveFactory"
.end annotation


# instance fields
.field private final phoneComponentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private final tabletComponentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;->phoneComponentClass:Ljava/lang/Class;

    .line 97
    iput-object p2, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;->tabletComponentClass:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    .line 101
    const-class p2, Lcom/squareup/ui/component/ComponentFactoryComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/component/ComponentFactoryComponent;

    .line 102
    invoke-interface {p2}, Lcom/squareup/ui/component/ComponentFactoryComponent;->device()Lcom/squareup/util/Device;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 104
    iget-object p2, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;->tabletComponentClass:Ljava/lang/Class;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->createChildComponent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 106
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/component/PosComponentFactory$WithComponentResponsiveFactory;->phoneComponentClass:Ljava/lang/Class;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->createChildComponent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
