.class public Lcom/squareup/ui/NfcProcessor;
.super Ljava/lang/Object;
.source "NfcProcessor.java"

# interfaces
.implements Lcom/squareup/ui/NfcProcessorInterface;
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/cardreader/NfcListener;
.implements Lcom/squareup/cardreader/PaymentCompletionListener;
.implements Lcom/squareup/cardreader/PinRequestListener;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;,
        Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;,
        Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;,
        Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;,
        Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;,
        Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;,
        Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;,
        Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;,
        Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
    }
.end annotation


# static fields
.field private static final NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

.field private static final ONE_CENT:J = 0x1L

.field private static final TIMEOUT_INTERVAL_AFTER_SCREEN_TRANSITION:J = 0x1f4L


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final authDelegate:Lcom/squareup/ui/StackedDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/StackedDelegate<",
            "Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

.field private final clock:Lcom/squareup/util/Clock;

.field private final defaultErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final features:Lcom/squareup/settings/server/Features;

.field private inactiveContactlessReaders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lcom/squareup/ui/StackedDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/StackedDelegate<",
            "Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

.field private originalPinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

.field private readerWarningScreenAlreadyRequested:Z

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/StackedDelegate<",
            "Lcom/squareup/ui/main/SmartPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final sonicBrandingAudioPlayer:Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;

.field private statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final timeoutReadersRunnable:Ljava/lang/Runnable;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 833
    new-instance v0, Lcom/squareup/ui/NfcProcessor$3;

    invoke-direct {v0}, Lcom/squareup/ui/NfcProcessor$3;-><init>()V

    sput-object v0, Lcom/squareup/ui/NfcProcessor;->NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/squareup/util/Clock;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v1, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;

    invoke-direct {v1, p0}, Lcom/squareup/ui/NfcProcessor$DefaultErrorHandler;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->defaultErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    .line 143
    new-instance v1, Lcom/squareup/ui/StackedDelegate;

    const/4 v2, 0x1

    new-array v3, v2, [Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    new-instance v4, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;

    invoke-direct {v4, p0}, Lcom/squareup/ui/NfcProcessor$DefaultNfcAuthDelegate;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-direct {v1, v3}, Lcom/squareup/ui/StackedDelegate;-><init>([Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->authDelegate:Lcom/squareup/ui/StackedDelegate;

    .line 145
    new-instance v1, Lcom/squareup/ui/StackedDelegate;

    new-array v3, v2, [Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    new-instance v4, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;

    invoke-direct {v4, p0}, Lcom/squareup/ui/NfcProcessor$DefaultNfcListenerOverrider;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    aput-object v4, v3, v5

    invoke-direct {v1, v3}, Lcom/squareup/ui/StackedDelegate;-><init>([Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    .line 147
    new-instance v1, Lcom/squareup/ui/StackedDelegate;

    new-array v2, v2, [Lcom/squareup/ui/main/SmartPaymentResultHandler;

    new-instance v3, Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/squareup/ui/NfcProcessor$DefaultSmartPaymentResultHandler;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/NfcProcessor$1;)V

    aput-object v3, v2, v5

    invoke-direct {v1, v2}, Lcom/squareup/ui/StackedDelegate;-><init>([Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    .line 153
    iget-object v1, v0, Lcom/squareup/ui/NfcProcessor;->defaultErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    move-object v1, p1

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    move-object v1, p2

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object v1, p3

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p4

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p5

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->res:Lcom/squareup/util/Res;

    move-object v1, p6

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->clock:Lcom/squareup/util/Clock;

    move-object v1, p7

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p8

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object v1, p9

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object/from16 v1, p10

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    move-object/from16 v1, p11

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object/from16 v1, p12

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    move-object/from16 v1, p13

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object/from16 v1, p14

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    move-object/from16 v1, p15

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    move-object/from16 v1, p16

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    move-object/from16 v1, p17

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    move-object/from16 v1, p18

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    move-object/from16 v1, p19

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object/from16 v1, p20

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 189
    sget-object v1, Lcom/squareup/ui/NfcProcessor;->NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 190
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    .line 191
    new-instance v1, Lcom/squareup/ui/-$$Lambda$XIeIuRABX_pnjU-ZnaDoDBQGRvg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/-$$Lambda$XIeIuRABX_pnjU-ZnaDoDBQGRvg;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->timeoutReadersRunnable:Ljava/lang/Runnable;

    move-object/from16 v1, p21

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/NfcProcessor;->sonicBrandingAudioPlayer:Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;

    return-void
.end method

.method static synthetic access$102(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/ui/NfcProcessor;->defaultErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/NfcProcessor;Z)V
    .locals 0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/ui/NfcProcessor;->startPaymentOnInactiveContactlessReaders(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/ui/NfcProcessor;->showErrorScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/payment/TenderInEdit;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/main/SmartPaymentFlowStarter;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/cardreader/CardReaderListeners;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    return-object p0
.end method

.method private cancelPaymentOnContactlessReader(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 765
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 766
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 770
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    .line 771
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 772
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 774
    :cond_1
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 775
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 776
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method

.method private performPaymentTerminationTasks(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
    .locals 4

    .line 811
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/payment/Transaction;->dropPaymentOrTender(ZLcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 812
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    .line 815
    iget-boolean v0, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 818
    iput-boolean p1, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    return-void

    .line 822
    :cond_0
    iput-boolean v2, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    .line 823
    invoke-static {p2}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object p2

    .line 825
    new-instance v0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;

    .line 826
    invoke-virtual {p2}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/NfcProcessor;->res:Lcom/squareup/util/Res;

    .line 827
    invoke-virtual {p2, v2}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;->getMessage(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    invoke-direct {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    .line 830
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private showErrorScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 795
    iget-boolean v0, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    if-eqz v0, :cond_0

    return-void

    .line 801
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    const/4 v0, 0x1

    .line 803
    iput-boolean v0, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    .line 805
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    .line 806
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private startPaymentOnAllContactlessReaders()V
    .locals 1

    .line 756
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 757
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 760
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/NfcProcessor;->updateListOfInactiveReaders()V

    .line 761
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->startPaymentOnInactiveContactlessReaders()V

    :cond_1
    :goto_0
    return-void
.end method

.method private startPaymentOnInactiveContactlessReaders(Z)V
    .locals 2

    .line 373
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    if-eqz p1, :cond_0

    .line 375
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p1}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;->setPaymentCompletionListener()V

    .line 377
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 378
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_2

    .line 382
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getGrandTotal()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 384
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isAmountWithinRange()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    .line 387
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p1

    .line 388
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 389
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 388
    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    .line 390
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->paymentIsWithinRange()Z

    move-result v0

    if-nez v0, :cond_4

    return-void

    .line 394
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    sget-object v1, Lcom/squareup/ui/NfcProcessor;->NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    if-eq v0, v1, :cond_5

    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    :goto_1
    const-string v1, "The status display is unset!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 396
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/NfcProcessor;->startPaymentWithAmount(J)V

    :cond_6
    :goto_2
    return-void
.end method

.method private startPaymentWithAmount(J)V
    .locals 9

    .line 428
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    const-string v1, "NfcProcessor::startPaymentWithAmount can\'t use contactless reader"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 431
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 432
    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderId;

    .line 433
    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v3, v2}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v3

    .line 434
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v4

    .line 436
    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 440
    :cond_0
    iget-object v5, p0, Lcom/squareup/ui/NfcProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v5, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 441
    iget-object v5, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v6, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_PAYMENT:Lcom/squareup/analytics/ReaderEventName;

    sget-object v7, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v8, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 442
    invoke-virtual {v8}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v8

    .line 441
    invoke-virtual {v5, v4, v6, v7, v8}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 443
    iget-object v4, p0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->ZERO_ARQC_TEST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-wide/16 v4, 0x1

    goto :goto_1

    :cond_1
    move-wide v4, p1

    .line 444
    :goto_1
    iget-object v6, p0, Lcom/squareup/ui/NfcProcessor;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v6}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v6

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/squareup/cardreader/CardReader;->startPayment(JJ)V

    .line 445
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 446
    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-interface {v3, v2}, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;->contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V

    goto :goto_0

    .line 449
    :cond_2
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardreader/CardReaderId;

    .line 450
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-void
.end method

.method private startRefundWithAmount(J)V
    .locals 9

    .line 455
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    const-string v1, "NfcProcessor::startRefundWithAmount can\'t use contactless reader"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 458
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 459
    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderId;

    .line 460
    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v3, v2}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v3

    .line 461
    invoke-interface {v3}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v4

    .line 463
    invoke-virtual {v4}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    .line 467
    :cond_0
    iget-object v5, p0, Lcom/squareup/ui/NfcProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v5, v2}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 468
    iget-object v5, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v6, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_STARTING_REFUND:Lcom/squareup/analytics/ReaderEventName;

    sget-object v7, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v8, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 469
    invoke-virtual {v8}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v8

    .line 468
    invoke-virtual {v5, v4, v6, v7, v8}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 470
    iget-object v4, p0, Lcom/squareup/ui/NfcProcessor;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v4}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v4

    invoke-interface {v3, p1, p2, v4, v5}, Lcom/squareup/cardreader/CardReader;->startRefund(JJ)V

    .line 471
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 474
    :cond_1
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/cardreader/CardReaderId;

    .line 475
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-void
.end method

.method private updateListOfInactiveReaders()V
    .locals 8

    .line 780
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 781
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 782
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 783
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    .line 784
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v3

    const/4 v4, 0x1

    xor-int/2addr v3, v4

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const-string v7, "Reader %s has a card present. We shouldn\'t start an NFC payment on it."

    invoke-static {v3, v7, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 787
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v2

    xor-int/2addr v2, v4

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v6

    const-string v4, "Reader %s is already processing a payment"

    invoke-static {v2, v4, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 788
    iget-object v2, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public areContactlessReadersReadyForPayments()Z
    .locals 4

    .line 279
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReader;

    .line 284
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 286
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 287
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 288
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->hasValidTmsCountryCode()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    return v1
.end method

.method public canUseContactlessReader()Z
    .locals 4

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 263
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReader;

    .line 268
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 269
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    :goto_0
    return v1
.end method

.method public cancelPaymentOnAllContactlessReaders()V
    .locals 4

    .line 480
    sget-object v0, Lcom/squareup/ui/NfcProcessor;->NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    iput-object v0, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 481
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 482
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 483
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484
    invoke-direct {p0, v1}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnContactlessReader(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    .line 487
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    invoke-interface {v0}, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;->unsetPaymentCompletionListener()V

    .line 488
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->originalPinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    .line 489
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->timeoutReadersRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method cancelPaymentOnOtherContactlessReaders(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 4

    .line 493
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 494
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    .line 495
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 496
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/cardreader/CardReaderId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 497
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 498
    invoke-direct {p0, v1}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnContactlessReader(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public continueMonitoring()V
    .locals 2

    .line 338
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->timeoutReadersRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
    .locals 2

    .line 593
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->sonicBrandingAudioPlayer:Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;

    invoke-interface {v0, p1}, Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;->playSonicBrandingAudio(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    goto :goto_0

    .line 596
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "onAudioVisualRequest is requested when READER_FW_SONIC_BRANDING is not enabled."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 505
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 507
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 510
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-interface {v0, p1}, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;->contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V

    :cond_0
    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 516
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 518
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 521
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-interface {v0, p1}, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;->contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/NfcProcessor;->originalPinRequestListener:Lcom/squareup/cardreader/PinRequestListener;

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setNfcListener(Lcom/squareup/cardreader/NfcListener;)V

    .line 199
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 200
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->sonicBrandingAudioPlayer:Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;

    invoke-interface {p1}, Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;->initialize()V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 205
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnAllContactlessReaders()V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetNfcListener()V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->sonicBrandingAudioPlayer:Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;

    invoke-interface {v0}, Lcom/squareup/sonicbranding/SonicBrandingAudioPlayer;->shutDown()V

    :cond_0
    return-void
.end method

.method public onHardwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 2

    .line 858
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 862
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnOtherContactlessReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 864
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 865
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContactlessTenderHardwarePinRequest(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/securetouch/SecureTouchPinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowHardwarePinRequested;

    move-result-object p1

    .line 867
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p2}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-interface {p2, p1}, Lcom/squareup/ui/main/SmartPaymentResultHandler;->handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method public onNfcActionRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 624
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 626
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 625
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 627
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleActionRequired()V

    return-void
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 576
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p3

    if-nez p3, :cond_0

    return-void

    .line 580
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p0, p3}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnOtherContactlessReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 582
    new-instance p3, Lcom/squareup/ui/NfcAuthData;

    invoke-direct {p3, p1, p2}, Lcom/squareup/ui/NfcAuthData;-><init>(Lcom/squareup/cardreader/CardReaderInfo;[B)V

    .line 585
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->authDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p1}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;

    invoke-interface {p1, p3}, Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;->onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V

    return-void
.end method

.method public onNfcCardBlocked(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 603
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 605
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 604
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 607
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleCardBlocked()V

    return-void
.end method

.method public onNfcCardDeclined(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 673
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 674
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 675
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 674
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 676
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleCardDeclined()V

    return-void
.end method

.method public onNfcCollisionDetected(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 640
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 642
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 641
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 643
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleCollisionDetected()V

    return-void
.end method

.method public onNfcInterfaceUnavailable(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 548
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 550
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 551
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 550
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 553
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleInterfaceUnavailable()V

    return-void
.end method

.method public onNfcLimitExceededInsertCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 655
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 658
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 656
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 659
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleLimitExceededInsertCard()V

    return-void
.end method

.method public onNfcLimitExceededTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 647
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 650
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 648
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 651
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleLimitExceededTryAnotherCard()V

    return-void
.end method

.method public onNfcPresentCardAgain(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 612
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 613
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 618
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 617
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 619
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleCardTapAgain()V

    return-void
.end method

.method public onNfcProcessingError(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 681
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 682
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 686
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 685
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 687
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleProcessingError()V

    return-void
.end method

.method public onNfcTimedOut(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 3

    .line 533
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 534
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/analytics/ReaderEventName;

    iget-object v2, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 539
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 538
    invoke-virtual {v0, p1, v1, p2, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 541
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    .line 542
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnContactlessReader(Lcom/squareup/cardreader/CardReader;)V

    .line 543
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    invoke-interface {p2, p1}, Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;->contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public onNfcTryAnotherCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 558
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 559
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnOtherContactlessReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 561
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 563
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 561
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 565
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleTryAnotherCard()V

    return-void
.end method

.method public onNfcUnlockDevice(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 632
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 634
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 633
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 635
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleUnlockDevice()V

    return-void
.end method

.method public onPaymentApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    if-eqz p4, :cond_1

    .line 697
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p3}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result p3

    xor-int/lit8 p3, p3, 0x1

    const-string p4, "NfcProcessor::onPaymentApproved no active card reader"

    invoke-static {p3, p4}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 701
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p3}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result p3

    if-nez p3, :cond_0

    .line 702
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p3

    .line 704
    iget-object p4, p0, Lcom/squareup/ui/NfcProcessor;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p4}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object p4

    .line 705
    invoke-virtual {p3}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 706
    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {p4, p3}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setSmartCardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 708
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {p3, p4}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 711
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 712
    invoke-virtual {p3, p1, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getBuyerFlowWithOfflineApprovalResult(Lcom/squareup/cardreader/CardReaderInfo;[B)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p2

    .line 713
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p3}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-interface {p3, p2}, Lcom/squareup/ui/main/SmartPaymentResultHandler;->handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    .line 715
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 716
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object p3, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED_OFFLINE:Lcom/squareup/analytics/ReaderEventName;

    iget-object p4, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 717
    invoke-virtual {p4}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p4

    .line 716
    invoke-virtual {p2, p1, p3, p5, p4}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    return-void

    .line 694
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "How can you get payment approval before auth with NFC?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 2

    .line 727
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 728
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p2

    if-nez p2, :cond_0

    return-void

    .line 731
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v0, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/analytics/ReaderEventName;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 732
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 731
    invoke-virtual {p2, p1, v0, p4, v1}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 734
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderPowerMonitor:Lcom/squareup/cardreader/CardReaderPowerMonitor;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 735
    sget-object p1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_DECLINED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/NfcProcessor;->performPaymentTerminationTasks(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public onPaymentReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 0

    .line 722
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "How can you get payment reversed before auth with NFC?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
    .locals 3

    .line 740
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 741
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 744
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/analytics/ReaderEventName;

    iget-object v2, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 745
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 744
    invoke-virtual {v0, p1, v1, p3, v2}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 747
    sget-object p1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/NfcProcessor;->performPaymentTerminationTasks(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    return-void
.end method

.method public onPaymentTerminatedDueToSwipe(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 752
    sget-object p2, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    sget-object v0, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/ui/NfcProcessor;->onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onRequestTapCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 663
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    .line 664
    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 665
    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isOtherActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_MUST_TAP:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    .line 667
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 666
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 668
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->nfcErrorHandler:Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;->handleOnRequestTapCard()V

    return-void
.end method

.method public onSoftwarePinRequested(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/PinRequestData;)V
    .locals 2

    .line 872
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 876
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/NfcProcessor;->cancelPaymentOnOtherContactlessReaders(Lcom/squareup/cardreader/CardReaderId;)V

    .line 880
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {p1}, Lcom/squareup/payment/TipDeterminerFactory;->usePreAuthTipping()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 881
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 883
    invoke-virtual {p1, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getPreAuthContactlessSingleTenderPinRequest(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 884
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p2}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-interface {p2, p1}, Lcom/squareup/ui/main/SmartPaymentResultHandler;->handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    goto :goto_0

    .line 886
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    .line 887
    invoke-virtual {p1, p2}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getContactlessTenderPinRequest(Lcom/squareup/cardreader/PinRequestData;)Lcom/squareup/ui/main/SmartPaymentResult$BuyerFlowPinRequested;

    move-result-object p1

    .line 888
    iget-object p2, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p2}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/main/SmartPaymentResultHandler;

    invoke-interface {p2, p1}, Lcom/squareup/ui/main/SmartPaymentResultHandler;->handleSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    :goto_0
    return-void
.end method

.method final peekInactiveContactlessReaders()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation

    .line 852
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->inactiveContactlessReaders:Ljava/util/Set;

    return-object v0
.end method

.method public registerErrorListenerForScope(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V
    .locals 1

    const-string v0, "scope"

    .line 244
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "nfcErrorHandler"

    .line 245
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 246
    new-instance v0, Lcom/squareup/ui/NfcProcessor$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/NfcProcessor$1;-><init>(Lcom/squareup/ui/NfcProcessor;Lcom/squareup/ui/NfcProcessor$NfcErrorHandler;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public registerNfcAuthDelegate(Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->authDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Ljava/lang/Object;)V

    return-void
.end method

.method public registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->authDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Lmortar/MortarScope;Ljava/lang/Object;)V

    return-void
.end method

.method public registerNfcListener(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Lmortar/MortarScope;Ljava/lang/Object;)V

    return-void
.end method

.method public registerSmartPaymentResultDelegate(Lcom/squareup/ui/main/SmartPaymentResultHandler;)V
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Ljava/lang/Object;)V

    return-void
.end method

.method public setStatusDisplay(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    return-void
.end method

.method public startMonitoring(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V
    .locals 2

    const/4 v0, 0x0

    .line 304
    iput-boolean v0, p0, Lcom/squareup/ui/NfcProcessor;->readerWarningScreenAlreadyRequested:Z

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->timeoutReadersRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 306
    iput-object p1, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 308
    iget-object p1, p0, Lcom/squareup/ui/NfcProcessor;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->hasPaymentStartedOnContactlessReader()Z

    move-result p1

    if-nez p1, :cond_0

    .line 309
    invoke-direct {p0}, Lcom/squareup/ui/NfcProcessor;->startPaymentOnAllContactlessReaders()V

    :cond_0
    return-void
.end method

.method public startMonitoringWithAutoFieldRestart()V
    .locals 1

    .line 314
    new-instance v0, Lcom/squareup/ui/NfcProcessor$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/NfcProcessor$2;-><init>(Lcom/squareup/ui/NfcProcessor;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/NfcProcessor;->startMonitoring(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    return-void
.end method

.method public startPaymentOnAllContactlessReadersWithAmount(J)V
    .locals 1

    .line 404
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 408
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    invoke-interface {v0}, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;->setPaymentCompletionListener()V

    .line 409
    invoke-direct {p0}, Lcom/squareup/ui/NfcProcessor;->updateListOfInactiveReaders()V

    .line 410
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/NfcProcessor;->startPaymentWithAmount(J)V

    :cond_1
    :goto_0
    return-void
.end method

.method public startPaymentOnInactiveContactlessReaders()V
    .locals 1

    const/4 v0, 0x1

    .line 369
    invoke-direct {p0, v0}, Lcom/squareup/ui/NfcProcessor;->startPaymentOnInactiveContactlessReaders(Z)V

    return-void
.end method

.method public startRefundOnAllContactlessReadersWithAmount(JLcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)Z
    .locals 0

    .line 416
    iput-object p3, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 417
    invoke-virtual {p0}, Lcom/squareup/ui/NfcProcessor;->canUseContactlessReader()Z

    move-result p3

    if-eqz p3, :cond_1

    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p3}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    .line 421
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/NfcProcessor;->listener:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {p3}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;

    invoke-interface {p3}, Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;->setPaymentCompletionListener()V

    .line 422
    invoke-direct {p0}, Lcom/squareup/ui/NfcProcessor;->updateListOfInactiveReaders()V

    .line 423
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/NfcProcessor;->startRefundWithAmount(J)V

    const/4 p1, 0x1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public stopMonitoringSoon()V
    .locals 4

    .line 353
    sget-object v0, Lcom/squareup/ui/NfcProcessor;->NOOP_STATUS_DISPLAY:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    iput-object v0, p0, Lcom/squareup/ui/NfcProcessor;->statusDisplay:Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/ui/NfcProcessor;->timeoutReadersRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public unregisterNfcAuthDelegate(Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->authDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->unregisterDelegate(Ljava/lang/Object;)V

    return-void
.end method

.method public unregisterSmartPaymentResultDelegate(Lcom/squareup/ui/main/SmartPaymentResultHandler;)V
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/ui/NfcProcessor;->smartPaymentResultDelegate:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->unregisterDelegate(Ljava/lang/Object;)V

    return-void
.end method
