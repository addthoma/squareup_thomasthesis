.class synthetic Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;
.super Ljava/lang/Object;
.source "InstantDepositsResultPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 59
    invoke-static {}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->values()[Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_SUCCEEDED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->DEPOSIT_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/instantdeposits/InstantDepositsResultPresenter$1;->$SwitchMap$com$squareup$instantdeposit$InstantDepositRunner$InstantDepositState:[I

    sget-object v1, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->SERVER_CALL_FAILED:Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;

    invoke-virtual {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
