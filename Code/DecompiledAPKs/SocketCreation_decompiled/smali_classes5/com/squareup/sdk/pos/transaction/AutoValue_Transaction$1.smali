.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction$1;
.super Ljava/lang/Object;
.source "AutoValue_Transaction.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;
    .locals 8

    .line 15
    new-instance v7, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;

    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-class v0, Lcom/squareup/sdk/pos/transaction/DateTime;

    .line 19
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/sdk/pos/transaction/DateTime;

    const-class v0, Lcom/squareup/sdk/pos/transaction/Order;

    .line 20
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/sdk/pos/transaction/Order;

    .line 21
    invoke-static {}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;->access$000()Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/sdk/pos/transaction/TendersTypeAdapter;->fromParcel(Landroid/os/Parcel;)Ljava/util/Set;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Order;Ljava/util/Set;)V

    return-object v7
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;
    .locals 0

    .line 26
    new-array p1, p1, [Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction$1;->newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;

    move-result-object p1

    return-object p1
.end method
