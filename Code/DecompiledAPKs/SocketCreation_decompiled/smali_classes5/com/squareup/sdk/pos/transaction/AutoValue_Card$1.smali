.class final Lcom/squareup/sdk/pos/transaction/AutoValue_Card$1;
.super Ljava/lang/Object;
.source "AutoValue_Card.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/AutoValue_Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/sdk/pos/transaction/AutoValue_Card;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Card;
    .locals 2

    .line 12
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Card;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/sdk/pos/transaction/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Card$Brand;

    move-result-object v1

    .line 14
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Card;-><init>(Lcom/squareup/sdk/pos/transaction/Card$Brand;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Card$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_Card;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Card;
    .locals 0

    .line 19
    new-array p1, p1, [Lcom/squareup/sdk/pos/transaction/AutoValue_Card;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_Card$1;->newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_Card;

    move-result-object p1

    return-object p1
.end method
