.class public interface abstract Lcom/squareup/sdk/pos/PosClient;
.super Ljava/lang/Object;
.source "PosClient.java"


# virtual methods
.method public abstract createTransactionIntent(Lcom/squareup/sdk/pos/TransactionRequest;)Landroid/content/Intent;
.end method

.method public abstract isPointOfSaleInstalled()Z
.end method

.method public abstract launchPointOfSale()V
.end method

.method public abstract openPointOfSalePlayStoreListing()V
.end method

.method public abstract parseTransactionError(Landroid/content/Intent;)Lcom/squareup/sdk/pos/TransactionRequest$Error;
.end method

.method public abstract parseTransactionSuccess(Landroid/content/Intent;)Lcom/squareup/sdk/pos/TransactionRequest$Success;
.end method
