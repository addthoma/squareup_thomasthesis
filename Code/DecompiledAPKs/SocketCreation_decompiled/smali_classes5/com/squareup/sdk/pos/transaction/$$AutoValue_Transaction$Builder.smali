.class final Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;
.super Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
.source "$$AutoValue_Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private autoTenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private clientId:Ljava/lang/String;

.field private createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

.field private locationId:Ljava/lang/String;

.field private order:Lcom/squareup/sdk/pos/transaction/Order;

.field private serverId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/sdk/pos/transaction/Transaction;)V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/sdk/pos/transaction/Transaction$Builder;-><init>()V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->clientId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->clientId:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->serverId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->serverId:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->locationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->locationId:Ljava/lang/String;

    .line 145
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->createdAt()Lcom/squareup/sdk/pos/transaction/DateTime;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->order()Lcom/squareup/sdk/pos/transaction/Order;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->order:Lcom/squareup/sdk/pos/transaction/Order;

    .line 147
    invoke-virtual {p1}, Lcom/squareup/sdk/pos/transaction/Transaction;->autoTenders()Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->autoTenders:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/pos/transaction/Transaction;Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$1;)V
    .locals 0

    .line 132
    invoke-direct {p0, p1}, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;-><init>(Lcom/squareup/sdk/pos/transaction/Transaction;)V

    return-void
.end method


# virtual methods
.method autoBuild()Lcom/squareup/sdk/pos/transaction/Transaction;
    .locals 9

    .line 204
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->clientId:Ljava/lang/String;

    const-string v1, ""

    if-nez v0, :cond_0

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " clientId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->locationId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " locationId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    if-nez v0, :cond_2

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " createdAt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->order:Lcom/squareup/sdk/pos/transaction/Order;

    if-nez v0, :cond_3

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " order"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->autoTenders:Ljava/util/Set;

    if-nez v0, :cond_4

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " autoTenders"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 219
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 222
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;

    iget-object v3, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->clientId:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->serverId:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->locationId:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    iget-object v7, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->order:Lcom/squareup/sdk/pos/transaction/Order;

    iget-object v8, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->autoTenders:Ljava/util/Set;

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/squareup/sdk/pos/transaction/AutoValue_Transaction;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/sdk/pos/transaction/DateTime;Lcom/squareup/sdk/pos/transaction/Order;Ljava/util/Set;)V

    return-object v0

    .line 220
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method autoTenders(Ljava/util/Set;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;)",
            "Lcom/squareup/sdk/pos/transaction/Transaction$Builder;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 191
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->autoTenders:Ljava/util/Set;

    return-object p0

    .line 189
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null autoTenders"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method autoTenders()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/pos/transaction/Tender;",
            ">;"
        }
    .end annotation

    .line 196
    iget-object v0, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->autoTenders:Ljava/util/Set;

    if-eqz v0, :cond_0

    return-object v0

    .line 197
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Property \"autoTenders\" has not been set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clientId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 154
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->clientId:Ljava/lang/String;

    return-object p0

    .line 152
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null clientId"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createdAt(Lcom/squareup/sdk/pos/transaction/DateTime;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 175
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->createdAt:Lcom/squareup/sdk/pos/transaction/DateTime;

    return-object p0

    .line 173
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null createdAt"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public locationId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 167
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->locationId:Ljava/lang/String;

    return-object p0

    .line 165
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null locationId"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public order(Lcom/squareup/sdk/pos/transaction/Order;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 183
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->order:Lcom/squareup/sdk/pos/transaction/Order;

    return-object p0

    .line 181
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Null order"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public serverId(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/Transaction$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/sdk/pos/transaction/$$AutoValue_Transaction$Builder;->serverId:Ljava/lang/String;

    return-object p0
.end method
