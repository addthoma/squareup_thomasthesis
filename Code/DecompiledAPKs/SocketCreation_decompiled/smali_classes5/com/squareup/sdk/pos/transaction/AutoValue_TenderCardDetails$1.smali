.class final Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails$1;
.super Ljava/lang/Object;
.source "AutoValue_TenderCardDetails.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;

    const-class v1, Lcom/squareup/sdk/pos/transaction/Card;

    .line 12
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/sdk/pos/transaction/Card;

    .line 13
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;->valueOf(Ljava/lang/String;)Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;-><init>(Lcom/squareup/sdk/pos/transaction/Card;Lcom/squareup/sdk/pos/transaction/TenderCardDetails$EntryMethod;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;
    .locals 0

    .line 18
    new-array p1, p1, [Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails$1;->newArray(I)[Lcom/squareup/sdk/pos/transaction/AutoValue_TenderCardDetails;

    move-result-object p1

    return-object p1
.end method
