.class public abstract Lcom/squareup/sdk/pos/transaction/Money;
.super Ljava/lang/Object;
.source "Money.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)Lcom/squareup/sdk/pos/transaction/Money;
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/sdk/pos/transaction/AutoValue_Money;-><init>(JLcom/squareup/sdk/pos/transaction/Money$CurrencyCode;)V

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter<",
            "Lcom/squareup/sdk/pos/transaction/Money;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/sdk/pos/transaction/$AutoValue_Money$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/sdk/pos/transaction/$AutoValue_Money$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract amount()J
.end method

.method public abstract currencyCode()Lcom/squareup/sdk/pos/transaction/Money$CurrencyCode;
.end method
