.class public interface abstract Lcom/squareup/sdk/reader/authorization/AuthorizationManager;
.super Ljava/lang/Object;
.source "AuthorizationManager.java"


# virtual methods
.method public abstract addAuthorizeCallback(Lcom/squareup/sdk/reader/authorization/AuthorizeCallback;)Lcom/squareup/sdk/reader/core/CallbackReference;
.end method

.method public abstract addDeauthorizeCallback(Lcom/squareup/sdk/reader/authorization/DeauthorizeCallback;)Lcom/squareup/sdk/reader/core/CallbackReference;
.end method

.method public abstract authorize(Ljava/lang/String;)V
.end method

.method public abstract deauthorize()V
.end method

.method public abstract getAuthorizationState()Lcom/squareup/sdk/reader/authorization/AuthorizationState;
.end method
