.class public final Lcom/squareup/sdk/reader/checkout/CheckoutResult;
.super Ljava/lang/Object;
.source "CheckoutResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    }
.end annotation


# instance fields
.field private final createdAt:Ljava/util/Date;

.field private final locationId:Ljava/lang/String;

.field private final tenders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/Tender;",
            ">;"
        }
    .end annotation
.end field

.field private final totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final transactionClientId:Ljava/lang/String;

.field private final transactionId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)V
    .locals 3

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 133
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$400(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    .line 134
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 135
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$500(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    .line 136
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$600(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    .line 137
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 138
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$700(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    .line 139
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$800(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Ljava/util/Set;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    return-void

    .line 127
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "All moneys in a CheckoutResult must have the same currency code. totalMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$200(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " but totalTipMoney currency is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-static {p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;->access$300(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/sdk/reader/checkout/Money;->getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/util/Date;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/util/Set;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/sdk/reader/checkout/CheckoutResult;)Ljava/lang/String;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method public static newBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 2

    const-string v0, "locationId"

    .line 27
    invoke-static {p0, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string/jumbo v0, "totalMoney"

    .line 28
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string/jumbo v0, "transactionClientId"

    .line 29
    invoke-static {p2, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 30
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;-><init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/Money;Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V

    return-object v0
.end method


# virtual methods
.method buildUpon()Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;
    .locals 2

    .line 122
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutResult$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutResult;Lcom/squareup/sdk/reader/checkout/CheckoutResult$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_a

    .line 144
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 146
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;

    .line 148
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 149
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 150
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 151
    :cond_4
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    if-eqz v2, :cond_6

    :goto_0
    return v1

    .line 155
    :cond_6
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    return v1

    .line 156
    :cond_7
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    iget-object v3, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    return v1

    .line 157
    :cond_8
    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_9

    return v1

    :cond_9
    return v0

    :cond_a
    :goto_1
    return v1
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    return-object v0
.end method

.method public getLocationId()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    return-object v0
.end method

.method public getTenders()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/Tender;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    return-object v0
.end method

.method public getTotalMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getTotalTipMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getTransactionClientId()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionId()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 164
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 165
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 166
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 167
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 168
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 169
    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckoutResult{locationId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->locationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", totalMoney="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", transactionClientId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionClientId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", totalTipMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->totalTipMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->createdAt:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutResult;->tenders:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
