.class public final Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
.super Ljava/lang/Object;
.source "CheckoutParameters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sdk/reader/checkout/CheckoutParameters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final additionalPaymentTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;",
            ">;"
        }
    .end annotation
.end field

.field private allowSplitTender:Z

.field private amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private collectSignature:Z

.field private delayCapture:Z

.field private note:Ljava/lang/String;

.field private setDeprecatedAlwaysRequireSignature:Z

.field private skipReceipt:Z

.field private tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;)V
    .locals 1

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "amountMoney"

    .line 212
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 213
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    const/4 p1, 0x0

    .line 214
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->note:Ljava/lang/String;

    .line 215
    const-class v0, Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    const/4 v0, 0x0

    .line 216
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->allowSplitTender:Z

    .line 217
    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    .line 218
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->collectSignature:Z

    .line 219
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->skipReceipt:Z

    .line 220
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->setDeprecatedAlwaysRequireSignature:Z

    .line 221
    iput-boolean v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->delayCapture:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/checkout/Money;Lcom/squareup/sdk/reader/checkout/CheckoutParameters$1;)V
    .locals 0

    .line 199
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;-><init>(Lcom/squareup/sdk/reader/checkout/Money;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Ljava/util/Set;
    .locals 0

    .line 199
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Lcom/squareup/sdk/reader/checkout/Money;
    .locals 0

    .line 199
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Ljava/lang/String;
    .locals 0

    .line 199
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Lcom/squareup/sdk/reader/checkout/TipSettings;
    .locals 0

    .line 199
    iget-object p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z
    .locals 0

    .line 199
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->skipReceipt:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z
    .locals 0

    .line 199
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->allowSplitTender:Z

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z
    .locals 0

    .line 199
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->collectSignature:Z

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z
    .locals 0

    .line 199
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->setDeprecatedAlwaysRequireSignature:Z

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;)Z
    .locals 0

    .line 199
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->delayCapture:Z

    return p0
.end method


# virtual methods
.method public additionalPaymentTypes(Ljava/util/Set;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;",
            ">;)",
            "Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;"
        }
    .end annotation

    const-string v0, "paymentTypes"

    .line 274
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 275
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 276
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public varargs additionalPaymentTypes([Lcom/squareup/sdk/reader/checkout/AdditionalPaymentType;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const-string v0, "paymentTypes"

    .line 285
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 286
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 287
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public allowSplitTender(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 0

    .line 311
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->allowSplitTender:Z

    return-object p0
.end method

.method public alwaysRequireSignature(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 p1, 0x1

    .line 346
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->setDeprecatedAlwaysRequireSignature:Z

    return-object p0
.end method

.method public amountMoney(Lcom/squareup/sdk/reader/checkout/Money;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const-string v0, "amountMoney"

    .line 229
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/Money;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->amountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/sdk/reader/checkout/CheckoutParameters;
    .locals 2

    .line 411
    new-instance v0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/checkout/CheckoutParameters;-><init>(Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;Lcom/squareup/sdk/reader/checkout/CheckoutParameters$1;)V

    return-object v0
.end method

.method public collectSignature(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 0

    .line 360
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->collectSignature:Z

    return-object p0
.end method

.method public delayCapture(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 0

    .line 400
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->delayCapture:Z

    return-object p0
.end method

.method public noAdditionalPaymentTypes()Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->additionalPaymentTypes:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-object p0
.end method

.method public noNote()Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 259
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public noTip()Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 337
    iput-object v0, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const-string v0, "note"

    .line 248
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public skipReceipt(Z)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 0

    .line 375
    iput-boolean p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->skipReceipt:Z

    return-object p0
.end method

.method public tipSettings(Lcom/squareup/sdk/reader/checkout/TipSettings;)Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;
    .locals 1

    const-string/jumbo v0, "tipSettings"

    .line 326
    invoke-static {p1, v0}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/TipSettings;

    iput-object p1, p0, Lcom/squareup/sdk/reader/checkout/CheckoutParameters$Builder;->tipSettings:Lcom/squareup/sdk/reader/checkout/TipSettings;

    return-object p0
.end method
