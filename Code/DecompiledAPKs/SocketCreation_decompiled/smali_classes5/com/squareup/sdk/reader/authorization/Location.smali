.class public final Lcom/squareup/sdk/reader/authorization/Location;
.super Ljava/lang/Object;
.source "Location.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sdk/reader/authorization/Location$Builder;
    }
.end annotation


# instance fields
.field private final businessName:Ljava/lang/String;

.field private final cardProcessingActivated:Z

.field private final currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

.field private final locationId:Ljava/lang/String;

.field private final maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/sdk/reader/authorization/Location$Builder;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$100(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$200(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    .line 38
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$300(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    .line 39
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$400(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$500(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    .line 41
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$600(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    .line 42
    invoke-static {p1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;->access$700(Lcom/squareup/sdk/reader/authorization/Location$Builder;)Lcom/squareup/sdk/reader/checkout/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/sdk/reader/authorization/Location$Builder;Lcom/squareup/sdk/reader/authorization/Location$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/sdk/reader/authorization/Location;-><init>(Lcom/squareup/sdk/reader/authorization/Location$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/sdk/reader/authorization/Location;)Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/sdk/reader/authorization/Location;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/sdk/reader/authorization/Location;)Ljava/lang/String;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    return-object p0
.end method

.method public static newBuilder(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CurrencyCode;)Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/sdk/reader/authorization/Location$Builder;

    const-string v1, "locationId"

    invoke-static {p0, v1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    const-string v1, "currencyCode"

    invoke-static {p1, v1}, Lcom/squareup/sdk/reader/internal/InternalSdkHelper;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;-><init>(Ljava/lang/String;Lcom/squareup/sdk/reader/checkout/CurrencyCode;Lcom/squareup/sdk/reader/authorization/Location$1;)V

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/sdk/reader/authorization/Location$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/sdk/reader/authorization/Location$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/sdk/reader/authorization/Location$Builder;-><init>(Lcom/squareup/sdk/reader/authorization/Location;Lcom/squareup/sdk/reader/authorization/Location$1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_9

    .line 94
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/sdk/reader/authorization/Location;

    .line 98
    iget-boolean v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    iget-boolean v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    if-eq v2, v3, :cond_2

    return v1

    .line 99
    :cond_2
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    return v1

    .line 100
    :cond_3
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    iget-object v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    if-eq v2, v3, :cond_4

    return v1

    .line 101
    :cond_4
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    return v1

    .line 102
    :cond_5
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    return v1

    .line 103
    :cond_6
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object v3, p1, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    return v1

    .line 104
    :cond_7
    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    iget-object p1, p1, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v2, p1}, Lcom/squareup/sdk/reader/checkout/Money;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    return v1

    :cond_8
    return v0

    :cond_9
    :goto_0
    return v1
.end method

.method public getBusinessName()Ljava/lang/String;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/sdk/reader/checkout/CurrencyCode;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    return-object v0
.end method

.method public getLocationId()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    return-object v0
.end method

.method public getMaximumCardPaymentAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getMinimumCardPaymentAmountMoney()Lcom/squareup/sdk/reader/checkout/Money;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 111
    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/CurrencyCode;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 112
    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 113
    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 114
    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 115
    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 116
    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v1}, Lcom/squareup/sdk/reader/checkout/Money;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public isCardProcessingActivated()Z
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Location{locationId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->locationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", currencyCode="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->currencyCode:Lcom/squareup/sdk/reader/checkout/CurrencyCode;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", businessName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/sdk/reader/authorization/Location;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", cardProcessingActivated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->cardProcessingActivated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", minimumCardPaymentAmountMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->minimumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maximumCardPaymentAmountMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/sdk/reader/authorization/Location;->maximumCardPaymentAmountMoney:Lcom/squareup/sdk/reader/checkout/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
