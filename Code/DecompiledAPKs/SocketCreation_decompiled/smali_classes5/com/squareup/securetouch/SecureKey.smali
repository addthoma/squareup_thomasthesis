.class public final enum Lcom/squareup/securetouch/SecureKey;
.super Ljava/lang/Enum;
.source "SecureTouchFeature.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/securetouch/SecureKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0014\u0008\u0086\u0001\u0018\u0000 \u00142\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0014B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureKey;",
        "",
        "(Ljava/lang/String;I)V",
        "Cancel",
        "Accessibility",
        "Done",
        "Clear",
        "Delete",
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
        "EmptySpace",
        "AccessibilityKeypadArea",
        "Companion",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/securetouch/SecureKey;

.field public static final enum Accessibility:Lcom/squareup/securetouch/SecureKey;

.field public static final enum AccessibilityKeypadArea:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Cancel:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Clear:Lcom/squareup/securetouch/SecureKey;

.field public static final Companion:Lcom/squareup/securetouch/SecureKey$Companion;

.field public static final enum Delete:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Done:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Eight:Lcom/squareup/securetouch/SecureKey;

.field public static final enum EmptySpace:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Five:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Four:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Nine:Lcom/squareup/securetouch/SecureKey;

.field public static final enum One:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Seven:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Six:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Three:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Two:Lcom/squareup/securetouch/SecureKey;

.field public static final enum Zero:Lcom/squareup/securetouch/SecureKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/squareup/securetouch/SecureKey;

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x0

    const-string v3, "Cancel"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Cancel:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x1

    const-string v3, "Accessibility"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Accessibility:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x2

    const-string v3, "Done"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Done:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x3

    const-string v3, "Clear"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Clear:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x4

    const-string v3, "Delete"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Delete:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x5

    const-string v3, "Zero"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Zero:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x6

    const-string v3, "One"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->One:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/4 v2, 0x7

    const-string v3, "Two"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Two:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0x8

    const-string v3, "Three"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Three:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0x9

    const-string v3, "Four"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Four:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xa

    const-string v3, "Five"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Five:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xb

    const-string v3, "Six"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Six:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xc

    const-string v3, "Seven"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Seven:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xd

    const-string v3, "Eight"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Eight:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xe

    const-string v3, "Nine"

    invoke-direct {v1, v3, v2}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->Nine:Lcom/squareup/securetouch/SecureKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const-string v2, "EmptySpace"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->EmptySpace:Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/securetouch/SecureKey;

    const-string v2, "AccessibilityKeypadArea"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/securetouch/SecureKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/securetouch/SecureKey;->AccessibilityKeypadArea:Lcom/squareup/securetouch/SecureKey;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/securetouch/SecureKey;->$VALUES:[Lcom/squareup/securetouch/SecureKey;

    new-instance v0, Lcom/squareup/securetouch/SecureKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/SecureKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/SecureKey;->Companion:Lcom/squareup/securetouch/SecureKey$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/securetouch/SecureKey;
    .locals 1

    const-class v0, Lcom/squareup/securetouch/SecureKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/securetouch/SecureKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/securetouch/SecureKey;
    .locals 1

    sget-object v0, Lcom/squareup/securetouch/SecureKey;->$VALUES:[Lcom/squareup/securetouch/SecureKey;

    invoke-virtual {v0}, [Lcom/squareup/securetouch/SecureKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/securetouch/SecureKey;

    return-object v0
.end method
