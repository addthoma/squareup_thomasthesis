.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;
.super Ljava/lang/Object;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lio/reactivex/CompletableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->cardreaderEventToAudioCompletable()Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/CompletableEmitter;",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/CompletableEmitter;)V
    .locals 6

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    new-instance v0, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    .line 216
    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;

    invoke-static {v1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->access$getCardreader$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/SecureTouchFeature;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/securetouch/SecureTouchFeature;->eventsFromReader()Lio/reactivex/Observable;

    move-result-object v1

    .line 217
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;

    invoke-static {v3}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->access$getMainThread$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lio/reactivex/Scheduler;

    move-result-object v3

    const-wide/16 v4, 0x1e

    invoke-virtual {v1, v4, v5, v2, v3}, Lio/reactivex/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 218
    new-instance v2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;

    invoke-direct {v2, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    .line 225
    new-instance v1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$2;

    invoke-direct {v1, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$2;-><init>(Lio/reactivex/disposables/SerialDisposable;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/CompletableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    return-void
.end method
