.class public final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSecureTouchAccessibilityWorkflow.kt"

# interfaces
.implements Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchAccessibilityWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchAccessibilityWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,139:1\n85#2:140\n41#2:148\n56#2,2:149\n240#3:141\n276#4:142\n276#4:151\n149#5,5:143\n*E\n*S KotlinDebug\n*F\n+ 1 RealSecureTouchAccessibilityWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow\n*L\n74#1:140\n99#1:148\n99#1,2:149\n74#1:141\n74#1:142\n99#1:151\n84#1,5:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\'B7\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u001a\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00032\u0008\u0010 \u001a\u0004\u0018\u00010!H\u0016JN\u0010\"\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u001f\u001a\u00020\u00032\u0006\u0010#\u001a\u00020\u00042\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050%H\u0016J\u0010\u0010&\u001a\u00020!2\u0006\u0010#\u001a\u00020\u0004H\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n \u001a*\u0004\u0018\u00010\u00190\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "tutorial",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;",
        "pinEntry",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "restoreAccessibilitySettingsLifecycleWorker",
        "Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;",
        "headsetConnectionListener",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
        "(Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V",
        "forceTalkBackOn",
        "Lio/reactivex/Completable;",
        "kotlin.jvm.PlatformType",
        "headphoneConnectionState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final forceTalkBackOn:Lio/reactivex/Completable;

.field private final headphoneConnectionState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final pinEntry:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;

.field private final restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

.field private final tutorial:Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;Lcom/squareup/settings/server/Features;Lcom/squareup/accessibility/AccessibilitySettings;Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "tutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pinEntry"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilitySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restoreAccessibilitySettingsLifecycleWorker"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "headsetConnectionListener"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->tutorial:Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->pinEntry:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    iput-object p5, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

    .line 48
    invoke-interface {p6}, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;->onHeadsetStateChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->headphoneConnectionState:Lio/reactivex/Observable;

    .line 49
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-virtual {p1}, Lcom/squareup/accessibility/AccessibilitySettings;->getTalkBackState()Lio/reactivex/Observable;

    move-result-object p1

    .line 50
    sget-object p2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$forceTalkBackOn$1;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$forceTalkBackOn$1;

    check-cast p2, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 51
    new-instance p2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$forceTalkBackOn$2;

    invoke-direct {p2, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$forceTalkBackOn$2;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->flatMapCompletable(Lio/reactivex/functions/Function;)Lio/reactivex/Completable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->forceTalkBackOn:Lio/reactivex/Completable;

    return-void
.end method

.method public static final synthetic access$getAccessibilitySettings$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)Lcom/squareup/accessibility/AccessibilitySettings;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;
    .locals 1

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$InitiateAccessibility;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;->getShowAccessibilityTutorial()Z

    move-result p1

    invoke-direct {p2, v0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$InitiateAccessibility;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    check-cast p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->initialState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;

    check-cast p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 69
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "SecureTouchAccessibilityWorkflow state -> %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    instance-of v0, p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$InitiateAccessibility;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 74
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_ACCESSIBLE_PIN_TUTORIAL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->enabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "features.enabled(USE_ACCESSIBLE_PIN_TUTORIAL)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v0, p1, v1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 141
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 142
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 75
    new-instance p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 73
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 84
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    sget-object p2, Lcom/squareup/securetouch/accessibility/GlassDialogScreen;->INSTANCE:Lcom/squareup/securetouch/accessibility/GlassDialogScreen;

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 144
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 145
    const-class v0, Lcom/squareup/securetouch/accessibility/GlassDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 146
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 144
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 84
    invoke-virtual {p1, p3}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    sget-object p3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, p3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 87
    :cond_0
    instance-of v0, p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityTutorial;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->tutorial:Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;

    invoke-direct {p1, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$2;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 91
    :cond_1
    instance-of v0, p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityPinEntry;

    if-eqz v0, :cond_3

    .line 93
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->forceTalkBackOn:Lio/reactivex/Completable;

    const-string v2, "forceTalkBackOn"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/workflow/rx2/RxWorkersKt;->asWorker(Lio/reactivex/Completable;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "force-talk-back-on-worker-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-static {p3, v0, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->headphoneConnectionState:Lio/reactivex/Observable;

    .line 148
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_2

    .line 150
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 151
    const-class v2, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Worker;

    const/4 v6, 0x0

    .line 99
    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$3;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p3

    invoke-static/range {v4 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->restoreAccessibilitySettingsLifecycleWorker:Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;

    check-cast p2, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityPinEntry;

    invoke-virtual {p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$DisplayAccessibilityPinEntry;->getSetTalkBackEnabledOnFinish()Z

    move-result p2

    invoke-interface {v0, p2}, Lcom/squareup/securetouch/accessibility/RestoreAccessibilitySettingsLifecycleWorker;->create(Z)Lcom/squareup/workflow/LifecycleWorker;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/Worker;

    const/4 v0, 0x2

    .line 107
    invoke-static {p3, p2, v1, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 111
    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->pinEntry:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryWorkflow;

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    new-instance p2, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$4;

    invoke-direct {p2, p0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$4;-><init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    move-object v0, p3

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    .line 150
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 111
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->snapshotState(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
