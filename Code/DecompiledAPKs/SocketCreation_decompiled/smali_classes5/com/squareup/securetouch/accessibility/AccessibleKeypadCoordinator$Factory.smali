.class public final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;
.super Ljava/lang/Object;
.source "AccessibleKeypadCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ*\u0010\u000b\u001a\u00020\u000c2\"\u0010\r\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0008\u0012\u0004\u0012\u00020\u0010`\u00120\u000eR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "mainThread",
        "Lio/reactivex/Scheduler;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "accessibilitySettings",
        "Lcom/squareup/accessibility/AccessibilitySettings;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V",
        "create",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final mainThread:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilitySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->mainThread:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    .line 86
    iget-object v3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->mainThread:Lio/reactivex/Scheduler;

    iget-object v5, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v6, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;->accessibilitySettings:Lcom/squareup/accessibility/AccessibilitySettings;

    move-object v1, v0

    move-object v2, p1

    .line 85
    invoke-direct/range {v1 .. v6}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lio/reactivex/Scheduler;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/accessibility/AccessibilitySettings;)V

    return-object v0
.end method
