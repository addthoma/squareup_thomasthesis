.class public abstract Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;
.super Ljava/lang/Object;
.source "AccessibleKeypadScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AccessibleKeypadScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForReader;,
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForKeypadLayout;,
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;,
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u000f\u0010\u0011\u0012B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0004\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
        "",
        "()V",
        "digitsEntered",
        "",
        "getDigitsEntered",
        "()I",
        "displayInputDigits",
        "Lcom/squareup/securetouch/accessibility/InputDigits;",
        "getDisplayInputDigits",
        "()Lcom/squareup/securetouch/accessibility/InputDigits;",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "DisplayingDigits",
        "HidingDigits",
        "WaitingForKeypadLayout",
        "WaitingForReader",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForReader;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$WaitingForKeypadLayout;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$HidingDigits;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData$DisplayingDigits;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDigitsEntered()I
.end method

.method public abstract getDisplayInputDigits()Lcom/squareup/securetouch/accessibility/InputDigits;
.end method

.method public abstract getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
.end method
