.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
        "+",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "tutorialEnabled",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;->$state:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Z)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 77
    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;->$state:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState;

    check-cast p1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$InitiateAccessibility;

    invoke-virtual {p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityState$InitiateAccessibility;->getShowAccessibilityTutorial()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToTutorial;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToTutorial;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 78
    :cond_0
    new-instance p1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;

    .line 79
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;

    invoke-static {v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;->access$getAccessibilitySettings$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow;)Lcom/squareup/accessibility/AccessibilitySettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/accessibility/AccessibilitySettings;->isTalkBackEnabled()Z

    move-result v0

    .line 78
    invoke-direct {p1, v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$Action$ProceedToPinEntry;-><init>(Z)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityWorkflow$render$1;->invoke(Z)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
