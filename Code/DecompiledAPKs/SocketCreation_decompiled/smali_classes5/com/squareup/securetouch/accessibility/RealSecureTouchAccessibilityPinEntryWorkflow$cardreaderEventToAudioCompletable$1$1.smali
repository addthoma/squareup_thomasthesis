.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;
.super Ljava/lang/Object;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;->subscribe(Lio/reactivex/CompletableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 3

    .line 219
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;

    iget-object v0, v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1;->this$0:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;

    invoke-static {v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->access$getAudioPlayer$p(Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    move-result-object v0

    .line 220
    sget-object v1, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;->Companion:Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$Companion;

    const-string v2, "event"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio$Companion;->secureTouchEventToAudio(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;

    move-result-object p1

    .line 219
    invoke-virtual {v0, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;->maybePlayAudio(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudio;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$cardreaderEventToAudioCompletable$1$1;->accept(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V

    return-void
.end method
