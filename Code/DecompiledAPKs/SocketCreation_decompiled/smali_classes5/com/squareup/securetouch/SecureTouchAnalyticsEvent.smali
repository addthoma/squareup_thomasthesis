.class public final Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchAnalyticsEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0019\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 &2\u00020\u0001:\u0001&BG\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003JS\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00052\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u0003H\u00c6\u0001J\u000e\u0010\u001d\u001a\u00020\u00002\u0006\u0010\u001e\u001a\u00020\u001fJ\u0013\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u00d6\u0003J\t\u0010$\u001a\u00020\u0003H\u00d6\u0001J\t\u0010%\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000fR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "squid_secure_touch_events_attempt_number",
        "",
        "squid_secure_touch_events_event_type",
        "",
        "squid_secure_touch_events_event_value",
        "squid_secure_touch_events_message",
        "squid_secure_touch_events_seller_locale",
        "squid_secure_touch_events_session_id",
        "squid_secure_touch_events_version",
        "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V",
        "getSquid_secure_touch_events_attempt_number",
        "()I",
        "getSquid_secure_touch_events_event_type",
        "()Ljava/lang/String;",
        "getSquid_secure_touch_events_event_value",
        "getSquid_secure_touch_events_message",
        "getSquid_secure_touch_events_seller_locale",
        "getSquid_secure_touch_events_session_id",
        "getSquid_secure_touch_events_version",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "copyWith",
        "baseLogEvent",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "Companion",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CATALOG:Ljava/lang/String; = "squid_secure_touch_events"

.field public static final Companion:Lcom/squareup/securetouch/SecureTouchAnalyticsEvent$Companion;


# instance fields
.field private final squid_secure_touch_events_attempt_number:I

.field private final squid_secure_touch_events_event_type:Ljava/lang/String;

.field private final squid_secure_touch_events_event_value:Ljava/lang/String;

.field private final squid_secure_touch_events_message:Ljava/lang/String;

.field private final squid_secure_touch_events_seller_locale:Ljava/lang/String;

.field private final squid_secure_touch_events_session_id:Ljava/lang/String;

.field private final squid_secure_touch_events_version:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->Companion:Lcom/squareup/securetouch/SecureTouchAnalyticsEvent$Companion;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const-string v0, "squid_secure_touch_events_event_type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squid_secure_touch_events_seller_locale"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squid_secure_touch_events_session_id"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squid_secure_touch_events"

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    iput p7, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v0, p8, 0x4

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 27
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_1

    :cond_1
    move-object v4, p3

    :goto_1
    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_2

    .line 29
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    goto :goto_2

    :cond_2
    move-object v5, p4

    :goto_2
    move-object v1, p0

    move-object v3, p2

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget p1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget p7, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    :cond_6
    move v4, p7

    move-object p2, p0

    move p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->copy(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    return-object v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    return v0
.end method

.method public final copy(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;
    .locals 9

    const-string v0, "squid_secure_touch_events_event_type"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squid_secure_touch_events_seller_locale"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squid_secure_touch_events_session_id"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    move-object v1, v0

    move v2, p1

    move-object v4, p3

    move-object v5, p4

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final copyWith(Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;
    .locals 11

    const-string v0, "baseLogEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->getEventType()Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;->toString()Ljava/lang/String;

    move-result-object v3

    .line 43
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->getEventValue()Ljava/lang/String;

    move-result-object v4

    .line 44
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->getEventMessage()Ljava/lang/String;

    move-result-object v5

    .line 45
    invoke-virtual {p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->getAttemptNumber()I

    move-result v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x70

    const/4 v10, 0x0

    move-object v1, p0

    .line 41
    invoke-static/range {v1 .. v10}, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->copy$default(Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    iget p1, p1, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getSquid_secure_touch_events_attempt_number()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    return v0
.end method

.method public final getSquid_secure_touch_events_event_type()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquid_secure_touch_events_event_value()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquid_secure_touch_events_message()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquid_secure_touch_events_seller_locale()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquid_secure_touch_events_session_id()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquid_secure_touch_events_version()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchAnalyticsEvent(squid_secure_touch_events_attempt_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_attempt_number:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_event_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_event_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_event_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_seller_locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_seller_locale:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_session_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_session_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", squid_secure_touch_events_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsEvent;->squid_secure_touch_events_version:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
