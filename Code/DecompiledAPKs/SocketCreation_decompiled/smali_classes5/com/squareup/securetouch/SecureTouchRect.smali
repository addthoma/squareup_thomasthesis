.class public final Lcom/squareup/securetouch/SecureTouchRect;
.super Ljava/lang/Object;
.source "SecureTouchRect.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0013\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\u0016\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u0003J1\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\u00172\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u001a\u0010\u0006\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\t\"\u0004\u0008\r\u0010\u000bR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\t\"\u0004\u0008\u000f\u0010\u000bR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\t\"\u0004\u0008\u0011\u0010\u000b\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "",
        "left",
        "",
        "top",
        "right",
        "bottom",
        "(IIII)V",
        "getBottom",
        "()I",
        "setBottom",
        "(I)V",
        "getLeft",
        "setLeft",
        "getRight",
        "setRight",
        "getTop",
        "setTop",
        "component1",
        "component2",
        "component3",
        "component4",
        "contains",
        "",
        "x",
        "y",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bottom:I

.field private left:I

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/securetouch/SecureTouchRect;-><init>(IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    iput p2, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    iput p3, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    iput p4, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    return-void
.end method

.method public synthetic constructor <init>(IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const/4 p2, 0x0

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 10
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/securetouch/SecureTouchRect;-><init>(IIII)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchRect;IIIIILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/securetouch/SecureTouchRect;->copy(IIII)Lcom/squareup/securetouch/SecureTouchRect;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    return v0
.end method

.method public final contains(II)Z
    .locals 2

    .line 16
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    if-le v0, p1, :cond_0

    goto :goto_0

    :cond_0
    if-lt v1, p1, :cond_2

    iget p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    if-le p1, p2, :cond_1

    goto :goto_0

    :cond_1
    if-lt v0, p2, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public final copy(IIII)Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    new-instance v0, Lcom/squareup/securetouch/SecureTouchRect;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/securetouch/SecureTouchRect;-><init>(IIII)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchRect;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchRect;

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    iget p1, p1, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBottom()I
    .locals 1

    .line 10
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    return v0
.end method

.method public final getLeft()I
    .locals 1

    .line 7
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    return v0
.end method

.method public final getRight()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    return v0
.end method

.method public final getTop()I
    .locals 1

    .line 8
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final setBottom(I)V
    .locals 0

    .line 10
    iput p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    return-void
.end method

.method public final setLeft(I)V
    .locals 0

    .line 7
    iput p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    return-void
.end method

.method public final setRight(I)V
    .locals 0

    .line 9
    iput p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    return-void
.end method

.method public final setTop(I)V
    .locals 0

    .line 8
    iput p1, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchRect(left="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->left:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", top="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->top:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", right="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->right:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", bottom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchRect;->bottom:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
