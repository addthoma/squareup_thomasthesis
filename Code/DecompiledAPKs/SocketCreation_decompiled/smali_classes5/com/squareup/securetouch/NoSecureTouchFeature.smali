.class public final Lcom/squareup/securetouch/NoSecureTouchFeature;
.super Ljava/lang/Object;
.source "SecureTouchFeature.kt"

# interfaces
.implements Lcom/squareup/securetouch/SecureTouchFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0016J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0004H\u0016J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0005H\u0016J\u0008\u0010\u000b\u001a\u00020\tH\u0016J\u0008\u0010\u000c\u001a\u00020\tH\u0016J\u0010\u0010\r\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007H\u0016J\u0010\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/securetouch/NoSecureTouchFeature;",
        "Lcom/squareup/securetouch/SecureTouchFeature;",
        "()V",
        "eventsForReader",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
        "eventsFromReader",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "onSecureTouchApplicationEvent",
        "",
        "event",
        "onSecureTouchDisabled",
        "onSecureTouchEnabled",
        "onSecureTouchFeatureEvent",
        "onUnexpectedReleaseEvent",
        "secureTouchPoint",
        "Lcom/squareup/securetouch/SecureTouchPoint;",
        "onUnexpectedTouchEvent",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/NoSecureTouchFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 151
    new-instance v0, Lcom/squareup/securetouch/NoSecureTouchFeature;

    invoke-direct {v0}, Lcom/squareup/securetouch/NoSecureTouchFeature;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/NoSecureTouchFeature;->INSTANCE:Lcom/squareup/securetouch/NoSecureTouchFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eventsForReader()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
            ">;"
        }
    .end annotation

    .line 154
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.never()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public eventsFromReader()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
            ">;"
        }
    .end annotation

    .line 152
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.never()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No secure touch here!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onSecureTouchDisabled()V
    .locals 2

    .line 165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No secure touch here!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onSecureTouchEnabled()V
    .locals 2

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No secure touch here!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onSecureTouchFeatureEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No secure touch here!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onUnexpectedReleaseEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No secure touch here!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onUnexpectedTouchEvent(Lcom/squareup/securetouch/SecureTouchPoint;)V
    .locals 1

    const-string v0, "secureTouchPoint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No secure touch here!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
