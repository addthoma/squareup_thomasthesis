.class final Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflow;->onKeypadAccessibilityPressed(Lcom/squareup/securetouch/UsingKeypad;)Lcom/squareup/securetouch/SecureTouchState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "-",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_onKeypadAccessibilityPressed:Lcom/squareup/securetouch/UsingKeypad;

.field final synthetic this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/RealSecureTouchWorkflow;Lcom/squareup/securetouch/UsingKeypad;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    iput-object p2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->$this_onKeypadAccessibilityPressed:Lcom/squareup/securetouch/UsingKeypad;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "-",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    new-instance v0, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;

    .line 363
    iget-object v1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->$this_onKeypadAccessibilityPressed:Lcom/squareup/securetouch/UsingKeypad;

    invoke-virtual {v1}, Lcom/squareup/securetouch/UsingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v1

    .line 364
    iget-object v2, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->this$0:Lcom/squareup/securetouch/RealSecureTouchWorkflow;

    invoke-static {v2}, Lcom/squareup/securetouch/RealSecureTouchWorkflow;->access$getAccessibilitySettings$p(Lcom/squareup/securetouch/RealSecureTouchWorkflow;)Lcom/squareup/accessibility/AccessibilitySettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/accessibility/AccessibilitySettings;->isTalkBackEnabled()Z

    move-result v2

    .line 365
    iget-object v3, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$onKeypadAccessibilityPressed$1;->$this_onKeypadAccessibilityPressed:Lcom/squareup/securetouch/UsingKeypad;

    invoke-virtual {v3}, Lcom/squareup/securetouch/UsingKeypad;->getUseHighContrastMode()Z

    move-result v3

    .line 362
    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/securetouch/ShowingSecureTouchModeSelectionScreen;-><init>(ZLcom/squareup/securetouch/SecureTouchPinEntryState;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
