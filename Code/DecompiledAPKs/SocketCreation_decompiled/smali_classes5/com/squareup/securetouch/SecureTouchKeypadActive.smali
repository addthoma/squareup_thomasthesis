.class public abstract Lcom/squareup/securetouch/SecureTouchKeypadActive;
.super Lcom/squareup/securetouch/SecureTouchApplicationEvent;
.source "SecureTouchFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B\u001b\u0008\u0002\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\u0006R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchKeypadActive;",
        "Lcom/squareup/securetouch/SecureTouchApplicationEvent;",
        "coordinates",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "(Ljava/util/Map;)V",
        "getCoordinates",
        "()Ljava/util/Map;",
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final coordinates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/securetouch/SecureTouchApplicationEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchKeypadActive;->coordinates:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/securetouch/SecureTouchKeypadActive;-><init>(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final getCoordinates()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchKeypadActive;->coordinates:Ljava/util/Map;

    return-object v0
.end method
