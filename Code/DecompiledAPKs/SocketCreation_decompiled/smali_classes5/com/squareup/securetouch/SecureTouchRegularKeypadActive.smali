.class public final Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
.super Lcom/squareup/securetouch/SecureTouchKeypadActive;
.source "SecureTouchFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\u0006J\u0015\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u001f\u0010\n\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;",
        "Lcom/squareup/securetouch/SecureTouchKeypadActive;",
        "keyCoordinates",
        "",
        "Lcom/squareup/securetouch/SecureKey;",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "(Ljava/util/Map;)V",
        "getKeyCoordinates",
        "()Ljava/util/Map;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "secure-touch-feature_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final keyCoordinates:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;)V"
        }
    .end annotation

    const-string v0, "keyCoordinates"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 98
    invoke-direct {p0, p1, v0}, Lcom/squareup/securetouch/SecureTouchKeypadActive;-><init>(Ljava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->copy(Ljava/util/Map;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Ljava/util/Map;)Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;)",
            "Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;"
        }
    .end annotation

    const-string v0, "keyCoordinates"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    invoke-direct {v0, p1}, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getKeyCoordinates()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/securetouch/SecureKey;",
            "Lcom/squareup/securetouch/SecureTouchRect;",
            ">;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchRegularKeypadActive(keyCoordinates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchRegularKeypadActive;->keyCoordinates:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
