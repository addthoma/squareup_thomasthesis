.class public abstract Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;
.super Ljava/lang/Object;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B1\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u0082\u0001\t\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;",
        "",
        "eventType",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;",
        "eventValue",
        "",
        "eventMessage",
        "attemptNumber",
        "",
        "(Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;Ljava/lang/String;Ljava/lang/String;I)V",
        "getAttemptNumber",
        "()I",
        "getEventMessage",
        "()Ljava/lang/String;",
        "getEventType",
        "()Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;",
        "getEventValue",
        "Lcom/squareup/securetouch/ClearButton;",
        "Lcom/squareup/securetouch/BackButton;",
        "Lcom/squareup/securetouch/DoneButton;",
        "Lcom/squareup/securetouch/CancelButton;",
        "Lcom/squareup/securetouch/AccessibilityButton;",
        "Lcom/squareup/securetouch/StartFlow;",
        "Lcom/squareup/securetouch/StartAccessibilityFlow;",
        "Lcom/squareup/securetouch/FinishFlow;",
        "Lcom/squareup/securetouch/FinishAccessibilityFlow;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attemptNumber:I

.field private final eventMessage:Ljava/lang/String;

.field private final eventType:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

.field private final eventValue:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventType:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventValue:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventMessage:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->attemptNumber:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 51
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 52
    move-object p3, v0

    check-cast p3, Ljava/lang/String;

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 53
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;-><init>(Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final getAttemptNumber()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->attemptNumber:I

    return v0
.end method

.method public final getEventMessage()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventType()Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventType:Lcom/squareup/securetouch/SecureTouchAnalyticsEventType;

    return-object v0
.end method

.method public final getEventValue()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogEvent;->eventValue:Ljava/lang/String;

    return-object v0
.end method
