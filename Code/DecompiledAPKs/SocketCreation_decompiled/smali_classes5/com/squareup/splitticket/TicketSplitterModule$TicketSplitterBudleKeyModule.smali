.class public final Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;
.super Ljava/lang/Object;
.source "TicketSplitterModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/splitticket/TicketSplitterModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TicketSplitterBudleKeyModule"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007J\"\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule;",
        "",
        "()V",
        "provideFosterStateKey",
        "Lcom/squareup/BundleKey;",
        "Lcom/squareup/splitticket/FosterState;",
        "gson",
        "Lcom/google/gson/Gson;",
        "provideSplitStateKey",
        "",
        "",
        "Lcom/squareup/splitticket/RealSplitState;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideFosterStateKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/splitticket/FosterState;",
            ">;"
        }
    .end annotation

    const-string v0, "gson"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideFosterStateKey$1;

    invoke-direct {v0}, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideFosterStateKey$1;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideFosterStateKey$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "foster-state-key"

    .line 41
    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p1

    const-string v0, "BundleKey.json(\n        \u2026rState>() {}.type\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final provideSplitStateKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/splitticket/RealSplitState;",
            ">;>;"
        }
    .end annotation

    const-string v0, "gson"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideSplitStateKey$1;

    invoke-direct {v0}, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideSplitStateKey$1;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/splitticket/TicketSplitterModule$TicketSplitterBudleKeyModule$provideSplitStateKey$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "split-state-map-key"

    .line 34
    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p1

    const-string v0, "BundleKey.json(\n        \u2026State>>() {}.type\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
