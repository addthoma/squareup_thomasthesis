.class public final Lcom/squareup/splitticket/MoveConfiguration;
.super Ljava/lang/Object;
.source "MoveConfiguration.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/splitticket/MoveConfiguration$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0000\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB/\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003\u00a2\u0006\u0002\u0010\tR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000b\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/splitticket/MoveConfiguration;",
        "",
        "items",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "cartAmountDiscounts",
        "Lcom/squareup/checkout/Discount;",
        "associatedCourses",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
        "(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "getAssociatedCourses",
        "()Ljava/util/List;",
        "getCartAmountDiscounts",
        "getItems",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/splitticket/MoveConfiguration$Companion;

.field private static final EMPTY:Lcom/squareup/splitticket/MoveConfiguration;


# instance fields
.field private final associatedCourses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation
.end field

.field private final cartAmountDiscounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/splitticket/MoveConfiguration$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/splitticket/MoveConfiguration$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/splitticket/MoveConfiguration;->Companion:Lcom/squareup/splitticket/MoveConfiguration$Companion;

    .line 16
    new-instance v0, Lcom/squareup/splitticket/MoveConfiguration;

    .line 17
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 16
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/splitticket/MoveConfiguration;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    sput-object v0, Lcom/squareup/splitticket/MoveConfiguration;->EMPTY:Lcom/squareup/splitticket/MoveConfiguration;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartAmountDiscounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "associatedCourses"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/splitticket/MoveConfiguration;->items:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/splitticket/MoveConfiguration;->cartAmountDiscounts:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/splitticket/MoveConfiguration;->associatedCourses:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getEMPTY$cp()Lcom/squareup/splitticket/MoveConfiguration;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/splitticket/MoveConfiguration;->EMPTY:Lcom/squareup/splitticket/MoveConfiguration;

    return-object v0
.end method


# virtual methods
.method public final getAssociatedCourses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions$Course;",
            ">;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/splitticket/MoveConfiguration;->associatedCourses:Ljava/util/List;

    return-object v0
.end method

.method public final getCartAmountDiscounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/splitticket/MoveConfiguration;->cartAmountDiscounts:Ljava/util/List;

    return-object v0
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/splitticket/MoveConfiguration;->items:Ljava/util/List;

    return-object v0
.end method
