.class Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;
.super Ljava/lang/Object;
.source "StickyListHeadersListView.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/AdapterWrapper$OnHeaderClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/stickylistheaders/StickyListHeadersListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AdapterWrapperHeaderClickHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;


# direct methods
.method private constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .line 567
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Lcom/squareup/stickylistheaders/StickyListHeadersListView$1;)V
    .locals 0

    .line 567
    invoke-direct {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;-><init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V

    return-void
.end method


# virtual methods
.method public onHeaderClick(Landroid/view/View;IJ)V
    .locals 8

    .line 570
    iget-object v0, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v0}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$500(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    const/4 v7, 0x0

    move-object v3, p1

    move v4, p2

    move-wide v5, p3

    invoke-interface/range {v1 .. v7}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;->onHeaderClick(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/view/View;IJZ)V

    return-void
.end method
