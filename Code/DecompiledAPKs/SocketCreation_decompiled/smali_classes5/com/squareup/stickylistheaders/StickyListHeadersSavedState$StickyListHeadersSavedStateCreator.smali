.class public Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;
.super Ljava/lang/Object;
.source "StickyListHeadersSavedState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;
.implements Landroid/os/Parcelable$ClassLoaderCreator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StickyListHeadersSavedStateCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;",
        ">;",
        "Landroid/os/Parcelable$ClassLoaderCreator<",
        "Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;
    .locals 2

    .line 57
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;->createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;
    .locals 0

    .line 61
    new-array p1, p1, [Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/stickylistheaders/StickyListHeadersSavedState$StickyListHeadersSavedStateCreator;->newArray(I)[Lcom/squareup/stickylistheaders/StickyListHeadersSavedState;

    move-result-object p1

    return-object p1
.end method
