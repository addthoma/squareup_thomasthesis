.class Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;
.super Ljava/lang/Object;
.source "StickyListHeadersListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/stickylistheaders/StickyListHeadersListView;->setOnHeaderClickListener(Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;


# direct methods
.method constructor <init>(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 652
    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$500(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {v1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$200(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;

    move-result-object v2

    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    .line 653
    invoke-static {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$300(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object p1, p0, Lcom/squareup/stickylistheaders/StickyListHeadersListView$2;->this$0:Lcom/squareup/stickylistheaders/StickyListHeadersListView;

    invoke-static {p1}, Lcom/squareup/stickylistheaders/StickyListHeadersListView;->access$400(Lcom/squareup/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/4 v6, 0x1

    .line 652
    invoke-interface/range {v0 .. v6}, Lcom/squareup/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;->onHeaderClick(Lcom/squareup/stickylistheaders/StickyListHeadersListView;Landroid/view/View;IJZ)V

    return-void
.end method
