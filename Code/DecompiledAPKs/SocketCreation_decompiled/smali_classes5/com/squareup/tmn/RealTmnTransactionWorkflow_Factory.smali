.class public final Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/audio/TmnAudioPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnObservablesHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/MiryoWorkerDelayer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/audio/TmnAudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnObservablesHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/MiryoWorkerDelayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p7, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p8, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p9, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/audio/TmnAudioPlayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnObservablesHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/MiryoWorkerDelayer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;"
        }
    .end annotation

    .line 60
    new-instance v10, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)Lcom/squareup/tmn/RealTmnTransactionWorkflow;
    .locals 11

    .line 66
    new-instance v10, Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;-><init>(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/tmn/RealTmnTransactionWorkflow;
    .locals 10

    .line 52
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/felica/FelicaService;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tmn/CardReaderHelper;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/tmn/audio/TmnAudioPlayer;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tmn/TmnObservablesHelper;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tmn/MiryoWorkerDelayer;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/tmn/TmnTimings;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v9}, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->newInstance(Lcom/squareup/server/felica/FelicaService;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/audio/TmnAudioPlayer;Lcom/squareup/tmn/TmnObservablesHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;Lcom/squareup/tmn/MiryoWorkerDelayer;Lcom/squareup/tmn/TmnTimings;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;)Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow_Factory;->get()Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    move-result-object v0

    return-object v0
.end method
