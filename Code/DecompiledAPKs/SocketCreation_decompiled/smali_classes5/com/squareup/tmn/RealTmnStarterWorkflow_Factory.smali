.class public final Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealTmnStarterWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tmn/RealTmnStarterWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTransactionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTransactionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p3, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTransactionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/CardReaderHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/EmoneyAnalyticsLogger;",
            ">;)",
            "Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tmn/TmnTransactionWorkflow;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;)Lcom/squareup/tmn/RealTmnStarterWorkflow;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/tmn/RealTmnStarterWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/tmn/RealTmnStarterWorkflow;-><init>(Lcom/squareup/tmn/TmnTransactionWorkflow;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tmn/RealTmnStarterWorkflow;
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tmn/TmnTransactionWorkflow;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tmn/CardReaderHelper;

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tmn/EmoneyAnalyticsLogger;

    invoke-static {v0, v1, v2}, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->newInstance(Lcom/squareup/tmn/TmnTransactionWorkflow;Lcom/squareup/tmn/CardReaderHelper;Lcom/squareup/tmn/EmoneyAnalyticsLogger;)Lcom/squareup/tmn/RealTmnStarterWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnStarterWorkflow_Factory;->get()Lcom/squareup/tmn/RealTmnStarterWorkflow;

    move-result-object v0

    return-object v0
.end method
