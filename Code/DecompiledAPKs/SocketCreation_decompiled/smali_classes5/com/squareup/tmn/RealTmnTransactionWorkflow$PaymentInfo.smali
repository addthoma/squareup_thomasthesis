.class public final Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;",
        "",
        "transactionType",
        "Lcom/squareup/tmn/TmnTransactionType;",
        "brandId",
        "Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "amountAuthorized",
        "",
        "(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)V",
        "getAmountAuthorized",
        "()J",
        "getBrandId",
        "()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;",
        "getTransactionType",
        "()Lcom/squareup/tmn/TmnTransactionType;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountAuthorized:J

.field private final brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

.field private final transactionType:Lcom/squareup/tmn/TmnTransactionType;


# direct methods
.method public constructor <init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)V
    .locals 1

    const-string/jumbo v0, "transactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iput-wide p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;JILjava/lang/Object;)Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget-wide p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->copy(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tmn/TmnTransactionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    return-wide v0
.end method

.method public final copy(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;
    .locals 1

    const-string/jumbo v0, "transactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;-><init>(Lcom/squareup/tmn/TmnTransactionType;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    iget-object v1, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    iget-wide v2, p1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountAuthorized()J
    .locals 2

    .line 720
    iget-wide v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    return-wide v0
.end method

.method public final getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;
    .locals 1

    .line 719
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    return-object v0
.end method

.method public final getTransactionType()Lcom/squareup/tmn/TmnTransactionType;
    .locals 1

    .line 718
    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentInfo(transactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->transactionType:Lcom/squareup/tmn/TmnTransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", brandId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->brandId:Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amountAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$PaymentInfo;->amountAuthorized:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
