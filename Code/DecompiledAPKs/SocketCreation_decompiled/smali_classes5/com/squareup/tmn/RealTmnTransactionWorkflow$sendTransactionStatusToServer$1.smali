.class final Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;
.super Ljava/lang/Object;
.source "RealTmnTransactionWorkflow.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tmn/RealTmnTransactionWorkflow;->sendTransactionStatusToServer(Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lio/reactivex/SingleSource<",
        "+TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $connId:Ljava/lang/String;

.field final synthetic $status:Lcom/squareup/protos/client/felica/Status;

.field final synthetic $transactionId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tmn/RealTmnTransactionWorkflow;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    iput-object p2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$connId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$status:Lcom/squareup/protos/client/felica/Status;

    iput-object p4, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 459
    new-instance v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;-><init>()V

    .line 460
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$connId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->connection_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    move-result-object v0

    .line 461
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$status:Lcom/squareup/protos/client/felica/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->status(Lcom/squareup/protos/client/felica/Status;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    move-result-object v0

    .line 462
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->transaction_id(Ljava/lang/String;)Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    move-result-object v0

    .line 463
    invoke-virtual {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->build()Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;

    move-result-object v0

    .line 466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TMN Sending transaction status to server transId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$transactionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " connId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$connId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$status:Lcom/squareup/protos/client/felica/Status;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 465
    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    iget-object v1, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v1}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getFelicaService$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Lcom/squareup/server/felica/FelicaService;

    move-result-object v1

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->$transactionId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->this$0:Lcom/squareup/tmn/RealTmnTransactionWorkflow;

    invoke-static {v3}, Lcom/squareup/tmn/RealTmnTransactionWorkflow;->access$getEnableFelicaCertEnv$p(Lcom/squareup/tmn/RealTmnTransactionWorkflow;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/squareup/server/felica/FelicaService;->setTransactionStatus(Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Lcom/squareup/server/felica/FelicaService$SetTransactionStatusStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 471
    sget-object v1, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1$1;->INSTANCE:Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/tmn/RealTmnTransactionWorkflow$sendTransactionStatusToServer$1;->call()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
