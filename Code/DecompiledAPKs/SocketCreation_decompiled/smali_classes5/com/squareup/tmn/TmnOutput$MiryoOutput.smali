.class public abstract Lcom/squareup/tmn/TmnOutput$MiryoOutput;
.super Lcom/squareup/tmn/TmnOutput;
.source "TmnOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/TmnOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MiryoOutput"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;,
        Lcom/squareup/tmn/TmnOutput$MiryoOutput$NetworkErrorInMiryo;,
        Lcom/squareup/tmn/TmnOutput$MiryoOutput$TmnErrorInMiryo;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnOutput$MiryoOutput;",
        "Lcom/squareup/tmn/TmnOutput;",
        "beforeBalance",
        "",
        "amount",
        "(II)V",
        "getAmount",
        "()I",
        "getBeforeBalance",
        "ActiveCardReaderDisconnectedInMiryo",
        "NetworkErrorInMiryo",
        "TmnErrorInMiryo",
        "Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;",
        "Lcom/squareup/tmn/TmnOutput$MiryoOutput$NetworkErrorInMiryo;",
        "Lcom/squareup/tmn/TmnOutput$MiryoOutput$TmnErrorInMiryo;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:I

.field private final beforeBalance:I


# direct methods
.method private constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/tmn/TmnOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/tmn/TmnOutput$MiryoOutput;->beforeBalance:I

    iput p2, p0, Lcom/squareup/tmn/TmnOutput$MiryoOutput;->amount:I

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/tmn/TmnOutput$MiryoOutput;-><init>(II)V

    return-void
.end method


# virtual methods
.method public getAmount()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/tmn/TmnOutput$MiryoOutput;->amount:I

    return v0
.end method

.method public getBeforeBalance()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/tmn/TmnOutput$MiryoOutput;->beforeBalance:I

    return v0
.end method
