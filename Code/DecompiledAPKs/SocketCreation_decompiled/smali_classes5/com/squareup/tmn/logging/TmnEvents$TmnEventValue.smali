.class public final enum Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;
.super Ljava/lang/Enum;
.source "TmnEvents.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tmn/logging/TmnEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TmnEventValue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u001a\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "RECEIVED_TMN_COMPLETION",
        "RECEIVED_TMN_DATA",
        "RECEIVED_TMN_WRITE_NOTIFY",
        "CARDREADER_START_SUCCESS",
        "CARDREADER_START_FAILED",
        "CARDREADER_ERROR",
        "CARDREADER_DISCONNECTED",
        "NETWORK_FAILURE",
        "NETWORK_SUCCESS",
        "TMN_TRANSACTION_COMPLETED",
        "TMN_TRANSACTION_CARD_WRITE_STARTED",
        "ABLE_TO_START_MIRYO",
        "SHOULD_ENTER_MIRYO",
        "SHOULD_NOT_ENTER_MIRYO",
        "SENDING_PACKET_DATA",
        "SENT_PACKET_DATA",
        "SENDING_READER_DATA",
        "STARTING_PAYMENT_ON_READER",
        "RECEIVED_READER_WRITE_NOTIFY",
        "RECEIVED_READER_DATA",
        "RECEIVED_READER_COMPLETION",
        "PLAYED_AUDIO_MESSAGE",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum ABLE_TO_START_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum CARDREADER_DISCONNECTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum CARDREADER_ERROR:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum CARDREADER_START_FAILED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum CARDREADER_START_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum NETWORK_FAILURE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum NETWORK_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum PLAYED_AUDIO_MESSAGE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_READER_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_READER_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_TMN_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum RECEIVED_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum SENDING_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum SENDING_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum SENT_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum SHOULD_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum SHOULD_NOT_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum TMN_TRANSACTION_CARD_WRITE_STARTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

.field public static final enum TMN_TRANSACTION_COMPLETED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x16

    new-array v0, v0, [Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x0

    const-string v3, "RECEIVED_TMN_COMPLETION"

    const-string v4, "TMN Transaction Completion From Cardreader"

    .line 25
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x1

    const-string v3, "RECEIVED_TMN_DATA"

    const-string v4, "Received Tmn Data From Cardreader"

    .line 26
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x2

    const-string v3, "RECEIVED_TMN_WRITE_NOTIFY"

    const-string v4, "TMN Write Notify From Cardreader"

    .line 27
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x3

    const-string v3, "CARDREADER_START_SUCCESS"

    const-string v4, "Cardreader Start Success"

    .line 28
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_START_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x4

    const-string v3, "CARDREADER_START_FAILED"

    const-string v4, "Cardreader Start Failed "

    .line 29
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_START_FAILED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x5

    const-string v3, "CARDREADER_ERROR"

    const-string v4, "Cardreader Error"

    .line 30
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_ERROR:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x6

    const-string v3, "CARDREADER_DISCONNECTED"

    const-string v4, "Cardreader Disconnected"

    .line 31
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->CARDREADER_DISCONNECTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/4 v2, 0x7

    const-string v3, "NETWORK_FAILURE"

    const-string v4, "Network Failure"

    .line 32
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->NETWORK_FAILURE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x8

    const-string v3, "NETWORK_SUCCESS"

    const-string v4, "Network Success"

    .line 33
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->NETWORK_SUCCESS:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x9

    const-string v3, "TMN_TRANSACTION_COMPLETED"

    const-string v4, "TMN Transaction Completed"

    .line 36
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->TMN_TRANSACTION_COMPLETED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xa

    const-string v3, "TMN_TRANSACTION_CARD_WRITE_STARTED"

    const-string v4, "TMN card write started"

    .line 37
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->TMN_TRANSACTION_CARD_WRITE_STARTED:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xb

    const-string v3, "ABLE_TO_START_MIRYO"

    const-string v4, "Able To Start Miryo"

    .line 38
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->ABLE_TO_START_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xc

    const-string v3, "SHOULD_ENTER_MIRYO"

    const-string v4, "Should Enter Miryo State After Failure"

    .line 39
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SHOULD_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xd

    const-string v3, "SHOULD_NOT_ENTER_MIRYO"

    const-string v4, "Should Not Enter Miryo State After Failure"

    .line 40
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SHOULD_NOT_ENTER_MIRYO:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xe

    const-string v3, "SENDING_PACKET_DATA"

    const-string v4, "[TMN] INFO: Sending Packet Data"

    .line 42
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENDING_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "SENT_PACKET_DATA"

    const/16 v3, 0xf

    const-string v4, "[TMN] INFO: Sent Packet Data"

    .line 43
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENT_PACKET_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "SENDING_READER_DATA"

    const/16 v3, 0x10

    const-string v4, "[TMN] INFO: Sending Reader Data"

    .line 44
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->SENDING_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "STARTING_PAYMENT_ON_READER"

    const/16 v3, 0x11

    const-string v4, "[TMN] INFO: Starting Payment On Reader"

    .line 45
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->STARTING_PAYMENT_ON_READER:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "RECEIVED_READER_WRITE_NOTIFY"

    const/16 v3, 0x12

    const-string v4, "[TMN] INFO: Received Reader Write Notify"

    .line 46
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_WRITE_NOTIFY:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "RECEIVED_READER_DATA"

    const/16 v3, 0x13

    const-string v4, "[TMN] INFO: Received Reader Data"

    .line 47
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_DATA:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "RECEIVED_READER_COMPLETION"

    const/16 v3, 0x14

    const-string v4, "[TMN] INFO: Received Reader Completion"

    .line 48
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->RECEIVED_READER_COMPLETION:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const-string v2, "PLAYED_AUDIO_MESSAGE"

    const/16 v3, 0x15

    const-string v4, "[TMN] INFO: Played Audio Message"

    .line 49
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->PLAYED_AUDIO_MESSAGE:Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->$VALUES:[Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;
    .locals 1

    const-class v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;
    .locals 1

    sget-object v0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->$VALUES:[Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    invoke-virtual {v0}, [Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/tmn/logging/TmnEvents$TmnEventValue;->value:Ljava/lang/String;

    return-object v0
.end method
