.class public final Lcom/squareup/tmn/TmnTimingsEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "TmnTimings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000b\n\u0002\u0010\u000e\n\u0002\u0008+\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0080\u0008\u0018\u00002\u00020\u0001B\u0087\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010\u0015J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u0005H\u00c6\u0003J\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\u0003H\u00c6\u0003J\t\u0010/\u001a\u00020\u0011H\u00c6\u0003J\t\u00100\u001a\u00020\u0003H\u00c6\u0003J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\t\u00103\u001a\u00020\u0005H\u00c6\u0003J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0003H\u00c6\u0003J\t\u00106\u001a\u00020\u0003H\u00c6\u0003J\t\u00107\u001a\u00020\u0003H\u00c6\u0003J\t\u00108\u001a\u00020\u0005H\u00c6\u0003J\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\t\u0010:\u001a\u00020\u0005H\u00c6\u0003J\u00ab\u0001\u0010;\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u0008\u0008\u0002\u0010\r\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0002\u0010\u0012\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00032\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0011H\u00c6\u0001J\u0013\u0010<\u001a\u00020=2\u0008\u0010>\u001a\u0004\u0018\u00010?H\u00d6\u0003J\t\u0010@\u001a\u00020\u0005H\u00d6\u0001J\t\u0010A\u001a\u00020\u0011H\u00d6\u0001R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0017R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u0005X\u0086D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0017R\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001eR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001eR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u001eR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u001eR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u001eR\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u001eR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001eR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u001eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010\u0017R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\u001a\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/tmn/TmnTimingsEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "totalServerMilliseconds",
        "",
        "totalServerRequestCount",
        "",
        "totalBLEWriteTimeMilliseconds",
        "tapToBeepMilliseconds",
        "tapToBeepInferredClientMilliseconds",
        "tapToBeepServerMilliseconds",
        "tapToBeepServerRequestCount",
        "tapToBeepBLEWriteMilliseconds",
        "connectionIntervalMilliseconds",
        "MTU",
        "tapToBeepPosBleStackTimeMilliseconds",
        "tapToBeepLcrTimeMilliseconds",
        "transactionId",
        "",
        "tapToBeepBLEReadTimeMilliseconds",
        "tapToBeepBLEReadMaxMilliseconds",
        "connectionNetworkType",
        "(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)V",
        "getMTU",
        "()I",
        "getConnectionIntervalMilliseconds",
        "getConnectionNetworkType",
        "()Ljava/lang/String;",
        "schemaVersion",
        "getSchemaVersion",
        "getTapToBeepBLEReadMaxMilliseconds",
        "()J",
        "getTapToBeepBLEReadTimeMilliseconds",
        "getTapToBeepBLEWriteMilliseconds",
        "getTapToBeepInferredClientMilliseconds",
        "getTapToBeepLcrTimeMilliseconds",
        "getTapToBeepMilliseconds",
        "getTapToBeepPosBleStackTimeMilliseconds",
        "getTapToBeepServerMilliseconds",
        "getTapToBeepServerRequestCount",
        "getTotalBLEWriteTimeMilliseconds",
        "getTotalServerMilliseconds",
        "getTotalServerRequestCount",
        "getTransactionId",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "tmn-timings_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final MTU:I

.field private final connectionIntervalMilliseconds:I

.field private final connectionNetworkType:Ljava/lang/String;

.field private final schemaVersion:I

.field private final tapToBeepBLEReadMaxMilliseconds:J

.field private final tapToBeepBLEReadTimeMilliseconds:J

.field private final tapToBeepBLEWriteMilliseconds:J

.field private final tapToBeepInferredClientMilliseconds:J

.field private final tapToBeepLcrTimeMilliseconds:J

.field private final tapToBeepMilliseconds:J

.field private final tapToBeepPosBleStackTimeMilliseconds:J

.field private final tapToBeepServerMilliseconds:J

.field private final tapToBeepServerRequestCount:I

.field private final totalBLEWriteTimeMilliseconds:J

.field private final totalServerMilliseconds:J

.field private final totalServerRequestCount:I

.field private final transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)V
    .locals 4

    move-object v0, p0

    move-object/from16 v1, p21

    const-string/jumbo v2, "transactionId"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 350
    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v3, "EMoney: Transaction Timings"

    invoke-direct {p0, v2, v3}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    move-wide v2, p1

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    move v2, p3

    iput v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    move-wide v2, p4

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    move-wide v2, p6

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    move-wide v2, p8

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    move-wide v2, p10

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    move/from16 v2, p12

    iput v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    move-wide/from16 v2, p13

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    move/from16 v2, p15

    iput v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    move/from16 v2, p16

    iput v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    move-wide/from16 v2, p17

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    move-wide/from16 v2, p19

    iput-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    iput-object v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    move-wide/from16 v1, p22

    iput-wide v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    move-wide/from16 v1, p24

    iput-wide v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    move-object/from16 v1, p26

    iput-object v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    const/4 v1, 0x3

    .line 351
    iput v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->schemaVersion:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/tmn/TmnTimingsEvent;JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/tmn/TmnTimingsEvent;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p27

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    goto :goto_0

    :cond_0
    move-wide/from16 v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget v4, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    goto :goto_1

    :cond_1
    move/from16 v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget-wide v5, v0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    goto :goto_2

    :cond_2
    move-wide/from16 v5, p4

    :goto_2
    and-int/lit8 v7, v1, 0x8

    if-eqz v7, :cond_3

    iget-wide v7, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    goto :goto_3

    :cond_3
    move-wide/from16 v7, p6

    :goto_3
    and-int/lit8 v9, v1, 0x10

    if-eqz v9, :cond_4

    iget-wide v9, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    goto :goto_4

    :cond_4
    move-wide/from16 v9, p8

    :goto_4
    and-int/lit8 v11, v1, 0x20

    if-eqz v11, :cond_5

    iget-wide v11, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    goto :goto_5

    :cond_5
    move-wide/from16 v11, p10

    :goto_5
    and-int/lit8 v13, v1, 0x40

    if-eqz v13, :cond_6

    iget v13, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    goto :goto_6

    :cond_6
    move/from16 v13, p12

    :goto_6
    and-int/lit16 v14, v1, 0x80

    if-eqz v14, :cond_7

    iget-wide v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    goto :goto_7

    :cond_7
    move-wide/from16 v14, p13

    :goto_7
    move-wide/from16 p13, v14

    and-int/lit16 v14, v1, 0x100

    if-eqz v14, :cond_8

    iget v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    goto :goto_8

    :cond_8
    move/from16 v14, p15

    :goto_8
    and-int/lit16 v15, v1, 0x200

    if-eqz v15, :cond_9

    iget v15, v0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    goto :goto_9

    :cond_9
    move/from16 v15, p16

    :goto_9
    move/from16 p16, v15

    and-int/lit16 v15, v1, 0x400

    move/from16 p15, v14

    if-eqz v15, :cond_a

    iget-wide v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    goto :goto_a

    :cond_a
    move-wide/from16 v14, p17

    :goto_a
    move-wide/from16 p17, v14

    and-int/lit16 v14, v1, 0x800

    if-eqz v14, :cond_b

    iget-wide v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    goto :goto_b

    :cond_b
    move-wide/from16 v14, p19

    :goto_b
    move-wide/from16 p19, v14

    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p21

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    move-object/from16 p21, v14

    if-eqz v15, :cond_d

    iget-wide v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    goto :goto_d

    :cond_d
    move-wide/from16 v14, p22

    :goto_d
    move-wide/from16 p22, v14

    and-int/lit16 v14, v1, 0x4000

    if-eqz v14, :cond_e

    iget-wide v14, v0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    goto :goto_e

    :cond_e
    move-wide/from16 v14, p24

    :goto_e
    const v16, 0x8000

    and-int v1, v1, v16

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    goto :goto_f

    :cond_f
    move-object/from16 v1, p26

    :goto_f
    move-wide/from16 p1, v2

    move/from16 p3, v4

    move-wide/from16 p4, v5

    move-wide/from16 p6, v7

    move-wide/from16 p8, v9

    move-wide/from16 p10, v11

    move/from16 p12, v13

    move-wide/from16 p24, v14

    move-object/from16 p26, v1

    invoke-virtual/range {p0 .. p26}, Lcom/squareup/tmn/TmnTimingsEvent;->copy(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)Lcom/squareup/tmn/TmnTimingsEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    return-wide v0
.end method

.method public final component10()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    return v0
.end method

.method public final component11()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    return-wide v0
.end method

.method public final component12()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    return-wide v0
.end method

.method public final component13()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component14()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    return-wide v0
.end method

.method public final component15()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    return-wide v0
.end method

.method public final component16()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    return v0
.end method

.method public final component3()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    return-wide v0
.end method

.method public final component4()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    return-wide v0
.end method

.method public final component5()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    return-wide v0
.end method

.method public final component6()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    return-wide v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    return v0
.end method

.method public final component8()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    return-wide v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    return v0
.end method

.method public final copy(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)Lcom/squareup/tmn/TmnTimingsEvent;
    .locals 28

    move-wide/from16 v1, p1

    move/from16 v3, p3

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move-wide/from16 v13, p13

    move/from16 v15, p15

    move/from16 v16, p16

    move-wide/from16 v17, p17

    move-wide/from16 v19, p19

    move-object/from16 v21, p21

    move-wide/from16 v22, p22

    move-wide/from16 v24, p24

    move-object/from16 v26, p26

    const-string/jumbo v0, "transactionId"

    move-object/from16 v1, p21

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v27, Lcom/squareup/tmn/TmnTimingsEvent;

    move-object/from16 v0, v27

    move-wide/from16 v1, p1

    invoke-direct/range {v0 .. v26}, Lcom/squareup/tmn/TmnTimingsEvent;-><init>(JIJJJJIJIIJJLjava/lang/String;JJLjava/lang/String;)V

    return-object v27
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tmn/TmnTimingsEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tmn/TmnTimingsEvent;

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    iget v1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    iget v1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    iget v1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    iget v1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    iget-wide v2, p1, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConnectionIntervalMilliseconds()I
    .locals 1

    .line 342
    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    return v0
.end method

.method public final getConnectionNetworkType()Ljava/lang/String;
    .locals 1

    .line 349
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    return-object v0
.end method

.method public final getMTU()I
    .locals 1

    .line 343
    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    return v0
.end method

.method public final getSchemaVersion()I
    .locals 1

    .line 351
    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->schemaVersion:I

    return v0
.end method

.method public final getTapToBeepBLEReadMaxMilliseconds()J
    .locals 2

    .line 348
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepBLEReadTimeMilliseconds()J
    .locals 2

    .line 347
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepBLEWriteMilliseconds()J
    .locals 2

    .line 341
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepInferredClientMilliseconds()J
    .locals 2

    .line 338
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepLcrTimeMilliseconds()J
    .locals 2

    .line 345
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepMilliseconds()J
    .locals 2

    .line 337
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepPosBleStackTimeMilliseconds()J
    .locals 2

    .line 344
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepServerMilliseconds()J
    .locals 2

    .line 339
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    return-wide v0
.end method

.method public final getTapToBeepServerRequestCount()I
    .locals 1

    .line 340
    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    return v0
.end method

.method public final getTotalBLEWriteTimeMilliseconds()J
    .locals 2

    .line 336
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    return-wide v0
.end method

.method public final getTotalServerMilliseconds()J
    .locals 2

    .line 334
    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    return-wide v0
.end method

.method public final getTotalServerRequestCount()I
    .locals 1

    .line 335
    iget v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    return v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    .line 346
    iget-object v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    invoke-static {v3, v4}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    invoke-static {v3, v4}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TmnTimingsEvent(totalServerMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", totalServerRequestCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalServerRequestCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", totalBLEWriteTimeMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->totalBLEWriteTimeMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepInferredClientMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepInferredClientMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepServerMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepServerRequestCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepServerRequestCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepBLEWriteMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEWriteMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", connectionIntervalMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionIntervalMilliseconds:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", MTU="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->MTU:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepPosBleStackTimeMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepPosBleStackTimeMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepLcrTimeMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepLcrTimeMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepBLEReadTimeMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadTimeMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tapToBeepBLEReadMaxMilliseconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->tapToBeepBLEReadMaxMilliseconds:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", connectionNetworkType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/tmn/TmnTimingsEvent;->connectionNetworkType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
