.class public final Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$2;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;-><init>(Lcom/squareup/thread/executor/MainThread;Ljava/util/List;Ldagger/Lazy;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$1\n+ 2 RealTutorialCore.kt\ncom/squareup/tutorialv2/RealTutorialCore\n*L\n1#1,1655:1\n101#2:1656\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0001*\u00020\u00032\u0006\u0010\u0005\u001a\u0002H\u00022\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T1",
        "",
        "T2",
        "t1",
        "t2",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic receiver$0$inlined:Lcom/squareup/tutorialv2/RealTutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/tutorialv2/RealTutorialCore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$2;->receiver$0$inlined:Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    const-string v0, "t1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p2, Ljava/util/List;

    check-cast p1, Lcom/squareup/tutorialv2/TutorialAndPriority;

    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore$$special$$inlined$combineLatest$2;->receiver$0$inlined:Lcom/squareup/tutorialv2/RealTutorialCore;

    .line 1656
    invoke-static {v0, p1, p2}, Lcom/squareup/tutorialv2/RealTutorialCore;->access$maybeProduceSeed(Lcom/squareup/tutorialv2/RealTutorialCore;Lcom/squareup/tutorialv2/TutorialAndPriority;Ljava/util/List;)Lcom/squareup/tutorialv2/TutorialSeed;

    move-result-object p1

    return-object p1
.end method
