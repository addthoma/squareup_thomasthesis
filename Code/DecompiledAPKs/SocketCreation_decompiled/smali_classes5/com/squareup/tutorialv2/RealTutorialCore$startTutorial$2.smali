.class final Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;->startTutorial(Lmortar/MortarScope;Lcom/squareup/tutorialv2/TutorialSeed;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $started:Lcom/squareup/tutorialv2/Tutorial;

.field final synthetic $tutorialSeed:Lcom/squareup/tutorialv2/TutorialSeed;

.field final synthetic this$0:Lcom/squareup/tutorialv2/RealTutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/tutorialv2/RealTutorialCore;Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->this$0:Lcom/squareup/tutorialv2/RealTutorialCore;

    iput-object p2, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->$started:Lcom/squareup/tutorialv2/Tutorial;

    iput-object p3, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->$tutorialSeed:Lcom/squareup/tutorialv2/TutorialSeed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 283
    iget-object v0, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->this$0:Lcom/squareup/tutorialv2/RealTutorialCore;

    invoke-static {v0}, Lcom/squareup/tutorialv2/RealTutorialCore;->access$getActive$p(Lcom/squareup/tutorialv2/RealTutorialCore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    new-instance v1, Lcom/squareup/tutorialv2/TutorialAndPriority;

    iget-object v2, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->$started:Lcom/squareup/tutorialv2/Tutorial;

    iget-object v3, p0, Lcom/squareup/tutorialv2/RealTutorialCore$startTutorial$2;->$tutorialSeed:Lcom/squareup/tutorialv2/TutorialSeed;

    invoke-virtual {v3}, Lcom/squareup/tutorialv2/TutorialSeed;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/tutorialv2/TutorialAndPriority;-><init>(Lcom/squareup/tutorialv2/Tutorial;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
