.class final Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "+",
        "Lcom/squareup/tutorialv2/Tutorial;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;

    invoke-direct {v0}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;-><init>()V

    sput-object v0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 64
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$7;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;",
            "+",
            "Lcom/squareup/tutorialv2/Tutorial;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tutorialv2/Tutorial;

    const-string/jumbo v1, "tutorial"

    .line 214
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$TutorialEvent;->sendToTutorial(Lcom/squareup/tutorialv2/Tutorial;)V

    return-void
.end method
