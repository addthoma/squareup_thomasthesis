.class final Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;
.super Ljava/lang/Object;
.source "RealTutorialCore.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tutorialv2/RealTutorialCore;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/tutorialv2/TutorialState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "tutorialState",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;

    invoke-direct {v0}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;-><init>()V

    sput-object v0, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;->INSTANCE:Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/tutorialv2/TutorialState;)Z
    .locals 1

    const-string/jumbo v0, "tutorialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p0, p1}, Lcom/squareup/tutorialv2/RealTutorialCore$onEnterScope$11;->test(Lcom/squareup/tutorialv2/TutorialState;)Z

    move-result p1

    return p1
.end method
