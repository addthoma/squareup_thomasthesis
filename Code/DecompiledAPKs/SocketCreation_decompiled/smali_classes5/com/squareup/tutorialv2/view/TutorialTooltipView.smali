.class public Lcom/squareup/tutorialv2/view/TutorialTooltipView;
.super Landroid/widget/FrameLayout;
.source "TutorialTooltipView.java"

# interfaces
.implements Lcom/squareup/tutorialv2/view/TutorialView;


# instance fields
.field private anchor:Landroid/view/View;

.field private final anchorLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final anchorScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field private arrowBottom:Lcom/squareup/widgets/TriangleView;

.field private arrowTop:Lcom/squareup/widgets/TriangleView;

.field private final attachListener:Landroid/view/View$OnAttachStateChangeListener;

.field private content:Lcom/squareup/marketfont/MarketTextView;

.field private contentRoot:Landroid/view/ViewGroup;

.field private final controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

.field private desiredPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

.field private exitGlyph:Lcom/squareup/glyph/SquareGlyphView;

.field private final popupWindow:Landroid/widget/PopupWindow;

.field private primaryButton:Lcom/squareup/marketfont/MarketButton;

.field private secondaryButton:Lcom/squareup/marketfont/MarketButton;

.field private steps:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    new-instance p1, Lcom/squareup/tutorialv2/view/TutorialTooltipView$1;

    invoke-direct {p1, p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView$1;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->attachListener:Landroid/view/View$OnAttachStateChangeListener;

    .line 90
    new-instance p1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$yd--FUIy5SnSFdypGRtPF-DAZQY;

    invoke-direct {p1, p0}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$yd--FUIy5SnSFdypGRtPF-DAZQY;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 91
    new-instance p1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$MqxcjFw3eYZ5-hsCf5JG9O1Yz0E;

    invoke-direct {p1, p0}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$MqxcjFw3eYZ5-hsCf5JG9O1Yz0E;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 93
    new-instance p1, Landroid/widget/PopupWindow;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/common/tutorial/R$dimen;->tutorial2_tooltip_width:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    const/4 v0, -0x2

    invoke-direct {p1, p0, p2, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    .line 97
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 99
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->isLandscape()Z

    move-result p1

    if-nez p1, :cond_0

    .line 103
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {p1, p2}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 105
    :cond_0
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 108
    new-instance p2, Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    invoke-direct {p2, v0, p1}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;-><init>(Landroid/widget/PopupWindow;Landroid/content/res/Resources;)V

    iput-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    .line 109
    iget-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 109
    invoke-virtual {p2, p1}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->setVerticalOverflow(I)V

    return-void
.end method

.method private bindToAnchor(Landroid/view/View;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 199
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->unbindAnchor()V

    .line 201
    :cond_0
    iput-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    .line 202
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->attachListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 204
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 207
    new-instance p1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$b3MPPbS5OJZQ-y5Ryxhi0njYwmY;

    invoke-direct {p1, p0}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$b3MPPbS5OJZQ-y5Ryxhi0njYwmY;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V

    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->whenViewsAreMeasured(Ljava/lang/Runnable;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 234
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_tooltip_content_root:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->contentRoot:Landroid/view/ViewGroup;

    .line 235
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_arrow_top:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/TriangleView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowTop:Lcom/squareup/widgets/TriangleView;

    .line 236
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial_arrow_bottom:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/TriangleView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowBottom:Lcom/squareup/widgets/TriangleView;

    .line 237
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_cancel:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->exitGlyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 238
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->content:Lcom/squareup/marketfont/MarketTextView;

    .line 239
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_progress_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->steps:Lcom/squareup/marketfont/MarketTextView;

    .line 240
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    .line 241
    sget v0, Lcom/squareup/common/tutorial/R$id;->tutorial2_secondary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/squareup/tutorialv2/view/TutorialView;
    .locals 2

    .line 114
    sget v0, Lcom/squareup/common/tutorial/R$layout;->tutorial2_tooltip_view:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;

    return-object p0
.end method

.method private getScreenOrientation()I
    .locals 1

    .line 249
    invoke-static {p0}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    return v0
.end method

.method private isLandscape()Z
    .locals 2

    .line 245
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->getScreenOrientation()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$MqxcjFw3eYZ5-hsCf5JG9O1Yz0E(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->repositionTooltip()V

    return-void
.end method

.method static synthetic lambda$null$3(Ljava/lang/Runnable;Landroid/view/View;II)V
    .locals 0

    .line 226
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method static synthetic lambda$setOnInteractionListener$1(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
    .locals 0

    if-eqz p0, :cond_0

    .line 127
    invoke-interface {p0}, Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;->onDismissed()V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$yd--FUIy5SnSFdypGRtPF-DAZQY(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->repositionTooltip()V

    return-void
.end method

.method private repositionTooltip()V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    iget-object v2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->desiredPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->alignToAnchor(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V

    return-void
.end method

.method private unbindAnchor()V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->attachListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    const/4 v0, 0x0

    .line 219
    iput-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    :cond_0
    return-void
.end method

.method private whenViewsAreMeasured(Ljava/lang/Runnable;)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    new-instance v1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$vIF-reRgCg-60K7MdibZQ4NEg7M;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$vIF-reRgCg-60K7MdibZQ4NEg7M;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;Ljava/lang/Runnable;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .line 154
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->unbindAnchor()V

    .line 155
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    invoke-virtual {v0}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->unbindArrowViews()V

    .line 156
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method

.method public hidePrimaryButton()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method public hideSecondaryButton()V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$bindToAnchor$2$TutorialTooltipView()V
    .locals 2

    .line 208
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->repositionTooltip()V

    .line 209
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorScrollListener:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchorLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$TutorialTooltipView()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->anchor:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/tutorialv2/view/TutorialScroller;->scrollViewIntoScreen(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$whenViewsAreMeasured$4$TutorialTooltipView(Ljava/lang/Runnable;Landroid/view/View;II)V
    .locals 0

    .line 225
    new-instance p2, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$S1CzMi3deX2LqCaWdJHk2BfnHA4;

    invoke-direct {p2, p1}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$S1CzMi3deX2LqCaWdJHk2BfnHA4;-><init>(Ljava/lang/Runnable;)V

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 118
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 119
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->bindViews()V

    .line 120
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->contentRoot:Landroid/view/ViewGroup;

    new-instance v1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$Ahjz81toHz1jt-giVK8u54WxlMc;

    invoke-direct {v1, p0}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$Ahjz81toHz1jt-giVK8u54WxlMc;-><init>(Lcom/squareup/tutorialv2/view/TutorialTooltipView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 180
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 181
    invoke-direct {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->repositionTooltip()V

    return-void
.end method

.method public setCloseButtonColor(I)V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->exitGlyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    return-void
.end method

.method public setContentText(I)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->content:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method public setContentText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->content:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnInteractionListener(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->exitGlyph:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$bdNTMsuJx2HjErMjXIrCjXw9w5k;

    invoke-direct {v1, p1}, Lcom/squareup/tutorialv2/view/-$$Lambda$TutorialTooltipView$bdNTMsuJx2HjErMjXIrCjXw9w5k;-><init>(Lcom/squareup/tutorialv2/view/TutorialView$OnInteractionListener;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setPrimaryButton(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 161
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method public setSecondaryButton(ILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 171
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {p2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object p1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method public setStepsText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->steps:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->steps:Lcom/squareup/marketfont/MarketTextView;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setViewBackgroundColor(I)V
    .locals 1

    .line 185
    invoke-virtual {p0}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    .line 186
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->contentRoot:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    .line 187
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowBottom:Lcom/squareup/widgets/TriangleView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/TriangleView;->setTriangleColor(I)V

    .line 188
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowTop:Lcom/squareup/widgets/TriangleView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/TriangleView;->setTriangleColor(I)V

    .line 189
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->primaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    .line 190
    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->secondaryButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setTextColor(I)V

    return-void
.end method

.method public show(Landroid/view/View;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;)V
    .locals 2

    const-string v0, "Position cannot be null for a tooltip"

    .line 147
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 148
    iput-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->desiredPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    .line 149
    iget-object p2, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->controller:Lcom/squareup/tutorialv2/view/TutorialTooltipController;

    iget-object v0, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowTop:Lcom/squareup/widgets/TriangleView;

    iget-object v1, p0, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->arrowBottom:Lcom/squareup/widgets/TriangleView;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/tutorialv2/view/TutorialTooltipController;->bindArrowViews(Landroid/view/View;Landroid/view/View;)V

    .line 150
    invoke-direct {p0, p1}, Lcom/squareup/tutorialv2/view/TutorialTooltipView;->bindToAnchor(Landroid/view/View;)V

    return-void
.end method
