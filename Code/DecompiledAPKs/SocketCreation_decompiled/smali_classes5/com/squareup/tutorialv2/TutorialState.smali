.class public final Lcom/squareup/tutorialv2/TutorialState;
.super Ljava/lang/Object;
.source "TutorialState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tutorialv2/TutorialState$AnchorState;,
        Lcom/squareup/tutorialv2/TutorialState$TooltipAlignment;,
        Lcom/squareup/tutorialv2/TutorialState$TooltipGravity;,
        Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;,
        Lcom/squareup/tutorialv2/TutorialState$Builder;,
        Lcom/squareup/tutorialv2/TutorialState$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTutorialState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TutorialState.kt\ncom/squareup/tutorialv2/TutorialState\n*L\n1#1,335:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0017\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u000c\u0008\u0086\u0008\u0018\u0000 32\u00020\u0001:\u0006123456B\u009d\u0001\u0008\u0002\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0001\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\u000e\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010\u0012\u0008\u0008\u0003\u0010\u0011\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0010\u0012\u0008\u0008\u0003\u0010\u0013\u001a\u00020\u0007\u0012\u0008\u0008\u0003\u0010\u0014\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0015J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0010H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0005H\u00c2\u0003J\t\u0010 \u001a\u00020\u0007H\u00c2\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010$\u001a\u00020\u0007H\u00c6\u0003J\t\u0010%\u001a\u00020\u0007H\u00c6\u0003J\t\u0010&\u001a\u00020\u0007H\u00c6\u0003J\u000e\u0010\u0004\u001a\u00020\u00052\u0006\u0010\'\u001a\u00020(J\u009f\u0001\u0010)\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u00072\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00072\u0008\u0008\u0002\u0010\r\u001a\u00020\u00072\u0008\u0008\u0003\u0010\u000e\u001a\u00020\u00072\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0008\u0008\u0003\u0010\u0011\u001a\u00020\u00072\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00102\u0008\u0008\u0003\u0010\u0013\u001a\u00020\u00072\u0008\u0008\u0003\u0010\u0014\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010*\u001a\u00020+2\u0008\u0010,\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010-\u001a\u00020+J\u0006\u0010.\u001a\u00020+J\t\u0010/\u001a\u00020\u0007H\u00d6\u0001J\u0008\u00100\u001a\u00020\u0010H\u0016R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u0010\u0010\u0008\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00108\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u00018\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/tutorialv2/TutorialState;",
        "",
        "anchorState",
        "Lcom/squareup/tutorialv2/TutorialState$AnchorState;",
        "content",
        "",
        "contentId",
        "",
        "idAnchor",
        "tagAnchor",
        "tooltipPosition",
        "Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;",
        "step",
        "stepCount",
        "primaryButtonTextId",
        "primaryButtonEvent",
        "",
        "secondaryButtonTextId",
        "secondaryButtonEvent",
        "backgroundColor",
        "closeButtonColor",
        "(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)V",
        "contentOrContentId",
        "getContentOrContentId",
        "()Ljava/lang/CharSequence;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "resources",
        "Landroid/content/res/Resources;",
        "copy",
        "equals",
        "",
        "other",
        "hasNoContent",
        "hasStep",
        "hashCode",
        "toString",
        "AnchorState",
        "Builder",
        "Companion",
        "TooltipAlignment",
        "TooltipGravity",
        "TooltipPosition",
        "tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CLEAR:Lcom/squareup/tutorialv2/TutorialState;

.field public static final Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

.field public static final FINISHED:Lcom/squareup/tutorialv2/TutorialState;

.field public static final HIDE_BUTTON:I = -0x1


# instance fields
.field public final anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

.field public final backgroundColor:I

.field public final closeButtonColor:I

.field private final content:Ljava/lang/CharSequence;

.field private final contentId:I

.field public final idAnchor:I

.field public final primaryButtonEvent:Ljava/lang/String;

.field public final primaryButtonTextId:I

.field public final secondaryButtonEvent:Ljava/lang/String;

.field public final secondaryButtonTextId:I

.field public final step:I

.field public final stepCount:I

.field public final tagAnchor:Ljava/lang/Object;

.field public final tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;


# direct methods
.method static constructor <clinit>()V
    .locals 36

    new-instance v0, Lcom/squareup/tutorialv2/TutorialState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/tutorialv2/TutorialState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    .line 318
    new-instance v0, Lcom/squareup/tutorialv2/TutorialState;

    move-object v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x3fff

    const/16 v18, 0x0

    invoke-direct/range {v2 .. v18}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    .line 320
    new-instance v0, Lcom/squareup/tutorialv2/TutorialState;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x3fff

    const/16 v35, 0x0

    invoke-direct/range {v19 .. v35}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iput-object p2, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    iput p3, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    iput p4, p0, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    iput-object p5, p0, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    iput-object p6, p0, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    iput p7, p0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    iput p8, p0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    iput p9, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    iput-object p10, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    iput p11, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    iput-object p12, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    iput p13, p0, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    iput p14, p0, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    .line 73
    iget-object p1, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    if-eqz p1, :cond_2

    .line 74
    iget p1, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot set content and contentId"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 14

    move/from16 v0, p15

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 29
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState$AnchorState;->NO_ANCHOR:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    .line 31
    move-object v2, v3

    check-cast v2, Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move-object/from16 v2, p2

    :goto_1
    and-int/lit8 v4, v0, 0x4

    const/4 v5, -0x1

    if-eqz v4, :cond_2

    const/4 v4, -0x1

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v6, v0, 0x8

    if-eqz v6, :cond_3

    const/4 v6, -0x1

    goto :goto_3

    :cond_3
    move/from16 v6, p4

    :goto_3
    and-int/lit8 v7, v0, 0x10

    if-eqz v7, :cond_4

    move-object v7, v3

    goto :goto_4

    :cond_4
    move-object/from16 v7, p5

    :goto_4
    and-int/lit8 v8, v0, 0x20

    if-eqz v8, :cond_5

    .line 42
    move-object v8, v3

    check-cast v8, Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    goto :goto_5

    :cond_5
    move-object/from16 v8, p6

    :goto_5
    and-int/lit8 v9, v0, 0x40

    const/4 v10, 0x0

    if-eqz v9, :cond_6

    const/4 v9, 0x0

    goto :goto_6

    :cond_6
    move/from16 v9, p7

    :goto_6
    and-int/lit16 v11, v0, 0x80

    if-eqz v11, :cond_7

    goto :goto_7

    :cond_7
    move/from16 v10, p8

    :goto_7
    and-int/lit16 v11, v0, 0x100

    if-eqz v11, :cond_8

    const/4 v11, -0x1

    goto :goto_8

    :cond_8
    move/from16 v11, p9

    :goto_8
    and-int/lit16 v12, v0, 0x200

    if-eqz v12, :cond_9

    .line 60
    move-object v12, v3

    check-cast v12, Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v12, p10

    :goto_9
    and-int/lit16 v13, v0, 0x400

    if-eqz v13, :cond_a

    goto :goto_a

    :cond_a
    move/from16 v5, p11

    :goto_a
    and-int/lit16 v13, v0, 0x800

    if-eqz v13, :cond_b

    .line 64
    check-cast v3, Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v3, p12

    :goto_b
    and-int/lit16 v13, v0, 0x1000

    if-eqz v13, :cond_c

    .line 66
    sget v13, Lcom/squareup/marin/R$color;->marin_green:I

    goto :goto_c

    :cond_c
    move/from16 v13, p13

    :goto_c
    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_d

    .line 68
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    goto :goto_d

    :cond_d
    move/from16 v0, p14

    :goto_d
    move-object p1, p0

    move-object/from16 p2, v1

    move-object/from16 p3, v2

    move/from16 p4, v4

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move/from16 p10, v11

    move-object/from16 p11, v12

    move/from16 p12, v5

    move-object/from16 p13, v3

    move/from16 p14, v13

    move/from16 p15, v0

    invoke-direct/range {p1 .. p15}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct/range {p0 .. p14}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)V

    return-void
.end method

.method private final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    return v0
.end method

.method public static synthetic copy$default(Lcom/squareup/tutorialv2/TutorialState;Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;IIILjava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 15

    move-object v0, p0

    move/from16 v1, p15

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    goto :goto_3

    :cond_3
    move/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget v12, v0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget v14, v0, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    goto :goto_c

    :cond_c
    move/from16 v14, p13

    :goto_c
    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_d

    iget v1, v0, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    goto :goto_d

    :cond_d
    move/from16 v1, p14

    :goto_d
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move/from16 p3, v4

    move/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move-object/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v13

    move/from16 p13, v14

    move/from16 p14, v1

    invoke-virtual/range {p0 .. p14}, Lcom/squareup/tutorialv2/TutorialState;->copy(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method

.method public static final display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(I)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->Companion:Lcom/squareup/tutorialv2/TutorialState$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/tutorialv2/TutorialState$Companion;->display(Ljava/lang/CharSequence;)Lcom/squareup/tutorialv2/TutorialState$Builder;

    move-result-object p0

    return-object p0
.end method

.method private final getContentOrContentId()Ljava/lang/CharSequence;
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "contentId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/tutorialv2/TutorialState$AnchorState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    return-object v0
.end method

.method public final component11()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    return v0
.end method

.method public final component12()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    return-object v0
.end method

.method public final component13()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    return v0
.end method

.method public final component14()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    return v0
.end method

.method public final component5()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    return-object v0
.end method

.method public final component6()Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;
    .locals 1

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    return-object v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    return v0
.end method

.method public final content(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string p1, "resources.getText(contentId)"

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public final copy(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)Lcom/squareup/tutorialv2/TutorialState;
    .locals 16

    const-string v0, "anchorState"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tutorialv2/TutorialState;

    move-object v1, v0

    move-object/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move-object/from16 v13, p12

    move/from16 v14, p13

    move/from16 v15, p14

    invoke-direct/range {v1 .. v15}, Lcom/squareup/tutorialv2/TutorialState;-><init>(Lcom/squareup/tutorialv2/TutorialState$AnchorState;Ljava/lang/CharSequence;IILjava/lang/Object;Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;IIILjava/lang/String;ILjava/lang/String;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/tutorialv2/TutorialState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/tutorialv2/TutorialState;

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->step:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    iget v1, p1, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    iget p1, p1, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final hasNoContent()Z
    .locals 2

    .line 87
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasStep()Z
    .locals 1

    .line 91
    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/tutorialv2/TutorialState;->anchorState:Lcom/squareup/tutorialv2/TutorialState$AnchorState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialState;->content:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->contentId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->idAnchor:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialState;->tagAnchor:Ljava/lang/Object;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialState;->tooltipPosition:Lcom/squareup/tutorialv2/TutorialState$TooltipPosition;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->step:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->stepCount:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonTextId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialState;->primaryButtonEvent:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonTextId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/tutorialv2/TutorialState;->secondaryButtonEvent:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialState;->backgroundColor:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/tutorialv2/TutorialState;->closeButtonColor:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TutorialState{CLEAR}"

    goto :goto_0

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "TutorialState{FINISHED}"

    goto :goto_0

    .line 82
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TutorialState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialState;->getContentOrContentId()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
