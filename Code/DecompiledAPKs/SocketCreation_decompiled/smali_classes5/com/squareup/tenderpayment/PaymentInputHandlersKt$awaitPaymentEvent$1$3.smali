.class public final Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;
.super Ljava/lang/Object;
.source "PaymentInputHandlers.kt"

# interfaces
.implements Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3",
        "Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;",
        "contactlessReaderAdded",
        "",
        "cardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "contactlessReaderReadyForPayment",
        "contactlessReaderRemoved",
        "contactlessReaderTimedOut",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;


# direct methods
.method constructor <init>(Lio/reactivex/ObservableEmitter;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public contactlessReaderAdded(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v0, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;->INSTANCE:Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public contactlessReaderReadyForPayment(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v0, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;->INSTANCE:Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public contactlessReaderRemoved(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v0, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;->INSTANCE:Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public contactlessReaderTimedOut(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1

    const-string v0, "cardReaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/tenderpayment/PaymentInputHandlersKt$awaitPaymentEvent$1$3;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v0, Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;->INSTANCE:Lcom/squareup/tenderpayment/PaymentInputHandlerOutput$NfcStatusChanged;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
