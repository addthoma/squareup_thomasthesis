.class public abstract Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;
.super Ljava/lang/Object;
.source "TenderPaymentWorkflowState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;,
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;,
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;,
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;,
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;,
        Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0003\u0004\u0005\u0006\u0007\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\t\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;",
        "Landroid/os/Parcelable;",
        "()V",
        "InBuyerCheckout",
        "InPayOther",
        "InSelectMethod",
        "InSellerQuickCash",
        "InSeparateTender",
        "InTenderOption",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InBuyerCheckout;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSelectMethod;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InPayOther;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSellerQuickCash;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;",
        "Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InSeparateTender;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState;-><init>()V

    return-void
.end method
