.class public final Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption$Creator;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Creator"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    const-string v0, "in"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    const-class v1, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    const-class v2, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    invoke-direct {v0, v1, p1}, Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 0

    new-array p1, p1, [Lcom/squareup/tenderpayment/TenderPaymentWorkflowState$InTenderOption;

    return-object p1
.end method
