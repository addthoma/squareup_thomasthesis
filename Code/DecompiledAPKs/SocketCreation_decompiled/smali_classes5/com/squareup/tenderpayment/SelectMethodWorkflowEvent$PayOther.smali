.class public final Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;
.super Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.source "SelectMethodWorkflowEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PayOther"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;",
        "Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "tenderName",
        "",
        "(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V",
        "getTender",
        "()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getTenderName",
        "()Ljava/lang/String;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

.field private final tenderName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)V
    .locals 1

    const-string v0, "tender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tenderName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;->tenderName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    return-object v0
.end method

.method public final getTenderName()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodWorkflowEvent$PayOther;->tenderName:Ljava/lang/String;

    return-object v0
.end method
