.class public final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "SeparateTenderAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->setupViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$1",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    .line 294
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$getWorkflow$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountChanged;

    iget-object v1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$$inlined$with$lambda$1;->this$0:Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;

    invoke-static {v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->access$getAmountEntered$p(Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderEvent$CustomAmountChanged;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
