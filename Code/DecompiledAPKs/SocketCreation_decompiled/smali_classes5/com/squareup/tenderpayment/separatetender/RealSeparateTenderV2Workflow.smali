.class public final Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSeparateTenderV2Workflow.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomAmountAction;,
        Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$CustomEvenSplitAction;,
        Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$ConfirmCancelSeparateAction;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSeparateTenderV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSeparateTenderV2Workflow.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,298:1\n32#2,12:299\n*E\n*S KotlinDebug\n*F\n+ 1 RealSeparateTenderV2Workflow.kt\ncom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow\n*L\n56#1,12:299\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0003\u0019\u001a\u001bB\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u001a\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00022\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016JN\u0010\u0014\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00032\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0003H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderV2Workflow;",
        "x2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "ConfirmCancelSeparateAction",
        "CustomAmountAction",
        "CustomEvenSplitAction",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "x2SellerScreenRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p2, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getX2SellerScreenRunner$p(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;
    .locals 17

    const-string v0, "props"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 299
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->size()I

    move-result v2

    const/4 v3, 0x0

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v4, 0x0

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_3

    .line 304
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    const-string v4, "Parcel.obtain()"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    .line 306
    array-length v4, v0

    invoke-virtual {v2, v0, v3, v4}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 307
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 308
    const-class v0, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v0, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 310
    :cond_3
    check-cast v4, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    if-eqz v4, :cond_4

    move-object v0, v4

    goto :goto_2

    .line 56
    :cond_4
    new-instance v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    .line 57
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 58
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getMaxSplits()J

    move-result-wide v7

    .line 59
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v9

    .line 60
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v10

    .line 61
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v11

    .line 62
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;->getCompletedTenders()Ljava/util/List;

    move-result-object v12

    .line 63
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    const/16 v15, 0x80

    const/16 v16, 0x0

    move-object v5, v0

    .line 56
    invoke-direct/range {v5 .. v16}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;-><init>(Lcom/squareup/protos/common/Money;JLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_2
    return-object v0
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->initialState(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;

    check-cast p2, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->render(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderInput;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;",
            "-",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v8, 0x0

    .line 74
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getScreen()Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;

    move-result-object p1

    .line 75
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomAmountSplit;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    new-instance p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    .line 77
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$Splitting;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$Splitting;

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    .line 78
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 79
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 80
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 81
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 82
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v6

    .line 84
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCompletedTenders()Ljava/util/List;

    move-result-object v9

    move-object v0, p1

    .line 76
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V

    .line 87
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 88
    new-instance p3, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;

    invoke-direct {p3, p0, p2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$1;-><init>(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-static {p3}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 112
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderScreen(Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 114
    :cond_1
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$CustomEvenSplit;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 115
    new-instance p1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;

    .line 116
    sget-object v0, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$EvenSplit;->INSTANCE:Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState$EvenSplit;

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;

    .line 117
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountEntered()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 118
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getAmountRemaining()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 119
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getBillAmount()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 120
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplitAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 121
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getMaxSplits()J

    move-result-wide v6

    .line 123
    invoke-virtual {p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;->getCompletedTenders()Ljava/util/List;

    move-result-object v9

    move-object v0, p1

    .line 115
    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTender$CustomScreenState;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;JZLjava/util/List;)V

    .line 126
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 127
    new-instance p3, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$2;

    invoke-direct {p3, p0, p2}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$2;-><init>(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;Lcom/squareup/workflow/Sink;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-static {p3}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 139
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderCustomEvenSplitScreen(Lcom/squareup/tenderpayment/separatetender/SeparateTender$SeparateTenderData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_1

    .line 141
    :cond_2
    instance-of p2, p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen$ConfirmCancelSeparate;

    if-eqz p2, :cond_3

    .line 142
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 143
    new-instance p3, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$3;

    invoke-direct {p3, p0, p2, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow$render$inputs$3;-><init>(Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/tenderpayment/separatetender/SeparateTenderState$SeparateTenderScreen;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-static {p3}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 155
    invoke-static {p1}, Lcom/squareup/tenderpayment/separatetender/SeparateTenderScreenKt;->createSeparateTenderConfirmCancelScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/separatetender/RealSeparateTenderV2Workflow;->snapshotState(Lcom/squareup/tenderpayment/separatetender/SeparateTenderState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
