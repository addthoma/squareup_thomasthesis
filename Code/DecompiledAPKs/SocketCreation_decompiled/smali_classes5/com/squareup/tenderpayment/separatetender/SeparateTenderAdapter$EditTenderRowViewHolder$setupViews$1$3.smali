.class final Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;
.super Ljava/lang/Object;
.source "SeparateTenderAdapter.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder;->setupViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSeparateTenderAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SeparateTenderAdapter.kt\ncom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3\n*L\n1#1,462:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "hasFocus",
        "",
        "onFocusChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_with:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;->$this_with:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_2

    .line 314
    iget-object p1, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;->$this_with:Lcom/squareup/widgets/SelectableEditText;

    check-cast p1, Landroid/widget/EditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->requireText(Landroid/widget/EditText;)Landroid/text/Editable;

    move-result-object p1

    .line 315
    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 316
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 318
    :cond_1
    iget-object p2, p0, Lcom/squareup/tenderpayment/separatetender/SeparateTenderAdapter$EditTenderRowViewHolder$setupViews$1$3;->$this_with:Lcom/squareup/widgets/SelectableEditText;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setSelection(I)V

    :cond_2
    return-void
.end method
