.class public Lcom/squareup/tenderpayment/BaseTenderDiff;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "BaseTenderDiff.java"


# instance fields
.field private final newList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field

.field private final oldList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->oldList:Ljava/util/List;

    .line 16
    iput-object p2, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->newList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->oldList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/BaseTender;

    .line 35
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->newList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/payment/tender/BaseTender;

    .line 36
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->oldList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/BaseTender;

    iget-object p1, p1, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->newList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/payment/tender/BaseTender;

    iget-object p2, p2, Lcom/squareup/payment/tender/BaseTender;->clientId:Ljava/lang/String;

    .line 30
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getNewListSize()I
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->newList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/tenderpayment/BaseTenderDiff;->oldList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
