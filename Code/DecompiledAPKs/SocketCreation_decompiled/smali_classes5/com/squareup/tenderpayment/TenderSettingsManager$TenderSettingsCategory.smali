.class public final enum Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;
.super Ljava/lang/Enum;
.source "TenderSettingsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderSettingsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderSettingsCategory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field public static final enum DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field public static final enum PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

.field public static final enum SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 479
    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v1, 0x0

    const-string v2, "PRIMARY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v2, 0x1

    const-string v3, "SECONDARY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    new-instance v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v3, 0x2

    const-string v4, "DISABLED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    .line 478
    sget-object v4, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->PRIMARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->SECONDARY:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->$VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 478
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;
    .locals 1

    .line 478
    const-class v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;
    .locals 1

    .line 478
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->$VALUES:[Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-object v0
.end method
