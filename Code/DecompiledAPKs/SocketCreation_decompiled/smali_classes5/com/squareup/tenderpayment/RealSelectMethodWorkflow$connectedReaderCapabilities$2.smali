.class final Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;
.super Ljava/lang/Object;
.source "RealSelectMethodWorkflow.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->connectedReaderCapabilities()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012*\u0010\u0004\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/EnumSet;",
        "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
        "kotlin.jvm.PlatformType",
        "readerCapabilities",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Ljava/util/EnumSet;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;->call(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/util/EnumSet;)Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;)",
            "Ljava/util/EnumSet<",
            "Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;",
            ">;"
        }
    .end annotation

    .line 723
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow$connectedReaderCapabilities$2;->this$0:Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;

    invoke-static {v0}, Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;->access$shouldEnableNfcField(Lcom/squareup/tenderpayment/RealSelectMethodWorkflow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 724
    sget-object v0, Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;->SUPPORTS_TAP_AND_HAS_SS:Lcom/squareup/cardreader/CardReaderHubUtils$ConnectedReaderCapabilities;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-object p1
.end method
