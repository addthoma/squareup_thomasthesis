.class public Lcom/squareup/tenderpayment/SelectMethodScreensFactory;
.super Ljava/lang/Object;
.source "SelectMethodScreensFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J2\u0010\u0008\u001a\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000c2\u0006\u0010\r\u001a\u00020\u00062\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00100\u000fJ2\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\tj\u0002`\u00142\u0006\u0010\u0015\u001a\u00020\u00122\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00100\u000fJ2\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00180\tj\u0002`\u00192\u0006\u0010\u001a\u001a\u00020\u00172\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00100\u000fJ:\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\tj\u0002`\u001e2\u0006\u0010\u001f\u001a\u00020\u001c2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u001d0$J:\u0010%\u001a\u0012\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\tj\u0002`\u001e2\u0006\u0010\u001f\u001a\u00020\u001c2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u001d0$J&\u0010&\u001a\u0012\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\tj\u0002`\u001e2\u0006\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\'\u001a\u00020(J$\u0010)\u001a\u0012\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020*0\tj\u0002`+2\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00100-R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/tenderpayment/SelectMethodScreensFactory;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/text/Formatter;)V",
        "createCancelSplitTenderTransactionDialogScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogScreen;",
        "amountDue",
        "onEventHandler",
        "Lkotlin/Function1;",
        "",
        "createConfirmChargeCardOnFileDialogScreen",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
        "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialogScreen;",
        "confirmData",
        "createConfirmCollectCashDialogScreen",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
        "Lcom/squareup/tenderpayment/ConfirmCollectCashDialogScreen;",
        "dialogData",
        "createPrimarySelectMethodScreen",
        "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "Lcom/squareup/tenderpayment/SelectMethodScreen;",
        "selectMethodData",
        "completedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "selectMethodInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "createSecondarySelectMethodScreen",
        "createSelectMethodScreenForBackground",
        "showSecondaryMethods",
        "",
        "createSplitTenderWarningDialogScreen",
        "Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;",
        "Lcom/squareup/tenderpayment/SplitTenderWarningDialogScreen;",
        "onDismissed",
        "Lkotlin/Function0;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final createCancelSplitTenderTransactionDialogScreen(Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "amountDue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;

    .line 65
    sget v2, Lcom/squareup/tenderworkflow/R$string;->cancel_transaction:I

    .line 66
    sget v3, Lcom/squareup/tenderworkflow/R$string;->keep:I

    .line 67
    sget v4, Lcom/squareup/tenderworkflow/R$string;->cancel_split_tender_transaction_title:I

    .line 68
    sget v5, Lcom/squareup/tenderworkflow/R$string;->cancel_split_tender_transaction_message:I

    .line 69
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    .line 64
    invoke-direct/range {v1 .. v6}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;-><init>(IIIILjava/lang/String;)V

    .line 73
    new-instance p1, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createCancelSplitTenderTransactionDialogScreen$1;

    invoke-direct {p1, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createCancelSplitTenderTransactionDialogScreen$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 71
    invoke-static {v0, p1}, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogScreenKt;->CancelSplitTenderTransactionDialogScreen(Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createConfirmChargeCardOnFileDialogScreen(Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "confirmData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createConfirmChargeCardOnFileDialogScreen$1;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createConfirmChargeCardOnFileDialogScreen$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 88
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialogScreenKt;->ConfirmChargeCardOnFileDialogScreen(Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createConfirmCollectCashDialogScreen(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;",
            "Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "dialogData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createConfirmCollectCashDialogScreen$1;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createConfirmCollectCashDialogScreen$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 80
    invoke-static {p1, p2}, Lcom/squareup/tenderpayment/ConfirmCollectCashDialogScreenKt;->ConfirmCollectCashDialogScreen(Lcom/squareup/tenderpayment/ConfirmCollectCashDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createPrimarySelectMethodScreen(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "selectMethodData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodInput"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-static {p2}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->primaryScreenKey(I)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 33
    invoke-static {p2, p1, p3}, Lcom/squareup/tenderpayment/SelectMethod;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createSecondarySelectMethodScreen(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "selectMethodData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completedTenders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectMethodInput"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, 0x1

    invoke-static {p2}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->secondaryScreenKey(I)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 44
    invoke-static {p2, p1, p3}, Lcom/squareup/tenderpayment/SelectMethod;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createSelectMethodScreenForBackground(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Z)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Z)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "selectMethodData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 55
    sget-object p2, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 57
    :goto_0
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 54
    invoke-static {p2, p1, v0}, Lcom/squareup/tenderpayment/SelectMethod;->createScreen(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method public final createSplitTenderWarningDialogScreen(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;",
            ">;"
        }
    .end annotation

    const-string v0, "onDismissed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createSplitTenderWarningDialogScreen$1;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethodScreensFactory$createSplitTenderWarningDialogScreen$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 95
    invoke-static {p1}, Lcom/squareup/tenderpayment/SplitTenderWarningDialogScreenKt;->SplitTenderWarningDialogScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
