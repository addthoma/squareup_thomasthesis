.class final Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectMethodStateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->render(Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/tenderpayment/SelectMethodWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/tenderpayment/SelectMethod$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

.field final synthetic this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 183
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/tenderpayment/SelectMethod$Event;)V
    .locals 4

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 398
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onBackPressedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 400
    :cond_0
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onCashSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 401
    :cond_1
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 402
    :cond_2
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onGiftCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 403
    :cond_3
    instance-of v1, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 404
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;->getTender()Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    move-result-object v2

    .line 405
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$OtherTenderSelected;->getTenderName()Ljava/lang/String;

    move-result-object p1

    .line 403
    invoke-static {v1, v2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onOtherTenderSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 407
    :cond_4
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ThirdPartyCardSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onThirdPartyCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 408
    :cond_5
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$InvoiceSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onInvoiceSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 409
    :cond_6
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onCardOnFileSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 410
    :cond_7
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onAddGiftCardSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 411
    :cond_8
    instance-of v1, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 412
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 413
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getInstrumentToken()Ljava/lang/String;

    move-result-object v3

    .line 414
    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;->getCardNameAndNumber()Ljava/lang/String;

    move-result-object p1

    .line 411
    invoke-static {v1, v2, v3, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onConfirmChargeCardOnFileAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 416
    :cond_9
    instance-of v1, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    .line 417
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->$props:Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;

    .line 418
    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;->getTenderedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 416
    invoke-static {v1, v2, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onQuickCashTenderReceivedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/tenderpayment/SelectMethodScreenWorkflow$StartArgs;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 420
    :cond_a
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onSecondaryTendersSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 421
    :cond_b
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$SplitTender;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SplitTender;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onSplitTenderAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 422
    :cond_c
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ReenableContactlessClicked;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onReenableContactlessClickedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 423
    :cond_d
    instance-of v1, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$RecordFullyCompedPayment;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onRecordFullyCompedPaymentAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 424
    :cond_e
    sget-object v1, Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$ContactlessSelected;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onContactlessSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 425
    :cond_f
    instance-of v1, p1, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow$render$selectMethodInput$1;->this$0:Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$TenderOptionSelection;->getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;->access$onTenderOptionSelectedAction(Lcom/squareup/tenderpayment/SelectMethodStateWorkflow;Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 396
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 425
    :cond_10
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
