.class public Lcom/squareup/tenderpayment/SelectMethodCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SelectMethodCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;
    }
.end annotation


# instance fields
.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private selectPaymentContainer:Landroid/view/ViewGroup;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private splitTenderButton:Landroid/widget/Button;

.field private splitTenderTransactionTotal:Landroid/widget/TextView;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private transactionTotal:Landroid/widget/TextView;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private upGlyph:Landroid/view/View;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/cardreader/dipper/ReaderHudManager;",
            "Lcom/squareup/payment/PaymentHudToaster;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .line 119
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 120
    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->screens:Lio/reactivex/Observable;

    .line 121
    iput-object p2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 122
    iput-object p3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 123
    iput-object p4, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    .line 124
    iput-object p5, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 125
    iput-object p6, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    .line 126
    iput-object p7, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    .line 127
    iput-object p8, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    .line 128
    iput-object p9, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 129
    iput-object p10, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;)V
    .locals 0

    .line 64
    invoke-direct/range {p0 .. p10}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-void
.end method

.method private addLineRowToPaymentSelectionList(Landroid/view/View;)V
    .locals 3

    .line 463
    new-instance v0, Lcom/squareup/marin/widgets/BorderedLinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 464
    invoke-virtual {v0, v1, v1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setHorizontalBorders(ZZ)V

    .line 465
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addView(Landroid/view/View;)V

    .line 466
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {p1, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 468
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->selectPaymentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/16 v2, 0x10

    .line 470
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setGravity(I)V

    .line 472
    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->selectPaymentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private addMoreOption(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)V"
        }
    .end annotation

    .line 423
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->secondaryTenderViewData:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->hasEnabledTenders(Ljava/util/List;)Z

    move-result v0

    .line 424
    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->createLineRow(Landroid/view/View;Z)Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    .line 425
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/tenderworkflow/R$string;->more:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 426
    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 427
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/noho/R$color;->noho_text_body:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    if-nez v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/buyercart/R$color;->title_color_disabled:I

    .line 431
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    .line 430
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    :cond_0
    const/4 v0, 0x0

    .line 434
    invoke-virtual {p1, v0, v0, v0, v0}, Lcom/squareup/ui/account/view/LineRow;->setPadding(IIII)V

    .line 436
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4MKJ98v6a94z_ZLyGtLd1HQLkA0;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4MKJ98v6a94z_ZLyGtLd1HQLkA0;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 437
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    .line 436
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 438
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->addLineRowToPaymentSelectionList(Landroid/view/View;)V

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 476
    sget v0, Lcom/squareup/tenderworkflow/R$id;->select_payment_up_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->upGlyph:Landroid/view/View;

    .line 477
    sget v0, Lcom/squareup/tenderworkflow/R$id;->select_method_transaction_total:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->transactionTotal:Landroid/widget/TextView;

    .line 478
    sget v0, Lcom/squareup/tenderworkflow/R$id;->select_payment_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->selectPaymentContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method private clearPaymentMethodLineRows()V
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->selectPaymentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method private createLineRow(Landroid/view/View;Z)Lcom/squareup/ui/account/view/LineRow;
    .locals 2

    .line 442
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 443
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->IF_ENABLED:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 444
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$dimen;->noho_text_size_body:I

    .line 445
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitleTextSize(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    .line 447
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object p1

    .line 449
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkout/R$dimen;->payment_options_row_height:I

    .line 450
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    .line 451
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/LineRow;->setMinimumHeight(I)V

    .line 453
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow;->setEnabled(Z)V

    if-eqz p2, :cond_0

    .line 456
    sget p2, Lcom/squareup/noho/R$drawable;->noho_selector_row_background:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/LineRow;->setBackgroundResource(I)V

    :cond_0
    return-object p1
.end method

.method private getLineRowForData(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;Lcom/squareup/workflow/legacy/Screen;)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 305
    iget-boolean v0, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->createLineRow(Landroid/view/View;Z)Lcom/squareup/ui/account/view/LineRow;

    move-result-object v0

    .line 307
    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->title:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1, v2}, Lcom/squareup/tenderpayment/SelectMethod$TextData;->string(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v2, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->title:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget v2, v2, Lcom/squareup/tenderpayment/SelectMethod$TextData;->color:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setPreservedLabelTextColor(I)V

    .line 309
    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->action:Lcom/squareup/fsm/SideEffect;

    if-eqz v1, :cond_0

    .line 310
    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$AYqTYyXs2bExt-vAFYG1NALal74;

    invoke-direct {v1, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$AYqTYyXs2bExt-vAFYG1NALal74;-><init>(Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    :cond_0
    iget-boolean v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->quickCashTender:Z

    if-eqz v1, :cond_1

    .line 314
    sget v1, Lcom/squareup/noho/R$drawable;->transparent_selector:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setBackgroundResource(I)V

    .line 315
    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 317
    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4qmg3IY_4dLscj7hV6T0k17ZKnw;

    invoke-direct {v1, p0, v0, p3}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4qmg3IY_4dLscj7hV6T0k17ZKnw;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Lcom/squareup/ui/account/view/LineRow;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    .line 324
    :cond_1
    iget-boolean v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->creditCardOnFileTender:Z

    if-eqz v1, :cond_2

    .line 325
    iget-boolean v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->initializeQuickSelectCreditCardOnFile(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/ui/account/view/LineRow;Z)V

    .line 328
    :cond_2
    iget-boolean v1, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->giftCardsOnFileTender:Z

    if-eqz v1, :cond_3

    .line 329
    iget-boolean p2, p2, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    invoke-direct {p0, p1, p3, v0, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->initializeQuickSelectGiftCardOnFile(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/ui/account/view/LineRow;Z)V

    :cond_3
    const/4 p1, 0x0

    .line 332
    invoke-virtual {v0, p1, p1, p1, p1}, Lcom/squareup/ui/account/view/LineRow;->setPadding(IIII)V

    return-object v0
.end method

.method private getNumberOfCardsTitle(Landroid/view/View;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/CardOnFileSummary;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 412
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget v0, Lcom/squareup/tenderworkflow/R$string;->customer_number_of_cards_on_file_many:I

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/tenderworkflow/R$string;->customer_number_of_cards_on_file_one:I

    .line 416
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 417
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const-string v0, "number"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 418
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 419
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private hasEnabledTenders(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;",
            ">;)Z"
        }
    .end annotation

    .line 214
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    .line 215
    iget-boolean v0, v0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->enabled:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private initializeQuickCashRow(Lcom/squareup/ui/account/view/LineRow;Lcom/squareup/workflow/legacy/Screen;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/account/view/LineRow;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 338
    new-instance v6, Lcom/squareup/tenderpayment/CashOptionsView;

    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Lcom/squareup/tenderpayment/CashOptionsView;-><init>(Landroid/content/Context;)V

    .line 340
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow;->getPreservedTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 341
    sget v1, Lcom/squareup/widgets/pos/R$id;->preserved_label:I

    .line 342
    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/LineRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/PreservedLabelView;

    .line 343
    invoke-virtual {v1}, Lcom/squareup/widgets/PreservedLabelView;->getTitleView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 344
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v0

    float-to-int v0, v0

    .line 346
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/LineRow;->getWidth()I

    move-result v1

    sub-int/2addr v1, v0

    .line 348
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->quickCashOptions:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v4, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->disableQuickCashOptions:Z

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v5, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    move-object v0, v6

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/tenderpayment/CashOptionsView;->setCashOptions(ILjava/util/List;Lcom/squareup/text/Formatter;ZLcom/squareup/protos/common/Money;)V

    .line 352
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$6fJjtGYboLuDnSGblpQYXGGKNE0;

    invoke-direct {v0, v6, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$6fJjtGYboLuDnSGblpQYXGGKNE0;-><init>(Lcom/squareup/tenderpayment/CashOptionsView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 355
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4-BIr2Km98eGKO2XvQGuMmxTuGA;

    invoke-direct {v0, v6, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$4-BIr2Km98eGKO2XvQGuMmxTuGA;-><init>(Lcom/squareup/tenderpayment/CashOptionsView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-object v6
.end method

.method private initializeQuickSelectCreditCardOnFile(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/ui/account/view/LineRow;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Lcom/squareup/ui/account/view/LineRow;",
            "Z)V"
        }
    .end annotation

    .line 364
    new-instance v7, Lcom/squareup/tenderpayment/CardsOnFileView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->creditCardOptions:Ljava/util/List;

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->creditCardOptions:Ljava/util/List;

    .line 365
    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->getNumberOfCardsTitle(Landroid/view/View;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isPaymentInCardRange:Z

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    const/4 p4, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x0

    iget-object p4, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v6, p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isTablet:Z

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tenderpayment/CardsOnFileView;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;ZZZ)V

    .line 368
    iget-object p4, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean p4, p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isPaymentInCardRange:Z

    if-eqz p4, :cond_1

    .line 369
    new-instance p4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$raI4Vy40ldkRaSW7ME0NWRthw9s;

    invoke-direct {p4, v7, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$raI4Vy40ldkRaSW7ME0NWRthw9s;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, p4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 375
    new-instance p4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$K-qzo880iPuRFhtPWcxm9LdEqVk;

    invoke-direct {p4, v7, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$K-qzo880iPuRFhtPWcxm9LdEqVk;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, p4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 379
    :cond_1
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 p4, -0x1

    invoke-direct {p1, p2, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 381
    invoke-virtual {p3, v7, p1}, Lcom/squareup/ui/account/view/LineRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 382
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method private initializeQuickSelectGiftCardOnFile(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/ui/account/view/LineRow;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/tenderpayment/SelectMethod$ScreenData;",
            "Lcom/squareup/tenderpayment/SelectMethod$Event;",
            ">;",
            "Lcom/squareup/ui/account/view/LineRow;",
            "Z)V"
        }
    .end annotation

    .line 389
    new-instance v7, Lcom/squareup/tenderpayment/CardsOnFileView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v2, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->giftCardOptions:Ljava/util/List;

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->giftCardOptions:Ljava/util/List;

    .line 390
    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->getNumberOfCardsTitle(Landroid/view/View;Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isPaymentInGiftCardRange:Z

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    const/4 p4, 0x1

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 p4, 0x0

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    iget-object p4, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v6, p4, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isTablet:Z

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/tenderpayment/CardsOnFileView;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;ZZZ)V

    .line 393
    new-instance p4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$F_kbmgraJ-J0E6JD37Hd_j4JIn8;

    invoke-direct {p4, v7, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$F_kbmgraJ-J0E6JD37Hd_j4JIn8;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, p4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 399
    new-instance p4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$2IaGXWQlCes5IfjYZza7tmHHHwA;

    invoke-direct {p4, v7, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$2IaGXWQlCes5IfjYZza7tmHHHwA;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, p4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 402
    new-instance p4, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$8k9HMglRlNaL8gKEyq1VBn8TteY;

    invoke-direct {p4, v7, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$8k9HMglRlNaL8gKEyq1VBn8TteY;-><init>(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, p4}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 405
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 p4, -0x1

    invoke-direct {p1, p2, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 407
    invoke-virtual {p3, v7, p1}, Lcom/squareup/ui/account/view/LineRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 408
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method private initializeSplitTenderButton(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V
    .locals 2

    .line 252
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    .line 254
    sget-object p1, Lcom/squareup/tenderpayment/SelectMethodCoordinator$2;->$SwitchMap$com$squareup$tenderpayment$SelectMethod$SplitTenderState:[I

    iget-object v0, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderState:Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/SelectMethod$SplitTenderState;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 264
    :cond_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 265
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderButtonLabel:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->action:Lcom/squareup/fsm/SideEffect;

    if-eqz p1, :cond_3

    .line 266
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$YDQpuI92kmXO5vhc_Z8mq88_-OI;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$YDQpuI92kmXO5vhc_Z8mq88_-OI;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V

    .line 267
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 266
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 259
    :cond_1
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 260
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 261
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 256
    :cond_2
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 271
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderButton:Landroid/widget/Button;

    iget-object p2, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderButtonLabel:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v0}, Lcom/squareup/tenderpayment/SelectMethod$TextData;->string(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic lambda$HwtcD0qP2aUylwfrBhQLnTD4c6Q(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Lcom/squareup/tenderpayment/SelectMethod$ToastData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->updateToast(Lcom/squareup/tenderpayment/SelectMethod$ToastData;)V

    return-void
.end method

.method static synthetic lambda$addMoreOption$27(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 437
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$SecondaryTendersSelected;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$getLineRowForData$11(Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;Landroid/view/View;)V
    .locals 0

    .line 310
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;->action:Lcom/squareup/fsm/SideEffect;

    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method static synthetic lambda$initializeQuickCashRow$14(Lcom/squareup/tenderpayment/CashOptionsView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 352
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->onCashAmountSelected()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$QKCdIjDUyTm1896RbhWtkMyZHwU;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$QKCdIjDUyTm1896RbhWtkMyZHwU;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 353
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickCashRow$16(Lcom/squareup/tenderpayment/CashOptionsView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 355
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CashOptionsView;->onCustomCashOptionClicked()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$8-tnjL_h943M2kinUW1-50fDXVo;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$8-tnjL_h943M2kinUW1-50fDXVo;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 356
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickSelectCreditCardOnFile$18(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 369
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->onCardOnFileSelected()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$eUxPrJ1eF-I8kKahb3TsNK7larg;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$eUxPrJ1eF-I8kKahb3TsNK7larg;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 370
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickSelectCreditCardOnFile$20(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 375
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->onNumberOfCardsClicked()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$VliN8I2MdY5htHgH3MhgaeWEZkQ;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$VliN8I2MdY5htHgH3MhgaeWEZkQ;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 376
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickSelectGiftCardOnFile$22(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 393
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->onCardOnFileSelected()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$oetODhd458c1Wncg-czOEwT5cr8;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$oetODhd458c1Wncg-czOEwT5cr8;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 394
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickSelectGiftCardOnFile$24(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 399
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->onNumberOfCardsClicked()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$XnQcUfdak7NRo7o2JGWfzXVjid4;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$XnQcUfdak7NRo7o2JGWfzXVjid4;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 400
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeQuickSelectGiftCardOnFile$26(Lcom/squareup/tenderpayment/CardsOnFileView;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 402
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/CardsOnFileView;->onAddNewCardClicked()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$AXgE5Lj89000NU7uJIQLJ4D0zxE;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$AXgE5Lj89000NU7uJIQLJ4D0zxE;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 403
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$initializeSplitTenderButton$9(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Landroid/view/View;)V
    .locals 0

    .line 267
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->splitTenderButtonLabel:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->action:Lcom/squareup/fsm/SideEffect;

    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/workflow/legacy/Screen;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 141
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isPrimaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$null$13(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/protos/common/Money;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 353
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v0, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$QuickCashTenderReceived;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$15(Lcom/squareup/workflow/legacy/Screen;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 356
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CashSelected;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$17(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 371
    iget-object v0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    iget-object v2, p1, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;->instrumentToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;->cardNameAndNumber:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$19(Lcom/squareup/workflow/legacy/Screen;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 376
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$CardOnFileSelected;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$21(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 395
    iget-object v0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;

    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    iget-object v2, p1, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;->instrumentToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/tenderpayment/CardsOnFileView$CardOnFile;->cardNameAndNumber:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/squareup/tenderpayment/SelectMethod$Event$ConfirmChargeCardOnFile;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$23(Lcom/squareup/workflow/legacy/Screen;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 400
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$GiftCardSelected;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$25(Lcom/squareup/workflow/legacy/Screen;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 403
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$AddGiftCardSelected;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/tenderpayment/SelectMethod$ToastData;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->toastData:Lcom/squareup/tenderpayment/SelectMethod$ToastData;

    return-object p0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 152
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1

    .line 153
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;->INSTANCE:Lcom/squareup/tenderpayment/SelectMethod$Event$BackPressed;

    invoke-interface {p0, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$updateContactlessPrompt$10(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;Landroid/view/View;)V
    .locals 0

    .line 294
    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object p0, p0, Lcom/squareup/tenderpayment/SelectMethod$TextData;->action:Lcom/squareup/fsm/SideEffect;

    invoke-interface {p0}, Lcom/squareup/fsm/SideEffect;->call()V

    return-void
.end method

.method private updateContactlessPrompt(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V
    .locals 4

    .line 275
    sget v0, Lcom/squareup/tenderworkflow/R$id;->nfc_prompt:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 276
    sget v1, Lcom/squareup/tenderworkflow/R$id;->swipe_insert_tap_prompt:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 278
    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget v1, v1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->visibility:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 279
    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    goto :goto_0

    .line 281
    :cond_0
    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object v3, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1, v3}, Lcom/squareup/tenderpayment/SelectMethod$TextData;->string(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget v3, v3, Lcom/squareup/tenderpayment/SelectMethod$TextData;->color:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 283
    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->paymentPromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object v1, v1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 286
    :goto_0
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->visibility:I

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 287
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->visibility:I

    if-eq p1, v2, :cond_1

    .line 288
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v1}, Lcom/squareup/tenderpayment/SelectMethod$TextData;->string(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget v1, v1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->color:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 290
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->weight:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 291
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-boolean p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->enabled:Z

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 292
    iget-object p1, p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->actionablePromptText:Lcom/squareup/tenderpayment/SelectMethod$TextData;

    iget-object p1, p1, Lcom/squareup/tenderpayment/SelectMethod$TextData;->action:Lcom/squareup/fsm/SideEffect;

    if-eqz p1, :cond_1

    .line 293
    new-instance p1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$jsA5SLkuRoIEaF7eryYRUejlqRA;

    invoke-direct {p1, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$jsA5SLkuRoIEaF7eryYRUejlqRA;-><init>(Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V

    .line 294
    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    .line 293
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method private updateToast(Lcom/squareup/tenderpayment/SelectMethod$ToastData;)V
    .locals 3

    .line 224
    sget-object v0, Lcom/squareup/tenderpayment/SelectMethodCoordinator$2;->$SwitchMap$com$squareup$tenderpayment$SelectMethod$ToastDataType:[I

    iget-object p1, p1, Lcom/squareup/tenderpayment/SelectMethod$ToastData;->type:Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$ToastDataType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 246
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    .line 243
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {p1, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    .line 239
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 240
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTransactionMinimum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 239
    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeGiftCard(Ljava/lang/Long;)Z

    goto :goto_0

    .line 236
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->paymentHudToaster:Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z

    goto :goto_0

    .line 233
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastRemoveChipCard()Z

    goto :goto_0

    .line 228
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v1, Lcom/squareup/cardreader/R$string;->contactless_ready_title:I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    :goto_0
    :pswitch_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 133
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 134
    invoke-direct {p0, p1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->bindViews(Landroid/view/View;)V

    .line 138
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$noXYxgtTwP7cg1iZqe0UgsRWbZM;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$noXYxgtTwP7cg1iZqe0UgsRWbZM;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 146
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$CGKdlOCHYf-k6yQWkolnAYUCIpU;

    invoke-direct {v0, p0}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$CGKdlOCHYf-k6yQWkolnAYUCIpU;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$9RauOEVTXJE737cBQoVd2Cim1Tk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$9RauOEVTXJE737cBQoVd2Cim1Tk;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$2$SelectMethodCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 140
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$jSvAXPnhtcgcw4hLg1rPsjQC-rQ;->INSTANCE:Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$jSvAXPnhtcgcw4hLg1rPsjQC-rQ;

    .line 141
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$vWrQSYMJd24iWDCXIOQYTB8vdEs;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$vWrQSYMJd24iWDCXIOQYTB8vdEs;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;)V

    .line 142
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$4$SelectMethodCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$69kF_RR5ZnPe4V-gI4dIGuQY9F0;->INSTANCE:Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$69kF_RR5ZnPe4V-gI4dIGuQY9F0;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$HwtcD0qP2aUylwfrBhQLnTD4c6Q;

    invoke-direct {v1, p0}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$HwtcD0qP2aUylwfrBhQLnTD4c6Q;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;)V

    .line 148
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$8$SelectMethodCoordinator(Landroid/view/View;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$BHAmm9nAAjHG5J39y4A_px4eTzw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$BHAmm9nAAjHG5J39y4A_px4eTzw;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getLineRowForData$12$SelectMethodCoordinator(Lcom/squareup/ui/account/view/LineRow;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;II)V
    .locals 0

    .line 318
    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p4, -0x2

    const/4 p5, -0x1

    invoke-direct {p3, p4, p5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 320
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->initializeQuickCashRow(Lcom/squareup/ui/account/view/LineRow;Lcom/squareup/workflow/legacy/Screen;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/account/view/LineRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public synthetic lambda$null$1$SelectMethodCoordinator(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->getPrimaryTutorialString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$7$SelectMethodCoordinator(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->upGlyph:Landroid/view/View;

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$TS7PLf96n4ydqGmF47Z1yoK2_xs;

    invoke-direct {v1, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$TS7PLf96n4ydqGmF47Z1yoK2_xs;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 152
    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$eluUbWFvpLVFec_9ZTZ5cdEUvwM;

    invoke-direct {v0, p2}, Lcom/squareup/tenderpayment/-$$Lambda$SelectMethodCoordinator$eluUbWFvpLVFec_9ZTZ5cdEUvwM;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 155
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isPrimaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-direct {p0, p1, v0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->initializeSplitTenderButton(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V

    .line 160
    sget v0, Lcom/squareup/tenderworkflow/R$id;->split_tender_transaction_total:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderTransactionTotal:Landroid/widget/TextView;

    .line 161
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-boolean v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->isSplitTenderPayment:Z

    if-eqz v0, :cond_0

    .line 162
    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender_total_subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v2, v2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->totalAmountDue:Lcom/squareup/protos/common/Money;

    .line 163
    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    const-string/jumbo v2, "total"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-wide v1, v1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->currentSplitNumber:J

    .line 164
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "current_payment"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-wide v1, v1, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->totalSplits:J

    .line 165
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "num_payments"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderTransactionTotal:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->splitTenderTransactionTotal:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->transactionTotal:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v2, v2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->clearPaymentMethodLineRows()V

    .line 174
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->primaryTenderViewData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    .line 175
    invoke-direct {p0, p1, v1, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->getLineRowForData(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;Lcom/squareup/workflow/legacy/Screen;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->addLineRowToPaymentSelectionList(Landroid/view/View;)V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->secondaryTenderViewData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 178
    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->addMoreOption(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    .line 181
    :cond_2
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    invoke-direct {p0, p1, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->updateContactlessPrompt(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$ScreenData;)V

    .line 183
    sget p2, Lcom/squareup/tenderworkflow/R$id;->payment_type_options_scroll_view:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/InteractiveScrollView;

    .line 184
    new-instance p2, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator;Lcom/squareup/widgets/InteractiveScrollView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/InteractiveScrollView;->setListener(Lcom/squareup/widgets/InteractiveScrollView$ScrollViewListener;)V

    goto :goto_2

    .line 197
    :cond_3
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->key:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {v0}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isSecondaryScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 198
    iget-object v0, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->transactionTotal:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/tenderworkflow/R$string;->secondary_payment_title_amount:I

    .line 199
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v3, v3, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->amountDue:Lcom/squareup/protos/common/Money;

    .line 200
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    invoke-direct {p0}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->clearPaymentMethodLineRows()V

    .line 204
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;

    iget-object v0, v0, Lcom/squareup/tenderpayment/SelectMethod$ScreenData;->secondaryTenderViewData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;

    .line 205
    invoke-direct {p0, p1, v1, p2}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->getLineRowForData(Landroid/view/View;Lcom/squareup/tenderpayment/SelectMethod$TenderViewData;Lcom/squareup/workflow/legacy/Screen;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/tenderpayment/SelectMethodCoordinator;->addLineRowToPaymentSelectionList(Landroid/view/View;)V

    goto :goto_1

    :cond_4
    :goto_2
    return-void

    .line 208
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unrecognized select method key."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
