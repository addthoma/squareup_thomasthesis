.class Lcom/squareup/tenderpayment/SplitTenderWarningDialogFactory;
.super Ljava/lang/Object;
.source "SplitTenderWarningDialogFactory.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;",
            ">;>;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/tenderpayment/SplitTenderWarningDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method static synthetic lambda$create$2(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen;)Landroid/app/Dialog;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p0, Lcom/squareup/tenderworkflow/R$string;->split_tender_unavailable_title:I

    .line 29
    invoke-virtual {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/tenderworkflow/R$string;->split_tender_unavailable_message:I

    .line 30
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    new-instance v0, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$0OvfLne-2jiQ2y-wMUfYXM-H1hE;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$0OvfLne-2jiQ2y-wMUfYXM-H1hE;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 31
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/tenderworkflow/R$string;->ok:I

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$9JLDnXw3Us79M0Td0mNN0756cKw;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$9JLDnXw3Us79M0Td0mNN0756cKw;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    .line 32
    invoke-virtual {p0, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/workflow/legacy/Screen;Landroid/content/DialogInterface;)V
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;->INSTANCE:Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/squareup/workflow/legacy/Screen;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object p1, Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;->INSTANCE:Lcom/squareup/tenderpayment/SplitTenderWarningDialog$SplitTenderWarningDismissed;

    invoke-interface {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/tenderpayment/SplitTenderWarningDialogFactory;->screens:Lio/reactivex/Observable;

    .line 27
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$XpTAbuU1ZeNncEJc1a_D0O8Ud48;

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/-$$Lambda$SplitTenderWarningDialogFactory$XpTAbuU1ZeNncEJc1a_D0O8Ud48;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
