.class public final Lcom/squareup/tenderpayment/RealPaymentViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealPaymentViewFactory.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/PaymentViewFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPaymentViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPaymentViewFactory.kt\ncom/squareup/tenderpayment/RealPaymentViewFactory\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,172:1\n37#2,2:173\n*E\n*S KotlinDebug\n*F\n+ 1 RealPaymentViewFactory.kt\ncom/squareup/tenderpayment/RealPaymentViewFactory\n*L\n46#1,2:173\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u001fB\\\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0013\u0008\u0001\u0010\u000f\u001a\r\u0012\t\u0012\u00070\u0011\u00a2\u0006\u0002\u0008\u00120\u0010\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u001c\u0010\u0018\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001bj\u0002`\u001cH\u0016J\u001c\u0010\u001d\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001bj\u0002`\u001cH\u0016J\u001c\u0010\u001e\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001bj\u0002`\u001cH\u0016R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/tenderpayment/RealPaymentViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "Lcom/squareup/tenderpayment/PaymentViewFactory;",
        "selectMethodFactory",
        "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
        "soloSellerCashReceivedLayoutRunnerFactory",
        "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
        "buyerCartFactory",
        "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
        "paymentPromptCoordinator",
        "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
        "separateTenderFactory",
        "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
        "separateCustomEvenFactory",
        "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
        "viewFactories",
        "",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "selectTenderViewFactory",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
        "blockedByBuyerFacingDisplayViewFactory",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
        "(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)V",
        "isInBuyerCheckoutWorkflow",
        "",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "isInSelectMethodWorkflow",
        "matchesSelectMethodKey",
        "DismissAndShowSpot",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final selectTenderViewFactory:Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)V
    .locals 29
    .param p7    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ForMainActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v5, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v1, p7

    move-object/from16 v9, p8

    move-object/from16 v8, p9

    const-string v10, "selectMethodFactory"

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "soloSellerCashReceivedLayoutRunnerFactory"

    invoke-static {v5, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "buyerCartFactory"

    invoke-static {v3, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "paymentPromptCoordinator"

    invoke-static {v4, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "separateTenderFactory"

    invoke-static {v6, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "separateCustomEvenFactory"

    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v10, "viewFactories"

    invoke-static {v1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "selectTenderViewFactory"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "blockedByBuyerFacingDisplayViewFactory"

    invoke-static {v8, v10}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v10, Lkotlin/jvm/internal/SpreadBuilder;

    const/4 v11, 0x4

    invoke-direct {v10, v11}, Lkotlin/jvm/internal/SpreadBuilder;-><init>(I)V

    .line 46
    check-cast v1, Ljava/util/Collection;

    const/4 v12, 0x0

    new-array v13, v12, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 174
    invoke-interface {v1, v13}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v10, v1}, Lkotlin/jvm/internal/SpreadBuilder;->addSpread(Ljava/lang/Object;)V

    .line 47
    move-object v1, v9

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v10, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    .line 48
    move-object v1, v8

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v10, v1}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    .line 49
    new-instance v13, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;

    const/16 v1, 0xc

    new-array v8, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 50
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 51
    sget-object v15, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->PRIMARY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v16, Lcom/squareup/tenderworkflow/R$layout;->payment_type_layout:I

    .line 52
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    sget-object v22, Lcom/squareup/workflow/SoftInputMode;->HIDDEN:Lcom/squareup/workflow/SoftInputMode;

    sget-object v17, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;->Companion:Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;

    invoke-virtual/range {v17 .. v17}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot$Companion;->getINSTANCE()Lcom/squareup/tenderpayment/RealPaymentViewFactory$DismissAndShowSpot;

    move-result-object v17

    move-object/from16 v23, v17

    check-cast v23, Lcom/squareup/container/spot/Spot;

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x1cf

    const/16 v28, 0x0

    move-object/from16 v17, v1

    invoke-direct/range {v17 .. v28}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 53
    new-instance v11, Lcom/squareup/tenderpayment/RealPaymentViewFactory$2;

    invoke-direct {v11, v2}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$2;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;)V

    move-object/from16 v19, v11

    check-cast v19, Lkotlin/jvm/functions/Function1;

    const/16 v20, 0x8

    .line 50
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    aput-object v1, v8, v12

    const/4 v1, 0x1

    .line 56
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 57
    sget-object v15, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->SECONDARY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v16, Lcom/squareup/tenderworkflow/R$layout;->payment_type_more_layout:I

    const/16 v17, 0x0

    .line 58
    new-instance v11, Lcom/squareup/tenderpayment/RealPaymentViewFactory$3;

    invoke-direct {v11, v2}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$3;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;)V

    move-object/from16 v19, v11

    check-cast v19, Lkotlin/jvm/functions/Function1;

    const/16 v20, 0xc

    .line 56
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/4 v1, 0x2

    .line 61
    sget-object v11, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v12, Lcom/squareup/tenderpayment/ConfirmChargeCardOnFileDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v14, Lcom/squareup/tenderpayment/RealPaymentViewFactory$4;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$4;

    check-cast v14, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v11, v12, v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/4 v1, 0x3

    .line 63
    sget-object v11, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 64
    sget-object v12, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v14, Lcom/squareup/tenderpayment/RealPaymentViewFactory$5;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$5;

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 63
    invoke-virtual {v11, v12, v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    .line 67
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 68
    sget-object v11, Lcom/squareup/tenderpayment/ConfirmCollectCashDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v12, Lcom/squareup/tenderpayment/RealPaymentViewFactory$6;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$6;

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 67
    invoke-virtual {v1, v11, v12}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v11, 0x4

    aput-object v1, v8, v11

    const/4 v1, 0x5

    .line 71
    sget-object v11, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v12, Lcom/squareup/tenderpayment/SplitTenderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v14, Lcom/squareup/tenderpayment/RealPaymentViewFactory$7;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$7;

    check-cast v14, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v11, v12, v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/4 v1, 0x6

    .line 73
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 74
    sget-object v11, Lcom/squareup/buyercheckout/BuyerCart;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCart;

    invoke-virtual {v11}, Lcom/squareup/buyercheckout/BuyerCart;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v15

    sget v16, Lcom/squareup/tenderworkflow/R$layout;->buyer_cart_layout:I

    .line 75
    new-instance v11, Lcom/squareup/tenderpayment/RealPaymentViewFactory$8;

    invoke-direct {v11, v3}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$8;-><init>(Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;)V

    move-object/from16 v19, v11

    check-cast v19, Lkotlin/jvm/functions/Function1;

    .line 73
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/4 v1, 0x7

    .line 78
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 79
    sget-object v11, Lcom/squareup/buyercheckout/PaymentPrompt;->INSTANCE:Lcom/squareup/buyercheckout/PaymentPrompt;

    invoke-virtual {v11}, Lcom/squareup/buyercheckout/PaymentPrompt;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v15

    sget v16, Lcom/squareup/tenderworkflow/R$layout;->payment_prompt_layout:I

    .line 80
    new-instance v11, Lcom/squareup/tenderpayment/RealPaymentViewFactory$9;

    invoke-direct {v11, v4}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$9;-><init>(Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;)V

    move-object/from16 v19, v11

    check-cast v19, Lkotlin/jvm/functions/Function1;

    .line 78
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/16 v1, 0x8

    .line 83
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 84
    const-class v11, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedScreen;

    invoke-static {v11}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v15

    sget v16, Lcom/squareup/tenderworkflow/R$layout;->seller_cash_received_layout:I

    .line 85
    new-instance v11, Lcom/squareup/tenderpayment/RealPaymentViewFactory$10;

    invoke-direct {v11, v5}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$10;-><init>(Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;)V

    move-object/from16 v19, v11

    check-cast v19, Lkotlin/jvm/functions/Function1;

    .line 83
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v11

    check-cast v11, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    aput-object v11, v8, v1

    const/16 v1, 0x9

    .line 88
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 89
    sget-object v15, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 90
    sget v16, Lcom/squareup/tenderworkflow/R$layout;->split_tender_amount_layout:I

    .line 91
    new-instance v11, Lcom/squareup/workflow/ScreenHint;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v22, 0x0

    sget-object v23, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/16 v27, 0x1df

    move-object/from16 v17, v11

    invoke-direct/range {v17 .. v28}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 92
    new-instance v12, Lcom/squareup/tenderpayment/RealPaymentViewFactory$11;

    invoke-direct {v12, v6}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$11;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;)V

    move-object/from16 v19, v12

    check-cast v19, Lkotlin/jvm/functions/Function1;

    const/16 v20, 0x8

    .line 88
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/16 v1, 0xa

    .line 95
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 96
    sget-object v15, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->EVEN_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 97
    sget v16, Lcom/squareup/tenderworkflow/R$layout;->split_tender_custom_even_split_layout:I

    .line 98
    new-instance v11, Lcom/squareup/workflow/ScreenHint;

    const/16 v19, 0x0

    const/16 v20, 0x0

    sget-object v23, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    move-object/from16 v17, v11

    invoke-direct/range {v17 .. v28}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 99
    new-instance v12, Lcom/squareup/tenderpayment/RealPaymentViewFactory$12;

    invoke-direct {v12, v7}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$12;-><init>(Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;)V

    move-object/from16 v19, v12

    check-cast v19, Lkotlin/jvm/functions/Function1;

    const/16 v20, 0x8

    .line 95
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    const/16 v1, 0xb

    .line 102
    sget-object v11, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 103
    sget-object v12, Lcom/squareup/tenderpayment/separatetender/SeparateTender;->CONFIRMING_CANCEL_SPLIT_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 104
    sget-object v14, Lcom/squareup/tenderpayment/RealPaymentViewFactory$13;->INSTANCE:Lcom/squareup/tenderpayment/RealPaymentViewFactory$13;

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 102
    invoke-virtual {v11, v12, v14}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v11

    aput-object v11, v8, v1

    move-object v1, v13

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/tenderpayment/RealPaymentViewFactory$1;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    check-cast v13, Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v10, v13}, Lkotlin/jvm/internal/SpreadBuilder;->add(Ljava/lang/Object;)V

    invoke-virtual {v10}, Lkotlin/jvm/internal/SpreadBuilder;->size()I

    move-result v1

    new-array v1, v1, [Lcom/squareup/workflow/WorkflowViewFactory;

    invoke-virtual {v10, v1}, Lkotlin/jvm/internal/SpreadBuilder;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 46
    invoke-direct {v0, v1}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    iput-object v9, v0, Lcom/squareup/tenderpayment/RealPaymentViewFactory;->selectTenderViewFactory:Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;

    return-void

    .line 174
    :cond_0
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public isInBuyerCheckoutWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    invoke-static {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutScreenKeys;->isInBuyerCheckoutWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p1

    return p1
.end method

.method public isInSelectMethodWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->isInSelectMethodWorkflow(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory;->selectTenderViewFactory:Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;

    invoke-interface {v0, p1}, Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;->isInScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public matchesSelectMethodKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-static {p1}, Lcom/squareup/tenderpayment/SelectMethodScreenKeys;->matchesSelectMethodScreenKey(Lcom/squareup/workflow/legacy/Screen$Key;)Z

    move-result p1

    return p1
.end method
