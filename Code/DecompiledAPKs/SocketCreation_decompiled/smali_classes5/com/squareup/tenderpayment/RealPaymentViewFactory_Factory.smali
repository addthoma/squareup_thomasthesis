.class public final Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealPaymentViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tenderpayment/RealPaymentViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final blockedByBuyerFacingDisplayViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerCartFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentPromptCoordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final selectTenderViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final separateCustomEvenFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final separateTenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final soloSellerCashReceivedLayoutRunnerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoriesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->selectMethodFactoryProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p2, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->soloSellerCashReceivedLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p3, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->buyerCartFactoryProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p4, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->paymentPromptCoordinatorProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p5, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->separateTenderFactoryProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p6, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->separateCustomEvenFactoryProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p7, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->viewFactoriesProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p8, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->selectTenderViewFactoryProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p9, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->blockedByBuyerFacingDisplayViewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
            ">;)",
            "Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;"
        }
    .end annotation

    .line 79
    new-instance v10, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)Lcom/squareup/tenderpayment/RealPaymentViewFactory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;",
            "Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;",
            "Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            ">;",
            "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;",
            ")",
            "Lcom/squareup/tenderpayment/RealPaymentViewFactory;"
        }
    .end annotation

    .line 91
    new-instance v10, Lcom/squareup/tenderpayment/RealPaymentViewFactory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/tenderpayment/RealPaymentViewFactory;-><init>(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/tenderpayment/RealPaymentViewFactory;
    .locals 10

    .line 66
    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->selectMethodFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->soloSellerCashReceivedLayoutRunnerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->buyerCartFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->paymentPromptCoordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->separateTenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->separateCustomEvenFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->viewFactoriesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Set;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->selectTenderViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;

    iget-object v0, p0, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->blockedByBuyerFacingDisplayViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;

    invoke-static/range {v1 .. v9}, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->newInstance(Lcom/squareup/tenderpayment/SelectMethodCoordinator$Factory;Lcom/squareup/tenderpayment/sellercashreceived/SoloSellerCashReceivedLayoutRunner$Factory;Lcom/squareup/buyercheckout/BuyerCartCoordinator$Factory;Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateTenderCoordinator$Factory;Lcom/squareup/tenderpayment/separatetender/SeparateCustomEvenCoordinator$Factory;Ljava/util/Set;Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayViewFactory;)Lcom/squareup/tenderpayment/RealPaymentViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/tenderpayment/RealPaymentViewFactory_Factory;->get()Lcom/squareup/tenderpayment/RealPaymentViewFactory;

    move-result-object v0

    return-object v0
.end method
