.class public final enum Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
.super Ljava/lang/Enum;
.source "TenderCompleter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderpayment/TenderCompleter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CompleteTenderResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum SHOW_THIRD_PARTY:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

.field public static final enum START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 19
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v1, 0x0

    const-string v2, "DO_NOTHING"

    invoke-direct {v0, v2, v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 20
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v2, 0x1

    const-string v3, "START_BUYER_FLOW"

    invoke-direct {v0, v3, v2}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 21
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v3, 0x2

    const-string v4, "START_BUYER_FLOW_AND_AUTHORIZE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 22
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v4, 0x3

    const-string v5, "START_SPLIT_TENDER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 23
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v5, 0x4

    const-string v6, "SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN"

    invoke-direct {v0, v6, v5}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 24
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v6, 0x5

    const-string v7, "SHOW_POS_APPLET"

    invoke-direct {v0, v7, v6}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 25
    new-instance v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v7, 0x6

    const-string v8, "SHOW_THIRD_PARTY"

    invoke-direct {v0, v8, v7}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_THIRD_PARTY:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    .line 18
    sget-object v8, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_THIRD_PARTY:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->$VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 18
    const-class v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->$VALUES:[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v0}, [Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    return-object v0
.end method
