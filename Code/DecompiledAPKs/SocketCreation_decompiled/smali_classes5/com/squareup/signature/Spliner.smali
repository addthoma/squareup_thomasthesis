.class public Lcom/squareup/signature/Spliner;
.super Ljava/lang/Object;
.source "Spliner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signature/Spliner$Bezier;
    }
.end annotation


# instance fields
.field private final beziers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/signature/Spliner$Bezier;",
            ">;"
        }
    .end annotation
.end field

.field private final dirtyRect:Landroid/graphics/Rect;

.field private final points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/signature/Point;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/signature/Spliner;->beziers:Ljava/util/List;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    return-void
.end method

.method private addArtificialPoint()V
    .locals 7

    .line 112
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/signature/Point;

    .line 114
    iget-object v2, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/signature/Point;

    .line 116
    new-instance v3, Lcom/squareup/signature/Point;

    iget v4, v0, Lcom/squareup/signature/Point;->x:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v4, v4, v5

    iget v6, v2, Lcom/squareup/signature/Point;->x:F

    sub-float/2addr v4, v6

    iget v0, v0, Lcom/squareup/signature/Point;->y:F

    mul-float v0, v0, v5

    iget v2, v2, Lcom/squareup/signature/Point;->y:F

    sub-float/2addr v0, v2

    invoke-direct {v3, v4, v0}, Lcom/squareup/signature/Point;-><init>(FF)V

    .line 117
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-void
.end method

.method private static computeBSpline([F)[F
    .locals 6

    .line 88
    array-length v0, p0

    .line 91
    filled-new-array {v0, v0}, [I

    move-result-object v1

    const-class v2, F

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[F

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    if-lez v2, :cond_0

    .line 93
    aget-object v4, v1, v2

    add-int/lit8 v5, v2, -0x1

    aput v3, v4, v5

    .line 94
    :cond_0
    aget-object v4, v1, v2

    const/high16 v5, 0x40800000    # 4.0f

    aput v5, v4, v2

    add-int/lit8 v4, v0, -0x1

    if-ge v2, v4, :cond_1

    .line 95
    aget-object v4, v1, v2

    add-int/lit8 v5, v2, 0x1

    aput v3, v4, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    :cond_2
    invoke-static {v1, p0}, Lcom/squareup/signature/Spliner;->solve([[F[F)[F

    move-result-object p0

    return-object p0
.end method

.method private varargs expandDirtyRect([Lcom/squareup/signature/Point;)V
    .locals 5

    .line 169
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 170
    iget-object v3, p0, Lcom/squareup/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    iget v4, v2, Lcom/squareup/signature/Point;->x:F

    float-to-int v4, v4

    iget v2, v2, Lcom/squareup/signature/Point;->y:F

    float-to-int v2, v2

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Rect;->union(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static solve([[F[F)[F
    .locals 9

    .line 139
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    add-int/lit8 v2, v1, 0x1

    move v3, v2

    :goto_1
    if-ge v3, v0, :cond_1

    .line 147
    aget-object v4, p0, v3

    aget v4, v4, v1

    aget-object v5, p0, v1

    aget v5, v5, v1

    div-float/2addr v4, v5

    move v5, v1

    :goto_2
    if-ge v5, v0, :cond_0

    .line 149
    aget-object v6, p0, v3

    aget v7, v6, v5

    aget-object v8, p0, v1

    aget v8, v8, v5

    mul-float v8, v8, v4

    sub-float/2addr v7, v8

    aput v7, v6, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 151
    :cond_0
    aget v5, p1, v3

    aget v6, p1, v1

    mul-float v4, v4, v6

    sub-float/2addr v5, v4

    aput v5, p1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0

    .line 156
    :cond_2
    new-array v1, v0, [F

    add-int/lit8 v2, v0, -0x1

    :goto_3
    if-ltz v2, :cond_4

    const/4 v3, 0x0

    add-int/lit8 v4, v2, 0x1

    :goto_4
    if-ge v4, v0, :cond_3

    .line 160
    aget-object v5, p0, v2

    aget v5, v5, v4

    aget v6, v1, v4

    mul-float v5, v5, v6

    add-float/2addr v3, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 162
    :cond_3
    aget v4, p1, v2

    sub-float/2addr v4, v3

    aget-object v3, p0, v2

    aget v3, v3, v2

    div-float/2addr v4, v3

    aput v4, v1, v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    :cond_4
    return-object v1
.end method


# virtual methods
.method public addPoint(Lcom/squareup/signature/Point;)V
    .locals 13

    .line 32
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    iget-object p1, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/signature/Spliner;->addArtificialPoint()V

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v1, 0x4

    if-ge p1, v1, :cond_1

    return-void

    .line 39
    :cond_1
    iget-object p1, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    const/4 v3, 0x0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/signature/Spliner;->points:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p1, v2, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    .line 46
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v4, v2, -0x2

    .line 51
    new-array v5, v4, [F

    const/4 v6, 0x1

    .line 52
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/signature/Point;

    iget v7, v7, Lcom/squareup/signature/Point;->x:F

    const/high16 v8, 0x40c00000    # 6.0f

    mul-float v7, v7, v8

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/signature/Point;

    iget v9, v9, Lcom/squareup/signature/Point;->x:F

    sub-float/2addr v7, v9

    aput v7, v5, v3

    const/4 v7, 0x1

    :goto_0
    add-int/lit8 v9, v4, -0x1

    if-ge v7, v9, :cond_2

    .line 54
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/signature/Point;

    iget v9, v9, Lcom/squareup/signature/Point;->x:F

    mul-float v9, v9, v8

    aput v9, v5, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 56
    :cond_2
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/signature/Point;

    iget v7, v7, Lcom/squareup/signature/Point;->x:F

    mul-float v7, v7, v8

    add-int/lit8 v10, v2, -0x1

    .line 57
    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/signature/Point;

    iget v11, v11, Lcom/squareup/signature/Point;->x:F

    sub-float/2addr v7, v11

    aput v7, v5, v9

    .line 58
    invoke-static {v5}, Lcom/squareup/signature/Spliner;->computeBSpline([F)[F

    move-result-object v5

    .line 61
    new-array v7, v4, [F

    .line 62
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/signature/Point;

    iget v11, v11, Lcom/squareup/signature/Point;->y:F

    mul-float v11, v11, v8

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/signature/Point;

    iget v12, v12, Lcom/squareup/signature/Point;->y:F

    sub-float/2addr v11, v12

    aput v11, v7, v3

    const/4 v11, 0x1

    :goto_1
    if-ge v11, v9, :cond_3

    .line 64
    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/signature/Point;

    iget v12, v12, Lcom/squareup/signature/Point;->y:F

    mul-float v12, v12, v8

    aput v12, v7, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 66
    :cond_3
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/signature/Point;

    iget v11, v11, Lcom/squareup/signature/Point;->y:F

    mul-float v11, v11, v8

    .line 67
    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/signature/Point;

    iget v8, v8, Lcom/squareup/signature/Point;->y:F

    sub-float/2addr v11, v8

    aput v11, v7, v9

    .line 68
    invoke-static {v7}, Lcom/squareup/signature/Spliner;->computeBSpline([F)[F

    move-result-object v7

    const/4 v8, 0x3

    sub-int/2addr v2, v8

    .line 73
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/signature/Point;

    .line 74
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/signature/Point;

    .line 76
    new-instance v4, Lcom/squareup/signature/Point;

    array-length v9, v5

    sub-int/2addr v9, v0

    aget v9, v5, v9

    array-length v10, v7

    sub-int/2addr v10, v0

    aget v10, v7, v10

    invoke-direct {v4, v9, v10}, Lcom/squareup/signature/Point;-><init>(FF)V

    .line 77
    new-instance v9, Lcom/squareup/signature/Point;

    array-length v10, v5

    sub-int/2addr v10, v6

    aget v5, v5, v10

    array-length v10, v7

    sub-int/2addr v10, v6

    aget v7, v7, v10

    invoke-direct {v9, v5, v7}, Lcom/squareup/signature/Point;-><init>(FF)V

    .line 79
    invoke-virtual {v4, v9}, Lcom/squareup/signature/Point;->oneThirdTo(Lcom/squareup/signature/Point;)Lcom/squareup/signature/Point;

    move-result-object v5

    .line 80
    invoke-virtual {v4, v9}, Lcom/squareup/signature/Point;->twoThirdsTo(Lcom/squareup/signature/Point;)Lcom/squareup/signature/Point;

    move-result-object v4

    .line 82
    iget-object v7, p0, Lcom/squareup/signature/Spliner;->beziers:Ljava/util/List;

    new-instance v9, Lcom/squareup/signature/Spliner$Bezier;

    invoke-direct {v9, v2, p1, v5, v4}, Lcom/squareup/signature/Spliner$Bezier;-><init>(Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-array v1, v1, [Lcom/squareup/signature/Point;

    aput-object v2, v1, v3

    aput-object p1, v1, v6

    aput-object v5, v1, v0

    aput-object v4, v1, v8

    .line 83
    invoke-direct {p0, v1}, Lcom/squareup/signature/Spliner;->expandDirtyRect([Lcom/squareup/signature/Point;)V

    return-void
.end method

.method public getBeziers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/signature/Spliner$Bezier;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->beziers:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDirtyRect()Landroid/graphics/Rect;
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public isDirty()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public resetDirty()V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/signature/Spliner;->dirtyRect:Landroid/graphics/Rect;

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    return-void
.end method
