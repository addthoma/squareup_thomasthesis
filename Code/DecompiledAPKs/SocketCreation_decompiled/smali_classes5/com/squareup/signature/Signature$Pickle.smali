.class Lcom/squareup/signature/Signature$Pickle;
.super Ljava/lang/Object;
.source "Signature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/Signature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Pickle"
.end annotation


# instance fields
.field final color:I

.field final glyphs:[[[I

.field final height:I

.field final width:I


# direct methods
.method constructor <init>(Lcom/squareup/signature/Signature;)V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iget v0, p1, Lcom/squareup/signature/Signature;->width:I

    iput v0, p0, Lcom/squareup/signature/Signature$Pickle;->width:I

    .line 48
    iget v0, p1, Lcom/squareup/signature/Signature;->height:I

    iput v0, p0, Lcom/squareup/signature/Signature$Pickle;->height:I

    .line 49
    iget v0, p1, Lcom/squareup/signature/Signature;->color:I

    iput v0, p0, Lcom/squareup/signature/Signature$Pickle;->color:I

    .line 50
    invoke-static {p1}, Lcom/squareup/signature/Signature;->access$000(Lcom/squareup/signature/Signature;)[[[I

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/Signature$Pickle;->glyphs:[[[I

    return-void
.end method
