.class public final Lcom/squareup/transferreports/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transferreports/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final deposits_report:I = 0x7f12080e

.field public static final deposits_report_active_sales:I = 0x7f12080f

.field public static final deposits_report_bank_name_and_bank_account_suffix:I = 0x7f120810

.field public static final deposits_report_card_payments_in_deposit_uppercase:I = 0x7f120811

.field public static final deposits_report_collected_uppercase:I = 0x7f120812

.field public static final deposits_report_current_balance_uppercase:I = 0x7f120813

.field public static final deposits_report_deposit:I = 0x7f120814

.field public static final deposits_report_deposit_uppercase:I = 0x7f120815

.field public static final deposits_report_estimated_fees:I = 0x7f120816

.field public static final deposits_report_fee_details_learn_more:I = 0x7f120817

.field public static final deposits_report_fee_details_message:I = 0x7f120818

.field public static final deposits_report_fee_details_title:I = 0x7f120819

.field public static final deposits_report_fees:I = 0x7f12081a

.field public static final deposits_report_help_button:I = 0x7f12081b

.field public static final deposits_report_history_uppercase:I = 0x7f12081c

.field public static final deposits_report_instant_deposit:I = 0x7f12081d

.field public static final deposits_report_instant_deposit_fee:I = 0x7f12081e

.field public static final deposits_report_net_total_uppercase:I = 0x7f12081f

.field public static final deposits_report_next_business_day_deposit:I = 0x7f120820

.field public static final deposits_report_no_deposit:I = 0x7f120821

.field public static final deposits_report_pay_in:I = 0x7f120822

.field public static final deposits_report_pay_in_date:I = 0x7f120823

.field public static final deposits_report_pay_in_source:I = 0x7f120824

.field public static final deposits_report_pending_deposit:I = 0x7f120825

.field public static final deposits_report_pending_withdrawal:I = 0x7f120826

.field public static final deposits_report_same_day_deposit:I = 0x7f120827

.field public static final deposits_report_same_day_deposit_fee:I = 0x7f120828

.field public static final deposits_report_sending_date:I = 0x7f120829

.field public static final deposits_report_sent_date_time:I = 0x7f12082a

.field public static final deposits_report_summary_uppercase:I = 0x7f12082b

.field public static final deposits_report_total_collected:I = 0x7f12082c

.field public static final deposits_report_withdrawal:I = 0x7f12082d

.field public static final deposits_report_withdrawing_date:I = 0x7f12082e

.field public static final reports_error:I = 0x7f12168f

.field public static final reports_error_message:I = 0x7f121690

.field public static final reports_error_reload:I = 0x7f121691

.field public static final reports_net_total:I = 0x7f121693

.field public static final transfer_reports_help_url:I = 0x7f121a05


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
