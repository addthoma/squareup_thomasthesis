.class public Lcom/squareup/signout/SignoutDialogScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SignoutDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/signout/SignoutDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/signout/SignoutDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/signout/SignoutDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final signOutMessage:Ljava/lang/String;

.field private final signOutTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/signout/-$$Lambda$SignoutDialogScreen$2jVeL6mM8kUExB05qmWRRhX2wwo;->INSTANCE:Lcom/squareup/signout/-$$Lambda$SignoutDialogScreen$2jVeL6mM8kUExB05qmWRRhX2wwo;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/signout/SignoutDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutTitle:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/signout/SignoutDialogScreen;)Ljava/lang/String;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutMessage:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/signout/SignoutDialogScreen;)Ljava/lang/String;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutTitle:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/signout/SignoutDialogScreen;
    .locals 2

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 53
    new-instance v1, Lcom/squareup/signout/SignoutDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/signout/SignoutDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 46
    iget-object p2, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutTitle:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-object p2, p0, Lcom/squareup/signout/SignoutDialogScreen;->signOutMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
