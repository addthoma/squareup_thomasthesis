.class public Lcom/squareup/signout/SignoutDialogScreen$Factory;
.super Ljava/lang/Object;
.source "SignoutDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signout/SignoutDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/signout/SignOutRunner;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 38
    invoke-virtual {p0, p1}, Lcom/squareup/signout/SignOutRunner;->doSignOut(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 30
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/signout/SignoutDialogScreen;

    .line 31
    const-class v1, Lcom/squareup/signout/SignOutRunner$ParentComponent;

    .line 32
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/signout/SignOutRunner$ParentComponent;

    .line 33
    invoke-interface {v1}, Lcom/squareup/signout/SignOutRunner$ParentComponent;->signOutRunner()Lcom/squareup/signout/SignOutRunner;

    move-result-object v1

    .line 35
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-static {v0}, Lcom/squareup/signout/SignoutDialogScreen;->access$100(Lcom/squareup/signout/SignoutDialogScreen;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 37
    invoke-static {v0}, Lcom/squareup/signout/SignoutDialogScreen;->access$000(Lcom/squareup/signout/SignoutDialogScreen;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/squareup/signout/R$string;->sign_out:I

    new-instance v3, Lcom/squareup/signout/-$$Lambda$SignoutDialogScreen$Factory$trsrazUTofHqRanFX1vyxieqGyQ;

    invoke-direct {v3, v1, p1}, Lcom/squareup/signout/-$$Lambda$SignoutDialogScreen$Factory$trsrazUTofHqRanFX1vyxieqGyQ;-><init>(Lcom/squareup/signout/SignOutRunner;Landroid/content/Context;)V

    .line 38
    invoke-virtual {v0, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 39
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 40
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 35
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
