.class public final Lcom/squareup/tour/NoWhatsNewTourProvider;
.super Ljava/lang/Object;
.source "WhatsNewTourProvider.kt"

# interfaces
.implements Lcom/squareup/tour/WhatsNewTourProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/tour/NoWhatsNewTourProvider;",
        "Lcom/squareup/tour/WhatsNewTourProvider;",
        "()V",
        "displayAfterOnboarding",
        "",
        "getDisplayAfterOnboarding",
        "()Z",
        "tourPages",
        "",
        "Lcom/squareup/tour/SerializableHasTourPages;",
        "getTourPages",
        "()Ljava/util/List;",
        "pos-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayAfterOnboarding:Z

.field private final tourPages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/tour/NoWhatsNewTourProvider;->tourPages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getDisplayAfterOnboarding()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/tour/NoWhatsNewTourProvider;->displayAfterOnboarding:Z

    return v0
.end method

.method public getTourPages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/tour/NoWhatsNewTourProvider;->tourPages:Ljava/util/List;

    return-object v0
.end method
