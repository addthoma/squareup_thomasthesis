.class public Lcom/squareup/tour/WhatsNewSettings;
.super Ljava/lang/Object;
.source "WhatsNewSettings.java"


# instance fields
.field private final tourPreference:Lcom/squareup/tour/TourPreference;

.field private final tourProvider:Lcom/squareup/tour/WhatsNewTourProvider;

.field private final whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;


# direct methods
.method public constructor <init>(Lcom/squareup/tour/WhatsNewBadge;Lcom/squareup/tour/WhatsNewTourProvider;Lcom/squareup/tour/TourPreference;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/tour/WhatsNewSettings;->whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;

    .line 19
    iput-object p2, p0, Lcom/squareup/tour/WhatsNewSettings;->tourProvider:Lcom/squareup/tour/WhatsNewTourProvider;

    .line 20
    iput-object p3, p0, Lcom/squareup/tour/WhatsNewSettings;->tourPreference:Lcom/squareup/tour/TourPreference;

    .line 22
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllUnseenPages()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/tour/WhatsNewBadge;->changeBadgeCount(I)V

    return-void
.end method

.method private getAllPagesSerializable()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings;->tourProvider:Lcom/squareup/tour/WhatsNewTourProvider;

    invoke-interface {v0}, Lcom/squareup/tour/WhatsNewTourProvider;->getTourPages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private markPagesAsSeen(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/SerializableHasTourPages;",
            ">;)V"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings;->tourPreference:Lcom/squareup/tour/TourPreference;

    invoke-virtual {v0, p1}, Lcom/squareup/tour/TourPreference;->setSeenTourPages(Ljava/util/Set;)V

    return-void
.end method


# virtual methods
.method public getAllPages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllPagesSerializable()Ljava/util/List;

    move-result-object v0

    .line 53
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 55
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tour/SerializableHasTourPages;

    .line 56
    invoke-virtual {v2}, Lcom/squareup/tour/SerializableHasTourPages;->getHasTourPages()Lcom/squareup/tour/Tour$HasTourPages;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getAllUnseenPages()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings;->tourPreference:Lcom/squareup/tour/TourPreference;

    invoke-virtual {v0}, Lcom/squareup/tour/TourPreference;->getSeenTourPageNames()Ljava/util/Set;

    move-result-object v0

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 68
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllPagesSerializable()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tour/SerializableHasTourPages;

    .line 69
    invoke-virtual {v3}, Lcom/squareup/tour/SerializableHasTourPages;->getUniqueName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 70
    invoke-virtual {v3}, Lcom/squareup/tour/SerializableHasTourPages;->getHasTourPages()Lcom/squareup/tour/Tour$HasTourPages;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public hasPages()Z
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasUnseenPages()Z
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllUnseenPages()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public markAllPagesAsSeen()V
    .locals 2

    .line 91
    new-instance v0, Ljava/util/LinkedHashSet;

    .line 92
    invoke-direct {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllPagesSerializable()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 93
    invoke-direct {p0, v0}, Lcom/squareup/tour/WhatsNewSettings;->markPagesAsSeen(Ljava/util/Set;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings;->whatsNewBadge:Lcom/squareup/tour/WhatsNewBadge;

    invoke-virtual {p0}, Lcom/squareup/tour/WhatsNewSettings;->getAllUnseenPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/tour/WhatsNewBadge;->changeBadgeCount(I)V

    return-void
.end method

.method public shouldDisplayAfterOnboarding()Z
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/tour/WhatsNewSettings;->tourProvider:Lcom/squareup/tour/WhatsNewTourProvider;

    invoke-interface {v0}, Lcom/squareup/tour/WhatsNewTourProvider;->getDisplayAfterOnboarding()Z

    move-result v0

    return v0
.end method
