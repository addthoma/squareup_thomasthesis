.class public final Lcom/squareup/thread/MainThreadModule;
.super Ljava/lang/Object;
.source "MainThreadModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0001J\u0008\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/thread/MainThreadModule;",
        "",
        "()V",
        "provideAndroidMainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "provideMainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "impl-main-thread-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/thread/MainThreadModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/thread/MainThreadModule;

    invoke-direct {v0}, Lcom/squareup/thread/MainThreadModule;-><init>()V

    sput-object v0, Lcom/squareup/thread/MainThreadModule;->INSTANCE:Lcom/squareup/thread/MainThreadModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideAndroidMainThread()Lcom/squareup/thread/executor/MainThread;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/AppScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/thread/AndroidMainThread;->Companion:Lcom/squareup/thread/AndroidMainThread$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/AndroidMainThread$Companion;->create()Lcom/squareup/thread/AndroidMainThread;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    return-object v0
.end method

.method public final provideMainThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 1
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 17
    sget-object v0, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->INSTANCE:Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;

    check-cast v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object v0
.end method
