.class public final Lcom/squareup/thread/AndroidMainThread;
.super Lcom/squareup/thread/executor/StoppableHandlerExecutor;
.source "AndroidMainThread.kt"

# interfaces
.implements Lcom/squareup/thread/executor/MainThread;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/thread/AndroidMainThread$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00042\u00020\u00012\u00020\u0002:\u0001\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/thread/AndroidMainThread;",
        "Lcom/squareup/thread/executor/StoppableHandlerExecutor;",
        "Lcom/squareup/thread/executor/MainThread;",
        "()V",
        "Companion",
        "impl-main-thread_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/thread/AndroidMainThread$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/thread/AndroidMainThread$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/thread/AndroidMainThread$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/thread/AndroidMainThread;->Companion:Lcom/squareup/thread/AndroidMainThread$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 10
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/thread/executor/StoppableHandlerExecutor;-><init>(Landroid/os/Handler;Z)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/thread/AndroidMainThread;-><init>()V

    return-void
.end method

.method public static final create()Lcom/squareup/thread/AndroidMainThread;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/thread/AndroidMainThread;->Companion:Lcom/squareup/thread/AndroidMainThread$Companion;

    invoke-virtual {v0}, Lcom/squareup/thread/AndroidMainThread$Companion;->create()Lcom/squareup/thread/AndroidMainThread;

    move-result-object v0

    return-object v0
.end method
