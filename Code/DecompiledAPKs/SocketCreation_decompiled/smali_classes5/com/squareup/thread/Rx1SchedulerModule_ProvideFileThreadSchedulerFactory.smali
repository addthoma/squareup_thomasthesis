.class public final Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;
.super Ljava/lang/Object;
.source "Rx1SchedulerModule_ProvideFileThreadSchedulerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lrx/Scheduler;",
        ">;"
    }
.end annotation


# instance fields
.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx1FileScheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx1FileScheduler;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;->schedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/Rx1FileScheduler;",
            ">;)",
            "Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFileThreadScheduler(Lcom/squareup/thread/Rx1FileScheduler;)Lrx/Scheduler;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/thread/Rx1SchedulerModule;->provideFileThreadScheduler(Lcom/squareup/thread/Rx1FileScheduler;)Lrx/Scheduler;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lrx/Scheduler;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;->get()Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public get()Lrx/Scheduler;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/Rx1FileScheduler;

    invoke-static {v0}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideFileThreadSchedulerFactory;->provideFileThreadScheduler(Lcom/squareup/thread/Rx1FileScheduler;)Lrx/Scheduler;

    move-result-object v0

    return-object v0
.end method
