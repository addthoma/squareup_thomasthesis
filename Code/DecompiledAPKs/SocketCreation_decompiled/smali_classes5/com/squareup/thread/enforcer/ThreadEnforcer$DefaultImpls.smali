.class public final Lcom/squareup/thread/enforcer/ThreadEnforcer$DefaultImpls;
.super Ljava/lang/Object;
.source "ThreadEnforcer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/thread/enforcer/ThreadEnforcer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1

    const-string v0, "Operation not performed on required thread!"

    .line 30
    invoke-interface {p0, v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine(Ljava/lang/String;)V

    return-void
.end method

.method public static confine(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-interface {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->isTargetThread()Z

    move-result p0

    if-eqz p0, :cond_0

    return-void

    .line 35
    :cond_0
    new-instance p0, Ljava/lang/IllegalThreadStateException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1

    const-string v0, "Operation not allowed on this thread!"

    .line 40
    invoke-interface {p0, v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    return-void
.end method

.method public static forbid(Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-interface {p0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->isTargetThread()Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 45
    :cond_0
    new-instance p0, Ljava/lang/IllegalThreadStateException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
