.class public final Lcom/squareup/text/DurationFormatter;
.super Ljava/lang/Object;
.source "DurationFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDurationFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DurationFormatter.kt\ncom/squareup/text/DurationFormatter\n*L\n1#1,93:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/text/DurationFormatter;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "format",
        "",
        "duration",
        "Lorg/threeten/bp/Duration;",
        "showNoDurationIfZero",
        "",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static synthetic format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 30
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/text/DurationFormatter;->format(Lorg/threeten/bp/Duration;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final format(Lorg/threeten/bp/Duration;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final format(Lorg/threeten/bp/Duration;Z)Ljava/lang/String;
    .locals 12

    const-string v0, "duration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMinutes()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    if-eqz p2, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->no_duration:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 40
    :cond_0
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toDays()J

    move-result-wide v1

    const-wide/16 v5, 0x1

    cmp-long p2, v1, v3

    if-lez p2, :cond_2

    cmp-long v7, v1, v5

    if-lez v7, :cond_1

    .line 43
    iget-object v7, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/utilities/R$string;->time_days:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v7

    .line 44
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    const-string v9, "days"

    invoke-virtual {v7, v9, v8}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v7

    .line 45
    invoke-virtual {v7}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v7

    goto :goto_0

    .line 47
    :cond_1
    iget-object v7, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget v8, Lcom/squareup/utilities/R$string;->time_day:I

    invoke-interface {v7, v8}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v7

    .line 48
    invoke-virtual {v7}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v7

    .line 50
    :goto_0
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 51
    invoke-virtual {p1, v1, v2}, Lorg/threeten/bp/Duration;->minusDays(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-string v1, "duration.minusDays(days)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    :cond_2
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toHours()J

    move-result-wide v1

    const-string v7, ", "

    if-lez p2, :cond_3

    cmp-long v8, v1, v3

    if-lez v8, :cond_3

    .line 56
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    cmp-long v8, v1, v3

    if-lez v8, :cond_5

    cmp-long v9, v1, v5

    if-lez v9, :cond_4

    .line 61
    iget-object v9, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/utilities/R$string;->time_hours_long:I

    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v9

    .line 62
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    check-cast v10, Ljava/lang/CharSequence;

    const-string v11, "hours"

    invoke-virtual {v9, v11, v10}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v9

    .line 63
    invoke-virtual {v9}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v9

    goto :goto_1

    .line 65
    :cond_4
    iget-object v9, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget v10, Lcom/squareup/utilities/R$string;->time_hour_long:I

    invoke-interface {v9, v10}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v9

    .line 66
    invoke-virtual {v9}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v9

    .line 68
    :goto_1
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {p1, v1, v2}, Lorg/threeten/bp/Duration;->minusHours(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-string v1, "duration.minusHours(hours)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    :cond_5
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMinutes()J

    move-result-wide v1

    if-gtz p2, :cond_6

    if-lez v8, :cond_7

    :cond_6
    cmp-long p1, v1, v3

    if-lez p1, :cond_7

    .line 74
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    cmp-long p1, v1, v3

    if-gtz p1, :cond_9

    .line 77
    move-object p1, v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_8

    const/4 p1, 0x1

    goto :goto_2

    :cond_8
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_b

    :cond_9
    cmp-long p1, v1, v5

    if-nez p1, :cond_a

    .line 79
    iget-object p1, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->time_min:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 80
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "minute"

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_3

    .line 83
    :cond_a
    iget-object p1, p0, Lcom/squareup/text/DurationFormatter;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/utilities/R$string;->time_mins:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 84
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "minutes"

    invoke-virtual {p1, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 85
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 87
    :goto_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 90
    :cond_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "result.toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
