.class public Lcom/squareup/text/Fonts$FittedTextInfo;
.super Ljava/lang/Object;
.source "Fonts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/Fonts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FittedTextInfo"
.end annotation


# instance fields
.field public final height:I

.field public final textSize:I

.field public final width:I


# direct methods
.method public constructor <init>(FFF)V
    .locals 2

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    float-to-double v0, p1

    .line 115
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int p1, v0

    iput p1, p0, Lcom/squareup/text/Fonts$FittedTextInfo;->textSize:I

    float-to-double p1, p2

    .line 116
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    iput p1, p0, Lcom/squareup/text/Fonts$FittedTextInfo;->width:I

    float-to-double p1, p3

    .line 117
    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-int p1, p1

    iput p1, p0, Lcom/squareup/text/Fonts$FittedTextInfo;->height:I

    return-void
.end method
