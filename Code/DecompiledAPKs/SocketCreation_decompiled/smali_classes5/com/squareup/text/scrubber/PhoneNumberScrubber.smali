.class public Lcom/squareup/text/scrubber/PhoneNumberScrubber;
.super Ljava/lang/Object;
.source "PhoneNumberScrubber.java"

# interfaces
.implements Lcom/squareup/text/InsertingScrubber;


# instance fields
.field private final asYouTypeFormatter:Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/CountryCode;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/google/i18n/phonenumbers/PhoneNumberUtil;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->getAsYouTypeFormatter(Ljava/lang/String;)Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/text/scrubber/PhoneNumberScrubber;->asYouTypeFormatter:Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;

    return-void
.end method


# virtual methods
.method public scrub(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 23
    iget-object p1, p0, Lcom/squareup/text/scrubber/PhoneNumberScrubber;->asYouTypeFormatter:Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;

    invoke-virtual {p1}, Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;->clear()V

    .line 26
    invoke-static {p2}, Lcom/google/i18n/phonenumbers/PhoneNumberUtil;->normalizeDiallableCharsOnly(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    array-length p2, p1

    const/4 v0, 0x0

    const-string v1, ""

    move-object v2, v1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    aget-char v2, p1, v1

    .line 27
    iget-object v3, p0, Lcom/squareup/text/scrubber/PhoneNumberScrubber;->asYouTypeFormatter:Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;

    invoke-virtual {v3, v2}, Lcom/google/i18n/phonenumbers/AsYouTypeFormatter;->inputDigit(C)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string p1, " "

    .line 32
    invoke-virtual {v2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 33
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v2, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_1
    return-object v2
.end method
