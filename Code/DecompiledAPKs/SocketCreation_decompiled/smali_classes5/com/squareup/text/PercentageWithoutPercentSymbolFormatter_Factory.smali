.class public final Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;
.super Ljava/lang/Object;
.source "PercentageWithoutPercentSymbolFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter_Factory;->get()Lcom/squareup/text/PercentageWithoutPercentSymbolFormatter;

    move-result-object v0

    return-object v0
.end method
