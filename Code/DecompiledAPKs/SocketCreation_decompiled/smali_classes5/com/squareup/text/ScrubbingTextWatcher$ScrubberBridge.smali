.class Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;
.super Ljava/lang/Object;
.source "ScrubbingTextWatcher.java"

# interfaces
.implements Lcom/squareup/text/SelectableTextScrubber;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/ScrubbingTextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScrubberBridge"
.end annotation


# instance fields
.field private final scrubber:Lcom/squareup/text/Scrubber;


# direct methods
.method private constructor <init>(Lcom/squareup/text/Scrubber;)V
    .locals 0

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;->scrubber:Lcom/squareup/text/Scrubber;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/ScrubbingTextWatcher$1;)V
    .locals 0

    .line 166
    invoke-direct {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;-><init>(Lcom/squareup/text/Scrubber;)V

    return-void
.end method


# virtual methods
.method public scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 2

    .line 174
    iget-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher$ScrubberBridge;->scrubber:Lcom/squareup/text/Scrubber;

    iget-object v0, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/text/Scrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 178
    invoke-virtual {p2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->cursorAtEndOfText()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    new-instance p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p2, p1, v0, v0}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    goto :goto_0

    .line 184
    :cond_0
    iget v1, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 185
    iget p2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 186
    new-instance v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    move-object p2, v0

    :goto_0
    return-object p2
.end method
