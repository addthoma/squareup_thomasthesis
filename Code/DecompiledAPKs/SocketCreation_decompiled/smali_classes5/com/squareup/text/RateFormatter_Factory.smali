.class public final Lcom/squareup/text/RateFormatter_Factory;
.super Ljava/lang/Object;
.source "RateFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/RateFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/text/RateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/text/RateFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/text/RateFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/text/RateFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;)",
            "Lcom/squareup/text/RateFormatter_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/text/RateFormatter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/text/RateFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/text/RateFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Lcom/squareup/text/RateFormatter;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/text/RateFormatter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/text/RateFormatter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/text/RateFormatter;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/text/RateFormatter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/text/RateFormatter_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/text/RateFormatter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/text/RateFormatter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;)Lcom/squareup/text/RateFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/text/RateFormatter_Factory;->get()Lcom/squareup/text/RateFormatter;

    move-result-object v0

    return-object v0
.end method
