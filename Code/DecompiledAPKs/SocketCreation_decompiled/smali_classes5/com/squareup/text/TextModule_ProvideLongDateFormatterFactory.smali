.class public final Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;
.super Ljava/lang/Object;
.source "TextModule_ProvideLongDateFormatterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/text/DateFormat;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;

    invoke-direct {v0, p0}, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLongDateFormatter(Landroid/app/Application;)Ljava/text/DateFormat;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/text/TextModule;->provideLongDateFormatter(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/text/DateFormat;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->get()Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/text/DateFormat;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/text/TextModule_ProvideLongDateFormatterFactory;->provideLongDateFormatter(Landroid/app/Application;)Ljava/text/DateFormat;

    move-result-object v0

    return-object v0
.end method
