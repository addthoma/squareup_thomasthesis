.class public Lcom/squareup/text/PostalCodes;
.super Ljava/lang/Object;
.source "PostalCodes.java"


# static fields
.field private static final CA_POSTAL_CODE:Ljava/util/regex/Pattern;

.field private static final US_POSTAL_CODE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "^([0-9]{5})(-[0-9]{4})?$"

    .line 8
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/PostalCodes;->US_POSTAL_CODE:Ljava/util/regex/Pattern;

    const-string v0, "^[a-z][0-9][a-z] [0-9][a-z][0-9]$"

    const/4 v1, 0x2

    .line 10
    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/text/PostalCodes;->CA_POSTAL_CODE:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static getShortZipCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/text/PostalCodes;->US_POSTAL_CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 29
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    .line 30
    invoke-virtual {p0, p1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    return-object p1
.end method

.method public static isCaPostalCode(Ljava/lang/String;)Z
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/text/PostalCodes;->CA_POSTAL_CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    return p0
.end method

.method public static isUsZipCode(Ljava/lang/String;)Z
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/text/PostalCodes;->US_POSTAL_CODE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    return p0
.end method
