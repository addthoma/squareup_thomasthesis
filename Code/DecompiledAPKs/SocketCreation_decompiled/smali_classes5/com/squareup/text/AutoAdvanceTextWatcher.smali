.class public Lcom/squareup/text/AutoAdvanceTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "AutoAdvanceTextWatcher.java"


# instance fields
.field private final compact:Z

.field private length:I

.field private nextView:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(ZLandroid/view/View;Landroid/view/View;I)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 27
    iput-boolean p1, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->compact:Z

    .line 28
    iput-object p2, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->view:Landroid/view/View;

    .line 29
    iput-object p3, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->nextView:Landroid/view/View;

    .line 30
    iput p4, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->length:I

    return-void
.end method

.method public static focusNextView(Landroid/view/View;Landroid/view/View;Z)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const/16 v0, 0x82

    const/4 v1, 0x0

    .line 54
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    if-eqz p2, :cond_2

    const/16 p1, 0x8

    .line 56
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 34
    iget v0, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->length:I

    if-lez v0, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    iget v0, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->length:I

    if-ne p1, v0, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->nextView:Landroid/view/View;

    iget-boolean v1, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->compact:Z

    invoke-static {p1, v0, v1}, Lcom/squareup/text/AutoAdvanceTextWatcher;->focusNextView(Landroid/view/View;Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public setLength(I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->length:I

    return-void
.end method

.method public setNextView(Landroid/view/View;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/text/AutoAdvanceTextWatcher;->nextView:Landroid/view/View;

    return-void
.end method
