.class Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;
.super Ljava/lang/Object;
.source "SimplePasswordTransformationMethod.java"

# interfaces
.implements Ljava/lang/CharSequence;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/SimplePasswordTransformationMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PasswordCharSequence"
.end annotation


# instance fields
.field private final source:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/squareup/text/SimplePasswordTransformationMethod;


# direct methods
.method constructor <init>(Lcom/squareup/text/SimplePasswordTransformationMethod;Ljava/lang/CharSequence;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->this$0:Lcom/squareup/text/SimplePasswordTransformationMethod;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p2, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->source:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public charAt(I)C
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->source:Ljava/lang/CharSequence;

    invoke-interface {v0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    return v1

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->this$0:Lcom/squareup/text/SimplePasswordTransformationMethod;

    invoke-static {v0}, Lcom/squareup/text/SimplePasswordTransformationMethod;->access$000(Lcom/squareup/text/SimplePasswordTransformationMethod;)Z

    move-result v0

    const/16 v1, 0x2022

    if-nez v0, :cond_1

    return v1

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->this$0:Lcom/squareup/text/SimplePasswordTransformationMethod;

    invoke-static {v0}, Lcom/squareup/text/SimplePasswordTransformationMethod;->access$100(Lcom/squareup/text/SimplePasswordTransformationMethod;)Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 92
    iget-object v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->source:Ljava/lang/CharSequence;

    invoke-interface {v0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result p1

    return p1

    :cond_3
    return v1
.end method

.method public length()I
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->source:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 2

    sub-int/2addr p2, p1

    .line 98
    new-array p2, p2, [C

    const/4 v0, 0x0

    .line 99
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    add-int v1, v0, p1

    .line 100
    invoke-virtual {p0, v1}, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->charAt(I)C

    move-result v1

    aput-char v1, p2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_0
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/String;-><init>([C)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 106
    invoke-virtual {p0}, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/text/SimplePasswordTransformationMethod$PasswordCharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
