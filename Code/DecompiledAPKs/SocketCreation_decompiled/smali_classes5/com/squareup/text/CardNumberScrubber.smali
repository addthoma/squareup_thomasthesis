.class public Lcom/squareup/text/CardNumberScrubber;
.super Ljava/lang/Object;
.source "CardNumberScrubber.java"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# static fields
.field private static final OBFUSCATION_PAD_CHAR:C = '\u2022'

.field private static final SPACE_CHAR:C = ' '


# instance fields
.field private brand:Lcom/squareup/Card$Brand;

.field private strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object v0, p0, Lcom/squareup/text/CardNumberScrubber;->brand:Lcom/squareup/Card$Brand;

    .line 18
    new-instance v0, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;

    invoke-direct {v0}, Lcom/squareup/register/widgets/card/PermissiveCardPanValidationStrategy;-><init>()V

    iput-object v0, p0, Lcom/squareup/text/CardNumberScrubber;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-void
.end method

.method private insertSpace(I)Z
    .locals 5

    .line 47
    sget-object v0, Lcom/squareup/text/CardNumberScrubber$1;->$SwitchMap$com$squareup$Card$Brand:[I

    iget-object v1, p0, Lcom/squareup/text/CardNumberScrubber;->brand:Lcom/squareup/Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    const/4 v4, 0x2

    if-eq v0, v4, :cond_1

    if-lez p1, :cond_0

    .line 61
    rem-int/2addr p1, v2

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    if-eq p1, v2, :cond_2

    const/16 v0, 0xa

    if-ne p1, v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1
.end method


# virtual methods
.method getBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/text/CardNumberScrubber;->brand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    .line 25
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 26
    iget-object v5, p0, Lcom/squareup/text/CardNumberScrubber;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-interface {v5, v4}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->panCharacterValid(C)Z

    move-result v5

    if-nez v5, :cond_0

    const/16 v5, 0x2022

    if-ne v4, v5, :cond_2

    .line 27
    :cond_0
    invoke-direct {p0, v3}, Lcom/squareup/text/CardNumberScrubber;->insertSpace(I)Z

    move-result v5

    if-eqz v5, :cond_1

    const/16 v5, 0x20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 28
    :cond_1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    .line 32
    :cond_2
    iget-object v4, p0, Lcom/squareup/text/CardNumberScrubber;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    iget-object v5, p0, Lcom/squareup/text/CardNumberScrubber;->brand:Lcom/squareup/Card$Brand;

    invoke-interface {v4, v5, v3}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->isMaximumLength(Lcom/squareup/Card$Brand;I)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 35
    :cond_4
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public setBrand(Lcom/squareup/Card$Brand;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/text/CardNumberScrubber;->brand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method public setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/text/CardNumberScrubber;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-void
.end method
