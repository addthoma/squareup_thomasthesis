.class Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;
.super Ljava/lang/Object;
.source "ScrubbingTextWatcher.java"

# interfaces
.implements Lcom/squareup/text/SelectableTextScrubber;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/text/ScrubbingTextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InsertingScrubberBridge"
.end annotation


# instance fields
.field private final insertingScrubber:Lcom/squareup/text/InsertingScrubber;


# direct methods
.method private constructor <init>(Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    iput-object p1, p0, Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;->insertingScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/ScrubbingTextWatcher$1;)V
    .locals 0

    .line 196
    invoke-direct {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;-><init>(Lcom/squareup/text/InsertingScrubber;)V

    return-void
.end method


# virtual methods
.method public scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/text/ScrubbingTextWatcher$InsertingScrubberBridge;->insertingScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-interface {v0, p1, p2}, Lcom/squareup/text/InsertingScrubber;->scrub(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 205
    new-instance p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p2, p1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p2
.end method
