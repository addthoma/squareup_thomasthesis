.class public final Lcom/squareup/text/PatternScrubber;
.super Ljava/lang/Object;
.source "PatternScrubber.kt"

# interfaces
.implements Lcom/squareup/text/Scrubber;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPatternScrubber.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PatternScrubber.kt\ncom/squareup/text/PatternScrubber\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000c\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\n\u0010\u0008\u001a\u00060\tj\u0002`\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002J\u0010\u0010\r\u001a\u00020\u000c2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002J\u0018\u0010\u000e\u001a\u00020\u000c2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/text/PatternScrubber;",
        "Lcom/squareup/text/Scrubber;",
        "pattern",
        "",
        "(Ljava/lang/String;)V",
        "consumeLiterals",
        "",
        "index",
        "literals",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "isEscape",
        "",
        "isLiteral",
        "isMatch",
        "inputChar",
        "",
        "scrub",
        "proposed",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pattern:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "pattern"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    return-void
.end method

.method private final consumeLiterals(ILjava/lang/StringBuilder;)I
    .locals 2

    .line 101
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/text/PatternScrubber;->isEscape(I)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 p1, p1, 0x1

    .line 102
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne p1, v0, :cond_0

    goto :goto_2

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 104
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/text/PatternScrubber;->isLiteral(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    move p1, v1

    goto :goto_0

    :cond_2
    :goto_2
    return p1
.end method

.method private final isEscape(I)Z
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v0, 0x5c

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isLiteral(I)Z
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lt p1, v0, :cond_0

    return v1

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x23

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v0, 0x41

    if-eq p1, v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private final isMatch(IC)Z
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lt p1, v0, :cond_0

    return v1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    const/16 v0, 0x23

    if-eq p1, v0, :cond_2

    const/16 v0, 0x41

    if-eq p1, v0, :cond_1

    goto :goto_0

    .line 86
    :cond_1
    invoke-static {p2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    goto :goto_0

    .line 85
    :cond_2
    invoke-static {p2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    :goto_0
    return v1
.end method


# virtual methods
.method public scrub(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const-string v0, "proposed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const-string p1, ""

    return-object p1

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    invoke-direct {p0, v1, v2}, Lcom/squareup/text/PatternScrubber;->consumeLiterals(ILjava/lang/StringBuilder;)I

    move-result v3

    .line 42
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    const-string v4, "(this as java.lang.String).toCharArray()"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, p1

    move v5, v3

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_6

    aget-char v6, p1, v3

    .line 43
    iget-object v7, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v5, v7, :cond_2

    goto :goto_3

    .line 45
    :cond_2
    invoke-direct {p0, v5}, Lcom/squareup/text/PatternScrubber;->isEscape(I)Z

    move-result v7

    if-eqz v7, :cond_4

    add-int/lit8 v5, v5, 0x1

    .line 47
    iget-object v6, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v5, v6, :cond_3

    goto :goto_3

    .line 48
    :cond_3
    iget-object v6, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    add-int/lit8 v7, v5, 0x1

    invoke-virtual {v6, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v5, "pending.append(pattern[patternPosition++])"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move v5, v7

    goto :goto_2

    .line 49
    :cond_4
    invoke-direct {p0, v5, v6}, Lcom/squareup/text/PatternScrubber;->isMatch(IC)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 51
    move-object v7, v2

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 54
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    .line 58
    :cond_5
    :goto_2
    invoke-direct {p0, v5, v2}, Lcom/squareup/text/PatternScrubber;->consumeLiterals(ILjava/lang/StringBuilder;)I

    move-result v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 61
    :cond_6
    :goto_3
    iget-object p1, p0, Lcom/squareup/text/PatternScrubber;->pattern:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-ne v5, p1, :cond_7

    .line 63
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 66
    :cond_7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "sb.toString()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
