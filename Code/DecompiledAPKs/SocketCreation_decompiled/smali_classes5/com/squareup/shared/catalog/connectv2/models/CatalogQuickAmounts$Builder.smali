.class public Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
.super Ljava/lang/Object;
.source "CatalogQuickAmounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

.field private quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;->QUICK_AMOUNTS_SETTINGS:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 52
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iget-object v0, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 57
    iget-object p1, p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$1;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 3

    .line 84
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    iget-object v1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object v2, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    .line 86
    invoke-virtual {v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)V

    return-object v0
.end method

.method public enableOnlyAtLocation(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    .line 77
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 78
    iget-object p1, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->backingObject:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setAmounts(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
            ">;)",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->amounts(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    return-object p0
.end method

.method public setCatalogQuickAmountsSettingsOption(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->option(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettingsOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    return-object p0
.end method

.method public setEligibleForAutoAmounts(Z)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts$Builder;->quickAmountsSettings:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;->eligible_for_auto_amounts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings$Builder;

    return-object p0
.end method
