.class Lcom/squareup/shared/catalog/sync/CatalogSync$9;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->requestNewSessionId()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$9;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Catalog: Clearing all pending puts"

    .line 401
    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 402
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readPendingWriteRequests()Ljava/util/List;

    move-result-object v0

    .line 403
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/PendingWriteRequest;

    .line 404
    iget-wide v1, v1, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    invoke-virtual {p1, v1, v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->deletePendingWriteRequest(J)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
