.class public interface abstract Lcom/squareup/shared/catalog/Catalog$Local;
.super Ljava/lang/Object;
.source "Catalog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/Catalog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Local"
.end annotation


# virtual methods
.method public abstract canWrite()Z
.end method

.method public abstract countDiscounts()I
.end method

.method public abstract countItems(Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract countTaxItems(Ljava/lang/String;Ljava/util/List;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation
.end method

.method public abstract findAllItemModifiers()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findAllMenus()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogMenu;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findAllTicketTemplatesByGroupId()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract findByTokenOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract findCatalogConnectV2Object(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract findCatalogConnectV2Objects(Ljava/lang/Class;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract findCategoryItems(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findEnabledItemModifiers(Ljava/lang/String;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findItemFeeMemberships(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemFeeMemberships(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemFeeMembershipsForTax(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemModifierListMemberships(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemModifierMemberships(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemModifierRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierList;",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findItemPageMembershipsForPages(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemTaxRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findItemTaxes(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findItemVariations(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findModifierItemMemberships(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findModifierOptions(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findObjectIdsForRelatedObjects(Lcom/squareup/api/items/Type;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findObjectsByIds(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract findObjectsByIds(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findObjectsByTokens(Ljava/lang/Class;Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract findPageTiles(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/PageTiles;
.end method

.method public abstract findPagesForMenuGroup(Ljava/lang/String;Lcom/squareup/api/items/PageLayout;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/PageLayout;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findRelatedItemIdsForTax(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findRelatedItemIdsForTax(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findSurchargeFeeMemberships(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findSurchargeFeeMembershipsForTax(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogSurchargeFeeMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findTaxItemRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract findTaxItems(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findTicketTemplates(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation
.end method

.method public abstract findTilePageMemberships(Ljava/lang/String;Lcom/squareup/api/items/Type;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Type;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCategoryNameMap()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract hasAppliedServerVersion()Z
.end method

.method public abstract readAllCatalogConnectV2Objects(Ljava/lang/Class;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readAllCategories()Lcom/squareup/shared/catalog/CatalogObjectCursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/shared/catalog/CatalogObjectCursor<",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllDiningOptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiningOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllFixedDiscounts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ")",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract readAllPages()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllPlaceholders()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogPlaceholder;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllProductSets()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllTaxRules()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAllTaxes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTax;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readAppliedServerVersion()J
.end method

.method public abstract readCompDiscounts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readConfiguration()Lcom/squareup/shared/catalog/models/CatalogConfiguration;
.end method

.method public abstract readIdsForAllObjectsOfType(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readItems(Ljava/util/Set;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract readVoidReasons()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogVoidReason;",
            ">;"
        }
    .end annotation
.end method

.method public abstract write(Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation
.end method

.method public abstract write(Ljava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation
.end method

.method public abstract writeConnectV2Object(Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;)V
.end method

.method public abstract writeConnectV2Objects(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation
.end method
