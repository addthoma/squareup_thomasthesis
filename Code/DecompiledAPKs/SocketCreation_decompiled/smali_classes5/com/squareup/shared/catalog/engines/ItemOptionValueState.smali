.class public final enum Lcom/squareup/shared/catalog/engines/ItemOptionValueState;
.super Ljava/lang/Enum;
.source "ItemOptionValueState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/engines/ItemOptionValueState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

.field public static final enum HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

.field public static final enum SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

.field public static final enum SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

.field public static final enum UNSELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 7
    new-instance v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    const/4 v1, 0x0

    const-string v2, "HIDDEN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 8
    new-instance v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    const/4 v2, 0x1

    const-string v3, "UNSELECTABLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->UNSELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 9
    new-instance v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    const/4 v3, 0x2

    const-string v4, "SELECTABLE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 10
    new-instance v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    const/4 v4, 0x3

    const-string v5, "SELECTED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    .line 5
    sget-object v5, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->HIDDEN:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->UNSELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTABLE:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->SELECTED:Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->$VALUES:[Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/engines/ItemOptionValueState;
    .locals 1

    .line 5
    const-class v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/engines/ItemOptionValueState;
    .locals 1

    .line 5
    sget-object v0, Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->$VALUES:[Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/engines/ItemOptionValueState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/engines/ItemOptionValueState;

    return-object v0
.end method
