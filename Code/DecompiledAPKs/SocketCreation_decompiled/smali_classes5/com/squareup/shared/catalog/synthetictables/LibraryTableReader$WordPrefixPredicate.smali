.class final Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;
.super Ljava/lang/Object;
.source "LibraryTableReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WordPrefixPredicate"
.end annotation


# instance fields
.field private final params:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final query:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1026
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1027
    iput-object p1, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->query:Ljava/lang/String;

    .line 1028
    iput-object p2, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->params:Ljava/util/List;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/util/List;
    .locals 0

    .line 1022
    iget-object p0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->params:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;)Ljava/lang/String;
    .locals 0

    .line 1022
    iget-object p0, p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader$WordPrefixPredicate;->query:Ljava/lang/String;

    return-object p0
.end method
