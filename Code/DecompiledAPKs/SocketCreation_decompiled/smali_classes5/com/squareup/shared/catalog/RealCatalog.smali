.class public Lcom/squareup/shared/catalog/RealCatalog;
.super Ljava/lang/Object;
.source "RealCatalog.java"

# interfaces
.implements Lcom/squareup/shared/catalog/Catalog;


# instance fields
.field private final catalogLocal:Lcom/squareup/shared/catalog/CatalogLocal;

.field private final catalogOnline:Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;

.field private final catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

.field private final catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field private final connectV2UpdateHandler:Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/CatalogStore$Factory;Lcom/squareup/shared/catalog/CatalogFile;Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Ljava/util/List;Lcom/squareup/shared/catalog/logging/Clock;Ljava/io/File;Ljava/util/List;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogStore$Factory;",
            "Lcom/squareup/shared/catalog/CatalogFile<",
            "Lcom/squareup/shared/catalog/StorageMetadata;",
            ">;",
            "Lcom/squareup/shared/catalog/CatalogEndpoint;",
            "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;",
            "Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;",
            "Lcom/squareup/shared/catalog/logging/CatalogAnalytics;",
            "Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;",
            ">;",
            "Lcom/squareup/shared/catalog/logging/Clock;",
            "Ljava/io/File;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v14, p5

    move-object/from16 v15, p7

    move-object/from16 v8, p8

    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object v14, v0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    move-object/from16 v13, p6

    .line 68
    iput-object v13, v0, Lcom/squareup/shared/catalog/RealCatalog;->fileThread:Ljava/util/concurrent/Executor;

    .line 69
    iput-object v15, v0, Lcom/squareup/shared/catalog/RealCatalog;->httpThread:Ljava/util/concurrent/Executor;

    .line 70
    iput-object v8, v0, Lcom/squareup/shared/catalog/RealCatalog;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    .line 72
    new-instance v9, Lcom/squareup/shared/catalog/Storage;

    move-object v2, v9

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p8

    move-object/from16 v6, p15

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/shared/catalog/Storage;-><init>(Lcom/squareup/shared/catalog/CatalogStore$Factory;Lcom/squareup/shared/catalog/CatalogFile;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Ljava/io/File;Lcom/squareup/shared/catalog/CatalogEndpoint;)V

    .line 75
    new-instance v2, Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-direct {v2, v9, v8}, Lcom/squareup/shared/catalog/CatalogStoreProvider;-><init>(Lcom/squareup/shared/catalog/Storage;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;)V

    iput-object v2, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    .line 77
    new-instance v12, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    move-object/from16 v2, p16

    invoke-direct {v12, v1, v14, v15, v2}, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/List;)V

    .line 80
    new-instance v11, Lcom/squareup/shared/catalog/sync/CatalogSync;

    iget-object v9, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    move-object v2, v11

    move-object/from16 v3, p3

    move-object/from16 v4, p9

    move-object/from16 v5, p6

    move-object/from16 v6, p5

    move-object/from16 v7, p8

    move-object v8, v9

    move-object/from16 v9, p11

    move-object/from16 v10, p10

    move-object v14, v11

    move-object/from16 v11, p12

    move-object/from16 v16, v12

    move-object/from16 v12, p14

    move-object/from16 v13, v16

    invoke-direct/range {v2 .. v13}, Lcom/squareup/shared/catalog/sync/CatalogSync;-><init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/CatalogStoreProvider;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;)V

    iput-object v14, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 84
    new-instance v2, Lcom/squareup/shared/catalog/CatalogLocal;

    iget-object v3, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    iget-object v4, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    .line 85
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/sync/CatalogSync;->getSyncLocal()Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    move-result-object v4

    move-object/from16 v5, p3

    move-object/from16 v6, p13

    invoke-direct {v2, v3, v5, v6, v4}, Lcom/squareup/shared/catalog/CatalogLocal;-><init>(Lcom/squareup/shared/catalog/CatalogStoreProvider;Lcom/squareup/shared/catalog/CatalogEndpoint;Ljava/util/List;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)V

    iput-object v2, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogLocal:Lcom/squareup/shared/catalog/CatalogLocal;

    .line 86
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;

    invoke-direct {v2, v1}, Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;)V

    iput-object v2, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogOnline:Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;

    .line 87
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;

    iget-object v3, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogLocal:Lcom/squareup/shared/catalog/CatalogLocal;

    iget-object v4, v0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    move-object/from16 p8, v2

    move-object/from16 p9, p4

    move-object/from16 p10, v3

    move-object/from16 p11, v4

    move-object/from16 p12, p5

    move-object/from16 p13, p6

    move-object/from16 p14, p7

    invoke-direct/range {p8 .. p14}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;-><init>(Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/CatalogStoreProvider;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v2, v0, Lcom/squareup/shared/catalog/RealCatalog;->connectV2UpdateHandler:Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;

    return-void
.end method

.method private enforceMainThread()V
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    return-void
.end method

.method private executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 282
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$5;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private executeOnHttpThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->httpThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$6;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private executeOnlineWorkOnHttpThreadAndThenLocalWorkOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .line 247
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->setCloseEnqueued()V

    .line 252
    new-instance v0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$3;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$3;-><init>(Lcom/squareup/shared/catalog/RealCatalog;)V

    .line 255
    invoke-static {}, Lcom/squareup/shared/catalog/CatalogTasks;->explodeOnError()Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v1

    .line 252
    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/catalog/RealCatalog;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 248
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string v1, "Must not close after Storage is slated for closing."

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 106
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    const-string v0, "task"

    .line 107
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "callback"

    .line 108
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Lcom/squareup/shared/catalog/CatalogTask;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/shared/catalog/RealCatalog;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 112
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/StorageClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not expect Storage to be slated for closing when trying to execute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " with callback "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public executeOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 123
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    const-string v0, "task"

    .line 124
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "callback"

    .line 125
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 127
    new-instance v0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Lcom/squareup/shared/catalog/CatalogOnlineTask;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/shared/catalog/RealCatalog;->executeOnHttpThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public executeOnlineThenLocal(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask<",
            "TT;TS;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TS;>;)V"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    const-string v0, "onlineTask"

    .line 134
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "callback"

    .line 135
    invoke-static {p2, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->httpThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$2;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 182
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    .line 183
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/shared/catalog/sync/CatalogSync;->foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void

    .line 184
    :cond_0
    new-instance p1, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string p2, "Must not conduct foreground sync after Storage is slated for closing."

    invoke-direct {p1, p2}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCatalogSync()Lcom/squareup/shared/catalog/sync/CatalogSync;
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    return-object v0
.end method

.method public isCloseEnqueued()Z
    .locals 1

    .line 243
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result v0

    return v0
.end method

.method public isReady()Z
    .locals 1

    .line 239
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method final synthetic lambda$close$4$RealCatalog()Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 253
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->close()V

    const/4 v0, 0x0

    return-object v0
.end method

.method final synthetic lambda$execute$0$RealCatalog(Lcom/squareup/shared/catalog/CatalogTask;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogLocal:Lcom/squareup/shared/catalog/CatalogLocal;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method final synthetic lambda$executeOnFileThread$6$RealCatalog(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 1

    .line 285
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Lcom/squareup/shared/catalog/StorageClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/CatalogTasks;->succeed(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    const-string v0, "Error executing CatalogTask."

    .line 290
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 291
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    .line 292
    iget-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, v0}, Lcom/squareup/shared/catalog/CatalogTasks;->fail(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    .line 287
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/CatalogTasks;->fail(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$executeOnHttpThread$7$RealCatalog(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1

    const-string v0, "Error executing CatalogOnlineTask."

    .line 303
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    :catchall_0
    move-exception p1

    .line 309
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 310
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    .line 311
    iget-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    .line 305
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$executeOnline$1$RealCatalog(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogOnline:Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/CatalogOnlineTask;->perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method

.method final synthetic lambda$executeOnlineThenLocal$2$RealCatalog(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Ljava/lang/Object;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2

    const-string v0, "Error executing the local task in CatalogOnlineThenLocalTask."

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogLocal:Lcom/squareup/shared/catalog/CatalogLocal;

    invoke-interface {p1, v1, p2}, Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;->perform(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    iget-object p2, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-static {p2, p3, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    :catchall_0
    move-exception p1

    .line 169
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 170
    new-instance p2, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {p2, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1, p3, p2}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    .line 165
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 166
    iget-object p2, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p2, p3, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$executeOnlineThenLocal$3$RealCatalog(Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 3

    const-string v0, "Error executing the online task in CatalogOnlineThenLocalTask."

    .line 141
    :try_start_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogOnline:Lcom/squareup/shared/catalog/connectv2/sync/CatalogOnline;

    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;->perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v1

    .line 142
    iget-object v2, v1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-nez v2, :cond_0

    .line 143
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    iget-object v1, p0, Lcom/squareup/shared/catalog/RealCatalog;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$7;

    invoke-direct {v2, p0, p1, v0, p2}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$7;-><init>(Lcom/squareup/shared/catalog/RealCatalog;Lcom/squareup/shared/catalog/CatalogOnlineThenLocalTask;Ljava/lang/Object;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 146
    :cond_0
    :try_start_1
    iget-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    iget-object v1, v1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    invoke-static {v1}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    :try_end_1
    .catch Lcom/squareup/shared/catalog/CatalogException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    .line 154
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 155
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    .line 150
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$purge$5$RealCatalog()Ljava/lang/Void;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    const-string v1, "App intentionally purged the catalog store."

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->purgeIfExists(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;
    .locals 1

    .line 93
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    .line 94
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    move-result-object v0

    return-object v0
.end method

.method public purge()V
    .locals 2

    .line 259
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->setCloseEnqueued()V

    .line 264
    new-instance v0, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$4;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/RealCatalog$$Lambda$4;-><init>(Lcom/squareup/shared/catalog/RealCatalog;)V

    .line 267
    invoke-static {}, Lcom/squareup/shared/catalog/CatalogTasks;->explodeOnError()Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object v1

    .line 264
    invoke-direct {p0, v0, v1}, Lcom/squareup/shared/catalog/RealCatalog;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 260
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string v1, "Must not purge after Storage is slated for closing."

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    return-void
.end method

.method public resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    return-void
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 193
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    .line 194
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void

    .line 195
    :cond_0
    new-instance p1, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string p2, "Must not check if should foreground sync after Storage is slated for closing."

    invoke-direct {p1, p2}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 203
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    .line 204
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z

    move-result p1

    return p1

    .line 205
    :cond_0
    new-instance p1, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string v0, "Must not check if should foreground sync after Storage is slated for closing."

    invoke-direct {p1, v0}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    .line 213
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    .line 214
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->catalogSync:Lcom/squareup/shared/catalog/sync/CatalogSync;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method public upsertCatalogConnectV2Object(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            "Z",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;)V"
        }
    .end annotation

    .line 229
    invoke-direct {p0}, Lcom/squareup/shared/catalog/RealCatalog;->enforceMainThread()V

    if-eqz p3, :cond_1

    .line 230
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/RealCatalog;->isCloseEnqueued()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 231
    :cond_0
    new-instance p1, Lcom/squareup/shared/catalog/StorageClosedException;

    const-string p2, "Cannot upsert catalog connect v2 objects after Storage is slated for closing."

    invoke-direct {p1, p2}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 235
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/RealCatalog;->connectV2UpdateHandler:Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/shared/catalog/connectv2/sync/OnlineConnectV2UpdateHandler;->upsert(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
