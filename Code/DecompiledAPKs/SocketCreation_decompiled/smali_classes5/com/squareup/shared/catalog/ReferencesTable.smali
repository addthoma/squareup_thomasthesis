.class public Lcom/squareup/shared/catalog/ReferencesTable;
.super Ljava/lang/Object;
.source "ReferencesTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/ReferencesTable$Query;
    }
.end annotation


# static fields
.field private static final COLLATE_NOCASE:Ljava/lang/String; = " COLLATE NOCASE"

.field public static final COLUMN_REFERENT_ID:Ljava/lang/String; = "referent_id"

.field public static final COLUMN_REFERENT_TYPE:Ljava/lang/String; = "referent_type"

.field public static final COLUMN_REFERRER_ID:Ljava/lang/String; = "referrer_id"

.field public static final COLUMN_REFERRER_TYPE:Ljava/lang/String; = "referrer_type"

.field private static INSTANCE:Lcom/squareup/shared/catalog/ReferencesTable; = null

.field public static final NAME:Ljava/lang/String; = "references_table"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static instance()Lcom/squareup/shared/catalog/ReferencesTable;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/ReferencesTable;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/squareup/shared/catalog/ReferencesTable;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/ReferencesTable;-><init>()V

    sput-object v0, Lcom/squareup/shared/catalog/ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/ReferencesTable;

    .line 49
    :cond_0
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable;->INSTANCE:Lcom/squareup/shared/catalog/ReferencesTable;

    return-object v0
.end method


# virtual methods
.method public countReferrers(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)I
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    .line 368
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x1

    aput-object p2, v0, p4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    .line 369
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/Queries;->intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I

    move-result p1

    return p1
.end method

.method public countRelatedItems(Lcom/squareup/shared/sql/SQLDatabase;IIILjava/lang/String;Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "III",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p5, v0, v1

    .line 377
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p5, 0x1

    aput-object p2, v0, p5

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x2

    aput-object p2, v0, p4

    .line 378
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x3

    aput-object p2, v0, p3

    .line 380
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->COUNT_OBJECTS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, p6, v0}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 381
    invoke-static {p1}, Lcom/squareup/shared/catalog/Queries;->intFromCountCursor(Lcom/squareup/shared/sql/SQLCursor;)I

    move-result p1

    return p1
.end method

.method public create(Lcom/squareup/shared/sql/SQLDatabase;)V
    .locals 1

    .line 346
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    .line 347
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->CREATE_INDEX_REFERENT_ID_REFERRER_TYPE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/sql/SQLDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public deleteAllReferences(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)V
    .locals 1

    .line 361
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->DELETE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 362
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 363
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method

.method public findAllIdsForRelationship(Lcom/squareup/shared/sql/SQLDatabase;ILjava/util/List;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 412
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 413
    aput-object p4, v0, v1

    .line 414
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x1

    aput-object p2, v0, p4

    .line 415
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    if-ge v1, p2, :cond_0

    add-int/lit8 p2, v1, 0x2

    .line 416
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/Integer;

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p4

    aput-object p4, v0, p2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 419
    :cond_0
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERRER_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    .line 420
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p4

    invoke-static {p4}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p4

    const-string v1, "%referrer_types%"

    invoke-virtual {p2, v1, p4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 421
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p2

    .line 425
    invoke-interface {p2}, Lcom/squareup/shared/sql/SQLCursor;->getCount()I

    move-result p4

    if-nez p4, :cond_1

    .line 426
    invoke-interface {p2}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    .line 427
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_ALL_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    .line 428
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    invoke-static {p3}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p3

    const-string p4, "%referent_types%"

    invoke-virtual {p2, p4, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 429
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p2

    :cond_1
    return-object p2
.end method

.method public findFirstReferentId(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    .line 387
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x1

    aput-object p2, v0, p4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    .line 388
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_FIRST_REFERENT_ID:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    .line 390
    :try_start_0
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 391
    invoke-interface {p1, v1}, Lcom/squareup/shared/sql/SQLCursor;->getString(I)Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :cond_0
    const/4 p2, 0x0

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/squareup/shared/sql/SQLCursor;->close()V

    throw p2
.end method

.method public findReferrers(Lcom/squareup/shared/sql/SQLDatabase;IILjava/util/List;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "II",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 401
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->FIND_REFERRERS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 402
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "%referent_in%"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 404
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p4

    check-cast p4, [Ljava/lang/String;

    .line 405
    array-length v1, p4

    add-int/lit8 v1, v1, -0x2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, v1

    .line 406
    array-length p2, p4

    add-int/lit8 p2, p2, -0x1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p4, p2

    .line 407
    invoke-interface {p1, v0, p4}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public resolve(Lcom/squareup/shared/sql/SQLDatabase;IIILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "III",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p5, v0, v1

    .line 438
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p5, 0x1

    aput-object p2, v0, p5

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x2

    aput-object p2, v0, p4

    .line 439
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x3

    aput-object p2, v0, p3

    if-eqz p6, :cond_0

    .line 443
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_WITH_ITEM_TYPES:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, p6, v0}, Lcom/squareup/shared/catalog/Queries;->rawQueryWithItemTypes(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;Ljava/util/List;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1

    .line 446
    :cond_0
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public resolveIdsForRelationship(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    .line 460
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    const/4 p4, 0x1

    aput-object p3, v0, p4

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    .line 462
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public resolveMembershipsWithPossibleIds(Lcom/squareup/shared/sql/SQLDatabase;ILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 491
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 492
    aput-object p3, v0, v2

    .line 493
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x1

    aput-object p2, v0, p3

    .line 494
    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p2

    if-ge v2, p2, :cond_0

    add-int p2, v1, v2

    .line 495
    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    aput-object p3, v0, p2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 498
    :cond_0
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_MEMBERSHIPS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    .line 499
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result p3

    invoke-static {p3}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p3

    const-string p4, "%referent_ids%"

    invoke-virtual {p2, p4, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 498
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public resolveOuter(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;I)Lcom/squareup/shared/sql/SQLCursor;
    .locals 2

    .line 504
    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p5

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    .line 506
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p4, 0x1

    aput-object p2, v0, p4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    const/4 p2, 0x3

    aput-object p5, v0, p2

    const/4 p2, 0x4

    aput-object p5, v0, p2

    const/4 p2, 0x5

    aput-object p5, v0, p2

    .line 509
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_OUTER:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public resolveRelatedIdsWithPossibleIds(Lcom/squareup/shared/sql/SQLDatabase;IILjava/lang/String;Ljava/util/List;)Lcom/squareup/shared/sql/SQLCursor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/sql/SQLCursor;"
        }
    .end annotation

    .line 472
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 473
    aput-object p4, v0, v2

    .line 474
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    const/4 p4, 0x1

    aput-object p3, v0, p4

    .line 475
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x2

    aput-object p2, v0, p3

    .line 476
    :goto_0
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p2

    if-ge v2, p2, :cond_0

    add-int p2, v1, v2

    .line 477
    invoke-interface {p5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    aput-object p3, v0, p2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 480
    :cond_0
    sget-object p2, Lcom/squareup/shared/catalog/ReferencesTable$Query;->RESOLVE_RELATED_IDS_WITH_POSSIBLE_REFERENT_IDS:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object p2

    .line 481
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p3

    invoke-static {p3}, Lcom/squareup/shared/catalog/Queries;->groupPlaceholder(I)Ljava/lang/String;

    move-result-object p3

    const-string p4, "%referent_ids%"

    invoke-virtual {p2, p4, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 480
    invoke-interface {p1, p2, v0}, Lcom/squareup/shared/sql/SQLDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Lcom/squareup/shared/sql/SQLCursor;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/squareup/shared/sql/SQLDatabase;ILjava/lang/String;ILjava/lang/String;)V
    .locals 1

    .line 352
    sget-object v0, Lcom/squareup/shared/catalog/ReferencesTable$Query;->WRITE:Lcom/squareup/shared/catalog/ReferencesTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/ReferencesTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 353
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 354
    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 355
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindInt(Ljava/lang/Integer;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 356
    invoke-virtual {p1, p5}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 357
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method
