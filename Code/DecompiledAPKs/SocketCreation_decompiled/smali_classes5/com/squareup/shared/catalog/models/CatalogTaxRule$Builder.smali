.class public final Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
.super Ljava/lang/Object;
.source "CatalogTaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogTaxRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final taxRule:Lcom/squareup/api/items/TaxRule$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX_RULE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 154
    new-instance v0, Lcom/squareup/api/items/TaxRule$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TaxRule$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogTaxRule;)V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 159
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object p1, p1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->tax_rule:Lcom/squareup/api/items/TaxRule;

    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule;->newBuilder()Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    return-void
.end method


# virtual methods
.method public setDiningOptionIds(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;"
        }
    .end annotation

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 190
    new-instance v2, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v2}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    new-instance v1, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    new-instance v2, Lcom/squareup/api/items/ObjectPredicate$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/ObjectPredicate$Builder;-><init>()V

    sget-object v3, Lcom/squareup/api/items/ObjectPredicate$Type;->IN:Lcom/squareup/api/items/ObjectPredicate$Type;

    .line 196
    invoke-virtual {v2, v3}, Lcom/squareup/api/items/ObjectPredicate$Builder;->type(Lcom/squareup/api/items/ObjectPredicate$Type;)Lcom/squareup/api/items/ObjectPredicate$Builder;

    move-result-object v2

    .line 197
    invoke-virtual {v2, v0}, Lcom/squareup/api/items/ObjectPredicate$Builder;->object_id(Ljava/util/List;)Lcom/squareup/api/items/ObjectPredicate$Builder;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lcom/squareup/api/items/ObjectPredicate$Builder;->build()Lcom/squareup/api/items/ObjectPredicate;

    move-result-object v0

    .line 194
    invoke-virtual {v1, v0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate(Lcom/squareup/api/items/ObjectPredicate;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object v0

    .line 193
    invoke-virtual {p1, v0}, Lcom/squareup/api/items/TaxRule$Builder;->condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method

.method public setMaxItemPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    new-instance v1, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    .line 209
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    .line 208
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method

.method public setMaxTotalAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 2

    .line 219
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    new-instance v1, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    .line 220
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    .line 219
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method

.method public setMinItemPrice(Lcom/squareup/protos/common/Money;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 2

    .line 230
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    new-instance v1, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    .line 231
    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/Dineros;->toDineroOrNull(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/dinero/Money;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object p1

    .line 232
    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->build()Lcom/squareup/api/items/TaxRule$Condition;

    move-result-object p1

    .line 230
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->condition(Lcom/squareup/api/items/TaxRule$Condition;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TaxRule$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method

.method public setTaxIds(Ljava/util/List;)Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;"
        }
    .end annotation

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 172
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 173
    new-instance v2, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v2}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_0
    iget-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogTaxRule$Builder;->taxRule:Lcom/squareup/api/items/TaxRule$Builder;

    new-instance v1, Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;-><init>()V

    sget-object v2, Lcom/squareup/api/items/ObjectFilter;->INCLUDE:Lcom/squareup/api/items/ObjectFilter;

    .line 177
    invoke-virtual {v1, v2}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->filter(Lcom/squareup/api/items/ObjectFilter;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    move-result-object v1

    .line 178
    invoke-virtual {v1, v0}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->tax(Ljava/util/List;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->build()Lcom/squareup/api/items/TaxSetConfiguration;

    move-result-object v0

    .line 176
    invoke-virtual {p1, v0}, Lcom/squareup/api/items/TaxRule$Builder;->tax_config(Lcom/squareup/api/items/TaxSetConfiguration;)Lcom/squareup/api/items/TaxRule$Builder;

    return-object p0
.end method
