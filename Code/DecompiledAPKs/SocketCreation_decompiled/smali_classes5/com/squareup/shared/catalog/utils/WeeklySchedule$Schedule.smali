.class public Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;
.super Ljava/lang/Object;
.source "WeeklySchedule.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/WeeklySchedule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Schedule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;",
        ">;"
    }
.end annotation


# instance fields
.field private endsAt:Ljava/util/Calendar;

.field private startsAt:Ljava/util/Calendar;

.field private weekDays:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)V
    .locals 2

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    .line 103
    iget-object v0, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    .line 104
    iget-object p1, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            ")V"
        }
    .end annotation

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    .line 93
    new-instance p1, Ljava/util/TreeSet;

    invoke-direct {p1}, Ljava/util/TreeSet;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    goto :goto_0

    .line 95
    :cond_0
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p1}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    .line 97
    :goto_0
    iput-object p2, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    .line 98
    iput-object p3, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/TreeSet;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)Ljava/util/Calendar;
    .locals 0

    .line 86
    iget-object p0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    return-object p0
.end method

.method private dayOfTheWeekName(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 2

    .line 147
    sget-object v0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$1;->$SwitchMap$com$squareup$shared$ical$rrule$RecurrenceRule$WeekDay:[I

    invoke-virtual {p1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 163
    new-instance p2, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported weekday: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 161
    :pswitch_0
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SATURDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 159
    :pswitch_1
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->FRIDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 157
    :pswitch_2
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->THURSDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 155
    :pswitch_3
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->WEDNESDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 153
    :pswitch_4
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->TUESDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 151
    :pswitch_5
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->MONDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 149
    :pswitch_6
    sget-object p1, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SUNDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p2, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public compareTo(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)I
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    iget-object p1, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {p1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, -0x1

    return p1

    .line 192
    :cond_1
    iget-object v0, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    return p1

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    iget-object v1, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->compareTo(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    iget-object p1, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 86
    check-cast p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->compareTo(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)I

    move-result p1

    return p1
.end method

.method public daysOfTheWeek(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 6

    .line 120
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 121
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_DAILY:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_4

    .line 125
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 126
    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v4

    sget-object v5, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SA:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v4, v5}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 127
    invoke-virtual {v1}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v1

    sget-object v4, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->SU:Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    invoke-virtual {v1, v4}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    .line 133
    sget-object v0, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_WEEKDAYS:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v0}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    const/4 v0, 0x3

    new-array v1, v0, [Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    .line 136
    sget-object v4, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    .line 137
    invoke-static {v3, v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v4

    aput-object v4, v1, v2

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->LIST_TWO:Lcom/squareup/shared/i18n/Key;

    const/4 v4, 0x2

    .line 138
    invoke-static {v4, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->exactly(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_DAYS_OF_WEEK_MANY:Lcom/squareup/shared/i18n/Key;

    .line 139
    invoke-static {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->orMore(ILcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    move-result-object v0

    aput-object v0, v1, v4

    .line 136
    invoke-static {p1, v1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 141
    invoke-virtual {v2}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->dayOfTheWeekName(Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    goto :goto_1

    .line 143
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 203
    instance-of v0, p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;

    invoke-virtual {p0, p1}, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->compareTo(Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getEndsAt()Ljava/util/Calendar;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    return-object v0
.end method

.method public getStartsAt()Ljava/util/Calendar;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    return-object v0
.end method

.method public getWeekDays()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;",
            ">;"
        }
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .line 208
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {v0}, Ljava/util/Calendar;->hashCode()I

    move-result v0

    int-to-long v3, v0

    add-long/2addr v1, v3

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    if-eqz v0, :cond_1

    .line 212
    invoke-virtual {v0}, Ljava/util/Calendar;->hashCode()I

    move-result v0

    int-to-long v3, v0

    add-long/2addr v1, v3

    :cond_1
    const/4 v0, 0x7

    shl-long v0, v1, v0

    .line 215
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->weekDays:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;

    .line 216
    invoke-virtual {v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDayNum;->getWeekDay()Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/ical/rrule/RecurrenceRule$WeekDay;->getDayValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const-wide/16 v4, 0x1

    shl-long v3, v4, v3

    add-long/2addr v0, v3

    goto :goto_0

    :cond_2
    const-wide v2, 0x80000000L

    .line 219
    rem-long/2addr v0, v2

    long-to-int v1, v0

    return v1
.end method

.method public timeRange(Lcom/squareup/shared/i18n/Localizer;)Ljava/lang/String;
    .locals 8

    .line 168
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/TimeUtils;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/i18n/Localizer$DateTimeType;->TIME:Lcom/squareup/shared/i18n/Localizer$DateTimeType;

    invoke-interface {p1, v1, v2}, Lcom/squareup/shared/i18n/Localizer;->dateTime(Ljava/lang/String;Lcom/squareup/shared/i18n/Localizer$DateTimeType;)Ljava/lang/String;

    move-result-object v1

    .line 170
    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    invoke-virtual {v4, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    sub-int/2addr v2, v4

    const-string v4, "end_time"

    const-string v5, "start_time"

    const/4 v6, 0x1

    if-eq v2, v6, :cond_1

    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    .line 171
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v7, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->startsAt:Ljava/util/Calendar;

    invoke-virtual {v7, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v7

    if-ne v2, v7, :cond_0

    iget-object v2, p0, Lcom/squareup/shared/catalog/utils/WeeklySchedule$Schedule;->endsAt:Ljava/util/Calendar;

    .line 172
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v2, v6, :cond_0

    goto :goto_0

    .line 178
    :cond_0
    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_TIME_RANGE:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v2}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 179
    invoke-interface {p1, v5, v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 180
    invoke-interface {p1, v4, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 181
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 173
    :cond_1
    :goto_0
    sget-object v2, Lcom/squareup/shared/catalog/CatalogKeyManifest;->SCHEDULE_TIME_RANGE_NEXT_DAY:Lcom/squareup/shared/i18n/Key;

    invoke-interface {p1, v2}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 174
    invoke-interface {p1, v5, v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 175
    invoke-interface {p1, v4, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object p1

    .line 176
    invoke-interface {p1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
