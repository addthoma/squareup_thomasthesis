.class public Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;
.super Ljava/lang/Object;
.source "CatalogSyncHandler.java"

# interfaces
.implements Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;,
        Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$ResponseHandler;,
        Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;
    }
.end annotation


# instance fields
.field private final endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private final httpThread:Ljava/util/concurrent/Executor;

.field private final mainThread:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 39
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->mainThread:Ljava/util/concurrent/Executor;

    .line 40
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->fileThread:Ljava/util/concurrent/Executor;

    .line 41
    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->httpThread:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$1400(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Lcom/squareup/shared/catalog/CatalogEndpoint;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->endpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->mainThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->fileThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->httpThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method


# virtual methods
.method public send(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    const-string v0, "messages"

    .line 51
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/utils/PreconditionUtils;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 53
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;-><init>(Ljava/lang/Iterable;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 55
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->access$100(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, p1, v3

    const-string v2, "Catalog: Sending sync request batch of %s requests."

    invoke-static {v2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;->httpThread:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;

    invoke-direct {v2, v0, p2, p0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Requester;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V

    invoke-interface {p1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
