.class public final Lcom/squareup/shared/catalog/models/CatalogItem;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItem.java"

# interfaces
.implements Lcom/squareup/shared/catalog/models/SupportsSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/Item;",
        ">;",
        "Lcom/squareup/shared/catalog/models/SupportsSearch;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getAbbreviation()Ljava/lang/String;
    .locals 2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->abbreviation:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAbbreviationOrAbbreviatedName()Ljava/lang/String;
    .locals 2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getAllItemOptionIds()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/Item;

    iget-object v1, v1, Lcom/squareup/api/items/Item;->item_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/ItemOptionForItem;

    .line 110
    iget-object v2, v2, Lcom/squareup/api/items/ItemOptionForItem;->item_option_id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->color:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 56
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getImageId()Ljava/lang/String;
    .locals 2

    .line 74
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 2

    .line 44
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item;->DEFAULT_TYPE:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item$Type;

    return-object v0
.end method

.method public getMenuCategoryId()Ljava/lang/String;
    .locals 2

    .line 99
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasMenuCategory()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 52
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Type;

    .line 127
    sget-object v1, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 116
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasMenuCategory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getMenuCategoryId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getImageId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 2

    .line 131
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasImage()Z
    .locals 2

    .line 68
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    .line 69
    iget-object v1, v0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/Item;->image:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasMenuCategory()Z
    .locals 2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    .line 93
    iget-object v1, v0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/Item;->menu_category:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    const-string v1, ""

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAvailableForPickup()Z
    .locals 2

    .line 60
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->available_for_pickup:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Item;->DEFAULT_AVAILABLE_FOR_PICKUP:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isGiftCard()Z
    .locals 2

    .line 48
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPublic()Z
    .locals 3

    .line 64
    sget-object v0, Lcom/squareup/api/items/Visibility;->PUBLIC:Lcom/squareup/api/items/Visibility;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/Item;

    iget-object v1, v1, Lcom/squareup/api/items/Item;->visibility:Lcom/squareup/api/items/Visibility;

    sget-object v2, Lcom/squareup/api/items/Item;->DEFAULT_VISIBILITY:Lcom/squareup/api/items/Visibility;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Visibility;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public searchKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 135
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->normalizedKeywordsForWordPrefixSearchWithSpecialCharactersIgnored(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public skipsModifierScreen()Z
    .locals 2

    .line 104
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItem;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Item;

    iget-object v0, v0, Lcom/squareup/api/items/Item;->skips_modifier_screen:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/Item;->DEFAULT_SKIPS_MODIFIER_SCREEN:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
