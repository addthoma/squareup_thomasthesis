.class public Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuNameItemNameTable;
.super Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;
.source "ItemVariationSkuNameItemNameTable.java"


# static fields
.field private static final VERSION:J = 0xaL


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable;-><init>()V

    return-void
.end method

.method private processUpdatedItems(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 63
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 64
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->findReferrers(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 66
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuNameItemNameTable;->processUpdatedVariations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private processUpdatedVariations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/sql/SQLDatabase;",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;)V"
        }
    .end annotation

    .line 72
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 73
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuNameItemNameTable;->upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private upsert(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->searchKeywords()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/shared/catalog/utils/StringUtils;->joinWordsWithLeadingWhitespace(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogItem;->searchKeywords()Ljava/util/List;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/shared/catalog/utils/StringUtils;->joinWordsWithLeadingWhitespace(Ljava/util/List;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 90
    sget-object v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->UPSERT_VARIATION:Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuAndNameTable$Query;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->forStatement(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 91
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getSku()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 92
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 93
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 94
    invoke-virtual {p1, p2}, Lcom/squareup/shared/sql/SQLStatementBuilder;->bindString(Ljava/lang/String;)Lcom/squareup/shared/sql/SQLStatementBuilder;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/shared/sql/SQLStatementBuilder;->execute()V

    return-void
.end method


# virtual methods
.method public applyUpdates(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/UpdatedCatalogObjects;ZZ)V
    .locals 1

    .line 49
    iget-object p3, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedItemsById:Ljava/util/Map;

    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p3

    invoke-direct {p0, p1, p3}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuNameItemNameTable;->processUpdatedItems(Lcom/squareup/shared/sql/SQLDatabase;Ljava/util/Collection;)V

    .line 51
    iget-object p2, p2, Lcom/squareup/shared/catalog/UpdatedCatalogObjects;->updatedVariationsByItemId:Ljava/util/Map;

    .line 52
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    .line 51
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map$Entry;

    .line 53
    const-class p4, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, p4, v0}, Lcom/squareup/shared/catalog/SqliteCatalogStore;->readByIdOrNull(Lcom/squareup/shared/sql/SQLDatabase;Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p4

    check-cast p4, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 55
    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Collection;

    invoke-direct {p0, p1, p4, p3}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationSkuNameItemNameTable;->processUpdatedVariations(Lcom/squareup/shared/sql/SQLDatabase;Lcom/squareup/shared/catalog/models/CatalogItem;Ljava/util/Collection;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public tableVersion()J
    .locals 2

    const-wide/16 v0, 0xa

    return-wide v0
.end method
