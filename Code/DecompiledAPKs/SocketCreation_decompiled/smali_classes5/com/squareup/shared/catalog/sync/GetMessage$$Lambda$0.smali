.class final synthetic Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/squareup/shared/catalog/sync/SyncCallback;


# instance fields
.field private final arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

.field private final arg$2:J

.field private final arg$3:Z

.field private final arg$4:Lcom/squareup/shared/catalog/sync/SyncCallback;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/GetMessage;JZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

    iput-wide p2, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$2:J

    iput-boolean p4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$3:Z

    iput-object p5, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$4:Lcom/squareup/shared/catalog/sync/SyncCallback;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$1:Lcom/squareup/shared/catalog/sync/GetMessage;

    iget-wide v1, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$2:J

    iget-boolean v3, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$3:Z

    iget-object v4, p0, Lcom/squareup/shared/catalog/sync/GetMessage$$Lambda$0;->arg$4:Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/shared/catalog/sync/GetMessage;->lambda$onComplete$0$GetMessage(JZLcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
