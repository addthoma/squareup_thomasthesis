.class public Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;
.super Ljava/lang/Object;
.source "InflatedPricingRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/utils/InflatedPricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivePeriod"
.end annotation


# instance fields
.field private durationMs:J

.field private startsAt:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/util/Date;J)V
    .locals 0

    .line 765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 766
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->startsAt:Ljava/util/Date;

    .line 767
    iput-wide p2, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->durationMs:J

    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .line 779
    iget-wide v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->durationMs:J

    return-wide v0
.end method

.method public getStartsAt()Ljava/util/Date;
    .locals 1

    .line 771
    iget-object v0, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->startsAt:Ljava/util/Date;

    return-object v0
.end method

.method public setDuration(J)V
    .locals 0

    .line 783
    iput-wide p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->durationMs:J

    return-void
.end method

.method public setStartsAt(Ljava/util/Date;)V
    .locals 0

    .line 775
    iput-object p1, p0, Lcom/squareup/shared/catalog/utils/InflatedPricingRule$ActivePeriod;->startsAt:Ljava/util/Date;

    return-void
.end method
