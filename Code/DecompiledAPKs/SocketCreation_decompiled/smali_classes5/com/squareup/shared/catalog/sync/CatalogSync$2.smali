.class Lcom/squareup/shared/catalog/sync/CatalogSync$2;
.super Lcom/squareup/shared/catalog/sync/SyncTask;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/shared/catalog/sync/CatalogSync;->releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/sync/SyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

.field final synthetic val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$2;->this$0:Lcom/squareup/shared/catalog/sync/CatalogSync;

    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$2;->val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/SyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 151
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$2;->val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Releasing catalog lock %h"

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync$2;->val$lock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->unlock(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
