.class public interface abstract Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
.super Ljava/lang/Object;
.source "ItemVariationWithOptions.java"

# interfaces
.implements Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;


# virtual methods
.method public abstract addOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getItemId()Ljava/lang/String;
.end method

.method public abstract removeOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
.end method

.method public abstract reorderOption(Ljava/lang/String;II)Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemVariationWithOptions;
.end method
