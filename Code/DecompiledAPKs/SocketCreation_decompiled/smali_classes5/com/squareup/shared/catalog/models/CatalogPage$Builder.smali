.class public final Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
.super Ljava/lang/Object;
.source "CatalogPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final page:Lcom/squareup/api/items/Page$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 68
    new-instance v0, Lcom/squareup/api/items/Page$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Page$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Page$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogPage;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->page(Lcom/squareup/api/items/Page;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 98
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogPage;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogPage;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setLayout(Lcom/squareup/api/items/PageLayout;)Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Page$Builder;->layout(Lcom/squareup/api/items/PageLayout;)Lcom/squareup/api/items/Page$Builder;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Page$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;

    return-object p0
.end method

.method public setPageIndex(I)Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Page$Builder;->page_index(Ljava/lang/Integer;)Lcom/squareup/api/items/Page$Builder;

    return-object p0
.end method

.method public setTagId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogPage$Builder;
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogPage$Builder;->page:Lcom/squareup/api/items/Page$Builder;

    new-instance v1, Lcom/squareup/api/sync/ObjectId$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectId$Builder;-><init>()V

    .line 88
    invoke-virtual {v1, p1}, Lcom/squareup/api/sync/ObjectId$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/api/sync/ObjectType$Builder;

    invoke-direct {v1}, Lcom/squareup/api/sync/ObjectType$Builder;-><init>()V

    sget-object v2, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    .line 90
    invoke-virtual {v1, v2}, Lcom/squareup/api/sync/ObjectType$Builder;->type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectType$Builder;

    move-result-object v1

    .line 91
    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectType$Builder;->build()Lcom/squareup/api/sync/ObjectType;

    move-result-object v1

    .line 89
    invoke-virtual {p1, v1}, Lcom/squareup/api/sync/ObjectId$Builder;->type(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectId$Builder;

    move-result-object p1

    .line 92
    invoke-virtual {p1}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    .line 87
    invoke-virtual {v0, p1}, Lcom/squareup/api/items/Page$Builder;->tag(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Page$Builder;

    return-object p0
.end method
