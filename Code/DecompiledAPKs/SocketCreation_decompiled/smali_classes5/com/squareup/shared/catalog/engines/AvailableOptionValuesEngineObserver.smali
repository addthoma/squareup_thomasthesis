.class public interface abstract Lcom/squareup/shared/catalog/engines/AvailableOptionValuesEngineObserver;
.super Ljava/lang/Object;
.source "AvailableOptionValuesEngineObserver.java"


# virtual methods
.method public abstract onUpdate(Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/engines/ItemOptionValueState;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onVariationSelected(Lcom/squareup/shared/catalog/models/CatalogItemVariation;)V
.end method
