.class public final Lcom/squareup/shared/catalog/models/CatalogRelation;
.super Ljava/lang/Object;
.source "CatalogRelation.java"


# static fields
.field public static final REF_FLOOR_PLAN_TILE_FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_ITEM_FEE_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_ITEM_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_MENU_GROUP_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_MENU_GROUP_MEMBERSHIP_TAG:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_MENU_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_PAGE_MEMBERSHIP_PAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_PAGE_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_RULE_TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_SURCHARGE_FEE_MEMBERSHIP_SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_SURCHARGE_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

.field public static final REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;


# instance fields
.field public final referentType:Lcom/squareup/api/items/Type;

.field public final referrerType:Lcom/squareup/api/items/Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 10
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_OPTION:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    .line 11
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MODIFIER_OPTION_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 13
    sget-object v0, Lcom/squareup/api/items/Type;->TICKET_TEMPLATE:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->TICKET_GROUP:Lcom/squareup/api/items/Type;

    .line 14
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_TICKET_TEMPLATE_TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 16
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    .line 17
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 19
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    .line 20
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 22
    sget-object v0, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->SURCHARGE:Lcom/squareup/api/items/Type;

    .line 23
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_SURCHARGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 25
    sget-object v0, Lcom/squareup/api/items/Type;->SURCHARGE_FEE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->FEE:Lcom/squareup/api/items/Type;

    .line 26
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_SURCHARGE_FEE_MEMBERSHIP_TAX:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 28
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->MENU_CATEGORY:Lcom/squareup/api/items/Type;

    .line 29
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 31
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_IMAGE:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 33
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 35
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    .line 36
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 38
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM_MODIFIER_LIST:Lcom/squareup/api/items/Type;

    .line 39
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_LIST_MEMBERSHIP_LIST:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 41
    sget-object v0, Lcom/squareup/api/items/Type;->MENU:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 43
    sget-object v0, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_GROUP_MEMBERSHIP_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 45
    sget-object v0, Lcom/squareup/api/items/Type;->MENU_GROUP_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_MENU_GROUP_MEMBERSHIP_TAG:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 47
    sget-object v0, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->TAG:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MENU_GROUP:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 49
    sget-object v0, Lcom/squareup/api/items/Type;->FLOOR_PLAN_TILE:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->FLOOR_PLAN:Lcom/squareup/api/items/Type;

    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_FLOOR_PLAN_TILE_FLOOR_PLAN:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 51
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_VARIATION:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    .line 52
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 54
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->PAGE:Lcom/squareup/api/items/Type;

    .line 55
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_PAGE_MEMBERSHIP_PAGE:Lcom/squareup/shared/catalog/models/CatalogRelation;

    .line 57
    sget-object v0, Lcom/squareup/api/items/Type;->PRICING_RULE:Lcom/squareup/api/items/Type;

    sget-object v1, Lcom/squareup/api/items/Type;->TIME_PERIOD:Lcom/squareup/api/items/Type;

    .line 58
    invoke-static {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogRelation;->catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;

    move-result-object v0

    sput-object v0, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_RULE_TIME_PERIOD:Lcom/squareup/shared/catalog/models/CatalogRelation;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    .line 65
    iput-object p2, p0, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    return-void
.end method

.method static catalogRelation(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogRelation;
    .locals 1

    .line 69
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/models/CatalogRelation;-><init>(Lcom/squareup/api/items/Type;Lcom/squareup/api/items/Type;)V

    return-object v0
.end method
