.class public interface abstract Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;
.super Ljava/lang/Object;
.source "CatalogMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Handler"
.end annotation


# virtual methods
.method public abstract send(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage;",
            ">;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation
.end method
