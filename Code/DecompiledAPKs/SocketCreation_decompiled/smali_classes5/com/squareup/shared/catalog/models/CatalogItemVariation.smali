.class public final Lcom/squareup/shared/catalog/models/CatalogItemVariation;
.super Lcom/squareup/shared/catalog/models/CatalogObject;
.source "CatalogItemVariation.java"

# interfaces
.implements Lcom/squareup/shared/catalog/models/SupportsSearch;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/shared/catalog/models/CatalogObject<",
        "Lcom/squareup/api/items/ItemVariation;",
        ">;",
        "Lcom/squareup/shared/catalog/models/SupportsSearch;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/api/sync/ObjectWrapper;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/models/CatalogObject;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-void
.end method


# virtual methods
.method public getAllItemOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation

    .line 140
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    return-object v0
.end method

.method public getAvailableOnOnlineBookingSite()Z
    .locals 2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_AVAILABLE_ON_ONLINE_BOOKING_SITE:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getCatalogMeasurementUnitToken()Ljava/lang/String;
    .locals 2

    .line 53
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .line 67
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_SERVICE_DURATION:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getEmployeeTokens()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->employee_tokens:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getIntermissions()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation

    .line 92
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->intermissions:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getInventoryAlertThreshold()J
    .locals 2

    .line 135
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_threshold:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_INVENTORY_ALERT_THRESHOLD:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 2

    .line 118
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    .line 119
    iget-object v1, v0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getMerchantCatalogObjectToken()Ljava/lang/String;
    .locals 1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v0, v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNoShowFee()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getOrdinal()I
    .locals 2

    .line 63
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 109
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getPriceDescription()Ljava/lang/String;
    .locals 2

    .line 71
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReferentTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Type;",
            ">;"
        }
    .end annotation

    .line 156
    sget-object v0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRelations()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/models/CatalogRelation;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 148
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 149
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 152
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogRelation;->REF_VARIATION_ITEM:Lcom/squareup/shared/catalog/models/CatalogRelation;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getSku()Ljava/lang/String;
    .locals 2

    .line 49
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSortText()Ljava/lang/String;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getOrdinal()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/Queries;->fixedLengthOrdinal(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransitionTime()J
    .locals 2

    .line 75
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_TRANSITION_TIME:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public isInventoryAlertEnabled()Z
    .locals 2

    .line 130
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->inventory_alert_type:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_INVENTORY_ALERT_TYPE:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    .line 131
    sget-object v1, Lcom/squareup/api/items/ItemVariation$InventoryAlertType;->LOW_QUANTITY:Lcom/squareup/api/items/ItemVariation$InventoryAlertType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInventoryTracked()Z
    .locals 2

    .line 124
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/ItemVariation;

    iget-object v0, v0, Lcom/squareup/api/items/ItemVariation;->track_inventory:Ljava/lang/Boolean;

    sget-object v1, Lcom/squareup/api/items/ItemVariation;->DEFAULT_TRACK_INVENTORY:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isVariablePricing()Z
    .locals 3

    .line 96
    sget-object v0, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemVariation;

    iget-object v1, v1, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    sget-object v2, Lcom/squareup/api/items/ItemVariation;->DEFAULT_PRICING_TYPE:Lcom/squareup/api/items/PricingType;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/PricingType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    if-nez v1, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The FIXED_PRICING ItemVariation doesn\'t have price."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public searchKeywords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 160
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/shared/catalog/utils/StringUtils;->normalizedKeywordsForWordPrefixSearchWithSpecialCharactersIgnored(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
