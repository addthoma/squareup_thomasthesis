.class public Lcom/squareup/shared/catalog/sync/CatalogSync;
.super Ljava/lang/Object;
.source "CatalogSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;,
        Lcom/squareup/shared/catalog/sync/CatalogSync$FileThreadProgressNotifier;,
        Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;,
        Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

.field private final catalogStoreIsReady:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

.field private final catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

.field private final catalogUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

.field private final clock:Lcom/squareup/shared/catalog/logging/Clock;

.field private final cogsEndpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

.field private final connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

.field private final fileThread:Ljava/util/concurrent/Executor;

.field private hasInitialCatalogState:Z

.field private final lastKnownHasServerVersion:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mainThread:Ljava/util/concurrent/Executor;

.field private final progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

.field private final syncHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

.field private final syncInProgress:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

.field private final updateSessionId:Lcom/squareup/shared/catalog/sync/SyncCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/CatalogEndpoint;Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/CatalogStoreProvider;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;)V
    .locals 2

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync$UpdateSessionIdCallback;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->updateSessionId:Lcom/squareup/shared/catalog/sync/SyncCallback;

    .line 73
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->lastKnownHasServerVersion:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreIsReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 91
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    .line 115
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->cogsEndpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 116
    iput-object p2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

    .line 117
    iput-object p3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->fileThread:Ljava/util/concurrent/Executor;

    .line 118
    iput-object p4, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->mainThread:Ljava/util/concurrent/Executor;

    .line 119
    iput-object p5, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    .line 120
    iput-object p6, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    .line 121
    iput-object p7, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    .line 122
    iput-object p8, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    .line 123
    iput-object p9, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    .line 124
    iput-object p10, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    .line 125
    new-instance p1, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-direct {p1, p6}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;-><init>(Lcom/squareup/shared/catalog/CatalogStoreProvider;)V

    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    .line 126
    iput-object p11, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->readSyncInfo(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Lcom/squareup/shared/catalog/CatalogStoreProvider;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/Executor;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->mainThread:Ljava/util/concurrent/Executor;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/shared/catalog/sync/CatalogSync;)Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/shared/catalog/sync/CatalogSync;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreIsReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->shouldSyncWithServer(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/shared/catalog/sync/CatalogSync;->doSync(Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;JLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method private doSync(Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;",
            "J",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p4

    .line 473
    iget-object v0, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 475
    iget-object v0, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    invoke-interface {v0, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 477
    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Catalog: SyncInfo already in progress joining callback."

    .line 478
    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 482
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v0, 0x0

    .line 488
    iget-boolean v2, v13, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->hasSession:Z

    const-wide/16 v3, 0x1

    if-eqz v2, :cond_1

    .line 490
    iget-object v2, v13, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->writeRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/shared/catalog/PendingWriteRequest;

    .line 492
    iget-object v6, v5, Lcom/squareup/shared/catalog/PendingWriteRequest;->request:Lcom/squareup/api/rpc/Request;

    invoke-virtual {v6}, Lcom/squareup/api/rpc/Request;->newBuilder()Lcom/squareup/api/rpc/Request$Builder;

    move-result-object v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/api/rpc/Request$Builder;->id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object v6

    add-long/2addr v0, v3

    .line 494
    new-instance v7, Lcom/squareup/shared/catalog/sync/WriteMessage;

    iget-wide v8, v5, Lcom/squareup/shared/catalog/PendingWriteRequest;->id:J

    invoke-direct {v7, v6, v8, v9, v12}, Lcom/squareup/shared/catalog/sync/WriteMessage;-><init>(Lcom/squareup/api/rpc/Request;JLcom/squareup/shared/catalog/sync/CatalogSync;)V

    .line 495
    invoke-interface {v15, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 499
    :cond_1
    new-instance v2, Lcom/squareup/shared/catalog/sync/SessionMessage;

    iget-object v5, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->cogsEndpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    .line 500
    invoke-virtual {v5, v0, v1}, Lcom/squareup/shared/catalog/CatalogEndpoint;->createCreateSessionRequest(J)Lcom/squareup/api/rpc/Request;

    move-result-object v0

    iget-object v1, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->updateSessionId:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-direct {v2, v0, v12, v1}, Lcom/squareup/shared/catalog/sync/SessionMessage;-><init>(Lcom/squareup/api/rpc/Request;Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 503
    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v0, v3

    .line 507
    :cond_2
    new-instance v2, Lcom/squareup/api/sync/GetRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/api/sync/GetRequest$Builder;-><init>()V

    iget-wide v3, v13, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/api/sync/GetRequest$Builder;->applied_server_version(Ljava/lang/Long;)Lcom/squareup/api/sync/GetRequest$Builder;

    move-result-object v2

    .line 508
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/api/sync/GetRequest$Builder;->max_batch_size(Ljava/lang/Long;)Lcom/squareup/api/sync/GetRequest$Builder;

    move-result-object v2

    .line 509
    invoke-virtual {v2}, Lcom/squareup/api/sync/GetRequest$Builder;->build()Lcom/squareup/api/sync/GetRequest;

    move-result-object v2

    .line 510
    iget-object v3, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->cogsEndpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    invoke-virtual {v3, v0, v1, v2}, Lcom/squareup/shared/catalog/CatalogEndpoint;->createGetRequest(JLcom/squareup/api/sync/GetRequest;)Lcom/squareup/api/rpc/Request;

    move-result-object v1

    .line 511
    new-instance v11, Lcom/squareup/shared/catalog/sync/GetMessage;

    iget-wide v3, v13, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->cogsServerVersion:J

    iget-object v5, v13, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    iget-object v6, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    iget-object v7, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->analytics:Lcom/squareup/shared/catalog/logging/CatalogAnalytics;

    iget-object v8, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->clock:Lcom/squareup/shared/catalog/logging/Clock;

    iget-object v9, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->progressNotifier:Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;

    iget-object v10, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    iget-object v2, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogUpdateDispatcher:Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;

    move-object v0, v11

    move-object/from16 v16, v2

    move-object/from16 v2, p0

    move-object v13, v11

    move-object/from16 v11, v16

    invoke-direct/range {v0 .. v11}, Lcom/squareup/shared/catalog/sync/GetMessage;-><init>(Lcom/squareup/api/rpc/Request;Lcom/squareup/shared/catalog/sync/CatalogSync;JLjava/util/List;Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;Lcom/squareup/shared/catalog/logging/CatalogAnalytics;Lcom/squareup/shared/catalog/logging/Clock;Lcom/squareup/shared/catalog/CatalogStore$ProgressNotifier;Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;Lcom/squareup/shared/catalog/CatalogUpdateDispatcher;)V

    .line 523
    invoke-interface {v15, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 527
    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    iget-object v1, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    move-object/from16 v2, p1

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, v12, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$2;

    invoke-direct {v1, v12, v2}, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$2;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)V

    invoke-interface {v0, v15, v1}, Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;->send(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method private executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "TT;>;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 546
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->fileThread:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$3;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private readSyncInfo(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;
    .locals 12

    .line 569
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    const-string v1, "Cannot read sync info from outside of file I/O thread."

    invoke-interface {v0, v1}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceFileThread(Ljava/lang/String;)V

    .line 570
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readCogsAppliedServerVersion()J

    move-result-wide v3

    .line 571
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readLastSyncTimestamp()Ljava/util/Date;

    move-result-object v5

    .line 572
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readSessionId()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 573
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readVersionSyncIncomplete()Z

    move-result v7

    .line 574
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->requiresSyntheticTableRebuild()Z

    move-result v8

    .line 575
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    if-eqz v6, :cond_1

    .line 577
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readPendingWriteRequests()Ljava/util/List;

    move-result-object v0

    :cond_1
    move-object v9, v0

    .line 579
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->readAllSyncedConnectV2ObjectTypes()Ljava/util/List;

    move-result-object v10

    .line 580
    new-instance p1, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;

    const/4 v11, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v11}, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;-><init>(JLjava/util/Date;ZZZLjava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V

    return-object p1
.end method

.method private shouldSyncWithServer(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;)Z
    .locals 4

    .line 434
    iget-object v0, p2, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->lastSyncTimestamp:Ljava/util/Date;

    .line 435
    iget-boolean v1, p2, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->versionSyncIncomplete:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    return v2

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    iget-object v1, v1, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v3, p2, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v1, v3, :cond_5

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->connectV2SyncHandler:Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;

    iget-object v1, v1, Lcom/squareup/shared/catalog/connectv2/sync/ConnectV2SyncHandler;->supportedObjectTypes:Ljava/util/List;

    iget-object p2, p2, Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;->syncedObjectTypes:Ljava/util/List;

    .line 444
    invoke-interface {v1, p2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result p2

    if-nez p2, :cond_1

    goto :goto_0

    .line 448
    :cond_1
    new-instance p2, Ljava/util/Date;

    invoke-direct {p2}, Ljava/util/Date;-><init>()V

    const/4 v1, 0x0

    if-nez v0, :cond_2

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Catalog: No last-sync timestamp. Proceeding to sync."

    .line 451
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    .line 453
    :cond_2
    invoke-virtual {p2, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_3

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Catalog: last-sync timestamp is *after* the current time! Ignoring timestamp and syncing."

    .line 455
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v2

    .line 459
    :cond_3
    invoke-static {v0, p2}, Lcom/squareup/shared/catalog/utils/TimeUtils;->getElapsedTime(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/shared/catalog/utils/ElapsedTime;

    move-result-object p2

    .line 460
    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/utils/ElapsedTime;->isGreaterThan(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Catalog: Aborting sync request; last-sync timestamp within max age."

    .line 461
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return v1

    :cond_4
    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Catalog: Last sync happened too long ago, syncing."

    .line 466
    invoke-static {p2, p1}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    :goto_0
    return v2
.end method


# virtual methods
.method public executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/shared/catalog/sync/SyncTask<",
            "TT;>;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "TT;>;)V"
        }
    .end annotation

    .line 344
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 345
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->isSyncInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    .line 362
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/StorageClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Did not expect Storage to be slated for closing when trying to execute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " with callback "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/StorageClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_1
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$0;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncTask;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeOnFileThread(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public foregroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;JLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "J",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$3;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSync$3;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    new-instance v7, Lcom/squareup/shared/catalog/sync/CatalogSync$4;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    move-wide v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/shared/catalog/sync/CatalogSync$4;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)V

    invoke-virtual {p0, v0, v7}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public getSyncLocal()Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->lastKnownHasServerVersion:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreIsReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSyncInProgress()Z
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 160
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method final synthetic lambda$doSync$3$CatalogSync(Lcom/squareup/shared/catalog/sync/CatalogSync$SyncInfo;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 3

    .line 531
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncInProgress:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_0

    .line 532
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 533
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Catalog: delivering sync result to %s callbacks."

    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 534
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/sync/SyncCallback;

    .line 535
    invoke-interface {v0, p2}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method final synthetic lambda$executeOnFileThread$4$CatalogSync(Ljava/util/concurrent/Callable;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 1

    .line 549
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;
    :try_end_0
    .catch Lcom/squareup/shared/catalog/StorageClosedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    .line 561
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncComplete(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void

    :catchall_0
    move-exception p1

    const-string v0, "Error executing CatalogTask."

    .line 554
    invoke-static {p1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->remoteLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 555
    new-instance v0, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    .line 556
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, v0}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    .line 551
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->mainThread:Ljava/util/concurrent/Executor;

    invoke-static {v0, p2, p1}, Lcom/squareup/shared/catalog/sync/SyncTasks;->syncFailWithException(Ljava/util/concurrent/Executor;Lcom/squareup/shared/catalog/sync/SyncCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method final synthetic lambda$executeSyncTask$0$CatalogSync(Lcom/squareup/shared/catalog/sync/SyncTask;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 371
    iget-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->hasInitialCatalogState:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 372
    iput-boolean v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->hasInitialCatalogState:Z

    .line 373
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->lastKnownHasServerVersion:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->hasAppliedServerVersion()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 374
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreIsReady:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->requiresSyntheticTableRebuild()Z

    move-result v2

    xor-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/sync/SyncTask;->perform(Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object v0

    .line 378
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncTask;->shouldUpdateLastKnownServerVersion()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 379
    iget-object p1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->lastKnownHasServerVersion:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogSyncLocal:Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLocal;->hasAppliedServerVersion()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :cond_1
    return-object v0
.end method

.method final synthetic lambda$requestNewSessionId$1$CatalogSync(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1

    .line 419
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 422
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    return-void
.end method

.method final synthetic lambda$requestNewSessionId$2$CatalogSync(Lcom/squareup/shared/catalog/sync/CatalogMessage;Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 1

    .line 414
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    const-string v0, "Catalog: Requesting new session ID."

    .line 416
    invoke-static {v0, p2}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    new-instance p2, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$4;

    invoke-direct {p2, p0}, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$4;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    .line 424
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->syncHandler:Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogMessage$Handler;->send(Ljava/util/List;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;
    .locals 1

    .line 130
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    invoke-direct {v0}, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;-><init>()V

    .line 131
    invoke-virtual {p0, v0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    return-object v0
.end method

.method public releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->die()V

    .line 149
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$2;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    .line 155
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p1

    .line 149
    invoke-virtual {p0, v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method requestNewSessionId()V
    .locals 5

    .line 392
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->threadsEnforcer:Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;

    invoke-interface {v0}, Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;->enforceMainThread()V

    .line 395
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 399
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$9;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSync$9;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    .line 410
    new-instance v1, Lcom/squareup/shared/catalog/sync/SessionMessage;

    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->cogsEndpoint:Lcom/squareup/shared/catalog/CatalogEndpoint;

    const-wide/16 v3, 0x0

    .line 411
    invoke-virtual {v2, v3, v4}, Lcom/squareup/shared/catalog/CatalogEndpoint;->createCreateSessionRequest(J)Lcom/squareup/api/rpc/Request;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->updateSessionId:Lcom/squareup/shared/catalog/sync/SyncCallback;

    invoke-direct {v1, v2, p0, v3}, Lcom/squareup/shared/catalog/sync/SessionMessage;-><init>(Lcom/squareup/api/rpc/Request;Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 412
    new-instance v2, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$1;

    invoke-direct {v2, p0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync$$Lambda$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogMessage;)V

    .line 426
    invoke-virtual {p0, v0, v2}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public resumeLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "Locking catalog with lock %h"

    .line 136
    invoke-static {v1, v0}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncLock;->assertLive()V

    .line 139
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$1;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    .line 144
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p1

    .line 139
    invoke-virtual {p0, v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
            "Lcom/squareup/shared/catalog/CatalogCallback<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 220
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$5;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSync$5;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSync$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSync$6;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/CatalogCallback;)V

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method public shouldForegroundSync(Lcom/squareup/shared/catalog/utils/ElapsedTime;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 253
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;Lcom/squareup/shared/catalog/utils/ElapsedTime;Lcom/squareup/shared/catalog/sync/CatalogSync$1;)V

    .line 254
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    .line 260
    monitor-enter p0

    .line 261
    :goto_0
    :try_start_0
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->access$900(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 262
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_0

    .line 264
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->access$1000(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Ljava/lang/Throwable;

    move-result-object p1

    if-nez p1, :cond_1

    .line 270
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->access$1100(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Z

    move-result p1

    return p1

    .line 268
    :cond_1
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;->access$1000(Lcom/squareup/shared/catalog/sync/CatalogSync$ShouldForegroundSyncTask;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catchall_0
    move-exception p1

    .line 264
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;Z)V"
        }
    .end annotation

    .line 274
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/sync/CatalogSync;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSync;->catalogStoreProvider:Lcom/squareup/shared/catalog/CatalogStoreProvider;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/CatalogStoreProvider;->isCloseEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 296
    :cond_0
    new-instance v0, Lcom/squareup/shared/catalog/sync/CatalogSync$7;

    invoke-direct {v0, p0}, Lcom/squareup/shared/catalog/sync/CatalogSync$7;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;)V

    new-instance v1, Lcom/squareup/shared/catalog/sync/CatalogSync$8;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/shared/catalog/sync/CatalogSync$8;-><init>(Lcom/squareup/shared/catalog/sync/CatalogSync;ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    invoke-virtual {p0, v0, v1}, Lcom/squareup/shared/catalog/sync/CatalogSync;->executeSyncTask(Lcom/squareup/shared/catalog/sync/SyncTask;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void

    .line 292
    :cond_1
    :goto_0
    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncResults;->empty()Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/shared/catalog/sync/SyncCallback;->call(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
