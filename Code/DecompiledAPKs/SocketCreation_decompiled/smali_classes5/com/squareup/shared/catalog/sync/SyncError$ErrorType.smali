.class public final enum Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
.super Ljava/lang/Enum;
.source "SyncError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/SyncError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum CATALOG_OBJECT_NOT_FOUND:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

.field public static final enum HTTP_TOO_MANY_REQUESTS:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 22
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v1, 0x0

    const-string v2, "CATALOG_CLIENT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 24
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v2, 0x1

    const-string v3, "CATALOG_SERVER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 26
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v3, 0x2

    const-string v4, "CATALOG_OBJECT_NOT_FOUND"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_OBJECT_NOT_FOUND:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 28
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v4, 0x3

    const-string v5, "CATALOG_UNKNOWN"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 30
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v5, 0x4

    const-string v6, "HTTP_CLIENT"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 35
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v6, 0x5

    const-string v7, "HTTP_NETWORK"

    invoke-direct {v0, v7, v6}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 37
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v7, 0x6

    const-string v8, "HTTP_SERVER"

    invoke-direct {v0, v8, v7}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 39
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/4 v8, 0x7

    const-string v9, "HTTP_SESSION_EXPIRED"

    invoke-direct {v0, v9, v8}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 41
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/16 v9, 0x8

    const-string v10, "HTTP_TOO_MANY_REQUESTS"

    invoke-direct {v0, v10, v9}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_TOO_MANY_REQUESTS:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    .line 20
    sget-object v10, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_OBJECT_NOT_FOUND:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_TOO_MANY_REQUESTS:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->$VALUES:[Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromRpcError(Lcom/squareup/api/rpc/Error;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 2

    .line 44
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$1;->$SwitchMap$com$squareup$api$rpc$Error$ServerErrorCode:[I

    iget-object p0, p0, Lcom/squareup/api/rpc/Error;->server_error_code:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    sget-object v1, Lcom/squareup/api/rpc/Error;->DEFAULT_SERVER_ERROR_CODE:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-static {p0, v1}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {p0}, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 52
    sget-object p0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object p0

    .line 50
    :cond_0
    sget-object p0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object p0

    .line 48
    :cond_1
    sget-object p0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object p0

    .line 46
    :cond_2
    sget-object p0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->$VALUES:[Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v0}, [Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    return-object v0
.end method
