.class final Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;
.super Ljava/lang/Object;
.source "CatalogSyncHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/CatalogSyncHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Batch"
.end annotation


# instance fields
.field private final messages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/squareup/shared/catalog/sync/CatalogMessage;",
            ">;)V"
        }
    .end annotation

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    .line 66
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/sync/CatalogMessage;

    .line 67
    iget-object v1, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->getRequestId()Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/squareup/api/rpc/Request;->DEFAULT_ID:Ljava/lang/Long;

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Iterable;Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$1;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;-><init>(Ljava/lang/Iterable;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;)Ljava/util/Map;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;)Lcom/squareup/api/rpc/RequestBatch;
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->buildRequest()Lcom/squareup/api/rpc/RequestBatch;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;IZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->onComplete(IZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method

.method private buildRequest()Lcom/squareup/api/rpc/RequestBatch;
    .locals 4

    .line 88
    new-instance v0, Lcom/squareup/api/rpc/RequestBatch$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/RequestBatch$Builder;-><init>()V

    .line 89
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 90
    iget-object v2, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/sync/CatalogMessage;

    .line 91
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->startRequest()Lcom/squareup/api/rpc/Request;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/api/rpc/RequestBatch$Builder;->request(Ljava/util/List;)Lcom/squareup/api/rpc/RequestBatch$Builder;

    .line 94
    invoke-virtual {v0}, Lcom/squareup/api/rpc/RequestBatch$Builder;->build()Lcom/squareup/api/rpc/RequestBatch;

    move-result-object v0

    return-object v0
.end method

.method private onComplete(IZLcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/sync/CatalogMessage;

    if-eqz p2, :cond_0

    if-nez p1, :cond_0

    const-string v2, "Catalog: Got empty batch response."

    .line 79
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->onError(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_0
    invoke-virtual {v1, p2, p3}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->onComplete(ZLcom/squareup/shared/catalog/sync/SyncCallback;)V

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    .line 84
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p2, p3

    const-string p1, "Catalog: Received response batch with %s responses."

    invoke-static {p1, p2}, Lcom/squareup/shared/catalog/logging/CatalogLogger$Logger;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/rpc/Response;",
            "Lcom/squareup/shared/catalog/sync/SyncCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/CatalogSyncHandler$Batch;->messages:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/api/rpc/Response;->id:Ljava/lang/Long;

    sget-object v2, Lcom/squareup/api/rpc/Response;->DEFAULT_ID:Ljava/lang/Long;

    invoke-static {v1, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/sync/CatalogMessage;

    .line 73
    invoke-virtual {v0, p1, p2}, Lcom/squareup/shared/catalog/sync/CatalogMessage;->onResponse(Lcom/squareup/api/rpc/Response;Lcom/squareup/shared/catalog/sync/SyncCallback;)V

    return-void
.end method
