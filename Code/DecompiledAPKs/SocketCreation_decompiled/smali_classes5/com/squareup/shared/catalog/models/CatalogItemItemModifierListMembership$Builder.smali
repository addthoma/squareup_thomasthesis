.class public final Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;
.super Ljava/lang/Object;
.source "CatalogItemItemModifierListMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;)V
    .locals 1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->getBackingObject()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/sync/ObjectWrapper;->newBuilder()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 111
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership;->newBuilder()Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 105
    new-instance p1, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;-><init>()V

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_ITEM_MODIFIER_LIST_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper()Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 98
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;-><init>()V

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 99
    invoke-virtual {v0, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->build()Lcom/squareup/api/items/ItemItemModifierListMembership;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_item_modifier_list_membership(Lcom/squareup/api/items/ItemItemModifierListMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 134
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setEnabled(Z)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-object p0
.end method

.method public setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-object p0
.end method

.method public setModifierId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemItemModifierListMembership$Builder;->membership:Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_MODIFIER_LIST:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    return-object p0
.end method
