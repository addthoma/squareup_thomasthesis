.class public final Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;
.super Lcom/squareup/shared/catalog/sync/SyncResult;
.source "SyncTasks.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/sync/SyncTasks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SyncFailure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final t:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    const/4 v0, 0x0

    .line 109
    invoke-direct {p0, v0, v0}, Lcom/squareup/shared/catalog/sync/SyncResult;-><init>(Ljava/lang/Object;Lcom/squareup/shared/catalog/sync/SyncError;)V

    .line 110
    iput-object p1, p0, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;->t:Ljava/lang/Throwable;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/shared/catalog/CatalogException;
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/shared/catalog/sync/SyncTasks$SyncFailure;->t:Ljava/lang/Throwable;

    instance-of v1, v0, Lcom/squareup/shared/catalog/CatalogException;

    if-eqz v1, :cond_0

    .line 115
    check-cast v0, Lcom/squareup/shared/catalog/CatalogException;

    throw v0

    .line 117
    :cond_0
    new-instance v1, Lcom/squareup/shared/catalog/CatalogException;

    invoke-direct {v1, v0}, Lcom/squareup/shared/catalog/CatalogException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
