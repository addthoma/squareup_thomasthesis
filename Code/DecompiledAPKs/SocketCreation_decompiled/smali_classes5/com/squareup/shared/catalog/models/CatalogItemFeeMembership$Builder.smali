.class public final Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;
.super Ljava/lang/Object;
.source "CatalogItemFeeMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final membership:Lcom/squareup/api/items/ItemFeeMembership$Builder;

.field private final wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v0, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_FEE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->createWrapper(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 72
    new-instance p1, Lcom/squareup/api/items/ItemFeeMembership$Builder;

    invoke-direct {p1}, Lcom/squareup/api/items/ItemFeeMembership$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectWrapper$Builder;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/ItemFeeMembership$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemFeeMembership$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->membership:Lcom/squareup/api/items/ItemFeeMembership$Builder;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->membership:Lcom/squareup/api/items/ItemFeeMembership$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemFeeMembership$Builder;->build()Lcom/squareup/api/items/ItemFeeMembership;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->item_fee_membership(Lcom/squareup/api/items/ItemFeeMembership;)Lcom/squareup/api/sync/ObjectWrapper$Builder;

    .line 87
    new-instance v0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    iget-object v1, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->wrapper:Lcom/squareup/api/sync/ObjectWrapper$Builder;

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectWrapper$Builder;->build()Lcom/squareup/api/sync/ObjectWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;-><init>(Lcom/squareup/api/sync/ObjectWrapper;)V

    return-object v0
.end method

.method public setFeeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->membership:Lcom/squareup/api/items/ItemFeeMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TAX:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemFeeMembership$Builder;->fee(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemFeeMembership$Builder;

    return-object p0
.end method

.method public setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->membership:Lcom/squareup/api/items/ItemFeeMembership$Builder;

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemFeeMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemFeeMembership$Builder;

    return-object p0
.end method
