.class public final Lcom/squareup/shared/catalog/ManyToMany$Lookup;
.super Ljava/lang/Object;
.source "ManyToMany.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/ManyToMany;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lookup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        "J:",
        "Lcom/squareup/shared/catalog/models/CatalogObject;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final fromId:Ljava/lang/String;

.field public final joinType:Lcom/squareup/api/items/Type;

.field public final referentType:Lcom/squareup/api/items/Type;

.field public final referrerType:Lcom/squareup/api/items/Type;


# direct methods
.method private constructor <init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogRelation;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p2, p0, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->fromId:Ljava/lang/String;

    .line 67
    iget-object p2, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referrerType:Lcom/squareup/api/items/Type;

    iput-object p2, p0, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->joinType:Lcom/squareup/api/items/Type;

    .line 68
    iget-object p1, p1, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    iput-object p1, p0, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referrerType:Lcom/squareup/api/items/Type;

    .line 69
    iget-object p1, p3, Lcom/squareup/shared/catalog/models/CatalogRelation;->referentType:Lcom/squareup/api/items/Type;

    iput-object p1, p0, Lcom/squareup/shared/catalog/ManyToMany$Lookup;->referentType:Lcom/squareup/api/items/Type;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogRelation;Lcom/squareup/shared/catalog/ManyToMany$1;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/shared/catalog/ManyToMany$Lookup;-><init>(Lcom/squareup/shared/catalog/models/CatalogRelation;Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogRelation;)V

    return-void
.end method
