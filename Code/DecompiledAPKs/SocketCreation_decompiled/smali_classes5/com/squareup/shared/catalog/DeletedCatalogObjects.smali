.class public Lcom/squareup/shared/catalog/DeletedCatalogObjects;
.super Ljava/lang/Object;
.source "DeletedCatalogObjects.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;
    }
.end annotation


# instance fields
.field public deletedCategoryIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedDiscountIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedModifierListIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedModifierOptionIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedPricingRuleIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedTicketGroupIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedTicketTemplateIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedTimePeriodIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedVariationItemIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public deletedVariations:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public final isLocalEdit:Z

.field public updatedTicketTemplateByGroupIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;)V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    .line 25
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedDiscountIds:Ljava/util/Set;

    .line 26
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedCategoryIds:Ljava/util/Set;

    .line 28
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    .line 29
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariations:Ljava/util/Set;

    .line 30
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierListIds:Ljava/util/Set;

    .line 31
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierOptionIds:Ljava/util/Set;

    .line 32
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedPricingRuleIds:Ljava/util/Set;

    .line 33
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketGroupIds:Ljava/util/Set;

    .line 34
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketTemplateIds:Ljava/util/Set;

    .line 37
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTimePeriodIds:Ljava/util/Set;

    .line 42
    iget-boolean v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->isLocalEdit:Z

    iput-boolean v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->isLocalEdit:Z

    .line 43
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedItemIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedItemIds:Ljava/util/Set;

    .line 44
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedDiscountIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedDiscountIds:Ljava/util/Set;

    .line 45
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedCategoryIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedCategoryIds:Ljava/util/Set;

    .line 46
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariationItemIds:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariationItemIds:Ljava/util/Map;

    .line 47
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedVariations:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedVariations:Ljava/util/Set;

    .line 48
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedModifierListIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierListIds:Ljava/util/Set;

    .line 49
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedModifierOptionIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedModifierOptionIds:Ljava/util/Set;

    .line 50
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedPricingRuleIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedPricingRuleIds:Ljava/util/Set;

    .line 51
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTicketGroupIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketGroupIds:Ljava/util/Set;

    .line 52
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTicketTemplateIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTicketTemplateIds:Ljava/util/Set;

    .line 53
    iget-object v0, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->updatedTicketTemplateByGroupIds:Ljava/util/Map;

    .line 54
    iget-object p1, p1, Lcom/squareup/shared/catalog/DeletedCatalogObjects$Builder;->deletedTimePeriodIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/catalog/DeletedCatalogObjects;->deletedTimePeriodIds:Ljava/util/Set;

    return-void
.end method
