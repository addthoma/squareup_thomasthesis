.class public Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;
.super Ljava/lang/Object;
.source "CatalogObjectType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/catalog/models/CatalogObjectType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Adapter"
.end annotation


# static fields
.field private static INSTANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;


# instance fields
.field private final typeByObjectClass:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private final typeByProtoType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/api/items/Type;",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 7

    .line 512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 515
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 516
    invoke-static {}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->values()[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v2, v4

    .line 517
    iget-object v6, v5, Lcom/squareup/shared/catalog/models/CatalogObjectType;->catalogObjectClass:Ljava/lang/Class;

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    iget-object v6, v5, Lcom/squareup/shared/catalog/models/CatalogObjectType;->objectType:Lcom/squareup/api/items/Type;

    invoke-interface {v1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 520
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeByObjectClass:Ljava/util/Map;

    .line 521
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeByProtoType:Ljava/util/Map;

    return-void
.end method

.method private static getTypeOrNull(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 574
    iget-object v0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, v0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-eqz v0, :cond_0

    .line 575
    iget-object p0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object p0, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object p0, p0, Lcom/squareup/api/sync/ObjectType;->type:Lcom/squareup/api/items/Type;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    .line 577
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Type;->ITEM:Lcom/squareup/api/items/Type;

    .line 578
    :goto_1
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p0

    return-object p0
.end method

.method private static declared-synchronized instance()Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;
    .locals 2

    const-class v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    monitor-enter v0

    .line 527
    :try_start_0
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->INSTANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    if-nez v1, :cond_0

    .line 528
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;-><init>()V

    sput-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->INSTANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    .line 530
    :cond_0
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->INSTANCE:Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static isKnown(Lcom/squareup/api/sync/ObjectWrapper;)Z
    .locals 0

    .line 534
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->getTypeOrNull(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static objectFromByteArray([B)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>([B)TT;"
        }
    .end annotation

    .line 540
    :try_start_0
    sget-object v0, Lcom/squareup/api/sync/ObjectWrapper;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/ObjectWrapper;

    .line 541
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 543
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static objectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>(",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ")TT;"
        }
    .end annotation

    .line 548
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p0

    return-object p0
.end method

.method public static typeFromObject(Ljava/lang/Class;)Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/shared/catalog/models/CatalogObject;",
            ">;)",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;"
        }
    .end annotation

    .line 553
    invoke-static {}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->instance()Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeByObjectClass:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object p0
.end method

.method public static typeFromProtoType(Lcom/squareup/api/items/Type;)Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 557
    invoke-static {}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->instance()Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->typeByProtoType:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object p0
.end method

.method public static typeFromWrapper(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 3

    .line 561
    invoke-static {p0}, Lcom/squareup/shared/catalog/models/CatalogObjectType$Adapter;->getTypeOrNull(Lcom/squareup/api/sync/ObjectWrapper;)Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 564
    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v1, v1, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    if-eqz v1, :cond_0

    .line 565
    iget-object p0, p0, Lcom/squareup/api/sync/ObjectWrapper;->object_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v0, p0, Lcom/squareup/api/sync/ObjectId;->type:Lcom/squareup/api/sync/ObjectType;

    .line 567
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown object type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    return-object v0
.end method
