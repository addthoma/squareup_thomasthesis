.class final enum Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;
.super Ljava/lang/Enum;
.source "MetricsInProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/shared/pricing/engine/search/MetricsInProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ClockState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum LOADER_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum LOADER_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum NOT_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum SEARCH_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum SEARCH_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum TOTAL_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

.field public static final enum TOTAL_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 35
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v1, 0x0

    const-string v2, "NOT_STARTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->NOT_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 36
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v2, 0x1

    const-string v3, "TOTAL_STARTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 37
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v3, 0x2

    const-string v4, "LOADER_STARTED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 38
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v4, 0x3

    const-string v5, "LOADER_FINISHED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 39
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v5, 0x4

    const-string v6, "SEARCH_STARTED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 40
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v6, 0x5

    const-string v7, "SEARCH_FINISHED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 41
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v7, 0x6

    const-string v8, "TOTAL_FINISHED"

    invoke-direct {v0, v8, v7}, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    .line 34
    sget-object v8, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->NOT_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->LOADER_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_STARTED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->SEARCH_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->TOTAL_FINISHED:Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->$VALUES:[Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->$VALUES:[Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    invoke-virtual {v0}, [Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/shared/pricing/engine/search/MetricsInProgress$ClockState;

    return-object v0
.end method
