.class public interface abstract Lcom/squareup/shared/pricing/engine/catalog/models/ProductSetFacade;
.super Ljava/lang/Object;
.source "ProductSetFacade.java"


# virtual methods
.method public abstract getEffectiveMax()Ljava/math/BigDecimal;
.end method

.method public abstract getEffectiveMin()Ljava/math/BigDecimal;
.end method

.method public abstract getFlagAllProducts()Z
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getProducts()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getProductsAll()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getProductsAny()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/catalog/models/ObjectIdFacade;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getQuantity()Ljava/math/BigDecimal;
.end method

.method public abstract getQuantityExact()Ljava/math/BigDecimal;
.end method

.method public abstract getQuantityMin()Ljava/math/BigDecimal;
.end method

.method public abstract hasProductsAll()Z
.end method

.method public abstract hasProductsAny()Z
.end method

.method public abstract isExact()Z
.end method

.method public abstract isGreedy()Z
.end method

.method public abstract isMin()Z
.end method
