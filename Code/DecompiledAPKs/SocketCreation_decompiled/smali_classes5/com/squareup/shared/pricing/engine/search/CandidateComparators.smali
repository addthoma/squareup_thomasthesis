.class public Lcom/squareup/shared/pricing/engine/search/CandidateComparators;
.super Ljava/lang/Object;
.source "CandidateComparators.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static BY_EXISTING_AUTO_DISCOUNT(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 3

    .line 67
    invoke-interface {p1, p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p2, p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 68
    :goto_0
    invoke-interface {p1, p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-interface {p2, p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->alreadyHasRule(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 67
    :goto_1
    invoke-static {v0, v1}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->COMPARE_HELPER(ZZ)I

    move-result p0

    return p0
.end method

.method private static BY_PRE_MATCHED(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 3

    .line 28
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getPreMatched()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getPreMatched()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 29
    :goto_0
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getPreMatched()Z

    move-result p0

    if-nez p0, :cond_1

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getPreMatched()Z

    move-result p0

    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 28
    :goto_1
    invoke-static {v0, v1}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->COMPARE_HELPER(ZZ)I

    move-result p0

    return p0
.end method

.method private static BY_TIEBREAKERS(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 0

    .line 76
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getTiebreakers()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getTiebreakers()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private static COMPARE_HELPER(ZZ)I
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, -0x1

    return p0

    :cond_0
    if-eqz p1, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static HIGHER_UNIT_VALUE(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 7

    .line 37
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v0

    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    cmp-long v6, v0, v2

    if-lez v6, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide v1

    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getUnitValue()J

    move-result-wide p0

    cmp-long v3, v1, p0

    if-lez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 37
    :goto_1
    invoke-static {v0, v4}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->COMPARE_HELPER(ZZ)I

    move-result p0

    return p0
.end method

.method private static UNIT_PRICE_COMPARATOR(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)I
    .locals 5

    .line 48
    sget-object v0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$1;->$SwitchMap$com$squareup$shared$pricing$engine$search$Provider$Mode:[I

    invoke-virtual {p2}, Lcom/squareup/shared/pricing/engine/search/Provider$Mode;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eq p2, v1, :cond_3

    const/4 v2, 0x2

    if-eq p2, v2, :cond_0

    return v0

    .line 54
    :cond_0
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMaxUnitPrice()J

    move-result-wide v2

    .line 55
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMaxUnitPrice()J

    move-result-wide p0

    cmp-long p2, v2, p0

    if-gez p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    cmp-long v4, p0, v2

    if-gez v4, :cond_2

    const/4 v0, 0x1

    .line 56
    :cond_2
    invoke-static {p2, v0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->COMPARE_HELPER(ZZ)I

    move-result p0

    return p0

    .line 50
    :cond_3
    invoke-interface {p0}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMinUnitPrice()J

    move-result-wide v2

    .line 51
    invoke-interface {p1}, Lcom/squareup/shared/pricing/engine/search/Candidate;->getMinUnitPrice()J

    move-result-wide p0

    cmp-long p2, v2, p0

    if-lez p2, :cond_4

    const/4 p0, 0x1

    goto :goto_1

    :cond_4
    const/4 p0, 0x0

    :goto_1
    if-gez p2, :cond_5

    const/4 v0, 0x1

    .line 52
    :cond_5
    invoke-static {p0, v0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->COMPARE_HELPER(ZZ)I

    move-result p0

    return p0
.end method

.method public static exhaustComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;

    invoke-direct {v0, p1, p0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$1;-><init>(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)V

    return-object v0
.end method

.method static final synthetic lambda$exhaustComparator$1$CandidateComparators(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 1

    .line 133
    invoke-static {p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_PRE_MATCHED(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 138
    :cond_0
    invoke-static {p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->HIGHER_UNIT_VALUE(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 143
    :cond_1
    invoke-static {p2, p3, p0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->UNIT_PRICE_COMPARATOR(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)I

    move-result p0

    if-eqz p0, :cond_2

    return p0

    .line 149
    :cond_2
    invoke-static {p1, p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_EXISTING_AUTO_DISCOUNT(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result p0

    if-eqz p0, :cond_3

    return p0

    .line 155
    :cond_3
    invoke-static {p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_TIEBREAKERS(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result p0

    neg-int p0, p0

    if-eqz p0, :cond_4

    return p0

    :cond_4
    const/4 p0, 0x0

    return p0
.end method

.method static final synthetic lambda$satisfyComparator$0$CandidateComparators(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I
    .locals 1

    .line 93
    invoke-static {p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_PRE_MATCHED(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 98
    :cond_0
    invoke-static {p2, p3, p0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->UNIT_PRICE_COMPARATOR(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)I

    move-result p0

    if-eqz p0, :cond_1

    return p0

    .line 104
    :cond_1
    invoke-static {p1, p3, p2}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_EXISTING_AUTO_DISCOUNT(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result p0

    if-eqz p0, :cond_2

    return p0

    .line 109
    :cond_2
    invoke-static {p2, p3}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators;->BY_TIEBREAKERS(Lcom/squareup/shared/pricing/engine/search/Candidate;Lcom/squareup/shared/pricing/engine/search/Candidate;)I

    move-result p0

    if-eqz p0, :cond_3

    return p0

    :cond_3
    const/4 p0, 0x0

    return p0
.end method

.method public static satisfyComparator(Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;Lcom/squareup/shared/pricing/engine/search/Provider$Mode;)Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;",
            "Lcom/squareup/shared/pricing/engine/search/Provider$Mode;",
            ")",
            "Ljava/util/Comparator<",
            "Lcom/squareup/shared/pricing/engine/search/Candidate;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$0;

    invoke-direct {v0, p1, p0}, Lcom/squareup/shared/pricing/engine/search/CandidateComparators$$Lambda$0;-><init>(Lcom/squareup/shared/pricing/engine/search/Provider$Mode;Lcom/squareup/shared/pricing/engine/catalog/models/PricingRuleFacade;)V

    return-object v0
.end method
