.class public Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;
.super Ljava/lang/Object;
.source "ApplicationWholeBlock.java"


# instance fields
.field private final discountId:Ljava/lang/String;

.field private final exclusions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            ">;"
        }
    .end annotation
.end field

.field private final ruleId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->ruleId:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->discountId:Ljava/lang/String;

    .line 22
    new-instance p1, Ljava/util/TreeSet;

    invoke-direct {p1, p3}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->exclusions:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public getDiscountId()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->discountId:Ljava/lang/String;

    return-object v0
.end method

.method public getExclusions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->exclusions:Ljava/util/Set;

    return-object v0
.end method

.method public getRuleId()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->ruleId:Ljava/lang/String;

    return-object v0
.end method
