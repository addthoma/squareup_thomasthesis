.class public interface abstract Lcom/squareup/shared/sql/SQLCursor;
.super Ljava/lang/Object;
.source "SQLCursor.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getBlob(I)[B
.end method

.method public abstract getColumnIndex(Ljava/lang/String;)I
.end method

.method public abstract getCount()I
.end method

.method public abstract getInt(I)I
.end method

.method public abstract getLong(I)J
.end method

.method public abstract getPosition()I
.end method

.method public abstract getString(I)Ljava/lang/String;
.end method

.method public abstract isClosed()Z
.end method

.method public abstract isNull(I)Z
.end method

.method public varargs abstract mergeWithCursors([Lcom/squareup/shared/sql/SQLCursor;)Lcom/squareup/shared/sql/SQLCursor;
.end method

.method public abstract moveToFirst()Z
.end method

.method public abstract moveToNext()Z
.end method

.method public abstract moveToPosition(I)Z
.end method
