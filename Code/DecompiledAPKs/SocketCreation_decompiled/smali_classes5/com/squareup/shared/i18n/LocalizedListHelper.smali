.class public Lcom/squareup/shared/i18n/LocalizedListHelper;
.super Ljava/lang/Object;
.source "LocalizedListHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;
    }
.end annotation


# static fields
.field public static final AS_IS:Lcom/squareup/shared/i18n/Key;


# instance fields
.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final keys:[Lcom/squareup/shared/i18n/Key;

.field private final localizer:Lcom/squareup/shared/i18n/Localizer;

.field private numOfListItems:I

.field private final orMoreThreshold:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 44
    new-instance v0, Lcom/squareup/shared/i18n/Key;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "localized_list_helper_as_is"

    const-string v3, "LocalizedListHelperAsIs"

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/i18n/Key;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    return-void
.end method

.method private varargs constructor <init>(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)V
    .locals 7

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    .line 53
    iput-object p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->localizer:Lcom/squareup/shared/i18n/Localizer;

    .line 54
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/squareup/shared/i18n/LocalizedListHelper$1;

    invoke-direct {v0, p0}, Lcom/squareup/shared/i18n/LocalizedListHelper$1;-><init>(Lcom/squareup/shared/i18n/LocalizedListHelper;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;

    .line 59
    invoke-static {p1}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lcom/squareup/shared/i18n/Key;

    iput-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    .line 61
    array-length v0, p2

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    :goto_0
    if-ge v2, v0, :cond_3

    aget-object v4, p2, v2

    .line 62
    invoke-static {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$100(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-ne v3, v1, :cond_1

    .line 66
    invoke-static {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result v3

    invoke-static {p1}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result v5

    if-lt v3, v5, :cond_0

    .line 70
    invoke-static {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result v3

    goto :goto_1

    .line 67
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The quantity specified in orMore() must be the highest quantity in the list."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 64
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "orMore() must be used only once per list."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 72
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    invoke-static {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$000(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)I

    move-result v6

    invoke-static {v4}, Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;->access$200(Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/Key;

    move-result-object v4

    aput-object v4, v5, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-ne v3, v1, :cond_4

    const/4 p1, 0x0

    goto :goto_2

    .line 74
    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->orMoreThreshold:Ljava/lang/Integer;

    return-void
.end method

.method private hydrateTemplate(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/shared/i18n/Key;->tokens()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 121
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->localizer:Lcom/squareup/shared/i18n/Localizer;

    invoke-interface {v0, p1}, Lcom/squareup/shared/i18n/Localizer;->string(Lcom/squareup/shared/i18n/Key;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move-result-object v0

    .line 122
    invoke-virtual {p1}, Lcom/squareup/shared/i18n/Key;->tokens()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    .line 124
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 125
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;

    move v1, v4

    goto :goto_0

    .line 127
    :cond_0
    invoke-interface {v0}, Lcom/squareup/shared/i18n/LocalizedStringBuilder;->format()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 112
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The template with key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {p1}, Lcom/squareup/shared/i18n/Key;->androidKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " expects "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    invoke-virtual {p1}, Lcom/squareup/shared/i18n/Key;->tokens()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " tokens, but "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    .line 117
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " tokens were found. "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static varargs list(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)Lcom/squareup/shared/i18n/LocalizedListHelper;
    .locals 1

    .line 78
    new-instance v0, Lcom/squareup/shared/i18n/LocalizedListHelper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;-><init>(Lcom/squareup/shared/i18n/Localizer;[Lcom/squareup/shared/i18n/LocalizedListHelper$Rule;)V

    return-object v0
.end method


# virtual methods
.method public format()Ljava/lang/String;
    .locals 2

    .line 96
    iget v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    aget-object v0, v0, v1

    sget-object v1, Lcom/squareup/shared/i18n/LocalizedListHelper;->AS_IS:Lcom/squareup/shared/i18n/Key;

    if-ne v0, v1, :cond_0

    .line 97
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->orMoreThreshold:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 101
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    iget v1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    iget-object v1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->orMoreThreshold:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->hydrateTemplate(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    array-length v1, v0

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    if-nez v1, :cond_2

    aget-object v0, v0, v1

    if-nez v0, :cond_2

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->keys:[Lcom/squareup/shared/i18n/Key;

    iget v1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/shared/i18n/LocalizedListHelper;->hydrateTemplate(Lcom/squareup/shared/i18n/Key;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    :goto_0
    const-string v0, ""

    return-object v0
.end method

.method public put(I)Lcom/squareup/shared/i18n/LocalizedListHelper;
    .locals 0

    .line 92
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/shared/i18n/LocalizedListHelper;->put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedListHelper;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/squareup/shared/i18n/LocalizedListHelper;->numOfListItems:I

    return-object p0
.end method
