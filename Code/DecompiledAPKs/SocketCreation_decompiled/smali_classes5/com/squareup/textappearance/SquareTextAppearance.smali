.class public final Lcom/squareup/textappearance/SquareTextAppearance;
.super Ljava/lang/Object;
.source "SquareTextAppearance.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/textappearance/SquareTextAppearance$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 ,2\u00020\u0001:\u0001,BI\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000eJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u000bH\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003JS\u0010\"\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00072\u0008\u0008\u0002\u0010\r\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010#\u001a\u00020\u000b2\u0008\u0010$\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0010\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(J\t\u0010)\u001a\u00020\u0005H\u00d6\u0001J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u000c\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/textappearance/SquareTextAppearance;",
        "",
        "fontSelection",
        "Lcom/squareup/textappearance/FontSelection;",
        "textStyle",
        "",
        "textSize",
        "",
        "textColor",
        "Landroid/content/res/ColorStateList;",
        "textAllCaps",
        "",
        "letterSpacing",
        "legacyFontId",
        "(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)V",
        "getFontSelection",
        "()Lcom/squareup/textappearance/FontSelection;",
        "getLegacyFontId",
        "()I",
        "getLetterSpacing",
        "()F",
        "getTextAllCaps",
        "()Z",
        "getTextColor",
        "()Landroid/content/res/ColorStateList;",
        "getTextSize",
        "getTextStyle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "getTypeface",
        "Landroid/graphics/Typeface;",
        "context",
        "Landroid/content/Context;",
        "hashCode",
        "toString",
        "",
        "Companion",
        "textappearance_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;


# instance fields
.field private final fontSelection:Lcom/squareup/textappearance/FontSelection;

.field private final legacyFontId:I

.field private final letterSpacing:F

.field private final textAllCaps:Z

.field private final textColor:Landroid/content/res/ColorStateList;

.field private final textSize:F

.field private final textStyle:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/textappearance/SquareTextAppearance$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/textappearance/SquareTextAppearance;->Companion:Lcom/squareup/textappearance/SquareTextAppearance$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    iput p2, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    iput p3, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    iput-object p4, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    iput-boolean p5, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    iput p6, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    iput p7, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p8, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 30
    check-cast v0, Landroid/content/res/ColorStateList;

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 v0, p8, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v6, p5

    :goto_1
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const/4 v7, 0x0

    goto :goto_2

    :cond_2
    move v7, p6

    :goto_2
    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p7

    :goto_3
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    .line 33
    invoke-direct/range {v1 .. v8}, Lcom/squareup/textappearance/SquareTextAppearance;-><init>(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/textappearance/SquareTextAppearance;Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFIILjava/lang/Object;)Lcom/squareup/textappearance/SquareTextAppearance;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget p2, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    :cond_1
    move p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget p6, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget p7, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move p4, p9

    move p5, v0

    move-object p6, v1

    move p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/textappearance/SquareTextAppearance;->copy(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)Lcom/squareup/textappearance/SquareTextAppearance;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/textappearance/FontSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    return v0
.end method

.method public final component4()Landroid/content/res/ColorStateList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    return v0
.end method

.method public final component6()F
    .locals 1

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    return v0
.end method

.method public final copy(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)Lcom/squareup/textappearance/SquareTextAppearance;
    .locals 9

    new-instance v8, Lcom/squareup/textappearance/SquareTextAppearance;

    move-object v0, v8

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/textappearance/SquareTextAppearance;-><init>(Lcom/squareup/textappearance/FontSelection;IFLandroid/content/res/ColorStateList;ZFI)V

    return-object v8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/textappearance/SquareTextAppearance;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/textappearance/SquareTextAppearance;

    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    iget-object v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    iget v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    iget v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    iget-object v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    iget-boolean v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    iget v1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    iget p1, p1, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFontSelection()Lcom/squareup/textappearance/FontSelection;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    return-object v0
.end method

.method public final getLegacyFontId()I
    .locals 1

    .line 33
    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    return v0
.end method

.method public final getLetterSpacing()F
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    return v0
.end method

.method public final getTextAllCaps()Z
    .locals 1

    .line 31
    iget-boolean v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    return v0
.end method

.method public final getTextColor()Landroid/content/res/ColorStateList;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public final getTextSize()F
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    return v0
.end method

.method public final getTextStyle()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    return v0
.end method

.method public final getTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    invoke-virtual {v0, p1, v1}, Lcom/squareup/textappearance/FontSelection;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    iget v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    if-eqz v0, :cond_1

    invoke-static {p1, v0}, Lcom/squareup/fonts/FontsCompatKt;->getFont(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SquareTextAppearance(fontSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->fontSelection:Lcom/squareup/textappearance/FontSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", textStyle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textStyle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", textSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textSize:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", textColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", textAllCaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->textAllCaps:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", letterSpacing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->letterSpacing:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", legacyFontId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/textappearance/SquareTextAppearance;->legacyFontId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
