.class public Lcom/squareup/swipe/RecorderErrorReporterListener;
.super Ljava/lang/Object;
.source "RecorderErrorReporterListener.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final headset:Lcom/squareup/wavpool/swipe/Headset;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method constructor <init>(Landroid/telephony/TelephonyManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/Headset;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 24
    iput-object p2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->analytics:Lcom/squareup/analytics/Analytics;

    .line 25
    iput-object p3, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->headset:Lcom/squareup/wavpool/swipe/Headset;

    return-void
.end method

.method public static synthetic lambda$VaNOlI0MSr0xH-NijBNaXoUxvDU(Lcom/squareup/swipe/RecorderErrorReporterListener;Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/swipe/RecorderErrorReporterListener;->onRecordingEvent(Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;)V

    return-void
.end method

.method public static synthetic lambda$boWocuSGIIpkctmapb091IqDgUc(Lcom/squareup/swipe/RecorderErrorReporterListener;Lcom/squareup/wavpool/swipe/Recorder$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/swipe/RecorderErrorReporterListener;->onSampleRateUnset(Lcom/squareup/wavpool/swipe/Recorder$State;)V

    return-void
.end method

.method private onRecordingEvent(Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;)V
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;->eventId:Ljava/lang/String;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "device"

    aput-object v3, v1, v2

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    const-string v3, "model"

    aput-object v3, v1, v2

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x4

    const-string v3, "product"

    aput-object v3, v1, v2

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const/4 v2, 0x6

    const-string v3, "sdk"

    aput-object v3, v1, v2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 37
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v1, v3

    const/16 v2, 0x8

    const-string v3, "release"

    aput-object v3, v1, v2

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const/16 v3, 0x9

    aput-object v2, v1, v3

    .line 36
    invoke-interface {v0, p1, v1}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private onSampleRateUnset(Lcom/squareup/wavpool/swipe/Recorder$State;)V
    .locals 3

    .line 42
    sget-object v0, Lcom/squareup/wavpool/swipe/Recorder$State;->SAMPLE_RATE_UNSET:Lcom/squareup/wavpool/swipe/Recorder$State;

    if-eq p1, v0, :cond_0

    return-void

    .line 45
    :cond_0
    iget-object p1, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->analytics:Lcom/squareup/analytics/Analytics;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "headsetConnected"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->headset:Lcom/squareup/wavpool/swipe/Headset;

    .line 46
    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/Headset;->currentState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object v2

    iget-boolean v2, v2, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->connected:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "headsetHasMic"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->headset:Lcom/squareup/wavpool/swipe/Headset;

    .line 47
    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/Headset;->currentState()Lcom/squareup/wavpool/swipe/HeadsetConnectionState;

    move-result-object v2

    iget-boolean v2, v2, Lcom/squareup/wavpool/swipe/HeadsetConnectionState;->hasMicInput:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "callState"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/squareup/swipe/RecorderErrorReporterListener;->telephonyManager:Landroid/telephony/TelephonyManager;

    .line 48
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "model"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "release"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v2, v0, v1

    const-string v1, "SampleRateUnset"

    .line 45
    invoke-interface {p1, v1, v0}, Lcom/squareup/analytics/Analytics;->log(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 4

    .line 29
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    const-class v2, Lcom/squareup/wavpool/swipe/SwipeEvents$RecordingEvent;

    .line 30
    invoke-virtual {p1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/swipe/-$$Lambda$RecorderErrorReporterListener$VaNOlI0MSr0xH-NijBNaXoUxvDU;

    invoke-direct {v3, p0}, Lcom/squareup/swipe/-$$Lambda$RecorderErrorReporterListener$VaNOlI0MSr0xH-NijBNaXoUxvDU;-><init>(Lcom/squareup/swipe/RecorderErrorReporterListener;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/wavpool/swipe/Recorder$State;

    .line 31
    invoke-virtual {p1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v2, Lcom/squareup/swipe/-$$Lambda$RecorderErrorReporterListener$boWocuSGIIpkctmapb091IqDgUc;

    invoke-direct {v2, p0}, Lcom/squareup/swipe/-$$Lambda$RecorderErrorReporterListener$boWocuSGIIpkctmapb091IqDgUc;-><init>(Lcom/squareup/swipe/RecorderErrorReporterListener;)V

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>([Lio/reactivex/disposables/Disposable;)V

    return-object v0
.end method
