.class Lcom/squareup/swipe/SwipeValidator$1;
.super Ljava/lang/Object;
.source "SwipeValidator.java"

# interfaces
.implements Lcom/squareup/Card$InputType$InputTypeHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/swipe/SwipeValidator;->swipeHasEnoughData(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/Card$InputType$InputTypeHandler<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/swipe/SwipeValidator;


# direct methods
.method constructor <init>(Lcom/squareup/swipe/SwipeValidator;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/swipe/SwipeValidator$1;->this$0:Lcom/squareup/swipe/SwipeValidator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 1

    .line 65
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No A10."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x1

    .line 49
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x0

    .line 45
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x1

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x0

    .line 57
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x0

    .line 61
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 0

    const/4 p1, 0x0

    .line 73
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;
    .locals 1

    .line 69
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "X2 swipes should not pass through here"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/swipe/SwipeValidator$1;->handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
