.class public abstract Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.super Ljava/lang/Object;
.source "DebouncedOnEditorActionListener.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 19
    invoke-static {p1}, Lcom/squareup/debounce/Debouncers;->attemptPerform(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;->doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
