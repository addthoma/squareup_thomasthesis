.class final Lcom/squareup/debounce/Debouncers$2;
.super Lcom/squareup/debounce/DebouncedOnItemClickListener;
.source "Debouncers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debounce/Debouncers;->debounceItemClick(Landroid/widget/AdapterView$OnItemClickListener;)Lcom/squareup/debounce/DebouncedOnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$listener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method constructor <init>(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/debounce/Debouncers$2;->val$listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnItemClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/debounce/Debouncers$2;->val$listener:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    return-void
.end method
