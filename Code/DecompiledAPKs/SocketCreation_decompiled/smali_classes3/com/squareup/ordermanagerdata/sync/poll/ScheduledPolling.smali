.class public final Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;
.super Ljava/lang/Object;
.source "ScheduledPolling.kt"

# interfaces
.implements Lcom/squareup/ordermanagerdata/sync/poll/Polling;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;",
        "Lcom/squareup/ordermanagerdata/sync/poll/Polling;",
        "intervalProvider",
        "Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;",
        "intervalScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;Lio/reactivex/Scheduler;)V",
        "getIntervalScheduler",
        "()Lio/reactivex/Scheduler;",
        "sample",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/sync/SyncEvent$Poll;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final intervalProvider:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;

.field private final intervalScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "intervalProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intervalScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->intervalProvider:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->intervalScheduler:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public final getIntervalScheduler()Lio/reactivex/Scheduler;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->intervalScheduler:Lio/reactivex/Scheduler;

    return-object v0
.end method

.method public sample()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/sync/SyncEvent$Poll;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;->intervalProvider:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPollingIntervalProvider;->pollingInterval()Lio/reactivex/Observable;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$1;-><init>(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$2;->INSTANCE:Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling$sample$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "intervalProvider.polling\u2026  .map { SyncEvent.Poll }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
