.class public final Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory;
.super Ljava/lang/Object;
.source "OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lio/reactivex/Scheduler;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory$InstanceHolder;->access$000()Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSingleScheduler()Lio/reactivex/Scheduler;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/ordermanagerdata/OrderManagerDataModule;->Companion:Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;->provideSingleScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    return-object v0
.end method


# virtual methods
.method public get()Lio/reactivex/Scheduler;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory;->provideSingleScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule_Companion_ProvideSingleSchedulerFactory;->get()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method
