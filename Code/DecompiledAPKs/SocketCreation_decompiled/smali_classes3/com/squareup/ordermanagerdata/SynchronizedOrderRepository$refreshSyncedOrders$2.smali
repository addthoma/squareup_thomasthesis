.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->refreshSyncedOrders()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u001e\u0012\u001a\u0008\u0001\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u00020\u00012\u001a\u0010\u0005\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00070\u00070\u00060\u0002H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "Lcom/squareup/orders/model/Order;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;


# direct methods
.method constructor <init>(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;->this$0:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;

    invoke-static {v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;->access$getLocalOrderStore$p(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/squareup/ordermanagerdata/local/LocalOrderStore;

    move-result-object v0

    .line 196
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {p1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 197
    sget-object v1, Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshAllOrders;->INSTANCE:Lcom/squareup/ordermanagerdata/local/UpdateMode$RefreshAllOrders;

    check-cast v1, Lcom/squareup/ordermanagerdata/local/UpdateMode;

    .line 195
    invoke-interface {v0, p1, v1}, Lcom/squareup/ordermanagerdata/local/LocalOrderStore;->updateOrders(Ljava/util/List;Lcom/squareup/ordermanagerdata/local/UpdateMode;)Lio/reactivex/Completable;

    move-result-object p1

    .line 199
    sget-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2$1;->INSTANCE:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepositoryKt;->access$toResultStateSingle(Lio/reactivex/Completable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 201
    :cond_0
    instance-of v0, p1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(it)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$refreshSyncedOrders$2;->apply(Lcom/squareup/ordermanagerdata/ResultState;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
