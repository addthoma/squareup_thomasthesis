.class final Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;
.super Ljava/lang/Object;
.source "SynchronizedOrderRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SearchInFlight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0015\u0008\u0082\u0008\u0018\u0000 \"2\u00020\u0001:\u0001\"B3\u0012\u0012\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0015\u0010\u0019\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003J?\u0010\u001d\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u000e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\u000bH\u00d6\u0001J\t\u0010!\u001a\u00020\tH\u00d6\u0001R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001d\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;",
        "",
        "latestResult",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "searchQuery",
        "Lcom/squareup/ordermanagerdata/SearchQuery;",
        "pagToken",
        "",
        "pagesFetched",
        "",
        "(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)V",
        "hasMorePages",
        "",
        "getHasMorePages",
        "()Z",
        "getLatestResult",
        "()Lcom/squareup/ordermanagerdata/ResultState;",
        "getPagToken",
        "()Ljava/lang/String;",
        "getPagesFetched",
        "()I",
        "getSearchQuery",
        "()Lcom/squareup/ordermanagerdata/SearchQuery;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;


# instance fields
.field private final hasMorePages:Z

.field private final latestResult:Lcom/squareup/ordermanagerdata/ResultState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pagToken:Ljava/lang/String;

.field private final pagesFetched:I

.field private final searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->Companion:Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    const-string v0, "latestResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchQuery"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    iput-object p3, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    .line 69
    iget-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    const/16 p2, 0xa

    if-ge p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->hasMorePages:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;IILjava/lang/Object;)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->copy(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ordermanagerdata/ResultState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    return v0
.end method

.method public final copy(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;",
            "Lcom/squareup/ordermanagerdata/SearchQuery;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;"
        }
    .end annotation

    const-string v0, "latestResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchQuery"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;-><init>(Lcom/squareup/ordermanagerdata/ResultState;Lcom/squareup/ordermanagerdata/SearchQuery;Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    iget p1, p1, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHasMorePages()Z
    .locals 1

    .line 69
    iget-boolean v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->hasMorePages:Z

    return v0
.end method

.method public final getLatestResult()Lcom/squareup/ordermanagerdata/ResultState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    return-object v0
.end method

.method public final getPagToken()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getPagesFetched()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    return v0
.end method

.method public final getSearchQuery()Lcom/squareup/ordermanagerdata/SearchQuery;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SearchInFlight(latestResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->latestResult:Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", searchQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->searchQuery:Lcom/squareup/ordermanagerdata/SearchQuery;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", pagToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", pagesFetched="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository$SearchInFlight;->pagesFetched:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
