.class public abstract Lcom/squareup/ordermanagerdata/OrderManagerDataModule;
.super Ljava/lang/Object;
.source "OrderManagerDataModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\'\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H!\u00a2\u0006\u0002\u0008\u0016J\u0015\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u001a\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/OrderManagerDataModule;",
        "",
        "()V",
        "bindLocalDataStore",
        "Lcom/squareup/ordermanagerdata/local/LocalOrderStore;",
        "localDataStore",
        "Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;",
        "bindLocalDataStore$impl_wiring_release",
        "bindOrderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "repository",
        "Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;",
        "bindOrderRepository$impl_wiring_release",
        "bindPolling",
        "Lcom/squareup/ordermanagerdata/sync/poll/Polling;",
        "scheduledPolling",
        "Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;",
        "bindPolling$impl_wiring_release",
        "bindRemoteOrderStore",
        "Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;",
        "remoteStore",
        "Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;",
        "bindRemoteOrderStore$impl_wiring_release",
        "bindSynchronizedOrderRepository",
        "Lmortar/Scoped;",
        "synchronizedOrderRepository",
        "bindSynchronizedOrderRepository$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ordermanagerdata/OrderManagerDataModule;->Companion:Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideSingleScheduler()Lio/reactivex/Scheduler;
    .locals 1
    .annotation runtime Lcom/squareup/ordermanagerdata/local/Single;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ordermanagerdata/OrderManagerDataModule;->Companion:Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/OrderManagerDataModule$Companion;->provideSingleScheduler()Lio/reactivex/Scheduler;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract bindLocalDataStore$impl_wiring_release(Lcom/squareup/ordermanagerdata/local/InMemoryLocalOrderStore;)Lcom/squareup/ordermanagerdata/local/LocalOrderStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindOrderRepository$impl_wiring_release(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lcom/squareup/ordermanagerdata/OrderRepository;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindPolling$impl_wiring_release(Lcom/squareup/ordermanagerdata/sync/poll/ScheduledPolling;)Lcom/squareup/ordermanagerdata/sync/poll/Polling;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRemoteOrderStore$impl_wiring_release(Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;)Lcom/squareup/ordermanagerdata/remote/RemoteOrderStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindSynchronizedOrderRepository$impl_wiring_release(Lcom/squareup/ordermanagerdata/SynchronizedOrderRepository;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
