.class public final Lcom/squareup/ordermanagerdata/proto/CancellationReasonsKt;
.super Ljava/lang/Object;
.source "CancellationReasons.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "getReasonName",
        "",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "res",
        "Lcom/squareup/util/Res;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getReasonName(Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$getReasonName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/squareup/ordermanagerdata/proto/CancellationReasonsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/ordermanagerdata/CancellationReason;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 18
    sget p0, Lcom/squareup/ordermanagerdata/R$string;->orderhub_order_cancelation_reason_other:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 19
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Unexpected cancellation reason state"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 16
    :cond_1
    sget p0, Lcom/squareup/ordermanagerdata/R$string;->orderhub_order_cancelation_reason_customer_request:I

    .line 15
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 13
    :cond_2
    sget p0, Lcom/squareup/ordermanagerdata/R$string;->orderhub_order_cancelation_reason_items_out_of_stock:I

    .line 12
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method
