.class public Lcom/squareup/connectivity/check/RealConnectivityCheck;
.super Ljava/lang/Object;
.source "RealConnectivityCheck.java"

# interfaces
.implements Lcom/squareup/connectivity/check/ConnectivityCheck;


# instance fields
.field private final pingUrl:Ljava/lang/String;

.field private final retrofitClient:Lretrofit/client/Client;


# direct methods
.method public constructor <init>(Lretrofit/client/Client;Ljava/lang/String;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/connectivity/check/RealConnectivityCheck;->retrofitClient:Lretrofit/client/Client;

    .line 15
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "_status"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/connectivity/check/RealConnectivityCheck;->pingUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public checkStatusEndpoint()Z
    .locals 6

    const/4 v0, 0x0

    .line 20
    :try_start_0
    iget-object v1, p0, Lcom/squareup/connectivity/check/RealConnectivityCheck;->retrofitClient:Lretrofit/client/Client;

    new-instance v2, Lretrofit/client/Request;

    const-string v3, "GET"

    iget-object v4, p0, Lcom/squareup/connectivity/check/RealConnectivityCheck;->pingUrl:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v5}, Lretrofit/client/Request;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lretrofit/mime/TypedOutput;)V

    .line 21
    invoke-interface {v1, v2}, Lretrofit/client/Client;->execute(Lretrofit/client/Request;)Lretrofit/client/Response;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lretrofit/client/Response;->getStatus()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x1f4

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :catch_0
    :cond_0
    return v0
.end method
