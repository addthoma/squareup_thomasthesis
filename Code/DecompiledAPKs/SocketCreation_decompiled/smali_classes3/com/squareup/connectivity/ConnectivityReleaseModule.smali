.class public abstract Lcom/squareup/connectivity/ConnectivityReleaseModule;
.super Ljava/lang/Object;
.source "ConnectivityReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\'\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/connectivity/ConnectivityReleaseModule;",
        "",
        "()V",
        "bindRealConnectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "realConnectivityMonitor",
        "Lcom/squareup/connectivity/RealConnectivityMonitor;",
        "bindRealConnectivityMonitorToAppScoped",
        "Lmortar/Scoped;",
        "bindRealInternetStatusMonitor",
        "Lcom/squareup/internet/InternetStatusMonitor;",
        "realInternetStatusMonitor",
        "Lcom/squareup/internet/RealInternetStatusMonitor;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindRealConnectivityMonitor(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lcom/squareup/connectivity/ConnectivityMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindRealConnectivityMonitorToAppScoped(Lcom/squareup/connectivity/RealConnectivityMonitor;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForApp;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindRealInternetStatusMonitor(Lcom/squareup/internet/RealInternetStatusMonitor;)Lcom/squareup/internet/InternetStatusMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
