.class public final Lcom/squareup/orderhub/applet/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderhub/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final orderhub_alert_new_order_message_new_plural:I = 0x7f12121e

.field public static final orderhub_alert_new_order_message_new_singular:I = 0x7f12121f

.field public static final orderhub_alert_new_order_message_upcoming_and_new_singular:I = 0x7f121220

.field public static final orderhub_alert_new_order_message_upcoming_plural:I = 0x7f121221

.field public static final orderhub_alert_new_order_message_upcoming_plural_and_new_plural:I = 0x7f121222

.field public static final orderhub_alert_new_order_message_upcoming_plural_and_new_singular:I = 0x7f121223

.field public static final orderhub_alert_new_order_message_upcoming_singular:I = 0x7f121224

.field public static final orderhub_alert_new_order_message_upcoming_singular_and_new_plural:I = 0x7f121225

.field public static final orderhub_alert_new_order_title:I = 0x7f121226

.field public static final orderhub_alert_new_orders_primary_button:I = 0x7f121227

.field public static final orderhub_alert_new_orders_secondary_button:I = 0x7f121228

.field public static final orderhub_alert_new_orders_title:I = 0x7f121229

.field public static final orderhub_alert_order_update_failed_message:I = 0x7f12122a

.field public static final orderhub_alert_order_update_failed_title:I = 0x7f12122b

.field public static final orderhub_alert_order_update_network_failure_message:I = 0x7f12122c

.field public static final orderhub_alert_order_update_network_failure_title:I = 0x7f12122d

.field public static final orderhub_alert_order_version_mismatch_message:I = 0x7f12122e

.field public static final orderhub_alert_order_version_mismatch_title:I = 0x7f12122f

.field public static final orderhub_applet_name:I = 0x7f121238

.field public static final orderhub_connection_error:I = 0x7f121239

.field public static final orderhub_connection_error_message:I = 0x7f12123a

.field public static final orderhub_date_format_hour_minute_time:I = 0x7f12123b

.field public static final orderhub_date_format_month_day_and_time:I = 0x7f12123c

.field public static final orderhub_detail_order_missing_recipient:I = 0x7f12123d

.field public static final orderhub_detail_order_no_courier_assigned:I = 0x7f12123e

.field public static final orderhub_detail_order_title_format:I = 0x7f12123f

.field public static final orderhub_detail_orders_not_syncing_message:I = 0x7f121240

.field public static final orderhub_detail_orders_not_syncing_message_last_sync_date:I = 0x7f121241

.field public static final orderhub_detail_orders_not_syncing_title:I = 0x7f121242

.field public static final orderhub_fulfillment_type_name_short_custom:I = 0x7f121243

.field public static final orderhub_fulfillment_type_name_short_delivery:I = 0x7f121244

.field public static final orderhub_fulfillment_type_name_short_digital:I = 0x7f121245

.field public static final orderhub_fulfillment_type_name_short_managed_delivery:I = 0x7f121246

.field public static final orderhub_fulfillment_type_name_short_pickup:I = 0x7f121247

.field public static final orderhub_fulfillment_type_name_short_shipment:I = 0x7f121248

.field public static final orderhub_main_section_name:I = 0x7f121249

.field public static final orderhub_message_no_active_sources_button:I = 0x7f12124a

.field public static final orderhub_message_no_active_sources_subtitle:I = 0x7f12124b

.field public static final orderhub_message_no_active_sources_title:I = 0x7f12124c

.field public static final orderhub_message_no_orders_found_for_search:I = 0x7f12124d

.field public static final orderhub_message_no_orders_subtitle:I = 0x7f12124e

.field public static final orderhub_message_no_orders_title:I = 0x7f12124f

.field public static final orderhub_message_search_error:I = 0x7f121250

.field public static final orderhub_message_search_error_retry_cta:I = 0x7f121251

.field public static final orderhub_message_search_instructions:I = 0x7f121252

.field public static final orderhub_message_transactions_link_text:I = 0x7f121253

.field public static final orderhub_new_orders_notification_content_plural:I = 0x7f121254

.field public static final orderhub_new_orders_notification_content_singular:I = 0x7f121255

.field public static final orderhub_new_orders_notification_title_plural:I = 0x7f121256

.field public static final orderhub_new_orders_notification_title_singular:I = 0x7f121257

.field public static final orderhub_note_action_bar_title:I = 0x7f121258

.field public static final orderhub_note_action_button:I = 0x7f121259

.field public static final orderhub_note_characters_remaining:I = 0x7f12125a

.field public static final orderhub_note_delete_note:I = 0x7f12125b

.field public static final orderhub_order_action_bar_title:I = 0x7f12125c

.field public static final orderhub_order_add_note:I = 0x7f12125d

.field public static final orderhub_order_add_tracking:I = 0x7f12125e

.field public static final orderhub_order_adjust:I = 0x7f12125f

.field public static final orderhub_order_adjust_pickup_time:I = 0x7f121260

.field public static final orderhub_order_cancelation_bill_retrieval_failed_message:I = 0x7f121261

.field public static final orderhub_order_cancelation_bill_retrieval_failed_title:I = 0x7f121262

.field public static final orderhub_order_cancelation_cancel_items_header:I = 0x7f121263

.field public static final orderhub_order_cancelation_inventory_adjustment_failed_message:I = 0x7f121264

.field public static final orderhub_order_cancelation_inventory_adjustment_failed_title:I = 0x7f121265

.field public static final orderhub_order_cancelation_inventory_lost:I = 0x7f121266

.field public static final orderhub_order_cancelation_inventory_restocked:I = 0x7f121267

.field public static final orderhub_order_cancelation_order_already_refunded_message:I = 0x7f121268

.field public static final orderhub_order_cancelation_order_already_refunded_ok:I = 0x7f121269

.field public static final orderhub_order_cancelation_order_already_refunded_title:I = 0x7f12126a

.field public static final orderhub_order_cancelation_refund_selection_canceling:I = 0x7f12126e

.field public static final orderhub_order_cancelation_refund_selection_continue_to_refund:I = 0x7f12126f

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed:I = 0x7f121270

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed_cancel:I = 0x7f121271

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed_retry:I = 0x7f121272

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_items_canceled:I = 0x7f121273

.field public static final orderhub_order_cancelation_refund_selection_skip_refund:I = 0x7f121274

.field public static final orderhub_order_cancelation_select_a_reason:I = 0x7f121275

.field public static final orderhub_order_cancelation_select_items_to_cancel:I = 0x7f121276

.field public static final orderhub_order_courier_header:I = 0x7f121277

.field public static final orderhub_order_customer_header:I = 0x7f121278

.field public static final orderhub_order_delivery_deliver_to_header:I = 0x7f121279

.field public static final orderhub_order_delivery_deliver_to_header_for_print:I = 0x7f12127a

.field public static final orderhub_order_delivery_time_header:I = 0x7f12127b

.field public static final orderhub_order_display_state_canceled:I = 0x7f12127c

.field public static final orderhub_order_display_state_completed:I = 0x7f12127d

.field public static final orderhub_order_display_state_failed:I = 0x7f12127e

.field public static final orderhub_order_display_state_in_progress:I = 0x7f12127f

.field public static final orderhub_order_display_state_new:I = 0x7f121280

.field public static final orderhub_order_display_state_ready:I = 0x7f121281

.field public static final orderhub_order_display_state_rejected:I = 0x7f121282

.field public static final orderhub_order_display_state_unknown:I = 0x7f121283

.field public static final orderhub_order_display_state_upcoming:I = 0x7f121284

.field public static final orderhub_order_edit_note:I = 0x7f121285

.field public static final orderhub_order_edit_tracking:I = 0x7f121286

.field public static final orderhub_order_external_source_manage:I = 0x7f121287

.field public static final orderhub_order_external_source_view:I = 0x7f121288

.field public static final orderhub_order_filter_header_source_uppercase:I = 0x7f121289

.field public static final orderhub_order_filter_header_status_uppercase:I = 0x7f12128a

.field public static final orderhub_order_filter_header_type_uppercase:I = 0x7f12128b

.field public static final orderhub_order_filter_value_status_active:I = 0x7f12128c

.field public static final orderhub_order_filter_value_status_active_shortened:I = 0x7f12128d

.field public static final orderhub_order_filter_value_status_completed:I = 0x7f12128e

.field public static final orderhub_order_filter_value_status_upcoming:I = 0x7f12128f

.field public static final orderhub_order_filter_value_type_custom:I = 0x7f121290

.field public static final orderhub_order_filter_value_type_delivery:I = 0x7f121291

.field public static final orderhub_order_filter_value_type_digital:I = 0x7f121292

.field public static final orderhub_order_filter_value_type_managed_delivery:I = 0x7f121293

.field public static final orderhub_order_filter_value_type_pickup:I = 0x7f121294

.field public static final orderhub_order_filter_value_type_shipment:I = 0x7f121295

.field public static final orderhub_order_filter_value_type_unknown:I = 0x7f121296

.field public static final orderhub_order_fulfillment_action_accept:I = 0x7f121297

.field public static final orderhub_order_fulfillment_action_cancel_items:I = 0x7f121298

.field public static final orderhub_order_fulfillment_action_complete:I = 0x7f121299

.field public static final orderhub_order_fulfillment_action_mark_in_progress:I = 0x7f12129a

.field public static final orderhub_order_fulfillment_action_mark_picked_up:I = 0x7f12129b

.field public static final orderhub_order_fulfillment_action_mark_ready:I = 0x7f12129c

.field public static final orderhub_order_fulfillment_action_mark_shipped:I = 0x7f12129d

.field public static final orderhub_order_id_header:I = 0x7f12129e

.field public static final orderhub_order_item_name_custom_amount:I = 0x7f12129f

.field public static final orderhub_order_item_name_unknown:I = 0x7f1212a0

.field public static final orderhub_order_item_quantity:I = 0x7f1212a1

.field public static final orderhub_order_item_selection_next:I = 0x7f1212a2

.field public static final orderhub_order_item_selection_select_all:I = 0x7f1212a3

.field public static final orderhub_order_items_header_canceled_uppercase:I = 0x7f1212a4

.field public static final orderhub_order_items_header_completed_uppercase:I = 0x7f1212a5

.field public static final orderhub_order_items_header_uppercase:I = 0x7f1212a6

.field public static final orderhub_order_pickup_time_header:I = 0x7f1212a7

.field public static final orderhub_order_price_discount:I = 0x7f1212a8

.field public static final orderhub_order_price_subtotal:I = 0x7f1212a9

.field public static final orderhub_order_price_tax:I = 0x7f1212aa

.field public static final orderhub_order_price_tip:I = 0x7f1212ab

.field public static final orderhub_order_price_total:I = 0x7f1212ac

.field public static final orderhub_order_print:I = 0x7f1212ad

.field public static final orderhub_order_relative_date_ago:I = 0x7f1212ae

.field public static final orderhub_order_relative_date_days:I = 0x7f1212af

.field public static final orderhub_order_relative_date_format:I = 0x7f1212b0

.field public static final orderhub_order_relative_date_hours_minutes:I = 0x7f1212b1

.field public static final orderhub_order_relative_date_hours_minutes_ago:I = 0x7f1212b2

.field public static final orderhub_order_relative_date_months:I = 0x7f1212b3

.field public static final orderhub_order_relative_date_one_day:I = 0x7f1212b4

.field public static final orderhub_order_relative_date_one_month:I = 0x7f1212b5

.field public static final orderhub_order_relative_date_one_week:I = 0x7f1212b6

.field public static final orderhub_order_relative_date_one_year:I = 0x7f1212b7

.field public static final orderhub_order_relative_date_time_format:I = 0x7f1212b8

.field public static final orderhub_order_relative_date_today:I = 0x7f1212b9

.field public static final orderhub_order_relative_date_tomorrow:I = 0x7f1212ba

.field public static final orderhub_order_relative_date_weeks:I = 0x7f1212bb

.field public static final orderhub_order_relative_date_years:I = 0x7f1212bc

.field public static final orderhub_order_relative_date_yesterday:I = 0x7f1212bd

.field public static final orderhub_order_reprint:I = 0x7f1212be

.field public static final orderhub_order_shipment_method_header:I = 0x7f1212bf

.field public static final orderhub_order_shipment_select_items_to_ship:I = 0x7f1212c0

.field public static final orderhub_order_shipment_ship_items_header:I = 0x7f1212c1

.field public static final orderhub_order_shipment_to_header:I = 0x7f1212c2

.field public static final orderhub_order_shipment_to_header_for_print:I = 0x7f1212c3

.field public static final orderhub_order_source_id:I = 0x7f1212c4

.field public static final orderhub_order_status_canceled_at:I = 0x7f1212c5

.field public static final orderhub_order_status_canceled_at_with_reason:I = 0x7f1212c6

.field public static final orderhub_order_status_canceled_with_reason:I = 0x7f1212c7

.field public static final orderhub_order_status_completed_at:I = 0x7f1212c8

.field public static final orderhub_order_status_picked_up_at:I = 0x7f1212c9

.field public static final orderhub_order_status_placed_at:I = 0x7f1212ca

.field public static final orderhub_order_status_rejected_at:I = 0x7f1212cb

.field public static final orderhub_order_status_shipped_at:I = 0x7f1212cc

.field public static final orderhub_order_tracking_info:I = 0x7f1212cd

.field public static final orderhub_order_tracking_label:I = 0x7f1212ce

.field public static final orderhub_order_txn_header:I = 0x7f1212cf

.field public static final orderhub_order_view_transaction_detail:I = 0x7f1212d0

.field public static final orderhub_print_delivery:I = 0x7f1212d1

.field public static final orderhub_print_digital:I = 0x7f1212d2

.field public static final orderhub_print_fulfillment_name_with_pickup_at:I = 0x7f1212d3

.field public static final orderhub_print_pickup:I = 0x7f1212d4

.field public static final orderhub_print_shipment:I = 0x7f1212d5

.field public static final orderhub_quick_actions_awaiting_courier:I = 0x7f1212d9

.field public static final orderhub_quick_actions_failed_action:I = 0x7f1212db

.field public static final orderhub_quick_actions_retry:I = 0x7f1212dc

.field public static final orderhub_quick_actions_view_to_ship:I = 0x7f1212df

.field public static final orderhub_search_hint:I = 0x7f1212e0

.field public static final orderhub_success_cancellation_message:I = 0x7f1212e1

.field public static final orderhub_success_cancellation_title:I = 0x7f1212e2

.field public static final orderhub_tracking_action_bar_title:I = 0x7f1212e3

.field public static final orderhub_tracking_action_button_done:I = 0x7f1212e4

.field public static final orderhub_tracking_action_button_skip_tracking:I = 0x7f1212e5

.field public static final orderhub_tracking_carrier_hint:I = 0x7f1212e6

.field public static final orderhub_tracking_carrier_title:I = 0x7f1212e7

.field public static final orderhub_tracking_hint:I = 0x7f1212e8

.field public static final orderhub_tracking_other:I = 0x7f1212e9

.field public static final orderhub_tracking_remove_tracking_button_text:I = 0x7f1212ea

.field public static final orderhub_tracking_tracking_number:I = 0x7f1212eb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
