.class public final Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "LocalNotificationEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0011\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000c\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "local_notifications_notification_id",
        "",
        "local_notifications_notification_title",
        "local_notifications_notification_body",
        "local_notifications_browser_dialog_body",
        "local_notifications_client_action",
        "local_notifications_notification_url",
        "local_notifications_created_at",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
        "getLocal_notifications_browser_dialog_body",
        "()Ljava/lang/String;",
        "getLocal_notifications_client_action",
        "getLocal_notifications_created_at",
        "getLocal_notifications_notification_body",
        "getLocal_notifications_notification_id",
        "getLocal_notifications_notification_title",
        "getLocal_notifications_notification_url",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "local_notifications"

.field public static final Companion:Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent$Companion;


# instance fields
.field private final local_notifications_browser_dialog_body:Ljava/lang/String;

.field private final local_notifications_client_action:Ljava/lang/String;

.field private final local_notifications_created_at:Ljava/lang/String;

.field private final local_notifications_notification_body:Ljava/lang/String;

.field private final local_notifications_notification_id:Ljava/lang/String;

.field private final local_notifications_notification_title:Ljava/lang/String;

.field private final local_notifications_notification_url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->Companion:Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "local_notifications_notification_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_notification_title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_notification_body"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_browser_dialog_body"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_client_action"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_notification_url"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications_created_at"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "local_notifications"

    .line 17
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_body:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_browser_dialog_body:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_client_action:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_url:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_created_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getLocal_notifications_browser_dialog_body()Ljava/lang/String;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_browser_dialog_body:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_client_action()Ljava/lang/String;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_client_action:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_created_at()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_created_at:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_notification_body()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_body:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_notification_id()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_id:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_notification_title()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_title:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocal_notifications_notification_url()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/logging/LocalNotificationEvent;->local_notifications_notification_url:Ljava/lang/String;

    return-object v0
.end method
