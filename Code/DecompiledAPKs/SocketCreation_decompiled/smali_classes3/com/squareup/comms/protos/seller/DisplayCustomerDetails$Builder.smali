.class public final Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayCustomerDetails.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;",
        "Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016J\u0014\u0010\u0004\u001a\u00020\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;",
        "()V",
        "details",
        "",
        "",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 68
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 70
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;->details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;
    .locals 3

    .line 78
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;

    .line 79
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;->details:Ljava/util/List;

    .line 80
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 78
    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayCustomerDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final details(Ljava/util/List;)Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;"
        }
    .end annotation

    const-string v0, "details"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 74
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayCustomerDetails$Builder;->details:Ljava/util/List;

    return-object p0
.end method
