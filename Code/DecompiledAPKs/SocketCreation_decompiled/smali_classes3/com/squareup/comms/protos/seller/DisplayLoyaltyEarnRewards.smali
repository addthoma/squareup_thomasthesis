.class public final Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;
.super Lcom/squareup/wire/AndroidMessage;
.source "DisplayLoyaltyEarnRewards.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;,
        Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;,
        Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayLoyaltyEarnRewards.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayLoyaltyEarnRewards.kt\ncom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards\n*L\n1#1,461:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0007\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0018\u0019\u001aBG\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010JV\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00082\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000fJ\u0013\u0010\u0012\u001a\u00020\u000b2\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0096\u0002J\u0008\u0010\u0015\u001a\u00020\u0004H\u0016J\u0008\u0010\u0016\u001a\u00020\u0002H\u0016J\u0008\u0010\u0017\u001a\u00020\u0008H\u0016R\u0010\u0010\r\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;",
        "points_earned",
        "",
        "points_terminology",
        "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;",
        "qualifying_purchase_description",
        "",
        "unit_name",
        "is_enrolled",
        "",
        "is_newly_enrolled",
        "dark_theme",
        "unknownFields",
        "Lokio/ByteString;",
        "(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "newBuilder",
        "toString",
        "Builder",
        "Companion",
        "PointsTerminology",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion;


# instance fields
.field public final dark_theme:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation
.end field

.field public final is_enrolled:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation
.end field

.field public final is_newly_enrolled:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation
.end field

.field public final points_earned:I
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field

.field public final points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.comms.protos.seller.DisplayLoyaltyEarnRewards$PointsTerminology#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation
.end field

.field public final qualifying_purchase_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation
.end field

.field public final unit_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->Companion:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion;

    .line 247
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion$ADAPTER$1;

    .line 249
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 250
    const-class v2, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 317
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)V
    .locals 1

    const-string v0, "points_terminology"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifying_purchase_description"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit_name"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unknownFields"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput p1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    iput-object p2, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iput-object p3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    iput-boolean p6, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    iput-boolean p7, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    return-void
.end method

.method public synthetic constructor <init>(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    move/from16 v0, p9

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v9, v0

    goto :goto_0

    :cond_0
    move-object/from16 v9, p8

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;-><init>(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    .line 148
    iget v2, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    .line 149
    iget-object v3, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    .line 150
    iget-object v4, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    .line 151
    iget-object v5, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    .line 152
    iget-boolean v6, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    .line 153
    iget-boolean v7, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    .line 154
    iget-boolean v8, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    .line 155
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->copy(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final copy(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;
    .locals 10

    const-string v0, "points_terminology"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qualifying_purchase_description"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit_name"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unknownFields"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    move-object v1, v0

    move v2, p1

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;-><init>(ILcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;Ljava/lang/String;Ljava/lang/String;ZZZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 106
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 107
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    iget v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    iget-boolean v3, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    if-ne v0, v3, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 119
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-virtual {v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;
    .locals 2

    .line 93
    new-instance v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;-><init>()V

    .line 94
    iget v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_earned:Ljava/lang/Integer;

    .line 95
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    .line 96
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->qualifying_purchase_description:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->unit_name:Ljava/lang/String;

    .line 98
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_enrolled:Ljava/lang/Boolean;

    .line 99
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->is_newly_enrolled:Ljava/lang/Boolean;

    .line 100
    iget-boolean v1, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->dark_theme:Ljava/lang/Boolean;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->newBuilder()Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 136
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "points_earned="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_earned:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "points_terminology="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->points_terminology:Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards$PointsTerminology;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "qualifying_purchase_description="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->qualifying_purchase_description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unit_name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->unit_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_enrolled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_enrolled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_newly_enrolled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->is_newly_enrolled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dark_theme="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/seller/DisplayLoyaltyEarnRewards;->dark_theme:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 143
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "DisplayLoyaltyEarnRewards{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string/jumbo v0, "}"

    .line 144
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 143
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
