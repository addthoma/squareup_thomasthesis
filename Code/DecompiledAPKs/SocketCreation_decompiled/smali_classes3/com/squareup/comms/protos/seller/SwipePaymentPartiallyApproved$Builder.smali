.class public final Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SwipePaymentPartiallyApproved.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;",
        "Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0015\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0008\u001a\u00020\u0002H\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;",
        "()V",
        "amount",
        "",
        "Ljava/lang/Long;",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 72
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;
    .locals 3

    .line 85
    new-instance v0, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;

    .line 86
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;->amount:Ljava/lang/Long;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 85
    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;-><init>(Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved$Builder;->build()Lcom/squareup/comms/protos/seller/SwipePaymentPartiallyApproved;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method
