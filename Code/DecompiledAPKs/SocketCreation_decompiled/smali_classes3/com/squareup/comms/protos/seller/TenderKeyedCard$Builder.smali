.class public final Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TenderKeyedCard.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/TenderKeyedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/TenderKeyedCard;",
        "Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0008\u0010\n\u001a\u00020\u0002H\u0016J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/TenderKeyedCard;",
        "()V",
        "amount",
        "",
        "Ljava/lang/Long;",
        "isGiftCard",
        "",
        "Ljava/lang/Boolean;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/Long;

.field public isGiftCard:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount(J)Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;
    .locals 0

    .line 90
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/TenderKeyedCard;
    .locals 7

    .line 99
    new-instance v0, Lcom/squareup/comms/protos/seller/TenderKeyedCard;

    .line 100
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->amount:Ljava/lang/Long;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 101
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->isGiftCard:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 99
    invoke-direct {v0, v5, v6, v1, v2}, Lcom/squareup/comms/protos/seller/TenderKeyedCard;-><init>(JZLokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "isGiftCard"

    aput-object v1, v0, v2

    .line 101
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v4, [Ljava/lang/Object;

    aput-object v1, v0, v3

    const-string v1, "amount"

    aput-object v1, v0, v2

    .line 100
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->build()Lcom/squareup/comms/protos/seller/TenderKeyedCard;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final isGiftCard(Z)Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;
    .locals 0

    .line 95
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TenderKeyedCard$Builder;->isGiftCard:Ljava/lang/Boolean;

    return-object p0
.end method
