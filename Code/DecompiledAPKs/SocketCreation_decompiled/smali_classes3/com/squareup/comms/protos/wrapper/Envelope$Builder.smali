.class public final Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Envelope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/wrapper/Envelope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/wrapper/Envelope;",
        "Lcom/squareup/comms/protos/wrapper/Envelope$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message_body:Lokio/ByteString;

.field public message_name:Ljava/lang/String;

.field public uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/wrapper/Envelope;
    .locals 5

    .line 130
    new-instance v0, Lcom/squareup/comms/protos/wrapper/Envelope;

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_body:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->uuid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/wrapper/Envelope;-><init>(Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->build()Lcom/squareup/comms/protos/wrapper/Envelope;

    move-result-object v0

    return-object v0
.end method

.method public message_body(Lokio/ByteString;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_body:Lokio/ByteString;

    return-object p0
.end method

.method public message_name(Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_name:Ljava/lang/String;

    return-object p0
.end method

.method public uuid(Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->uuid:Ljava/lang/String;

    return-object p0
.end method
