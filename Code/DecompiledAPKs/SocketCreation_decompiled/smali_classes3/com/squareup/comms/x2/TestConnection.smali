.class public Lcom/squareup/comms/x2/TestConnection;
.super Ljava/lang/Object;
.source "TestConnection.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBusConnection;


# instance fields
.field public subject:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/x2/TestConnection;->subject:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public observable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/comms/x2/TestConnection;->subject:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public post(Lcom/squareup/wire/Message;)V
    .locals 0

    return-void
.end method
