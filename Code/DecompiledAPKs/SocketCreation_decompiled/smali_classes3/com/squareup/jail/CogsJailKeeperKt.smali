.class public final Lcom/squareup/jail/CogsJailKeeperKt;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "MAX_AGE",
        "Lcom/squareup/shared/catalog/utils/ElapsedTime;",
        "jail_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final MAX_AGE:Lcom/squareup/shared/catalog/utils/ElapsedTime;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 66
    new-instance v0, Lcom/squareup/shared/catalog/utils/ElapsedTime;

    sget-object v1, Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;->DAYS:Lcom/squareup/shared/catalog/utils/ElapsedTime$Interval;

    const-wide/16 v2, 0xa

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/shared/catalog/utils/ElapsedTime;-><init>(JLcom/squareup/shared/catalog/utils/ElapsedTime$Interval;)V

    sput-object v0, Lcom/squareup/jail/CogsJailKeeperKt;->MAX_AGE:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    return-void
.end method

.method public static final synthetic access$getMAX_AGE$p()Lcom/squareup/shared/catalog/utils/ElapsedTime;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/jail/CogsJailKeeperKt;->MAX_AGE:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    return-object v0
.end method
