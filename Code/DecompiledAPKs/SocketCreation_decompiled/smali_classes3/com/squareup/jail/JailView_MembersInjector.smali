.class public final Lcom/squareup/jail/JailView_MembersInjector;
.super Ljava/lang/Object;
.source "JailView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/jail/JailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiRequestControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;"
        }
    .end annotation
.end field

.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/JailPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/JailPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/jail/JailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/jail/JailView_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/jail/JailView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jail/JailPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/jail/JailView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/jail/JailView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/jail/JailView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectApiRequestController(Lcom/squareup/jail/JailView;Lcom/squareup/api/ApiRequestController;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/jail/JailView;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    return-void
.end method

.method public static injectAppNameFormatter(Lcom/squareup/jail/JailView;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/jail/JailView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/jail/JailView;Ljava/lang/Object;)V
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/jail/JailPresenter;

    iput-object p1, p0, Lcom/squareup/jail/JailView;->presenter:Lcom/squareup/jail/JailPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/jail/JailView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/jail/JailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/jail/JailView_MembersInjector;->injectPresenter(Lcom/squareup/jail/JailView;Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/jail/JailView_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiRequestController;

    invoke-static {p1, v0}, Lcom/squareup/jail/JailView_MembersInjector;->injectApiRequestController(Lcom/squareup/jail/JailView;Lcom/squareup/api/ApiRequestController;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/jail/JailView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/jail/JailView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/jail/JailView;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/jail/JailView;

    invoke-virtual {p0, p1}, Lcom/squareup/jail/JailView_MembersInjector;->injectMembers(Lcom/squareup/jail/JailView;)V

    return-void
.end method
