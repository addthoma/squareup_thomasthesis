.class public final Lcom/squareup/jail/JailPresenter_Factory;
.super Ljava/lang/Object;
.source "JailPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/jail/JailPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final jailScreenConfigurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailScreenConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedOutStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailScreenConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/jail/JailPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/jail/JailPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/jail/JailPresenter_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/jail/JailPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/jail/JailPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/jail/JailPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/jail/JailPresenter_Factory;->jailScreenConfigurationProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/jail/JailPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/jail/JailPresenter_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/jail/JailPresenter_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/jail/JailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/jail/JailPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailScreenConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/jail/JailPresenter_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/jail/JailPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/jail/JailPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;Lcom/squareup/jailkeeper/JailScreenConfiguration;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/settings/server/Features;)Lcom/squareup/jail/JailPresenter;
    .locals 13

    .line 90
    new-instance v12, Lcom/squareup/jail/JailPresenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/jail/JailPresenter;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;Lcom/squareup/jailkeeper/JailScreenConfiguration;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/settings/server/Features;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/jail/JailPresenter;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/jailkeeper/JailKeeper;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cogs/Cogs;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->jailScreenConfigurationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/jailkeeper/JailScreenConfiguration;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->loggedOutStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/loggedout/LoggedOutStarter;

    iget-object v0, p0, Lcom/squareup/jail/JailPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v11}, Lcom/squareup/jail/JailPresenter_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/cogs/Cogs;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Clock;Lcom/squareup/jailkeeper/JailScreenConfiguration;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/loggedout/LoggedOutStarter;Lcom/squareup/settings/server/Features;)Lcom/squareup/jail/JailPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/jail/JailPresenter_Factory;->get()Lcom/squareup/jail/JailPresenter;

    move-result-object v0

    return-object v0
.end method
