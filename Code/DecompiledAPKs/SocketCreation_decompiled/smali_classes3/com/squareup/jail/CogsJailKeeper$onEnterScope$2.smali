.class final Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jail/CogsJailKeeper;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jail/CogsJailKeeper;


# direct methods
.method constructor <init>(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;)V
    .locals 0

    .line 116
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-virtual {p1}, Lcom/squareup/jail/CogsJailKeeper;->backgroundSync()V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;

    invoke-virtual {p0, p1}, Lcom/squareup/jail/CogsJailKeeper$onEnterScope$2;->accept(Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;)V

    return-void
.end method
