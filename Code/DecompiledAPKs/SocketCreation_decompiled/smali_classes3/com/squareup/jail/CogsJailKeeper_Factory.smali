.class public final Lcom/squareup/jail/CogsJailKeeper_Factory;
.super Ljava/lang/Object;
.source "CogsJailKeeper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/jail/CogsJailKeeper;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;>;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final callbacksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogFeesPreloaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final compDiscountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final diningOptionCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final loggedInScopeNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final pushMessageDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketGroupsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final voidReasonsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 96
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 97
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 98
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->loggedInScopeNotifierProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 99
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 100
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 101
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 102
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->catalogFeesPreloaderProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 103
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 104
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->cashDrawersProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 105
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 106
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 107
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 108
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 109
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->voidReasonsCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 110
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 111
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 112
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->callbacksProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 113
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 114
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 115
    iput-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->pushMessageDelegateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/jail/CogsJailKeeper_Factory;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;)",
            "Lcom/squareup/jail/CogsJailKeeper_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 140
    new-instance v21, Lcom/squareup/jail/CogsJailKeeper_Factory;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/jail/CogsJailKeeper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v21
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/catalogfees/CatalogFeesPreloader;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Ljava/util/Set;Lcom/squareup/pushmessages/PushMessageDelegate;)Lcom/squareup/jail/CogsJailKeeper;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Ldagger/Lazy<",
            "Lcom/squareup/LoggedInScopeNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/catalogfees/CatalogFeesPreloader;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/tickets/voidcomp/CompDiscountsCache;",
            "Lcom/squareup/tickets/voidcomp/VoidReasonsCache;",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            "Lcom/squareup/tickets/TicketCountsCache;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxCallbacks;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ")",
            "Lcom/squareup/jail/CogsJailKeeper;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    .line 153
    new-instance v21, Lcom/squareup/jail/CogsJailKeeper;

    move-object/from16 v0, v21

    invoke-direct/range {v0 .. v20}, Lcom/squareup/jail/CogsJailKeeper;-><init>(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/catalogfees/CatalogFeesPreloader;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Ljava/util/Set;Lcom/squareup/pushmessages/PushMessageDelegate;)V

    return-object v21
.end method


# virtual methods
.method public get()Lcom/squareup/jail/CogsJailKeeper;
    .locals 22

    move-object/from16 v0, p0

    .line 120
    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/badbus/BadEventSink;

    iget-object v3, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->loggedInScopeNotifierProvider:Ljavax/inject/Provider;

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v6, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v7, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->catalogFeesPreloaderProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/catalogfees/CatalogFeesPreloader;

    iget-object v8, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v9, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->cashDrawersProvider:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object v10, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v11, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/ui/main/DiningOptionCache;

    iget-object v12, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v13, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->compDiscountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v13}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/squareup/tickets/voidcomp/CompDiscountsCache;

    iget-object v14, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->voidReasonsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v14}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/squareup/tickets/voidcomp/VoidReasonsCache;

    iget-object v15, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->ticketGroupsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v15}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/squareup/tickets/TicketGroupsCache;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/tickets/TicketCountsCache;

    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->callbacksProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->additionalServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Ljava/util/Set;

    iget-object v1, v0, Lcom/squareup/jail/CogsJailKeeper_Factory;->pushMessageDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/pushmessages/PushMessageDelegate;

    move-object/from16 v1, v21

    invoke-static/range {v1 .. v20}, Lcom/squareup/jail/CogsJailKeeper_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/badbus/BadEventSink;Ldagger/Lazy;Ljavax/inject/Provider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/catalogfees/CatalogFeesPreloader;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/main/DiningOptionCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/tickets/voidcomp/CompDiscountsCache;Lcom/squareup/tickets/voidcomp/VoidReasonsCache;Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/tickets/TicketCountsCache;Ljavax/inject/Provider;Lio/reactivex/Scheduler;Ljava/util/Set;Lcom/squareup/pushmessages/PushMessageDelegate;)Lcom/squareup/jail/CogsJailKeeper;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/jail/CogsJailKeeper_Factory;->get()Lcom/squareup/jail/CogsJailKeeper;

    move-result-object v0

    return-object v0
.end method
