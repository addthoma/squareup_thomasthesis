.class public final Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;
.super Ljava/lang/Object;
.source "BasicSalesReportConfigRepository.kt"

# interfaces
.implements Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBasicSalesReportConfigRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BasicSalesReportConfigRepository.kt\ncom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository\n*L\n1#1,50:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;",
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
        "basicSalesReportConfigStore",
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;",
        "defaultReportConfigProvider",
        "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
        "(Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;Lcom/squareup/salesreport/DefaultReportConfigProvider;)V",
        "get",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "set",
        "",
        "reportConfig",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final basicSalesReportConfigStore:Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;

.field private final defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;Lcom/squareup/salesreport/DefaultReportConfigProvider;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "basicSalesReportConfigStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultReportConfigProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->basicSalesReportConfigStore:Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;

    iput-object p2, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/customreport/data/ReportConfig;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->basicSalesReportConfigStore:Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;

    invoke-interface {v0}, Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;->get()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->defaultReportConfigProvider:Lcom/squareup/salesreport/DefaultReportConfigProvider;

    .line 39
    sget-object v1, Lcom/squareup/api/salesreport/DetailLevel$Basic;->INSTANCE:Lcom/squareup/api/salesreport/DetailLevel$Basic;

    check-cast v1, Lcom/squareup/api/salesreport/DetailLevel;

    invoke-interface {v0, v1}, Lcom/squareup/salesreport/DefaultReportConfigProvider;->get(Lcom/squareup/api/salesreport/DetailLevel;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    .line 40
    invoke-virtual {p0, v0}, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->set(Lcom/squareup/customreport/data/ReportConfig;)V

    :goto_0
    return-object v0
.end method

.method public set(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/salesreport/basic/PersistedBasicSalesReportConfigRepository;->basicSalesReportConfigStore:Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;

    invoke-interface {v0, p1}, Lcom/squareup/salesreport/basic/BasicSalesReportConfigStore;->set(Lcom/squareup/customreport/data/ReportConfig;)V

    return-void
.end method
