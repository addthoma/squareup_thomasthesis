.class public final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 EmployeeSelectionCoordinator.kt\ncom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator\n*L\n1#1,1322:1\n173#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0008"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$onClickDebounced$1",
        "com/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$$special$$inlined$nohoRow$1$lambda$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $employeeFilterRow$inlined:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;->$employeeFilterRow$inlined:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;

    iget-object p1, p1, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;

    invoke-static {p1}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;->access$getEmployeeRowClicked$p(Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$bindViews$$inlined$adopt$lambda$1$1;->$employeeFilterRow$inlined:Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$EmployeeFilterRow;

    invoke-virtual {p1, v0}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
