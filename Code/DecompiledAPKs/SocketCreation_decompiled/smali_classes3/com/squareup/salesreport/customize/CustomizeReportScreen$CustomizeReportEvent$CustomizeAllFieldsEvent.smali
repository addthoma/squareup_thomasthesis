.class public abstract Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;
.super Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent;
.source "CustomizeReportScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CustomizeAllFieldsEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$GoBackFromCustomizeReport;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$SaveReportCustomization;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$DatePickerInitialized;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingStartTime;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingEndTime;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ToggleAllDay;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;,
        Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$FilterByEmployee;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\t\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent;",
        "()V",
        "ChooseDateRange",
        "ChooseDeviceSetting",
        "DatePickerInitialized",
        "FilterByEmployee",
        "GoBackFromCustomizeReport",
        "SaveReportCustomization",
        "StartEditingEndTime",
        "StartEditingStartTime",
        "ToggleAllDay",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$GoBackFromCustomizeReport;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$SaveReportCustomization;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$DatePickerInitialized;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingStartTime;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$StartEditingEndTime;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ToggleAllDay;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDeviceSetting;",
        "Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$FilterByEmployee;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;-><init>()V

    return-void
.end method
