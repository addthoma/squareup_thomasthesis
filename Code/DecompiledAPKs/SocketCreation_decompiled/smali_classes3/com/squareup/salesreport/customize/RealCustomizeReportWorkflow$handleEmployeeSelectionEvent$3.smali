.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->handleEmployeeSelectionEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$EmployeeSelectionEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "-",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleEmployeeSelectionEvent$3;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    .line 281
    invoke-virtual {v1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    sget-object v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    move-object v9, v0

    check-cast v9, Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x1bf

    const/4 v13, 0x0

    invoke-static/range {v2 .. v13}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    const/4 v5, 0x6

    .line 280
    invoke-static/range {v1 .. v6}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;->copy$default(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;Lcom/squareup/customreport/data/ReportConfig;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection$LoadedEmployeeSelection;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
