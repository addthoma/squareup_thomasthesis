.class final Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;
.super Lkotlin/jvm/internal/Lambda;
.source "CustomizeReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->handleCustomizeAllFieldsEvent(Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "-",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $customizeAllFieldsEvent:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;

.field final synthetic $state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

.field final synthetic $updatedEndTime:Lorg/threeten/bp/LocalTime;

.field final synthetic $updatedStartTime:Lorg/threeten/bp/LocalTime;

.field final synthetic this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    iput-object p3, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$customizeAllFieldsEvent:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;

    iput-object p4, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$updatedStartTime:Lorg/threeten/bp/LocalTime;

    iput-object p5, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$updatedEndTime:Lorg/threeten/bp/LocalTime;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/customize/CustomizeReportState;",
            "-",
            "Lcom/squareup/salesreport/customize/CustomizeReportResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->this$0:Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;

    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$state:Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-object v2, v0

    check-cast v2, Lcom/squareup/salesreport/customize/CustomizeReportState;

    .line 213
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$customizeAllFieldsEvent:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 214
    iget-object v0, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$customizeAllFieldsEvent:Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent;

    check-cast v0, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;

    invoke-virtual {v0}, Lcom/squareup/salesreport/customize/CustomizeReportScreen$CustomizeReportEvent$CustomizeAllFieldsEvent$ChooseDateRange;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    .line 215
    iget-object v5, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$updatedStartTime:Lorg/threeten/bp/LocalTime;

    .line 216
    iget-object v6, p0, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow$handleCustomizeAllFieldsEvent$5;->$updatedEndTime:Lorg/threeten/bp/LocalTime;

    .line 217
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    move-object v9, v0

    check-cast v9, Lcom/squareup/customreport/data/RangeSelection;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x30

    const/4 v11, 0x0

    .line 212
    invoke-static/range {v1 .. v11}, Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;->copyWithValues$default(Lcom/squareup/salesreport/customize/RealCustomizeReportWorkflow;Lcom/squareup/salesreport/customize/CustomizeReportState;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/customreport/data/RangeSelection;ILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
