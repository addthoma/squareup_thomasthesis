.class public final Lcom/squareup/salesreport/print/SalesReportPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "SalesReportPayload.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;,
        Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;,
        Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;,
        Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;,
        Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;,
        Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0006123456B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\rH\u00c6\u0003JG\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\u0008\u0010$\u001a\u0004\u0018\u00010%H\u00d6\u0003J\u0008\u0010&\u001a\u00020\'H\u0016J\t\u0010(\u001a\u00020)H\u00d6\u0001J\u0018\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020#H\u0016J\t\u0010/\u001a\u000200H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/salesreport/print/SalesReportPayload;",
        "Lcom/squareup/print/PrintablePayload;",
        "headerSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;",
        "salesSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;",
        "paymentsSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;",
        "discountSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;",
        "categorySection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;",
        "itemsSection",
        "Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;",
        "(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V",
        "getCategorySection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;",
        "getDiscountSection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;",
        "getHeaderSection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;",
        "getItemsSection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;",
        "getPaymentsSection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;",
        "getSalesSection",
        "()Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterActionName;",
        "hashCode",
        "",
        "renderBitmap",
        "Landroid/graphics/Bitmap;",
        "thermalBitmapBuilder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "isReprint",
        "toString",
        "",
        "CategorySection",
        "DiscountSection",
        "HeaderSection",
        "ItemSection",
        "PaymentsSection",
        "SalesSummarySection",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

.field private final discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

.field private final headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

.field private final itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

.field private final paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

.field private final salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V
    .locals 1

    const-string v0, "headerSection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentsSection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categorySection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    iput-object p2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    iput-object p3, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    iput-object p4, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    iput-object p5, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    iput-object p6, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/print/SalesReportPayload;Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;ILjava/lang/Object;)Lcom/squareup/salesreport/print/SalesReportPayload;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/salesreport/print/SalesReportPayload;->copy(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)Lcom/squareup/salesreport/print/SalesReportPayload;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    return-object v0
.end method

.method public final component2()Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    return-object v0
.end method

.method public final component3()Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    return-object v0
.end method

.method public final component4()Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    return-object v0
.end method

.method public final component5()Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    return-object v0
.end method

.method public final component6()Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    return-object v0
.end method

.method public final copy(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)Lcom/squareup/salesreport/print/SalesReportPayload;
    .locals 8

    const-string v0, "headerSection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentsSection"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountSection"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "categorySection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/print/SalesReportPayload;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/print/SalesReportPayload;-><init>(Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/print/SalesReportPayload;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/print/SalesReportPayload;

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    iget-object v1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    iget-object v1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    iget-object v1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    iget-object v1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    iget-object v1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    iget-object p1, p1, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_SALES_SUMMARY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public final getCategorySection()Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    return-object v0
.end method

.method public final getDiscountSection()Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    return-object v0
.end method

.method public final getHeaderSection()Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    return-object v0
.end method

.method public final getItemsSection()Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    return-object v0
.end method

.method public final getPaymentsSection()Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    return-object v0
.end method

.method public final getSalesSection()Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    const-string/jumbo p2, "thermalBitmapBuilder"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance p2, Lcom/squareup/salesreport/print/SalesReportRenderer;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/print/SalesReportRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    invoke-virtual {p2, p0}, Lcom/squareup/salesreport/print/SalesReportRenderer;->renderBitmap(Lcom/squareup/salesreport/print/SalesReportPayload;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SalesReportPayload(headerSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->headerSection:Lcom/squareup/salesreport/print/SalesReportPayload$HeaderSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", salesSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->salesSection:Lcom/squareup/salesreport/print/SalesReportPayload$SalesSummarySection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentsSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->paymentsSection:Lcom/squareup/salesreport/print/SalesReportPayload$PaymentsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discountSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->discountSection:Lcom/squareup/salesreport/print/SalesReportPayload$DiscountSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", categorySection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->categorySection:Lcom/squareup/salesreport/print/SalesReportPayload$CategorySection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemsSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesReportPayload;->itemsSection:Lcom/squareup/salesreport/print/SalesReportPayload$ItemSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
