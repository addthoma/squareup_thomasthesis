.class public final Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;
.super Ljava/lang/Object;
.source "SalesPrintFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/salesreport/print/SalesPrintFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/TimeZone;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/salesreport/print/SalesPrintFormatter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/print/SalesPrintFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/print/SalesPrintFormatter;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/salesreport/print/SalesPrintFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/print/SalesPrintFormatter_Factory;->get()Lcom/squareup/salesreport/print/SalesPrintFormatter;

    move-result-object v0

    return-object v0
.end method
