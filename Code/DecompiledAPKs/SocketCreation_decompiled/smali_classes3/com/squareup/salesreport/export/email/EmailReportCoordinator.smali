.class public final Lcom/squareup/salesreport/export/email/EmailReportCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EmailReportCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmailReportCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmailReportCoordinator.kt\ncom/squareup/salesreport/export/email/EmailReportCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,156:1\n1103#2,7:157\n*E\n*S KotlinDebug\n*F\n+ 1 EmailReportCoordinator.kt\ncom/squareup/salesreport/export/email/EmailReportCoordinator\n*L\n124#1,7:157\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001/BW\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\"\u0010\r\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0008\u0012\u0004\u0012\u00020\u0010`\u00120\u000e\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0017H\u0016J\u0010\u0010\'\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0017H\u0002J\u0018\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00102\u0006\u0010&\u001a\u00020\u0017H\u0002J\u0018\u0010*\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00102\u0006\u0010+\u001a\u00020,H\u0002J&\u0010-\u001a\u00020%2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0006\u0010)\u001a\u00020\u00102\u0006\u0010+\u001a\u00020.H\u0002R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R-\u0010\r\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fj\u0008\u0012\u0004\u0012\u00020\u0010`\u00120\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/email/EmailReportCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "res",
        "Lcom/squareup/util/Res;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/export/email/EmailReportScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addressView",
        "Landroid/view/View;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "emailEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "itemSelectedRow",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "getScreens",
        "()Lio/reactivex/Observable;",
        "sendButton",
        "Lcom/squareup/noho/NohoButton;",
        "attach",
        "",
        "view",
        "bindViews",
        "onScreen",
        "screen",
        "showAddressView",
        "state",
        "Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;",
        "showMessageView",
        "Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addressView:Landroid/view/View;

.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private emailEditText:Lcom/squareup/noho/NohoEditText;

.field private itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

.field private final localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private messageView:Lcom/squareup/noho/NohoMessageView;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private sendButton:Lcom/squareup/noho/NohoButton;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object p3, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    iput-object p6, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getEmailEditText$p(Lcom/squareup/salesreport/export/email/EmailReportCoordinator;)Lcom/squareup/noho/NohoEditText;
    .locals 1

    .line 37
    iget-object p0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez p0, :cond_0

    const-string v0, "emailEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/salesreport/export/email/EmailReportCoordinator;Lcom/squareup/salesreport/export/email/EmailReportScreen;Landroid/view/View;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->onScreen(Lcom/squareup/salesreport/export/email/EmailReportScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$setEmailEditText$p(Lcom/squareup/salesreport/export/email/EmailReportCoordinator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 84
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 85
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_report_address_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.email_report_address_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->addressView:Landroid/view/View;

    .line 86
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_edit:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.email_edit)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    .line 87
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_items_selected:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.email_items_selected)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 88
    sget v0, Lcom/squareup/salesreport/impl/R$id;->send_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.send_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    .line 89
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_report_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.email_report_message_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 91
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_report_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.email_report_animator)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 92
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p1, :cond_0

    const-string v0, "animator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v0, Lcom/squareup/salesreport/impl/R$id;->email_report_address_view:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method private final onScreen(Lcom/squareup/salesreport/export/email/EmailReportScreen;Landroid/view/View;)V
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->itemSelectedRow:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    const-string v1, "itemSelectedRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/salesreport/export/email/EmailReportScreen;->getIncludeItems()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 101
    new-instance v1, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$$inlined$run$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$$inlined$run$lambda$1;-><init>(Lcom/squareup/salesreport/export/email/EmailReportScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 104
    new-instance v0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$2;

    invoke-direct {v0, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$2;-><init>(Lcom/squareup/salesreport/export/email/EmailReportScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 108
    iget-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p2, :cond_1

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 105
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 106
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/salesreport/impl/R$string;->email_report_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 107
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$3;

    invoke-direct {v2, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$onScreen$3;-><init>(Lcom/squareup/salesreport/export/email/EmailReportScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/salesreport/export/email/EmailReportScreen;->getState()Lcom/squareup/salesreport/export/ExportReportState;

    move-result-object p2

    .line 111
    instance-of v0, p2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->showAddressView(Lcom/squareup/salesreport/export/email/EmailReportScreen;Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;)V

    goto :goto_0

    .line 112
    :cond_2
    instance-of v0, p2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->localeProvider:Ljavax/inject/Provider;

    check-cast p2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->showMessageView(Ljavax/inject/Provider;Lcom/squareup/salesreport/export/email/EmailReportScreen;Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final showAddressView(Lcom/squareup/salesreport/export/email/EmailReportScreen;Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;)V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/salesreport/impl/R$id;->email_report_address_view:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->emailEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_1

    const-string v1, "emailEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/salesreport/export/email/EmailReportScreen;->getEmail()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    const-string v1, "sendButton"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 123
    invoke-virtual {p2}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    if-eqz p2, :cond_4

    .line 124
    iget-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    if-nez p2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p2, Landroid/view/View;

    .line 157
    new-instance v0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showAddressView$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showAddressView$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/salesreport/export/email/EmailReportCoordinator;Lcom/squareup/salesreport/export/email/EmailReportScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 129
    :cond_4
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->sendButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget-object p2, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showAddressView$2;->INSTANCE:Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showAddressView$2;

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private final showMessageView(Ljavax/inject/Provider;Lcom/squareup/salesreport/export/email/EmailReportScreen;Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/export/email/EmailReportScreen;",
            "Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;",
            ")V"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/salesreport/impl/R$id;->email_report_message_view:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 140
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "messageView"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 141
    :cond_1
    new-instance v2, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showMessageView$1;

    invoke-direct {v2, p2}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$showMessageView$1;-><init>(Lcom/squareup/salesreport/export/email/EmailReportScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    const-string v2, "Debouncers.debounceRunnable { screen.onClose() }"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    .line 144
    iget-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p2}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result p2

    if-eqz p2, :cond_5

    .line 145
    iget-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v0, Lcom/squareup/salesreport/impl/R$string;->email_report_sent_message_title:I

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 147
    iget-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p3}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iget-object v2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p3, v0, v2, p1}, Lcom/squareup/salesreport/util/ReportConfigsKt;->title(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget p2, Lcom/squareup/vectoricons/R$drawable;->circle_check_96:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    goto :goto_0

    .line 150
    :cond_5
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget p2, Lcom/squareup/salesreport/impl/R$string;->email_report_connect_message_title:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 151
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    sget p2, Lcom/squareup/salesreport/impl/R$string;->email_report_connect_message:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 152
    iget-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    sget p2, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->bindViews(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->screens:Lio/reactivex/Observable;

    .line 79
    iget-object v1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v1, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$attach$1;-><init>(Lcom/squareup/salesreport/export/email/EmailReportCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final getScreens()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;->screens:Lio/reactivex/Observable;

    return-object v0
.end method
