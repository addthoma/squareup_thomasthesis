.class public final Lcom/squareup/salesreport/export/RealExportReportWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lcom/squareup/salesreport/export/ExportReportWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;,
        Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/salesreport/export/ExportReportProps;",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/salesreport/export/ExportReportWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nExportReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ExportReportWorkflow.kt\ncom/squareup/salesreport/export/RealExportReportWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,274:1\n149#2,5:275\n149#2,5:280\n149#2,5:285\n149#2,5:290\n*E\n*S KotlinDebug\n*F\n+ 1 ExportReportWorkflow.kt\ncom/squareup/salesreport/export/RealExportReportWorkflow\n*L\n97#1,5:275\n125#1,5:280\n140#1,5:285\n166#1,5:290\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0002-.BU\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u000e\u0008\u0001\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018\u0012\n\u0008\u0001\u0010\u001a\u001a\u0004\u0018\u00010\u001b\u00a2\u0006\u0002\u0010\u001cJ \u0010\u001d\u001a\u00020\u00052\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u00192\u0006\u0010!\u001a\u00020\"H\u0002J\u001a\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020\u00032\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0016JN\u0010(\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010%\u001a\u00020\u00032\u0006\u0010)\u001a\u00020\u00042\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050+H\u0016J\u0010\u0010,\u001a\u00020\'2\u0006\u0010)\u001a\u00020\u0004H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow;",
        "Lcom/squareup/salesreport/export/ExportReportWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/salesreport/export/ExportReportProps;",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "salesReportPrintPayloadFactory",
        "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
        "printerDispatcher",
        "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
        "salesReportAnalytics",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "taskQueue",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "includeItemsInReportLocalSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "deviceName",
        "",
        "(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Ljava/lang/String;)V",
        "emailReport",
        "email",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;",
        "includeItems",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "initialState",
        "Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "Email",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

.field private deviceName:Ljava/lang/String;

.field private final includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

.field private final salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

.field private final salesReportPrintPayloadFactory:Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method public constructor <init>(Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lcom/squareup/salesreport/analytics/SalesReportAnalytics;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Ljava/lang/String;)V
    .locals 1
    .param p5    # Lcom/squareup/queue/retrofit/RetrofitQueue;
        .annotation runtime Lcom/squareup/queue/Tasks;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/salesreport/export/IncludeItemsInReport;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/http/useragent/DeviceNamePII;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
            "Lcom/squareup/time/CurrentTimeZone;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "salesReportPrintPayloadFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerDispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesReportAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTimeZone"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskQueue"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "includeItemsInReportLocalSetting"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->salesReportPrintPayloadFactory:Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    iput-object p3, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    iput-object p4, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    iput-object p5, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p6, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p7, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p8, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->deviceName:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$emailReport(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;ZLcom/squareup/customreport/data/ReportConfig;)V
    .locals 0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->emailReport(Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;ZLcom/squareup/customreport/data/ReportConfig;)V

    return-void
.end method

.method public static final synthetic access$getAccountStatusSettings$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method public static final synthetic access$getIncludeItemsInReportLocalSetting$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method

.method public static final synthetic access$getPrinterDispatcher$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    return-object p0
.end method

.method public static final synthetic access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->salesReportAnalytics:Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getSalesReportPrintPayloadFactory$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->salesReportPrintPayloadFactory:Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    return-object p0
.end method

.method private final emailReport(Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;ZLcom/squareup/customreport/data/ReportConfig;)V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 178
    invoke-static {p3, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-static {p3, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object p3

    .line 180
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v2, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;

    invoke-virtual {p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;->getEmail()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v1, p3, p1, p2}, Lcom/squareup/reports/applet/ui/report/sales/SalesReportEmail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v0, v2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;
    .locals 1

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    new-instance p2, Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;

    invoke-virtual {p1}, Lcom/squareup/salesreport/export/ExportReportProps;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/export/ExportReportProps;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;-><init>(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;)V

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/salesreport/export/ExportReportProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->initialState(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/salesreport/export/ExportReportProps;

    check-cast p2, Lcom/squareup/salesreport/export/ExportReportState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/export/ExportReportProps;",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "props"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "state"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v4

    .line 81
    instance-of v5, v2, Lcom/squareup/salesreport/export/ExportReportState$SelectingExportAction;

    const/4 v8, 0x0

    const-string v9, ""

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    if-eqz v5, :cond_0

    .line 82
    iget-object v2, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->printerDispatcher:Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    invoke-interface {v2}, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;->canPrintSalesReport()Z

    move-result v2

    .line 83
    new-instance v3, Lcom/squareup/salesreport/export/ExportReportDialogScreen;

    .line 84
    new-instance v5, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$1;

    invoke-direct {v5, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 85
    new-instance v6, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;

    invoke-direct {v6, p0, p1, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$2;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Sink;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 89
    new-instance v7, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$3;

    invoke-direct {v7, p0, v2, p1, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$dialog$3;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;ZLcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Sink;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 83
    invoke-direct {v3, v5, v6, v7, v2}, Lcom/squareup/salesreport/export/ExportReportDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Z)V

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 276
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 277
    const-class v2, Lcom/squareup/salesreport/export/ExportReportDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 278
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 276
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 98
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v3, v10, [Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_1

    .line 100
    :cond_0
    instance-of v5, v2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    const-string v7, "ExportReportWorkflow.email"

    const-string v11, "includeItemsInReportLocalSetting.get(true)"

    if-eqz v5, :cond_1

    .line 101
    new-instance v12, Lcom/squareup/salesreport/export/email/EmailReportScreen;

    .line 103
    iget-object v5, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v5, v6}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 104
    new-instance v6, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$1;

    invoke-direct {v6, p0, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$1;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 107
    move-object v11, v2

    check-cast v11, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;

    invoke-virtual {v11}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportStart;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email;->getEmail()Ljava/lang/String;

    move-result-object v11

    new-instance v13, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;

    invoke-direct {v13, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$2;-><init>(Lcom/squareup/salesreport/export/ExportReportProps;)V

    check-cast v13, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v11, v7, v13}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v7

    .line 110
    new-instance v3, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$3;

    invoke-direct {v3, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v11, v3

    check-cast v11, Lkotlin/jvm/functions/Function0;

    .line 111
    new-instance v3, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;

    invoke-direct {v3, p0, v2, p1, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$4;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/workflow/Sink;)V

    move-object v13, v3

    check-cast v13, Lkotlin/jvm/functions/Function0;

    move-object v1, v12

    move-object/from16 v2, p2

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v11

    move-object v7, v13

    .line 101
    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/export/email/EmailReportScreen;-><init>(Lcom/squareup/salesreport/export/ExportReportState;ZLkotlin/jvm/functions/Function1;Lcom/squareup/workflow/text/WorkflowEditableText;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v12, Lcom/squareup/workflow/legacy/V2Screen;

    .line 281
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 282
    const-class v2, Lcom/squareup/salesreport/export/email/EmailReportScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 283
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 281
    invoke-direct {v1, v2, v12, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 126
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v3, v10, [Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_1

    .line 128
    :cond_1
    instance-of v5, v2, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;

    if-eqz v5, :cond_2

    .line 129
    new-instance v12, Lcom/squareup/salesreport/export/email/EmailReportScreen;

    .line 131
    iget-object v5, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v5, v6}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 132
    new-instance v6, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;

    invoke-direct {v6, p0, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 135
    move-object v11, v2

    check-cast v11, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;

    invoke-virtual {v11}, Lcom/squareup/salesreport/export/ExportReportState$EmailingReportComplete;->getEmail()Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;

    move-result-object v11

    invoke-virtual {v11}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Email$ValidEmail;->getEmail()Ljava/lang/String;

    move-result-object v11

    new-instance v13, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$6;

    invoke-direct {v13, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$6;-><init>(Lcom/squareup/salesreport/export/ExportReportProps;)V

    check-cast v13, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v11, v7, v13}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v7

    .line 138
    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$7;

    invoke-direct {v1, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$7;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function0;

    .line 139
    sget-object v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$8;->INSTANCE:Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$8;

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function0;

    move-object v1, v12

    move-object/from16 v2, p2

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v11

    move-object v7, v13

    .line 129
    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/export/email/EmailReportScreen;-><init>(Lcom/squareup/salesreport/export/ExportReportState;ZLkotlin/jvm/functions/Function1;Lcom/squareup/workflow/text/WorkflowEditableText;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v12, Lcom/squareup/workflow/legacy/V2Screen;

    .line 286
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 287
    const-class v2, Lcom/squareup/salesreport/export/email/EmailReportScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 288
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 286
    invoke-direct {v1, v2, v12, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 141
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v3, v10, [Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    goto :goto_1

    .line 143
    :cond_2
    instance-of v1, v2, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportStart;

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    instance-of v1, v2, Lcom/squareup/salesreport/export/ExportReportState$PrintingReportComplete;

    if-eqz v1, :cond_4

    .line 144
    :goto_0
    new-instance v12, Lcom/squareup/salesreport/export/print/PrintReportScreen;

    .line 146
    iget-object v1, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->includeItemsInReportLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v6}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 147
    iget-object v5, v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->deviceName:Ljava/lang/String;

    .line 148
    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$9;

    invoke-direct {v1, p0, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$9;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 151
    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$10;

    invoke-direct {v1, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$10;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 152
    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;

    invoke-direct {v1, p0, v2, v4}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$11;-><init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/Sink;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function2;

    move-object v1, v12

    move-object/from16 v2, p2

    move-object v4, v5

    move-object v5, v6

    move-object v6, v11

    .line 144
    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/export/print/PrintReportScreen;-><init>(Lcom/squareup/salesreport/export/ExportReportState;ZLjava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V

    check-cast v12, Lcom/squareup/workflow/legacy/V2Screen;

    .line 291
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 292
    const-class v2, Lcom/squareup/salesreport/export/print/PrintReportScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 293
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 291
    invoke-direct {v1, v2, v12, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 167
    sget-object v2, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v3, v10, [Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {v2, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_4
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/salesreport/export/ExportReportState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/salesreport/export/ExportReportState;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->snapshotState(Lcom/squareup/salesreport/export/ExportReportState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
