.class public final Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;
.super Ljava/lang/Object;
.source "GeneratedDetailedSalesReportConfigRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/DefaultReportConfigProvider;",
            ">;)",
            "Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/DefaultReportConfigProvider;)Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;-><init>(Lcom/squareup/salesreport/DefaultReportConfigProvider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/DefaultReportConfigProvider;

    invoke-static {v0}, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;->newInstance(Lcom/squareup/salesreport/DefaultReportConfigProvider;)Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository_Factory;->get()Lcom/squareup/salesreport/detailed/GeneratedDetailedSalesReportConfigRepository;

    move-result-object v0

    return-object v0
.end method
