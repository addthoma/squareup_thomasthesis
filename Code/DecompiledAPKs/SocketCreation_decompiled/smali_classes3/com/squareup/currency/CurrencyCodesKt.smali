.class public final Lcom/squareup/currency/CurrencyCodesKt;
.super Ljava/lang/Object;
.source "CurrencyCodes.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "doesSwedishRounding",
        "",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getDoesSwedishRounding",
        "(Lcom/squareup/protos/common/CurrencyCode;)Z",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getDoesSwedishRounding(Lcom/squareup/protos/common/CurrencyCode;)Z
    .locals 1

    const-string v0, "$this$doesSwedishRounding"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getSwedishRoundingInterval(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    const/4 v0, 0x1

    if-le p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
