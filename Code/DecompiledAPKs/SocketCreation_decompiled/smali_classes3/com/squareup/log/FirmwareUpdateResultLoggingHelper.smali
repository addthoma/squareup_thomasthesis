.class public Lcom/squareup/log/FirmwareUpdateResultLoggingHelper;
.super Ljava/lang/Object;
.source "FirmwareUpdateResultLoggingHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDescription(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)Ljava/lang/String;
    .locals 3

    .line 8
    sget-object v0, Lcom/squareup/log/FirmwareUpdateResultLoggingHelper$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsFirmwareUpdateResult:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 48
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const-string p0, "Unknown firmware update result: %s"

    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_0
    const-string p0, "None"

    return-object p0

    :pswitch_1
    const-string p0, "Invalid image version"

    return-object p0

    :pswitch_2
    const-string p0, "Unknown error"

    return-object p0

    :pswitch_3
    const-string p0, "Attempting to update a production unit with a plaintext image"

    return-object p0

    :pswitch_4
    const-string p0, "Duplicate update to slot"

    return-object p0

    :pswitch_5
    const-string p0, "Bad encryption key"

    return-object p0

    :pswitch_6
    const-string p0, "Bad write alignment"

    return-object p0

    :pswitch_7
    const-string p0, "Bad header"

    return-object p0

    :pswitch_8
    const-string p0, "Bad argument"

    return-object p0

    :pswitch_9
    const-string p0, "Flash failure"

    return-object p0

    :pswitch_a
    const-string p0, "Decryption failure"

    return-object p0

    :pswitch_b
    const-string p0, "Invalid image"

    return-object p0

    :pswitch_c
    const-string p0, "Invalid image header"

    return-object p0

    :pswitch_d
    const-string p0, "Not initialized"

    return-object p0

    :pswitch_e
    const-string p0, "Send data; Invalid image; Will retry"

    return-object p0

    :pswitch_f
    const-string p0, "Send data; Invalid image"

    return-object p0

    :pswitch_10
    const-string p0, "Send data; Invalid image heade"

    return-object p0

    :pswitch_11
    const-string p0, "Send data"

    return-object p0

    :pswitch_12
    const-string p0, "Success"

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
