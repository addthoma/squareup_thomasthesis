.class public Lcom/squareup/log/tickets/TicketSelectedToCartUi;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "TicketSelectedToCartUi.java"


# instance fields
.field private final duration_ms:I

.field private final ticket_client_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->OPEN_TICKETS_TICKET_SELECTED_TO_CART_UI:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 14
    iput p1, p0, Lcom/squareup/log/tickets/TicketSelectedToCartUi;->duration_ms:I

    .line 15
    iput-object p2, p0, Lcom/squareup/log/tickets/TicketSelectedToCartUi;->ticket_client_id:Ljava/lang/String;

    return-void
.end method
