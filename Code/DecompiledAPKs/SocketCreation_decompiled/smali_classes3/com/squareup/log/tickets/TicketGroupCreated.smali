.class public Lcom/squareup/log/tickets/TicketGroupCreated;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "TicketGroupCreated.java"


# instance fields
.field public final is_automatic_name:Z

.field public final template_count:I

.field public final ticket_group_name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKET_GROUP_CREATED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 15
    iput-object p1, p0, Lcom/squareup/log/tickets/TicketGroupCreated;->ticket_group_name:Ljava/lang/String;

    .line 16
    iput-boolean p2, p0, Lcom/squareup/log/tickets/TicketGroupCreated;->is_automatic_name:Z

    .line 17
    iput p3, p0, Lcom/squareup/log/tickets/TicketGroupCreated;->template_count:I

    return-void
.end method
