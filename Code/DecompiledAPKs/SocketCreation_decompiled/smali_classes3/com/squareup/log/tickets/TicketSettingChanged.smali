.class public Lcom/squareup/log/tickets/TicketSettingChanged;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "TicketSettingChanged.java"


# instance fields
.field public final enabled:Z


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 16
    iput-boolean p2, p0, Lcom/squareup/log/tickets/TicketSettingChanged;->enabled:Z

    return-void
.end method

.method public static openTicketsAsHomeScreenEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/log/tickets/TicketSettingChanged;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_AS_HOME_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/log/tickets/TicketSettingChanged;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-object v0
.end method

.method public static openTicketsEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;
    .locals 2

    .line 20
    new-instance v0, Lcom/squareup/log/tickets/TicketSettingChanged;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/log/tickets/TicketSettingChanged;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-object v0
.end method

.method public static predefinedTicketsEnabled(Z)Lcom/squareup/log/tickets/TicketSettingChanged;
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/log/tickets/TicketSettingChanged;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->PREDEFINED_TICKETS_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, p0}, Lcom/squareup/log/tickets/TicketSettingChanged;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-object v0
.end method
