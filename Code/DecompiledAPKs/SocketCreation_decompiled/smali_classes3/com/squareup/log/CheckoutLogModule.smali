.class public abstract Lcom/squareup/log/CheckoutLogModule;
.super Ljava/lang/Object;
.source "CheckoutLogModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideCheckoutInformationEventLogger(Lcom/squareup/log/RealCheckoutInformationEventLogger;)Lcom/squareup/log/CheckoutInformationEventLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
