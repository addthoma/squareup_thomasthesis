.class public Lcom/squareup/log/EventStreamCommonCrashLogger;
.super Ljava/lang/Object;
.source "EventStreamCommonCrashLogger.java"

# interfaces
.implements Lcom/squareup/log/CrashAdditionalLogger;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/log/EventStreamCommonCrashLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public addLogs(Ljava/lang/Throwable;)V
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/log/EventStreamCommonCrashLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/analytics/event/v1/CrashEvent;->posJavaCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logCrashSync(Lcom/squareup/analytics/event/v1/CrashEvent;)V

    return-void
.end method
