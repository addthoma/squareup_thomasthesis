.class public Lcom/squareup/log/RegisterExceptionHandler;
.super Ljava/lang/Object;
.source "RegisterExceptionHandler.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/RegisterExceptionHandler$Deps;,
        Lcom/squareup/log/RegisterExceptionHandler$Component;
    }
.end annotation


# static fields
.field private static final NO_WIRE_ADAPTER_PREFIX:Ljava/lang/String; = "failed to access "

.field private static final NO_WIRE_ADAPTER_SUFFIX:Ljava/lang/String; = "#ADAPTER"


# instance fields
.field private final application:Landroid/app/Application;

.field private final buildVersionName:Ljava/lang/String;

.field private final crashReporter:Lcom/squareup/log/CrashReporter;

.field private final defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

.field private final playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

.field private final posBuild:Lcom/squareup/util/PosBuild;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Thread$UncaughtExceptionHandler;Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler;->buildVersionName:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/log/RegisterExceptionHandler;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 88
    iput-object p3, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    .line 89
    iput-object p4, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    .line 91
    invoke-virtual {p3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    const-string p1, "notification"

    .line 93
    invoke-virtual {p3, p1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/NotificationManager;

    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationManager:Landroid/app/NotificationManager;

    .line 94
    iput-object p5, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    .line 95
    iput-object p6, p0, Lcom/squareup/log/RegisterExceptionHandler;->posBuild:Lcom/squareup/util/PosBuild;

    .line 96
    iput-object p7, p0, Lcom/squareup/log/RegisterExceptionHandler;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    return-void
.end method

.method private buildSecondaryDexLog(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 7

    .line 312
    new-instance v0, Ljava/io/File;

    iget-object p1, p1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "code_cache"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "secondary-dexes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 317
    array-length v0, p1

    const/4 v1, 0x0

    const-string v2, ""

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v3, p1, v1

    .line 318
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " lastModified: "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/util/Date;

    .line 319
    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-direct {v2, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 322
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dexDir does not exist or is not a directory "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 325
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_1
    return-object v2
.end method

.method private buildSourceApkLog(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    .locals 4

    .line 306
    new-instance v0, Ljava/io/File;

    iget-object p1, p1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 307
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " lastModified: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/util/Date;

    .line 308
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private createNoClassDefFoundError()Ljava/lang/NoClassDefFoundError;
    .locals 7

    .line 350
    new-instance v0, Ljava/lang/NoClassDefFoundError;

    const-string v1, "RA-13706 Classloading failed on startup."

    invoke-direct {v0, v1}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 354
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/StackTraceElement;

    .line 356
    new-instance v4, Ljava/lang/StackTraceElement;

    const-string v5, "pleaseFileToRA13706"

    const/16 v6, 0x2a

    invoke-direct {v4, v1, v5, v2, v6}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v1, 0x0

    aput-object v4, v3, v1

    .line 357
    invoke-virtual {v0, v3}, Ljava/lang/NoClassDefFoundError;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-object v0
.end method

.method private createNoSuchFieldException()Ljava/lang/NoSuchFieldException;
    .locals 7

    .line 363
    new-instance v0, Ljava/lang/NoSuchFieldException;

    const-string v1, "RA-11567 Could not find Response ADAPTER field"

    invoke-direct {v0, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    .line 365
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 367
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/StackTraceElement;

    .line 369
    new-instance v4, Ljava/lang/StackTraceElement;

    const-string v5, "pleaseFileToRA11567"

    const/16 v6, 0xd

    invoke-direct {v4, v1, v5, v2, v6}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v1, 0x0

    aput-object v4, v3, v1

    .line 370
    invoke-virtual {v0, v3}, Ljava/lang/NoSuchFieldException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-object v0
.end method

.method private createOutOfMemoryError()Ljava/lang/OutOfMemoryError;
    .locals 7

    .line 336
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Register ran out of memory. bugsnag/misc/realStacktrace has a trace, but there\'s probably a leak elsewhere. Check local LeakCanary."

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 341
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/StackTraceElement;

    .line 343
    new-instance v4, Ljava/lang/StackTraceElement;

    const-string v5, "flyCanaryFly"

    const v6, 0xdead

    invoke-direct {v4, v1, v5, v2, v6}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v1, 0x0

    aput-object v4, v3, v1

    .line 344
    invoke-virtual {v0, v3}, Ljava/lang/OutOfMemoryError;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-object v0
.end method

.method private static findErrnoExceptionWithCode(Ljava/lang/Throwable;Ljava/lang/String;)Z
    .locals 2

    :goto_0
    if-eqz p0, :cond_1

    .line 397
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ErrnoException"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    .line 401
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static findNoAdapterClass(Ljava/lang/Throwable;)Ljava/lang/Class;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    :goto_0
    if-eqz p0, :cond_1

    .line 422
    instance-of v0, p0, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "failed to access "

    .line 424
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "#ADAPTER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 p0, 0x11

    .line 427
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x8

    .line 426
    invoke-virtual {v0, p0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 429
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v1

    .line 431
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find class ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "] in ["

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "]"

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, p0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 436
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    .line 438
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not parse error message"

    invoke-direct {v0, v1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private injected()Z
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static isNoWireAdapterField(Ljava/lang/Throwable;)Z
    .locals 2

    :goto_0
    if-eqz p0, :cond_1

    .line 408
    instance-of v0, p0, Ljava/lang/IllegalArgumentException;

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "failed to access "

    .line 410
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "#ADAPTER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x1

    return p0

    .line 415
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static isOutOfDiskSpace(Ljava/lang/Throwable;)Z
    .locals 1

    const-string v0, "ENOSPC"

    .line 383
    invoke-static {p0, v0}, Lcom/squareup/log/RegisterExceptionHandler;->findErrnoExceptionWithCode(Ljava/lang/Throwable;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private static isReadOnlyFileSystem(Ljava/lang/Throwable;)Z
    .locals 1

    const-string v0, "EROFS"

    .line 391
    invoke-static {p0, v0}, Lcom/squareup/log/RegisterExceptionHandler;->findErrnoExceptionWithCode(Ljava/lang/Throwable;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private isRestartAfterCrashEnabled()Z
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

    iget-object v0, v0, Lcom/squareup/log/RegisterExceptionHandler$Deps;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    invoke-virtual {v0}, Lcom/squareup/log/FeatureFlagsForLogs;->enabledFeatures()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RESTART_APP_AFTER_CRASH"

    .line 159
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private reportNoClassDefFoundError(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 8

    .line 191
    :try_start_0
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "runningCodeAppVersion"

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->buildVersionName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 194
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 195
    iget-object v4, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v5, "systemAppVersion"

    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v4, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v5, "resourcesNull"

    iget-object v6, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    .line 200
    invoke-virtual {v6}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v7, 0x1

    if-nez v6, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    .line 199
    invoke-interface {v4, v5, v6}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 206
    invoke-direct {p0, v0}, Lcom/squareup/log/RegisterExceptionHandler;->buildSourceApkLog(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v1

    .line 207
    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v4, "sourceApk"

    invoke-interface {v2, v4, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-direct {p0, v0}, Lcom/squareup/log/RegisterExceptionHandler;->buildSecondaryDexLog(Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v2, "secondaryDexes"

    invoke-interface {v1, v2, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    const-string v1, "multidex.version"

    const/4 v2, 0x4

    .line 215
    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "dex.number"

    .line 216
    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 217
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string/jumbo v2, "totalDexNumber"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v2, "appClassLoader"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "appLastUpdateTime"

    new-instance v2, Ljava/util/Date;

    iget-wide v3, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 227
    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    .line 226
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "realStacktrace"

    invoke-static {p1}, Lcom/squareup/util/Logs;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->createNoClassDefFoundError()Ljava/lang/NoClassDefFoundError;

    move-result-object p1

    .line 233
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    .line 237
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    :goto_1
    return-void
.end method

.method private reportNoWireAdapterField(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 12

    const-string v0, "hasDeclaredAdapterField"

    const-string/jumbo v1, "true"

    const-string v2, "false"

    const-string v3, "hasAdapterField"

    const-string v4, "ADAPTER"

    .line 246
    :try_start_0
    invoke-static {p1}, Lcom/squareup/log/RegisterExceptionHandler;->findNoAdapterClass(Ljava/lang/Throwable;)Ljava/lang/Class;

    move-result-object v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :try_start_1
    invoke-virtual {v5, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 250
    iget-object v6, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v6, v3, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    :catch_0
    :try_start_2
    iget-object v6, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v6, v3, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 256
    :goto_0
    :try_start_3
    invoke-virtual {v5, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 257
    iget-object v3, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v3, v0, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 259
    :catch_1
    :try_start_4
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v1, v0, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :goto_1
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "classLoader"

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "classHashcode"

    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v5}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v1, "\n"

    const/4 v2, 0x0

    const-string v3, "null"

    const-string v4, "declaredFields"

    const-string v6, " "

    if-eqz v0, :cond_1

    .line 267
    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    array-length v8, v0

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v8, :cond_0

    aget-object v10, v0, v9

    .line 269
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v11

    invoke-static {v11}, Ljava/lang/reflect/Modifier;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    invoke-virtual {v10}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 274
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0, v4, v7}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v4, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_3
    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v4, "fields"

    if-eqz v0, :cond_3

    .line 282
    :try_start_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    array-length v5, v0

    :goto_4
    if-ge v2, v5, :cond_2

    aget-object v7, v0, v2

    .line 284
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v8

    invoke-static {v8}, Ljava/lang/reflect/Modifier;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, v4, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_5
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "realStacktrace"

    invoke-static {p1}, Lcom/squareup/util/Logs;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->createNoSuchFieldException()Ljava/lang/NoSuchFieldException;

    move-result-object p1

    .line 299
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_6

    :catchall_0
    move-exception p1

    .line 301
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    :goto_6
    return-void
.end method

.method private reportOom(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    .locals 2

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "realStacktrace"

    invoke-static {p1}, Lcom/squareup/util/Logs;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->createOutOfMemoryError()Ljava/lang/OutOfMemoryError;

    move-result-object p1

    .line 167
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 170
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    :goto_0
    return-void
.end method

.method private showOutOfDiskSpaceNotification()V
    .locals 4

    .line 442
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/crashreporting/R$string;->out_of_disk_space_content:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 443
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    sget-object v3, Lcom/squareup/notification/Channels;->APP_STORAGE_ERRORS:Lcom/squareup/notification/Channels;

    .line 444
    invoke-virtual {v1, v2, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x2

    .line 445
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/notification/R$drawable;->notification_square:I

    .line 446
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/crashreporting/R$string;->out_of_disk_space_title:I

    .line 447
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/crashreporting/R$string;->out_of_disk_space_title:I

    .line 448
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 449
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v2}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 450
    invoke-virtual {v2, v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 451
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/crashreporting/R$id;->notification_out_of_disk_space:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private showReadOnlyStorageNotification()V
    .locals 4

    .line 456
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/crashreporting/R$string;->read_only_storage_content:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    sget-object v3, Lcom/squareup/notification/Channels;->APP_STORAGE_ERRORS:Lcom/squareup/notification/Channels;

    .line 458
    invoke-virtual {v1, v2, v3}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    const/4 v2, 0x2

    .line 459
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/notification/R$drawable;->notification_square:I

    .line 460
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setSmallIcon(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/crashreporting/R$string;->read_only_storage_title:I

    .line 461
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->resources:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/crashreporting/R$string;->read_only_storage_title:I

    .line 462
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 463
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    new-instance v2, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v2}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 464
    invoke-virtual {v2, v0}, Landroidx/core/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$BigTextStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 465
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 466
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/crashreporting/R$id;->notification_read_only_storage:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public resolveDependencies(Lcom/squareup/log/RegisterExceptionHandler$Component;)V
    .locals 0

    .line 100
    invoke-interface {p1}, Lcom/squareup/log/RegisterExceptionHandler$Component;->exceptionHandlerDependencies()Lcom/squareup/log/RegisterExceptionHandler$Deps;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler;->deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5

    const/4 v0, 0x0

    .line 105
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->injected()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "isInjected"

    if-eqz v1, :cond_1

    .line 106
    :try_start_1
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

    iget-object v1, v1, Lcom/squareup/log/RegisterExceptionHandler$Deps;->anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-virtual {v1}, Lcom/squareup/anrchaperone/AnrChaperone;->stop()V

    .line 107
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->deps:Lcom/squareup/log/RegisterExceptionHandler$Deps;

    iget-object v1, v1, Lcom/squareup/log/RegisterExceptionHandler$Deps;->additionalLoggers:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/log/CrashAdditionalLogger;

    .line 108
    invoke-interface {v3, p2}, Lcom/squareup/log/CrashAdditionalLogger;->addLogs(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string/jumbo v3, "true"

    invoke-interface {v1, v2, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v3, "false"

    invoke-interface {v1, v2, v3}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->application:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object v3, p0, Lcom/squareup/log/RegisterExceptionHandler;->posBuild:Lcom/squareup/util/PosBuild;

    iget-object v4, p0, Lcom/squareup/log/RegisterExceptionHandler;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/log/CrashReportingLogger;->logNonInjectedInfo(Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    .line 119
    :goto_1
    invoke-static {p2}, Lcom/squareup/log/RegisterExceptionHandler;->isOutOfDiskSpace(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 121
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->showOutOfDiskSpaceNotification()V

    .line 122
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v1, p2}, Lcom/squareup/log/CrashReporter;->warningThrowable(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 123
    :cond_2
    invoke-static {p2}, Lcom/squareup/log/RegisterExceptionHandler;->isReadOnlyFileSystem(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->showReadOnlyStorageNotification()V

    .line 126
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v1, p2}, Lcom/squareup/log/CrashReporter;->warningThrowable(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 127
    :cond_3
    const-class v1, Ljava/lang/OutOfMemoryError;

    invoke-static {v1, p2}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 128
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/RegisterExceptionHandler;->reportOom(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    goto :goto_2

    .line 129
    :cond_4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_5

    const-class v1, Landroid/os/DeadSystemException;

    invoke-static {v1, p2}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 131
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v1, p2}, Lcom/squareup/log/CrashReporter;->warningThrowable(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 132
    :cond_5
    const-class v1, Ljava/lang/NoClassDefFoundError;

    invoke-static {v1, p2}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 133
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/RegisterExceptionHandler;->reportNoClassDefFoundError(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    goto :goto_2

    .line 134
    :cond_6
    invoke-static {p2}, Lcom/squareup/log/RegisterExceptionHandler;->isNoWireAdapterField(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 135
    invoke-direct {p0, p2, p1}, Lcom/squareup/log/RegisterExceptionHandler;->reportNoWireAdapterField(Ljava/lang/Throwable;Ljava/lang/Thread;)V

    goto :goto_2

    .line 137
    :cond_7
    iget-object v1, p0, Lcom/squareup/log/RegisterExceptionHandler;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v1, p2, p1}, Lcom/squareup/log/CrashReporter;->crashThrowableBlocking(Ljava/lang/Throwable;Ljava/lang/Thread;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    :goto_2
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->injected()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->isRestartAfterCrashEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    goto :goto_3

    .line 150
    :cond_8
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_a

    goto :goto_4

    :catchall_0
    move-exception v1

    :try_start_2
    const-string v2, "Error handling exception"

    new-array v3, v0, [Ljava/lang/Object;

    .line 140
    invoke-static {v1, v2, v3}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "Exception while handling exception"

    .line 141
    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 146
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->injected()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->isRestartAfterCrashEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 147
    :goto_3
    invoke-static {p2}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;)V

    .line 149
    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_5

    .line 150
    :cond_9
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_a

    .line 151
    :goto_4
    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :cond_a
    :goto_5
    return-void

    :catchall_1
    move-exception v1

    .line 146
    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->injected()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct {p0}, Lcom/squareup/log/RegisterExceptionHandler;->isRestartAfterCrashEnabled()Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_6

    .line 147
    :cond_b
    invoke-static {p2}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;)V

    .line 149
    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto :goto_7

    .line 150
    :cond_c
    :goto_6
    iget-object v0, p0, Lcom/squareup/log/RegisterExceptionHandler;->defaultHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_d

    .line 151
    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 153
    :cond_d
    :goto_7
    throw v1
.end method
