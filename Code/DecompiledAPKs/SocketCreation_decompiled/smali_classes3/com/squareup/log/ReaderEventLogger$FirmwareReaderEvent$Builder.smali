.class public Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public event:I

.field public message:Ljava/lang/String;

.field public source:I

.field public timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1212
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    const/4 v0, 0x0

    .line 1213
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->message(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;

    const/4 v0, 0x0

    .line 1214
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->source(I)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;

    .line 1215
    invoke-virtual {p0, v0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->event(I)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;

    const-wide/16 v0, 0x0

    .line 1216
    invoke-virtual {p0, v0, v1}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->timestamp(J)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1206
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;
    .locals 1

    .line 1240
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;)V

    return-object v0
.end method

.method public event(I)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
    .locals 0

    .line 1230
    iput p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->event:I

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
    .locals 0

    .line 1220
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public source(I)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
    .locals 0

    .line 1225
    iput p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->source:I

    return-object p0
.end method

.method public timestamp(J)Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;
    .locals 0

    .line 1235
    iput-wide p1, p0, Lcom/squareup/log/ReaderEventLogger$FirmwareReaderEvent$Builder;->timestamp:J

    return-object p0
.end method
