.class public Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;
.super Ljava/lang/Object;
.source "OhSnapLoggable.kt"

# interfaces
.implements Lcom/squareup/log/OhSnapLoggable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/OhSnapLoggable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JavaOhSnapLoggable"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/log/OhSnapLoggable$JavaOhSnapLoggable;",
        "Lcom/squareup/log/OhSnapLoggable;",
        "()V",
        "crash-reporting_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOhSnapMessage()Ljava/lang/String;
    .locals 1

    .line 14
    invoke-static {p0}, Lcom/squareup/log/OhSnapLoggable$DefaultImpls;->getOhSnapMessage(Lcom/squareup/log/OhSnapLoggable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
