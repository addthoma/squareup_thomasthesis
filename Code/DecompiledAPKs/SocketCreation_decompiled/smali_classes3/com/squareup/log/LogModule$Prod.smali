.class public abstract Lcom/squareup/log/LogModule$Prod;
.super Ljava/lang/Object;
.source "LogModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/LogModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideOhSnapLogger(Lcom/squareup/log/CrashReportingLogger;)Lcom/squareup/log/OhSnapLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
