.class public Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;
.super Ljava/lang/Object;
.source "RealCheckoutInformationEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private _payment_token:Ljava/lang/String;

.field private checkoutEndTime:J

.field private checkoutStartTime:J

.field private email_on_file:Z

.field private local_hour:I

.field private number_of_screens:I

.field private number_of_taps:I

.field private skip_receipt_screen_enabled:Z

.field private skip_signature_enabled:Z


# direct methods
.method public constructor <init>(JZZ)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-wide p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutStartTime:J

    .line 49
    iput-wide p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutEndTime:J

    const/4 p1, 0x0

    .line 50
    iput p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_taps:I

    .line 51
    iput p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_screens:I

    .line 52
    iput-boolean p3, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->skip_receipt_screen_enabled:Z

    .line 53
    iput-boolean p4, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->skip_signature_enabled:Z

    .line 54
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    const/16 p2, 0xb

    invoke-virtual {p1, p2}, Ljava/util/Calendar;->get(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->local_hour:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_taps:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_screens:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->_payment_token:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->skip_receipt_screen_enabled:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->skip_signature_enabled:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z
    .locals 0

    .line 35
    iget-boolean p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->email_on_file:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I
    .locals 0

    .line 35
    iget p0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->local_hour:I

    return p0
.end method


# virtual methods
.method public build(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;)Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;
    .locals 4

    .line 88
    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->simpleName()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 93
    iget-wide v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutEndTime:J

    iget-wide v2, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutStartTime:J

    sub-long/2addr v0, v2

    long-to-int v1, v0

    .line 94
    new-instance v0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;-><init>(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;ILjava/lang/String;Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$1;)V

    return-object v0

    .line 89
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Attempt to build invalid event."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCheckoutEndTime()J
    .locals 2

    .line 78
    iget-wide v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutEndTime:J

    return-wide v0
.end method

.method public incrementScreenCount()V
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_screens:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_screens:I

    return-void
.end method

.method public incrementTap()V
    .locals 1

    .line 58
    iget v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_taps:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->number_of_taps:I

    return-void
.end method

.method public isValid()Z
    .locals 5

    .line 84
    iget-wide v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutStartTime:J

    iget-wide v2, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutEndTime:J

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setCheckoutEndTime(J)V
    .locals 0

    .line 74
    iput-wide p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->checkoutEndTime:J

    return-void
.end method

.method public setEmailOnFile(Z)V
    .locals 0

    .line 66
    iput-boolean p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->email_on_file:Z

    return-void
.end method

.method public setPaymentToken(Ljava/lang/String;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->_payment_token:Ljava/lang/String;

    return-void
.end method
