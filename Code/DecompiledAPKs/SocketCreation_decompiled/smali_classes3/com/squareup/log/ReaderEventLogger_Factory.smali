.class public final Lcom/squareup/log/ReaderEventLogger_Factory;
.super Ljava/lang/Object;
.source "ReaderEventLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/ReaderEventLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final aclConnectionLoggingHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AclConnectionLoggingHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final readerSessionIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AclConnectionLoggingHelper;",
            ">;)V"
        }
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p3, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p4, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p5, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p6, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p7, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p8, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p9, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p10, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p11, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->aclConnectionLoggingHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/ReaderEventLogger_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderSessionIds;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/AclConnectionLoggingHelper;",
            ">;)",
            "Lcom/squareup/log/ReaderEventLogger_Factory;"
        }
    .end annotation

    .line 82
    new-instance v12, Lcom/squareup/log/ReaderEventLogger_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/log/ReaderEventLogger_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/util/Clock;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/internet/InternetStatusMonitor;Lcom/squareup/log/AclConnectionLoggingHelper;)Lcom/squareup/log/ReaderEventLogger;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/log/ReaderSessionIds;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            "Lcom/squareup/log/AclConnectionLoggingHelper;",
            ")",
            "Lcom/squareup/log/ReaderEventLogger;"
        }
    .end annotation

    .line 91
    new-instance v12, Lcom/squareup/log/ReaderEventLogger;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/log/ReaderEventLogger;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/util/Clock;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/internet/InternetStatusMonitor;Lcom/squareup/log/AclConnectionLoggingHelper;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/log/ReaderEventLogger;
    .locals 12

    .line 70
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->readerSessionIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/ReaderSessionIds;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v8, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/internet/InternetStatusMonitor;

    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger_Factory;->aclConnectionLoggingHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/log/AclConnectionLoggingHelper;

    invoke-static/range {v1 .. v11}, Lcom/squareup/log/ReaderEventLogger_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/util/Clock;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/internet/InternetStatusMonitor;Lcom/squareup/log/AclConnectionLoggingHelper;)Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger_Factory;->get()Lcom/squareup/log/ReaderEventLogger;

    move-result-object v0

    return-object v0
.end method
