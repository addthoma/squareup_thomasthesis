.class public Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "MainThreadBlockedLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/MainThreadBlockedLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MainThreadBlockedEvent"
.end annotation


# instance fields
.field final duration_ms:J

.field final visible_screens:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;J)V
    .locals 3

    .line 33
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->MAIN_THREAD_BLOCKED:Lcom/squareup/analytics/RegisterTimingName;

    const-wide v1, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;D)V

    .line 34
    iput-object p1, p0, Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;->visible_screens:Ljava/lang/String;

    .line 35
    iput-wide p2, p0, Lcom/squareup/log/MainThreadBlockedLogger$MainThreadBlockedEvent;->duration_ms:J

    return-void
.end method
