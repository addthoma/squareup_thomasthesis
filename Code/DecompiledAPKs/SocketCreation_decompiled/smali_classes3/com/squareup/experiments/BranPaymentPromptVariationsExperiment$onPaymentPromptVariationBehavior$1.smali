.class final Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;
.super Ljava/lang/Object;
.source "BranPaymentPromptVariationsExperiment.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->onPaymentPromptVariationBehavior()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;",
        "kotlin.jvm.PlatformType",
        "useExperiment",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;


# direct methods
.method constructor <init>(Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;->this$0:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "useExperiment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 34
    iget-object p1, p0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;->this$0:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;

    invoke-virtual {p1}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment;->bucket()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1$1;->INSTANCE:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 36
    :cond_0
    sget-object p1, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;->CLASSIC:Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$Behavior;

    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/experiments/BranPaymentPromptVariationsExperiment$onPaymentPromptVariationBehavior$1;->apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
