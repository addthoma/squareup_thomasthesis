.class public Lcom/squareup/experiments/ExperimentMap;
.super Ljava/util/HashMap;
.source "ExperimentMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap<",
        "Ljava/lang/String;",
        "Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method


# virtual methods
.method public updateFrom(Lcom/squareup/server/ExperimentsResponse;)V
    .locals 2

    .line 10
    iget-object v0, p1, Lcom/squareup/server/ExperimentsResponse;->responses:Ljava/util/List;

    if-nez v0, :cond_0

    return-void

    .line 12
    :cond_0
    iget-object p1, p1, Lcom/squareup/server/ExperimentsResponse;->responses:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;

    .line 13
    iget-object v0, v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfigWrapper;->experiment:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    if-eqz v0, :cond_1

    .line 16
    iget-object v1, v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/squareup/experiments/ExperimentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-void
.end method
