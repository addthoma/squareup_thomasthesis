.class public final Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;
.super Ljava/lang/Object;
.source "SquareCardUpsellExperiment_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
        ">;"
    }
.end annotation


# instance fields
.field private final storageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;->storageProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;)",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;)Lcom/squareup/experiments/SquareCardUpsellExperiment;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;)",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/experiments/SquareCardUpsellExperiment;

    invoke-direct {v0, p0}, Lcom/squareup/experiments/SquareCardUpsellExperiment;-><init>(Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/experiments/SquareCardUpsellExperiment;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;->storageProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;->newInstance(Ldagger/Lazy;)Lcom/squareup/experiments/SquareCardUpsellExperiment;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/experiments/SquareCardUpsellExperiment_Factory;->get()Lcom/squareup/experiments/SquareCardUpsellExperiment;

    move-result-object v0

    return-object v0
.end method
