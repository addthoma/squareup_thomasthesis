.class public Lcom/squareup/experiments/SquareCardUpsellExperiment;
.super Lcom/squareup/experiments/ExperimentProfile;
.source "SquareCardUpsellExperiment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;,
        Lcom/squareup/experiments/SquareCardUpsellExperiment$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardUpsellExperiment.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardUpsellExperiment.kt\ncom/squareup/experiments/SquareCardUpsellExperiment\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0016\u0018\u0000 \n2\u00020\u0001:\u0002\t\nB\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/experiments/SquareCardUpsellExperiment;",
        "Lcom/squareup/experiments/ExperimentProfile;",
        "storage",
        "Ldagger/Lazy;",
        "Lcom/squareup/experiments/ExperimentStorage;",
        "(Ldagger/Lazy;)V",
        "squareCardUpsellBehavior",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;",
        "Behavior",
        "Companion",
        "experiments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

.field public static final Companion:Lcom/squareup/experiments/SquareCardUpsellExperiment$Companion;

.field public static final DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

.field public static final SHOW:Lcom/squareup/server/ExperimentsResponse$Bucket;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/squareup/experiments/SquareCardUpsellExperiment$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/experiments/SquareCardUpsellExperiment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment;->Companion:Lcom/squareup/experiments/SquareCardUpsellExperiment$Companion;

    .line 30
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v1, 0x813

    const/16 v2, 0x13e0

    const-string v3, "control"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment;->CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 31
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$Bucket;

    const/16 v2, 0x13e1

    const-string v3, "show"

    const/16 v4, 0x64

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/ExperimentsResponse$Bucket;-><init>(IILjava/lang/String;I)V

    sput-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment;->SHOW:Lcom/squareup/server/ExperimentsResponse$Bucket;

    .line 33
    new-instance v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;-><init>()V

    .line 34
    sget-object v2, Lcom/squareup/experiments/SquareCardUpsellExperiment;->CONTROL:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {v0, v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 35
    sget-object v2, Lcom/squareup/experiments/SquareCardUpsellExperiment;->SHOW:Lcom/squareup/server/ExperimentsResponse$Bucket;

    invoke-virtual {v0, v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->addBucket(Lcom/squareup/server/ExperimentsResponse$Bucket;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v2, "Whether or not the Square Card up sell to order a card will be shown on the Instant Deposit success screen."

    .line 36
    invoke-virtual {v0, v2}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setDescription(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setId(I)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "android_square_card_upsell_on_instant_deposit_success"

    .line 41
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setName(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "NOT_RUNNING"

    .line 42
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setStatus(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setUpdatedAt(Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    const-string v1, "1"

    .line 44
    invoke-virtual {v0, v1}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->setVersion(Ljava/lang/String;)Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig$Builder;->build()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    move-result-object v0

    const-string v1, "ExperimentConfig.Builder\u2026ion(\"1\")\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "storage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;->CONTROL:Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;

    invoke-virtual {v0}, Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;->name()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/experiments/SquareCardUpsellExperiment;->DEFAULT_CONFIGURATION:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    .line 15
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/experiments/ExperimentProfile;-><init>(Ldagger/Lazy;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;)V

    return-void

    .line 16
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public squareCardUpsellBehavior()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/experiments/SquareCardUpsellExperiment$Behavior;",
            ">;"
        }
    .end annotation

    .line 19
    invoke-virtual {p0}, Lcom/squareup/experiments/SquareCardUpsellExperiment;->bucket()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;->INSTANCE:Lcom/squareup/experiments/SquareCardUpsellExperiment$squareCardUpsellBehavior$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "bucket().map { Behavior.\u2026(it.name.toUpperCase()) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
