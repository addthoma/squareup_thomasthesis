.class public interface abstract Lcom/squareup/notifications/StoredPaymentNotifier;
.super Ljava/lang/Object;
.source "StoredPaymentNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/StoredPaymentNotifier$NoStoredPaymentNotifier;
    }
.end annotation


# virtual methods
.method public abstract hideNotification()V
.end method

.method public abstract showNotification(Ljava/lang/String;)V
.end method
