.class public interface abstract Lcom/squareup/jailkeeper/JailKeeper;
.super Ljava/lang/Object;
.source "JailKeeper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jailkeeper/JailKeeper$State;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\rJ\u0008\u0010\n\u001a\u00020\u000bH&J\u0008\u0010\u000c\u001a\u00020\u0007H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "",
        "jailScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "getJailScreen",
        "()Lcom/squareup/container/ContainerTreeKey;",
        "state",
        "Lcom/squareup/jailkeeper/JailKeeper$State;",
        "getState",
        "()Lcom/squareup/jailkeeper/JailKeeper$State;",
        "backgroundSync",
        "",
        "foregroundSync",
        "State",
        "jailkeeper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract backgroundSync()V
.end method

.method public abstract foregroundSync()Lcom/squareup/jailkeeper/JailKeeper$State;
.end method

.method public abstract getJailScreen()Lcom/squareup/container/ContainerTreeKey;
.end method

.method public abstract getState()Lcom/squareup/jailkeeper/JailKeeper$State;
.end method
