.class public final Lcom/squareup/feedback/PlayStoreLauncher;
.super Ljava/lang/Object;
.source "PlayStoreLauncher.kt"

# interfaces
.implements Lcom/squareup/ui/ActivityDelegate;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPlayStoreLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PlayStoreLauncher.kt\ncom/squareup/feedback/PlayStoreLauncher\n*L\n1#1,43:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\t\u001a\u00020\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/feedback/PlayStoreLauncher;",
        "Lcom/squareup/ui/ActivityDelegate;",
        "playStoreIntentCreator",
        "Lcom/squareup/feedback/PlayStoreIntentCreator;",
        "intentAvailabilityManager",
        "Lcom/squareup/util/IntentAvailabilityManager;",
        "(Lcom/squareup/feedback/PlayStoreIntentCreator;Lcom/squareup/util/IntentAvailabilityManager;)V",
        "context",
        "Landroid/content/Context;",
        "launchPlayStore",
        "",
        "onCreate",
        "",
        "activity",
        "Landroid/app/Activity;",
        "onDestroy",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private final intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

.field private final playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;


# direct methods
.method public constructor <init>(Lcom/squareup/feedback/PlayStoreIntentCreator;Lcom/squareup/util/IntentAvailabilityManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "playStoreIntentCreator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intentAvailabilityManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/PlayStoreLauncher;->playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

    iput-object p2, p0, Lcom/squareup/feedback/PlayStoreLauncher;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    return-void
.end method


# virtual methods
.method public final launchPlayStore()Z
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/feedback/PlayStoreLauncher;->playStoreIntentCreator:Lcom/squareup/feedback/PlayStoreIntentCreator;

    invoke-virtual {v0}, Lcom/squareup/feedback/PlayStoreIntentCreator;->create()Landroid/content/Intent;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/squareup/feedback/PlayStoreLauncher;->intentAvailabilityManager:Lcom/squareup/util/IntentAvailabilityManager;

    invoke-interface {v1, v0}, Lcom/squareup/util/IntentAvailabilityManager;->isAvailable(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37
    iget-object v1, p0, Lcom/squareup/feedback/PlayStoreLauncher;->context:Landroid/content/Context;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t start Play Store; Activity is null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/squareup/feedback/PlayStoreLauncher;->context:Landroid/content/Context;

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/feedback/PlayStoreLauncher;->context:Landroid/content/Context;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 24
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/squareup/feedback/PlayStoreLauncher;->context:Landroid/content/Context;

    :cond_0
    return-void
.end method
