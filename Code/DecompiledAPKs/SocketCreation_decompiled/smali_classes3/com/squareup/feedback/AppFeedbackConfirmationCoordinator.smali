.class public final Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AppFeedbackConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppFeedbackConfirmationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppFeedbackConfirmationCoordinator.kt\ncom/squareup/feedback/AppFeedbackConfirmationCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,86:1\n1103#2,7:87\n1103#2,7:94\n*E\n*S KotlinDebug\n*F\n+ 1 AppFeedbackConfirmationCoordinator.kt\ncom/squareup/feedback/AppFeedbackConfirmationCoordinator\n*L\n53#1,7:87\n58#1,7:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0008\u0010\u0013\u001a\u00020\u000fH\u0002J\u0018\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "noThanksButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "rateOnPlayStoreButton",
        "rateOnPlayStorePrompt",
        "Lcom/squareup/widgets/MessageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "updateViews",
        "screenData",
        "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private noThanksButton:Lcom/squareup/marketfont/MarketButton;

.field private rateOnPlayStoreButton:Lcom/squareup/marketfont/MarketButton;

.field private rateOnPlayStorePrompt:Lcom/squareup/widgets/MessageView;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;

    iput-object p2, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;)Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;Landroid/view/View;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->updateViews(Landroid/view/View;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 72
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 73
    sget v0, Lcom/squareup/feedback/R$id;->prompt_rate_on_play_store:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStorePrompt:Lcom/squareup/widgets/MessageView;

    .line 74
    sget v0, Lcom/squareup/feedback/R$id;->rate_on_play_store_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStoreButton:Lcom/squareup/marketfont/MarketButton;

    .line 75
    sget v0, Lcom/squareup/feedback/R$id;->no_thanks_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final configureActionBar()V
    .locals 6

    .line 79
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 80
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/feedback/R$string;->feedback_and_more:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v3, 0x1

    .line 81
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 82
    new-instance v3, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v3, p0}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 83
    iget-object v3, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final updateViews(Landroid/view/View;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;)V
    .locals 4

    .line 44
    new-instance v0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$1;

    invoke-direct {v0, p0}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$1;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 46
    invoke-virtual {p2}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData;->getPlayStoreSectionVisibility()Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection;

    move-result-object p2

    .line 49
    instance-of v0, p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Show;

    const-string v1, "rateOnPlayStorePrompt"

    const-string v2, "noThanksButton"

    const-string v3, "rateOnPlayStoreButton"

    if-eqz v0, :cond_5

    .line 50
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStorePrompt:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Show;

    invoke-virtual {p2}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Show;->getPromptText()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 52
    iget-object p2, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStoreButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p2, Landroid/view/View;

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 53
    iget-object p2, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStoreButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p2, Landroid/view/View;

    .line 87
    new-instance v1, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 58
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    .line 94
    new-instance p2, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$$inlined$onClickDebounced$2;

    invoke-direct {p2, p0}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$updateViews$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 62
    :cond_5
    sget-object p1, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Hide;->INSTANCE:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$DisplayPlayStoreSection$Hide;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    .line 63
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStorePrompt:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 65
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->rateOnPlayStoreButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 66
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->noThanksButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p1, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p1, Landroid/view/View;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :cond_9
    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->configureActionBar()V

    .line 35
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$Runner;->confirmationScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator$attach$1;-><init>(Lcom/squareup/feedback/AppFeedbackConfirmationCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.confirmationScree\u2026{ updateViews(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
