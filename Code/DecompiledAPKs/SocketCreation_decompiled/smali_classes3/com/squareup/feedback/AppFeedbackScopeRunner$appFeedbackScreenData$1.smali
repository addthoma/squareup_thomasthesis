.class final Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;
.super Ljava/lang/Object;
.source "AppFeedbackScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feedback/AppFeedbackScopeRunner;->appFeedbackScreenData()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "call",
        "(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;

    invoke-direct {v0}, Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;-><init>()V

    sput-object v0, Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;->INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;
    .locals 2

    .line 52
    new-instance v0, Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$appFeedbackScreenData$1;->call(Ljava/lang/Boolean;)Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
