.class public interface abstract Lcom/squareup/crashnado/CrashnadoReporter;
.super Ljava/lang/Object;
.source "CrashnadoReporter.java"


# virtual methods
.method public abstract failedToInstall(Ljava/lang/Throwable;)V
.end method

.method public abstract logCrashnadoState(Ljava/lang/String;)V
.end method

.method public abstract preparingIllegalStack(Ljava/lang/String;)V
.end method

.method public abstract reportCrash(Ljava/lang/Throwable;Ljava/lang/String;)V
.end method
