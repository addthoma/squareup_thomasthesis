.class public final Lcom/squareup/crashnado/CrashnadoNative;
.super Ljava/lang/Object;
.source "CrashnadoNative.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V
    .locals 0

    .line 12
    invoke-static {p0, p1}, Lcom/squareup/crashnado/CrashnadoNative;->reportCrashFromFile(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V

    return-void
.end method

.method public static native crash()V
.end method

.method private static native init(Ljava/lang/String;)V
.end method

.method static install(Landroid/content/Context;Lcom/squareup/crashnado/CrashnadoReporter;)V
    .locals 2

    .line 14
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object p0

    const-string v1, "native_crash_directory"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 15
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "Creating crash directory."

    .line 16
    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 17
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result p0

    if-nez p0, :cond_0

    .line 20
    new-instance p0, Ljava/lang/RuntimeException;

    const-string v0, "Failed to open native crash directory."

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->failedToInstall(Ljava/lang/Throwable;)V

    return-void

    :cond_0
    const-string p0, "Checking for previous crash."

    .line 26
    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 27
    invoke-static {p1, v0}, Lcom/squareup/crashnado/CrashnadoNative;->reportPreviousCrash(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V

    :try_start_0
    const-string p0, "Preparing signal stack for main thread."

    .line 30
    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 31
    invoke-static {}, Lcom/squareup/crashnado/CrashnadoNative;->prepareSignalStack()V

    .line 33
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/crashnado/CrashnadoNative;->init(Ljava/lang/String;)V

    const-string p0, "Successfully installed Crashnado."

    .line 34
    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 36
    invoke-interface {p1, p0}, Lcom/squareup/crashnado/CrashnadoReporter;->failedToInstall(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private static native prepareSignalStack()V
.end method

.method static prepareStack()V
    .locals 0

    .line 41
    invoke-static {}, Lcom/squareup/crashnado/CrashnadoNative;->prepareSignalStack()V

    return-void
.end method

.method private static reportCrashFromFile(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V
    .locals 8

    .line 62
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v1, v0

    new-array v0, v1, [B

    .line 65
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 66
    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 67
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 70
    new-instance p1, Ljava/io/ByteArrayOutputStream;

    array-length v1, v0

    invoke-direct {p1, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 71
    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v1, p1}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 72
    invoke-virtual {v1, v0}, Ljava/util/zip/DeflaterOutputStream;->write([B)V

    .line 73
    invoke-virtual {v1}, Ljava/util/zip/DeflaterOutputStream;->close()V

    .line 75
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    .line 77
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Native crash"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const-string v2, "com.squareup.NativeLibrary"

    const-string v3, "reportNativeLibraryCrash"

    const-string v4, "NativeLibrary.java"

    const/16 v5, 0x2a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/StackTraceElement;

    .line 83
    new-instance v7, Ljava/lang/StackTraceElement;

    invoke-direct {v7, v2, v3, v4, v5}, Ljava/lang/StackTraceElement;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v7, v6, v0

    .line 84
    invoke-virtual {v1, v6}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 85
    invoke-interface {p0, v1, p1}, Lcom/squareup/crashnado/CrashnadoReporter;->reportCrash(Ljava/lang/Throwable;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Failed to read crash file!"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lcom/squareup/crashnado/CrashnadoReporter;->reportCrash(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private static reportPreviousCrash(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/crashnado/CrashnadoNative$1;

    const-string v1, "PriorCrashReporterCheck"

    invoke-direct {v0, v1, p1, p0}, Lcom/squareup/crashnado/CrashnadoNative$1;-><init>(Ljava/lang/String;Ljava/io/File;Lcom/squareup/crashnado/CrashnadoReporter;)V

    .line 58
    invoke-virtual {v0}, Lcom/squareup/crashnado/CrashnadoNative$1;->start()V

    return-void
.end method
