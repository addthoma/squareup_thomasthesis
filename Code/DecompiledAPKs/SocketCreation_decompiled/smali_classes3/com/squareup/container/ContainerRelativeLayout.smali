.class public abstract Lcom/squareup/container/ContainerRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "ContainerRelativeLayout.java"

# interfaces
.implements Lcom/squareup/container/ContainerView;


# instance fields
.field private interceptInputEvents:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/squareup/container/ContainerRelativeLayout;->interceptInputEvents:Z

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public getCardLayout()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCardOverSheetLayout()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentLayout()Landroid/view/ViewGroup;
    .locals 0

    return-object p0
.end method

.method public getFullSheetLayout()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMasterLayout()Landroid/view/ViewGroup;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public interceptInputEvents(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/container/ContainerRelativeLayout;->cancelPendingInputEvents()V

    .line 31
    :cond_0
    iput-boolean p1, p0, Lcom/squareup/container/ContainerRelativeLayout;->interceptInputEvents:Z

    return-void
.end method

.method public isInterceptingInputEvents()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/container/ContainerRelativeLayout;->interceptInputEvents:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/container/ContainerRelativeLayout;->interceptInputEvents:Z

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public setCardOverSheetVisible(ZZLflow/TraversalCallback;)V
    .locals 0

    .line 92
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "This simple view has no card over sheet layout."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setCardVisible(ZZLflow/TraversalCallback;)V
    .locals 0

    .line 64
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "This simple view has no card layout."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setFullSheetVisible(ZZLflow/TraversalCallback;)V
    .locals 0

    .line 78
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "This simple view has no full sheet layout."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
