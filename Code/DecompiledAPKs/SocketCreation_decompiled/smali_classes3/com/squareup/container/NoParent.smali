.class final Lcom/squareup/container/NoParent;
.super Lcom/squareup/container/ContainerTreeKey;
.source "WorkflowTreeKey.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/container/NoParent;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/NoParent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/container/NoParent;

    invoke-direct {v0}, Lcom/squareup/container/NoParent;-><init>()V

    sput-object v0, Lcom/squareup/container/NoParent;->INSTANCE:Lcom/squareup/container/NoParent;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 16
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
