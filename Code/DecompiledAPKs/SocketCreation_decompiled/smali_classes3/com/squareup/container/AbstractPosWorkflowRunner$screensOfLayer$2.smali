.class final Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->screensOfLayer(Lcom/squareup/container/ContainerLayering;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2\n*L\n1#1,525:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001j\u0002`\u0002\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0004\"\u0004\u0008\u0002\u0010\u000628\u0010\u0007\u001a4\u0012\u0004\u0012\u0002H\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\n\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0001j\u0002`\u00020\tj\n\u0012\u0006\u0008\u0001\u0012\u00020\n`\u000b0\u0008H\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "P",
        "",
        "O",
        "R",
        "it",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $layer:Lcom/squareup/container/ContainerLayering;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerLayering;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;->$layer:Lcom/squareup/container/ContainerLayering;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 493
    invoke-virtual {p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;->$layer:Lcom/squareup/container/ContainerLayering;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$screensOfLayer$2;->apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
