.class public final enum Lcom/squareup/container/PosLayering;
.super Ljava/lang/Enum;
.source "PosLayering.kt"

# interfaces
.implements Lcom/squareup/container/ContainerLayering;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/PosLayering$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/container/PosLayering;",
        ">;",
        "Lcom/squareup/container/ContainerLayering;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u0000 \n2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/container/PosLayering;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "(Ljava/lang/String;I)V",
        "MASTER",
        "BODY",
        "CARD",
        "SHEET",
        "CARD_OVER_SHEET",
        "DIALOG",
        "Companion",
        "pure"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/container/PosLayering;

.field public static final enum BODY:Lcom/squareup/container/PosLayering;

.field public static final enum CARD:Lcom/squareup/container/PosLayering;

.field public static final enum CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

.field public static final Companion:Lcom/squareup/container/PosLayering$Companion;

.field public static final enum DIALOG:Lcom/squareup/container/PosLayering;

.field public static final enum MASTER:Lcom/squareup/container/PosLayering;

.field public static final enum SHEET:Lcom/squareup/container/PosLayering;

.field private static final valuesTopToBottom$delegate:Lkotlin/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/container/PosLayering;

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x0

    const-string v3, "MASTER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->MASTER:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x1

    const-string v3, "BODY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x2

    const-string v3, "CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x3

    const-string v3, "SHEET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x4

    const-string v3, "CARD_OVER_SHEET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/container/PosLayering;

    const/4 v2, 0x5

    const-string v3, "DIALOG"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/PosLayering;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/container/PosLayering;->$VALUES:[Lcom/squareup/container/PosLayering;

    new-instance v0, Lcom/squareup/container/PosLayering$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/PosLayering$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 26
    sget-object v0, Lcom/squareup/container/PosLayering$Companion$valuesTopToBottom$2;->INSTANCE:Lcom/squareup/container/PosLayering$Companion$valuesTopToBottom$2;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/PosLayering;->valuesTopToBottom$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static final synthetic access$getValuesTopToBottom$cp()Lkotlin/Lazy;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/container/PosLayering;->valuesTopToBottom$delegate:Lkotlin/Lazy;

    return-object v0
.end method

.method public static final bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/container/PosLayering$Companion;->dialogStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final fullStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/container/PosLayering$Companion;->fullStack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static final varargs partial([Lkotlin/Pair;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/container/PosLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/container/PosLayering;
    .locals 1

    const-class v0, Lcom/squareup/container/PosLayering;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/PosLayering;

    return-object p0
.end method

.method public static values()[Lcom/squareup/container/PosLayering;
    .locals 1

    sget-object v0, Lcom/squareup/container/PosLayering;->$VALUES:[Lcom/squareup/container/PosLayering;

    invoke-virtual {v0}, [Lcom/squareup/container/PosLayering;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/container/PosLayering;

    return-object v0
.end method
