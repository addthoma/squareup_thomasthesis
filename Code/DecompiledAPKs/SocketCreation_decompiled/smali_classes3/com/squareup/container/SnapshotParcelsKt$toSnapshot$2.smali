.class final Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SnapshotParcels.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Lflow/History;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSnapshotParcels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt$toSnapshot$2\n*L\n1#1,72:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "bufferedSink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toSnapshot:Lflow/History;


# direct methods
.method constructor <init>(Lflow/History;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;->$this_toSnapshot:Lflow/History;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "bufferedSink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;->$this_toSnapshot:Lflow/History;

    .line 67
    invoke-static {v0}, Lcom/squareup/container/Histories;->scrubbed(Lflow/History;)Lflow/History;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/squareup/container/PassthroughParcer;

    invoke-direct {v1}, Lcom/squareup/container/PassthroughParcer;-><init>()V

    check-cast v1, Lflow/KeyParceler;

    invoke-virtual {v0, v1}, Lflow/History;->getParcelable(Lflow/KeyParceler;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {v0}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method
