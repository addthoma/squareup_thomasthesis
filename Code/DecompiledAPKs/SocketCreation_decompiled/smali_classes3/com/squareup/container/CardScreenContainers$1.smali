.class final Lcom/squareup/container/CardScreenContainers$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "CardScreenContainers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/CardScreenContainers;->setCardVisible(Landroid/view/ViewGroup;Landroid/view/View;ZZLflow/TraversalCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lflow/TraversalCallback;

.field final synthetic val$cardContainer:Landroid/view/ViewGroup;

.field final synthetic val$coveredByCard:Landroid/view/View;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;ZLandroid/view/View;Lflow/TraversalCallback;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/container/CardScreenContainers$1;->val$cardContainer:Landroid/view/ViewGroup;

    iput-boolean p2, p0, Lcom/squareup/container/CardScreenContainers$1;->val$visible:Z

    iput-object p3, p0, Lcom/squareup/container/CardScreenContainers$1;->val$coveredByCard:Landroid/view/View;

    iput-object p4, p0, Lcom/squareup/container/CardScreenContainers$1;->val$callback:Lflow/TraversalCallback;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 57
    iget-object p1, p0, Lcom/squareup/container/CardScreenContainers$1;->val$cardContainer:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/squareup/container/CardScreenContainers$1;->val$visible:Z

    invoke-static {p1, v0}, Lcom/squareup/container/CardScreenContainers;->access$000(Landroid/view/View;Z)V

    .line 58
    iget-object p1, p0, Lcom/squareup/container/CardScreenContainers$1;->val$coveredByCard:Landroid/view/View;

    iget-boolean v0, p0, Lcom/squareup/container/CardScreenContainers$1;->val$visible:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/container/CardScreenContainers;->access$000(Landroid/view/View;Z)V

    .line 59
    iget-object p1, p0, Lcom/squareup/container/CardScreenContainers$1;->val$callback:Lflow/TraversalCallback;

    invoke-interface {p1}, Lflow/TraversalCallback;->onTraversalCompleted()V

    .line 62
    iget-object p1, p0, Lcom/squareup/container/CardScreenContainers$1;->val$cardContainer:Landroid/view/ViewGroup;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    return-void
.end method
