.class final Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,525:1\n415#2,2:526\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractPosWorkflowRunner.kt\ncom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1\n*L\n417#1,2:526\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0004\"\u0004\u0008\u0002\u0010\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/sequences/Sequence;",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "P",
        "",
        "O",
        "R",
        "it",
        "Lflow/History;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lflow/History;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;->apply(Lflow/History;)Lkotlin/sequences/Sequence;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lflow/History;)Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 415
    invoke-virtual {p1}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object p1

    const-string v0, "it.framesFromBottom<ContainerTreeKey>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 416
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 527
    sget-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1$$special$$inlined$filterIsInstance$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1$$special$$inlined$filterIsInstance$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 418
    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1$1;-><init>(Lcom/squareup/container/AbstractPosWorkflowRunner$onEnterScope$myTreeKeysInFlow$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    return-object p1

    .line 527
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.sequences.Sequence<R>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
