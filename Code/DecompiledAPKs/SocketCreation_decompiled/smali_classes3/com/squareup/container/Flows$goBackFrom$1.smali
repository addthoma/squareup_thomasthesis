.class final Lcom/squareup/container/Flows$goBackFrom$1;
.super Ljava/lang/Object;
.source "Flows.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $expectedScreen:Ljava/lang/Class;


# direct methods
.method constructor <init>(Ljava/lang/Class;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/Flows$goBackFrom$1;->$expectedScreen:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 66
    iget-object v1, p0, Lcom/squareup/container/Flows$goBackFrom$1;->$expectedScreen:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->assertInScopeOf(Ljava/lang/Class;)V

    .line 67
    invoke-static {p1}, Lcom/squareup/container/Command;->goBack(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
