.class public Lcom/squareup/container/SquarePathContainer;
.super Lflow/path/PathContainer;
.source "SquarePathContainer.java"


# instance fields
.field private childInTransition:Landroid/view/View;

.field private completeTraversal:Lcom/squareup/util/RunnableOnce;

.field private final contextFactory:Lflow/path/PathContextFactory;

.field private windowBackgroundDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lflow/path/PathContextFactory;)V
    .locals 1

    .line 44
    sget v0, Lcom/squareup/containerconstants/R$id;->container_layout:I

    invoke-direct {p0, v0}, Lflow/path/PathContainer;-><init>(I)V

    .line 45
    iput-object p1, p0, Lcom/squareup/container/SquarePathContainer;->contextFactory:Lflow/path/PathContextFactory;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/container/SquarePathContainer;)Lcom/squareup/util/RunnableOnce;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/container/SquarePathContainer;->completeTraversal:Lcom/squareup/util/RunnableOnce;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/container/SquarePathContainer;Lcom/squareup/util/RunnableOnce;)Lcom/squareup/util/RunnableOnce;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/container/SquarePathContainer;->completeTraversal:Lcom/squareup/util/RunnableOnce;

    return-object p1
.end method

.method static synthetic access$102(Lcom/squareup/container/SquarePathContainer;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/container/SquarePathContainer;->childInTransition:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/container/SquarePathContainer;)Lflow/path/PathContextFactory;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/container/SquarePathContainer;->contextFactory:Lflow/path/PathContextFactory;

    return-object p0
.end method

.method private changeChild(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 16

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    .line 113
    invoke-static/range {p2 .. p4}, Lcom/squareup/container/spot/ContainerSpots;->whereAreWeGoing(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Lcom/squareup/container/spot/Spot;

    move-result-object v15

    if-nez v12, :cond_0

    .line 115
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v6

    .line 116
    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v7

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    if-nez v12, :cond_1

    const-string v1, "*"

    goto :goto_1

    .line 119
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    :goto_1
    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 120
    invoke-static {v7}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const-string v1, "%s -> %s"

    .line 118
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    if-ne v15, v0, :cond_2

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 124
    invoke-virtual {v11, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 125
    iget-object v0, v10, Lcom/squareup/container/SquarePathContainer;->contextFactory:Lflow/path/PathContextFactory;

    move-object/from16 v8, p6

    invoke-virtual {v6, v0, v7, v8}, Lflow/path/PathContext;->destroyNotIn(Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)V

    .line 126
    invoke-interface/range {p5 .. p5}, Lflow/TraversalCallback;->onTraversalCompleted()V

    goto/16 :goto_5

    :cond_2
    move-object/from16 v8, p6

    if-eqz v12, :cond_8

    .line 132
    invoke-static {v14, v13, v12, v15}, Lcom/squareup/container/spot/SpotHelper;->findTopChild(Lflow/Direction;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot;)Landroid/view/View;

    move-result-object v5

    .line 134
    invoke-virtual {v15}, Lcom/squareup/container/spot/Spot;->skipTemporaryBackground()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_5

    .line 137
    iget-object v0, v10, Lcom/squareup/container/SquarePathContainer;->windowBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_4

    .line 138
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/container/ContainerBackgroundsService;->getWindowBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, v10, Lcom/squareup/container/SquarePathContainer;->windowBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 141
    :cond_4
    iget-object v0, v10, Lcom/squareup/container/SquarePathContainer;->windowBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 144
    :cond_5
    iput-object v13, v10, Lcom/squareup/container/SquarePathContainer;->childInTransition:Landroid/view/View;

    .line 146
    invoke-static/range {p4 .. p4}, Lcom/squareup/container/spot/SpotHelper;->flowToSpotDirection(Lflow/Direction;)Lcom/squareup/container/spot/Spot$Direction;

    move-result-object v0

    .line 145
    invoke-virtual {v15, v0, v13, v12}, Lcom/squareup/container/spot/Spot;->forceOutgoingViewOnTop(Lcom/squareup/container/spot/Spot$Direction;Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    .line 148
    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    if-eq v14, v1, :cond_7

    if-eqz v0, :cond_6

    goto :goto_3

    .line 151
    :cond_6
    invoke-virtual {v11, v13, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_4

    .line 149
    :cond_7
    :goto_3
    invoke-virtual {v11, v13, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 154
    :goto_4
    new-instance v9, Lcom/squareup/container/SquarePathContainer$1;

    move-object v0, v9

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v8, p6

    move-object v11, v9

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/container/SquarePathContainer$1;-><init>(Lcom/squareup/container/SquarePathContainer;Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Lflow/path/PathContext;Lflow/path/PathContext;[Lflow/path/PathContext;Lflow/TraversalCallback;)V

    iput-object v11, v10, Lcom/squareup/container/SquarePathContainer;->completeTraversal:Lcom/squareup/util/RunnableOnce;

    .line 173
    new-instance v5, Lcom/squareup/container/-$$Lambda$SquarePathContainer$JNcD5-TlJjEBZw2jJynRkK9McKg;

    invoke-direct {v5, v10}, Lcom/squareup/container/-$$Lambda$SquarePathContainer$JNcD5-TlJjEBZw2jJynRkK9McKg;-><init>(Lcom/squareup/container/SquarePathContainer;)V

    move-object v0, v15

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-static/range {v0 .. v5}, Lcom/squareup/container/spot/SpotHelper;->runAnimation(Lcom/squareup/container/spot/Spot;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lkotlin/jvm/functions/Function0;)V

    :goto_5
    return-void

    .line 129
    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Spots other than HERE require two non-null views."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public forceFinishTraversal()V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer;->completeTraversal:Lcom/squareup/util/RunnableOnce;

    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {v0}, Lcom/squareup/util/RunnableOnce;->run()V

    :cond_0
    return-void
.end method

.method public getActiveChild(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/container/SquarePathContainer;->childInTransition:Landroid/view/View;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 52
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$changeChild$0$SquarePathContainer()Lkotlin/Unit;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/container/SquarePathContainer;->forceFinishTraversal()V

    .line 176
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method protected varargs performTraversal(Landroid/view/ViewGroup;Lflow/path/PathContainer$TraversalState;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
    .locals 9

    .line 58
    invoke-virtual {p2}, Lflow/path/PathContainer$TraversalState;->toPath()Lflow/path/Path;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    .line 63
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    if-lez v3, :cond_0

    .line 64
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 65
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v5

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object v5

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x1

    if-eqz v3, :cond_1

    .line 72
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_2

    .line 78
    invoke-interface {p4}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    :cond_2
    if-eqz v3, :cond_3

    .line 84
    invoke-virtual {p2, v3}, Lflow/path/PathContainer$TraversalState;->saveViewState(Landroid/view/View;)V

    .line 88
    :cond_3
    iget-object v8, p0, Lcom/squareup/container/SquarePathContainer;->contextFactory:Lflow/path/PathContextFactory;

    .line 89
    invoke-static {v2, v8, v5, p5}, Lflow/path/PathContext;->create(Lflow/path/Path;Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)Lflow/path/PathContext;

    move-result-object v5

    .line 96
    instance-of v8, v2, Lcom/squareup/container/BootstrapTreeKey;

    if-eqz v8, :cond_4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 97
    const-class v1, Lcom/squareup/container/BootstrapTreeKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    aput-object v2, v0, v6

    const-string v1, "Declining to render %s: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-interface {p4}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void

    .line 102
    :cond_4
    invoke-static {v5}, Lcom/squareup/container/ContainerKt;->getViewFactory(Landroid/content/Context;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object v8

    invoke-interface {v8, v2, v5, p1}, Lcom/squareup/container/ContainerViewFactory;->viewForKey(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v4

    const-string v2, "newView (from key %s)"

    .line 103
    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    invoke-virtual {p2, v5}, Lflow/path/PathContainer$TraversalState;->restoreViewState(Landroid/view/View;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, v3

    move-object v3, v5

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 108
    invoke-direct/range {v0 .. v6}, Lcom/squareup/container/SquarePathContainer;->changeChild(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lflow/Direction;Lflow/TraversalCallback;[Lflow/path/PathContext;)V

    return-void
.end method
