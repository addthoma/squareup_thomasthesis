.class public final Lcom/squareup/container/WorkflowDialogKey;
.super Lcom/squareup/container/WorkflowTreeKey;
.source "WorkflowDialogKey.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/WorkflowDialogScreen;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0010\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0005H\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowDialogKey;",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "runnerServiceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;)V",
        "buildDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "reparent",
        "newParent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            ")V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 22
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowTreeKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-void
.end method


# virtual methods
.method public final buildDialog(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "contextForNewDialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/container/WorkflowDialogKey;->runnerServiceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 42
    check-cast v0, Lcom/squareup/container/AbstractPosWorkflowRunner;

    .line 44
    move-object v1, p0

    check-cast v1, Lcom/squareup/container/WorkflowTreeKey;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->buildDialog$public_release(Lcom/squareup/container/WorkflowTreeKey;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowDialogKey;
    .locals 8

    const-string v0, "newParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/container/WorkflowDialogKey;

    iget-object v2, p0, Lcom/squareup/container/WorkflowDialogKey;->runnerServiceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/container/WorkflowDialogKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowDialogKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowDialogKey;->getProps()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowDialogKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v7

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/container/WorkflowDialogKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;)V

    return-object v0
.end method

.method public bridge synthetic reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowDialogKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowDialogKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    return-object p1
.end method
