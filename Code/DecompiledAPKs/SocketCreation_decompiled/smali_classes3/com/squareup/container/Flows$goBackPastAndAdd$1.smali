.class final Lcom/squareup/container/Flows$goBackPastAndAdd$1;
.super Ljava/lang/Object;
.source "Flows.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFlows.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Flows.kt\ncom/squareup/container/Flows$goBackPastAndAdd$1\n*L\n1#1,216:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $direction:Lflow/Direction;

.field final synthetic $screen:Lcom/squareup/container/ContainerTreeKey;

.field final synthetic $screenTypes:[Ljava/lang/Class;


# direct methods
.method constructor <init>([Ljava/lang/Class;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$screenTypes:[Ljava/lang/Class;

    iput-object p2, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    iput-object p3, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$direction:Lflow/Direction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$screenTypes:[Ljava/lang/Class;

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 118
    iget-object v0, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$screen:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 119
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/container/Flows$goBackPastAndAdd$1;->$direction:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
