.class final Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;
.super Ljava/lang/Object;
.source "ContainerPresenter.kt"

# interfaces
.implements Lcom/squareup/container/layer/ViewGroupLayer$Exposer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->prepareLayers(Lflow/path/PathContextFactory;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u000e\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "visible",
        "",
        "<anonymous parameter 1>",
        "callback",
        "Lflow/TraversalCallback;",
        "kotlin.jvm.PlatformType",
        "setVisible",
        "com/squareup/container/ContainerPresenter$prepareLayers$4$exposeMaster$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contextFactory$inlined:Lflow/path/PathContextFactory;

.field final synthetic $layers$inlined:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/container/ContainerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerPresenter;Lflow/path/PathContextFactory;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;->this$0:Lcom/squareup/container/ContainerPresenter;

    iput-object p2, p0, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;->$contextFactory$inlined:Lflow/path/PathContextFactory;

    iput-object p3, p0, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;->$layers$inlined:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final setVisible(ZZLflow/TraversalCallback;)V
    .locals 1

    .line 333
    iget-object p2, p0, Lcom/squareup/container/ContainerPresenter$prepareLayers$$inlined$let$lambda$4;->this$0:Lcom/squareup/container/ContainerPresenter;

    invoke-static {p2}, Lcom/squareup/container/ContainerPresenter;->access$getView(Lcom/squareup/container/ContainerPresenter;)Lcom/squareup/container/ContainerView;

    move-result-object p2

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Lcom/squareup/container/ContainerView;->getMasterLayout()Landroid/view/ViewGroup;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string/jumbo v0, "view.masterLayout!!"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 334
    invoke-interface {p3}, Lflow/TraversalCallback;->onTraversalCompleted()V

    return-void
.end method
