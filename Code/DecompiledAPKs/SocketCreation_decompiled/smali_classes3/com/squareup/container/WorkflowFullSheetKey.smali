.class public final Lcom/squareup/container/WorkflowFullSheetKey;
.super Lcom/squareup/container/WorkflowLayoutKey;
.source "WorkflowFullSheetKey.kt"

# interfaces
.implements Lcom/squareup/container/layer/MayHideStatusBar;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/WorkflowFullSheetKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowFullSheetKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowFullSheetKey.kt\ncom/squareup/container/WorkflowFullSheetKey\n+ 2 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,35:1\n150#2:36\n24#3,4:37\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowFullSheetKey.kt\ncom/squareup/container/WorkflowFullSheetKey\n*L\n32#1:36\n32#1,4:37\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u00162\u00020\u00012\u00020\u0002:\u0001\u0016BK\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0008\u0010\u0013\u001a\u00020\u0011H\u0016J\u0010\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u0006H\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowFullSheetKey;",
        "Lcom/squareup/container/WorkflowLayoutKey;",
        "Lcom/squareup/container/layer/MayHideStatusBar;",
        "runnerServiceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V",
        "hideStatusBar",
        "reparent",
        "newParent",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/WorkflowFullSheetKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/container/WorkflowFullSheetKey$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/WorkflowFullSheetKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/WorkflowFullSheetKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/WorkflowFullSheetKey;->Companion:Lcom/squareup/container/WorkflowFullSheetKey$Companion;

    .line 32
    sget-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    .line 37
    new-instance v0, Lcom/squareup/container/WorkflowFullSheetKey$$special$$inlined$creator$1;

    invoke-direct {v0}, Lcom/squareup/container/WorkflowFullSheetKey$$special$$inlined$creator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 40
    sput-object v0, Lcom/squareup/container/WorkflowFullSheetKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct/range {p0 .. p7}, Lcom/squareup/container/WorkflowLayoutKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-void
.end method


# virtual methods
.method public hideStatusBar()Z
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowFullSheetKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/ScreenHint;->getHideStatusBar()Z

    move-result v0

    return v0
.end method

.method public reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowFullSheetKey;
    .locals 9

    const-string v0, "newParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/container/WorkflowFullSheetKey;

    .line 26
    iget-object v2, p0, Lcom/squareup/container/WorkflowFullSheetKey;->runnerServiceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/container/WorkflowFullSheetKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowFullSheetKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowFullSheetKey;->getProps()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowFullSheetKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v7

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowFullSheetKey;->isPersistent()Z

    move-result v8

    move-object v1, v0

    move-object v3, p1

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowFullSheetKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-object v0
.end method

.method public bridge synthetic reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowFullSheetKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowFullSheetKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    return-object p1
.end method
