.class final Lcom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1;
.super Ljava/lang/Object;
.source "BlockingBootstrapTreeKey.kt"

# interfaces
.implements Lcom/squareup/container/RedirectStep;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/BlockingBootstrapTreeKeyKt;->executeAndPopBlockingBootstrapTreeKey(Lmortar/MortarScope;)Lcom/squareup/container/RedirectStep;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBlockingBootstrapTreeKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BlockingBootstrapTreeKey.kt\ncom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,86:1\n704#2:87\n777#2,2:88\n*E\n*S KotlinDebug\n*F\n+ 1 BlockingBootstrapTreeKey.kt\ncom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1\n*L\n68#1:87\n68#1,2:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "kotlin.jvm.PlatformType",
        "maybeRedirect"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflowScope:Lmortar/MortarScope;


# direct methods
.method constructor <init>(Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1;->$workflowScope:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final maybeRedirect(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 5

    .line 50
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    .line 51
    instance-of v1, v0, Lcom/squareup/container/BlockingBootstrapTreeKey;

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1;->$workflowScope:Lmortar/MortarScope;

    move-object v2, v0

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v1, v2}, Lcom/squareup/container/MortarScopeContainerKt;->bootstrap(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)V

    .line 53
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    .line 56
    sget-object v2, Lcom/squareup/container/WaitForBootstrap;->INSTANCE:Lcom/squareup/container/WaitForBootstrap;

    invoke-virtual {v1, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 57
    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v1

    .line 59
    new-instance v2, Lcom/squareup/container/RedirectStep$Result;

    .line 60
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invoked and popped "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v1, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    .line 59
    invoke-direct {v2, v0, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v2

    .line 64
    :cond_0
    instance-of v0, v0, Lcom/squareup/container/WaitForBootstrap;

    if-nez v0, :cond_4

    .line 65
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lflow/History$Builder;->clear()Lflow/History$Builder;

    move-result-object v0

    .line 68
    iget-object v1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v1}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v1

    const-string/jumbo v2, "traversal.destination.framesFromBottom<Any>()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 88
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 69
    instance-of v4, v3, Lcom/squareup/container/BlockingBootstrapTreeKey;

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    .line 72
    instance-of v4, v3, Lcom/squareup/container/WaitForBootstrap;

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BlockingBootstrapTreeKey must not be in the back stack: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 69
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 89
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    .line 68
    invoke-virtual {v0, v2}, Lflow/History$Builder;->addAll(Ljava/util/Collection;)Lflow/History$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lflow/History;->size()I

    move-result v1

    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->size()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 77
    new-instance v1, Lcom/squareup/container/RedirectStep$Result;

    .line 78
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v0, "Filtered WaitForBootstrap screens"

    .line 77
    invoke-direct {v1, v0, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    return-object v1

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method
