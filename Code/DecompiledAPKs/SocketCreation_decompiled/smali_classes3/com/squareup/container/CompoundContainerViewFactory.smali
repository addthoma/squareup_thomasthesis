.class public Lcom/squareup/container/CompoundContainerViewFactory;
.super Ljava/lang/Object;
.source "CompoundContainerViewFactory.java"

# interfaces
.implements Lcom/squareup/container/ContainerViewFactory;


# instance fields
.field private final delegates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/CompoundContainerViewFactory;->delegates:Ljava/util/List;

    return-void
.end method

.method private getDelegate(Ljava/lang/Object;)Lcom/squareup/container/ContainerViewFactory;
    .locals 3

    .line 47
    iget-object v0, p0, Lcom/squareup/container/CompoundContainerViewFactory;->delegates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerViewFactory;

    .line 48
    invoke-interface {v1, p1}, Lcom/squareup/container/ContainerViewFactory;->canRender(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public canRender(Ljava/lang/Object;)Z
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/container/CompoundContainerViewFactory;->getDelegate(Ljava/lang/Object;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public dialogForKey(Ljava/lang/Object;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/container/CompoundContainerViewFactory;->getDelegate(Ljava/lang/Object;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object v0

    const-string v1, "getDelegate"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerViewFactory;

    invoke-interface {v0, p1, p2}, Lcom/squareup/container/ContainerViewFactory;->dialogForKey(Ljava/lang/Object;Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public viewForKey(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/container/CompoundContainerViewFactory;->getDelegate(Ljava/lang/Object;)Lcom/squareup/container/ContainerViewFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 38
    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/container/ContainerViewFactory;->viewForKey(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 33
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    const/4 p3, 0x2

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-class v1, Lcom/squareup/container/ContainerViewFactory;

    .line 36
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    const/4 v0, 0x1

    aput-object p1, p3, v0

    const-string p1, "No %s found for %s. I bet you forgot to call ContainerActivityDelegate.takeActivity and ContainerActivityDelegate.dropActivity"

    .line 33
    invoke-static {p1, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
