.class public final Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SpotHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->onMeasured(Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1",
        "Landroid/animation/AnimatorListenerAdapter;",
        "onAnimationEnd",
        "",
        "animation",
        "Landroid/animation/Animator;",
        "onAnimationStart",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;


# direct methods
.method constructor <init>(Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 167
    iput-object p1, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;->this$0:Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object p1, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;->this$0:Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;

    iget-object p1, p1, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$onAnimationEnd:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    const-string v0, "animation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1$onMeasured$1;->this$0:Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;

    iget-object p1, p1, Lcom/squareup/container/spot/SpotHelper$Companion$runAnimation$onMeasured$1;->$to:Landroid/view/View;

    instance-of v0, p1, Lcom/squareup/container/VisualTransitionListener;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/container/VisualTransitionListener;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/squareup/container/VisualTransitionListener;->onStartVisualTransition()V

    :cond_1
    return-void
.end method
