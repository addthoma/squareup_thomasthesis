.class public Lcom/squareup/container/spot/ExchangeSet$Builder;
.super Ljava/lang/Object;
.source "ExchangeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/ExchangeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

.field private exchangeDuration:I

.field private exchangers:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/container/spot/ExchangeSet$Exchanger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangers:Ljava/util/LinkedHashMap;

    const/4 v0, -0x1

    .line 37
    iput v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangeDuration:I

    return-void
.end method

.method private build()Lcom/squareup/container/spot/ExchangeSet;
    .locals 5

    .line 79
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    const-string v1, "defaultExchange"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 80
    iget v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangeDuration:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Must set a exchange duration!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 81
    new-instance v0, Lcom/squareup/container/spot/ExchangeSet;

    iget-object v1, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    iget-object v2, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangers:Ljava/util/LinkedHashMap;

    iget v3, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangeDuration:I

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/container/spot/ExchangeSet;-><init>(Lcom/squareup/container/spot/ExchangeSet$Exchanger;Ljava/util/LinkedHashMap;ILcom/squareup/container/spot/ExchangeSet$1;)V

    return-object v0
.end method


# virtual methods
.method public defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    return-object p0
.end method

.method duration(I)Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangeDuration:I

    return-object p0
.end method

.method public exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 1

    const-string v0, "Use skipExchange to skip a exchange!"

    .line 68
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->access$000()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 1

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 v0, 0x10e0000

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->duration(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object p1

    return-object p1
.end method

.method public start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 75
    invoke-direct {p0}, Lcom/squareup/container/spot/ExchangeSet$Builder;->build()Lcom/squareup/container/spot/ExchangeSet;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet;->access$100(Lcom/squareup/container/spot/ExchangeSet;Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method
