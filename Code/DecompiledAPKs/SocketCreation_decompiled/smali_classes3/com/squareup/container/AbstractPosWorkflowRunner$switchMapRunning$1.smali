.class final Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->switchMapRunning(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006\"\u0004\u0008\u0003\u0010\u0002\"\u0008\u0008\u0004\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0005\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0006\u0010\u00062 \u0010\u0007\u001a\u001c\u0012\u0004\u0012\u0002H\u0004\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\t0\u0008H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "T",
        "P",
        "O",
        "",
        "R",
        "workflowHost",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transform:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;->$transform:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;+",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;)",
            "Lio/reactivex/Observable<",
            "+TT;>;"
        }
    .end annotation

    const-string/jumbo v0, "workflowHost"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    sget-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner;->Companion:Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner$Companion;->getNoWorkflowSentinel()Lcom/squareup/workflow/rx2/RxWorkflowHost;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "empty()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;->$transform:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/Observable;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$switchMapRunning$1;->apply(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
