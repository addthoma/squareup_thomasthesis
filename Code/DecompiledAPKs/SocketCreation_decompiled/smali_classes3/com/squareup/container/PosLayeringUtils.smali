.class public Lcom/squareup/container/PosLayeringUtils;
.super Ljava/lang/Object;
.source "PosLayeringUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPosLayeringUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PosLayeringUtils.kt\ncom/squareup/container/PosLayeringUtils\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,72:1\n310#2,7:73\n*E\n*S KotlinDebug\n*F\n+ 1 PosLayeringUtils.kt\ncom/squareup/container/PosLayeringUtils\n*L\n68#1,7:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002JZ\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0016\u0010\u000e\u001a\u0012\u0012\u0006\u0008\u0001\u0012\u00020\u0001\u0012\u0006\u0008\u0001\u0012\u00020\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\u0006H\u0016J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0018\u001a\u00020\u0006H\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/container/PosLayeringUtils;",
        "",
        "()V",
        "zOrdering",
        "",
        "Lkotlin/reflect/KClass;",
        "Lcom/squareup/container/WorkflowTreeKey;",
        "layeringToTreeKey",
        "layering",
        "Lcom/squareup/container/ContainerLayering;",
        "serviceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "treeKeyToLayering",
        "treeKey",
        "zIndex",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final zOrdering:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/container/WorkflowTreeKey;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/reflect/KClass;

    .line 19
    const-class v1, Lcom/squareup/container/WorkflowMasterKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 20
    const-class v1, Lcom/squareup/container/WorkflowBodyKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 21
    const-class v1, Lcom/squareup/container/WorkflowCardKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 22
    const-class v1, Lcom/squareup/container/WorkflowFullSheetKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 23
    const-class v1, Lcom/squareup/container/WorkflowCardOverSheetKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 24
    const-class v1, Lcom/squareup/container/WorkflowDialogKey;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 18
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/container/PosLayeringUtils;->zOrdering:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public layeringToTreeKey(Lcom/squareup/container/ContainerLayering;Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)Lcom/squareup/container/WorkflowTreeKey;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/ContainerLayering;",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "+",
            "Ljava/lang/Object;",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)",
            "Lcom/squareup/container/WorkflowTreeKey;"
        }
    .end annotation

    move-object v0, p1

    const-string v1, "layering"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "serviceName"

    move-object v3, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "screenKey"

    move-object v5, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "snapshot"

    move-object v6, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "props"

    move-object/from16 v7, p6

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "hint"

    move-object/from16 v8, p7

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object v1, Lcom/squareup/container/PosLayering;->MASTER:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/squareup/container/WorkflowMasterKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowMasterKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    goto/16 :goto_0

    .line 39
    :cond_0
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/squareup/container/WorkflowBodyKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowBodyKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    goto/16 :goto_0

    .line 40
    :cond_1
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/squareup/container/WorkflowCardKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowCardKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    goto :goto_0

    .line 41
    :cond_2
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/squareup/container/WorkflowCardOverSheetKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowCardOverSheetKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    goto :goto_0

    .line 44
    :cond_3
    sget-object v1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/squareup/container/WorkflowFullSheetKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/squareup/container/WorkflowFullSheetKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    goto :goto_0

    .line 47
    :cond_4
    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/squareup/container/WorkflowDialogKey;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v2 .. v8}, Lcom/squareup/container/WorkflowDialogKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;)V

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    :goto_0
    return-object v0

    .line 48
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "What\'s a "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public treeKeyToLayering(Lcom/squareup/container/WorkflowTreeKey;)Lcom/squareup/container/ContainerLayering;
    .locals 3

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p1, Lcom/squareup/container/WorkflowMasterKey;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/container/PosLayering;->MASTER:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    goto :goto_1

    .line 55
    :cond_0
    instance-of v0, p1, Lcom/squareup/container/WorkflowSectionKey;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 56
    :cond_1
    instance-of v0, p1, Lcom/squareup/container/WorkflowBodyKey;

    if-eqz v0, :cond_2

    :goto_0
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    goto :goto_1

    .line 57
    :cond_2
    instance-of v0, p1, Lcom/squareup/container/WorkflowCardKey;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    goto :goto_1

    .line 58
    :cond_3
    instance-of v0, p1, Lcom/squareup/container/WorkflowFullSheetKey;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    goto :goto_1

    .line 59
    :cond_4
    instance-of v0, p1, Lcom/squareup/container/WorkflowDialogKey;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    goto :goto_1

    .line 60
    :cond_5
    instance-of v0, p1, Lcom/squareup/container/WorkflowCardOverSheetKey;

    if-eqz v0, :cond_6

    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    check-cast p1, Lcom/squareup/container/ContainerLayering;

    :goto_1
    return-object p1

    .line 61
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "What\'s a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public zIndex(Lcom/squareup/container/WorkflowTreeKey;)I
    .locals 3

    const-string/jumbo v0, "treeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    instance-of v0, p1, Lcom/squareup/container/WorkflowSectionKey;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/container/PosLayeringUtils;->zOrdering:Ljava/util/List;

    const-class v0, Lcom/squareup/container/WorkflowBodyKey;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    goto :goto_1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/container/PosLayeringUtils;->zOrdering:Ljava/util/List;

    const/4 v1, 0x0

    .line 74
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 75
    check-cast v2, Lkotlin/reflect/KClass;

    .line 68
    invoke-static {v2}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move p1, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, -0x1

    :goto_1
    return p1
.end method
