.class public final Lcom/squareup/container/BlockingBootstrapTreeKeyKt;
.super Ljava/lang/Object;
.source "BlockingBootstrapTreeKey.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "executeAndPopBlockingBootstrapTreeKey",
        "Lcom/squareup/container/RedirectStep;",
        "workflowScope",
        "Lmortar/MortarScope;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final executeAndPopBlockingBootstrapTreeKey(Lmortar/MortarScope;)Lcom/squareup/container/RedirectStep;
    .locals 1

    const-string/jumbo v0, "workflowScope"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/BlockingBootstrapTreeKeyKt$executeAndPopBlockingBootstrapTreeKey$1;-><init>(Lmortar/MortarScope;)V

    check-cast v0, Lcom/squareup/container/RedirectStep;

    return-object v0
.end method
