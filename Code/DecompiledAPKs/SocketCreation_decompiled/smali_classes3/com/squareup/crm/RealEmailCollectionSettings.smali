.class Lcom/squareup/crm/RealEmailCollectionSettings;
.super Ljava/lang/Object;
.source "RealEmailCollectionSettings.java"

# interfaces
.implements Lcom/squareup/crm/EmailCollectionSettings;


# instance fields
.field private final emailCollectionEnabled:Lcom/squareup/crm/EmailCollectionEnabledSetting;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/crm/EmailCollectionEnabledSetting;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/crm/RealEmailCollectionSettings;->emailCollectionEnabled:Lcom/squareup/crm/EmailCollectionEnabledSetting;

    .line 16
    iput-object p2, p0, Lcom/squareup/crm/RealEmailCollectionSettings;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public isEmailCollectionAllowed()Z
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/crm/RealEmailCollectionSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_BUYER_EMAIL_COLLECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isEmailCollectionEnabled()Z
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/crm/RealEmailCollectionSettings;->isEmailCollectionAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/RealEmailCollectionSettings;->emailCollectionEnabled:Lcom/squareup/crm/EmailCollectionEnabledSetting;

    invoke-virtual {v0}, Lcom/squareup/crm/EmailCollectionEnabledSetting;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setEmailCollectionEnabled(Z)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/crm/RealEmailCollectionSettings;->emailCollectionEnabled:Lcom/squareup/crm/EmailCollectionEnabledSetting;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/EmailCollectionEnabledSetting;->setValueLocally(Ljava/lang/Object;)V

    return-void
.end method
