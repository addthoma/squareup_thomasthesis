.class public final Lcom/squareup/crm/groups/choose/ChooseGroupsInternalViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealChooseGroupsViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/crm/groups/choose/ChooseGroupsInternalViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "chooseGroupsLayoutRunnerFactory",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$Factory;",
        "(Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseGroupsLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 20
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 21
    const-class v2, Lcom/squareup/crm/groups/choose/ChooseGroupsRendering;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 22
    sget v3, Lcom/squareup/crm/groups/impl/R$layout;->crm_groups_choose_groups_view:I

    .line 23
    new-instance v4, Lcom/squareup/crm/groups/choose/ChooseGroupsInternalViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsInternalViewFactory$1;-><init>(Lcom/squareup/crm/groups/choose/ChooseGroupsLayoutRunner$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 20
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
