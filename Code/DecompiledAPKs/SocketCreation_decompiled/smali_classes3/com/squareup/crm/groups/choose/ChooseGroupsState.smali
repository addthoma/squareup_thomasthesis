.class public abstract Lcom/squareup/crm/groups/choose/ChooseGroupsState;
.super Ljava/lang/Object;
.source "ChooseGroupsState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/groups/choose/ChooseGroupsState$ShowingList;,
        Lcom/squareup/crm/groups/choose/ChooseGroupsState$CreatingGroup;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0018\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007R\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u0082\u0001\u0002\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState;",
        "Landroid/os/Parcelable;",
        "()V",
        "groups",
        "",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "getGroups",
        "()Ljava/util/List;",
        "selectedGroupsTokens",
        "",
        "",
        "getSelectedGroupsTokens",
        "()Ljava/util/Set;",
        "CreatingGroup",
        "ShowingList",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState$ShowingList;",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsState$CreatingGroup;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/crm/groups/choose/ChooseGroupsState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getGroups()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSelectedGroupsTokens()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
