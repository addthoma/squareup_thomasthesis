.class public final Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;
.super Ljava/lang/Object;
.source "EditGroupLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/crm/groups/edit/EditGroupRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditGroupLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditGroupLayoutRunner.kt\ncom/squareup/crm/groups/edit/EditGroupLayoutRunner\n+ 2 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox\n*L\n1#1,72:1\n172#2,2:73\n172#2,2:75\n*E\n*S KotlinDebug\n*F\n+ 1 EditGroupLayoutRunner.kt\ncom/squareup/crm/groups/edit/EditGroupLayoutRunner\n*L\n55#1,2:73\n28#1,2:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0018\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \n*\u0004\u0018\u00010\u000c0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/crm/groups/edit/EditGroupRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "input",
        "Lcom/squareup/noho/NohoInputBox;",
        "kotlin.jvm.PlatformType",
        "progress",
        "Landroid/widget/ProgressBar;",
        "doClose",
        "",
        "rendering",
        "doSave",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final input:Lcom/squareup/noho/NohoInputBox;

.field private final progress:Landroid/widget/ProgressBar;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 8

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    .line 23
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 24
    iget-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crm/groups/impl/R$id;->crm_group_name_input:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoInputBox;

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->input:Lcom/squareup/noho/NohoInputBox;

    .line 25
    iget-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crm/groups/impl/R$id;->crm_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->progress:Landroid/widget/ProgressBar;

    .line 28
    iget-object p1, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->input:Lcom/squareup/noho/NohoInputBox;

    .line 75
    invoke-virtual {p1}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    .line 29
    new-instance v7, Lcom/squareup/noho/ClearPlugin;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "context"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, v7}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public static final synthetic access$doClose(Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;Lcom/squareup/crm/groups/edit/EditGroupRendering;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->doClose(Lcom/squareup/crm/groups/edit/EditGroupRendering;)V

    return-void
.end method

.method public static final synthetic access$doSave(Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;Lcom/squareup/crm/groups/edit/EditGroupRendering;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->doSave(Lcom/squareup/crm/groups/edit/EditGroupRendering;)V

    return-void
.end method

.method private final doClose(Lcom/squareup/crm/groups/edit/EditGroupRendering;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 64
    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getOnClose()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private final doSave(Lcom/squareup/crm/groups/edit/EditGroupRendering;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 69
    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getOnSave()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/crm/groups/edit/EditGroupRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 5

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$1;-><init>(Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;Lcom/squareup/crm/groups/edit/EditGroupRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 39
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/crm/groups/impl/R$string;->crm_groups_create_group_label:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    .line 43
    new-instance v2, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$2;-><init>(Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;Lcom/squareup/crm/groups/edit/EditGroupRendering;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 41
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 46
    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 47
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getSaveEnabled()Z

    move-result v3

    .line 49
    new-instance v4, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$3;

    invoke-direct {v4, p0, p1}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner$showRendering$3;-><init>(Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;Lcom/squareup/crm/groups/edit/EditGroupRendering;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 45
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 53
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->input:Lcom/squareup/noho/NohoInputBox;

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getBusy()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 54
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->input:Lcom/squareup/noho/NohoInputBox;

    iget-object v0, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/crm/groups/impl/R$string;->crm_groups_group_name_label:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "view.resources.getString\u2026_groups_group_name_label)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoInputBox;->setTitle(Ljava/lang/String;)V

    .line 55
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->input:Lcom/squareup/noho/NohoInputBox;

    .line 73
    invoke-virtual {p2}, Lcom/squareup/noho/NohoInputBox;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object p2

    .line 56
    check-cast p2, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getGroupName()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 59
    iget-object p2, p0, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->progress:Landroid/widget/ProgressBar;

    const-string v0, "progress"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/crm/groups/edit/EditGroupRendering;->getBusy()Z

    move-result p1

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/crm/groups/edit/EditGroupRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/crm/groups/edit/EditGroupLayoutRunner;->showRendering(Lcom/squareup/crm/groups/edit/EditGroupRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
