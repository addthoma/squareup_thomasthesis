.class public interface abstract Lcom/squareup/crm/groups/edit/EditGroupWorkflow;
.super Ljava/lang/Object;
.source "EditGroupWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/crm/groups/edit/EditGroupWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/crm/groups/edit/EditGroupOutput;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
