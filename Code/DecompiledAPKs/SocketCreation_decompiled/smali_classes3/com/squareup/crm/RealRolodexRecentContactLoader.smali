.class Lcom/squareup/crm/RealRolodexRecentContactLoader;
.super Ljava/lang/Object;
.source "RealRolodexRecentContactLoader.java"

# interfaces
.implements Lcom/squareup/crm/RolodexRecentContactLoader;


# annotations
.annotation runtime Lcom/squareup/crm/RolodexRecentContactLoader$SharedScope;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final EMPTY_CONTACT_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private static final RECENT_RESULT_LIMIT:I = 0xa


# instance fields
.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private contacts:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation
.end field

.field private firstContactJustSaved:Z

.field private final topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->EMPTY_CONTACT_LIST:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .param p2    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    new-instance v0, Lcom/squareup/crm/RolodexContactLoader;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/crm/RolodexContactLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)V

    invoke-direct {p0, v0}, Lcom/squareup/crm/RealRolodexRecentContactLoader;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/crm/RolodexContactLoader;)V
    .locals 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    sget-object v0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->EMPTY_CONTACT_LIST:Ljava/util/List;

    .line 44
    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 62
    iput-object p1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 63
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {p1, v0}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    const/16 v0, 0xa

    .line 64
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/crm/RolodexContactLoader;->setDefaultPageSize(Ljava/lang/Integer;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 68
    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object p1

    const/4 v1, 0x0

    check-cast v1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    .line 69
    invoke-virtual {p1, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/squareup/crm/-$$Lambda$RealRolodexRecentContactLoader$XuxLnZfhxu8Rn7pXolVAym1qrrA;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RealRolodexRecentContactLoader$XuxLnZfhxu8Rn7pXolVAym1qrrA;

    .line 70
    invoke-virtual {p1, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v1, Lcom/squareup/crm/-$$Lambda$RealRolodexRecentContactLoader$-rPQUw0RurfY8inQYjA14MNcV7c;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RealRolodexRecentContactLoader$-rPQUw0RurfY8inQYjA14MNcV7c;

    .line 66
    invoke-static {v0, p1, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contacts:Lrx/Observable;

    return-void
.end method

.method private static combineContacts(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 127
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 129
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/16 v3, 0xa

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    .line 130
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v4, v3, :cond_0

    iget-object v3, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 131
    iget-object v3, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 137
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v2

    if-ge v2, v3, :cond_2

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 138
    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method public static synthetic lambda$-rPQUw0RurfY8inQYjA14MNcV7c(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/crm/RealRolodexRecentContactLoader;->combineContacts(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$0(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/util/List;
    .locals 3

    if-eqz p0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const-string v0, "Only one page of contacts is expected."

    invoke-static {v2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 73
    iget-object p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0

    .line 75
    :cond_1
    sget-object p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->EMPTY_CONTACT_LIST:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public addContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    const/4 v0, 0x1

    .line 105
    invoke-virtual {p0, v0}, Lcom/squareup/crm/RealRolodexRecentContactLoader;->setFirstContactJustSaved(Z)V

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v1, "contact"

    .line 107
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public contacts()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contacts:Lrx/Observable;

    return-object v0
.end method

.method public getFirstContactJustSaved()Z
    .locals 1

    .line 97
    iget-boolean v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->firstContactJustSaved:Z

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public progress()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            ">;>;>;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexContactLoader;->progress()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public removeContact(Ljava/lang/String;)V
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexContactLoader;->refresh()V

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v1, 0x0

    .line 116
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 117
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 119
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->topmostContacts:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public setFirstContactJustSaved(Z)V
    .locals 0

    .line 101
    iput-boolean p1, p0, Lcom/squareup/crm/RealRolodexRecentContactLoader;->firstContactJustSaved:Z

    return-void
.end method
