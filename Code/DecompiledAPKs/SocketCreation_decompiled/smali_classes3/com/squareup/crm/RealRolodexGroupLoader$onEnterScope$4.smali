.class final Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRolodexGroupLoader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexGroupLoader;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crm/RealRolodexGroupLoader;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListGroupsResponse;",
            ">;)V"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-static {v0}, Lcom/squareup/crm/RealRolodexGroupLoader;->access$getThreadEnforcer$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 60
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-static {v0}, Lcom/squareup/crm/RealRolodexGroupLoader;->access$getFailure$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-static {v0}, Lcom/squareup/crm/RealRolodexGroupLoader;->access$getSuccess$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/ListGroupsResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListGroupsResponse;->group:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    .line 65
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-static {p1}, Lcom/squareup/crm/RealRolodexGroupLoader;->access$getFailure$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
