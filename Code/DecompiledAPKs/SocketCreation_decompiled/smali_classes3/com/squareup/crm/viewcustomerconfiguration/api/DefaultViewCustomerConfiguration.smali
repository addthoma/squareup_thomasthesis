.class public final Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;
.super Ljava/lang/Object;
.source "ViewCustomerConfiguration.kt"

# interfaces
.implements Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;",
        "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
        "()V",
        "crmAppointmentsDataRenderer",
        "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;",
        "getCrmAppointmentsDataRenderer",
        "()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;",
        "displayAposOverflow",
        "",
        "getDisplayAposOverflow",
        "()Z",
        "crm-view-customer-configuration_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final crmAppointmentsDataRenderer:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

.field private final displayAposOverflow:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultCrmAppointmentsDataRenderer;

    invoke-direct {v0}, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultCrmAppointmentsDataRenderer;-><init>()V

    check-cast v0, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    iput-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;->crmAppointmentsDataRenderer:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    return-void
.end method


# virtual methods
.method public getCrmAppointmentsDataRenderer()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;->crmAppointmentsDataRenderer:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    return-object v0
.end method

.method public getDisplayAposOverflow()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;->displayAposOverflow:Z

    return v0
.end method
