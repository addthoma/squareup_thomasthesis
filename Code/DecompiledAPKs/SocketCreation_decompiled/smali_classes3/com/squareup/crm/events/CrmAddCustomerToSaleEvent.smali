.class public Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CrmAddCustomerToSaleEvent.java"


# instance fields
.field private final crm_register_add_to_sale_flow:Ljava/lang/String;

.field private final crm_register_event:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "crm_register"

    .line 15
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    iget-object v0, v0, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;->crm_register_event:Ljava/lang/String;

    .line 16
    iput-object p1, p0, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;->crm_register_add_to_sale_flow:Ljava/lang/String;

    return-void
.end method
