.class public Lcom/squareup/crm/FilterTemplateLoader;
.super Ljava/lang/Object;
.source "FilterTemplateLoader.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/FilterTemplateLoader$SharedScope;
    }
.end annotation


# static fields
.field public static final EMPTY_RESULT:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private static SUPPORTED_FILTER_TEMPLATE_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/protos/client/rolodex/Filter$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final results:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 43
    new-instance v0, Ljava/util/LinkedHashSet;

    const/16 v1, 0xc

    new-array v1, v1, [Lcom/squareup/protos/client/rolodex/Filter$Type;

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_TEXT:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_PHONE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_EMAIL:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v3, 0x8

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v3, 0x9

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v3, 0xa

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/rolodex/Filter$Type;->IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v3, 0xb

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/squareup/crm/FilterTemplateLoader;->SUPPORTED_FILTER_TEMPLATE_TYPES:Ljava/util/Set;

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/crm/FilterTemplateLoader;->EMPTY_RESULT:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    sget-object v0, Lcom/squareup/crm/FilterTemplateLoader;->EMPTY_RESULT:Ljava/util/List;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 69
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 70
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 71
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 75
    iput-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader;->features:Lcom/squareup/settings/server/Features;

    .line 76
    iput-object p2, p0, Lcom/squareup/crm/FilterTemplateLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 77
    iput-object p3, p0, Lcom/squareup/crm/FilterTemplateLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-void
.end method


# virtual methods
.method public failure()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method filterBySupportedFilterTemplateTypes(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 134
    sget-object v2, Lcom/squareup/crm/FilterTemplateLoader;->SUPPORTED_FILTER_TEMPLATE_TYPES:Ljava/util/Set;

    iget-object v3, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 136
    :cond_1
    iget-object v2, p0, Lcom/squareup/crm/FilterTemplateLoader;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    if-ne v2, v3, :cond_2

    .line 137
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 138
    :cond_2
    iget-object v2, p0, Lcom/squareup/crm/FilterTemplateLoader;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

    if-ne v2, v3, :cond_3

    .line 139
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_3
    iget-object v2, p0, Lcom/squareup/crm/FilterTemplateLoader;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Type;->X_PAYMENTS_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    if-ne v2, v3, :cond_0

    .line 142
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    return-object v0
.end method

.method public synthetic lambda$null$3$FilterTemplateLoader(Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListFilterTemplatesResponse;->filters:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$4$FilterTemplateLoader(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 102
    iget-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$FilterTemplateLoader(Lcom/squareup/connectivity/InternetState;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p1, v0, :cond_0

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-eq p2, p1, :cond_0

    .line 86
    iget-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$FilterTemplateLoader(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 92
    iget-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$FilterTemplateLoader()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$5$FilterTemplateLoader(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    new-instance v0, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$ToWClQBgF0FwN0ttPNUZk6X2z7o;

    invoke-direct {v0, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$ToWClQBgF0FwN0ttPNUZk6X2z7o;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    new-instance v1, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$4lNMmXoePhE6HlHmbefII7f9ZYU;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$4lNMmXoePhE6HlHmbefII7f9ZYU;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 81
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 82
    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crm/FilterTemplateLoader;->failure:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 83
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$TiDRnYKYK4e2VCLUOlwSa2_c804;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$TiDRnYKYK4e2VCLUOlwSa2_c804;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    .line 84
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 81
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 91
    invoke-interface {v0}, Lcom/squareup/crm/RolodexServiceHelper;->listFilterTemplates()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$QERj3rw3VP_psZ7DlK8ujBxA-iU;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$QERj3rw3VP_psZ7DlK8ujBxA-iU;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    .line 92
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$w8CUmjPXpiC2m-LL1q7QsD1bOo4;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$w8CUmjPXpiC2m-LL1q7QsD1bOo4;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crm/FilterTemplateLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 95
    invoke-static {v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->resubscribeWhen(Lio/reactivex/Observable;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$skX8BwEU291qXtn4cun5FgrdyZo;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$FilterTemplateLoader$skX8BwEU291qXtn4cun5FgrdyZo;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 90
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public progress()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->progress:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public results()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader;->results:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/crm/-$$Lambda$gUpSlhVPMmGnLkaMH1xcofeoIiA;

    invoke-direct {v1, p0}, Lcom/squareup/crm/-$$Lambda$gUpSlhVPMmGnLkaMH1xcofeoIiA;-><init>(Lcom/squareup/crm/FilterTemplateLoader;)V

    .line 112
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
