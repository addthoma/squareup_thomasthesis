.class public final synthetic Lcom/squareup/crm/-$$Lambda$gUpSlhVPMmGnLkaMH1xcofeoIiA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/crm/FilterTemplateLoader;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/crm/FilterTemplateLoader;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/-$$Lambda$gUpSlhVPMmGnLkaMH1xcofeoIiA;->f$0:Lcom/squareup/crm/FilterTemplateLoader;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/crm/-$$Lambda$gUpSlhVPMmGnLkaMH1xcofeoIiA;->f$0:Lcom/squareup/crm/FilterTemplateLoader;

    check-cast p1, Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/crm/FilterTemplateLoader;->filterBySupportedFilterTemplateTypes(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
