.class public Lcom/squareup/crm/RealRolodexMerchantLoader;
.super Ljava/lang/Object;
.source "RealRolodexMerchantLoader.kt"

# interfaces
.implements Lcom/squareup/crm/RolodexMerchantLoader;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0017\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u000eH\u0016J\u0008\u0010\u0014\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010\u0007\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\n \u000b*\n\u0012\u0004\u0012\u00020\n\u0018\u00010\t0\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000c\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\u000e0\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/crm/RealRolodexMerchantLoader;",
        "Lcom/squareup/crm/RolodexMerchantLoader;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)V",
        "merchant",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/rolodex/Merchant;",
        "kotlin.jvm.PlatformType",
        "onRefresh",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "Lio/reactivex/Observable;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "refresh",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final merchant:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/Merchant;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rolodex"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 33
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<SuccessOrFailure<Merchant>>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->merchant:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getMerchant$p(Lcom/squareup/crm/RealRolodexMerchantLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->merchant:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getRolodex$p(Lcom/squareup/crm/RealRolodexMerchantLoader;)Lcom/squareup/crm/RolodexServiceHelper;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-object p0
.end method


# virtual methods
.method public merchant()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/Merchant;",
            ">;>;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->merchant:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/crm/RealRolodexMerchantLoader$merchant$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexMerchantLoader$merchant$1;-><init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "merchant.doOnSubscribe {\u2026t.hasValue()) refresh() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 38
    new-instance v1, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$1;-><init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onRefresh\n        .switc\u2026t }\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v1, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$2;-><init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 47
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 48
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    sget-object v1, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$3;->INSTANCE:Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$3;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 50
    new-instance v1, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;-><init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "connectivityMonitor.inte\u2026nt.value is ShowFailure }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v1, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$5;

    invoke-direct {v1, p0}, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$5;-><init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/crm/RealRolodexMerchantLoader;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
