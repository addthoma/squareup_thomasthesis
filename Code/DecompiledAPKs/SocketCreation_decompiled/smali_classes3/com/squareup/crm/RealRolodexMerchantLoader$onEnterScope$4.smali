.class final Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;
.super Ljava/lang/Object;
.source "RealRolodexMerchantLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexMerchantLoader;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lcom/squareup/connectivity/InternetState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/connectivity/InternetState;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crm/RealRolodexMerchantLoader;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RealRolodexMerchantLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexMerchantLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final test(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;->this$0:Lcom/squareup/crm/RealRolodexMerchantLoader;

    invoke-static {p1}, Lcom/squareup/crm/RealRolodexMerchantLoader;->access$getMerchant$p(Lcom/squareup/crm/RealRolodexMerchantLoader;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    return p1
.end method

.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexMerchantLoader$onEnterScope$4;->test(Lcom/squareup/connectivity/InternetState;)Z

    move-result p1

    return p1
.end method
