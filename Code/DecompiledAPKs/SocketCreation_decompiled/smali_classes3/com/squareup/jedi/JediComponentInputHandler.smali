.class public interface abstract Lcom/squareup/jedi/JediComponentInputHandler;
.super Ljava/lang/Object;
.source "JediComponentInputHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediComponentInputHandler$None;
    }
.end annotation


# virtual methods
.method public abstract canPressButton()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onCall(Ljava/lang/String;)V
.end method

.method public abstract onLink(Landroid/content/Context;Ljava/lang/String;)V
.end method

.method public abstract onMessageUs()V
.end method

.method public abstract onRequiredTextInput(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract onSearchResultButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onTextInput(Ljava/lang/String;Ljava/lang/String;)V
.end method
