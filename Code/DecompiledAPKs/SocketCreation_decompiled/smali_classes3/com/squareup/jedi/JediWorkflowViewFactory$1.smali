.class final Lcom/squareup/jedi/JediWorkflowViewFactory$1;
.super Lkotlin/jvm/internal/Lambda;
.source "JediWorkflowViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/JediWorkflowViewFactory;-><init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/jedi/JediWorkflowScreenCoordinator;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/jedi/JediWorkflowScreenCoordinator;",
        "stacks",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/jedi/JediWorkflowScreen;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $device:Lcom/squareup/util/Device;

.field final synthetic $glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field final synthetic $messagingController:Lcom/squareup/ui/help/MessagingController;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p3, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$device:Lcom/squareup/util/Device;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/jedi/JediWorkflowScreenCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/jedi/JediWorkflowScreenCoordinator;"
        }
    .end annotation

    const-string v0, "stacks"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    .line 25
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$messagingController:Lcom/squareup/ui/help/MessagingController;

    .line 26
    iget-object v2, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 27
    iget-object v3, p0, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->$device:Lcom/squareup/util/Device;

    .line 24
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;-><init>(Lcom/squareup/ui/help/MessagingController;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/Device;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/JediWorkflowViewFactory$1;->invoke(Lio/reactivex/Observable;)Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    move-result-object p1

    return-object p1
.end method
