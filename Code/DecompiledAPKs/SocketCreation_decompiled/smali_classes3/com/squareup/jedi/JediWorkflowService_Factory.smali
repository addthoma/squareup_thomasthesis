.class public final Lcom/squareup/jedi/JediWorkflowService_Factory;
.super Ljava/lang/Object;
.source "JediWorkflowService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/jedi/JediWorkflowService;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/CustomJediComponentItemsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/CustomJediComponentItemsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p6, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p7, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/jedi/JediWorkflowService_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/JediHelpSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/help/JediService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jedi/CustomJediComponentItemsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;)",
            "Lcom/squareup/jedi/JediWorkflowService_Factory;"
        }
    .end annotation

    .line 53
    new-instance v8, Lcom/squareup/jedi/JediWorkflowService_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/jedi/JediWorkflowService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/server/help/JediService;Ljava/util/Locale;Lcom/squareup/jedi/CustomJediComponentItemsFactory;Landroid/content/res/Resources;Landroid/telephony/TelephonyManager;)Lcom/squareup/jedi/JediWorkflowService;
    .locals 9

    .line 59
    new-instance v8, Lcom/squareup/jedi/JediWorkflowService;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/jedi/JediWorkflowService;-><init>(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/server/help/JediService;Ljava/util/Locale;Lcom/squareup/jedi/CustomJediComponentItemsFactory;Landroid/content/res/Resources;Landroid/telephony/TelephonyManager;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/jedi/JediWorkflowService;
    .locals 8

    .line 46
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/jedi/JediHelpSettings;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/UserSettingsProvider;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/server/help/JediService;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/jedi/CustomJediComponentItemsFactory;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/telephony/TelephonyManager;

    invoke-static/range {v1 .. v7}, Lcom/squareup/jedi/JediWorkflowService_Factory;->newInstance(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/server/help/JediService;Ljava/util/Locale;Lcom/squareup/jedi/CustomJediComponentItemsFactory;Landroid/content/res/Resources;Landroid/telephony/TelephonyManager;)Lcom/squareup/jedi/JediWorkflowService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/jedi/JediWorkflowService_Factory;->get()Lcom/squareup/jedi/JediWorkflowService;

    move-result-object v0

    return-object v0
.end method
