.class public final Lcom/squareup/jedi/ui/JediPanelView;
.super Landroid/widget/LinearLayout;
.source "JediPanelView.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010$\u001a\u00020\u001bH\u0016J\u0018\u0010%\u001a\u00020\u001b2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u0010\u0010*\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020\u001b2\u0006\u0010.\u001a\u00020\u0015H\u0002J\u0010\u0010/\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u00100\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020\u0015H\u0002J\u0008\u00101\u001a\u00020\u001bH\u0014J\u0016\u00102\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020\u00152\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a8F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u001b0\u001b0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u001a8F\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010\u001dR\u001c\u0010#\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010!0!0\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/jedi/ui/JediPanelView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "componentsRecyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "inputHandler",
        "Lcom/squareup/jedi/JediComponentInputHandler;",
        "jediBanner",
        "Lcom/squareup/jedi/ui/JediBannerView;",
        "jediErrorView",
        "Lcom/squareup/ui/EmptyView;",
        "jediProgressBar",
        "Landroid/widget/ProgressBar;",
        "jediSearchBar",
        "Lcom/squareup/ui/XableEditText;",
        "panelState",
        "Lio/reactivex/subjects/BehaviorSubject;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "kotlin.jvm.PlatformType",
        "res",
        "Landroid/content/res/Resources;",
        "searchClear",
        "Lio/reactivex/Observable;",
        "",
        "getSearchClear",
        "()Lio/reactivex/Observable;",
        "searchClearRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "searchText",
        "",
        "getSearchText",
        "searchTextRelay",
        "clearFocus",
        "fadeInOrOut",
        "view",
        "Landroid/view/View;",
        "fadeIn",
        "",
        "maybeShowBanner",
        "screenData",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "maybeShowError",
        "errorScreenData",
        "maybeShowSearchBar",
        "maybeShowSuccess",
        "onAttachedToWindow",
        "update",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final componentsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

.field private final jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

.field private final jediErrorView:Lcom/squareup/ui/EmptyView;

.field private final jediProgressBar:Landroid/widget/ProgressBar;

.field private final jediSearchBar:Lcom/squareup/ui/XableEditText;

.field private final panelState:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;

.field private final searchClearRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchTextRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object p2

    const-string v0, "BehaviorSubject.create<JediHelpScreenData>()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->panelState:Lio/reactivex/subjects/BehaviorSubject;

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const-string v0, "context.resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->res:Landroid/content/res/Resources;

    .line 82
    new-instance p2, Lcom/squareup/jedi/JediComponentInputHandler$None;

    invoke-direct {p2}, Lcom/squareup/jedi/JediComponentInputHandler$None;-><init>()V

    check-cast p2, Lcom/squareup/jedi/JediComponentInputHandler;

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

    .line 84
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string v0, "BehaviorRelay.create<String>()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchTextRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 85
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string v0, "BehaviorRelay.create<Unit>()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchClearRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 94
    sget p2, Lcom/squareup/jedi/impl/R$layout;->jedi_panel_view:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 95
    sget p2, Lcom/squareup/jedi/impl/R$id;->jedi_banner:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.jedi_banner)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/jedi/ui/JediBannerView;

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    .line 96
    sget p2, Lcom/squareup/jedi/impl/R$id;->jedi_search_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.jedi_search_bar)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/ui/XableEditText;

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    .line 97
    sget p2, Lcom/squareup/jedi/impl/R$id;->jedi_progress_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.jedi_progress_bar)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediProgressBar:Landroid/widget/ProgressBar;

    .line 98
    sget p2, Lcom/squareup/jedi/impl/R$id;->components_recycler:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.components_recycler)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->componentsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 99
    sget p2, Lcom/squareup/jedi/impl/R$id;->jedi_error:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.jedi_error)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/EmptyView;

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    return-void
.end method

.method public static final synthetic access$fadeInOrOut(Lcom/squareup/jedi/ui/JediPanelView;Landroid/view/View;Z)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/squareup/jedi/ui/JediPanelView;->fadeInOrOut(Landroid/view/View;Z)V

    return-void
.end method

.method public static final synthetic access$getComponentsRecyclerView$p(Lcom/squareup/jedi/ui/JediPanelView;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->componentsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method public static final synthetic access$getJediErrorView$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/EmptyView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    return-object p0
.end method

.method public static final synthetic access$getJediProgressBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Landroid/widget/ProgressBar;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediProgressBar:Landroid/widget/ProgressBar;

    return-object p0
.end method

.method public static final synthetic access$getJediSearchBar$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/squareup/ui/XableEditText;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    return-object p0
.end method

.method public static final synthetic access$getPanelState$p(Lcom/squareup/jedi/ui/JediPanelView;)Lio/reactivex/subjects/BehaviorSubject;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->panelState:Lio/reactivex/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getSearchClearRelay$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchClearRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getSearchTextRelay$p(Lcom/squareup/jedi/ui/JediPanelView;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchTextRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$maybeShowError(Lcom/squareup/jedi/ui/JediPanelView;Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->maybeShowError(Lcom/squareup/jedi/JediHelpScreenData;)V

    return-void
.end method

.method public static final synthetic access$maybeShowSuccess(Lcom/squareup/jedi/ui/JediPanelView;Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->maybeShowSuccess(Lcom/squareup/jedi/JediHelpScreenData;)V

    return-void
.end method

.method private final fadeInOrOut(Landroid/view/View;Z)V
    .locals 2

    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-eqz p2, :cond_0

    .line 221
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 223
    :cond_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method private final maybeShowBanner(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 3

    .line 187
    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getBanner()Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 189
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/squareup/jedi/ui/JediPanelView;->fadeInOrOut(Landroid/view/View;Z)V

    .line 190
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;->label()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/jedi/ui/JediBannerView;->setLabel(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/JediPanelView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 193
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;->state()Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/jedi/ui/JediPanelView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem$BannerState;->ordinal()I

    move-result p1

    aget p1, v2, p1

    if-eq p1, v1, :cond_3

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    goto :goto_0

    .line 199
    :cond_1
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/jedi/ui/JediBannerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 197
    :cond_2
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/jedi/ui/JediBannerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 195
    :cond_3
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediBanner:Lcom/squareup/jedi/ui/JediBannerView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_red:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/jedi/ui/JediBannerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private final maybeShowError(Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 2

    .line 204
    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    if-nez v0, :cond_0

    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 209
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyphColor(I)V

    .line 210
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setTitle(Ljava/lang/CharSequence;)V

    .line 211
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediErrorView:Lcom/squareup/ui/EmptyView;

    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/EmptyView;->setTitleColorRes(I)V

    return-void
.end method

.method private final maybeShowSearchBar(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 3

    .line 146
    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getDisplaySearchBar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView;->res:Landroid/content/res/Resources;

    sget v2, Lcom/squareup/jedi/impl/R$string;->search_hint_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSearchText()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    new-instance v0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$1;-><init>(Lcom/squareup/jedi/ui/JediPanelView;)V

    check-cast v0, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    new-instance v0, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/ui/JediPanelView$maybeShowSearchBar$2;-><init>(Lcom/squareup/jedi/ui/JediPanelView;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setOnButtonClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final maybeShowSuccess(Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 2

    .line 134
    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    if-nez v0, :cond_0

    return-void

    .line 138
    :cond_0
    new-instance v0, Lcom/squareup/jedi/ui/JediComponentsAdapter;

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView;->inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

    invoke-direct {v0, p1, v1, p0}, Lcom/squareup/jedi/ui/JediComponentsAdapter;-><init>(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/ui/JediPanelView;)V

    .line 139
    iget-object v1, p0, Lcom/squareup/jedi/ui/JediPanelView;->componentsRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->maybeShowSearchBar(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    .line 142
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediPanelView;->maybeShowBanner(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    return-void
.end method


# virtual methods
.method public clearFocus()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->jediSearchBar:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->clearFocus()V

    return-void
.end method

.method public final getSearchClear()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchClearRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final getSearchText()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediPanelView;->searchTextRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 103
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 105
    move-object v0, p0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/ui/JediPanelView$onAttachedToWindow$1;-><init>(Lcom/squareup/jedi/ui/JediPanelView;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public final update(Lcom/squareup/jedi/JediHelpScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 1

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iput-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->inputHandler:Lcom/squareup/jedi/JediComponentInputHandler;

    .line 130
    iget-object p2, p0, Lcom/squareup/jedi/ui/JediPanelView;->panelState:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {p2, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
