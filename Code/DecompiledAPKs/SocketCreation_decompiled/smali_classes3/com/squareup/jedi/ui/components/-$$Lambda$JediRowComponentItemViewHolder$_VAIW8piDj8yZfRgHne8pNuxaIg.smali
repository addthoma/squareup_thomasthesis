.class public final synthetic Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;

.field private final synthetic f$1:Lcom/squareup/jedi/ui/components/JediRowComponentItem;

.field private final synthetic f$2:Lcom/squareup/jedi/JediComponentInputHandler;

.field private final synthetic f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$0:Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;

    iput-object p2, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$1:Lcom/squareup/jedi/ui/components/JediRowComponentItem;

    iput-object p3, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$2:Lcom/squareup/jedi/JediComponentInputHandler;

    iput-object p4, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$0:Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;

    iget-object v1, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$1:Lcom/squareup/jedi/ui/components/JediRowComponentItem;

    iget-object v2, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$2:Lcom/squareup/jedi/JediComponentInputHandler;

    iget-object v3, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;->f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->lambda$onBind$0$JediRowComponentItemViewHolder(Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Landroid/view/View;)V

    return-void
.end method
