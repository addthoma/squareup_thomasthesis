.class public Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;
.super Ljava/lang/Object;
.source "HideJediLinkTagParser.java"

# interfaces
.implements Lcom/squareup/text/html/TagParser;


# static fields
.field private static final A_TAG:Ljava/lang/String; = "a"

.field private static final CUSTOM_A_TAG:Ljava/lang/String; = "sq_a"

.field private static JEDI_LINK_CHAR:C = '>'


# instance fields
.field private startIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 28
    iput v0, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    return-void
.end method

.method private static closeATag(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static openATag(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 1

    const-string p4, "sq_a"

    .line 39
    invoke-virtual {p2, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_3

    if-eqz p1, :cond_0

    .line 42
    invoke-interface {p3}, Landroid/text/Editable;->length()I

    move-result p1

    iput p1, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    goto :goto_0

    .line 44
    :cond_0
    iget p1, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    return-void

    .line 49
    :cond_1
    invoke-interface {p3}, Landroid/text/Editable;->length()I

    move-result p1

    .line 52
    iget p4, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    invoke-interface {p3, p4, p1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p4

    sget-char v0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->JEDI_LINK_CHAR:C

    invoke-static {p4, v0}, Lcom/squareup/util/Strings;->endsWithCharIgnoringSpaces(Ljava/lang/CharSequence;C)Z

    move-result p4

    if-eqz p4, :cond_2

    .line 53
    iget p4, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    invoke-interface {p3, p4, p1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 57
    :cond_2
    iput p2, p0, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->startIndex:I

    :cond_3
    :goto_0
    return-void
.end method

.method public tagsToHandle()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v1, "a"

    .line 32
    invoke-static {v1}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->openATag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "sq_a"

    invoke-static {v3}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->openATag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    invoke-static {v1}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->closeATag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;->closeATag(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
