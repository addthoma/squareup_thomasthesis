.class public Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediInstantAnswerComponentItem.java"


# instance fields
.field private resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;Landroid/content/res/Resources;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 18
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 19
    iput-object p2, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method private text()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    .line 50
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public hasUrl()Z
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->url()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public htmlText()Ljava/lang/CharSequence;
    .locals 4

    .line 27
    invoke-direct {p0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->text()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/text/html/TagParser;

    new-instance v2, Lcom/squareup/text/html/ListTagParser;

    iget-object v3, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->resources:Landroid/content/res/Resources;

    .line 28
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-direct {v2, v3}, Lcom/squareup/text/html/ListTagParser;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 27
    invoke-static {v0, v1}, Lcom/squareup/text/html/HtmlText;->fromHtml(Ljava/lang/String;[Lcom/squareup/text/html/TagParser;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public htmlTextWithoutLinks()Ljava/lang/CharSequence;
    .locals 4

    .line 32
    invoke-direct {p0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->text()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/text/html/TagParser;

    new-instance v2, Lcom/squareup/text/html/ListTagParser;

    iget-object v3, p0, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->resources:Landroid/content/res/Resources;

    .line 33
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-direct {v2, v3}, Lcom/squareup/text/html/ListTagParser;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;

    invoke-direct {v2}, Lcom/squareup/jedi/ui/components/HideJediLinkTagParser;-><init>()V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 32
    invoke-static {v0, v1}, Lcom/squareup/text/html/HtmlText;->fromHtml(Ljava/lang/String;[Lcom/squareup/text/html/TagParser;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 38
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public title()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "title"

    .line 23
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public url()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "url"

    .line 46
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
