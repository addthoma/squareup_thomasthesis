.class public final synthetic Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

.field private final synthetic f$1:Lcom/squareup/jedi/JediComponentInputHandler;

.field private final synthetic f$2:Landroid/content/Context;

.field private final synthetic f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Landroid/content/Context;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$0:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

    iput-object p2, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$1:Lcom/squareup/jedi/JediComponentInputHandler;

    iput-object p3, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$2:Landroid/content/Context;

    iput-object p4, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$0:Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

    iget-object v1, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$1:Lcom/squareup/jedi/JediComponentInputHandler;

    iget-object v2, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$2:Landroid/content/Context;

    iget-object v3, p0, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;->f$3:Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->lambda$onBind$0(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Landroid/content/Context;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Landroid/view/View;)V

    return-void
.end method
