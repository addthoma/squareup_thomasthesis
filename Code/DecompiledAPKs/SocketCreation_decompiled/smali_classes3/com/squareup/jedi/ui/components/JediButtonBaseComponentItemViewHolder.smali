.class public Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediButtonBaseComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 22
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_button_base_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Landroid/content/Context;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Landroid/view/View;)V
    .locals 0

    .line 56
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->hasUrl()Z

    move-result p4

    if-eqz p4, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->url()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p2, p0}, Lcom/squareup/jedi/JediComponentInputHandler;->onLink(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_0
    iget-object p0, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object p0, p0, Lcom/squareup/protos/jedi/service/Component;->name:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSessionToken()Ljava/lang/String;

    move-result-object p2

    .line 60
    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getTransitionId()Ljava/lang/String;

    move-result-object p3

    .line 59
    invoke-interface {p1, p0, p2, p3}, Lcom/squareup/jedi/JediComponentInputHandler;->onButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$onBind$1(Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/marketfont/MarketButton;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 67
    invoke-interface {p0}, Lcom/squareup/jedi/JediComponentInputHandler;->canPressButton()Lio/reactivex/Observable;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/jedi/ui/components/-$$Lambda$wdBCQ-wgZO5DQ4njtGBMuPRW9a8;

    invoke-direct {v0, p1}, Lcom/squareup/jedi/ui/components/-$$Lambda$wdBCQ-wgZO5DQ4njtGBMuPRW9a8;-><init>(Lcom/squareup/marketfont/MarketButton;)V

    .line 68
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 6

    .line 27
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/jedi/impl/R$id;->button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    .line 28
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->label()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 30
    iget-object v1, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 31
    iget-object v2, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 33
    sget-object v3, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder$1;->$SwitchMap$com$squareup$jedi$ui$components$JediButtonBaseComponentItem$ButtonType:[I

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->type()Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    const/16 v5, 0x11

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 46
    sget v3, Lcom/squareup/noho/R$drawable;->transparent_selector:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 47
    sget v2, Lcom/squareup/marin/R$style;->TextAppearance_Marin_Medium_Blue:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 49
    invoke-virtual {v0, v4}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    goto :goto_0

    .line 52
    :cond_0
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unhandled ButtonBase Component ButtonType:"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->type()Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem$ButtonType;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 41
    :cond_1
    sget v3, Lcom/squareup/marin/R$drawable;->marin_selector_button:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 42
    sget v2, Lcom/squareup/marin/R$style;->Widget_Marin_Button:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 43
    invoke-virtual {v0, v5}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    goto :goto_0

    .line 35
    :cond_2
    sget v3, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 36
    sget v2, Lcom/squareup/marin/R$style;->TextAppearance_Marin_Medium_White:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marketfont/MarketButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 38
    invoke-virtual {v0, v5}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 55
    :goto_0
    new-instance v2, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;

    invoke-direct {v2, p1, p3, v1, p2}, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$hWvqJ49g2RVlwQhkAkCeIxuB-Tg;-><init>(Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Landroid/content/Context;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;->validate()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 66
    iget-object p1, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItemViewHolder;->itemView:Landroid/view/View;

    new-instance p2, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$ey3B7lxpVyPCPXzwAJ0USoal9Es;

    invoke-direct {p2, p3, v0}, Lcom/squareup/jedi/ui/components/-$$Lambda$JediButtonBaseComponentItemViewHolder$ey3B7lxpVyPCPXzwAJ0USoal9Es;-><init>(Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/marketfont/MarketButton;)V

    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_3
    return-void
.end method
