.class public final enum Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
.super Ljava/lang/Enum;
.source "JediRowComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/ui/components/JediRowComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

.field public static final enum HOURS:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

.field public static final enum NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

.field public static final enum PHONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

.field public static final enum TOKEN:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 17
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v1, 0x0

    const-string v2, "NONE"

    const-string v3, "none"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    .line 18
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v2, 0x1

    const-string v3, "PHONE"

    const-string v4, "phone"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->PHONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    .line 19
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v3, 0x2

    const-string v4, "HOURS"

    const-string v5, "hours"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->HOURS:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    .line 20
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v4, 0x3

    const-string v5, "TOKEN"

    const-string/jumbo v6, "token"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->TOKEN:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    .line 16
    sget-object v5, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->PHONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->HOURS:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->TOKEN:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 0

    .line 16
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    move-result-object p0

    return-object p0
.end method

.method private static fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 5

    .line 30
    invoke-static {}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->values()[Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 31
    iget-object v4, v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    invoke-virtual {v0}, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    return-object v0
.end method
