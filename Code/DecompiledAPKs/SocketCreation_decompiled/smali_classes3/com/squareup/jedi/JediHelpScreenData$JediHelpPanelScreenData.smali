.class public final Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
.super Lcom/squareup/jedi/JediHelpScreenData;
.source "JediHelpScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/JediHelpScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "JediHelpPanelScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001Bs\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000b\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\u0010\u0008\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0011J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010\"\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\rH\u00c6\u0003J\t\u0010$\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u00c6\u0003Jy\u0010\'\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000b2\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\u0010\u0008\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u00c6\u0001J\t\u0010(\u001a\u00020)H\u00d6\u0001J\u0013\u0010*\u001a\u00020\u000b2\u0008\u0010+\u001a\u0004\u0018\u00010,H\u00d6\u0003J\t\u0010-\u001a\u00020)H\u00d6\u0001J\t\u0010.\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010/\u001a\u0002002\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u00020)H\u00d6\u0001R\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0019\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001bR\u0010\u0010\u000e\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001b\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "transitionId",
        "",
        "sessionToken",
        "components",
        "",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        "header",
        "Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;",
        "displaySearchBar",
        "",
        "banner",
        "Lcom/squareup/jedi/ui/components/JediBannerComponentItem;",
        "showInlineLinks",
        "searchText",
        "searchResults",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)V",
        "getBanner",
        "()Lcom/squareup/jedi/ui/components/JediBannerComponentItem;",
        "getDisplaySearchBar",
        "()Z",
        "getHeader",
        "()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;",
        "getSearchResults",
        "()Ljava/util/List;",
        "getSearchText",
        "()Ljava/lang/String;",
        "getSessionToken",
        "getTransitionId",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

.field public final components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final displaySearchBar:Z

.field private final header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final searchText:Ljava/lang/String;

.field private final sessionToken:Ljava/lang/String;

.field public final showInlineLinks:Z

.field private final transitionId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData$Creator;

    invoke-direct {v0}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData$Creator;-><init>()V

    sput-object v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;",
            "Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;",
            "Z",
            "Lcom/squareup/jedi/ui/components/JediBannerComponentItem;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sessionToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "components"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, v0}, Lcom/squareup/jedi/JediHelpScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    iput-boolean p5, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    iput-object p6, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    iput-boolean p7, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    iput-object p8, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    iput-object p9, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 13

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 45
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object v4, p1

    :goto_0
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_1

    .line 47
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v6, v1

    goto :goto_1

    :cond_1
    move-object/from16 v6, p3

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 48
    move-object v1, v2

    check-cast v1, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    move-object v7, v1

    goto :goto_2

    :cond_2
    move-object/from16 v7, p4

    :goto_2
    and-int/lit8 v1, v0, 0x10

    const/4 v3, 0x0

    if-eqz v1, :cond_3

    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    move/from16 v8, p5

    :goto_3
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_4

    .line 50
    move-object v1, v2

    check-cast v1, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    move-object v9, v1

    goto :goto_4

    :cond_4
    move-object/from16 v9, p6

    :goto_4
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_5

    const/4 v10, 0x0

    goto :goto_5

    :cond_5
    move/from16 v10, p7

    :goto_5
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_6

    .line 52
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v11, v1

    goto :goto_6

    :cond_6
    move-object/from16 v11, p8

    :goto_6
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 53
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    move-object v12, v0

    goto :goto_7

    :cond_7
    move-object/from16 v12, p9

    :goto_7
    move-object v3, p0

    move-object v5, p2

    invoke-direct/range {v3 .. v12}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    return v0
.end method

.method public final component6()Lcom/squareup/jedi/ui/components/JediBannerComponentItem;
    .locals 1

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    return v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public final component9()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;",
            "Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;",
            "Z",
            "Lcom/squareup/jedi/ui/components/JediBannerComponentItem;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;)",
            "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;"
        }
    .end annotation

    const-string v0, "sessionToken"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "components"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-object v1, v0

    move-object v2, p1

    move-object v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    iget-boolean v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    iget-boolean v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBanner()Lcom/squareup/jedi/ui/components/JediBannerComponentItem;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    return-object v0
.end method

.method public final getDisplaySearchBar()Z
    .locals 1

    .line 49
    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    return v0
.end method

.method public final getHeader()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    return-object v0
.end method

.method public final getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/jedi/ui/JediComponentItem;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public final getSearchText()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public final getSessionToken()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransitionId()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JediHelpPanelScreenData(transitionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", sessionToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", components="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displaySearchBar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showInlineLinks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", searchResults="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->transitionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->sessionToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/jedi/ui/JediComponentItem;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->header:Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->displaySearchBar:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->banner:Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->searchResults:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/jedi/ui/JediComponentItem;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    :cond_2
    return-void
.end method
