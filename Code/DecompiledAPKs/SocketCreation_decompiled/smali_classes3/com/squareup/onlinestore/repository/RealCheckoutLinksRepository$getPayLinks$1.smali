.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->getPayLinks(Ljava/lang/String;IIZ)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckoutLinksRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckoutLinksRepository.kt\ncom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,350:1\n704#2:351\n777#2,2:352\n1642#2,2:354\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckoutLinksRepository.kt\ncom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1\n*L\n257#1:351\n257#1,2:352\n261#1,2:354\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $includeDonations:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;->$includeDonations:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_8

    .line 255
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 256
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->getData()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 351
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 352
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;

    .line 259
    iget-boolean v7, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;->$includeDonations:Z

    if-nez v7, :cond_1

    invoke-virtual {v6}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getHasBuyerControlledPrice()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v6}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountMoney()Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_2

    :cond_1
    const/4 v4, 0x1

    :cond_2
    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 354
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;

    .line 264
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getHasBuyerControlledPrice()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 265
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    new-instance v6, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    .line 266
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getId()Ljava/lang/String;

    move-result-object v7

    .line 267
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getName()Ljava/lang/String;

    move-result-object v8

    .line 268
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getEnabled()Z

    move-result v2

    .line 265
    invoke-direct {v6, v7, v8, v2}, Lcom/squareup/onlinestore/repository/PayLink$Donation;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 272
    :cond_4
    :try_start_0
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountCurrency()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    move-object v6, v0

    check-cast v6, Ljava/util/Collection;

    new-instance v7, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    .line 278
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getId()Ljava/lang/String;

    move-result-object v8

    .line 279
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getName()Ljava/lang/String;

    move-result-object v9

    .line 280
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getAmountMoney()Ljava/lang/Long;

    move-result-object v10

    if-nez v10, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v3, v10, v11}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 281
    invoke-virtual {v2}, Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;->getEnabled()Z

    move-result v2

    .line 277
    invoke-direct {v7, v8, v9, v3, v2}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    nop

    goto :goto_1

    .line 285
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    .line 286
    new-instance v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->getCurrentPage()I

    move-result v2

    invoke-virtual {p1}, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->getNextPageUrl()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    const/4 v4, 0x1

    :cond_7
    invoke-direct {v1, v0, v2, v4}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Success;-><init>(Ljava/util/List;IZ)V

    .line 285
    check-cast v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;

    goto :goto_2

    .line 289
    :cond_8
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_9

    sget-object p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Error;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult$Error;

    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;

    :goto_2
    return-object v1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$getPayLinks$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;

    move-result-object p1

    return-object p1
.end method
