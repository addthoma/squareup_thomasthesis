.class final Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;
.super Ljava/lang/Object;
.source "RealCheckoutLinksRepository.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->checkCheckoutLinksEnabled()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/onlinestore/api/squaresync/MerchantSquareSyncStatusResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;->this$0:Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-static {v0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;->access$processMerchantTokenResponse(Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository$checkCheckoutLinksEnabled$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;

    move-result-object p1

    return-object p1
.end method
