.class public interface abstract Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
.super Ljava/lang/Object;
.source "CheckoutLinksRepository.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;,
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;,
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;,
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;,
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;,
        Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001:\u0006 !\"#$%J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u001e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0008H&J&\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u000cH&J\u001e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0008H&J\u001e\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0008H&J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0003H&J2\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u00172\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u000eH&J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0003H&J\u001e\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u001e\u001a\u00020\u001fH&\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "",
        "checkCheckoutLinksEnabled",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;",
        "createDonationLink",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
        "jwtToken",
        "",
        "name",
        "createPayLink",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "deletePayLink",
        "",
        "payLinkId",
        "enableEcomAvailableForItem",
        "merchantCatalogObjectToken",
        "getJwtTokenForSquareSync",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "getPayLinks",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
        "page",
        "",
        "perPageCount",
        "includeDonations",
        "merchantExists",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;",
        "updatePayLink",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
        "payLink",
        "Lcom/squareup/onlinestore/repository/PayLink;",
        "CheckCheckoutLinksEnabledResult",
        "CreatePayLinkResult",
        "MerchantExistsResult",
        "PayLinksResult",
        "SquareSyncJwtTokenResult",
        "UpdatePayLinkResult",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract checkCheckoutLinksEnabled()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CheckCheckoutLinksEnabledResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createDonationLink(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract createPayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$CreatePayLinkResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract deletePayLink(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract enableEcomAvailableForItem(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getJwtTokenForSquareSync()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPayLinks(Ljava/lang/String;IIZ)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIZ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$PayLinksResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract merchantExists()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$MerchantExistsResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updatePayLink(Ljava/lang/String;Lcom/squareup/onlinestore/repository/PayLink;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/onlinestore/repository/PayLink;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;",
            ">;"
        }
    .end annotation
.end method
