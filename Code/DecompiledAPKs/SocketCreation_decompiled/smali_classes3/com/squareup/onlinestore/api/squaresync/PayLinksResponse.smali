.class public final Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;
.super Ljava/lang/Object;
.source "PayLinkResponse.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J/\u0010\u0013\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u001c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00078\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0018\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;",
        "",
        "nextPageUrl",
        "",
        "currentPage",
        "",
        "data",
        "",
        "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
        "(Ljava/lang/String;ILjava/util/List;)V",
        "getCurrentPage",
        "()I",
        "getData",
        "()Ljava/util/List;",
        "getNextPageUrl",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentPage:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "current_page"
    .end annotation
.end field

.field private final data:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final nextPageUrl:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "next_page_url"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    iput-object p3, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;Ljava/lang/String;ILjava/util/List;ILjava/lang/Object;)Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->copy(Ljava/lang/String;ILjava/util/List;)Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    return v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;ILjava/util/List;)Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;)",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;

    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    iget v1, p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentPage()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    return v0
.end method

.method public final getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/onlinestore/api/squaresync/PayLinkResponse;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    return-object v0
.end method

.method public final getNextPageUrl()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PayLinksResponse(nextPageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->nextPageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", currentPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->currentPage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/api/squaresync/PayLinksResponse;->data:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
