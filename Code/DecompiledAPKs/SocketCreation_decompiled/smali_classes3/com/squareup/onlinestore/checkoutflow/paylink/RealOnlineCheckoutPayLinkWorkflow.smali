.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnlineCheckoutPayLinkWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnlineCheckoutPayLinkWorkflow.kt\ncom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,293:1\n32#2,12:294\n149#3,5:306\n149#3,5:311\n85#4:316\n240#5:317\n276#6:318\n*E\n*S KotlinDebug\n*F\n+ 1 RealOnlineCheckoutPayLinkWorkflow.kt\ncom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow\n*L\n135#1,12:294\n153#1,5:306\n193#1,5:311\n276#1:316\n276#1:317\n276#1:318\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a5\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001 \u0018\u0000 ;2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0003:;<BU\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\u0002\u0010\u001eJ\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0002J\u0012\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010&\u001a\u00020\'H\u0007J\u001e\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+2\u0006\u0010-\u001a\u00020\'2\u0006\u0010.\u001a\u00020\u0015H\u0002J\u0010\u0010/\u001a\u00020\'2\u0006\u00100\u001a\u00020\'H\u0002J\u001a\u00101\u001a\u00020\u00032\u0006\u00102\u001a\u00020\u00022\u0008\u00103\u001a\u0004\u0018\u000104H\u0016J\u0018\u00105\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0002JN\u00106\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u00102\u001a\u00020\u00022\u0006\u00107\u001a\u00020\u00032\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000408H\u0016J\u0010\u00109\u001a\u0002042\u0006\u00107\u001a\u00020\u0003H\u0016R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010!R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkWorkflow;",
        "res",
        "Lcom/squareup/util/Res;",
        "server",
        "Lcom/squareup/http/Server;",
        "checkoutLinkShareSheet",
        "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
        "clipboard",
        "Lcom/squareup/util/Clipboard;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "toastFactory",
        "Lcom/squareup/util/ToastFactory;",
        "qrCodeGenerator",
        "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
        "checkoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "analytics",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
        "(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/util/Clipboard;Lcom/squareup/text/Formatter;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V",
        "logViewEventWorker",
        "com/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;",
        "copyCheckoutLinkUrlToClipboard",
        "",
        "context",
        "Landroid/content/Context;",
        "checkoutLinkUrl",
        "",
        "createCheckoutLinkQrCode",
        "Landroid/graphics/Bitmap;",
        "createPayLink",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;",
        "name",
        "amount",
        "getCheckoutLinkUrl",
        "id",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "openShareSheet",
        "render",
        "state",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "Companion",
        "PayLinkWorkerResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Companion;

.field public static final GET_LINK_WORKER_KEY:Ljava/lang/String; = "get-link-worker"

.field public static final LOG_VIEW_EVENT_WORKER_KEY:Ljava/lang/String; = "log-view-event-worker"


# instance fields
.field private final analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

.field private final checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

.field private final checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

.field private final clipboard:Lcom/squareup/util/Clipboard;

.field private final logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

.field private final res:Lcom/squareup/util/Res;

.field private final server:Lcom/squareup/http/Server;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/util/Clipboard;Lcom/squareup/text/Formatter;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/http/Server;",
            "Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;",
            "Lcom/squareup/util/Clipboard;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/ToastFactory;",
            "Lcom/squareup/qrcodegenerator/QrCodeGenerator;",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
            "Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinkShareSheet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clipboard"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "toastFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "qrCodeGenerator"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinksRepository"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->server:Lcom/squareup/http/Server;

    iput-object p3, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    iput-object p4, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->clipboard:Lcom/squareup/util/Clipboard;

    iput-object p5, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    iput-object p7, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    iput-object p8, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    iput-object p9, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    .line 89
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;

    return-void
.end method

.method public static final synthetic access$copyCheckoutLinkUrlToClipboard(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->copyCheckoutLinkUrlToClipboard(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object p0
.end method

.method public static final synthetic access$openShareSheet(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->openShareSheet(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private final copyCheckoutLinkUrlToClipboard(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->PAY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->clipboard:Lcom/squareup/util/Clipboard;

    .line 226
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_clipboard_label:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 227
    check-cast p2, Ljava/lang/CharSequence;

    .line 225
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/util/Clipboard;->copyPlainText(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 230
    sget p2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_copied_toast_message:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(R.stri\u2026out_copied_toast_message)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->toastFactory:Lcom/squareup/util/ToastFactory;

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private final createPayLink(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;",
            ">;"
        }
    .end annotation

    .line 257
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->checkoutLinksRepository:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    invoke-interface {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->getJwtTokenForSquareSync()Lio/reactivex/Single;

    move-result-object v0

    .line 258
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 275
    sget-object p2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$2;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "checkoutLinksRepository.\u2026rrorReturn { OtherError }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 316
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$$inlined$asWorker$1;

    const/4 v0, 0x0

    invoke-direct {p2, p1, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 317
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 318
    const-class p2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method private final getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->server:Lcom/squareup/http/Server;

    invoke-virtual {v0}, Lcom/squareup/http/Server;->isProduction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_paylink_url:I

    goto :goto_0

    .line 211
    :cond_0
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_paylink_url_staging:I

    .line 214
    :goto_0
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 215
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 217
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final openShareSheet(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .line 238
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->analytics:Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 239
    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->checkoutLinkShareSheet:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;

    .line 241
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_share_sheet_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "context.getString(R.stri\u2026eckout_share_sheet_title)"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_share_sheet_subject:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v0, "context.getString(R.stri\u2026kout_share_sheet_subject)"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    sget-object v7, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;->ONLINE_CHECKOUT_PAY_LINK_SCREEN:Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;

    move-object v3, p1

    move-object v6, p2

    .line 239
    invoke-interface/range {v2 .. v7}, Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;->openShareSheetForCheckoutLink(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet$ShareContext;)V

    return-void
.end method


# virtual methods
.method public final createCheckoutLinkQrCode(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2

    const-string v0, "checkoutLinkUrl"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$dimen;->online_checkout_qr_code_size:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    .line 250
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->qrCodeGenerator:Lcom/squareup/qrcodegenerator/QrCodeGenerator;

    invoke-interface {v1, p1, v0}, Lcom/squareup/qrcodegenerator/QrCodeGenerator;->generate(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public initialState(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    if-eqz p2, :cond_4

    .line 294
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, p1

    :goto_1
    if-eqz p2, :cond_3

    .line 299
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 301
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 302
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 303
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, p1

    .line 305
    :goto_2
    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    if-eqz p2, :cond_4

    goto :goto_3

    .line 135
    :cond_4
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;

    invoke-direct {p2, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;-><init>(Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    :goto_3
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->initialState(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 144
    instance-of v1, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;

    const-string v2, ""

    if-eqz v1, :cond_0

    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    .line 145
    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;->getName()Ljava/lang/String;

    move-result-object v4

    .line 146
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 148
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$1;

    invoke-direct {p1, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 149
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$2;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$2;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    move-object v3, p3

    .line 144
    invoke-direct/range {v3 .. v8}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 307
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 308
    const-class p2, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 309
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 307
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 153
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 154
    :cond_0
    instance-of v1, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-eqz v1, :cond_1

    .line 155
    move-object v1, p2

    check-cast v1, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->createPayLink(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    sget-object v1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$3;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const-string v2, "get-link-worker"

    invoke-interface {p3, p1, v2, v1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 164
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;

    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;

    invoke-direct {p3, v0, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p3}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 165
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-virtual {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;->toPosLayer(Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 167
    :cond_1
    instance-of v1, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;

    if-eqz v1, :cond_2

    .line 168
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->logViewEventWorker:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;

    check-cast v1, Lcom/squareup/workflow/Worker;

    const-string v2, "log-view-event-worker"

    invoke-static {p3, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 170
    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;->getPayLinkId()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p0, p3}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->getCheckoutLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 171
    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;

    .line 172
    invoke-virtual {p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;->getName()Ljava/lang/String;

    move-result-object v2

    .line 173
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 175
    invoke-virtual {p0, v4}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->createCheckoutLinkQrCode(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 176
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$5;

    move-object p2, p0

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$5;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function2;

    .line 177
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$6;

    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$6;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function2;

    .line 178
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function0;

    move-object v1, p3

    .line 171
    invoke-direct/range {v1 .. v8}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;)V

    .line 182
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-virtual {p3, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;->toPosLayer(Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 184
    :cond_2
    instance-of p3, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$AlreadyExistsErrorState;

    if-eqz p3, :cond_3

    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    .line 185
    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$AlreadyExistsErrorState;

    invoke-virtual {p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$AlreadyExistsErrorState;->getName()Ljava/lang/String;

    move-result-object v4

    .line 186
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    .line 188
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$8;

    invoke-direct {p1, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$8;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 189
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$9;

    invoke-direct {p1, p0, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$9;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v8, p1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    move-object v3, p3

    .line 184
    invoke-direct/range {v3 .. v8}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 312
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 313
    const-class p2, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 314
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 312
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 193
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 194
    :cond_3
    instance-of p1, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$FeatureNotAvailableState;

    if-eqz p1, :cond_4

    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;

    .line 195
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$10;

    invoke-direct {p2, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$10;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 194
    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 196
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-virtual {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;->toPosLayer(Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 197
    :cond_4
    instance-of p1, p2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$ErrorState;

    if-eqz p1, :cond_5

    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;

    .line 198
    new-instance p3, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$11;

    invoke-direct {p3, v0, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$11;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 199
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$12;

    invoke-direct {v1, p0, v0, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$12;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 197
    invoke-direct {p1, p3, v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 203
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-virtual {p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;->toPosLayer(Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->snapshotState(Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
