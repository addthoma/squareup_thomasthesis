.class public abstract Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$Close;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CreateLink;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CheckoutLink;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowError;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowAlreadyExists;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowFeatureNotAvailable;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0008\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "CheckoutLink",
        "Close",
        "CreateLink",
        "NewSale",
        "SetName",
        "ShowAlreadyExists",
        "ShowError",
        "ShowFeatureNotAvailable",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$Close;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CreateLink;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CheckoutLink;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowError;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowAlreadyExists;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowFeatureNotAvailable;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
            ">;)",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;

    move-object v2, p0

    check-cast v2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CreatePayLinkState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    :cond_0
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CreateLink;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    move-object v2, p0

    check-cast v2, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CreateLink;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CreateLink;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 109
    :cond_1
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CheckoutLink;

    if-eqz v0, :cond_4

    .line 110
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-nez v2, :cond_2

    move-object v0, v1

    :cond_2
    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-eqz v0, :cond_3

    .line 111
    new-instance v2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;->getName()Ljava/lang/String;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CheckoutLink;

    invoke-virtual {v3}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$CheckoutLink;->getPayLinkId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$CheckoutLinkState;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    return-object v1

    .line 113
    :cond_4
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowAlreadyExists;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowAlreadyExists;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 114
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-nez v2, :cond_5

    move-object v0, v1

    :cond_5
    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-eqz v0, :cond_6

    .line 115
    new-instance v2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$AlreadyExistsErrorState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$AlreadyExistsErrorState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_6
    return-object v1

    .line 117
    :cond_7
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowFeatureNotAvailable;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowFeatureNotAvailable;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 118
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$FeatureNotAvailableState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$FeatureNotAvailableState;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 120
    :cond_8
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowError;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$ShowError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 121
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-nez v2, :cond_9

    move-object v0, v1

    :cond_9
    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    if-eqz v0, :cond_a

    .line 122
    new-instance v2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$ErrorState;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$ErrorState;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_a
    return-object v1

    .line 124
    :cond_b
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$Close;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$Close;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_c

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    return-object p1

    .line 125
    :cond_c
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_d

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    return-object p1

    :cond_d
    :goto_0
    return-object v1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
