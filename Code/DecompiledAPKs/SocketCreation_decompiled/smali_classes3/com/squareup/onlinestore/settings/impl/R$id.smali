.class public final Lcom/squareup/onlinestore/settings/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bottom_sheet_dialog_container:I = 0x7f0a013e

.field public static final amount:I = 0x7f0a01b7

.field public static final body:I = 0x7f0a023a

.field public static final cancel_button:I = 0x7f0a0299

.field public static final checkout_link_list_screen_container:I = 0x7f0a031f

.field public static final checkout_link_screen_container:I = 0x7f0a0320

.field public static final checkoutlink_deactivate_confirmation_dialog:I = 0x7f0a0325

.field public static final checkoutlink_delete_confirmation_dialog:I = 0x7f0a0326

.field public static final checkoutlink_help_text:I = 0x7f0a0327

.field public static final checkoutlink_url:I = 0x7f0a0328

.field public static final create_link_button:I = 0x7f0a03c5

.field public static final create_link_screen_container:I = 0x7f0a03c6

.field public static final create_links_help_text_container:I = 0x7f0a03c7

.field public static final custom_amount_link_radio_btn:I = 0x7f0a0526

.field public static final deactivate_button:I = 0x7f0a055a

.field public static final delete_button:I = 0x7f0a0561

.field public static final details_label:I = 0x7f0a05ae

.field public static final dismiss_button:I = 0x7f0a05d6

.field public static final donation_link_radio_btn:I = 0x7f0a05e4

.field public static final edit_button:I = 0x7f0a060d

.field public static final enable_checkout_links_container:I = 0x7f0a06fb

.field public static final enable_checkout_links_loading_screen_container:I = 0x7f0a06fc

.field public static final enable_checkout_links_message_view:I = 0x7f0a06fd

.field public static final enable_checkout_links_result_container:I = 0x7f0a06fe

.field public static final enable_checkout_links_result_message_view:I = 0x7f0a06ff

.field public static final enabling_checkout_links_container:I = 0x7f0a0706

.field public static final error_container:I = 0x7f0a071d

.field public static final error_message_view:I = 0x7f0a0720

.field public static final error_msg:I = 0x7f0a0721

.field public static final help_text:I = 0x7f0a07d2

.field public static final link_name:I = 0x7f0a0942

.field public static final link_price:I = 0x7f0a0943

.field public static final link_purpose_label:I = 0x7f0a0944

.field public static final link_type_checkable_group:I = 0x7f0a0945

.field public static final link_updated_status_message:I = 0x7f0a0946

.field public static final links_header:I = 0x7f0a0948

.field public static final loading_screen_container:I = 0x7f0a0955

.field public static final name:I = 0x7f0a0a0c

.field public static final progress_bar:I = 0x7f0a0c7c

.field public static final qr_code:I = 0x7f0a0c9b

.field public static final recycler_view:I = 0x7f0a0d21

.field public static final save_link_screen_container:I = 0x7f0a0e02

.field public static final share_checkoutlink:I = 0x7f0a0e74

.field public static final title:I = 0x7f0a103f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
