.class final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;->invoke(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow;

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3$1;

    invoke-direct {v2, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkWorkflow$render$screenData$3$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v3, 0x1

    invoke-static {v1, p1, v2, v3, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
