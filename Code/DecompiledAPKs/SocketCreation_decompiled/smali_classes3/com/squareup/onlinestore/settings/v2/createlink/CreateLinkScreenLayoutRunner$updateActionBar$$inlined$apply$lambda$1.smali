.class final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateLinkScreenLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$1$1\n*L\n1#1,308:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $onClickUpButton$inlined:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickUpButton$inlined:Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 49
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 233
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getLinkName$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 234
    :goto_0
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_3

    .line 235
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_is_required:I

    invoke-static {v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$showNameError(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;I)V

    return-void

    .line 239
    :cond_3
    sget-object v2, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v2, v0}, Lcom/squareup/onlinestore/common/PayLinkUtil;->nameExceedsCharCountLimit(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 240
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->name_cannot_exceed_255:I

    invoke-static {v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$showNameError(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;I)V

    return-void

    .line 244
    :cond_4
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$isDonationsFeatureEnabled(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$isDonationLinkTypeSelected(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 245
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getLinkName$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 246
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    invoke-direct {v2, v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 250
    :cond_5
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getLinkPrice$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_6

    check-cast v2, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getCurrencyCode$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/squareup/money/MoneyLocaleHelperKt;->extractMoney(Ljava/lang/CharSequence;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :cond_6
    if-eqz v1, :cond_8

    .line 251
    sget-object v2, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-virtual {v2, v1}, Lcom/squareup/onlinestore/common/PayLinkUtil;->isAmountInRange(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_3

    .line 256
    :cond_7
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$getLinkName$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 257
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    invoke-direct {v3, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    invoke-interface {v2, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    .line 252
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->access$showEmptyPriceError(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V

    return-void
.end method
