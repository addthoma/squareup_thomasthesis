.class public final Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;
.super Ljava/lang/Object;
.source "CreateLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$Factory;,
        Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner\n*L\n1#1,308:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00aa\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002ABB;\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0008\u0010 \u001a\u00020!H\u0002J\u0008\u0010\"\u001a\u00020!H\u0002J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020!H\u0002J\u0008\u0010&\u001a\u00020$H\u0002J\u0010\u0010\'\u001a\u00020$2\u0006\u0010(\u001a\u00020)H\u0002J\u0008\u0010*\u001a\u00020$H\u0002J\u0010\u0010+\u001a\u00020$2\u0006\u0010(\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020$2\u0006\u0010(\u001a\u00020.H\u0002J\u0012\u0010/\u001a\u00020$2\u0008\u0008\u0001\u00100\u001a\u000201H\u0002J\u0018\u00102\u001a\u00020$2\u0006\u00103\u001a\u00020\u00022\u0006\u00104\u001a\u000205H\u0016J\u0010\u00106\u001a\u00020$2\u0006\u0010(\u001a\u000207H\u0002J,\u00108\u001a\u00020$2\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020$0:2\u0014\u0010;\u001a\u0010\u0012\u0004\u0012\u00020=\u0012\u0004\u0012\u00020$\u0018\u00010<H\u0002J\u0010\u0010>\u001a\u00020$2\u0006\u0010%\u001a\u00020!H\u0002J\u0012\u0010?\u001a\u00020$2\u0008\u0010@\u001a\u0004\u0018\u00010=H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006C"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;",
        "view",
        "Landroid/view/View;",
        "moneyScrubber",
        "Lcom/squareup/text/SelectableTextScrubber;",
        "moneyDigitsKeyListener",
        "Landroid/text/method/DigitsKeyListener;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "onlineStoreRestrictions",
        "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
        "(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "createLinkScreenContainer",
        "detailsLabel",
        "errorMsg",
        "Landroid/widget/TextView;",
        "helpText",
        "Lcom/squareup/noho/NohoLabel;",
        "linkName",
        "Lcom/squareup/noho/NohoEditRow;",
        "linkPrice",
        "linkPurposeLabel",
        "linkTypeCheckableGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "saveLinkScreenContainer",
        "isDonationLinkTypeSelected",
        "",
        "isDonationsFeatureEnabled",
        "selectLinkType",
        "",
        "isDonation",
        "setLinkTypeSelectionVisibility",
        "showCreateLinkScreenData",
        "screenData",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;",
        "showEmptyPriceError",
        "showLinkAlreadyExistsErrorScreenData",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;",
        "showLinkCreationFailedScreenData",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;",
        "showNameError",
        "msg",
        "",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showSaveLinkScreenData",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;",
        "updateActionBar",
        "onClickUpButton",
        "Lkotlin/Function0;",
        "onClickSaveButton",
        "Lkotlin/Function1;",
        "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
        "updateHelpText",
        "updateLinkSelectionAndInputFields",
        "createLinkInfo",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final createLinkScreenContainer:Landroid/view/View;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final detailsLabel:Landroid/view/View;

.field private final errorMsg:Landroid/widget/TextView;

.field private final helpText:Lcom/squareup/noho/NohoLabel;

.field private final linkName:Lcom/squareup/noho/NohoEditRow;

.field private final linkPrice:Lcom/squareup/noho/NohoEditRow;

.field private final linkPurposeLabel:Landroid/view/View;

.field private final linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

.field private final saveLinkScreenContainer:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/text/SelectableTextScrubber;Landroid/text/method/DigitsKeyListener;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/text/SelectableTextScrubber;",
            "Landroid/text/method/DigitsKeyListener;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyScrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyDigitsKeyListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineStoreRestrictions"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p5, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    .line 58
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 60
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->create_link_screen_container:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    .line 61
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->save_link_screen_container:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    .line 63
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_purpose_label:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPurposeLabel:Landroid/view/View;

    .line 64
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->details_label:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->detailsLabel:Landroid/view/View;

    .line 66
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_type_checkable_group:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 67
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_name:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    .line 68
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->link_price:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    .line 69
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->error_msg:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    .line 70
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget p4, Lcom/squareup/onlinestore/settings/impl/R$id;->help_text:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    .line 73
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    iget-object p4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p5, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {p5, v0, v1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p5

    invoke-interface {p4, p5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    new-instance p4, Lcom/squareup/text/ScrubbingTextWatcher;

    move-object p5, p1

    check-cast p5, Lcom/squareup/text/HasSelectableText;

    invoke-direct {p4, p2, p5}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoEditRow;->setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p3, Landroid/text/method/KeyListener;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoEditRow;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance p2, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;

    invoke-direct {p2, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V

    check-cast p2, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getLinkName$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    return-object p0
.end method

.method public static final synthetic access$getLinkPrice$p(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    return-object p0
.end method

.method public static final synthetic access$isDonationLinkTypeSelected(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Z
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationLinkTypeSelected()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$isDonationsFeatureEnabled(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)Z
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationsFeatureEnabled()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$showEmptyPriceError(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showEmptyPriceError()V

    return-void
.end method

.method public static final synthetic access$showNameError(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;I)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showNameError(I)V

    return-void
.end method

.method public static final synthetic access$updateHelpText(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;Z)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateHelpText(Z)V

    return-void
.end method

.method private final isDonationLinkTypeSelected()Z
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->getCheckedId()I

    move-result v0

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->donation_link_radio_btn:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final isDonationsFeatureEnabled()Z
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->onlineStoreRestrictions:Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-interface {v0}, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;->isOnlineCheckoutSettingsDonationsEnabled()Z

    move-result v0

    return v0
.end method

.method private final selectLinkType(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 167
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->donation_link_radio_btn:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    goto :goto_0

    .line 169
    :cond_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->custom_amount_link_radio_btn:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    :goto_0
    return-void
.end method

.method private final setLinkTypeSelectionVisibility()V
    .locals 2

    .line 158
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationsFeatureEnabled()Z

    move-result v0

    .line 159
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPurposeLabel:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 160
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 161
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->detailsLabel:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final showCreateLinkScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;)V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 112
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    .line 114
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->setLinkTypeSelectionVisibility()V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateLinkSelectionAndInputFields(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showCreateLinkScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showCreateLinkScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 119
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationsFeatureEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationLinkTypeSelected()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateHelpText(Z)V

    .line 120
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final showEmptyPriceError()V
    .locals 5

    .line 274
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 275
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 279
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    .line 276
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_price_error_msg:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 277
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    sget-object v3, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, v4}, Lcom/squareup/onlinestore/common/PayLinkUtil;->minPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "min"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 278
    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    sget-object v3, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v4, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v3, v4}, Lcom/squareup/onlinestore/common/PayLinkUtil;->maxPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "max"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final showLinkAlreadyExistsErrorScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 178
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    .line 180
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->setLinkTypeSelectionVisibility()V

    .line 181
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateLinkSelectionAndInputFields(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    .line 182
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 183
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 185
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showLinkAlreadyExistsErrorScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showLinkAlreadyExistsErrorScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 186
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_already_exists_error_msg:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 188
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 189
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showLinkCreationFailedScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;)V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 194
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getOnClickSaveButton()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    .line 196
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->setLinkTypeSelectionVisibility()V

    .line 197
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;->getCreateLinkInfo()Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateLinkSelectionAndInputFields(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 199
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 201
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showLinkCreationFailedScreenData$1;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$showLinkCreationFailedScreenData$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 202
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_error_msg:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 204
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 205
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final showNameError(I)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 267
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 268
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 269
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->errorMsg:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 270
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final showSaveLinkScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;)V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 210
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateActionBar(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 220
    new-instance v8, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 222
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_create_link:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v8, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    if-eqz p2, :cond_0

    .line 224
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v8, v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 226
    :cond_0
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v8, v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    :goto_0
    if-eqz p2, :cond_1

    .line 230
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 231
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_action_save:I

    invoke-direct {v1, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/resources/TextModel;

    const/4 v4, 0x0

    .line 232
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    move-object v5, v1

    check-cast v5, Lkotlin/jvm/functions/Function0;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v8

    .line 229
    invoke-static/range {v1 .. v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 262
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateHelpText(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 126
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_donation_help_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->helpText:Lcom/squareup/noho/NohoLabel;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->create_link_help_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    :goto_0
    return-void
.end method

.method private final updateLinkSelectionAndInputFields(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo;)V
    .locals 3

    .line 134
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;->moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateCustomAmountLinkInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 139
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationsFeatureEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 140
    invoke-direct {p0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->selectLinkType(Z)V

    goto :goto_0

    .line 143
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkInfo$CreateDonationLinkInfo;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkName:Lcom/squareup/noho/NohoEditRow;

    invoke-static {p1}, Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;->moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->linkPrice:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    const/4 p1, 0x1

    .line 147
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->selectLinkType(Z)V

    goto :goto_0

    .line 150
    :cond_1
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->isDonationsFeatureEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 151
    invoke-direct {p0, v1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->selectLinkType(Z)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 93
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;

    if-eqz p2, :cond_0

    .line 94
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showCreateLinkScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$CreateLinkScreenData;)V

    goto :goto_0

    .line 96
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    if-eqz p2, :cond_1

    .line 97
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showLinkAlreadyExistsErrorScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkAlreadyExistsErrorScreenData;)V

    goto :goto_0

    .line 99
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    if-eqz p2, :cond_2

    .line 100
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showLinkCreationFailedScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$LinkCreationFailedErrorScreenData;)V

    goto :goto_0

    .line 102
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;

    if-eqz p2, :cond_3

    .line 103
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showSaveLinkScreenData(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData$SaveLinkScreenData;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->createLinkScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->saveLinkScreenContainer:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/v2/createlink/CreateLinkFlowScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
