.class public final Lcom/squareup/onlinestore/settings/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept_donations:I = 0x7f120029

.field public static final activate:I = 0x7f12003f

.field public static final activate_confirmation_dialog_body:I = 0x7f120041

.field public static final activate_confirmation_dialog_title:I = 0x7f120042

.field public static final activate_link:I = 0x7f120043

.field public static final checkout_link_screen_deactivated_msg:I = 0x7f12040f

.field public static final checkoutlink_clipboard_label:I = 0x7f120418

.field public static final checkoutlink_copied_toast_message:I = 0x7f120419

.field public static final collect_payments:I = 0x7f120434

.field public static final connection_error_msg:I = 0x7f120490

.field public static final connection_error_title:I = 0x7f120491

.field public static final connection_error_try_again:I = 0x7f120492

.field public static final create_link_already_exists_error_msg:I = 0x7f1205db

.field public static final create_link_donation_help_text:I = 0x7f1205dc

.field public static final create_link_error_msg:I = 0x7f1205dd

.field public static final create_link_help_text:I = 0x7f1205de

.field public static final create_link_name_hint:I = 0x7f1205df

.field public static final create_link_name_label:I = 0x7f1205e0

.field public static final create_link_price_error_msg:I = 0x7f1205e1

.field public static final create_link_price_label:I = 0x7f1205e2

.field public static final create_links_help_text_body:I = 0x7f1205e3

.field public static final create_links_help_text_title:I = 0x7f1205e4

.field public static final deactivate:I = 0x7f1207dc

.field public static final deactivate_confirmation_dialog_body:I = 0x7f1207dd

.field public static final deactivate_confirmation_dialog_title:I = 0x7f1207de

.field public static final deactivate_link:I = 0x7f1207df

.field public static final delete_confirmation_dialog_body:I = 0x7f1207e7

.field public static final delete_confirmation_dialog_title:I = 0x7f1207e8

.field public static final delete_link:I = 0x7f1207ea

.field public static final details:I = 0x7f120831

.field public static final donation_link_price_label:I = 0x7f1208d2

.field public static final inactive:I = 0x7f120bf3

.field public static final link_activated_msg:I = 0x7f120ecc

.field public static final link_deactivated_msg:I = 0x7f120ed6

.field public static final link_deleted_msg:I = 0x7f120ed8

.field public static final link_purpose:I = 0x7f120ed9

.field public static final name_cannot_exceed_255:I = 0x7f12104c

.field public static final name_is_required:I = 0x7f12104d

.field public static final online_checkout_create_link:I = 0x7f121133

.field public static final online_checkout_edit_link:I = 0x7f121134

.field public static final online_checkout_links:I = 0x7f12113c

.field public static final online_checkout_settings_action_save:I = 0x7f121144

.field public static final online_checkout_settings_checkout_link_actions:I = 0x7f121145

.field public static final online_checkout_settings_checkout_link_help_text:I = 0x7f121146

.field public static final online_checkout_settings_checkout_link_help_text_no_qr_code:I = 0x7f121147

.field public static final online_checkout_settings_checkout_link_label:I = 0x7f121148

.field public static final online_checkout_settings_checkout_link_qr_code_content_desc:I = 0x7f121149

.field public static final online_checkout_settings_checkout_link_share_link:I = 0x7f12114a

.field public static final online_checkout_settings_could_not_enable:I = 0x7f12114b

.field public static final online_checkout_settings_could_not_enable_help_text:I = 0x7f12114c

.field public static final online_checkout_settings_donation_checkout_link_help_text:I = 0x7f12114d

.field public static final online_checkout_settings_donation_checkout_link_help_text_no_qr_code:I = 0x7f12114e

.field public static final online_checkout_settings_enable_checkout_links:I = 0x7f12114f

.field public static final online_checkout_settings_enabled:I = 0x7f121150

.field public static final online_checkout_settings_enabled_help_text:I = 0x7f121151

.field public static final online_checkout_settings_enabling_checkout_links_help_text:I = 0x7f121152

.field public static final online_checkout_settings_enabling_checkout_links_title:I = 0x7f121153

.field public static final online_checkout_settings_feature_not_available_msg:I = 0x7f121154

.field public static final online_checkout_settings_network_error_title:I = 0x7f121155

.field public static final online_checkout_settings_retry:I = 0x7f121156

.field public static final online_checkout_settings_start_selling_online:I = 0x7f121157

.field public static final online_checkout_settings_start_selling_online_help_text:I = 0x7f121158

.field public static final online_checkout_settings_title:I = 0x7f121159

.field public static final paylink_url:I = 0x7f1213ad

.field public static final paylink_url_staging:I = 0x7f1213ae

.field public static final share_sheet_subject:I = 0x7f1217de

.field public static final share_sheet_title:I = 0x7f1217df


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
