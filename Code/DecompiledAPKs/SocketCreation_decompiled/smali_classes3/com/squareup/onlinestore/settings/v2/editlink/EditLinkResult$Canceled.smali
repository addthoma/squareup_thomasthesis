.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;
.super Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;
.source "EditLinkResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Canceled"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;

    invoke-direct {v0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Canceled;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
