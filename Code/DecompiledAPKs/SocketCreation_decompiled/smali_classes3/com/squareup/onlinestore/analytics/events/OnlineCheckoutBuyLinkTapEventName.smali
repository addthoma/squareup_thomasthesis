.class public final enum Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;
.super Ljava/lang/Enum;
.source "OnlineCheckoutBuyLinkEvents.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "BUY_LINK_NEW_SALE",
        "BUY_LINK_TAP_TO_COPY_LINK",
        "BUY_LINK_TRY_AGAIN",
        "SHARE_BUY",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

.field public static final enum BUY_LINK_NEW_SALE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

.field public static final enum BUY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

.field public static final enum BUY_LINK_TRY_AGAIN:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

.field public static final enum SHARE_BUY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    const/4 v2, 0x0

    const-string v3, "BUY_LINK_NEW_SALE"

    const-string v4, "SPOS Checkout: Online Checkout: Buy Link: New Sale"

    .line 8
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->BUY_LINK_NEW_SALE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    const/4 v2, 0x1

    const-string v3, "BUY_LINK_TAP_TO_COPY_LINK"

    const-string v4, "SPOS Checkout: Online Checkout: Tap to copy link"

    .line 9
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->BUY_LINK_TAP_TO_COPY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    const/4 v2, 0x2

    const-string v3, "BUY_LINK_TRY_AGAIN"

    const-string v4, "SPOS Checkout: Online Checkout: Buy Link: Try Again"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->BUY_LINK_TRY_AGAIN:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    const/4 v2, 0x3

    const-string v3, "SHARE_BUY"

    const-string v4, "SPOS Checkout: Online Checkout: Share Buy"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->SHARE_BUY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;
    .locals 1

    const-class v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;
    .locals 1

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    invoke-virtual {v0}, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkTapEventName;->value:Ljava/lang/String;

    return-object v0
.end method
