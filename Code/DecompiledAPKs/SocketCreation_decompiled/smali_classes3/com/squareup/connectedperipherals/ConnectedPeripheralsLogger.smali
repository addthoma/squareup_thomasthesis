.class public Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsLogger.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectedPeripheralsLogger.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectedPeripheralsLogger.kt\ncom/squareup/connectedperipherals/ConnectedPeripheralsLogger\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,87:1\n1360#2:88\n1429#2,3:89\n1265#2,12:92\n*E\n*S KotlinDebug\n*F\n+ 1 ConnectedPeripheralsLogger.kt\ncom/squareup/connectedperipherals/ConnectedPeripheralsLogger\n*L\n59#1:88\n59#1,3:89\n84#1,12:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0017\u0018\u00002\u00020\u0001BL\u0008\u0007\u0012\u0011\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000fJ\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0002J\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0015H\u0002J\u0008\u0010\u0019\u001a\u00020\u001aH\u0016J\u000e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015H\u0002R\u000e\u0010\u000e\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;",
        "",
        "providers",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "versionName",
        "",
        "versionCode",
        "",
        "installationId",
        "(Ljava/util/Set;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;ILjava/lang/String;)V",
        "connectedPeripheralTypeToProto",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;",
        "connectionType",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralType;",
        "getAllConnectedPeripherals",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
        "getConnectedPeripheralsProto",
        "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
        "getPeripheralMetaData",
        "Lcom/squareup/protos/common/payment/PeripheralMetadata;",
        "mapProvidersToList",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final installationId:Ljava/lang/String;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final providers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final versionCode:I

.field private final versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/util/ProductVersionName;
        .end annotation
    .end param
    .param p5    # I
        .annotation runtime Lcom/squareup/util/ProductVersionCode;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/settings/InstallationId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "providers"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "versionName"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installationId"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->providers:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p4, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->versionName:Ljava/lang/String;

    iput p5, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->versionCode:I

    iput-object p6, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->installationId:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$mapProvidersToList(Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;)Ljava/util/List;
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->mapProvidersToList()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final connectedPeripheralTypeToProto(Lcom/squareup/connectedperipherals/ConnectedPeripheralType;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 72
    :cond_0
    sget-object v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 79
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 78
    :pswitch_0
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->UNKNOWN:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    goto :goto_0

    .line 77
    :pswitch_1
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->AUDIO:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    goto :goto_0

    .line 76
    :pswitch_2
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLE:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    goto :goto_0

    .line 75
    :pswitch_3
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->BLUETOOTH:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    goto :goto_0

    .line 74
    :pswitch_4
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->USB:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    goto :goto_0

    .line 73
    :pswitch_5
    sget-object p1, Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;->NETWORK:Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final getConnectedPeripheralsProto()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/payment/ConnectedPeripheral;",
            ">;"
        }
    .end annotation

    .line 59
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->getAllConnectedPeripherals()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 88
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 89
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 90
    check-cast v2, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;

    .line 60
    new-instance v3, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;-><init>()V

    .line 61
    invoke-virtual {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;->getConnectionType()Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->connectedPeripheralTypeToProto(Lcom/squareup/connectedperipherals/ConnectedPeripheralType;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->connection_type(Lcom/squareup/protos/common/payment/ConnectedPeripheral$ConnectionType;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object v3

    .line 62
    invoke-virtual {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;->getDeviceType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->device_type(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object v3

    .line 63
    invoke-virtual {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;->getManufacturer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->manufacturer_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object v3

    .line 64
    invoke-virtual {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;->getModel()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->model_name(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object v3

    .line 65
    invoke-virtual {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;->getUniqueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->unique_identifier(Ljava/lang/String;)Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;

    move-result-object v2

    .line 66
    invoke-virtual {v2}, Lcom/squareup/protos/common/payment/ConnectedPeripheral$Builder;->build()Lcom/squareup/protos/common/payment/ConnectedPeripheral;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method private final mapProvidersToList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->providers:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 99
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 100
    check-cast v2, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;

    .line 84
    invoke-interface {v2}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;->getConnectedPeripherals()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 101
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 103
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method


# virtual methods
.method public final getAllConnectedPeripherals()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->isTargetThread()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->mapProvidersToList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 42
    :cond_0
    new-instance v0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;

    invoke-direct {v0, p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger$getAllConnectedPeripherals$1;-><init>(Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Single.fromCallable { ma\u2026\n          .blockingGet()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public getPeripheralMetaData()Lcom/squareup/protos/common/payment/PeripheralMetadata;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->versionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_release(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object v0

    .line 51
    iget v1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->versionCode:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->app_version(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->installationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->source_device_installation_id(Ljava/lang/String;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object v0

    .line 53
    invoke-direct {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;->getConnectedPeripheralsProto()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->connected_peripherals(Ljava/util/List;)Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/protos/common/payment/PeripheralMetadata$Builder;->build()Lcom/squareup/protos/common/payment/PeripheralMetadata;

    move-result-object v0

    const-string v1, "PeripheralMetadata.Build\u2026Proto())\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
