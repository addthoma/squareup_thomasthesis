.class public final Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;
.super Ljava/lang/Object;
.source "ConnectedPeripheralsLogger_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;",
        ">;"
    }
.end annotation


# instance fields
.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final providersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;>;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final versionCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final versionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->providersProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->versionNameProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->versionCodeProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->installationIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljava/util/Set;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;ILjava/lang/String;)Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
            ">;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;"
        }
    .end annotation

    .line 60
    new-instance v7, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;-><init>(Ljava/util/Set;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;ILjava/lang/String;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->providersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Set;

    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->versionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->versionCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->newInstance(Ljava/util/Set;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/lang/String;ILjava/lang/String;)Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger_Factory;->get()Lcom/squareup/connectedperipherals/ConnectedPeripheralsLogger;

    move-result-object v0

    return-object v0
.end method
