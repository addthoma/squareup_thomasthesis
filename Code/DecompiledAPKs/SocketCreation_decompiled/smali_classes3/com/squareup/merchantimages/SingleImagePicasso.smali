.class public Lcom/squareup/merchantimages/SingleImagePicasso;
.super Ljava/lang/Object;
.source "SingleImagePicasso.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;,
        Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;
    }
.end annotation


# static fields
.field static final CACHE_SIZE_BYTES:J = 0x500000L


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final uriOverrider:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;


# direct methods
.method constructor <init>(Lcom/squareup/picasso/Picasso$Builder;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/Picasso$RequestTransformer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/picasso/Picasso$Builder;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/picasso/Picasso$RequestTransformer;",
            ")V"
        }
    .end annotation

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    new-instance v0, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;

    invoke-direct {v0, p2, p4}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;-><init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/picasso/Picasso$RequestTransformer;)V

    iput-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->uriOverrider:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;

    .line 121
    iget-object p2, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->uriOverrider:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;

    .line 122
    invoke-virtual {p1, p2}, Lcom/squareup/picasso/Picasso$Builder;->requestTransformer(Lcom/squareup/picasso/Picasso$RequestTransformer;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/picasso/Picasso$Builder;->build()Lcom/squareup/picasso/Picasso;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->picasso:Lcom/squareup/picasso/Picasso;

    .line 124
    iput-object p3, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method

.method private isConnectedToInternet()Z
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    return v0
.end method


# virtual methods
.method cancelRequest(Landroid/widget/ImageView;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Landroid/widget/ImageView;)V

    return-void
.end method

.method public load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 143
    invoke-direct {p0}, Lcom/squareup/merchantimages/SingleImagePicasso;->isConnectedToInternet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    sget-object v0, Lcom/squareup/picasso/NetworkPolicy;->OFFLINE:Lcom/squareup/picasso/NetworkPolicy;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/squareup/picasso/NetworkPolicy;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/picasso/RequestCreator;->networkPolicy(Lcom/squareup/picasso/NetworkPolicy;[Lcom/squareup/picasso/NetworkPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public setOverride(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "uri"

    .line 162
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 163
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->uriOverrider:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;

    invoke-virtual {v0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverrider;->setOverride(Ljava/lang/String;)V

    return-void
.end method

.method shutdown()V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicasso;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {v0}, Lcom/squareup/picasso/Picasso;->shutdown()V

    return-void
.end method
