.class public final Lcom/squareup/merchantimages/RealCuratedImage_Factory;
.super Ljava/lang/Object;
.source "RealCuratedImage_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/merchantimages/RealCuratedImage;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final thumborProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->picassoFactoryProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->thumborProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->contextProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/merchantimages/RealCuratedImage_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pollexor/Thumbor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/merchantimages/RealCuratedImage_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/merchantimages/RealCuratedImage_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/app/Application;)Lcom/squareup/merchantimages/RealCuratedImage;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/merchantimages/RealCuratedImage;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/merchantimages/RealCuratedImage;-><init>(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/merchantimages/RealCuratedImage;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->picassoFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    iget-object v1, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->thumborProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/pollexor/Thumbor;

    iget-object v3, p0, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Application;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->newInstance(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/app/Application;)Lcom/squareup/merchantimages/RealCuratedImage;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/merchantimages/RealCuratedImage_Factory;->get()Lcom/squareup/merchantimages/RealCuratedImage;

    move-result-object v0

    return-object v0
.end method
