.class public final Lcom/squareup/ms/NoopMinesweeperModule;
.super Ljava/lang/Object;
.source "NoopMinesweeperModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0008\u0010\u0005\u001a\u00020\u0006H\u0007J\u0008\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ms/NoopMinesweeperModule;",
        "",
        "()V",
        "provideMinesweeper",
        "Lcom/squareup/ms/Minesweeper;",
        "provideMinesweeperTicket",
        "Lcom/squareup/ms/MinesweeperTicket;",
        "provideMsFactory",
        "Lcom/squareup/ms/MsFactory;",
        "impl-noop-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ms/NoopMinesweeperModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/ms/NoopMinesweeperModule;

    invoke-direct {v0}, Lcom/squareup/ms/NoopMinesweeperModule;-><init>()V

    sput-object v0, Lcom/squareup/ms/NoopMinesweeperModule;->INSTANCE:Lcom/squareup/ms/NoopMinesweeperModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 20
    sget-object v0, Lcom/squareup/ms/NoopMinesweeper;->INSTANCE:Lcom/squareup/ms/NoopMinesweeper;

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    return-object v0
.end method

.method public static final provideMinesweeperTicket()Lcom/squareup/ms/MinesweeperTicket;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 10
    sget-object v0, Lcom/squareup/ms/NoopMinesweeperTicket;->INSTANCE:Lcom/squareup/ms/NoopMinesweeperTicket;

    check-cast v0, Lcom/squareup/ms/MinesweeperTicket;

    return-object v0
.end method

.method public static final provideMsFactory()Lcom/squareup/ms/MsFactory;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 15
    sget-object v0, Lcom/squareup/ms/NoopMsFactory;->INSTANCE:Lcom/squareup/ms/NoopMsFactory;

    check-cast v0, Lcom/squareup/ms/MsFactory;

    return-object v0
.end method
