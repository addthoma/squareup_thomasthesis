.class public Lcom/squareup/ms/RealMsFactory;
.super Ljava/lang/Object;
.source "RealMsFactory.java"

# interfaces
.implements Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;
.implements Lcom/squareup/ms/MsFactory;


# instance fields
.field private final application:Landroid/app/Application;

.field private final dataListener:Lcom/squareup/ms/Minesweeper$DataListener;

.field private final executorService:Ljava/util/concurrent/ExecutorService;

.field private isInitialized:Z

.field private final libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private minesweeper:Lcom/squareup/ms/Minesweeper;

.field private final minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

.field private final ms:Lcom/squareup/ms/Ms;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/ms/Minesweeper$DataListener;Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms;Lcom/squareup/ms/Minesweeper;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 21
    iput-boolean v0, p0, Lcom/squareup/ms/RealMsFactory;->isInitialized:Z

    .line 26
    iput-object p1, p0, Lcom/squareup/ms/RealMsFactory;->application:Landroid/app/Application;

    .line 27
    iput-object p2, p0, Lcom/squareup/ms/RealMsFactory;->dataListener:Lcom/squareup/ms/Minesweeper$DataListener;

    .line 28
    iput-object p3, p0, Lcom/squareup/ms/RealMsFactory;->executorService:Ljava/util/concurrent/ExecutorService;

    .line 29
    iput-object p5, p0, Lcom/squareup/ms/RealMsFactory;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 30
    iput-object p4, p0, Lcom/squareup/ms/RealMsFactory;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    .line 31
    iput-object p7, p0, Lcom/squareup/ms/RealMsFactory;->ms:Lcom/squareup/ms/Ms;

    .line 32
    iput-object p6, p0, Lcom/squareup/ms/RealMsFactory;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    .line 33
    iput-object p8, p0, Lcom/squareup/ms/RealMsFactory;->minesweeper:Lcom/squareup/ms/Minesweeper;

    return-void
.end method

.method private doInitialize()V
    .locals 4

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->ms:Lcom/squareup/ms/Ms;

    iget-object v1, p0, Lcom/squareup/ms/RealMsFactory;->application:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/ms/RealMsFactory;->dataListener:Lcom/squareup/ms/Minesweeper$DataListener;

    new-instance v3, Lcom/squareup/ms/-$$Lambda$RealMsFactory$hZSN4ibJ_SKHZAl9Rbs-dxnfXtA;

    invoke-direct {v3, p0}, Lcom/squareup/ms/-$$Lambda$RealMsFactory$hZSN4ibJ_SKHZAl9Rbs-dxnfXtA;-><init>(Lcom/squareup/ms/RealMsFactory;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ms/Ms;->initialize(Landroid/app/Application;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/ms/Ms$InitializedCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 73
    sget-object v1, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;->TOTAL_FAILURE:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    invoke-direct {p0, v1, v0}, Lcom/squareup/ms/RealMsFactory;->postError(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static synthetic lambda$8SwBntBaDfqtPyaT7yi0YKMCwVM(Lcom/squareup/ms/RealMsFactory;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ms/RealMsFactory;->doInitialize()V

    return-void
.end method

.method private postError(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/ms/-$$Lambda$RealMsFactory$yBydcZxekKGbyYg8SSr0pmcz0Vk;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ms/-$$Lambda$RealMsFactory$yBydcZxekKGbyYg8SSr0pmcz0Vk;-><init>(Lcom/squareup/ms/RealMsFactory;Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public getMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->minesweeper:Lcom/squareup/ms/Minesweeper;

    return-object v0
.end method

.method public initialize()V
    .locals 2

    const/4 v0, 0x1

    .line 47
    iput-boolean v0, p0, Lcom/squareup/ms/RealMsFactory;->isInitialized:Z

    .line 49
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    const-string v1, "Native lib loaded, starting up Minesweeper."

    invoke-interface {v0, v1}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->logMinesweeperEvent(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ms/RealMsFactory;->onLibrariesLoaded()V

    goto :goto_0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    const-string v1, "Native lib not yet loaded, adding MsFactory as a LibraryLoaderListener."

    invoke-interface {v0, v1}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->logMinesweeperEvent(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    :goto_0
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .line 37
    iget-boolean v0, p0, Lcom/squareup/ms/RealMsFactory;->isInitialized:Z

    return v0
.end method

.method public synthetic lambda$doInitialize$0$RealMsFactory([B)V
    .locals 0

    .line 71
    iget-object p1, p0, Lcom/squareup/ms/RealMsFactory;->ms:Lcom/squareup/ms/Ms;

    iput-object p1, p0, Lcom/squareup/ms/RealMsFactory;->minesweeper:Lcom/squareup/ms/Minesweeper;

    return-void
.end method

.method public synthetic lambda$postError$1$RealMsFactory(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    invoke-interface {v0, p1, p2}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->onMinesweeperFailureToInitialize(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onLibrariesFailedToLoad(Ljava/lang/String;)V
    .locals 0

    .line 66
    iget-object p1, p0, Lcom/squareup/ms/RealMsFactory;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void
.end method

.method public onLibrariesLoaded()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ms/RealMsFactory;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/ms/-$$Lambda$RealMsFactory$8SwBntBaDfqtPyaT7yi0YKMCwVM;

    invoke-direct {v1, p0}, Lcom/squareup/ms/-$$Lambda$RealMsFactory$8SwBntBaDfqtPyaT7yi0YKMCwVM;-><init>(Lcom/squareup/ms/RealMsFactory;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
