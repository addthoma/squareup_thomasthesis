.class public Lcom/squareup/ms/AppFunctionStatusListenerBridge;
.super Ljava/lang/Object;
.source "AppFunctionStatusListenerBridge.java"

# interfaces
.implements Lcom/squareup/ms/NativeAppFunctionStatusListener;


# instance fields
.field private final appFunctionStatusListener:Lcom/squareup/ms/AppFunctionStatusListener;


# direct methods
.method public constructor <init>(Lcom/squareup/ms/AppFunctionStatusListener;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ms/AppFunctionStatusListenerBridge;->appFunctionStatusListener:Lcom/squareup/ms/AppFunctionStatusListener;

    return-void
.end method

.method private decodeOrWarn([B)Lcom/squareup/protos/client/flipper/AugmentedStatus;
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 43
    :try_start_0
    sget-object v1, Lcom/squareup/protos/client/flipper/AugmentedStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/flipper/AugmentedStatus;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 45
    iget-object v1, p0, Lcom/squareup/ms/AppFunctionStatusListenerBridge;->appFunctionStatusListener:Lcom/squareup/ms/AppFunctionStatusListener;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lcom/squareup/ms/AppFunctionStatusListener;->onStatusUpdateInvalidData(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0

    :cond_0
    move-object p1, v0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public onStatusUpdate(I[B)V
    .locals 2

    .line 24
    invoke-static {p1}, Lcom/squareup/ms/AppFunction;->fromInteger(I)Lcom/squareup/ms/AppFunction;

    move-result-object v0

    .line 25
    sget-object v1, Lcom/squareup/ms/AppFunction;->VALUE_NOT_SUPPORTED:Lcom/squareup/ms/AppFunction;

    invoke-virtual {v1, v0}, Lcom/squareup/ms/AppFunction;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    iget-object p2, p0, Lcom/squareup/ms/AppFunctionStatusListenerBridge;->appFunctionStatusListener:Lcom/squareup/ms/AppFunctionStatusListener;

    invoke-interface {p2, p1}, Lcom/squareup/ms/AppFunctionStatusListener;->onStatusUpdateInvalidAppFunction(I)V

    return-void

    .line 32
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/ms/AppFunctionStatusListenerBridge;->decodeOrWarn([B)Lcom/squareup/protos/client/flipper/AugmentedStatus;

    move-result-object p1

    .line 37
    iget-object p2, p0, Lcom/squareup/ms/AppFunctionStatusListenerBridge;->appFunctionStatusListener:Lcom/squareup/ms/AppFunctionStatusListener;

    invoke-interface {p2, v0, p1}, Lcom/squareup/ms/AppFunctionStatusListener;->onStatusUpdate(Lcom/squareup/ms/AppFunction;Lcom/squareup/protos/client/flipper/AugmentedStatus;)V

    return-void
.end method
