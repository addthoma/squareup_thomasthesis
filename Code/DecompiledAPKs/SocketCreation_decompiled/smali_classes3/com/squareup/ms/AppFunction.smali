.class public final enum Lcom/squareup/ms/AppFunction;
.super Ljava/lang/Enum;
.source "AppFunction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ms/AppFunction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ms/AppFunction;

.field public static final enum CARD_NOT_PRESENT:Lcom/squareup/ms/AppFunction;

.field public static final enum SECURE_SESSION:Lcom/squareup/ms/AppFunction;

.field public static final enum VALUE_NOT_SUPPORTED:Lcom/squareup/ms/AppFunction;


# instance fields
.field private final val:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 10
    new-instance v0, Lcom/squareup/ms/AppFunction;

    const/4 v1, 0x0

    const-string v2, "VALUE_NOT_SUPPORTED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/ms/AppFunction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ms/AppFunction;->VALUE_NOT_SUPPORTED:Lcom/squareup/ms/AppFunction;

    .line 11
    new-instance v0, Lcom/squareup/ms/AppFunction;

    const/4 v2, 0x1

    const-string v3, "SECURE_SESSION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/ms/AppFunction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ms/AppFunction;->SECURE_SESSION:Lcom/squareup/ms/AppFunction;

    .line 12
    new-instance v0, Lcom/squareup/ms/AppFunction;

    const/4 v3, 0x2

    const-string v4, "CARD_NOT_PRESENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/ms/AppFunction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ms/AppFunction;->CARD_NOT_PRESENT:Lcom/squareup/ms/AppFunction;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/ms/AppFunction;

    .line 9
    sget-object v4, Lcom/squareup/ms/AppFunction;->VALUE_NOT_SUPPORTED:Lcom/squareup/ms/AppFunction;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/ms/AppFunction;->SECURE_SESSION:Lcom/squareup/ms/AppFunction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ms/AppFunction;->CARD_NOT_PRESENT:Lcom/squareup/ms/AppFunction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ms/AppFunction;->$VALUES:[Lcom/squareup/ms/AppFunction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lcom/squareup/ms/AppFunction;->val:I

    return-void
.end method

.method public static fromInteger(I)Lcom/squareup/ms/AppFunction;
    .locals 3

    .line 29
    invoke-static {}, Lcom/squareup/ms/AppFunction;->values()[Lcom/squareup/ms/AppFunction;

    move-result-object v0

    const/4 v1, 0x0

    .line 30
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 31
    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/squareup/ms/AppFunction;->getVal()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 32
    aget-object p0, v0, v1

    return-object p0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_1
    sget-object p0, Lcom/squareup/ms/AppFunction;->VALUE_NOT_SUPPORTED:Lcom/squareup/ms/AppFunction;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ms/AppFunction;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/ms/AppFunction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ms/AppFunction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ms/AppFunction;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/ms/AppFunction;->$VALUES:[Lcom/squareup/ms/AppFunction;

    invoke-virtual {v0}, [Lcom/squareup/ms/AppFunction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ms/AppFunction;

    return-object v0
.end method


# virtual methods
.method public getVal()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/ms/AppFunction;->val:I

    return v0
.end method
