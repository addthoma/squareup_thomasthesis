.class public final enum Lcom/squareup/notification/Channels;
.super Ljava/lang/Enum;
.source "Channels.java"

# interfaces
.implements Lcom/squareup/notification/Channel;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/notification/Channels;",
        ">;",
        "Lcom/squareup/notification/Channel;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/notification/Channels;

.field public static final enum ACCOUNT_ISSUE:Lcom/squareup/notification/Channels;

.field public static final enum APP_STORAGE_ERRORS:Lcom/squareup/notification/Channels;

.field public static final enum BACKGROUND_PROCESSING:Lcom/squareup/notification/Channels;

.field public static final enum CUSTOMER_MESSAGES:Lcom/squareup/notification/Channels;

.field public static final enum MERCHANT_PROFILE:Lcom/squareup/notification/Channels;

.field public static final enum PAYMENTS:Lcom/squareup/notification/Channels;

.field public static final enum SUPPORT_MESSAGE:Lcom/squareup/notification/Channels;

.field public static final enum UPDATES:Lcom/squareup/notification/Channels;


# instance fields
.field private final channelId:Ljava/lang/String;

.field private final defaultChannel:Z

.field private final importance:I

.field private final nameResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 13
    new-instance v7, Lcom/squareup/notification/Channels;

    sget v4, Lcom/squareup/notification/R$string;->notification_channel_payments:I

    const-string v1, "PAYMENTS"

    const/4 v2, 0x0

    const-string v3, "channel-1"

    const/4 v5, 0x1

    const/4 v6, 0x5

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v7, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    .line 14
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v12, Lcom/squareup/notification/R$string;->notification_channel_updates:I

    const-string v9, "UPDATES"

    const/4 v10, 0x1

    const-string v11, "channel-2"

    const/4 v13, 0x1

    const/4 v14, 0x2

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->UPDATES:Lcom/squareup/notification/Channels;

    .line 15
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v5, Lcom/squareup/notification/R$string;->notification_channel_messages:I

    const-string v2, "CUSTOMER_MESSAGES"

    const/4 v3, 0x2

    const-string v4, "channel-3"

    const/4 v6, 0x1

    const/4 v7, 0x3

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->CUSTOMER_MESSAGES:Lcom/squareup/notification/Channels;

    .line 17
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v12, Lcom/squareup/notification/R$string;->notification_channel_profile_errors:I

    const-string v9, "MERCHANT_PROFILE"

    const/4 v10, 0x3

    const-string v11, "channel-4"

    const/4 v13, 0x0

    const/4 v14, 0x3

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->MERCHANT_PROFILE:Lcom/squareup/notification/Channels;

    .line 19
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v5, Lcom/squareup/notification/R$string;->notification_channel_storage_errors:I

    const-string v2, "APP_STORAGE_ERRORS"

    const/4 v3, 0x4

    const-string v4, "channel-5"

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->APP_STORAGE_ERRORS:Lcom/squareup/notification/Channels;

    .line 21
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v12, Lcom/squareup/notification/R$string;->notification_channel_background_processing:I

    const-string v9, "BACKGROUND_PROCESSING"

    const/4 v10, 0x5

    const-string v11, "channel-6"

    const/4 v13, 0x1

    const/4 v14, 0x2

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->BACKGROUND_PROCESSING:Lcom/squareup/notification/Channels;

    .line 23
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v5, Lcom/squareup/notification/R$string;->notification_channel_support_messaging:I

    const-string v2, "SUPPORT_MESSAGE"

    const/4 v3, 0x6

    const-string v4, "channel-7"

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->SUPPORT_MESSAGE:Lcom/squareup/notification/Channels;

    .line 25
    new-instance v0, Lcom/squareup/notification/Channels;

    sget v12, Lcom/squareup/notification/R$string;->notification_channel_account_issues:I

    const-string v9, "ACCOUNT_ISSUE"

    const/4 v10, 0x7

    const-string v11, "channel-8"

    const/4 v13, 0x0

    const/4 v14, 0x5

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/squareup/notification/Channels;-><init>(Ljava/lang/String;ILjava/lang/String;IZI)V

    sput-object v0, Lcom/squareup/notification/Channels;->ACCOUNT_ISSUE:Lcom/squareup/notification/Channels;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/notification/Channels;

    .line 9
    sget-object v1, Lcom/squareup/notification/Channels;->PAYMENTS:Lcom/squareup/notification/Channels;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->UPDATES:Lcom/squareup/notification/Channels;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->CUSTOMER_MESSAGES:Lcom/squareup/notification/Channels;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->MERCHANT_PROFILE:Lcom/squareup/notification/Channels;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->APP_STORAGE_ERRORS:Lcom/squareup/notification/Channels;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->BACKGROUND_PROCESSING:Lcom/squareup/notification/Channels;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->SUPPORT_MESSAGE:Lcom/squareup/notification/Channels;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/notification/Channels;->ACCOUNT_ISSUE:Lcom/squareup/notification/Channels;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/notification/Channels;->$VALUES:[Lcom/squareup/notification/Channels;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZI)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput-object p3, p0, Lcom/squareup/notification/Channels;->channelId:Ljava/lang/String;

    .line 37
    iput p4, p0, Lcom/squareup/notification/Channels;->nameResId:I

    .line 38
    iput-boolean p5, p0, Lcom/squareup/notification/Channels;->defaultChannel:Z

    .line 39
    iput p6, p0, Lcom/squareup/notification/Channels;->importance:I

    return-void
.end method

.method public static getChannelById(Ljava/lang/String;)Lcom/squareup/notification/Channels;
    .locals 5

    .line 43
    invoke-static {}, Lcom/squareup/notification/Channels;->values()[Lcom/squareup/notification/Channels;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 44
    iget-object v4, v3, Lcom/squareup/notification/Channels;->channelId:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/notification/Channels;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/notification/Channels;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/notification/Channels;

    return-object p0
.end method

.method public static values()[Lcom/squareup/notification/Channels;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/notification/Channels;->$VALUES:[Lcom/squareup/notification/Channels;

    invoke-virtual {v0}, [Lcom/squareup/notification/Channels;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/notification/Channels;

    return-object v0
.end method


# virtual methods
.method public getChannelId()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/notification/Channels;->channelId:Ljava/lang/String;

    return-object v0
.end method

.method public getImportance()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/notification/Channels;->importance:I

    return v0
.end method

.method public getNameResId()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/notification/Channels;->nameResId:I

    return v0
.end method

.method public isDefaultChannel()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/squareup/notification/Channels;->defaultChannel:Z

    return v0
.end method
