.class public Lcom/squareup/foregroundservice/mock/MockForegroundServiceStarter;
.super Ljava/lang/Object;
.source "MockForegroundServiceStarter.java"

# interfaces
.implements Lcom/squareup/foregroundservice/ForegroundServiceStarter;


# instance fields
.field private final context:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/foregroundservice/mock/MockForegroundServiceStarter;->context:Landroid/app/Application;

    return-void
.end method


# virtual methods
.method public maybePromoteToForeground(Landroid/app/Service;Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public setStartInForegroundEnabled(Z)V
    .locals 0

    return-void
.end method

.method public startInForeground(Landroid/content/Intent;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/foregroundservice/mock/MockForegroundServiceStarter;->context:Landroid/app/Application;

    invoke-virtual {v0, p1}, Landroid/app/Application;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public startWaitingForForeground(Landroid/content/Intent;)V
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/foregroundservice/mock/MockForegroundServiceStarter;->startInForeground(Landroid/content/Intent;)V

    return-void
.end method
