.class public final Lcom/squareup/disputes/SummaryCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SummaryCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/SummaryCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSummaryCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SummaryCoordinator.kt\ncom/squareup/disputes/SummaryCoordinator\n*L\n1#1,136:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0000\u0018\u00002\u00020\u0001:\u0001%B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0010\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0016H\u0002J\u001e\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u00052\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0005H\u0002J\u0010\u0010 \u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0005H\u0002J(\u0010!\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00162\u0016\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0016\u0010#\u001a\u00020\u00182\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001eH\u0002J\u0012\u0010$\u001a\u00020\u0018*\u0008\u0012\u0004\u0012\u00020\u00060\u001eH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/disputes/SummaryCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/disputes/Summary$ScreenData;",
        "Lcom/squareup/disputes/SummaryEvent;",
        "Lcom/squareup/disputes/SummaryScreen;",
        "adapter",
        "Lcom/squareup/disputes/SummaryAdapter;",
        "(Lio/reactivex/Observable;Lcom/squareup/disputes/SummaryAdapter;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "context",
        "Landroid/content/Context;",
        "errorContainer",
        "Landroid/view/ViewGroup;",
        "errorView",
        "Lcom/squareup/noho/NohoMessageView;",
        "list",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "spinner",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "bindViews",
        "maybeShowErrorView",
        "screenData",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "maybeShowLoadingSpinner",
        "maybeShowSummary",
        "onScreen",
        "screen",
        "setUpActionBar",
        "backFromSummary",
        "Factory",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final adapter:Lcom/squareup/disputes/SummaryAdapter;

.field private context:Landroid/content/Context;

.field private errorContainer:Landroid/view/ViewGroup;

.field private errorView:Lcom/squareup/noho/NohoMessageView;

.field private list:Landroidx/recyclerview/widget/RecyclerView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/Summary$ScreenData;",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private spinner:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/disputes/SummaryAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/Summary$ScreenData;",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;>;",
            "Lcom/squareup/disputes/SummaryAdapter;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/disputes/SummaryCoordinator;->adapter:Lcom/squareup/disputes/SummaryAdapter;

    return-void
.end method

.method public static final synthetic access$backFromSummary(Lcom/squareup/disputes/SummaryCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/disputes/SummaryCoordinator;->backFromSummary(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/disputes/SummaryCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/disputes/SummaryCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final backFromSummary(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;)V"
        }
    .end annotation

    .line 84
    sget-object v0, Lcom/squareup/disputes/SummaryEvent$BackFromChallengeSummary;->INSTANCE:Lcom/squareup/disputes/SummaryEvent$BackFromChallengeSummary;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 128
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->context:Landroid/content/Context;

    .line 129
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 130
    sget v0, Lcom/squareup/disputes/R$id;->challenge_summary:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    .line 131
    sget v0, Lcom/squareup/disputes/R$id;->spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->spinner:Landroid/view/View;

    .line 132
    sget v0, Lcom/squareup/disputes/R$id;->disputes_error_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->errorContainer:Landroid/view/ViewGroup;

    .line 133
    sget v0, Lcom/squareup/disputes/R$id;->disputes_error:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator;->errorView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final maybeShowErrorView(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/Summary$ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;)V"
        }
    .end annotation

    .line 91
    instance-of v0, p1, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;

    const-string v1, "errorContainer"

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->errorContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->errorView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "errorView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 95
    :cond_1
    sget v1, Lcom/squareup/vectoricons/R$drawable;->circle_alert_96:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setDrawable(I)V

    .line 96
    move-object v1, p1

    check-cast v1, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;

    invoke-virtual {v1}, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;->getTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {v1}, Lcom/squareup/disputes/Summary$ScreenData$SummaryError;->getMessage()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 98
    invoke-virtual {v0}, Lcom/squareup/noho/NohoMessageView;->showPrimaryButton()V

    .line 99
    sget v1, Lcom/squareup/disputes/R$string;->disputes_error_try_again:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 100
    new-instance v1, Lcom/squareup/disputes/SummaryCoordinator$maybeShowErrorView$$inlined$with$lambda$1;

    invoke-direct {v1, p1, p2}, Lcom/squareup/disputes/SummaryCoordinator$maybeShowErrorView$$inlined$with$lambda$1;-><init>(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string p2, "com.squareup.debounce.De\u2026adSummaryAgain)\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    goto :goto_0

    .line 105
    :cond_2
    iget-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator;->errorContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final maybeShowLoadingSpinner(Lcom/squareup/disputes/Summary$ScreenData;)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->spinner:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "spinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    instance-of p1, p1, Lcom/squareup/disputes/Summary$ScreenData$Loading;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final maybeShowSummary(Lcom/squareup/disputes/Summary$ScreenData;)V
    .locals 3

    .line 112
    instance-of v0, p1, Lcom/squareup/disputes/Summary$ScreenData$WithData;

    const-string v1, "list"

    if-eqz v0, :cond_3

    .line 113
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object v0

    if-nez v0, :cond_5

    .line 115
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->adapter:Lcom/squareup/disputes/SummaryAdapter;

    check-cast p1, Lcom/squareup/disputes/Summary$ScreenData$WithData;

    invoke-virtual {p1}, Lcom/squareup/disputes/Summary$ScreenData$WithData;->getChallengeEngine()Lcom/squareup/disputes/ChallengeEngine;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/disputes/SummaryAdapter;->init(Lcom/squareup/disputes/ChallengeEngine;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->adapter:Lcom/squareup/disputes/SummaryAdapter;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_0

    .line 119
    :cond_3
    iget-object p1, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    :cond_5
    :goto_0
    return-void
.end method

.method private final onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/Summary$ScreenData;",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;)V"
        }
    .end annotation

    .line 62
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/disputes/Summary$ScreenData;

    .line 63
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 64
    new-instance v1, Lcom/squareup/disputes/SummaryCoordinator$onScreen$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/disputes/SummaryCoordinator$onScreen$1;-><init>(Lcom/squareup/disputes/SummaryCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 66
    invoke-direct {p0, p2}, Lcom/squareup/disputes/SummaryCoordinator;->setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/disputes/SummaryCoordinator;->maybeShowLoadingSpinner(Lcom/squareup/disputes/Summary$ScreenData;)V

    .line 68
    invoke-direct {p0, v0}, Lcom/squareup/disputes/SummaryCoordinator;->maybeShowSummary(Lcom/squareup/disputes/Summary$ScreenData;)V

    .line 69
    invoke-direct {p0, v0, p2}, Lcom/squareup/disputes/SummaryCoordinator;->maybeShowErrorView(Lcom/squareup/disputes/Summary$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/SummaryEvent;",
            ">;)V"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 77
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/disputes/SummaryCoordinator$setUpActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/disputes/SummaryCoordinator$setUpActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/disputes/SummaryCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 78
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/disputes/R$string;->dispute_challenge_submission:I

    invoke-direct {p1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 80
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/disputes/SummaryCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_0

    const-string v1, "list"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    iget-object v2, p0, Lcom/squareup/disputes/SummaryCoordinator;->context:Landroid/content/Context;

    if-nez v2, :cond_1

    const-string v3, "context"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "context.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/disputes/SummaryCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/disputes/SummaryCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/SummaryCoordinator$attach$1;-><init>(Lcom/squareup/disputes/SummaryCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { scre\u2026 onScreen(view, screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
