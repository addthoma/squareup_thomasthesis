.class final Lcom/squareup/disputes/DisputesReactor$onReact$5$3;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$5;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/disputes/DisputesState$DetailState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/disputes/DisputesState$DetailState;",
        "it",
        "Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$5;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$3;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/disputes/DisputesState$DetailState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$3;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$5;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {v0}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const-string v2, "it.dispute.payment_token"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->selectedDispute(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    .line 141
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;->getDispute()Lcom/squareup/protos/client/cbms/DisputedPayment;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/disputes/DisputesState$DetailState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$5$3;->invoke(Lcom/squareup/disputes/AllDisputesEvent$SelectDispute;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
