.class public final Lcom/squareup/disputes/DisputesViewFactory$Factory;
.super Ljava/lang/Object;
.source "DisputesViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u00020\n2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesViewFactory$Factory;",
        "",
        "allDisputesFactory",
        "Lcom/squareup/disputes/AllDisputesCoordinator$Factory;",
        "disputesDetailFactory",
        "Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;",
        "challengeSummaryFactory",
        "Lcom/squareup/disputes/SummaryCoordinator$Factory;",
        "(Lcom/squareup/disputes/AllDisputesCoordinator$Factory;Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;Lcom/squareup/disputes/SummaryCoordinator$Factory;)V",
        "create",
        "Lcom/squareup/disputes/DisputesViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allDisputesFactory:Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

.field private final challengeSummaryFactory:Lcom/squareup/disputes/SummaryCoordinator$Factory;

.field private final disputesDetailFactory:Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/AllDisputesCoordinator$Factory;Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;Lcom/squareup/disputes/SummaryCoordinator$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "allDisputesFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputesDetailFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "challengeSummaryFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->allDisputesFactory:Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->disputesDetailFactory:Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->challengeSummaryFactory:Lcom/squareup/disputes/SummaryCoordinator$Factory;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/disputes/DisputesViewFactory$Factory;Ljava/lang/Class;ILjava/lang/Object;)Lcom/squareup/disputes/DisputesViewFactory;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 51
    check-cast p1, Ljava/lang/Class;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesViewFactory;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesViewFactory;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/disputes/DisputesViewFactory;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/disputes/DisputesViewFactory;

    .line 52
    iget-object v1, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->allDisputesFactory:Lcom/squareup/disputes/AllDisputesCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->disputesDetailFactory:Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;

    iget-object v3, p0, Lcom/squareup/disputes/DisputesViewFactory$Factory;->challengeSummaryFactory:Lcom/squareup/disputes/SummaryCoordinator$Factory;

    .line 51
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/disputes/DisputesViewFactory;-><init>(Ljava/lang/Class;Lcom/squareup/disputes/AllDisputesCoordinator$Factory;Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;Lcom/squareup/disputes/SummaryCoordinator$Factory;)V

    return-object v0
.end method
