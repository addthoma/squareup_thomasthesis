.class public final Lcom/squareup/disputes/DisputesTutorialCreator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "DisputesTutorialCreator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/DisputesTutorialCreator$Seed;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesTutorialCreator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesTutorialCreator.kt\ncom/squareup/disputes/DisputesTutorialCreator\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,45:1\n57#2,4:46\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesTutorialCreator.kt\ncom/squareup/disputes/DisputesTutorialCreator\n*L\n27#1,4:46\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B%\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u000e\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0013H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0010\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u000c0\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesTutorialCreator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "tutorialProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/disputes/DisputesTutorial;",
        "appIdling",
        "Lcom/squareup/ui/main/AppIdling;",
        "(Lcom/squareup/disputes/api/HandlesDisputes;Ljavax/inject/Provider;Lcom/squareup/ui/main/AppIdling;)V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "kotlin.jvm.PlatformType",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "triggeredTutorial",
        "Lio/reactivex/Observable;",
        "Seed",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

.field private final seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/api/HandlesDisputes;Ljavax/inject/Provider;Lcom/squareup/ui/main/AppIdling;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;",
            "Lcom/squareup/ui/main/AppIdling;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "handlesDisputes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appIdling"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->tutorialProvider:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    .line 24
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialSeed>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getSeeds$p(Lcom/squareup/disputes/DisputesTutorialCreator;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getTutorialProvider$p(Lcom/squareup/disputes/DisputesTutorialCreator;)Ljavax/inject/Provider;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->tutorialProvider:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 28
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->handlesDisputes:Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-interface {v0}, Lcom/squareup/disputes/api/HandlesDisputes;->shouldShowPopup()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->appIdling:Lcom/squareup/ui/main/AppIdling;

    invoke-virtual {v1}, Lcom/squareup/ui/main/AppIdling;->onIdleChanged()Lio/reactivex/Observable;

    move-result-object v1

    .line 47
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 48
    new-instance v2, Lcom/squareup/disputes/DisputesTutorialCreator$onEnterScope$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/disputes/DisputesTutorialCreator$onEnterScope$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 46
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/squareup/disputes/DisputesTutorialCreator$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/disputes/DisputesTutorialCreator$onEnterScope$2;-><init>(Lcom/squareup/disputes/DisputesTutorialCreator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n        h\u2026orialSeed.NONE)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
