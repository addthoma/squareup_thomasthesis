.class public final Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;
.super Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisputeViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAllDisputesAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AllDisputesAdapter.kt\ncom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,336:1\n1103#2,7:337\n*E\n*S KotlinDebug\n*F\n+ 1 AllDisputesAdapter.kt\ncom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder\n*L\n293#1,7:337\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;",
        "view",
        "Landroid/view/View;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/disputes/AllDisputesEvent;",
        "(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "getWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "bind",
        "",
        "data",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final row:Lcom/squareup/noho/NohoRow;

.field private final workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 285
    sget p2, Lcom/squareup/disputes/R$id;->dispute_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->getDate()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->getAmount()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 290
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->getDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->getDescriptionStyle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setDescriptionAppearanceId(I)V

    .line 293
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->row:Lcom/squareup/noho/NohoRow;

    check-cast v0, Landroid/view/View;

    .line 337
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic bind(Lcom/squareup/disputes/AllDisputesAdapter$Data;)V
    .locals 0

    .line 281
    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;)V

    return-void
.end method

.method public final getWorkflow()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;"
        }
    .end annotation

    .line 283
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
