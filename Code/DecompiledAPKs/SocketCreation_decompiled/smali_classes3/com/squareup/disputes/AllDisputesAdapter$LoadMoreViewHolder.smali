.class public final Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;
.super Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoadMoreViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;",
        "view",
        "Landroid/view/View;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/disputes/AllDisputesEvent;",
        "(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "getWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "bind",
        "",
        "data",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;->getMoreType()Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    move-result-object v0

    sget-object v1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->LOADING_MORE:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    if-ne v0, v1, :cond_0

    return-void

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder$bind$1;-><init>(Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public bridge synthetic bind(Lcom/squareup/disputes/AllDisputesAdapter$Data;)V
    .locals 0

    .line 297
    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->bind(Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;)V

    return-void
.end method

.method public final getWorkflow()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method
