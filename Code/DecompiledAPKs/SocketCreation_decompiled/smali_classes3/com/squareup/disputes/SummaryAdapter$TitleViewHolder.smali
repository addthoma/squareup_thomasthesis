.class public final Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;
.super Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;
.source "SummaryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/SummaryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TitleViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder<",
        "Lcom/squareup/disputes/SummaryAdapter$FormData$Title;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;",
        "Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/SummaryAdapter$FormData$Title;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "title",
        "Landroid/widget/TextView;",
        "bind",
        "",
        "data",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/disputes/SummaryAdapter$BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 104
    sget v0, Lcom/squareup/disputes/R$id;->header_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;->title:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/disputes/SummaryAdapter$FormData$Title;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;->getTitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic bind(Lcom/squareup/disputes/SummaryAdapter$FormData;)V
    .locals 0

    .line 103
    check-cast p1, Lcom/squareup/disputes/SummaryAdapter$FormData$Title;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/SummaryAdapter$TitleViewHolder;->bind(Lcom/squareup/disputes/SummaryAdapter$FormData$Title;)V

    return-void
.end method
