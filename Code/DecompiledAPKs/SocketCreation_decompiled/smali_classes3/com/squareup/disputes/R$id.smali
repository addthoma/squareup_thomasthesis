.class public final Lcom/squareup/disputes/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept_button:I = 0x7f0a00fd

.field public static final answer_row:I = 0x7f0a01c4

.field public static final card_row:I = 0x7f0a02c8

.field public static final challenge_button:I = 0x7f0a02fe

.field public static final challenge_summary:I = 0x7f0a02ff

.field public static final count_row:I = 0x7f0a03b0

.field public static final deadline_row:I = 0x7f0a055b

.field public static final dispute_row:I = 0x7f0a05dc

.field public static final disputed_amount_row:I = 0x7f0a05dd

.field public static final disputed_row:I = 0x7f0a05de

.field public static final disputes_error:I = 0x7f0a05df

.field public static final disputes_error_container:I = 0x7f0a05e0

.field public static final disputes_list:I = 0x7f0a05e1

.field public static final disputes_summary_section:I = 0x7f0a05e2

.field public static final file_category_row:I = 0x7f0a074e

.field public static final header_row:I = 0x7f0a07ca

.field public static final held_amount_description:I = 0x7f0a07d0

.field public static final held_amount_row:I = 0x7f0a07d1

.field public static final overview_row:I = 0x7f0a0b7c

.field public static final protection_row:I = 0x7f0a0c85

.field public static final reason_row:I = 0x7f0a0cfc

.field public static final receipt_row:I = 0x7f0a0d10

.field public static final spinner:I = 0x7f0a0ebc

.field public static final transaction_amount_row:I = 0x7f0a1065

.field public static final transaction_date_row:I = 0x7f0a1067

.field public static final view_submission:I = 0x7f0a1106


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
