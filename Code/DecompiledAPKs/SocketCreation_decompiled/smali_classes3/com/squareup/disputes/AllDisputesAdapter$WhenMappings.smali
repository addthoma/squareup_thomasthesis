.class public final synthetic Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->values()[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_HEADER:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_SUMMARY:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_DISPUTE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_LOAD_MORE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    invoke-virtual {v1}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/ActionableStatus;->values()[Lcom/squareup/protos/client/cbms/ActionableStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->VIEWABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_DISPUTED_AMOUNT_TOO_LOW:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NO_ACTION_REFUNDED:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/ActionableStatus;->NOT_ACTIONABLE:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    const/4 v5, 0x5

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->values()[Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->WON:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->LOST:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ACCEPTED:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
