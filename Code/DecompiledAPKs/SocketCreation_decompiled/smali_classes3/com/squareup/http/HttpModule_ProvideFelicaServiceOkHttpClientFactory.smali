.class public final Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;
.super Ljava/lang/Object;
.source "HttpModule_ProvideFelicaServiceOkHttpClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field private final okHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;->okHttpClientProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)",
            "Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;

    invoke-direct {v0, p0}, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideFelicaServiceOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/http/HttpModule;->provideFelicaServiceOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/OkHttpClient;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;->okHttpClientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    invoke-static {v0}, Lcom/squareup/http/HttpModule_ProvideFelicaServiceOkHttpClientFactory;->provideFelicaServiceOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method
