.class public final Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;
.super Ljava/lang/Object;
.source "HttpModule_ProvideCogsOkHttpClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field private final okHttpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;->okHttpClientProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)",
            "Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;

    invoke-direct {v0, p0}, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCogsOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/http/HttpModule;->provideCogsOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/OkHttpClient;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;->okHttpClientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    invoke-static {v0}, Lcom/squareup/http/HttpModule_ProvideCogsOkHttpClientFactory;->provideCogsOkHttpClient(Lokhttp3/OkHttpClient;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method
