.class public final Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;
.super Lcom/squareup/analytics/event/v1/ViewEvent;
.source "LoyaltyReportViewEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;",
        "Lcom/squareup/analytics/event/v1/ViewEvent;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;

    invoke-direct {v0}, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;-><init>()V

    sput-object v0, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;->INSTANCE:Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY_REPORT:Lcom/squareup/analytics/RegisterViewName;

    check-cast v0, Lcom/squareup/analytics/EventNamedView;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;)V

    return-void
.end method
