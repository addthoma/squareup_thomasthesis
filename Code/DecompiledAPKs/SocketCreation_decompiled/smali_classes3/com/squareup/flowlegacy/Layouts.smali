.class public final Lcom/squareup/flowlegacy/Layouts;
.super Ljava/lang/Object;
.source "Layouts.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createView(Landroid/content/Context;Lcom/squareup/container/LayoutScreen;)Landroid/view/View;
    .locals 2

    .line 18
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 19
    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p0

    .line 20
    invoke-interface {p1}, Lcom/squareup/container/LayoutScreen;->screenLayout()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    .line 21
    instance-of v0, p1, Lcom/squareup/coordinators/CoordinatorProvider;

    if-eqz v0, :cond_0

    .line 22
    check-cast p1, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {p0, p1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    :cond_0
    return-object p0
.end method
