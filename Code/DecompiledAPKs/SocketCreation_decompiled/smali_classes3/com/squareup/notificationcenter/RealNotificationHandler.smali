.class public final Lcom/squareup/notificationcenter/RealNotificationHandler;
.super Ljava/lang/Object;
.source "RealNotificationHandler.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/NotificationHandler;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationHandler.kt\ncom/squareup/notificationcenter/RealNotificationHandler\n*L\n1#1,67:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationHandler;",
        "Lcom/squareup/notificationcenter/NotificationHandler;",
        "clientActionTranslationDispatcher",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "notificationCenterAnalytics",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V",
        "handleNotification",
        "",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

.field private final notificationCenterAnalytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;


# direct methods
.method public constructor <init>(Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clientActionTranslationDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationCenterAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->notificationCenterAnalytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-void
.end method


# virtual methods
.method public handleNotification(Lcom/squareup/notificationcenterdata/Notification;)V
    .locals 5

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDestination()Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/squareup/notificationcenter/RealNotificationHandlerKt;->access$getClientAction$p(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->clientActionTranslationDispatcher:Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;

    invoke-static {v0}, Lcom/squareup/notificationcenter/RealNotificationHandlerKt;->access$getClientAction$p(Lcom/squareup/notificationcenterdata/Notification$Destination;)Lcom/squareup/protos/client/ClientAction;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;->handle(Lcom/squareup/protos/client/ClientAction;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 25
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->notificationCenterAnalytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-interface {v0, p1, v2, v3}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logNotificationClicked(Lcom/squareup/notificationcenterdata/Notification;ZZ)V

    return-void

    .line 33
    :cond_1
    invoke-static {v0}, Lcom/squareup/notificationcenter/RealNotificationHandlerKt;->access$getExternalUrl$p(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;

    move-result-object v0

    .line 34
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_4

    .line 35
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->notificationCenterAnalytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-interface {v0, p1, v3, v2}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logNotificationClicked(Lcom/squareup/notificationcenterdata/Notification;ZZ)V

    return-void

    .line 45
    :cond_4
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationHandler;->notificationCenterAnalytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-interface {v0, p1, v3, v3}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logNotificationClicked(Lcom/squareup/notificationcenterdata/Notification;ZZ)V

    return-void
.end method
