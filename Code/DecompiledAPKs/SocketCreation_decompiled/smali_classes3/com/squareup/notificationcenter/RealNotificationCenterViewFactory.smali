.class public final Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealNotificationCenterViewFactory.kt"

# interfaces
.implements Lcom/squareup/notificationcenter/NotificationCenterViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory;",
        "Lcom/squareup/notificationcenter/NotificationCenterViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "notificationCenterLayoutRunnerFactory",
        "Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;",
        "openInBrowserDialogFactory",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;",
        "(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "notificationCenterLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openInBrowserDialogFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 11
    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Binding;-><init>(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 12
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    const-class v1, Lcom/squareup/notificationcenter/OpenInBrowserDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 14
    new-instance v2, Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory$1;

    invoke-direct {v2, p2}, Lcom/squareup/notificationcenter/RealNotificationCenterViewFactory$1;-><init>(Lcom/squareup/notificationcenter/OpenInBrowserDialogFactory$Factory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 12
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
