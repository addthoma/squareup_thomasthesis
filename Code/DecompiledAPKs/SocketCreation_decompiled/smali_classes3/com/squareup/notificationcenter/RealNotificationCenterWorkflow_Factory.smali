.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealNotificationCenterWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationResolver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationResolver;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationResolver;",
            ">;)",
            "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;Lcom/squareup/notificationcenterdata/NotificationsRepository;Lcom/squareup/notificationcenterdata/NotificationResolver;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogWorkflow;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
            "Lcom/squareup/notificationcenterdata/NotificationsRepository;",
            "Lcom/squareup/notificationcenterdata/NotificationResolver;",
            ")",
            "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;-><init>(Ljavax/inject/Provider;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;Lcom/squareup/notificationcenterdata/NotificationsRepository;Lcom/squareup/notificationcenterdata/NotificationResolver;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/notificationcenterdata/NotificationsRepository;

    iget-object v3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notificationcenterdata/NotificationResolver;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;Lcom/squareup/notificationcenterdata/NotificationsRepository;Lcom/squareup/notificationcenterdata/NotificationResolver;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow_Factory;->get()Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    move-result-object v0

    return-object v0
.end method
