.class public final Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;
.super Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.source "RealNotificationCenterAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationsLoaded"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0017\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u000cH\u00c6\u0003JY\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cH\u00c6\u0001J\u0013\u0010\"\u001a\u00020\u00032\u0008\u0010#\u001a\u0004\u0018\u00010$H\u00d6\u0003J\t\u0010%\u001a\u00020\u0005H\u00d6\u0001J\t\u0010&\u001a\u00020\u000cH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000fR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u000f\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;",
        "has_unread_notification",
        "",
        "account_notification_count",
        "",
        "read_account_notification_count",
        "unread_account_notification_count",
        "product_notification_count",
        "unread_product_notification_count",
        "read_product_notification_count",
        "tab_opened",
        "",
        "(ZIIIIIILjava/lang/String;)V",
        "getAccount_notification_count",
        "()I",
        "getHas_unread_notification",
        "()Z",
        "getProduct_notification_count",
        "getRead_account_notification_count",
        "getRead_product_notification_count",
        "getTab_opened",
        "()Ljava/lang/String;",
        "getUnread_account_notification_count",
        "getUnread_product_notification_count",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final account_notification_count:I

.field private final has_unread_notification:Z

.field private final product_notification_count:I

.field private final read_account_notification_count:I

.field private final read_product_notification_count:I

.field private final tab_opened:Ljava/lang/String;

.field private final unread_account_notification_count:I

.field private final unread_product_notification_count:I


# direct methods
.method public constructor <init>(ZIIIIIILjava/lang/String;)V
    .locals 2

    const-string v0, "tab_opened"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_LOADED:Lcom/squareup/analytics/RegisterActionName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;-><init>(Lcom/squareup/analytics/RegisterActionName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    iput p2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    iput p3, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    iput p4, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    iput p5, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    iput p6, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    iput p7, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    iput-object p8, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;ZIIIIIILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget v3, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget v4, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget v5, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget v8, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move p5, v6

    move p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->copy(ZIIIIIILjava/lang/String;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    return v0
.end method

.method public final component8()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZIIIIIILjava/lang/String;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;
    .locals 10

    const-string v0, "tab_opened"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;-><init>(ZIIIIIILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAccount_notification_count()I
    .locals 1

    .line 163
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    return v0
.end method

.method public final getHas_unread_notification()Z
    .locals 1

    .line 162
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    return v0
.end method

.method public final getProduct_notification_count()I
    .locals 1

    .line 166
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    return v0
.end method

.method public final getRead_account_notification_count()I
    .locals 1

    .line 164
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    return v0
.end method

.method public final getRead_product_notification_count()I
    .locals 1

    .line 168
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    return v0
.end method

.method public final getTab_opened()Ljava/lang/String;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    return-object v0
.end method

.method public final getUnread_account_notification_count()I
    .locals 1

    .line 165
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    return v0
.end method

.method public final getUnread_product_notification_count()I
    .locals 1

    .line 167
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotificationsLoaded(has_unread_notification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->has_unread_notification:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", account_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->account_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", read_account_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_account_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unread_account_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_account_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", product_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->product_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unread_product_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->unread_product_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", read_product_notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->read_product_notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tab_opened="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationsLoaded;->tab_opened:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
