.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 NotificationCenterRecyclerFactory.kt\ncom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,87:1\n44#2,13:88\n57#2:108\n1103#3,7:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$$special$$inlined$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $browserIcon$inlined:Landroid/widget/ImageView;

.field final synthetic $container$inlined:Landroid/view/ViewGroup;

.field final synthetic $content$inlined:Lcom/squareup/noho/NohoLabel;

.field final synthetic $dot$inlined:Landroid/widget/ImageView;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic $title$inlined:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoLabel;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$title$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p3, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$content$inlined:Lcom/squareup/noho/NohoLabel;

    iput-object p4, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$dot$inlined:Landroid/widget/ImageView;

    iput-object p5, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$browserIcon$inlined:Landroid/widget/ImageView;

    iput-object p6, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$container$inlined:Landroid/view/ViewGroup;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getTextColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 90
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$title$inlined:Lcom/squareup/noho/NohoLabel;

    const-string/jumbo v1, "title"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$title$inlined:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$content$inlined:Lcom/squareup/noho/NohoLabel;

    const-string v1, "content"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getContent()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$content$inlined:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 96
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$dot$inlined:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getDotDrawable()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$dot$inlined:Landroid/widget/ImageView;

    const-string v0, "dot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getDotContentDescription()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$browserIcon$inlined:Landroid/widget/ImageView;

    const-string v0, "browserIcon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getShowOpenInBrowserIcon()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 100
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;->getOnClickHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1;->$container$inlined:Landroid/view/ViewGroup;

    check-cast p2, Landroid/view/View;

    .line 101
    new-instance v0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1$1;

    invoke-direct {v0, p1}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactoryKt$createNotificationRow$$inlined$create$1$lambda$1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
