.class public interface abstract Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
.super Ljava/lang/Object;
.source "NotificationCenterAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J,\u0010\n\u001a\u00020\u00032\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH&J\u001e\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000cH&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "",
        "logNotificationClicked",
        "",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "openedInternally",
        "",
        "openedExternally",
        "logNotificationOpened",
        "logNotificationsLoaded",
        "importantNotifications",
        "",
        "generalNotifications",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "logTabToggled",
        "notifications",
        "logWebBrowserDialogCanceled",
        "logWebBrowserDialogOpened",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logNotificationClicked(Lcom/squareup/notificationcenterdata/Notification;ZZ)V
.end method

.method public abstract logNotificationOpened(Lcom/squareup/notificationcenterdata/Notification;)V
.end method

.method public abstract logNotificationsLoaded(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            ")V"
        }
    .end annotation
.end method

.method public abstract logTabToggled(Lcom/squareup/notificationcenter/NotificationCenterTab;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract logWebBrowserDialogCanceled(Lcom/squareup/notificationcenterdata/Notification;)V
.end method

.method public abstract logWebBrowserDialogOpened(Lcom/squareup/notificationcenterdata/Notification;)V
.end method
