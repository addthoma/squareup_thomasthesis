.class public final Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;
.super Ljava/lang/Object;
.source "RealBluetoothDevicesCountInitializer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothManagerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/Context;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;)",
            "Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;"
        }
    .end annotation

    .line 53
    new-instance v6, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/bluetooth/BluetoothManager;",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ")",
            "Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;"
        }
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    iget-object v2, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v3, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/BluetoothUtils;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->newInstance(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer_Factory;->get()Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    move-result-object v0

    return-object v0
.end method
