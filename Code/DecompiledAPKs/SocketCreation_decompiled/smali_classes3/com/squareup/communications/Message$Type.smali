.class public abstract Lcom/squareup/communications/Message$Type;
.super Ljava/lang/Object;
.source "Message.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/communications/Message;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;,
        Lcom/squareup/communications/Message$Type$AlertCustomerComms;,
        Lcom/squareup/communications/Message$Type$AlertHighPriority;,
        Lcom/squareup/communications/Message$Type$AlertResolveDispute;,
        Lcom/squareup/communications/Message$Type$AlertSetupPos;,
        Lcom/squareup/communications/Message$Type$AlertSupportCenter;,
        Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;,
        Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;,
        Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;,
        Lcom/squareup/communications/Message$Type$Unsupported;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\n\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\n\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/communications/Message$Type;",
        "Landroid/os/Parcelable;",
        "()V",
        "AlertCustomerComms",
        "AlertHighPriority",
        "AlertResolveDispute",
        "AlertSetupPos",
        "AlertSupportCenter",
        "AlertToCompleteOrders",
        "MarketingToCustomersYouWantToAcquire",
        "MarketingToYourExistingCustomers",
        "NonUrgentBusinessNoticesForSellers",
        "Unsupported",
        "Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;",
        "Lcom/squareup/communications/Message$Type$AlertCustomerComms;",
        "Lcom/squareup/communications/Message$Type$AlertHighPriority;",
        "Lcom/squareup/communications/Message$Type$AlertResolveDispute;",
        "Lcom/squareup/communications/Message$Type$AlertSetupPos;",
        "Lcom/squareup/communications/Message$Type$AlertSupportCenter;",
        "Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;",
        "Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;",
        "Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;",
        "Lcom/squareup/communications/Message$Type$Unsupported;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 65
    invoke-direct {p0}, Lcom/squareup/communications/Message$Type;-><init>()V

    return-void
.end method
