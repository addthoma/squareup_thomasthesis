.class public interface abstract Lcom/squareup/communications/service/CommunicationsService;
.super Ljava/lang/Object;
.source "CommunicationsService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/communications/service/CommunicationsService;",
        "",
        "getMessageUnits",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
        "request",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;",
        "trackEngagement",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementResponse;",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getMessageUnits(Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/messageservice/service/GetMessageUnitsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v2/messages"
    .end annotation
.end method

.method public abstract trackEngagement(Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/messageservice/service/TrackEngagementResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/api/v2/messages/events"
    .end annotation
.end method
