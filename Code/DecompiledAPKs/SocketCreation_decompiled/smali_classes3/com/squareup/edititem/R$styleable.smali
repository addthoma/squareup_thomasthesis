.class public final Lcom/squareup/edititem/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/edititem/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final DiscountRuleSectionView:[I

.field public static final DiscountRuleSectionView_android_paddingBottom:I = 0x3

.field public static final DiscountRuleSectionView_android_paddingLeft:I = 0x0

.field public static final DiscountRuleSectionView_android_paddingRight:I = 0x2

.field public static final DiscountRuleSectionView_android_paddingTop:I = 0x1

.field public static final DiscountRuleSectionView_sectionTitle:I = 0x4

.field public static final EditItemItemOptionAndValuesRow:[I

.field public static final EditItemItemOptionAndValuesRow_android_minHeight:I = 0x0

.field public static final EditItemItemOptionAndValuesRow_sqOptionNamePhoneAppearance:I = 0x1

.field public static final EditItemItemOptionAndValuesRow_sqOptionNameTabletAppearance:I = 0x2

.field public static final EditItemItemOptionAndValuesRow_sqOptionValuesPhoneAppearance:I = 0x3

.field public static final EditItemItemOptionAndValuesRow_sqOptionValuesTabletAppearance:I = 0x4

.field public static final StockCountRow:[I

.field public static final StockCountRow_labelPaddingLeft:I = 0x0

.field public static final StockCountRow_showLabel:I = 0x1

.field public static final StockCountRow_sqHideBorder:I = 0x2

.field public static final StockCountRow_sqLabelLayoutWeight:I = 0x3

.field public static final StockCountRow_stockFieldAlignment:I = 0x4

.field public static final StockCountRow_stockFieldPaddingLeft:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x5

    new-array v1, v0, [I

    .line 429
    fill-array-data v1, :array_0

    sput-object v1, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView:[I

    new-array v0, v0, [I

    .line 435
    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/edititem/R$styleable;->EditItemItemOptionAndValuesRow:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    .line 441
    fill-array-data v0, :array_2

    sput-object v0, Lcom/squareup/edititem/R$styleable;->StockCountRow:[I

    return-void

    :array_0
    .array-data 4
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x7f04033c
    .end array-data

    :array_1
    .array-data 4
        0x1010140
        0x7f04039f
        0x7f0403a0
        0x7f0403a1
        0x7f0403a2
    .end array-data

    :array_2
    .array-data 4
        0x7f040234
        0x7f04034b
        0x7f040382
        0x7f040392
        0x7f0403f7
        0x7f0403f8
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
