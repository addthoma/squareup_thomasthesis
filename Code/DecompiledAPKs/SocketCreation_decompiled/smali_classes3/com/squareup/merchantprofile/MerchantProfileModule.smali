.class public abstract Lcom/squareup/merchantprofile/MerchantProfileModule;
.super Ljava/lang/Object;
.source "MerchantProfileModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/merchantprofile/MerchantProfileModule;",
        "",
        "()V",
        "bindProfileUpdater",
        "Lcom/squareup/merchantprofile/MerchantProfileUpdater;",
        "updater",
        "Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;",
        "bindUpdaterIntoLoggedIn",
        "Lmortar/Scoped;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindProfileUpdater(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)Lcom/squareup/merchantprofile/MerchantProfileUpdater;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindUpdaterIntoLoggedIn(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForLoggedIn;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
