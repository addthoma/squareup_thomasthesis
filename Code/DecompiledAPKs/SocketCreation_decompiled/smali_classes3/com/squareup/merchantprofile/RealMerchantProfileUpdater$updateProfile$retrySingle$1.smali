.class final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfile(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0010\u0010\u0002\u001a\u000c\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;->this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;)V"
        }
    .end annotation

    .line 169
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    .line 170
    iget-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;->this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    invoke-static {p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->access$cancelErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "[MerchantProfileUpdater] Merchant profile update failed too often, giving up and sending notification."

    .line 172
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;->this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    invoke-static {p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->access$sendErrorNotification(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$1;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
