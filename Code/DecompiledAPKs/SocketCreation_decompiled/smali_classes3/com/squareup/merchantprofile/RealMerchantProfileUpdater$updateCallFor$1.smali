.class final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateCallFor(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

.field final synthetic this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;->this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    iput-object p2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;->$snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;>;"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;->this$0:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    iget-object v1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;->$snapshot:Lcom/squareup/merchantprofile/MerchantProfileSnapshot;

    invoke-static {v0, v1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->access$doUpdateProfileCall(Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateCallFor$1;->call()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
