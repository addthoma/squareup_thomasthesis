.class public final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p8, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p9, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg8Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantprofile/MerchantProfileService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;"
        }
    .end annotation

    .line 65
    new-instance v10, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;
    .locals 11

    .line 72
    new-instance v10, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;-><init>(Landroid/app/Application;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;
    .locals 10

    .line 56
    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/account/PersistentAccountService;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/merchantprofile/MerchantProfileService;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/notification/NotificationWrapper;

    iget-object v0, p0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/AppNameFormatter;

    invoke-static/range {v1 .. v9}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/account/PersistentAccountService;Lcom/squareup/merchantprofile/MerchantProfileService;Landroid/app/NotificationManager;Ljava/util/concurrent/Executor;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater_Factory;->get()Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;

    move-result-object v0

    return-object v0
.end method
