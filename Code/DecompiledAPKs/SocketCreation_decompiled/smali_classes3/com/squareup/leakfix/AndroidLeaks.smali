.class public final Lcom/squareup/leakfix/AndroidLeaks;
.super Ljava/lang/Object;
.source "AndroidLeaks.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAndroidLeaks.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AndroidLeaks.kt\ncom/squareup/leakfix/AndroidLeaks\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,431:1\n9654#2,9:432\n11416#2,2:441\n9663#2:443\n*E\n*S KotlinDebug\n*F\n+ 1 AndroidLeaks.kt\ncom/squareup/leakfix/AndroidLeaks\n*L\n215#1,9:432\n215#1,2:441\n215#1:443\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0002J\u0008\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0008\u0010\u000f\u001a\u00020\nH\u0002J\u0010\u0010\u0010\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u0008\u0010\u0012\u001a\u00020\nH\u0002J\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0007J\u001a\u0010\u0014\u001a\u00020\n*\u00020\u00152\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0017H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/leakfix/AndroidLeaks;",
        "",
        "()V",
        "backgroundExecutor",
        "Ljava/util/concurrent/ScheduledExecutorService;",
        "kotlin.jvm.PlatformType",
        "findAllHandlerThreads",
        "",
        "Landroid/os/HandlerThread;",
        "fixAccessibilityNodeInfoLeak",
        "",
        "fixIMMmCurRootViewLeak",
        "application",
        "Landroid/app/Application;",
        "fixMediaSessionLegacyHelperLeak",
        "fixSpellCheckerLeak",
        "fixTextLinePoolLeak",
        "fixUserManagerLeak",
        "flushHandlerThreads",
        "plugLeaks",
        "onEachIdle",
        "Landroid/os/Handler;",
        "onIdle",
        "Lkotlin/Function0;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

.field private static final backgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0}, Lcom/squareup/leakfix/AndroidLeaks;-><init>()V

    sput-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    const-string v0, "android-leaks"

    .line 31
    invoke-static {v0}, Lcom/squareup/thread/Threads;->backgroundThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1, v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lcom/squareup/leakfix/AndroidLeaks;->backgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$findAllHandlerThreads(Lcom/squareup/leakfix/AndroidLeaks;)Ljava/util/List;
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/leakfix/AndroidLeaks;->findAllHandlerThreads()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onEachIdle(Lcom/squareup/leakfix/AndroidLeaks;Landroid/os/Handler;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/leakfix/AndroidLeaks;->onEachIdle(Landroid/os/Handler;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final findAllHandlerThreads()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/os/HandlerThread;",
            ">;"
        }
    .end annotation

    .line 209
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "Thread.currentThread()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->getThreadGroup()Ljava/lang/ThreadGroup;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 210
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/ThreadGroup;->getParent()Ljava/lang/ThreadGroup;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/ThreadGroup;->getParent()Ljava/lang/ThreadGroup;

    move-result-object v0

    const-string v1, "rootGroup.parent"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ThreadGroup;->activeCount()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Thread;

    :goto_1
    const/4 v2, 0x1

    .line 212
    invoke-virtual {v0, v1, v2}, Ljava/lang/ThreadGroup;->enumerate([Ljava/lang/Thread;Z)I

    move-result v2

    array-length v3, v1

    if-ne v2, v3, :cond_2

    .line 213
    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [Ljava/lang/Thread;

    goto :goto_1

    .line 432
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 441
    array-length v2, v1

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_5

    aget-object v4, v1, v3

    .line 215
    instance-of v5, v4, Landroid/os/HandlerThread;

    if-eqz v5, :cond_3

    check-cast v4, Landroid/os/HandlerThread;

    goto :goto_3

    :cond_3
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_4

    .line 440
    invoke-interface {v0, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 443
    :cond_5
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final fixAccessibilityNodeInfoLeak()V
    .locals 8

    .line 227
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    const/16 v0, 0x32

    .line 232
    sget-object v1, Lcom/squareup/leakfix/AndroidLeaks;->backgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lcom/squareup/leakfix/AndroidLeaks$fixAccessibilityNodeInfoLeak$1;

    invoke-direct {v2, v0}, Lcom/squareup/leakfix/AndroidLeaks$fixAccessibilityNodeInfoLeak$1;-><init>(I)V

    check-cast v2, Ljava/lang/Runnable;

    const-wide/16 v3, 0x5

    const-wide/16 v5, 0x5

    .line 236
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 232
    invoke-interface/range {v1 .. v7}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method private final fixIMMmCurRootViewLeak(Landroid/app/Application;)V
    .locals 3

    .line 258
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    const-string v0, "input_method"

    .line 261
    invoke-virtual {p1, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 267
    :try_start_0
    const-class v1, Landroid/view/inputmethod/InputMethodManager;

    const-string v2, "mCurRootView"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const-string v2, "InputMethodManager::clas\u2026aredField(\"mCurRootView\")"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    .line 268
    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 273
    new-instance v2, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;-><init>(Ljava/lang/reflect/Field;Landroid/view/inputmethod/InputMethodManager;)V

    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, v2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void

    :catch_0
    move-exception p1

    .line 270
    check-cast p1, Ljava/lang/Throwable;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Could not read InputMethodManager.mCurRootView field"

    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 261
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final fixMediaSessionLegacyHelperLeak(Landroid/app/Application;)V
    .locals 2

    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    return-void

    .line 57
    :cond_0
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->backgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/squareup/leakfix/AndroidLeaks$fixMediaSessionLegacyHelperLeak$1;

    invoke-direct {v1, p1}, Lcom/squareup/leakfix/AndroidLeaks$fixMediaSessionLegacyHelperLeak$1;-><init>(Landroid/app/Application;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final fixSpellCheckerLeak()V
    .locals 14

    .line 341
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 346
    :try_start_0
    const-class v1, Landroid/view/textservice/TextServicesManager;

    const-string v2, "getInstance"

    new-array v3, v0, [Ljava/lang/Class;

    .line 347
    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const-string/jumbo v3, "textServiceClass.getDeclaredMethod(\"getInstance\")"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "sService"

    .line 349
    invoke-virtual {v1, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const-string/jumbo v3, "textServiceClass.getDeclaredField(\"sService\")"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    .line 350
    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const-string v4, "com.android.internal.textservice.ITextServicesManager"

    .line 353
    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "Class.forName(\"com.andro\u2026ce.ITextServicesManager\")"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "android.view.textservice.SpellCheckerSession"

    .line 355
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "Class.forName(\"android.v\u2026ice.SpellCheckerSession\")"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "mSpellCheckerSessionListener"

    .line 357
    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v12

    const-string v5, "spellCheckSessionClass.g\u2026lCheckerSessionListener\")"

    invoke-static {v12, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 358
    invoke-virtual {v12, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const-string v5, "android.view.textservice.SpellCheckerSession$SpellCheckerSessionListenerImpl"

    .line 361
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "Class.forName(\"android.v\u2026ckerSessionListenerImpl\")"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "mHandler"

    .line 362
    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v8

    const-string v5, "spellCheckerSessionListe\u2026DeclaredField(\"mHandler\")"

    invoke-static {v8, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    invoke-virtual {v8, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const-string v5, "android.view.textservice.SpellCheckerSession$1"

    .line 366
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "Class.forName(\"android.v\u2026.SpellCheckerSession\\$1\")"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v6, "this$0"

    .line 367
    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v9

    const-string v5, "spellCheckSessionHandler\u2026etDeclaredField(\"this$0\")"

    invoke-static {v9, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    invoke-virtual {v9, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    const-string v5, "android.view.textservice.SpellCheckerSession$SpellCheckerSessionListener"

    .line 371
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "Class.forName(\"android.v\u2026lCheckerSessionListener\")"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    new-array v7, v3, [Ljava/lang/Class;

    aput-object v5, v7, v0

    .line 374
    sget-object v5, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$noOpListener$1;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$noOpListener$1;

    check-cast v5, Ljava/lang/reflect/InvocationHandler;

    .line 372
    invoke-static {v6, v7, v5}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v11

    const-string v5, "Proxy.newProxyInstance(\n\u2026 session closed\")\n      }"

    invoke-static {v11, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 380
    invoke-virtual {v2, v6, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-nez v13, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 383
    :cond_1
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    move-object v10, v2

    check-cast v10, Ljava/util/Map;

    .line 386
    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Class;

    aput-object v4, v3, v0

    .line 387
    new-instance v4, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;

    move-object v7, v4

    invoke-direct/range {v7 .. v13}, Lcom/squareup/leakfix/AndroidLeaks$fixSpellCheckerLeak$proxyService$1;-><init>(Ljava/lang/reflect/Field;Ljava/lang/reflect/Field;Ljava/util/Map;Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V

    check-cast v4, Ljava/lang/reflect/InvocationHandler;

    .line 385
    invoke-static {v2, v3, v4}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Proxy.newProxyInstance(\n\u2026ception\n        }\n      }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 425
    invoke-virtual {v1, v6, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 427
    check-cast v1, Ljava/lang/Throwable;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "Unable to fix SpellChecker leak"

    invoke-static {v1, v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final fixTextLinePoolLeak(Landroid/app/Application;)V
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->backgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1;

    invoke-direct {v1, p1}, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1;-><init>(Landroid/app/Application;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private final fixUserManagerLeak(Landroid/app/Application;)V
    .locals 6

    .line 126
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 130
    :try_start_0
    const-class v1, Landroid/os/UserManager;

    const-string v2, "get"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    .line 131
    const-class v5, Landroid/content/Context;

    aput-object v5, v4, v0

    .line 130
    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-string v2, "UserManager::class.java.\u2026ntext::class.java\n      )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    .line 133
    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 135
    check-cast p1, Ljava/lang/Throwable;

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Could not call get() to fix UserManager leak"

    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final flushHandlerThreads()V
    .locals 8

    const-string v0, "HandlerThread-flush"

    .line 145
    invoke-static {v0}, Lcom/squareup/thread/Threads;->backgroundThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    .line 147
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 149
    new-instance v2, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1;

    invoke-direct {v2, v0}, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1;-><init>(Ljava/util/Set;)V

    check-cast v2, Ljava/lang/Runnable;

    .line 185
    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x2

    const-wide/16 v5, 0x3

    .line 149
    invoke-interface/range {v1 .. v7}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method private final onEachIdle(Landroid/os/Handler;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 193
    :try_start_0
    new-instance v0, Lcom/squareup/leakfix/AndroidLeaks$onEachIdle$1;

    invoke-direct {v0, p2}, Lcom/squareup/leakfix/AndroidLeaks$onEachIdle$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static final plugLeaks(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "application"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0, p0}, Lcom/squareup/leakfix/AndroidLeaks;->fixMediaSessionLegacyHelperLeak(Landroid/app/Application;)V

    .line 36
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0, p0}, Lcom/squareup/leakfix/AndroidLeaks;->fixTextLinePoolLeak(Landroid/app/Application;)V

    .line 37
    sget-object v0, Lcom/squareup/leakfix/IMMLeaks;->INSTANCE:Lcom/squareup/leakfix/IMMLeaks;

    invoke-virtual {v0, p0}, Lcom/squareup/leakfix/IMMLeaks;->fixFocusedViewLeak(Landroid/app/Application;)V

    .line 38
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0, p0}, Lcom/squareup/leakfix/AndroidLeaks;->fixUserManagerLeak(Landroid/app/Application;)V

    .line 39
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0}, Lcom/squareup/leakfix/AndroidLeaks;->flushHandlerThreads()V

    .line 40
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0}, Lcom/squareup/leakfix/AndroidLeaks;->fixAccessibilityNodeInfoLeak()V

    .line 41
    sget-object v0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {v0, p0}, Lcom/squareup/leakfix/AndroidLeaks;->fixIMMmCurRootViewLeak(Landroid/app/Application;)V

    .line 42
    sget-object p0, Lcom/squareup/leakfix/AndroidLeaks;->INSTANCE:Lcom/squareup/leakfix/AndroidLeaks;

    invoke-direct {p0}, Lcom/squareup/leakfix/AndroidLeaks;->fixSpellCheckerLeak()V

    return-void
.end method
