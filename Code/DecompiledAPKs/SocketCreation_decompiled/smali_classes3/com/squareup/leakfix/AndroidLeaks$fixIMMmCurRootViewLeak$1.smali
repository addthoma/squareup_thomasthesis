.class public final Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;
.super Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;
.source "AndroidLeaks.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakfix/AndroidLeaks;->fixIMMmCurRootViewLeak(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1",
        "Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;",
        "onActivityDestroyed",
        "",
        "activity",
        "Landroid/app/Activity;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field final synthetic $mCurRootViewField:Ljava/lang/reflect/Field;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Field;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$mCurRootViewField:Ljava/lang/reflect/Field;

    iput-object p2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {p0}, Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    :try_start_0
    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$mCurRootViewField:Ljava/lang/reflect/Field;

    iget-object v1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 277
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const-string v1, "activity.window"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    if-ne p1, v0, :cond_0

    .line 279
    iget-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$mCurRootViewField:Ljava/lang/reflect/Field;

    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$fixIMMmCurRootViewLeak$1;->$inputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 282
    check-cast p1, Ljava/lang/Throwable;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Could not update InputMethodManager.mCurRootView field"

    invoke-static {p1, v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void
.end method
