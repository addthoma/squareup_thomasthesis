.class final Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AndroidLeaks.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $flushHandler:Landroid/os/Handler;

.field final synthetic $scheduleFlush:Lkotlin/jvm/internal/Ref$BooleanRef;


# direct methods
.method constructor <init>(Lkotlin/jvm/internal/Ref$BooleanRef;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->$scheduleFlush:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p2, p0, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->$flushHandler:Landroid/os/Handler;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 167
    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->$scheduleFlush:Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-boolean v0, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->$scheduleFlush:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;->$flushHandler:Landroid/os/Handler;

    new-instance v1, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1$1;

    invoke-direct {v1, p0}, Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1$1;-><init>(Lcom/squareup/leakfix/AndroidLeaks$flushHandlerThreads$1$3$1;)V

    check-cast v1, Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method
