.class public final Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory$InstanceHolder;->access$000()Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSupportsDeviceCodeLogin()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;
    .locals 2

    .line 28
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule$EnableDeviceCodeLoginModule;->provideSupportsDeviceCodeLogin()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;->provideSupportsDeviceCodeLogin()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;->get()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    move-result-object v0

    return-object v0
.end method
