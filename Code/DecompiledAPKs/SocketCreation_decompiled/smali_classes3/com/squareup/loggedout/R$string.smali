.class public final Lcom/squareup/loggedout/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept_credit_cards:I = 0x7f120022

.field public static final accept_documents_legal_text:I = 0x7f120026

.field public static final accept_documents_prompt_message:I = 0x7f120027

.field public static final accept_documents_prompt_title:I = 0x7f120028

.field public static final activation_legal_dialog_title:I = 0x7f120049

.field public static final confirm_email:I = 0x7f120480

.field public static final content_description_enter_email:I = 0x7f1204c2

.field public static final content_description_enter_email_confirm:I = 0x7f1204c3

.field public static final country_selection_prompt_message:I = 0x7f12056f

.field public static final country_selection_prompt_message_no_payments:I = 0x7f120570

.field public static final country_selection_prompt_title:I = 0x7f120571

.field public static final duplicate_account_message:I = 0x7f1208e6

.field public static final duplicate_account_title:I = 0x7f1208e7

.field public static final email_address:I = 0x7f120991

.field public static final email_mismatch:I = 0x7f120998

.field public static final email_mismatch_message:I = 0x7f120999

.field public static final landing_pointofsale_logo:I = 0x7f120eae

.field public static final learn_about_square:I = 0x7f120eb4

.field public static final missing_account_fields:I = 0x7f120fe9

.field public static final password_hint:I = 0x7f121390

.field public static final password_too_short:I = 0x7f121392

.field public static final password_too_short_message:I = 0x7f121393

.field public static final postal_country_prompt:I = 0x7f121457

.field public static final run_and_grow_your_business:I = 0x7f1216ae

.field public static final signing_up:I = 0x7f121809

.field public static final signing_up_fail:I = 0x7f12180a

.field public static final signup_actionbar_sign_up:I = 0x7f12180b

.field public static final signup_lets_get_started_body:I = 0x7f12180c

.field public static final signup_lets_get_started_title:I = 0x7f12180d

.field public static final signup_missing_required_field:I = 0x7f12180e

.field public static final splash_page_accept_payments:I = 0x7f12183e

.field public static final splash_page_build_trust:I = 0x7f12183f

.field public static final splash_page_get_started:I = 0x7f121840

.field public static final splash_page_reports:I = 0x7f121841

.field public static final splash_page_sell_in_minutes:I = 0x7f121842

.field public static final splash_page_sign_in:I = 0x7f121843

.field public static final uppercase_header_account_information:I = 0x7f121b2f

.field public static final uppercase_header_country:I = 0x7f121b37

.field public static final view_documents_legal_text:I = 0x7f121baa


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
