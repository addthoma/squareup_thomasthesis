.class public final Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/AuthenticationServiceEndpoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final dependenciesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;->dependenciesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;",
            ">;)",
            "Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;

    invoke-direct {v0, p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAuthenticationServiceEndpoint(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
    .locals 1

    .line 38
    invoke-static {p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule;->provideAuthenticationServiceEndpoint(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/AuthenticationServiceEndpoint;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;->dependenciesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;

    invoke-static {v0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;->provideAuthenticationServiceEndpoint(Lcom/squareup/ui/login/RealAuthenticationServiceEndpointDependencies$Implementation;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_ProvideAuthenticationServiceEndpointFactory;->get()Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object v0

    return-object v0
.end method
