.class public Lcom/squareup/loggedout/LoggedOutFeatureStarter;
.super Ljava/lang/Object;
.source "LoggedOutFeatureStarter.java"

# interfaces
.implements Lcom/squareup/loggedout/LoggedOutStarter;


# static fields
.field private static final CREATE_ACCOUNT_PATH:Ljava/lang/String; = "/create-account"

.field private static final LOGIN_PATH:Ljava/lang/String; = "/login"

.field private static final ROOT_HOST:Ljava/lang/String; = "root"

.field private static final SQUARE_REGISTER_SCHEME:Ljava/lang/String; = "square-register"


# instance fields
.field private final legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;


# direct methods
.method constructor <init>(Lcom/squareup/account/LegacyAuthenticator;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutFeatureStarter;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-void
.end method

.method private getRootIntentPath(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2

    .line 72
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    return-object v1

    .line 76
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 78
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/loggedout/LoggedOutFeatureStarter;->isRootHost(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/loggedout/LoggedOutFeatureStarter;->isSquareRegisterScheme(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 82
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v1
.end method

.method private isRootHost(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "root"

    .line 94
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private isSquareRegisterScheme(Ljava/lang/String;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const-string v0, "square-register"

    .line 90
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public showLogin(Landroid/content/Context;)V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutFeatureStarter;->legacyAuthenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "showLogin requires that you have already logged out and that you have taken care of any necessary cleanup tasks before the logout."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 54
    instance-of v0, p1, Landroid/app/Activity;

    .line 55
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/squareup/ui/PaymentActivity$StartPaymentActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-nez v0, :cond_0

    const/high16 v2, 0x10000000

    .line 58
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 61
    :cond_0
    invoke-static {v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->putDirectToLogin(Landroid/content/Intent;)V

    .line 62
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    if-eqz v0, :cond_1

    .line 66
    check-cast p1, Landroid/app/Activity;

    const/4 v0, 0x0

    const v1, 0x10a0001

    .line 67
    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    :cond_1
    return-void
.end method

.method public startLoggedOutActivity(Landroid/app/Activity;)V
    .locals 7

    .line 27
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 28
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/squareup/ui/loggedout/LoggedOutActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    invoke-direct {p0, v0}, Lcom/squareup/loggedout/LoggedOutFeatureStarter;->getRootIntentPath(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x512c2bb5

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eq v3, v4, :cond_1

    const v4, 0x5659b49a

    if-eq v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "/login"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const-string v3, "/create-account"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v2, -0x1

    :goto_1
    if-eqz v2, :cond_4

    if-eq v2, v6, :cond_3

    goto :goto_2

    .line 35
    :cond_3
    invoke-static {v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->putDirectToLogin(Landroid/content/Intent;)V

    goto :goto_2

    .line 32
    :cond_4
    invoke-static {v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity;->putDirectToCreateAccount(Landroid/content/Intent;)V

    .line 40
    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 41
    invoke-virtual {p1, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const/high16 v0, 0x10a0000

    .line 44
    invoke-virtual {p1, v0, v5}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method
