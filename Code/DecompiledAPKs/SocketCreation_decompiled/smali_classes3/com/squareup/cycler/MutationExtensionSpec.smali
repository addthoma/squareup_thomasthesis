.class public final Lcom/squareup/cycler/MutationExtensionSpec;
.super Ljava/lang/Object;
.source "RecyclerMutations.kt"

# interfaces
.implements Lcom/squareup/cycler/ExtensionSpec;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/ExtensionSpec<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u001e2\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0006J\u001a\u0010\u000c\u001a\u00020\u001e2\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0006J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00028\u00000.H\u0016J>\u0010\u0017\u001a\u00020\u001e26\u0010,\u001a2\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\u001b\u0012\u0004\u0008\u0008(\u001c\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\u001b\u0012\u0004\u0008\u0008(\u001d\u0012\u0004\u0012\u00020\u001e0\u0018J \u0010#\u001a\u00020\u001e2\u0018\u0010,\u001a\u0014\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001e0\u0018J\u001a\u0010&\u001a\u00020\u001e2\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001e0\u0006R&\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000bR&\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00070\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\t\"\u0004\u0008\u000e\u0010\u000bR\u001a\u0010\u000f\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u001a\u0010\u0014\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\u0011\"\u0004\u0008\u0016\u0010\u0013RL\u0010\u0017\u001a4\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\u001b\u0012\u0004\u0008\u0008(\u001c\u0012\u0013\u0012\u00110\u0019\u00a2\u0006\u000c\u0008\u001a\u0012\u0008\u0008\u001b\u0012\u0004\u0008\u0008(\u001d\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u0018X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001f\u0010 \"\u0004\u0008!\u0010\"R.\u0010#\u001a\u0016\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u0018X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008$\u0010 \"\u0004\u0008%\u0010\"R(\u0010&\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010\t\"\u0004\u0008(\u0010\u000bR\u001a\u0010)\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008*\u0010\u0011\"\u0004\u0008+\u0010\u0013\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/cycler/MutationExtensionSpec;",
        "T",
        "",
        "Lcom/squareup/cycler/ExtensionSpec;",
        "()V",
        "canDropOverItem",
        "Lkotlin/Function1;",
        "",
        "getCanDropOverItem$lib_release",
        "()Lkotlin/jvm/functions/Function1;",
        "setCanDropOverItem$lib_release",
        "(Lkotlin/jvm/functions/Function1;)V",
        "canSwipeToRemoveItem",
        "getCanSwipeToRemoveItem$lib_release",
        "setCanSwipeToRemoveItem$lib_release",
        "dragAndDropEnabled",
        "getDragAndDropEnabled",
        "()Z",
        "setDragAndDropEnabled",
        "(Z)V",
        "longPressDragEnabled",
        "getLongPressDragEnabled",
        "setLongPressDragEnabled",
        "onDragDrop",
        "Lkotlin/Function2;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "originalIndex",
        "dropIndex",
        "",
        "getOnDragDrop$lib_release",
        "()Lkotlin/jvm/functions/Function2;",
        "setOnDragDrop$lib_release",
        "(Lkotlin/jvm/functions/Function2;)V",
        "onMove",
        "getOnMove$lib_release",
        "setOnMove$lib_release",
        "onSwipeToRemove",
        "getOnSwipeToRemove$lib_release",
        "setOnSwipeToRemove$lib_release",
        "swipeToRemoveEnabled",
        "getSwipeToRemoveEnabled",
        "setSwipeToRemoveEnabled",
        "block",
        "create",
        "Lcom/squareup/cycler/Extension;",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private canDropOverItem:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private canSwipeToRemoveItem:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private dragAndDropEnabled:Z

.field private longPressDragEnabled:Z

.field private onDragDrop:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onMove:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onSwipeToRemove:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private swipeToRemoveEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lcom/squareup/cycler/MutationExtensionSpec$canSwipeToRemoveItem$1;->INSTANCE:Lcom/squareup/cycler/MutationExtensionSpec$canSwipeToRemoveItem$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canSwipeToRemoveItem:Lkotlin/jvm/functions/Function1;

    .line 19
    sget-object v0, Lcom/squareup/cycler/MutationExtensionSpec$canDropOverItem$1;->INSTANCE:Lcom/squareup/cycler/MutationExtensionSpec$canDropOverItem$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canDropOverItem:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final canDropOverItem(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canDropOverItem:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final canSwipeToRemoveItem(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canSwipeToRemoveItem:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public create()Lcom/squareup/cycler/Extension;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Extension<",
            "TT;>;"
        }
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/cycler/MutationExtension;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/MutationExtension;-><init>(Lcom/squareup/cycler/MutationExtensionSpec;)V

    check-cast v0, Lcom/squareup/cycler/Extension;

    return-object v0
.end method

.method public final getCanDropOverItem$lib_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canDropOverItem:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getCanSwipeToRemoveItem$lib_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canSwipeToRemoveItem:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getDragAndDropEnabled()Z
    .locals 1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->dragAndDropEnabled:Z

    return v0
.end method

.method public final getLongPressDragEnabled()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->longPressDragEnabled:Z

    return v0
.end method

.method public final getOnDragDrop$lib_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onDragDrop:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getOnMove$lib_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onMove:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getOnSwipeToRemove$lib_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TT;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onSwipeToRemove:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getSwipeToRemoveEnabled()Z
    .locals 1

    .line 26
    iget-boolean v0, p0, Lcom/squareup/cycler/MutationExtensionSpec;->swipeToRemoveEnabled:Z

    return v0
.end method

.method public final onDragDrop(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onDragDrop:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final onMove(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onMove:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final onSwipeToRemove(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onSwipeToRemove:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setCanDropOverItem$lib_release(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canDropOverItem:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setCanSwipeToRemoveItem$lib_release(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->canSwipeToRemoveItem:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setDragAndDropEnabled(Z)V
    .locals 0

    .line 30
    iput-boolean p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->dragAndDropEnabled:Z

    return-void
.end method

.method public final setLongPressDragEnabled(Z)V
    .locals 0

    .line 35
    iput-boolean p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->longPressDragEnabled:Z

    return-void
.end method

.method public final setOnDragDrop$lib_release(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 20
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onDragDrop:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setOnMove$lib_release(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 16
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onMove:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setOnSwipeToRemove$lib_release(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 17
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->onSwipeToRemove:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setSwipeToRemoveEnabled(Z)V
    .locals 0

    .line 26
    iput-boolean p1, p0, Lcom/squareup/cycler/MutationExtensionSpec;->swipeToRemoveEnabled:Z

    return-void
.end method
