.class public final Lcom/squareup/cycler/MutationMap;
.super Ljava/lang/Object;
.source "MutationMap.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMutationMap.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MutationMap.kt\ncom/squareup/cycler/MutationMap\n*L\n1#1,84:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u0015\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u000c\u0018\u00002\u00020\u0001B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u000f\u0008\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0000\u00a2\u0006\u0002\u0010\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0003H\u0002J\u0011\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0003H\u0086\u0002J\u0016\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0003J\u000e\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0003J\u0018\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u0003H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\u0004\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/cycler/MutationMap;",
        "",
        "size",
        "",
        "(I)V",
        "src",
        "(Lcom/squareup/cycler/MutationMap;)V",
        "()V",
        "positions",
        "",
        "getSize",
        "()I",
        "setSize",
        "ensureSize",
        "",
        "pos",
        "get",
        "i",
        "move",
        "from",
        "to",
        "remove",
        "index",
        "swap",
        "a",
        "b",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private positions:[I

.field private size:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/squareup/cycler/MutationMapKt;->access$getEMPTY_ARRAY$p()[I

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/cycler/MutationMap;-><init>()V

    .line 23
    iput p1, p0, Lcom/squareup/cycler/MutationMap;->size:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cycler/MutationMap;)V
    .locals 1

    const-string v0, "src"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/cycler/MutationMap;-><init>()V

    .line 28
    iget-object p1, p1, Lcom/squareup/cycler/MutationMap;->positions:[I

    invoke-virtual {p1}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [I

    iput-object p1, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    return-void
.end method

.method private final ensureSize(I)V
    .locals 8

    const/4 v0, 0x1

    add-int/2addr p1, v0

    .line 66
    iget-object v1, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    array-length v2, v1

    if-ge v2, p1, :cond_3

    .line 70
    array-length v1, v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/16 v0, 0x10

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    .line 71
    :goto_1
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 72
    new-array v7, p1, [I

    .line 73
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v1, v7

    invoke-static/range {v0 .. v6}, Lkotlin/collections/ArraysKt;->copyInto$default([I[IIIIILjava/lang/Object;)[I

    .line 74
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    array-length v0, v0

    :goto_2
    if-ge v0, p1, :cond_2

    .line 75
    aput v0, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 77
    :cond_2
    iput-object v7, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    :cond_3
    return-void
.end method

.method private final swap(II)V
    .locals 3

    .line 58
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/cycler/MutationMap;->ensureSize(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    aget v1, v0, p1

    .line 60
    aget v2, v0, p2

    aput v2, v0, p1

    .line 61
    aput v1, v0, p2

    return-void
.end method


# virtual methods
.method public final get(I)I
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    array-length v1, v0

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    aget p1, v0, p1

    :goto_0
    return p1
.end method

.method public final getSize()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/cycler/MutationMap;->size:I

    return v0
.end method

.method public final move(II)V
    .locals 1

    if-ge p1, p2, :cond_0

    :goto_0
    if-ge p1, p2, :cond_1

    add-int/lit8 v0, p1, 0x1

    .line 42
    invoke-direct {p0, p1, v0}, Lcom/squareup/cycler/MutationMap;->swap(II)V

    move p1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p2, 0x1

    if-lt p1, p2, :cond_1

    :goto_1
    add-int/lit8 v0, p1, -0x1

    .line 46
    invoke-direct {p0, p1, v0}, Lcom/squareup/cycler/MutationMap;->swap(II)V

    if-eq p1, p2, :cond_1

    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final remove(I)V
    .locals 3

    .line 52
    iget v0, p0, Lcom/squareup/cycler/MutationMap;->size:I

    invoke-direct {p0, v0}, Lcom/squareup/cycler/MutationMap;->ensureSize(I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/cycler/MutationMap;->positions:[I

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/squareup/cycler/MutationMap;->size:I

    invoke-static {v0, v0, p1, v1, v2}, Lkotlin/collections/ArraysKt;->copyInto([I[IIII)[I

    .line 54
    iget p1, p0, Lcom/squareup/cycler/MutationMap;->size:I

    add-int/lit8 p1, p1, -0x1

    iput p1, p0, Lcom/squareup/cycler/MutationMap;->size:I

    return-void
.end method

.method public final setSize(I)V
    .locals 0

    .line 35
    iput p1, p0, Lcom/squareup/cycler/MutationMap;->size:I

    return-void
.end method
