.class public interface abstract Lcom/squareup/cycler/ExtensionSpec;
.super Ljava/lang/Object;
.source "ExtensionSpec.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/cycler/ExtensionSpec;",
        "I",
        "",
        "create",
        "Lcom/squareup/cycler/Extension;",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# virtual methods
.method public abstract create()Lcom/squareup/cycler/Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Extension<",
            "TI;>;"
        }
    .end annotation
.end method
