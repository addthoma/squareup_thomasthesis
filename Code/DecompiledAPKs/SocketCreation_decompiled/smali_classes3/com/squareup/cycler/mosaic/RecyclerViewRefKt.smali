.class public final Lcom/squareup/cycler/mosaic/RecyclerViewRefKt;
.super Ljava/lang/Object;
.source "RecyclerViewRef.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "getRecyclerFactory",
        "()Lcom/squareup/recycler/RecyclerFactory;",
        "recycler-mosaic_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 51
    new-instance v0, Lcom/squareup/recycler/RecyclerFactory;

    .line 52
    sget-object v1, Lcom/squareup/thread/CoroutineDispatchers;->INSTANCE:Lcom/squareup/thread/CoroutineDispatchers;

    invoke-virtual {v1}, Lcom/squareup/thread/CoroutineDispatchers;->getMain()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getDefault()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    .line 51
    invoke-direct {v0, v1, v2}, Lcom/squareup/recycler/RecyclerFactory;-><init>(Lkotlinx/coroutines/CoroutineDispatcher;Lkotlinx/coroutines/CoroutineDispatcher;)V

    sput-object v0, Lcom/squareup/cycler/mosaic/RecyclerViewRefKt;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method

.method public static final getRecyclerFactory()Lcom/squareup/recycler/RecyclerFactory;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/cycler/mosaic/RecyclerViewRefKt;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-object v0
.end method
