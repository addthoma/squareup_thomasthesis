.class final Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;
.super Landroidx/recyclerview/widget/ItemTouchHelper$Callback;
.source "RecyclerMutations.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/MutationExtension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyItemTouchCallback"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerMutations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerMutations.kt\ncom/squareup/cycler/MutationExtension$MyItemTouchCallback\n+ 2 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n*L\n1#1,278:1\n57#2,7:279\n57#2,7:286\n57#2,7:293\n57#2,7:300\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerMutations.kt\ncom/squareup/cycler/MutationExtension$MyItemTouchCallback\n*L\n153#1,7:279\n163#1,7:286\n191#1,7:293\n217#1,7:300\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0016J\u0018\u0010\u000e\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u000cH\u0016J\u0008\u0010\u0010\u001a\u00020\u0008H\u0016J \u0010\u0011\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0016J\u001a\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0018\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;",
        "Landroidx/recyclerview/widget/ItemTouchHelper$Callback;",
        "(Lcom/squareup/cycler/MutationExtension;)V",
        "draggedItemCurrentIndex",
        "",
        "draggedItemOriginalIndex",
        "lastActionState",
        "canDropOver",
        "",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "current",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "target",
        "getMovementFlags",
        "viewHolder",
        "isLongPressDragEnabled",
        "onMove",
        "onSelectedChanged",
        "",
        "actionState",
        "onSwiped",
        "direction",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private draggedItemCurrentIndex:I

.field private draggedItemOriginalIndex:I

.field private lastActionState:I

.field final synthetic this$0:Lcom/squareup/cycler/MutationExtension;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/MutationExtension;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 138
    iput-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-direct {p0}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;-><init>()V

    const/4 p1, -0x1

    .line 141
    iput p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemOriginalIndex:I

    .line 142
    iput p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemCurrentIndex:I

    return-void
.end method


# virtual methods
.method public canDropOver(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 1

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "current"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "target"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 191
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p2

    .line 294
    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result p3

    const/4 v0, 0x0

    if-ne p1, p3, :cond_0

    .line 295
    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 298
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p3

    invoke-interface {p3}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result p3

    if-gez p1, :cond_1

    goto :goto_0

    :cond_1
    if-le p3, p1, :cond_2

    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 194
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtensionSpec;->getCanDropOverItem$lib_release()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_2
    :goto_0
    return v0
.end method

.method public getMovementFlags(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)I
    .locals 3

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "viewHolder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getFrozen()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 150
    invoke-static {v0, v0}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;->makeMovementFlags(II)I

    move-result p1

    return p1

    .line 152
    :cond_0
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtensionSpec;->getDragAndDropEnabled()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 153
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    .line 154
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    .line 280
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 281
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 284
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v2

    if-gez v1, :cond_2

    goto :goto_0

    :cond_2
    if-le v2, v1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    const/4 p1, 0x3

    goto :goto_1

    :cond_3
    :goto_0
    const/4 p1, 0x0

    .line 162
    :goto_1
    iget-object v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {v1}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cycler/MutationExtensionSpec;->getSwipeToRemoveEnabled()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 163
    iget-object v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {v1}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v1

    .line 164
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    .line 287
    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v2

    if-ne p2, v2, :cond_4

    .line 288
    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_2

    .line 291
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v2

    if-gez p2, :cond_5

    goto :goto_2

    :cond_5
    if-le v2, p2, :cond_6

    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p2

    .line 166
    iget-object v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {v1}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cycler/MutationExtensionSpec;->getCanSwipeToRemoveItem$lib_release()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-interface {v1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_6

    const/16 v0, 0x30

    .line 178
    :cond_6
    :goto_2
    invoke-static {p1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;->makeMovementFlags(II)I

    move-result p1

    return p1
.end method

.method public isLongPressDragEnabled()Z
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {v0}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/MutationExtensionSpec;->getLongPressDragEnabled()Z

    move-result v0

    return v0
.end method

.method public onMove(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)Z
    .locals 2

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "viewHolder"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "target"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getFrozen()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return v0

    .line 215
    :cond_0
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 216
    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p2

    .line 217
    iget-object p3, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p3}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p3

    .line 301
    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    if-ne p2, v1, :cond_1

    .line 302
    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_5

    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 305
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-gez p2, :cond_2

    goto :goto_0

    :cond_2
    if-le v1, p2, :cond_5

    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p3

    invoke-interface {p3, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    .line 221
    iget-object p3, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p3}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/squareup/cycler/RecyclerData;->move(II)V

    .line 222
    iget-object p3, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-static {p3}, Lcom/squareup/cycler/MutationExtension;->access$getRecycler$p(Lcom/squareup/cycler/MutationExtension;)Lcom/squareup/cycler/Recycler;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p3

    invoke-virtual {p3}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p3

    if-nez p3, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {p3, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemMoved(II)V

    .line 224
    iget-object p3, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p3}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/cycler/MutationExtensionSpec;->getOnMove$lib_release()Lkotlin/jvm/functions/Function2;

    move-result-object p3

    if-eqz p3, :cond_4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, p1, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_4
    const/4 v0, 0x1

    :cond_5
    :goto_0
    if-eqz v0, :cond_6

    .line 233
    iput p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemCurrentIndex:I

    :cond_6
    return v0
.end method

.method public onSelectedChanged(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .line 243
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/ItemTouchHelper$Callback;->onSelectedChanged(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    const/4 v0, 0x2

    const/4 v1, -0x1

    if-eqz p2, :cond_2

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 246
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v1

    :cond_1
    iput v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemOriginalIndex:I

    .line 247
    iget p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemOriginalIndex:I

    iput p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemCurrentIndex:I

    goto :goto_0

    .line 250
    :cond_2
    iget p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->lastActionState:I

    if-ne p1, v0, :cond_4

    .line 251
    iget-object p1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cycler/MutationExtensionSpec;->getOnDragDrop$lib_release()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    if-eqz p1, :cond_3

    iget v0, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemOriginalIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemCurrentIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    .line 252
    :cond_3
    iput v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemOriginalIndex:I

    .line 253
    iput v1, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->draggedItemCurrentIndex:I

    .line 257
    :cond_4
    :goto_0
    iput p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->lastActionState:I

    return-void
.end method

.method public onSwiped(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 1

    const-string/jumbo p2, "viewHolder"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cycler/RecyclerData;->getFrozen()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 270
    :cond_0
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result p1

    .line 271
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/cycler/RecyclerData;->remove(I)V

    .line 272
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-static {p2}, Lcom/squareup/cycler/MutationExtension;->access$getRecycler$p(Lcom/squareup/cycler/MutationExtension;)Lcom/squareup/cycler/Recycler;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRemoved(I)V

    .line 274
    iget-object p2, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtension;->getSpec()Lcom/squareup/cycler/MutationExtensionSpec;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cycler/MutationExtensionSpec;->getOnSwipeToRemove$lib_release()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/squareup/cycler/MutationExtension$MyItemTouchCallback;->this$0:Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {v0}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_2
    return-void
.end method
