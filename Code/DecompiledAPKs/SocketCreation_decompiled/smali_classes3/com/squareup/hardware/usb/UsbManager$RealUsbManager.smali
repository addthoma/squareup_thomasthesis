.class public Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;
.super Ljava/lang/Object;
.source "UsbManager.java"

# interfaces
.implements Lcom/squareup/hardware/usb/UsbManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/hardware/usb/UsbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RealUsbManager"
.end annotation


# instance fields
.field usbManager:Landroid/hardware/usb/UsbManager;


# direct methods
.method public constructor <init>(Landroid/hardware/usb/UsbManager;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;->usbManager:Landroid/hardware/usb/UsbManager;

    return-void
.end method


# virtual methods
.method public getDeviceList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public hasPermission(Landroid/hardware/usb/UsbDevice;)Z
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result p1

    return p1
.end method

.method public openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1}, Landroid/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object p1

    return-object p1
.end method

.method public requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;->usbManager:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v0, p1, p2}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    return-void
.end method
