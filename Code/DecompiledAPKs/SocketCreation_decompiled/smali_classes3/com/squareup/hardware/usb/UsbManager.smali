.class public interface abstract Lcom/squareup/hardware/usb/UsbManager;
.super Ljava/lang/Object;
.source "UsbManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/hardware/usb/UsbManager$RealUsbManager;
    }
.end annotation


# static fields
.field public static final FAKE:Lcom/squareup/hardware/usb/UsbManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 87
    new-instance v0, Lcom/squareup/hardware/usb/UsbManager$1;

    invoke-direct {v0}, Lcom/squareup/hardware/usb/UsbManager$1;-><init>()V

    sput-object v0, Lcom/squareup/hardware/usb/UsbManager;->FAKE:Lcom/squareup/hardware/usb/UsbManager;

    return-void
.end method


# virtual methods
.method public abstract getDeviceList()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasPermission(Landroid/hardware/usb/UsbDevice;)Z
.end method

.method public abstract openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;
.end method

.method public abstract requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V
.end method
