.class public final Lcom/squareup/marin/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final icon_blue_dot_12:I = 0x7f080241

.field public static final icon_keypad_24:I = 0x7f080254

.field public static final icon_keypad_selected_24:I = 0x7f080255

.field public static final icon_library_24:I = 0x7f080256

.field public static final icon_library_selected_24:I = 0x7f080257

.field public static final marin_app_loading:I = 0x7f08028c

.field public static final marin_app_loading_light:I = 0x7f08028d

.field public static final marin_app_loading_light_large:I = 0x7f08028e

.field public static final marin_app_loading_logo:I = 0x7f08028f

.field public static final marin_app_loading_logo_large_light:I = 0x7f080290

.field public static final marin_app_loading_logo_light:I = 0x7f080291

.field public static final marin_badge_background:I = 0x7f080292

.field public static final marin_badge_background_fatal:I = 0x7f080293

.field public static final marin_badge_background_red:I = 0x7f080294

.field public static final marin_badge_fatal_border_dark_gray:I = 0x7f080295

.field public static final marin_badge_fatal_border_dark_gray_pressed:I = 0x7f080296

.field public static final marin_badge_fatal_border_ultra_light_gray:I = 0x7f080297

.field public static final marin_badge_fatal_border_ultra_light_gray_pressed:I = 0x7f080298

.field public static final marin_badge_rounded_blue_border_dark_gray_2dp:I = 0x7f080299

.field public static final marin_badge_rounded_blue_border_dark_gray_pressed_2dp:I = 0x7f08029a

.field public static final marin_badge_rounded_blue_border_ultra_light_gray_2dp:I = 0x7f08029b

.field public static final marin_badge_rounded_blue_border_ultra_light_gray_pressed_2dp:I = 0x7f08029c

.field public static final marin_badge_rounded_red_border_dark_gray_2dp:I = 0x7f08029d

.field public static final marin_badge_rounded_red_border_dark_gray_pressed_2dp:I = 0x7f08029e

.field public static final marin_badge_rounded_red_border_ultra_light_gray_2dp:I = 0x7f08029f

.field public static final marin_badge_rounded_red_border_ultra_light_gray_pressed_2dp:I = 0x7f0802a0

.field public static final marin_barely_dark_translucent_pressed:I = 0x7f0802a1

.field public static final marin_black_tranpsarent_fifty_border_white_1dp:I = 0x7f0802a2

.field public static final marin_black_tranpsarent_seventyfive_border_white_1dp:I = 0x7f0802a3

.field public static final marin_black_transparent_forty:I = 0x7f0802a4

.field public static final marin_black_transparent_thirty:I = 0x7f0802a5

.field public static final marin_blue:I = 0x7f0802a6

.field public static final marin_blue_border_right_dark_blue_1px:I = 0x7f0802a7

.field public static final marin_blue_disabled:I = 0x7f0802a8

.field public static final marin_blue_pressed:I = 0x7f0802a9

.field public static final marin_blue_pressed_border_right_dark_blue_1px:I = 0x7f0802aa

.field public static final marin_blue_selected:I = 0x7f0802ab

.field public static final marin_blue_selected_pressed:I = 0x7f0802ac

.field public static final marin_button_check_off:I = 0x7f0802ad

.field public static final marin_button_check_off_disabled:I = 0x7f0802ae

.field public static final marin_button_check_off_focused:I = 0x7f0802af

.field public static final marin_button_check_off_pressed:I = 0x7f0802b0

.field public static final marin_button_check_on:I = 0x7f0802b1

.field public static final marin_button_check_on_disabled:I = 0x7f0802b2

.field public static final marin_button_check_on_focused:I = 0x7f0802b3

.field public static final marin_button_check_on_pressed:I = 0x7f0802b4

.field public static final marin_button_radio_off:I = 0x7f0802b5

.field public static final marin_button_radio_off_disabled:I = 0x7f0802b6

.field public static final marin_button_radio_off_focused:I = 0x7f0802b7

.field public static final marin_button_radio_off_focused_red:I = 0x7f0802b8

.field public static final marin_button_radio_off_pressed:I = 0x7f0802b9

.field public static final marin_button_radio_on:I = 0x7f0802ba

.field public static final marin_button_radio_on_disabled:I = 0x7f0802bb

.field public static final marin_button_radio_on_focused:I = 0x7f0802bc

.field public static final marin_button_radio_on_focused_red:I = 0x7f0802bd

.field public static final marin_button_radio_on_pressed:I = 0x7f0802be

.field public static final marin_button_radio_on_pressed_red:I = 0x7f0802bf

.field public static final marin_button_radio_on_red:I = 0x7f0802c0

.field public static final marin_cart_quantity_tile:I = 0x7f0802c1

.field public static final marin_clear:I = 0x7f0802c2

.field public static final marin_clear_border_bottom_dark_gray_1px:I = 0x7f0802c3

.field public static final marin_clear_border_bottom_dim_translucent_1px:I = 0x7f0802c4

.field public static final marin_clear_border_bottom_light_gray_1dp:I = 0x7f0802c5

.field public static final marin_clear_border_bottom_light_gray_1px:I = 0x7f0802c6

.field public static final marin_clear_border_light_gray_1px:I = 0x7f0802c7

.field public static final marin_clear_border_top_bottom_light_gray_1px:I = 0x7f0802c8

.field public static final marin_clear_border_top_light_gray:I = 0x7f0802c9

.field public static final marin_clear_border_top_right_light_gray_1px:I = 0x7f0802ca

.field public static final marin_cursor_medium_gray:I = 0x7f0802cb

.field public static final marin_custom_tile:I = 0x7f0802cc

.field public static final marin_dark_gray:I = 0x7f0802cd

.field public static final marin_dark_gray_pressed:I = 0x7f0802ce

.field public static final marin_dark_gray_pressed_rounded:I = 0x7f0802cf

.field public static final marin_dark_gray_rounded:I = 0x7f0802d0

.field public static final marin_dark_translucent:I = 0x7f0802d1

.field public static final marin_dark_translucent_pressed:I = 0x7f0802d2

.field public static final marin_darker_gray:I = 0x7f0802d3

.field public static final marin_darker_gray_pressed:I = 0x7f0802d4

.field public static final marin_dialog_full_holo_light:I = 0x7f0802d5

.field public static final marin_dim_translucent_pressed:I = 0x7f0802d6

.field public static final marin_dim_translucent_pressed_border_bottom_clear_1px:I = 0x7f0802d7

.field public static final marin_dim_translucent_pressed_border_top_clear_1px:I = 0x7f0802d8

.field public static final marin_divider_charge_tickets:I = 0x7f0802d9

.field public static final marin_divider_horizontal_clear_20dp:I = 0x7f0802da

.field public static final marin_divider_horizontal_clear_buyer_gutter_half:I = 0x7f0802db

.field public static final marin_divider_horizontal_clear_small:I = 0x7f0802dc

.field public static final marin_divider_horizontal_dark_gray:I = 0x7f0802dd

.field public static final marin_divider_horizontal_darker_gray:I = 0x7f0802de

.field public static final marin_divider_horizontal_gutter_quarter:I = 0x7f0802df

.field public static final marin_divider_horizontal_light_gray:I = 0x7f0802e0

.field public static final marin_divider_horizontal_light_gray_padding_gutter_half:I = 0x7f0802e1

.field public static final marin_divider_horizontal_light_gray_pressed:I = 0x7f0802e2

.field public static final marin_divider_square_clear_medium:I = 0x7f0802e3

.field public static final marin_divider_vertical_blue_charge:I = 0x7f0802e4

.field public static final marin_divider_vertical_clear_20dp:I = 0x7f0802e5

.field public static final marin_divider_vertical_clear_gutter_half:I = 0x7f0802e6

.field public static final marin_divider_vertical_light_gray:I = 0x7f0802e7

.field public static final marin_divider_vertical_light_gray_28dp:I = 0x7f0802e8

.field public static final marin_divider_vertical_light_grey_charge:I = 0x7f0802e9

.field public static final marin_drawer_background:I = 0x7f0802ea

.field public static final marin_drawer_background_pressed:I = 0x7f0802eb

.field public static final marin_edit_text_disabled:I = 0x7f0802ec

.field public static final marin_edit_text_focused:I = 0x7f0802ed

.field public static final marin_edit_text_normal:I = 0x7f0802ee

.field public static final marin_education_gradient:I = 0x7f0802ef

.field public static final marin_green:I = 0x7f0802f0

.field public static final marin_green_border_bottom_1dp:I = 0x7f0802f1

.field public static final marin_green_pressed:I = 0x7f0802f2

.field public static final marin_green_pressed_border_bottom_clear_1px:I = 0x7f0802f3

.field public static final marin_image_border_light_gray_1px:I = 0x7f0802f4

.field public static final marin_image_border_light_gray_pressed_1px:I = 0x7f0802f5

.field public static final marin_light_gray:I = 0x7f0802f6

.field public static final marin_light_gray_border_top_1dp:I = 0x7f0802f7

.field public static final marin_light_gray_left_border_ultra_light_gray_pressed_1px:I = 0x7f0802f8

.field public static final marin_light_gray_pressed:I = 0x7f0802f9

.field public static final marin_medium_gray:I = 0x7f0802fa

.field public static final marin_medium_gray_border_bottom_medium_gray_pressed_1px:I = 0x7f0802fb

.field public static final marin_medium_gray_pressed:I = 0x7f0802fc

.field public static final marin_photo_placeholder:I = 0x7f0802fd

.field public static final marin_primary_button_disabled:I = 0x7f0802fe

.field public static final marin_progress_large_dark:I = 0x7f080300

.field public static final marin_progress_large_light:I = 0x7f080301

.field public static final marin_progress_medium_dark:I = 0x7f080302

.field public static final marin_progress_medium_light:I = 0x7f080303

.field public static final marin_progress_small_dark:I = 0x7f080306

.field public static final marin_progress_small_light:I = 0x7f080307

.field public static final marin_progress_spinner_large_dark:I = 0x7f080308

.field public static final marin_progress_spinner_large_light:I = 0x7f080309

.field public static final marin_progress_spinner_medium_dark:I = 0x7f08030a

.field public static final marin_progress_spinner_medium_light:I = 0x7f08030b

.field public static final marin_progress_spinner_small_dark:I = 0x7f08030c

.field public static final marin_progress_spinner_small_light:I = 0x7f08030d

.field public static final marin_progress_spinner_tiny:I = 0x7f08030e

.field public static final marin_progress_tiny:I = 0x7f08030f

.field public static final marin_red:I = 0x7f080310

.field public static final marin_screen_scrim:I = 0x7f080311

.field public static final marin_scrollbar_handle:I = 0x7f080312

.field public static final marin_section_header_gradient_background:I = 0x7f080313

.field public static final marin_selector_badge_dark_gray:I = 0x7f080314

.field public static final marin_selector_badge_fatal_border_dark_gray:I = 0x7f080315

.field public static final marin_selector_badge_fatal_border_ultra_light_gray:I = 0x7f080316

.field public static final marin_selector_badge_red_border_dark_gray:I = 0x7f080317

.field public static final marin_selector_badge_red_border_ultra_light_gray:I = 0x7f080318

.field public static final marin_selector_badge_ultra_light_gray:I = 0x7f080319

.field public static final marin_selector_barely_dark_translucent:I = 0x7f08031a

.field public static final marin_selector_black_transparent_fifty_border_white_transparent_twenty:I = 0x7f08031b

.field public static final marin_selector_black_transparent_fifty_pressed:I = 0x7f08031c

.field public static final marin_selector_black_transparent_thirty:I = 0x7f08031d

.field public static final marin_selector_blue:I = 0x7f08031e

.field public static final marin_selector_blue_pressed:I = 0x7f08031f

.field public static final marin_selector_blue_pressed_blue_selected:I = 0x7f080320

.field public static final marin_selector_blue_selected_light_gray_pressed:I = 0x7f080321

.field public static final marin_selector_blue_white_when_disabled:I = 0x7f080322

.field public static final marin_selector_border_bottom_1dp_ultra_light_gray:I = 0x7f080323

.field public static final marin_selector_button:I = 0x7f080324

.field public static final marin_selector_button_bluebackground_inline:I = 0x7f080325

.field public static final marin_selector_button_check:I = 0x7f080326

.field public static final marin_selector_button_edit_text:I = 0x7f080327

.field public static final marin_selector_button_inline:I = 0x7f080328

.field public static final marin_selector_button_radio:I = 0x7f080329

.field public static final marin_selector_button_radio_red:I = 0x7f08032a

.field public static final marin_selector_button_white_medium_gray_border:I = 0x7f08032b

.field public static final marin_selector_checked_dark_gray:I = 0x7f08032c

.field public static final marin_selector_clear_border_bottom_light_gray_1px:I = 0x7f08032d

.field public static final marin_selector_clear_border_left_light_gray_1px:I = 0x7f08032e

.field public static final marin_selector_clear_border_top_1px:I = 0x7f08032f

.field public static final marin_selector_clear_pressed_ultra_light_gray_pressed_checked_dark_gray:I = 0x7f080330

.field public static final marin_selector_clear_ultra_light_gray_pressed:I = 0x7f080331

.field public static final marin_selector_dark_gray:I = 0x7f080332

.field public static final marin_selector_dark_gray_pressed_darker_gray_selected:I = 0x7f080333

.field public static final marin_selector_dark_gray_rounded:I = 0x7f080334

.field public static final marin_selector_dark_gray_when_pressed:I = 0x7f080335

.field public static final marin_selector_dark_translucent:I = 0x7f080336

.field public static final marin_selector_darker_blue:I = 0x7f080337

.field public static final marin_selector_dim_translucent_pressed:I = 0x7f080338

.field public static final marin_selector_dim_translucent_pressed_border_bottom_clear_1px:I = 0x7f080339

.field public static final marin_selector_dim_translucent_pressed_border_top_clear_1px:I = 0x7f08033a

.field public static final marin_selector_drawer_pressed:I = 0x7f08033b

.field public static final marin_selector_edit_text:I = 0x7f08033c

.field public static final marin_selector_edit_text_border:I = 0x7f08033d

.field public static final marin_selector_edit_text_border_red:I = 0x7f08033e

.field public static final marin_selector_enable_setting_button:I = 0x7f08033f

.field public static final marin_selector_glyph_button_inline:I = 0x7f080340

.field public static final marin_selector_green_pressed:I = 0x7f080341

.field public static final marin_selector_green_pressed_border:I = 0x7f080342

.field public static final marin_selector_image_border_light_gray:I = 0x7f080343

.field public static final marin_selector_keypad:I = 0x7f080344

.field public static final marin_selector_list:I = 0x7f080345

.field public static final marin_selector_list_half_gutter:I = 0x7f080346

.field public static final marin_selector_list_top_bottom_border_light_gray_1px:I = 0x7f080347

.field public static final marin_selector_medium_gray:I = 0x7f080348

.field public static final marin_selector_red:I = 0x7f080349

.field public static final marin_selector_scrim:I = 0x7f08034a

.field public static final marin_selector_switch_inner:I = 0x7f08034b

.field public static final marin_selector_switch_track:I = 0x7f08034c

.field public static final marin_selector_ultra_light_gray:I = 0x7f08034d

.field public static final marin_selector_ultra_light_gray_border_bottom_light_gray_1px:I = 0x7f08034e

.field public static final marin_selector_ultra_light_gray_pressed:I = 0x7f08034f

.field public static final marin_selector_ultra_light_gray_pressed_border_bottom_clear_1dp:I = 0x7f080350

.field public static final marin_selector_ultra_light_gray_pressed_border_bottom_clear_2dp:I = 0x7f080351

.field public static final marin_selector_ultra_light_gray_when_pressed:I = 0x7f080352

.field public static final marin_selector_ultra_light_gray_when_pressed_border_bottom_clear_1dp:I = 0x7f080353

.field public static final marin_selector_ultra_light_gray_when_pressed_border_bottom_clear_2dp:I = 0x7f080354

.field public static final marin_selector_white:I = 0x7f080355

.field public static final marin_selector_white_border_left_ultra_light_gray_1px:I = 0x7f080356

.field public static final marin_selector_white_border_light_gray_1px_blue_selected:I = 0x7f080357

.field public static final marin_selector_white_disabled_clear_activated_blue:I = 0x7f080358

.field public static final marin_selector_white_ultra_light_gray_pressed:I = 0x7f080359

.field public static final marin_shape_text_box:I = 0x7f08035a

.field public static final marin_shape_text_box_blue:I = 0x7f08035b

.field public static final marin_shape_text_box_dark:I = 0x7f08035c

.field public static final marin_shape_text_box_red:I = 0x7f08035d

.field public static final marin_switch_bg:I = 0x7f08035e

.field public static final marin_switch_bg_disabled:I = 0x7f08035f

.field public static final marin_switch_bg_focused:I = 0x7f080360

.field public static final marin_switch_thumb:I = 0x7f080361

.field public static final marin_switch_thumb_activated:I = 0x7f080362

.field public static final marin_switch_thumb_disabled:I = 0x7f080363

.field public static final marin_switch_thumb_pressed:I = 0x7f080364

.field public static final marin_tool_tip_left_up_arrow:I = 0x7f080366

.field public static final marin_transition_clear_to_ultra_light_gray:I = 0x7f080368

.field public static final marin_ultra_light_gray:I = 0x7f080369

.field public static final marin_ultra_light_gray_border_blue_1dp:I = 0x7f08036a

.field public static final marin_ultra_light_gray_border_blue_1px:I = 0x7f08036b

.field public static final marin_ultra_light_gray_border_blue_1px_no_padding:I = 0x7f08036c

.field public static final marin_ultra_light_gray_border_bottom_clear_1dp:I = 0x7f08036d

.field public static final marin_ultra_light_gray_border_bottom_clear_2dp:I = 0x7f08036e

.field public static final marin_ultra_light_gray_border_bottom_light_gray_1dp:I = 0x7f08036f

.field public static final marin_ultra_light_gray_border_bottom_light_gray_1px:I = 0x7f080370

.field public static final marin_ultra_light_gray_border_left_light_gray_1px:I = 0x7f080371

.field public static final marin_ultra_light_gray_border_light_gray_1px:I = 0x7f080372

.field public static final marin_ultra_light_gray_border_light_gray_1px_no_padding:I = 0x7f080373

.field public static final marin_ultra_light_gray_border_top_blue_1px:I = 0x7f080374

.field public static final marin_ultra_light_gray_border_top_bottom_light_gray_1px:I = 0x7f080375

.field public static final marin_ultra_light_gray_border_top_light_gray_1px:I = 0x7f080376

.field public static final marin_ultra_light_gray_border_top_right_light_gray_1px:I = 0x7f080377

.field public static final marin_ultra_light_gray_pressed:I = 0x7f080378

.field public static final marin_ultra_light_gray_pressed_border_bottom_clear_1dp:I = 0x7f080379

.field public static final marin_ultra_light_gray_pressed_border_bottom_clear_2dp:I = 0x7f08037a

.field public static final marin_ultra_light_gray_pressed_border_bottom_light_gray_1px:I = 0x7f08037b

.field public static final marin_ultra_light_gray_pressed_border_light_gray_1px:I = 0x7f08037c

.field public static final marin_ultra_light_gray_rounded:I = 0x7f08037d

.field public static final marin_white:I = 0x7f08037e

.field public static final marin_white_border_blue_1dp:I = 0x7f08037f

.field public static final marin_white_border_bottom_light_gray_1px:I = 0x7f080380

.field public static final marin_white_border_bottom_medium_gray_1px:I = 0x7f080381

.field public static final marin_white_border_left_light_gray_1px:I = 0x7f080382

.field public static final marin_white_border_light_gray:I = 0x7f080383

.field public static final marin_white_border_light_gray_2dp:I = 0x7f080384

.field public static final marin_white_border_light_gray_pressed:I = 0x7f080385

.field public static final marin_white_border_medium_gray_1px:I = 0x7f080386

.field public static final marin_white_border_top_light_gray_1px:I = 0x7f080387

.field public static final marin_white_to_clear_gradient:I = 0x7f080388

.field public static final window_background_logo:I = 0x7f0804fa


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
