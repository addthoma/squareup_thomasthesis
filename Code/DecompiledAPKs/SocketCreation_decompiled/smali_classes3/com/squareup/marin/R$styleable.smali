.class public final Lcom/squareup/marin/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final BorderedLinearLayout:[I

.field public static final BorderedLinearLayout_borders:I = 0x0

.field public static final BorderedLinearLayout_marinBorderWidth:I = 0x1

.field public static final DetailConfirmationView:[I

.field public static final DetailConfirmationView_android_title:I = 0x0

.field public static final DetailConfirmationView_buttonText:I = 0x1

.field public static final DetailConfirmationView_equalizeLines:I = 0x2

.field public static final DetailConfirmationView_glyph:I = 0x3

.field public static final DetailConfirmationView_helperText:I = 0x4

.field public static final DetailConfirmationView_message:I = 0x5

.field public static final MarinActionBarView:[I

.field public static final MarinActionBarView_marinActionBarPrimaryButtonStyle:I = 0x0

.field public static final MarinActionBarView_marinActionBarSecondaryButtonStyle:I = 0x1

.field public static final MarinActionBarView_marinActionBarStyle:I = 0x2

.field public static final MarinActionBarView_marinActionBarTheme:I = 0x3

.field public static final MarinActionBarView_marinActionBarUpButtonStyle:I = 0x4

.field public static final MarinAppletTabletLayout:[I

.field public static final MarinAppletTabletLayout_dividerOffsetFromTop:I = 0x0

.field public static final MarinAppletTabletLayout_dividerWidth:I = 0x1

.field public static final MarinAppletTabletLayout_ratio:I = 0x2

.field public static final MarinGlyphMessage:[I

.field public static final MarinGlyphMessage_android_title:I = 0x0

.field public static final MarinGlyphMessage_button_text:I = 0x1

.field public static final MarinGlyphMessage_glyph:I = 0x2

.field public static final MarinGlyphMessage_glyphSaveEnabled:I = 0x3

.field public static final MarinGlyphMessage_glyphVisible:I = 0x4

.field public static final MarinGlyphMessage_marinGlyphMessageTitleColor:I = 0x5

.field public static final MarinGlyphMessage_message:I = 0x6

.field public static final MarinGlyphMessage_messageColor:I = 0x7

.field public static final MarinGlyphMessage_sq_glyphColor:I = 0x8

.field public static final MarinGlyphTextView:[I

.field public static final MarinGlyphTextView_android_textColor:I = 0x0

.field public static final MarinGlyphTextView_chevronColor:I = 0x1

.field public static final MarinGlyphTextView_chevronVisibility:I = 0x2

.field public static final MarinGlyphTextView_glyph:I = 0x3

.field public static final MarinGlyphTextView_glyphFontSizeOverride:I = 0x4

.field public static final MarinGlyphTextView_glyphPosition:I = 0x5

.field public static final MarinGlyphTextView_glyphShadowColor:I = 0x6

.field public static final MarinGlyphTextView_glyphShadowDx:I = 0x7

.field public static final MarinGlyphTextView_glyphShadowDy:I = 0x8

.field public static final MarinGlyphTextView_glyphShadowRadius:I = 0x9

.field public static final MarinPageIndicator:[I

.field public static final MarinPageIndicator_indicatorDiameter:I = 0x0

.field public static final MarinPageIndicator_indicatorHighlightColor:I = 0x1

.field public static final MarinPageIndicator_indicatorMatteColor:I = 0x2

.field public static final MarinPageIndicator_indicatorSpacing:I = 0x3

.field public static final MarinSpinnerGlyph:[I

.field public static final MarinSpinnerGlyph_failureGlyph:I = 0x0

.field public static final MarinSpinnerGlyph_failureGlyphColor:I = 0x1

.field public static final MarinSpinnerGlyph_glyph:I = 0x2

.field public static final MarinSpinnerGlyph_glyphColor:I = 0x3

.field public static final MarinSpinnerGlyph_marinSpinnerGlyphStyle:I = 0x4

.field public static final MarinSpinnerGlyph_transitionDuration:I = 0x5

.field public static final MarinVerticalCaretView:[I

.field public static final MarinVerticalCaretView_pointingDown:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 965
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/marin/R$styleable;->BorderedLinearLayout:[I

    const/4 v0, 0x6

    new-array v1, v0, [I

    .line 968
    fill-array-data v1, :array_1

    sput-object v1, Lcom/squareup/marin/R$styleable;->DetailConfirmationView:[I

    const/4 v1, 0x5

    new-array v1, v1, [I

    .line 975
    fill-array-data v1, :array_2

    sput-object v1, Lcom/squareup/marin/R$styleable;->MarinActionBarView:[I

    const/4 v1, 0x3

    new-array v1, v1, [I

    .line 981
    fill-array-data v1, :array_3

    sput-object v1, Lcom/squareup/marin/R$styleable;->MarinAppletTabletLayout:[I

    const/16 v1, 0x9

    new-array v1, v1, [I

    .line 985
    fill-array-data v1, :array_4

    sput-object v1, Lcom/squareup/marin/R$styleable;->MarinGlyphMessage:[I

    const/16 v1, 0xa

    new-array v1, v1, [I

    .line 995
    fill-array-data v1, :array_5

    sput-object v1, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView:[I

    const/4 v1, 0x4

    new-array v1, v1, [I

    .line 1006
    fill-array-data v1, :array_6

    sput-object v1, Lcom/squareup/marin/R$styleable;->MarinPageIndicator:[I

    new-array v0, v0, [I

    .line 1011
    fill-array-data v0, :array_7

    sput-object v0, Lcom/squareup/marin/R$styleable;->MarinSpinnerGlyph:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f040315

    aput v2, v0, v1

    .line 1018
    sput-object v0, Lcom/squareup/marin/R$styleable;->MarinVerticalCaretView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040068
        0x7f04029a
    .end array-data

    :array_1
    .array-data 4
        0x10101e1
        0x7f04008a
        0x7f04015f
        0x7f0401a0
        0x7f0401b3
        0x7f0402bf
    .end array-data

    :array_2
    .array-data 4
        0x7f040295
        0x7f040296
        0x7f040297
        0x7f040298
        0x7f040299
    .end array-data

    :array_3
    .array-data 4
        0x7f04012e
        0x7f040131
        0x7f040329
    .end array-data

    :array_4
    .array-data 4
        0x10101e1
        0x7f04008e
        0x7f0401a0
        0x7f0401a4
        0x7f0401ab
        0x7f04029e
        0x7f0402bf
        0x7f0402c0
        0x7f0403c4
    .end array-data

    :array_5
    .array-data 4
        0x1010098
        0x7f0400a8
        0x7f0400a9
        0x7f0401a0
        0x7f0401a2
        0x7f0401a3
        0x7f0401a5
        0x7f0401a6
        0x7f0401a7
        0x7f0401a8
    .end array-data

    :array_6
    .array-data 4
        0x7f04020d
        0x7f04020f
        0x7f040210
        0x7f040211
    .end array-data

    :array_7
    .array-data 4
        0x7f04017c
        0x7f04017d
        0x7f0401a0
        0x7f0401a1
        0x7f0402a1
        0x7f040479
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
