.class public final Lcom/squareup/marin/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BOTTOM:I = 0x7f0a0014

.field public static final LEFT:I = 0x7f0a0086

.field public static final RIGHT:I = 0x7f0a00bb

.field public static final TOP:I = 0x7f0a00e0

.field public static final actionbar_custom_view_container:I = 0x7f0a014d

.field public static final badge_view:I = 0x7f0a01f0

.field public static final buyer_action_bar:I = 0x7f0a027c

.field public static final buyer_floating_content:I = 0x7f0a028a

.field public static final detail_confirmation_button:I = 0x7f0a0593

.field public static final detail_confirmation_glyph:I = 0x7f0a0594

.field public static final detail_confirmation_helper_text:I = 0x7f0a0595

.field public static final detail_confirmation_message:I = 0x7f0a0596

.field public static final detail_confirmation_title:I = 0x7f0a0597

.field public static final fourth_button:I = 0x7f0a0770

.field public static final glyph_message_button:I = 0x7f0a07b2

.field public static final glyph_message_glyph:I = 0x7f0a07b3

.field public static final glyph_message_message:I = 0x7f0a07b4

.field public static final glyph_message_title:I = 0x7f0a07b5

.field public static final gone:I = 0x7f0a07b8

.field public static final header:I = 0x7f0a07c8

.field public static final marin_action_bar_background:I = 0x7f0a09aa

.field public static final primary_button:I = 0x7f0a0c5b

.field public static final secondary_button:I = 0x7f0a0e2e

.field public static final sticky_bar:I = 0x7f0a0f3a

.field public static final subtitle:I = 0x7f0a0f49

.field public static final third_button:I = 0x7f0a0faa

.field public static final title:I = 0x7f0a103f

.field public static final up_button:I = 0x7f0a10c0

.field public static final up_button_glyph:I = 0x7f0a10c1

.field public static final up_text:I = 0x7f0a10c2

.field public static final visible:I = 0x7f0a1107


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
