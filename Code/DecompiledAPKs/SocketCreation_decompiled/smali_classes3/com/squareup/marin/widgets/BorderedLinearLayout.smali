.class public Lcom/squareup/marin/widgets/BorderedLinearLayout;
.super Landroid/widget/LinearLayout;
.source "BorderedLinearLayout.java"


# instance fields
.field private final borderPainter:Lcom/squareup/marin/widgets/BorderPainter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    sget-object v0, Lcom/squareup/marin/R$styleable;->BorderedLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 31
    sget p2, Lcom/squareup/marin/R$styleable;->BorderedLinearLayout_marinBorderWidth:I

    sget v0, Lcom/squareup/marin/R$dimen;->marin_divider_width_1dp:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    .line 33
    new-instance v0, Lcom/squareup/marin/widgets/BorderPainter;

    invoke-direct {v0, p0, p2}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 34
    iget-object p2, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    sget v0, Lcom/squareup/marin/R$styleable;->BorderedLinearLayout_borders:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 35
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public addBorder(I)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method public clearBorders()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->clearBorders()V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 39
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setBorderColor(I)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setColor(I)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method protected setBorderWidth(I)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setBorderWidth(I)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method protected setBordersToMultiply()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->setMultiply()V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method public setHorizontalBorders(ZZ)V
    .locals 0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->clearBorders()V

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    .line 51
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addBorder(I)V

    :cond_0
    if-eqz p2, :cond_1

    const/16 p1, 0x8

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addBorder(I)V

    :cond_1
    return-void
.end method

.method protected setHorizontalBordersRightInset(I)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setRightInset(I)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method

.method protected setHorizontalInsets(I)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/marin/widgets/BorderedLinearLayout;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->setHorizontalInsets(I)V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->invalidate()V

    return-void
.end method
