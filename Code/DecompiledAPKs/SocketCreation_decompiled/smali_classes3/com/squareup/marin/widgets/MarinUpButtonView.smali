.class public Lcom/squareup/marin/widgets/MarinUpButtonView;
.super Lcom/squareup/marin/widgets/MarinGlyphTextView;
.source "MarinUpButtonView.java"


# instance fields
.field private availableWidth:I

.field private includeFontPadding:Z

.field private layout:Landroid/text/Layout;

.field private maxTextSize:F

.field private minTextSize:F

.field private scalable:Z

.field private textPaint:Landroid/text/TextPaint;

.field private yOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010084

    .line 49
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinUpButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    .line 57
    sget-object v0, Lcom/squareup/widgets/R$styleable;->ScalingTextView:[I

    const/4 v1, 0x0

    .line 58
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextSize()F

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->maxTextSize:F

    .line 62
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    iget p3, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->maxTextSize:F

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 63
    iget-object p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 65
    sget p2, Lcom/squareup/widgets/R$styleable;->ScalingTextView_minTextSize:I

    const/high16 p3, 0x41800000    # 16.0f

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->minTextSize:F

    .line 66
    sget p2, Lcom/squareup/widgets/R$styleable;->ScalingTextView_android_includeFontPadding:I

    const/4 p3, 0x1

    .line 67
    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->includeFontPadding:Z

    .line 70
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object p2

    sget p3, Lcom/squareup/widgets/R$styleable;->ScalingTextView_android_shadowRadius:I

    const/4 v0, 0x0

    .line 71
    invoke-virtual {p1, p3, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p3

    sget v2, Lcom/squareup/widgets/R$styleable;->ScalingTextView_android_shadowDx:I

    .line 72
    invoke-virtual {p1, v2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    sget v3, Lcom/squareup/widgets/R$styleable;->ScalingTextView_android_shadowDy:I

    .line 73
    invoke-virtual {p1, v3, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    sget v3, Lcom/squareup/widgets/R$styleable;->ScalingTextView_android_shadowColor:I

    .line 74
    invoke-virtual {p1, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 70
    invoke-virtual {p2, p3, v2, v0, v1}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 77
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private autoFit(II)I
    .locals 8

    .line 161
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingLeft()I

    move-result v0

    .line 162
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingRight()I

    move-result v1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getCompoundDrawablePadding()I

    move-result v2

    sub-int/2addr p2, v0

    sub-int/2addr p2, v1

    .line 164
    iput p2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->availableWidth:I

    .line 168
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getDrawableLeftWidth()I

    move-result p2

    const/4 v3, -0x1

    if-eq p2, v3, :cond_0

    add-int/2addr p2, v2

    .line 171
    iget v2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->availableWidth:I

    sub-int/2addr v2, p2

    iput v2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->availableWidth:I

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 174
    :goto_0
    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->availableWidth:I

    iget v5, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->minTextSize:F

    iget v6, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->maxTextSize:F

    invoke-static {v2, v3, v4, v5, v6}, Lcom/squareup/text/Fonts;->autoFitText(Landroid/text/TextPaint;Ljava/lang/String;IFF)V

    .line 178
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v3

    iget v4, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->availableWidth:I

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 180
    invoke-direct {p0, v2}, Lcom/squareup/marin/widgets/MarinUpButtonView;->createLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->layout:Landroid/text/Layout;

    .line 182
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingTop()I

    move-result v2

    .line 183
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingBottom()I

    move-result v3

    sub-int v4, p1, v2

    sub-int/2addr v4, v3

    .line 185
    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->layout:Landroid/text/Layout;

    invoke-virtual {v5}, Landroid/text/Layout;->getHeight()I

    move-result v5

    .line 187
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getGravity()I

    move-result v6

    and-int/lit8 v6, v6, 0x70

    const/4 v7, 0x3

    if-eq v6, v7, :cond_2

    const/16 p1, 0x10

    if-eq v6, p1, :cond_1

    int-to-float p1, v2

    .line 197
    iput p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->yOffset:F

    goto :goto_1

    :cond_1
    sub-int/2addr v4, v5

    shr-int/lit8 p1, v4, 0x1

    add-int/2addr v2, p1

    int-to-float p1, v2

    .line 190
    iput p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->yOffset:F

    goto :goto_1

    :cond_2
    sub-int/2addr p1, v3

    sub-int/2addr p1, v5

    int-to-float p1, p1

    .line 193
    iput p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->yOffset:F

    .line 200
    :goto_1
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->layout:Landroid/text/Layout;

    invoke-virtual {p1}, Landroid/text/Layout;->getWidth()I

    move-result p1

    add-int/2addr p1, v0

    add-int/2addr p1, v1

    add-int/2addr p1, p2

    return p1
.end method

.method private createLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;
    .locals 9

    .line 207
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x7

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 216
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    :goto_0
    move-object v4, v0

    goto :goto_1

    .line 213
    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 210
    :cond_1
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_0

    .line 219
    :goto_1
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/BoringLayout;->isBoring(Ljava/lang/CharSequence;Landroid/text/TextPaint;)Landroid/text/BoringLayout$Metrics;

    move-result-object v0

    if-nez v0, :cond_2

    .line 223
    new-instance v0, Landroid/text/BoringLayout$Metrics;

    invoke-direct {v0}, Landroid/text/BoringLayout$Metrics;-><init>()V

    .line 226
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    :cond_2
    move-object v7, v0

    .line 229
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 230
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v3, v0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    iget-boolean v8, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->includeFontPadding:Z

    move-object v1, p1

    .line 229
    invoke-static/range {v1 .. v8}, Landroid/text/BoringLayout;->make(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFLandroid/text/BoringLayout$Metrics;Z)Landroid/text/BoringLayout;

    move-result-object p1

    return-object p1
.end method

.method private getDrawableLeftWidth()I
    .locals 2

    .line 235
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method private getTextPaint()Landroid/text/TextPaint;
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 268
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    .line 269
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    invoke-static {v0}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->textPaint:Landroid/text/TextPaint;

    return-object v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 125
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->scalable:Z

    if-nez v0, :cond_0

    .line 126
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getCurrentTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 132
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 134
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v3, 0x0

    .line 138
    aget-object v1, v1, v3

    if-eqz v1, :cond_1

    .line 140
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 143
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getHeight()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 144
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 145
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 146
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getCompoundDrawablePadding()I

    move-result v3

    add-int/2addr v1, v3

    int-to-float v1, v1

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    :cond_1
    iget v1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->yOffset:F

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 151
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->layout:Landroid/text/Layout;

    invoke-virtual {v1, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 153
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 90
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->scalable:Z

    if-nez v0, :cond_0

    .line 91
    invoke-super {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->onMeasure(II)V

    return-void

    .line 95
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 97
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 98
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    if-ne v0, v4, :cond_1

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_0
    if-ne v2, v4, :cond_2

    goto :goto_1

    .line 112
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->createLayout(Ljava/lang/CharSequence;)Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getHeight()I

    move-result v0

    .line 113
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getPaddingBottom()I

    move-result v0

    add-int/2addr v2, v0

    .line 114
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 118
    :goto_1
    invoke-direct {p0, v3, v1}, Lcom/squareup/marin/widgets/MarinUpButtonView;->autoFit(II)I

    move-result v0

    .line 120
    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinUpButtonView;->resolveSize(II)I

    move-result p1

    .line 121
    invoke-static {v3, p2}, Lcom/squareup/marin/widgets/MarinUpButtonView;->resolveSize(II)I

    move-result p2

    .line 120
    invoke-virtual {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinUpButtonView;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 276
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 279
    iget-boolean p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->scalable:Z

    if-eqz p1, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->requestLayout()V

    .line 281
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 0

    .line 242
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGravity(I)V

    .line 244
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->requestLayout()V

    .line 245
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->invalidate()V

    return-void
.end method

.method public setScalable(Z)V
    .locals 0

    .line 286
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->scalable:Z

    return-void
.end method

.method public setShadowLayer(FFFI)V
    .locals 1

    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setShadowLayer(FFFI)V

    .line 84
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->invalidate()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 1

    .line 258
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTextSize(F)V

    .line 259
    iput p1, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->maxTextSize:F

    .line 260
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object p1

    iget v0, p0, Lcom/squareup/marin/widgets/MarinUpButtonView;->maxTextSize:F

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 262
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->requestLayout()V

    .line 263
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->invalidate()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    .line 250
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 252
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 253
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->requestLayout()V

    .line 254
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinUpButtonView;->invalidate()V

    return-void
.end method
