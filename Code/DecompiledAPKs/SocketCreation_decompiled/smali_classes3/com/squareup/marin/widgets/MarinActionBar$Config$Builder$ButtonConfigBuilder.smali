.class Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ButtonConfigBuilder"
.end annotation


# instance fields
.field private backgroundColor:I

.field private command:Ljava/lang/Runnable;

.field private enabled:Z

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private text:Ljava/lang/CharSequence;

.field private textColor:I

.field private tooltip:Ljava/lang/CharSequence;

.field private visible:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 635
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 631
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->enabled:Z

    const/4 v0, 0x0

    .line 632
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->visible:Z

    return-void
.end method

.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;)V
    .locals 1

    .line 638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 631
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->enabled:Z

    const/4 v0, 0x0

    .line 632
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->visible:Z

    .line 639
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 640
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->text:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->text:Ljava/lang/CharSequence;

    .line 641
    iget v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->textColor:I

    iput v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->textColor:I

    .line 642
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->tooltip:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    .line 643
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->command:Ljava/lang/Runnable;

    .line 644
    iget-boolean v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->enabled:Z

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->enabled:Z

    .line 645
    iget-boolean v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->visible:Z

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->visible:Z

    .line 646
    iget p1, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->backgroundColor:I

    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->backgroundColor:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 625
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method static synthetic access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p1
.end method

.method static synthetic access$100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/CharSequence;
    .locals 0

    .line 625
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->text:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->text:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)I
    .locals 0

    .line 625
    iget p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->textColor:I

    return p0
.end method

.method static synthetic access$202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;I)I
    .locals 0

    .line 625
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->textColor:I

    return p1
.end method

.method static synthetic access$300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/CharSequence;
    .locals 0

    .line 625
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/Runnable;
    .locals 0

    .line 625
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->command:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 625
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->command:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Z
    .locals 0

    .line 625
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->enabled:Z

    return p0
.end method

.method static synthetic access$502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z
    .locals 0

    .line 625
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->enabled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Z
    .locals 0

    .line 625
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->visible:Z

    return p0
.end method

.method static synthetic access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z
    .locals 0

    .line 625
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->visible:Z

    return p1
.end method

.method static synthetic access$700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)I
    .locals 0

    .line 625
    iget p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->backgroundColor:I

    return p0
.end method

.method static synthetic access$702(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;I)I
    .locals 0

    .line 625
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->backgroundColor:I

    return p1
.end method
