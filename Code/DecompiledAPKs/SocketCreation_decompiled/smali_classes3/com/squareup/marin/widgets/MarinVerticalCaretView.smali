.class public Lcom/squareup/marin/widgets/MarinVerticalCaretView;
.super Landroidx/appcompat/widget/AppCompatImageView;
.source "MarinVerticalCaretView.java"


# instance fields
.field private final caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 17
    invoke-direct {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    sget-object v0, Lcom/squareup/marin/R$styleable;->MarinVerticalCaretView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 19
    sget p2, Lcom/squareup/marin/R$styleable;->MarinVerticalCaretView_pointingDown:I

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    .line 20
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 22
    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 23
    new-instance p1, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;-><init>(ZLandroid/content/res/Resources;)V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    .line 24
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 25
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->isEnabled()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public animateArrowDown()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animateArrowDown()V

    return-void
.end method

.method public animateArrowUp()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animateArrowUp()V

    return-void
.end method

.method public isPointingDown()Z
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->isPointingDown()Z

    move-result v0

    return v0
.end method

.method public setArrowDown()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrowDown()V

    return-void
.end method

.method public setArrowUp()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrowUp()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->isEnabled()Z

    move-result v0

    if-ne p1, v0, :cond_0

    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setEnabled(Z)V

    .line 63
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setEnabled(Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;->caretDrawable:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->toggle()V

    return-void
.end method
