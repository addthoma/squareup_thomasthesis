.class public interface abstract Lcom/squareup/development/drawer/DevelopmentDrawerSection;
.super Ljava/lang/Object;
.source "DevelopmentDrawerSection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;,
        Lcom/squareup/development/drawer/DevelopmentDrawerSection$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0001\u000eR\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
        "",
        "coordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "getCoordinator",
        "()Lcom/squareup/coordinators/Coordinator;",
        "layoutContentId",
        "",
        "getLayoutContentId",
        "()I",
        "rank",
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;",
        "getRank",
        "()Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;",
        "Rank",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getCoordinator()Lcom/squareup/coordinators/Coordinator;
.end method

.method public abstract getLayoutContentId()I
.end method

.method public abstract getRank()Lcom/squareup/development/drawer/DevelopmentDrawerSection$Rank;
.end method
