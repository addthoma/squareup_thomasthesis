.class public abstract Lcom/squareup/features/invoices/widgets/V1WidgetsModule;
.super Ljava/lang/Object;
.source "InvoiceWidgetsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/features/invoices/widgets/InvoiceWidgetsModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/V1WidgetsModule;",
        "",
        "()V",
        "editPaymentRequestRowDataFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
        "factory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;",
        "editPaymentRequestRowDataFactory$impl_wiring_release",
        "invoiceSectionRenderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "renderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
        "invoiceSectionRenderer$impl_wiring_release",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract editPaymentRequestRowDataFactory$impl_wiring_release(Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract invoiceSectionRenderer$impl_wiring_release(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;)Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
