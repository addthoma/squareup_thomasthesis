.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactoryKt;
.super Ljava/lang/Object;
.source "InvoiceSectionContainerViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceSectionContainerViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceSectionContainerViewFactory.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactoryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,75:1\n1550#2,3:76\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceSectionContainerViewFactory.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactoryKt\n*L\n74#1,3:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u001a\u001f\u0010\u0000\u001a\u00020\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0003*\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0086\u0008\u00a8\u0006\u0006"
    }
    d2 = {
        "contains",
        "",
        "T",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic contains(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$contains"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    check-cast p0, Ljava/lang/Iterable;

    .line 76
    instance-of v0, p0, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 77
    :cond_0
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 74
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v5, 0x3

    const-string v6, "T"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    instance-of v4, v4, Lcom/squareup/features/invoices/widgets/SectionElement;

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_5
    :goto_2
    return v2
.end method
