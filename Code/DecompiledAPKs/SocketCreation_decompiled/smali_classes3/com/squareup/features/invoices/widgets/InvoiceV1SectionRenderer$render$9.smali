.class public final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "InvoiceV1SectionRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onEvent:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_render:Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;->$this_render:Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;->$onEvent:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 316
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;->$onEvent:Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$EditTextChanged;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$9;->$this_render:Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/widgets/SectionElement$EditTextData;->getEvent()Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$EditTextChanged;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
