.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;
.super Ljava/lang/Object;
.source "EditPaymentRequestsSectionDataFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestsSectionDataFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestsSectionDataFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,55:1\n1360#2:56\n1429#2,3:57\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestsSectionDataFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory\n*L\n36#1:56\n36#1,3:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ&\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00042\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V",
        "create",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;",
        "invoiceAmount",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceSendDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->clock:Lcom/squareup/util/Clock;

    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;"
        }
    .end annotation

    const-string v0, "invoiceAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequests"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceSendDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 35
    sget-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v1, p2}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v1

    .line 36
    invoke-static {p2, p1}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 56
    new-instance p2, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {p2, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 57
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 58
    check-cast v2, Lkotlin/Pair;

    invoke-virtual {v2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-virtual {v2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 37
    new-instance v10, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;

    .line 38
    iget-object v5, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->name:Ljava/lang/String;

    .line 39
    iget-object v4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    const-string v2, "moneyFormatter.format(amount)"

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v2, Lcom/squareup/invoices/DateType$DaysUntil;

    iget-object v4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->clock:Lcom/squareup/util/Clock;

    invoke-direct {v2, p3, v4}, Lcom/squareup/invoices/DateType$DaysUntil;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)V

    check-cast v2, Lcom/squareup/invoices/DateType;

    .line 42
    invoke-static {v3, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    .line 43
    iget-object v7, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->dateFormat:Ljava/text/DateFormat;

    .line 44
    iget-object v8, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;->res:Lcom/squareup/util/Res;

    .line 40
    invoke-static {v2, v4, v7, v8}, Lcom/squareup/invoices/PaymentRequestsKt;->formatDueDate(Lcom/squareup/invoices/DateType;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 46
    iget-object v8, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const-string v2, "request.amount_type"

    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v9, v3, Lcom/squareup/protos/client/invoice/PaymentRequest;->percentage_amount:Ljava/lang/Long;

    move-object v4, v10

    .line 37
    invoke-direct/range {v4 .. v9}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Ljava/lang/Long;)V

    .line 48
    invoke-interface {p2, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    :cond_1
    check-cast p2, Ljava/util/List;

    xor-int/lit8 p1, v0, 0x1

    xor-int/lit8 p3, v0, 0x1

    .line 34
    new-instance v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    invoke-direct {v0, v1, p2, p1, p3}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;-><init>(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)V

    return-object v0
.end method
