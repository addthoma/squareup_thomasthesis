.class public final Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;
.super Ljava/lang/Object;
.source "InvoiceAttachmentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceAttachmentState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceAttachmentState.kt\ncom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,287:1\n32#2,12:288\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceAttachmentState.kt\ncom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion\n*L\n135#1,12:288\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tJ\u000c\u0010\n\u001a\u00020\u0004*\u00020\u000bH\u0002J\u000c\u0010\n\u001a\u00020\u0004*\u00020\u000cH\u0002J\u000c\u0010\n\u001a\u00020\u0004*\u00020\rH\u0002\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;",
        "",
        "()V",
        "launchState",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "snapShot",
        "Lcom/squareup/workflow/Snapshot;",
        "startState",
        "startAttachmentInfo",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;",
        "toStartState",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;-><init>()V

    return-void
.end method

.method private final toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
    .locals 7

    .line 181
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->isSupportedImageMimeType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    .line 182
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getAttachmentToken()Ljava/lang/String;

    move-result-object v2

    .line 184
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 185
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getExtension()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getMimeType()Ljava/lang/String;

    move-result-object v6

    .line 184
    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;

    .line 188
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object p1

    .line 187
    invoke-direct {v4, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$ReadOnly;-><init>(Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;

    .line 181
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    goto :goto_0

    .line 192
    :cond_0
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    .line 193
    sget-object v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Remote;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Remote;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 194
    new-instance v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 195
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getExtension()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getMimeType()Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;

    .line 198
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getUploadedAt()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getAttachmentToken()Ljava/lang/String;

    move-result-object v5

    .line 197
    invoke-direct {v3, v4, v5}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$ReadOnly;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    .line 200
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object p1

    .line 192
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    :goto_0
    return-object v0
.end method

.method private final toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
    .locals 6

    .line 152
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getMimeType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/SupportedFileFormatsKt;->isSupportedImageMimeType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getAttachmentToken()Ljava/lang/String;

    move-result-object v2

    .line 155
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getMimeType()Ljava/lang/String;

    move-result-object p1

    .line 155
    invoke-direct {v3, v4, v5, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-object p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType$Update;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;

    .line 152
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/DownloadingImageType;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    goto :goto_0

    .line 161
    :cond_0
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    .line 162
    sget-object v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Remote;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Remote;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    .line 163
    new-instance v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    .line 164
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getExtensionToPreserve()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getMimeType()Ljava/lang/String;

    move-result-object v5

    .line 163
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    new-instance v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;

    .line 167
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getAttachmentToken()Ljava/lang/String;

    move-result-object v4

    .line 166
    invoke-direct {v3, v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType$Update;-><init>(Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;

    .line 169
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object p1

    .line 161
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;-><init>(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/ViewingImageType;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    :goto_0
    return-object v0
.end method

.method private final toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
    .locals 3

    .line 139
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$AddAttachment;

    .line 140
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;->getDefaultTitle()Ljava/lang/String;

    move-result-object v2

    .line 142
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;->getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    move-result-object p1

    .line 139
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$AddAttachment;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    return-object v0
.end method


# virtual methods
.method public final launchState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
    .locals 3

    if-eqz p1, :cond_4

    .line 288
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_3

    .line 293
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 295
    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 296
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 297
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 299
    :cond_3
    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 135
    :cond_4
    sget-object p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Idle;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Idle;

    move-object v2, p1

    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    :goto_2
    return-object v2
.end method

.method public final startState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;
    .locals 1

    const-string v0, "startAttachmentInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;->toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    move-result-object p1

    goto :goto_0

    .line 125
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;->toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    move-result-object p1

    goto :goto_0

    .line 126
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Companion;->toStartState(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
