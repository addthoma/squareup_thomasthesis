.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;
.super Ljava/lang/Object;
.source "RealEditPaymentRequestRowDataFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditPaymentRequestRowDataFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditPaymentRequestRowDataFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,99:1\n1360#2:100\n1429#2,3:101\n1360#2:104\n1429#2,3:105\n1370#2:108\n1401#2,4:109\n1529#2,3:113\n1370#2:116\n1401#2,4:117\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditPaymentRequestRowDataFactory.kt\ncom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory\n*L\n43#1:100\n43#1,3:101\n54#1:104\n54#1,3:105\n59#1:108\n59#1,4:109\n71#1,3:113\n75#1:116\n75#1,4:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ,\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000b2\u0006\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u001c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000b2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u000bH\u0002J\u0014\u0010\u0015\u001a\u00020\u0013*\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0011H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V",
        "fromPaymentRequests",
        "",
        "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
        "paymentRequests",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoiceAmount",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "installmentNames",
        "",
        "installments",
        "dueString",
        "firstSentDate",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private final dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 5

    .line 86
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 87
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->payment_request_due_upon_receipt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 89
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/widgets/impl/R$string;->payment_request_due_in:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 90
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "number_of_days"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :goto_1
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    const-string p2, "ProtoDates.calendarFromY\u2026teDueDate(firstSentDate))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 96
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final installmentNames(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 71
    check-cast p1, Ljava/lang/Iterable;

    .line 113
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 114
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 71
    invoke-static {v3}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v2, 0x0

    :cond_2
    :goto_0
    if-eqz v2, :cond_5

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 118
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_3

    .line 119
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_3
    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 76
    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/invoices/R$string;->payment_request_number:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 77
    invoke-virtual {v1, v2, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_1

    .line 120
    :cond_4
    check-cast v0, Ljava/util/List;

    return-object v0

    .line 72
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "installment rows must only contain installments"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public fromPaymentRequests(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    const-string v3, "paymentRequests"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "invoiceAmount"

    move-object/from16 v4, p2

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "invoiceFirstSentDate"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    sget-object v3, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    invoke-virtual {v3, v1}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;->fromPaymentRequests(Ljava/util/List;)Lcom/squareup/invoices/PaymentRequestsConfig;

    move-result-object v3

    .line 42
    sget-object v5, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/invoices/PaymentRequestsConfig;->ordinal()I

    move-result v3

    aget v3, v5, v3

    const/4 v5, 0x0

    const/16 v6, 0xa

    const/4 v7, 0x1

    if-eq v3, v7, :cond_4

    const/4 v8, 0x2

    if-eq v3, v8, :cond_4

    const/4 v8, 0x3

    if-eq v3, v8, :cond_3

    const/4 v7, 0x4

    if-eq v3, v7, :cond_2

    const/4 v2, 0x5

    if-ne v3, v2, :cond_1

    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid payment request config: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast v1, Ljava/lang/Iterable;

    .line 104
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v1, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 105
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 106
    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 54
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    :cond_0
    check-cast v3, Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2

    :cond_1
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 53
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->installmentNames(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_2

    .line 45
    :cond_3
    invoke-static/range {p1 .. p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 46
    iget-object v8, v0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    invoke-static {v3, v8}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 47
    move-object v8, v1

    check-cast v8, Ljava/lang/Iterable;

    invoke-static {v8, v7}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->installmentNames(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    new-array v7, v7, [Ljava/lang/CharSequence;

    aput-object v3, v7, v5

    .line 49
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 50
    check-cast v8, Ljava/util/Collection;

    invoke-interface {v3, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 43
    :cond_4
    move-object v3, v1

    check-cast v3, Ljava/lang/Iterable;

    .line 100
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v3, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 101
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 102
    check-cast v8, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 43
    iget-object v9, v0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->res:Lcom/squareup/util/Res;

    invoke-static {v8, v9}, Lcom/squareup/invoices/PaymentRequestsKt;->getPaymentRequestType(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 103
    :cond_5
    move-object v3, v7

    check-cast v3, Ljava/util/List;

    .line 57
    :goto_2
    invoke-static/range {p1 .. p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmounts(Ljava/util/List;Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v4

    .line 59
    check-cast v1, Ljava/lang/Iterable;

    .line 108
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v1, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 110
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v8, v5, 0x1

    if-gez v5, :cond_6

    .line 111
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_6
    check-cast v6, Lcom/squareup/protos/client/invoice/PaymentRequest;

    if-nez v4, :cond_7

    .line 60
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lkotlin/Pair;

    invoke-virtual {v9}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/protos/common/Money;

    .line 61
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    .line 63
    new-instance v15, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    .line 64
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v11, v9}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const/16 v9, 0x20

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 65
    invoke-direct {v0, v6, v2}, Lcom/squareup/features/invoices/widgets/paymentrequest/RealEditPaymentRequestRowDataFactory;->dueString(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v5

    move-object v12, v5

    check-cast v12, Ljava/lang/CharSequence;

    const/4 v13, 0x0

    const/4 v14, 0x4

    const/4 v5, 0x0

    move-object v10, v15

    move-object v6, v15

    move-object v15, v5

    .line 63
    invoke-direct/range {v10 .. v15}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 66
    invoke-interface {v7, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v5, v8

    goto :goto_3

    .line 112
    :cond_8
    check-cast v7, Ljava/util/List;

    return-object v7
.end method
