.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;
.super Ljava/lang/Object;
.source "InvoiceTimelineView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceTimelineView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceTimelineView.kt\ncom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,236:1\n1550#2,3:237\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceTimelineView.kt\ncom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory\n*L\n218#1,3:237\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;",
        "",
        "invoiceTimelineDateFormatter",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
        "invoiceTimelineCtaFactory",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;",
        "(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;)V",
        "create",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;",
        "context",
        "Landroid/content/Context;",
        "timelineData",
        "Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final invoiceTimelineCtaFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;

.field private final invoiceTimelineDateFormatter:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;


# direct methods
.method public constructor <init>(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceTimelineDateFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceTimelineCtaFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->invoiceTimelineDateFormatter:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->invoiceTimelineCtaFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;

    return-void
.end method


# virtual methods
.method public final create(Landroid/content/Context;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "context"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v4, "timelineData"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "eventHandler"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    new-instance v4, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    const/4 v5, 0x0

    const/4 v11, 0x2

    invoke-direct {v4, v1, v5, v11, v5}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 178
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getEvents()Ljava/util/List;

    move-result-object v12

    .line 179
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getMinify()Z

    move-result v13

    .line 180
    iget-object v5, v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->invoiceTimelineCtaFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;

    .line 181
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getTimeline()Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getCtaEvent()Ljava/lang/Object;

    move-result-object v7

    new-instance v8, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$timelineCta$1;

    invoke-direct {v8, v3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$timelineCta$1;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 180
    invoke-virtual {v5, v6, v7, v8}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->createForSectionData(Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    move-result-object v5

    .line 186
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getTimeline()Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->primary_status:Ljava/lang/String;

    .line 187
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getBackgroundColor()I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 188
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v7

    const/4 v14, 0x1

    xor-int/2addr v7, v14

    .line 185
    invoke-static {v4, v6, v1, v7, v5}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->access$addTitleRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Ljava/lang/String;IZLcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;)V

    .line 193
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getMinify()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->access$Companion()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;

    invoke-static {v1, v11}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    .line 196
    :cond_0
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    const/4 v15, 0x0

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v1, :cond_2

    .line 200
    invoke-interface {v12, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/invoice/InvoiceEvent;

    .line 202
    iget-object v6, v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->invoiceTimelineDateFormatter:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    iget-object v7, v5, Lcom/squareup/protos/client/invoice/InvoiceEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string v8, "event.occurred_at"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    const-string v9, "TimeZone.getDefault()"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;->format(Lcom/squareup/protos/client/ISO8601Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v9

    .line 203
    iget-object v6, v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->invoiceTimelineCtaFactory:Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;

    .line 204
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;->getCtaEvent()Ljava/lang/Object;

    move-result-object v7

    new-instance v8, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$eventCta$1;

    invoke-direct {v8, v3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$eventCta$1;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 203
    invoke-virtual {v6, v5, v7, v8}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->createForSectionData(Lcom/squareup/protos/client/invoice/InvoiceEvent;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    move-result-object v6

    .line 208
    iget-object v7, v5, Lcom/squareup/protos/client/invoice/InvoiceEvent;->detailed_description:Ljava/lang/String;

    xor-int/lit8 v8, v13, 0x1

    .line 211
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, v14

    if-ne v5, v10, :cond_1

    const/16 v16, 0x1

    goto :goto_2

    :cond_1
    const/16 v16, 0x0

    :goto_2
    move-object v5, v4

    move/from16 v17, v10

    move/from16 v10, v16

    .line 206
    invoke-static/range {v5 .. v10}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->access$addEventRow(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;Ljava/lang/String;ZLjava/lang/String;Z)V

    add-int/lit8 v10, v17, 0x1

    goto :goto_1

    :cond_2
    if-eqz v13, :cond_a

    .line 217
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->access$Companion()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Companion;

    if-le v1, v11, :cond_3

    const/4 v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    .line 218
    :goto_3
    check-cast v12, Ljava/lang/Iterable;

    .line 237
    instance-of v5, v12, Ljava/util/Collection;

    if-eqz v5, :cond_4

    move-object v5, v12

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_6

    .line 238
    :cond_4
    invoke-interface {v12}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/client/invoice/InvoiceEvent;

    .line 219
    iget-object v6, v6, Lcom/squareup/protos/client/invoice/InvoiceEvent;->detailed_description:Ljava/lang/String;

    check-cast v6, Ljava/lang/CharSequence;

    if-eqz v6, :cond_7

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_6

    goto :goto_4

    :cond_6
    const/4 v6, 0x0

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v6, 0x1

    :goto_5
    xor-int/2addr v6, v14

    if-eqz v6, :cond_5

    const/4 v15, 0x1

    :cond_8
    :goto_6
    if-nez v1, :cond_9

    if-eqz v15, :cond_a

    .line 222
    :cond_9
    new-instance v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$1;

    invoke-direct {v1, v3, v2}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory$create$1;-><init>(Lcom/squareup/features/invoices/widgets/EventHandler;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v4, v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;->access$setButtonVisibleOrGone(Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;Lkotlin/jvm/functions/Function0;)V

    :cond_a
    return-object v4
.end method
