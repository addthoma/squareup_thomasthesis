.class final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;
.super Ljava/lang/Object;
.source "InvoiceV1SectionRenderer.kt"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/widgets/list/ToggleButtonRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/CompoundButton;",
        "kotlin.jvm.PlatformType",
        "checked",
        "",
        "onCheckedChanged",
        "com/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$4$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onEvent$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 180
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$$inlined$apply$lambda$3;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;->getEvent()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$ToggleClicked;-><init>(Ljava/lang/Object;Z)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
