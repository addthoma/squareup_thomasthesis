.class public final Lcom/squareup/features/invoices/widgets/impl/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final TextAppearance_Invoices_V2_Button_Label:I = 0x7f13022a

.field public static final TextAppearance_Invoices_V2_PaymentSchedule_InstallmentLabel:I = 0x7f13022b

.field public static final TextAppearance_Invoices_V2_PaymentSchedule_Value:I = 0x7f13022c

.field public static final TextAppearance_Invoices_V2_Row_Description:I = 0x7f13022d

.field public static final TextAppearance_Invoices_V2_ToggleRow_Label:I = 0x7f13022e

.field public static final Theme_Invoices_BigNohoRow:I = 0x7f1302f1

.field public static final Theme_Invoices_SmallNohoRow:I = 0x7f1302f2

.field public static final Widget_Invoices_DatePicker:I = 0x7f1303e9

.field public static final Widget_Invoices_DatePicker_Title:I = 0x7f1303ea

.field public static final Widget_Invoices_Noho_AmountField:I = 0x7f1303eb

.field public static final Widget_Invoices_V2_BigRow:I = 0x7f1303ec

.field public static final Widget_Invoices_V2_Row_Icon:I = 0x7f1303ed

.field public static final Widget_Invoices_V2_SmallRow:I = 0x7f1303ee


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
