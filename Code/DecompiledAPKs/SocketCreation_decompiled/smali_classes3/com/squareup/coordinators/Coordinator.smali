.class public Lcom/squareup/coordinators/Coordinator;
.super Ljava/lang/Object;
.source "Coordinator.java"


# instance fields
.field private attached:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public final isAttached()Z
    .locals 1

    .line 60
    iget-boolean v0, p0, Lcom/squareup/coordinators/Coordinator;->attached:Z

    return v0
.end method

.method final setAttached(Z)V
    .locals 0

    .line 33
    iput-boolean p1, p0, Lcom/squareup/coordinators/Coordinator;->attached:Z

    return-void
.end method
