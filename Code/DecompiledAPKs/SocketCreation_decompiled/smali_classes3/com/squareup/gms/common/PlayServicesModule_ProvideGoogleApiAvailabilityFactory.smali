.class public final Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;
.super Ljava/lang/Object;
.source "PlayServicesModule_ProvideGoogleApiAvailabilityFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/google/android/gms/common/GoogleApiAvailability;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory$InstanceHolder;->access$000()Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideGoogleApiAvailability()Lcom/google/android/gms/common/GoogleApiAvailability;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/gms/common/PlayServicesModule;->provideGoogleApiAvailability()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/GoogleApiAvailability;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/google/android/gms/common/GoogleApiAvailability;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;->provideGoogleApiAvailability()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/gms/common/PlayServicesModule_ProvideGoogleApiAvailabilityFactory;->get()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    return-object v0
.end method
