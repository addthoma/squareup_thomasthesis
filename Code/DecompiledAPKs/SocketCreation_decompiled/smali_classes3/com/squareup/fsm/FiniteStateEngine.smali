.class public final Lcom/squareup/fsm/FiniteStateEngine;
.super Lcom/squareup/fsm/AbstractFiniteStateEngine;
.source "FiniteStateEngine.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/fsm/AbstractFiniteStateEngine<",
        "TS;TE;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public varargs constructor <init>([Lcom/squareup/fsm/Rule;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/fsm/Rule<",
            "TS;-TE;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;-><init>([Lcom/squareup/fsm/Rule;)V

    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/fsm/FiniteStateEngine;->doOnEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic peekStateToSave()Ljava/lang/Object;
    .locals 1

    .line 4
    invoke-super {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->peekStateToSave()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic startFromState(Ljava/lang/Object;)V
    .locals 0

    .line 4
    invoke-super {p0, p1}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->startFromState(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .line 4
    invoke-super {p0}, Lcom/squareup/fsm/AbstractFiniteStateEngine;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
