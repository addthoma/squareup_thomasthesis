.class public final Lcom/squareup/money/CentsMoneyFormatter;
.super Ljava/lang/Object;
.source "CentsMoneyFormatter.kt"

# interfaces
.implements Lcom/squareup/text/Formatter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/Money;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCentsMoneyFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CentsMoneyFormatter.kt\ncom/squareup/money/CentsMoneyFormatter\n*L\n1#1,44:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/money/CentsMoneyFormatter;",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyLocaleFormatter",
        "Lcom/squareup/money/MoneyLocaleFormatter;",
        "(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)V",
        "format",
        "",
        "money",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/money/MoneyLocaleFormatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/CentsMoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/money/CentsMoneyFormatter;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/money/CentsMoneyFormatter;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    return-void
.end method


# virtual methods
.method public bridge synthetic format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/money/CentsMoneyFormatter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method public format(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 6

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 24
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 25
    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySubunitSymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    .line 26
    check-cast v1, Ljava/lang/CharSequence;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v4, 0x1

    :goto_1
    xor-int/2addr v4, v3

    .line 27
    sget-object v5, Lcom/squareup/protos/common/CurrencyCode;->CAD:Lcom/squareup/protos/common/CurrencyCode;

    if-eq v0, v5, :cond_4

    sget-object v5, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    if-ne v0, v5, :cond_3

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v4, :cond_5

    if-eqz v0, :cond_5

    .line 31
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 32
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isOneWholeUnitOrMore(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v2, 0x1

    :cond_5
    if-eqz v2, :cond_6

    .line 34
    iget-object v0, p0, Lcom/squareup/money/CentsMoneyFormatter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/proto_utilities/R$string;->currency_format_cents:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 35
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int p1, v2

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "symbol"

    .line 36
    invoke-virtual {p1, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_4

    .line 39
    :cond_6
    iget-object v0, p0, Lcom/squareup/money/CentsMoneyFormatter;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    .line 40
    iget-object v1, p0, Lcom/squareup/money/CentsMoneyFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "localeProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Locale;

    sget-object v2, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->SHORTER:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    .line 39
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/money/MoneyLocaleFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/util/Locale;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)Ljava/lang/String;

    move-result-object p1

    :goto_4
    return-object p1
.end method
