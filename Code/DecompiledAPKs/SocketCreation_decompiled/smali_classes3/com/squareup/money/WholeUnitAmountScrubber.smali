.class public abstract Lcom/squareup/money/WholeUnitAmountScrubber;
.super Ljava/lang/Object;
.source "WholeUnitAmountScrubber.java"

# interfaces
.implements Lcom/squareup/text/SelectableTextScrubber;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;
    }
.end annotation


# instance fields
.field private final maxValue:J

.field private final zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;


# direct methods
.method public constructor <init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-wide p1, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->maxValue:J

    .line 33
    iput-object p3, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    return-void
.end method

.method public static maxValueFor(I)J
    .locals 3

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    const-wide v0, 0x174876e7ffL

    return-wide v0

    .line 60
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Decimal part length must be between 0 and 5 (inclusive) but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide v0, 0x2540be3ffL

    return-wide v0

    :cond_2
    const-wide/32 v0, 0x3b9ac9ff

    return-wide v0

    :cond_3
    const-wide/32 v0, 0x5f5e0ff

    return-wide v0

    :cond_4
    const-wide/32 v0, 0x98967f

    return-wide v0

    :cond_5
    const-wide/32 v0, 0xf423f

    return-wide v0
.end method

.method private parseLong(Ljava/lang/String;)J
    .locals 2

    .line 197
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method private removeNonAmountCharacters(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 119
    invoke-virtual {p0}, Lcom/squareup/money/WholeUnitAmountScrubber;->nonSelectableSuffix()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 120
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 121
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v2, v0

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 123
    :cond_0
    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method countDigitsToRight(Ljava/lang/String;I)I
    .locals 3

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    return v0

    .line 150
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/money/WholeUnitAmountScrubber;->nonSelectableSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/money/WholeUnitAmountScrubber;->nonSelectableSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 155
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v0

    if-ge p2, v2, :cond_3

    .line 156
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 157
    invoke-static {v2}, Lcom/squareup/util/Characters;->isLatinDigit(C)Z

    move-result v2

    if-eqz v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method findSelectionIndex(Ljava/lang/String;I)I
    .locals 4

    .line 179
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/money/WholeUnitAmountScrubber;->nonSelectableSuffix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    :goto_0
    if-ltz v0, :cond_2

    .line 180
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 181
    invoke-static {v3}, Lcom/squareup/util/Characters;->isLatinDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    if-lt v2, p2, :cond_0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    return v1
.end method

.method protected abstract formatAmount(J)Ljava/lang/String;
.end method

.method protected nonSelectableSuffix()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public scrub(Lcom/squareup/text/SelectableTextScrubber$SelectableText;Lcom/squareup/text/SelectableTextScrubber$SelectableText;)Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 11

    .line 65
    iget-object v0, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, ""

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p1

    .line 73
    :cond_0
    iget-object v0, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    iget v2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    .line 74
    invoke-virtual {p0, v0, v2}, Lcom/squareup/money/WholeUnitAmountScrubber;->countDigitsToRight(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    iget v2, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    .line 75
    invoke-virtual {p0, v0, v2}, Lcom/squareup/money/WholeUnitAmountScrubber;->countDigitsToRight(Ljava/lang/String;I)I

    move-result v0

    .line 76
    :goto_0
    iget-object v2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/squareup/money/WholeUnitAmountScrubber;->removeNonAmountCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    iget-object v3, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    sget-object v4, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    if-ne v3, v4, :cond_2

    invoke-static {v2}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "0"

    .line 84
    :cond_2
    invoke-direct {p0, v2}, Lcom/squareup/money/WholeUnitAmountScrubber;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 85
    iget-wide v5, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->maxValue:J

    cmp-long v7, v3, v5

    if-lez v7, :cond_3

    move-wide v3, v5

    .line 89
    :cond_3
    iget-object v5, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    sget-object v6, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    if-eq v5, v6, :cond_6

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_6

    .line 90
    iget-object v7, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    sget-object v8, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NEVER_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    if-ne v7, v8, :cond_4

    .line 91
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p1

    .line 93
    :cond_4
    iget-object p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/money/WholeUnitAmountScrubber;->removeNonAmountCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/money/WholeUnitAmountScrubber;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 95
    iget-object v9, p0, Lcom/squareup/money/WholeUnitAmountScrubber;->zeroState:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    sget-object v10, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    if-ne v9, v10, :cond_5

    cmp-long v9, v7, v5

    if-lez v9, :cond_5

    .line 97
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_5
    cmp-long v9, v7, v5

    if-nez v9, :cond_6

    .line 98
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-ge v2, p1, :cond_6

    .line 100
    new-instance p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p1, v1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p1

    .line 104
    :cond_6
    invoke-virtual {p0, v3, v4}, Lcom/squareup/money/WholeUnitAmountScrubber;->formatAmount(J)Ljava/lang/String;

    move-result-object p1

    .line 105
    iget v1, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget p2, p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    if-eq v1, p2, :cond_7

    .line 108
    new-instance p2, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {p2, p1}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;)V

    return-object p2

    .line 110
    :cond_7
    invoke-virtual {p0, p1, v0}, Lcom/squareup/money/WholeUnitAmountScrubber;->findSelectionIndex(Ljava/lang/String;I)I

    move-result p2

    .line 111
    new-instance v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    invoke-direct {v0, p1, p2, p2}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method
