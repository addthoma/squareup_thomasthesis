.class public Lcom/squareup/money/QuickCashCalculator;
.super Ljava/lang/Object;
.source "QuickCashCalculator.java"


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static amountInRange(Lcom/squareup/protos/common/Money;II)Z
    .locals 4

    .line 96
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    int-to-long v2, p1

    cmp-long p1, v0, v2

    if-ltz p1, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    int-to-long v0, p2

    cmp-long p2, p0, v0

    if-gez p2, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static containsMultipleFromArray(J[IIII)Z
    .locals 6

    :goto_0
    if-ge p3, p4, :cond_1

    .line 153
    aget v0, p2, p3

    int-to-long v1, v0

    .line 154
    rem-long v1, p0, v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    if-le v0, p5, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static denominationsForCurrencyCode(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 165
    :try_start_0
    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getDenominations(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 168
    :catch_0
    sget-object p0, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p0}, Lcom/squareup/currency_db/Currencies;->getDenominations(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static removeQuickCashOption(Lcom/squareup/protos/common/Money;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 50
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 51
    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    if-ne v0, v1, :cond_0

    .line 52
    invoke-static {p0, p1}, Lcom/squareup/money/QuickCashCalculator;->removeQuickCashOptionUSD(Lcom/squareup/protos/common/Money;Ljava/util/List;)V

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/money/QuickCashCalculator;->removeQuickCashOptionOther(Lcom/squareup/protos/common/Money;Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method private static removeQuickCashOptionOther(Lcom/squareup/protos/common/Money;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 90
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    if-lez p0, :cond_0

    .line 91
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {p1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private static removeQuickCashOptionUSD(Lcom/squareup/protos/common/Money;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const/16 v0, 0x190

    const/4 v1, 0x0

    .line 59
    invoke-static {p0, v1, v0}, Lcom/squareup/money/QuickCashCalculator;->amountInRange(Lcom/squareup/protos/common/Money;II)Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_0

    new-array v0, v2, [I

    .line 60
    fill-array-data v0, :array_0

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    :cond_0
    const/16 v1, 0x3e8

    .line 64
    invoke-static {p0, v0, v1}, Lcom/squareup/money/QuickCashCalculator;->amountInRange(Lcom/squareup/protos/common/Money;II)Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v2, [I

    .line 65
    fill-array-data v0, :array_1

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    :cond_1
    const/16 v0, 0x578

    .line 69
    invoke-static {p0, v1, v0}, Lcom/squareup/money/QuickCashCalculator;->amountInRange(Lcom/squareup/protos/common/Money;II)Z

    move-result v1

    if-eqz v1, :cond_2

    new-array v0, v2, [I

    .line 70
    fill-array-data v0, :array_2

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    :cond_2
    const/16 v1, 0x7d0

    .line 74
    invoke-static {p0, v0, v1}, Lcom/squareup/money/QuickCashCalculator;->amountInRange(Lcom/squareup/protos/common/Money;II)Z

    move-result v0

    if-eqz v0, :cond_3

    new-array v0, v2, [I

    .line 75
    fill-array-data v0, :array_3

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    :cond_3
    const/16 v0, 0x9c4

    .line 79
    invoke-static {p0, v1, v0}, Lcom/squareup/money/QuickCashCalculator;->amountInRange(Lcom/squareup/protos/common/Money;II)Z

    move-result v0

    if-eqz v0, :cond_4

    new-array v0, v2, [I

    .line 80
    fill-array-data v0, :array_4

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    :cond_4
    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 85
    fill-array-data v0, :array_5

    invoke-static {p0, p1, v0}, Lcom/squareup/money/QuickCashCalculator;->tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V

    return-void

    nop

    :array_0
    .array-data 4
        0x3e8
        0x7d0
    .end array-data

    :array_1
    .array-data 4
        0x7d0
        0x3e8
    .end array-data

    :array_2
    .array-data 4
        0x1f4
        0x64
    .end array-data

    :array_3
    .array-data 4
        0x64
        0x1f4
    .end array-data

    :array_4
    .array-data 4
        0x3e8
        0x7d0
    .end array-data

    :array_5
    .array-data 4
        0x3e8
        0x64
        0x1f4
    .end array-data
.end method

.method private static varargs tryRemovingAmountsFromOptions(Lcom/squareup/protos/common/Money;Ljava/util/List;[I)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;[I)V"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 101
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_4

    .line 103
    aget v2, p2, v1

    const/4 v9, 0x0

    .line 104
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v9, v3, :cond_3

    .line 107
    iget-object v3, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    int-to-long v5, v2

    rem-long/2addr v3, v5

    .line 108
    iget-object v7, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    sub-long/2addr v7, v3

    add-long v3, v7, v5

    .line 111
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/common/Money;

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v7, v5, v3

    if-nez v7, :cond_2

    const/16 v5, 0x64

    if-le v2, v5, :cond_0

    .line 116
    iget-object v6, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v3, v6

    const-wide/16 v10, 0x64

    cmp-long v8, v6, v10

    if-gez v8, :cond_0

    goto :goto_2

    :cond_0
    if-le v2, v5, :cond_1

    add-int/lit8 v6, v1, 0x1

    .line 124
    array-length v7, p2

    move-object v5, p2

    move v8, v2

    invoke-static/range {v3 .. v8}, Lcom/squareup/money/QuickCashCalculator;->containsMultipleFromArray(J[IIII)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_2

    .line 130
    :cond_1
    invoke-interface {p1, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void

    :cond_2
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {p1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public buildQuickCashOptions(Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 26
    invoke-static {v0}, Lcom/squareup/money/QuickCashCalculator;->denominationsForCurrencyCode(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object v1

    .line 28
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 30
    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 36
    iget-object v4, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    int-to-long v6, v3

    rem-long/2addr v4, v6

    .line 37
    iget-object v3, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    sub-long v10, v6, v4

    :goto_1
    add-long/2addr v8, v10

    .line 38
    invoke-static {v8, v9, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 44
    sget-object v0, Lcom/squareup/money/MoneyMath;->COMPARATOR:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object p1
.end method
