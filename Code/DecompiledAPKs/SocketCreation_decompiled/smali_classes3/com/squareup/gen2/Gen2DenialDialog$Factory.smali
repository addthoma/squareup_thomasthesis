.class public final Lcom/squareup/gen2/Gen2DenialDialog$Factory;
.super Ljava/lang/Object;
.source "Gen2DenialDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/gen2/Gen2DenialDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/analytics/Analytics;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 45
    new-instance p2, Lcom/squareup/gen2/Gen2DenialEvent;

    sget-object p3, Lcom/squareup/gen2/Gen2DenialDialog$Result;->REQUEST_A_READER:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-direct {p2, p3}, Lcom/squareup/gen2/Gen2DenialEvent;-><init>(Lcom/squareup/gen2/Gen2DenialDialog$Result;)V

    invoke-interface {p0, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 46
    sget p0, Lcom/squareup/cardreader/ui/R$string;->gen2_eol_request_reader_url:I

    invoke-virtual {p1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/analytics/Analytics;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 49
    new-instance p1, Lcom/squareup/gen2/Gen2DenialEvent;

    sget-object p2, Lcom/squareup/gen2/Gen2DenialDialog$Result;->OK:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-direct {p1, p2}, Lcom/squareup/gen2/Gen2DenialEvent;-><init>(Lcom/squareup/gen2/Gen2DenialDialog$Result;)V

    invoke-interface {p0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method static synthetic lambda$create$2(Lcom/squareup/analytics/Analytics;Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 51
    new-instance p2, Lcom/squareup/gen2/Gen2DenialEvent;

    sget-object p3, Lcom/squareup/gen2/Gen2DenialDialog$Result;->LEARN_MORE:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-direct {p2, p3}, Lcom/squareup/gen2/Gen2DenialEvent;-><init>(Lcom/squareup/gen2/Gen2DenialDialog$Result;)V

    invoke-interface {p0, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 52
    sget p0, Lcom/squareup/cardreader/ui/R$string;->gen2_eol_learn_more_url:I

    invoke-virtual {p1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$create$3(Lcom/squareup/analytics/Analytics;Landroid/content/DialogInterface;)V
    .locals 1

    .line 55
    new-instance p1, Lcom/squareup/gen2/Gen2DenialEvent;

    sget-object v0, Lcom/squareup/gen2/Gen2DenialDialog$Result;->OK:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-direct {p1, v0}, Lcom/squareup/gen2/Gen2DenialEvent;-><init>(Lcom/squareup/gen2/Gen2DenialDialog$Result;)V

    invoke-interface {p0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 38
    const-class v0, Lcom/squareup/gen2/Gen2DenialDialog$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/gen2/Gen2DenialDialog$Component;

    .line 39
    invoke-interface {v0}, Lcom/squareup/gen2/Gen2DenialDialog$Component;->analytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    .line 41
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_title:I

    .line 42
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_body:I

    .line 43
    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_confirm:I

    new-instance v3, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$tYH82xYKCQy0IURIMx0uhKlQdyc;

    invoke-direct {v3, v0, p1}, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$tYH82xYKCQy0IURIMx0uhKlQdyc;-><init>(Lcom/squareup/analytics/Analytics;Landroid/content/Context;)V

    .line 44
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_cancel:I

    new-instance v3, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$VL0xrlE91LLl6wAuraotfn9-ugs;

    invoke-direct {v3, v0}, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$VL0xrlE91LLl6wAuraotfn9-ugs;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 48
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/cardreader/ui/R$string;->gen2_denial_learn_more:I

    new-instance v3, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$4b2Bzb7rsRMh-ql6-RbUWARcJPM;

    invoke-direct {v3, v0, p1}, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$4b2Bzb7rsRMh-ql6-RbUWARcJPM;-><init>(Lcom/squareup/analytics/Analytics;Landroid/content/Context;)V

    .line 50
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 54
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$7Jvnxky80NDEaLdtEWUTaQKIwDg;

    invoke-direct {v1, v0}, Lcom/squareup/gen2/-$$Lambda$Gen2DenialDialog$Factory$7Jvnxky80NDEaLdtEWUTaQKIwDg;-><init>(Lcom/squareup/analytics/Analytics;)V

    .line 55
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 41
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
