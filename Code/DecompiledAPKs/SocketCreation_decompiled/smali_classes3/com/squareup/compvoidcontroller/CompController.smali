.class public interface abstract Lcom/squareup/compvoidcontroller/CompController;
.super Ljava/lang/Object;
.source "CompController.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH&J\u001a\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/compvoidcontroller/CompController;",
        "",
        "compTicket",
        "Lcom/squareup/protos/client/IdPair;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "compDiscount",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "baseOrder",
        "Lcom/squareup/payment/OrderSnapshot;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "compTransactionTicket",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract compTicket(Lcom/squareup/tickets/OpenTicket;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;
.end method

.method public abstract compTransactionTicket(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
.end method
