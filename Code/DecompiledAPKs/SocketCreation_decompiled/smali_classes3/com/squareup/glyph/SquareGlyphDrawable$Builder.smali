.class public Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
.super Ljava/lang/Object;
.source "SquareGlyphDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/SquareGlyphDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private bounds:Landroid/graphics/Rect;

.field private color:I

.field private colorStateList:Landroid/content/res/ColorStateList;

.field private fontSizeOverride:Z

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final resources:Landroid/content/res/Resources;

.field private shadowColor:I

.field private shadowDx:F

.field private shadowDy:F

.field private shadowRadius:I

.field private textPaintSize:F


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x1000000

    .line 141
    iput v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color:I

    const/4 v0, -0x1

    .line 145
    iput v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowRadius:I

    .line 152
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->resources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public bounds(Landroid/graphics/Rect;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->bounds:Landroid/graphics/Rect;

    return-object p0
.end method

.method public build()Lcom/squareup/glyph/SquareGlyphDrawable;
    .locals 13

    .line 197
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_0

    .line 198
    iget v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color:I

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList:Landroid/content/res/ColorStateList;

    .line 200
    :cond_0
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable;

    iget-object v2, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->resources:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget v4, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->textPaintSize:F

    iget-object v5, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->bounds:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList:Landroid/content/res/ColorStateList;

    iget-boolean v7, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->fontSizeOverride:Z

    iget v8, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowRadius:I

    iget v9, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowDx:F

    iget v10, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowDy:F

    iget v11, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowColor:I

    const/4 v12, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v12}, Lcom/squareup/glyph/SquareGlyphDrawable;-><init>(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;FLandroid/graphics/Rect;Landroid/content/res/ColorStateList;ZIFFILcom/squareup/glyph/SquareGlyphDrawable$1;)V

    return-object v0
.end method

.method public color(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 0

    .line 167
    iput p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color:I

    return-object p0
.end method

.method public colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->color:I

    return-object p0
.end method

.method public colorStateList(Landroid/content/res/ColorStateList;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList:Landroid/content/res/ColorStateList;

    return-object p0
.end method

.method public glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 1

    .line 156
    iput-object p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 157
    iget-object p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->resources:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/glyph/R$dimen;->glyph_font_size:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    iput p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->textPaintSize:F

    return-object p0
.end method

.method public shadow(IFFI)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 0

    .line 184
    iput p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowRadius:I

    .line 185
    iput p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowDx:F

    .line 186
    iput p3, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowDy:F

    .line 187
    iput p4, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadowColor:I

    return-object p0
.end method

.method public textPaintSizeOverride(F)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    .locals 0

    .line 178
    iput p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->textPaintSize:F

    const/4 p1, 0x1

    .line 179
    iput-boolean p1, p0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->fontSizeOverride:Z

    return-object p0
.end method
