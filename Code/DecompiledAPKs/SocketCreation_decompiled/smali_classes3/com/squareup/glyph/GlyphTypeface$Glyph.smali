.class public final enum Lcom/squareup/glyph/GlyphTypeface$Glyph;
.super Ljava/lang/Enum;
.source "GlyphTypeface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/GlyphTypeface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Glyph"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final ALL_GLYPHS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUTOMATIC_PAYMENT_COF:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BACKSPACE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BACK_ARROW_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BATTERY_TINY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BIRTHDAY_CAKE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BOX_DOLLAR_SIGN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BOX_EQUALS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BOX_PENNIES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BRIEFCASE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BRIGHTNESS_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BRIGHTNESS_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum BURGER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_CUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_DISCOVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_DISCOVER_DINERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_INTERAC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_JCB:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_MC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CHECK_X2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CHECKLIST:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_EMPLOYEE_MANAGEMENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_HEART:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_LIGHTNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_OPEN_TICKETS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_PAYROLL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_PLAY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_REPORTS_CUSTOMIZE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_STAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_SWIPE_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_TIMECARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CIRCLE_X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CLOCK_SKEW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CUSTOMER_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CUSTOMER_GROUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum CUSTOM_AMOUNT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum DOLLAR_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum DOWN_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum DRAG_HANDLE_BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum DRAG_N_DROP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum GIFT_CARD_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum GIFT_CARD_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_BARCODE_SCANNER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_BARCODE_SCANNER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CASH_DRAWER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CASH_DRAWER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CHIP_CARD_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_LOGOUT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_OVERLAY_BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_OVERLAY_BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_OVERLAY_BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_OVERLAY_BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_OVERLAY_BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_PRINTER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R12:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R12_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R12_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R12_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R4_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R6_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R6_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_R6_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_READER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_STORE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum KEYBOARD_ALPHA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum KEYBOARD_NUMBERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LEFT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOCATION_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOCATION_PIN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_EN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_EN_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_ES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_ES_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_FR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_FR_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_JA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum LOGOTYPE_JA_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum MEMO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum MINUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_5:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_6:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_7:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_KEYPAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NAVIGATION_LIBRARY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NOTE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum NO_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PHONE_RECEIVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PIN_CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PIN_CARD_GENERIC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PIN_CARD_MASTER_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PIN_CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PLUS_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum READER_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum READER_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REDEMPTION_CODE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REFERRAL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REFUNDED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum RELOAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REWARDS_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REWARDS_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum REWARD_TROPHY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum RIBBON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SEARCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPEECH_BUBBLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPLIT_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPLIT_TENDER_CASH_DOLLAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPLIT_TENDER_CASH_YEN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPLIT_TENDER_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SPLIT_TENDER_OTHER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SQUARE_LOGO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SQUARE_LOGO_SW600:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SQUARE_LOGO_SW720:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SQUARE_WALLET_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STACK_HUGE_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STACK_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STACK_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STOPWATCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_DEVICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_HELP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_ITEM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_REGISTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_REPORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum SWITCHER_TRANSACTION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum TAG_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum TAG_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum USER_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum VOLUME_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum VOLUME_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_ETHERNET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_ETHERNET_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_INFO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_NETWORK_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X2_WIFI_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum X_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public static final enum YEN_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;


# instance fields
.field private final characterString:Ljava/lang/String;

.field public final heightDp:F

.field public final heightId:I

.field private heightPx:I

.field private final textSizeId:I

.field private textSizePx:I

.field public final widthDp:F

.field public final widthId:I

.field private widthPx:I


# direct methods
.method static constructor <clinit>()V
    .locals 34

    .line 158
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v2, "AUTOMATIC_PAYMENT_COF"

    const/4 v3, 0x0

    const/16 v4, 0xf8

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->AUTOMATIC_PAYMENT_COF:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 159
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v1, "BACK_ARROW"

    const/4 v2, 0x1

    const/16 v3, 0x2264

    const/high16 v4, 0x41940000    # 18.5f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 160
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v1, 0x3a

    const-string v2, "BACK_ARROW_LARGE"

    const/4 v3, 0x2

    const/high16 v4, 0x41dc0000    # 27.5f

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 161
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/high16 v2, 0x41c80000    # 25.0f

    const-string v3, "BACKSPACE"

    const/4 v4, 0x3

    const/16 v5, 0x222b

    invoke-direct {v0, v3, v4, v5, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACKSPACE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 162
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_CHARGING"

    const/4 v5, 0x4

    const/16 v6, 0x446

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 163
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_DEAD"

    const/4 v5, 0x5

    const/16 v6, 0x448

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 164
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_FULL"

    const/4 v5, 0x6

    const/16 v6, 0x439

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 165
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_HIGH"

    const/4 v5, 0x7

    const/16 v6, 0x42d

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 166
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_LOW"

    const/16 v5, 0x8

    const/16 v6, 0x43b

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 167
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_MID"

    const/16 v5, 0x9

    const/16 v6, 0x43a

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 168
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BATTERY_OUTLINE"

    const/16 v5, 0xa

    const/16 v6, 0x444

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 169
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v8, "BATTERY_TINY_CHARGING"

    const/16 v9, 0xb

    const/16 v10, 0x442

    const/high16 v11, 0x41600000    # 14.0f

    const/high16 v12, 0x41080000    # 8.5f

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 170
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v14, "BATTERY_TINY_DEAD"

    const/16 v15, 0xc

    const/16 v16, 0x43f

    const/high16 v17, 0x41600000    # 14.0f

    const/high16 v18, 0x41080000    # 8.5f

    move-object v13, v0

    invoke-direct/range {v13 .. v18}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 171
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v4, "BATTERY_TINY_FULL"

    const/16 v5, 0xd

    const/16 v6, 0x401

    const/high16 v7, 0x41600000    # 14.0f

    const/high16 v8, 0x41080000    # 8.5f

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 172
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v10, "BATTERY_TINY_HIGH"

    const/16 v11, 0xe

    const/16 v12, 0x41d

    const/high16 v13, 0x41600000    # 14.0f

    const/high16 v14, 0x41080000    # 8.5f

    move-object v9, v0

    invoke-direct/range {v9 .. v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 173
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v4, "BATTERY_TINY_LOW"

    const/16 v5, 0xf

    const/16 v6, 0x43c

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 174
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v10, "BATTERY_TINY_MID"

    const/16 v11, 0x10

    const/16 v12, 0x451

    move-object v9, v0

    invoke-direct/range {v9 .. v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 175
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v4, "BATTERY_TINY_OUTLINE"

    const/16 v5, 0x11

    const/16 v6, 0x43d

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 176
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BIRTHDAY_CAKE"

    const/16 v5, 0x12

    const/16 v6, 0x44a

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BIRTHDAY_CAKE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 177
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v4, "BOX_PENNIES"

    const/16 v5, 0x13

    const/16 v6, 0x2022

    invoke-direct {v0, v4, v5, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PENNIES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 178
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v4, 0x42

    const-string v5, "BOX_PERCENT"

    const/16 v6, 0x14

    invoke-direct {v0, v5, v6, v4, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 179
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v5, 0x43

    const-string v6, "BOX_PLUS"

    const/16 v7, 0x15

    invoke-direct {v0, v6, v7, v5, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 180
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v6, 0x46

    const-string v7, "BOX_DOLLAR_SIGN"

    const/16 v8, 0x16

    invoke-direct {v0, v7, v8, v6, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_DOLLAR_SIGN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 181
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v7, 0x45

    const-string v8, "BOX_EQUALS"

    const/16 v9, 0x17

    invoke-direct {v0, v8, v9, v7, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_EQUALS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 182
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "BRIEFCASE"

    const/16 v9, 0x18

    const/16 v10, 0x44c

    invoke-direct {v0, v8, v9, v10, v3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIEFCASE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 183
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v3, 0xbb

    const-string v8, "BRIGHTNESS_HIGH"

    const/16 v9, 0x19

    invoke-direct {v0, v8, v9, v3, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIGHTNESS_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 184
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v8, "BRIGHTNESS_LOW"

    const/16 v9, 0x1a

    const/16 v10, 0x201d

    invoke-direct {v0, v8, v9, v10, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIGHTNESS_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 185
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v12, "BURGER"

    const/16 v13, 0x1b

    const/16 v14, 0x5f

    const/high16 v15, 0x41940000    # 18.5f

    const/high16 v16, 0x41680000    # 14.5f

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 186
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v9, "BURGER_SETTINGS"

    const/16 v10, 0x1c

    const/16 v11, 0x3b6

    invoke-direct {v0, v9, v10, v11, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 187
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v9, 0x39

    const-string v10, "CARD_AMEX"

    const/16 v11, 0x1d

    invoke-direct {v0, v10, v11, v9, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 188
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v10, 0x31

    const-string v11, "CARD_BACK"

    const/16 v12, 0x1e

    invoke-direct {v0, v11, v12, v10, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 189
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v11, "CARD_CHIP"

    const/16 v12, 0x1f

    const/16 v13, 0x433

    invoke-direct {v0, v11, v12, v13, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 190
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v11, 0x76

    const-string v12, "CARD_CUP"

    const/16 v13, 0x20

    invoke-direct {v0, v12, v13, v11, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 191
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v12, 0x21

    const-string v13, "CARD_DISCOVER"

    const/16 v14, 0x71

    invoke-direct {v0, v13, v12, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 192
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CARD_DISCOVER_DINERS"

    const/16 v14, 0x22

    const/16 v15, 0x75

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER_DINERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 193
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CARD_INTERAC"

    const/16 v14, 0x23

    const/16 v15, 0x2206

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_INTERAC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 194
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CARD_JCB"

    const/16 v14, 0x24

    const/16 v15, 0x72

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_JCB:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 195
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CARD_MC"

    const/16 v14, 0x25

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_MC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 196
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CARD_VISA"

    const/16 v14, 0x26

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 197
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v17, "CHECK"

    const/16 v18, 0x27

    const/16 v19, 0x221a

    const/high16 v20, 0x41940000    # 18.5f

    const/high16 v21, 0x41480000    # 12.5f

    move-object/from16 v16, v0

    invoke-direct/range {v16 .. v21}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 198
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CHECK_X2"

    const/16 v14, 0x28

    const/16 v15, 0x221a

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK_X2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 199
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CARD"

    const/16 v14, 0x29

    invoke-direct {v0, v13, v14, v12, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 200
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CARD_CHIP"

    const/16 v14, 0x2a

    const/16 v15, 0x435

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 201
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CHECK"

    const/16 v14, 0x2b

    const/16 v15, 0x53

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 202
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CHECKLIST"

    const/16 v14, 0x2c

    const/16 v15, 0x5a

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECKLIST:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 203
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CLOCK"

    const/16 v14, 0x2d

    const/16 v15, 0x3bb

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 204
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CONTACTLESS"

    const/16 v14, 0x2e

    const/16 v15, 0x3c5

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 205
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_CONTACTS"

    const/16 v14, 0x2f

    const/16 v15, 0x204c

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 206
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_EMPLOYEE_MANAGEMENT"

    const/16 v14, 0x30

    const/16 v15, 0x62

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EMPLOYEE_MANAGEMENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 207
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_ENVELOPE"

    const/16 v14, 0x52

    invoke-direct {v0, v13, v10, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 208
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_EXCLAMATION"

    const/16 v14, 0x32

    const/16 v15, 0x7e

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 209
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_GIFT_CARD"

    const/16 v14, 0x33

    const/16 v15, 0x3c1

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 210
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_HEART"

    const/16 v14, 0x34

    const/16 v15, 0x54

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_HEART:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 211
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_INVOICE"

    const/16 v14, 0x35

    const/16 v15, 0x7a

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 212
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v13, "CIRCLE_LIGHTNING"

    const/16 v14, 0x36

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LIGHTNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 213
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v13, 0x37

    const-string v14, "CIRCLE_LOCATION"

    const/16 v15, 0x204d

    invoke-direct {v0, v14, v13, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 214
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v14, "CIRCLE_LOCK"

    const/16 v15, 0x38

    const/16 v10, 0x3b8

    invoke-direct {v0, v14, v15, v10, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 215
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_MICROPHONE"

    const/16 v14, 0x29bf

    invoke-direct {v0, v10, v9, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 216
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_OPEN_TICKETS"

    const/16 v14, 0x67

    invoke-direct {v0, v10, v1, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_OPEN_TICKETS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 217
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_PAYROLL"

    const/16 v14, 0x3b

    const/16 v15, 0x63

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PAYROLL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 218
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_PHONE"

    const/16 v14, 0x3c

    const/16 v15, 0x25a0

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 219
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_PLAY"

    const/16 v14, 0x3d

    const/16 v15, 0x257

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PLAY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 220
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_PRINTER"

    const/16 v14, 0x3e

    const/16 v15, 0x26

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 221
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_REPORTS_CUSTOMIZE"

    const/16 v14, 0x3f

    const/16 v15, 0x3b7

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_REPORTS_CUSTOMIZE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 222
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_RECEIPT"

    const/16 v14, 0x40

    const/16 v15, 0x56

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 223
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_REWARDS"

    const/16 v14, 0x41

    const/16 v15, 0x57

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 224
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_SMS"

    const/16 v14, 0x6b

    invoke-direct {v0, v10, v4, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 225
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_STACK"

    const/16 v14, 0x3c2

    invoke-direct {v0, v10, v5, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 226
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_STAR"

    const/16 v14, 0x44

    const/16 v15, 0x55

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 227
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_STORAGE"

    const/16 v14, 0x2043

    invoke-direct {v0, v10, v7, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 228
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_SWIPE_ERROR"

    const/16 v14, 0x39a

    invoke-direct {v0, v10, v6, v14, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SWIPE_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 229
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_TAG"

    const/16 v14, 0x47

    const/16 v15, 0x3be

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 230
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v10, "CIRCLE_TIMECARDS"

    const/16 v14, 0x48

    const/16 v15, 0x61

    invoke-direct {v0, v10, v14, v15, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TIMECARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 231
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v10, 0x59

    const-string v14, "CIRCLE_WARNING"

    const/16 v15, 0x49

    invoke-direct {v0, v14, v15, v10, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 232
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v14, "CIRCLE_X"

    const/16 v15, 0x4a

    const/16 v6, 0x3c6

    invoke-direct {v0, v14, v15, v6, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 233
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CLOCK"

    const/16 v14, 0x4b

    const/16 v15, 0x4c

    invoke-direct {v0, v8, v14, v15, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 234
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v19, "CLOCK_SKEW"

    const/16 v20, 0x4c

    const/16 v21, 0xe6

    const v22, 0x43958000    # 299.0f

    const/high16 v23, 0x42f50000    # 122.5f

    move-object/from16 v18, v0

    invoke-direct/range {v18 .. v23}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CLOCK_SKEW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 235
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CONTACTLESS"

    const/16 v14, 0x4d

    const/16 v15, 0xbf

    invoke-direct {v0, v8, v14, v15, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 236
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/high16 v6, 0x42940000    # 74.0f

    const-string v8, "CONTACTS"

    const/16 v14, 0x4e

    const/16 v15, 0x30fb

    invoke-direct {v0, v8, v14, v15, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 237
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v8, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v14, "CUSTOM_AMOUNT"

    const/16 v15, 0x4f

    const/16 v7, 0x20ae

    invoke-direct {v0, v14, v15, v7, v8}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOM_AMOUNT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 238
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CUSTOMER"

    const/16 v14, 0x50

    const/16 v15, 0x440

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 239
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CUSTOMER_ADD"

    const/16 v14, 0x51

    const/16 v15, 0x441

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 240
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CUSTOMER_CIRCLE"

    const/16 v14, 0x52

    const/16 v15, 0xb0

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 241
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "CUSTOMER_GROUP"

    const/16 v14, 0x53

    const/16 v15, 0x44d

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_GROUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 242
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "DOLLAR_BILL"

    const/16 v14, 0x54

    const/16 v15, 0x32

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOLLAR_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 243
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v20, "DOWN_CARET"

    const/16 v21, 0x55

    const/16 v22, 0x2265

    const/high16 v23, 0x41940000    # 18.5f

    const/high16 v24, 0x41300000    # 11.0f

    move-object/from16 v19, v0

    invoke-direct/range {v19 .. v24}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOWN_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 244
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v26, "DRAG_HANDLE_BURGER"

    const/16 v27, 0x56

    const/16 v28, 0x64

    const/high16 v29, 0x41880000    # 17.0f

    const/high16 v30, 0x41680000    # 14.5f

    move-object/from16 v25, v0

    invoke-direct/range {v25 .. v30}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DRAG_HANDLE_BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 245
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v7, "DRAG_N_DROP"

    const/16 v8, 0x57

    const/16 v14, 0x23

    const/high16 v15, 0x42b00000    # 88.0f

    invoke-direct {v0, v7, v8, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DRAG_N_DROP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 246
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "ENVELOPE"

    const/16 v14, 0x58

    const/16 v15, 0x50

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 247
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "GIFT_CARD_SMALL"

    const/16 v14, 0x34

    invoke-direct {v0, v8, v10, v14, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 248
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v20, "GIFT_CARD_MEDIUM"

    const/16 v21, 0x5a

    const/16 v22, 0x3a9

    const/high16 v23, 0x42460000    # 49.5f

    const/high16 v24, 0x42120000    # 36.5f

    move-object/from16 v19, v0

    invoke-direct/range {v19 .. v24}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 249
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v26, "GIFT_CARD_LARGE"

    const/16 v27, 0x5b

    const/16 v28, 0x41a

    const/high16 v29, 0x42720000    # 60.5f

    const/high16 v30, 0x42320000    # 44.5f

    move-object/from16 v25, v0

    invoke-direct/range {v25 .. v30}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 250
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_BARCODE_SCANNER_CONNECTED"

    const/16 v14, 0x5c

    const/16 v15, 0x3ae

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 251
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_BARCODE_SCANNER_DISCONNECTED"

    const/16 v14, 0x5d

    const/16 v15, 0x3af

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 252
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CARD"

    const/16 v14, 0x5e

    const/16 v15, 0x6d

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 253
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CASH_DRAWER_CONNECTED"

    const/16 v14, 0x5f

    const/16 v15, 0x386

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 254
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CASH_DRAWER_DISCONNECTED"

    const/16 v14, 0x60

    const/16 v15, 0x388

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 255
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CHIP_CARD_NOT_USABLE"

    const/16 v14, 0x61

    const/16 v15, 0x5c

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 256
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CHIP_CARD_USABLE"

    const/16 v14, 0x62

    const/16 v15, 0xc5

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 257
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_CONTACTLESS"

    const/16 v14, 0x63

    const/16 v15, 0x3c5

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 258
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_LOGOUT"

    const/16 v14, 0x64

    const/16 v15, 0x153

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_LOGOUT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 259
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_OVERLAY_BATTERY_DEAD"

    const/16 v14, 0x65

    const/16 v15, 0x419

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 260
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_OVERLAY_BATTERY_FULL"

    const/16 v14, 0x66

    const/16 v15, 0x414

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 261
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_OVERLAY_BATTERY_HIGH"

    const/16 v14, 0x67

    const/16 v15, 0x445

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 262
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_OVERLAY_BATTERY_LOW"

    const/16 v14, 0x68

    const/16 v15, 0x418

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 263
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_OVERLAY_BATTERY_MID"

    const/16 v14, 0x69

    const/16 v15, 0x416

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 264
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_PRINTER_CONNECTED"

    const/16 v14, 0x6a

    const/16 v15, 0x2c

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 265
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v8, "HUD_PRINTER_DISCONNECTED"

    const/16 v14, 0x6b

    const/16 v15, 0x2e

    invoke-direct {v0, v8, v14, v15, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 266
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/16 v8, 0x6f

    const-string v14, "HUD_R4_READER"

    const/16 v15, 0x6c

    invoke-direct {v0, v14, v15, v8, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R4_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 267
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v14, "HUD_R6_READER"

    const/16 v15, 0x6d

    invoke-direct {v0, v14, v15, v8, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 268
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v7, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v14, "HUD_R6_BATTERY_CHARGING"

    const/16 v15, 0x6e

    const/16 v5, 0xa3

    invoke-direct {v0, v14, v15, v5, v7}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 269
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_R6_BATTERY_OUTLINE"

    const/16 v14, 0x60

    invoke-direct {v0, v7, v8, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 270
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_R12"

    const/16 v14, 0x70

    const/16 v15, 0x411

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 271
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_R12_BATTERY_CHARGING"

    const/16 v14, 0x71

    const/16 v15, 0x432

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 272
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_R12_BATTERY_OUTLINE"

    const/16 v14, 0x72

    const/16 v15, 0x413

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 273
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_R12_DISCONNECTED"

    const/16 v14, 0x73

    const/16 v15, 0x41f

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 274
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_READER_DISCONNECTED"

    const/16 v14, 0x74

    const/16 v15, 0x70

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_READER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 275
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_REWARDS"

    const/16 v14, 0x75

    const/16 v15, 0x447

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 276
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_STORE"

    const/16 v14, 0x29

    invoke-direct {v0, v7, v11, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_STORE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 277
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_SWIPE"

    const/16 v14, 0x77

    const/16 v15, 0x6e

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 278
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "HUD_WARNING"

    const/16 v14, 0x78

    invoke-direct {v0, v7, v14, v10, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 279
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "INVOICE"

    const/16 v14, 0x79

    const/16 v15, 0x3f

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 280
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "KEYBOARD_ALPHA"

    const/16 v7, 0x7a

    const/16 v14, 0xae

    invoke-direct {v0, v5, v7, v14, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_ALPHA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 281
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "KEYBOARD_NUMBERS"

    const/16 v7, 0x7b

    const/16 v14, 0x3c0

    invoke-direct {v0, v5, v7, v14, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_NUMBERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 282
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "LEFT_CARET"

    const/16 v22, 0x7c

    const/16 v23, 0x3c

    const/high16 v24, 0x41300000    # 11.0f

    const/high16 v25, 0x41940000    # 18.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LEFT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 283
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "LOCATION"

    const/16 v7, 0x7d

    const/16 v14, 0x74

    invoke-direct {v0, v5, v7, v14, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 284
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "LOCATION_CIRCLE"

    const/16 v7, 0x7e

    const/16 v14, 0x25e6

    invoke-direct {v0, v5, v7, v14, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 285
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "LOCATION_PIN"

    const/16 v14, 0x7f

    const/16 v15, 0x44e

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_PIN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 286
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v24, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_en:I

    sget v25, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height:I

    sget v26, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size:I

    const-string v21, "LOGOTYPE_EN"

    const/16 v22, 0x80

    const/16 v23, 0x24

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v26}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 288
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v31, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_en_world:I

    sget v32, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height_world:I

    sget v33, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size_world:I

    const-string v28, "LOGOTYPE_EN_WORLD"

    const/16 v29, 0x81

    const/16 v30, 0x24

    move-object/from16 v27, v0

    invoke-direct/range {v27 .. v33}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 290
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v24, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_es:I

    sget v25, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height:I

    sget v26, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size:I

    const-string v21, "LOGOTYPE_ES"

    const/16 v22, 0x82

    const/16 v23, 0xa2

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v26}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 292
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v31, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_es_world:I

    sget v32, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height_world:I

    sget v33, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size_world:I

    const-string v28, "LOGOTYPE_ES_WORLD"

    const/16 v29, 0x83

    const/16 v30, 0xa2

    move-object/from16 v27, v0

    invoke-direct/range {v27 .. v33}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 294
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v24, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_fr:I

    sget v25, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height:I

    sget v26, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size:I

    const-string v21, "LOGOTYPE_FR"

    const/16 v22, 0x84

    const/16 v23, 0x20ac

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v26}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 296
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v31, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_fr_world:I

    sget v32, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height_world:I

    sget v33, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size_world:I

    const-string v28, "LOGOTYPE_FR_WORLD"

    const/16 v29, 0x85

    const/16 v30, 0x20ac

    move-object/from16 v27, v0

    invoke-direct/range {v27 .. v33}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 298
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v24, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_ja:I

    sget v25, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height:I

    sget v26, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size:I

    const-string v21, "LOGOTYPE_JA"

    const/16 v22, 0x86

    const/16 v23, 0xa5

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v26}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 300
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v31, Lcom/squareup/glyph/R$dimen;->glyph_logotype_width_ja_world:I

    sget v32, Lcom/squareup/glyph/R$dimen;->glyph_logotype_height_world:I

    sget v33, Lcom/squareup/glyph/R$dimen;->glyph_logotype_font_size_world:I

    const-string v28, "LOGOTYPE_JA_WORLD"

    const/16 v29, 0x87

    const/16 v30, 0xa5

    move-object/from16 v27, v0

    invoke-direct/range {v27 .. v33}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICIII)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 302
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "MEMO"

    const/16 v14, 0x88

    const/16 v15, 0x44b

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MEMO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 303
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "MICROPHONE"

    const/16 v7, 0x89

    const/16 v14, 0x25c9

    invoke-direct {v0, v5, v7, v14, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 304
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "MINUS"

    const/16 v7, 0x8a

    const/16 v14, 0x2d

    const/high16 v15, 0x41880000    # 17.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MINUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 305
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_KEYPAD"

    const/16 v14, 0x8b

    const/16 v15, 0x5e

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_KEYPAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 306
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_LIBRARY"

    const/16 v14, 0x8c

    const/16 v15, 0x40

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_LIBRARY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 307
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_1"

    const/16 v14, 0x8d

    const/16 v15, 0x3b1

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 308
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_2"

    const/16 v14, 0x8e

    const/16 v15, 0x3b2

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 309
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_3"

    const/16 v14, 0x8f

    const/16 v15, 0x3b3

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 310
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_4"

    const/16 v14, 0x90

    const/16 v15, 0x3b4

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 311
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_5"

    const/16 v14, 0x91

    const/16 v15, 0x3b5

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_5:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 312
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_6"

    const/16 v14, 0x92

    const/16 v15, 0x2191

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_6:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 313
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NAVIGATION_7"

    const/16 v14, 0x93

    const/16 v15, 0x2192

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_7:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 314
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NOTE"

    const/16 v14, 0x94

    const/16 v15, 0x79

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NOTE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 315
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "NO_CIRCLE"

    const/16 v14, 0x95

    const/16 v15, 0x51

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NO_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 316
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "OTHER_TENDER"

    const/16 v14, 0x96

    const/16 v15, 0x33

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 317
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "PERSON"

    const/16 v14, 0x97

    const/16 v15, 0xb0

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 318
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "PHONE"

    const/16 v7, 0x98

    const/16 v14, 0x25cb

    invoke-direct {v0, v5, v7, v14, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 319
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "PHONE_RECEIVER"

    const/16 v14, 0x99

    const/16 v15, 0x44f

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE_RECEIVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 320
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "PIN_CARD_GENERIC"

    const/16 v22, 0x9a

    const/16 v23, 0x3bc

    const/high16 v24, 0x41a40000    # 20.5f

    const/high16 v25, 0x41680000    # 14.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_GENERIC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 321
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v27, "PIN_CARD_MASTER_CARD"

    const/16 v28, 0x9b

    const/16 v29, 0x3b0

    const/high16 v30, 0x41a40000    # 20.5f

    const/high16 v31, 0x41680000    # 14.5f

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_MASTER_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 322
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "PIN_CARD_AMEX"

    const/16 v22, 0x9c

    const/16 v23, 0x3aa

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 323
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v27, "PIN_CARD_VISA"

    const/16 v28, 0x9d

    const/16 v29, 0x3b9

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 324
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "PLUS"

    const/16 v7, 0x9e

    const/16 v14, 0x2b

    const/high16 v15, 0x41880000    # 17.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 325
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "PLUS_CIRCLE"

    const/16 v14, 0x9f

    const/16 v15, 0x2020

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PLUS_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 326
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "PRINTER"

    const/16 v14, 0xa0

    const/16 v15, 0x4e

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 327
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "READER_MEDIUM"

    const/16 v22, 0xa1

    const/16 v23, 0xcf

    const/high16 v24, 0x42760000    # 61.5f

    const/high16 v25, 0x42720000    # 60.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->READER_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 328
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "READER_SMALL"

    const/16 v7, 0xa2

    const/16 v14, 0x3bf

    const/high16 v15, 0x41e00000    # 28.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->READER_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 329
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "RECEIPT"

    const/16 v14, 0xa3

    const/16 v15, 0x4b

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 330
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "REDEMPTION_CODE"

    const/16 v14, 0xa4

    const/16 v15, 0x36

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REDEMPTION_CODE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 331
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    iget v5, v5, Lcom/squareup/glyph/GlyphTypeface$Size;->dimenDp:F

    const-string v21, "REFERRAL"

    const/16 v22, 0xa5

    const/16 v23, 0x41

    const/high16 v24, 0x430a0000    # 138.0f

    move-object/from16 v20, v0

    move/from16 v25, v5

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REFERRAL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 332
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "REFUNDED"

    const/16 v14, 0xa6

    const/16 v15, 0x6c

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REFUNDED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 333
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "RELOAD"

    const/16 v14, 0xa7

    const/16 v15, 0x5b

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RELOAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 334
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "REWARD_TROPHY"

    const/16 v14, 0xa8

    const/16 v15, 0xb6

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARD_TROPHY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 335
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "REWARDS"

    const/16 v14, 0xa9

    const/16 v15, 0x2211

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 336
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "REWARDS_MEDIUM"

    const/16 v22, 0xaa

    const/16 v23, 0x3bd

    const/high16 v24, 0x42460000    # 49.5f

    const/high16 v25, 0x42120000    # 36.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 337
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v27, "REWARDS_LARGE"

    const/16 v28, 0xab

    const/16 v29, 0x41b

    const/high16 v30, 0x42720000    # 60.5f

    const/high16 v31, 0x42320000    # 44.5f

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 338
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "RIBBON"

    const/16 v7, 0xac

    const/16 v14, 0x3c7

    const/high16 v15, 0x435c0000    # 220.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIBBON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 339
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "RIGHT_CARET"

    const/16 v22, 0xad

    const/16 v23, 0x3e

    const/high16 v24, 0x41300000    # 11.0f

    const/high16 v25, 0x41940000    # 18.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 340
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SAVE_CARD"

    const/16 v14, 0xae

    const/16 v15, 0x2190

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 341
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "SEARCH"

    const/16 v22, 0xaf

    const/16 v23, 0x66

    const/high16 v24, 0x41a80000    # 21.0f

    const/high16 v25, 0x41a40000    # 20.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SEARCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 342
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v27, "SORT"

    const/16 v28, 0xb0

    const/16 v29, 0x2019

    const/high16 v30, 0x41940000    # 18.5f

    const/high16 v31, 0x41880000    # 17.0f

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 343
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPEECH_BUBBLE"

    const/16 v14, 0xb1

    const/16 v15, 0x4f

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPEECH_BUBBLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 344
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPLIT_TENDER"

    const/16 v14, 0xb2

    const/16 v15, 0xe5

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 345
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPLIT_TENDER_CASH_DOLLAR"

    const/16 v14, 0xb3

    const/16 v15, 0xdf

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_DOLLAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 346
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPLIT_TENDER_CASH_YEN"

    const/16 v14, 0xb4

    const/16 v15, 0x192

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_YEN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 347
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPLIT_TENDER_CHIP"

    const/16 v14, 0xb5

    const/16 v15, 0x449

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 348
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SPLIT_TENDER_OTHER"

    const/16 v14, 0xb6

    const/16 v15, 0x2202

    invoke-direct {v0, v7, v14, v15, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_OTHER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 349
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "SQUARE_LOGO"

    const/16 v7, 0xb7

    const/16 v14, 0x42a

    const/high16 v15, 0x42780000    # 62.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 350
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "SQUARE_LOGO_SW600"

    const/16 v7, 0xb8

    const/16 v14, 0x429

    const/high16 v15, 0x428c0000    # 70.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO_SW600:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 351
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "SQUARE_LOGO_SW720"

    const/16 v7, 0xb9

    const/16 v14, 0x428

    const/high16 v15, 0x429c0000    # 78.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO_SW720:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 352
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "SQUARE_WALLET_TENDER"

    const/16 v14, 0xba

    invoke-direct {v0, v7, v14, v13, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_WALLET_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 353
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v7, "STACK_SMALL"

    const/16 v14, 0x4d

    invoke-direct {v0, v7, v3, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 354
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "STACK_MEDIUM"

    const/16 v7, 0xbc

    const/16 v14, 0x427

    const/high16 v15, 0x42340000    # 45.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 355
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "STACK_LARGE"

    const/16 v7, 0xbd

    const/16 v14, 0x424

    const/high16 v15, 0x425c0000    # 55.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 356
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "STACK_HUGE_ADD"

    const/16 v7, 0xbe

    const/16 v14, 0x2a

    const/high16 v15, 0x42900000    # 72.0f

    invoke-direct {v0, v5, v7, v14, v15}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_HUGE_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 357
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "STOPWATCH"

    const/16 v22, 0xbf

    const/16 v23, 0x3ad

    const/high16 v24, 0x41c40000    # 24.5f

    const/high16 v25, 0x41d40000    # 26.5f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STOPWATCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 358
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "STORAGE"

    const/16 v7, 0xc0

    const/16 v14, 0x25cf

    invoke-direct {v0, v5, v7, v14, v6}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 359
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_CUSTOMER"

    const/16 v7, 0xc1

    const/16 v14, 0xc7

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 360
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_DEVICE"

    const/16 v7, 0xc2

    const/16 v14, 0x2014

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_DEVICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 361
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_HELP"

    const/16 v7, 0xc3

    const/16 v14, 0x2021

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_HELP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 362
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_ITEM"

    const/16 v7, 0xc4

    const/16 v14, 0x203a

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_ITEM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 363
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_REPORT"

    const/16 v7, 0xc5

    const v14, 0xfb01

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_REPORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 364
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_TRANSACTION"

    const/16 v7, 0xc6

    const/16 v14, 0x2039

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_TRANSACTION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 365
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_REGISTER"

    const/16 v7, 0xc7

    const/16 v14, 0x2044

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_REGISTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 366
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "SWITCHER_SETTINGS"

    const/16 v7, 0xc8

    const v14, 0xfb02

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 367
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "TAG_LARGE"

    const/16 v6, 0xc9

    const/16 v7, 0x410

    const/high16 v14, 0x426c0000    # 59.0f

    invoke-direct {v0, v5, v6, v7, v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 368
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "TAG_MEDIUM"

    const/16 v6, 0xca

    const/16 v7, 0x7c

    const/high16 v14, 0x42440000    # 49.0f

    invoke-direct {v0, v5, v6, v7, v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 369
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "TAG_SMALL"

    const/16 v7, 0xcb

    const/16 v14, 0x69

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 370
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "TAG_TINY"

    const/16 v6, 0xcc

    const/16 v7, 0x3c4

    const/high16 v14, 0x41880000    # 17.0f

    invoke-direct {v0, v5, v6, v7, v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 371
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "UNKNOWN_TENDER"

    const/16 v7, 0xcd

    invoke-direct {v0, v6, v7, v13, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 372
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v21, "USER_LARGE"

    const/16 v22, 0xce

    const/16 v23, 0x7b

    const/high16 v24, 0x42780000    # 62.0f

    const/high16 v25, 0x42840000    # 66.0f

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->USER_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 373
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "VOLUME_HIGH"

    const/16 v6, 0xcf

    const v7, 0xf8ff

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->VOLUME_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 374
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "VOLUME_LOW"

    const/16 v6, 0xd0

    const/16 v7, 0xd2

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->VOLUME_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 375
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v6, "WARNING_SMALL"

    const/16 v7, 0xd1

    const/16 v14, 0x73

    invoke-direct {v0, v6, v7, v14, v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 376
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X"

    const/16 v6, 0xd2

    const/16 v7, 0x58

    const/high16 v14, 0x41940000    # 18.5f

    invoke-direct {v0, v5, v6, v7, v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 377
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X_LARGE"

    const/16 v6, 0xd3

    const/16 v7, 0x3b

    const/high16 v14, 0x41e00000    # 28.0f

    invoke-direct {v0, v5, v6, v7, v14}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 378
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_INFO"

    const/16 v6, 0xd4

    const/16 v7, 0xcd

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_INFO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 379
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_1"

    const/16 v6, 0xd5

    const/16 v7, 0x2d8

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 380
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_2"

    const/16 v6, 0xd6

    const/16 v7, 0xaf

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 381
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_3"

    const/16 v6, 0xd7

    const/16 v7, 0x2dc

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 382
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_4"

    const/16 v6, 0xd8

    const/16 v7, 0x131

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 383
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_ERROR"

    const/16 v6, 0xd9

    const/16 v7, 0x25ca

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 384
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_WIFI_LOCK"

    const/16 v6, 0xda

    const/16 v7, 0xce

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 385
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_ETHERNET"

    const/16 v6, 0xdb

    const/16 v7, 0xb8

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_ETHERNET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 386
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_ETHERNET_ERROR"

    const/16 v6, 0xdc

    const/16 v7, 0x2db

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_ETHERNET_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 387
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v5, "X2_NETWORK_ERROR"

    const/16 v6, 0xdd

    const/16 v7, 0xa8

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_NETWORK_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 388
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    const-string v5, "YEN_BILL"

    const/16 v6, 0xde

    const/16 v7, 0x35

    invoke-direct {v0, v5, v6, v7, v2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->YEN_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v0, 0xdf

    new-array v0, v0, [Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 157
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->AUTOMATIC_PAYMENT_COF:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x0

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x1

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x2

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACKSPACE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x3

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x4

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x5

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x6

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v5, 0x7

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x8

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x9

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xa

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xb

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xc

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xd

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xe

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0xf

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x10

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BATTERY_TINY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x11

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BIRTHDAY_CAKE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x12

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PENNIES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x13

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PERCENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x14

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x15

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_DOLLAR_SIGN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x16

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BOX_EQUALS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x17

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIEFCASE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x18

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIGHTNESS_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x19

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BRIGHTNESS_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1a

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1b

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1c

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1d

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_BACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1e

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x1f

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_CUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x20

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v2, v0, v12

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_DISCOVER_DINERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x22

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_INTERAC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x23

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_JCB:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x24

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_MC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x25

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x26

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x27

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK_X2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x28

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x29

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CARD_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2a

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2b

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECKLIST:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2c

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2d

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2e

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x2f

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EMPLOYEE_MANAGEMENT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x30

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x31

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_EXCLAMATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x32

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_GIFT_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x33

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_HEART:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x34

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x35

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LIGHTNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x36

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v2, v0, v13

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v5, 0x38

    aput-object v2, v0, v5

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v2, v0, v9

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_OPEN_TICKETS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PAYROLL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PLAY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_REPORTS_CUSTOMIZE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SMS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STACK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_SWIPE_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TAG:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TIMECARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CLOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CLOCK_SKEW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOM_AMOUNT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CUSTOMER_GROUP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOLLAR_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DOWN_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DRAG_HANDLE_BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->DRAG_N_DROP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CASH_DRAWER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CONTACTLESS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_LOGOUT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_DEAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_FULL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_OVERLAY_BATTERY_MID:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R4_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_READER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R6_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_BATTERY_CHARGING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_BATTERY_OUTLINE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_R12_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_READER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_STORE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->INVOICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_ALPHA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->KEYBOARD_NUMBERS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LEFT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_PIN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MEMO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MINUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_KEYPAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_LIBRARY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_5:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_6:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NAVIGATION_7:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NOTE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NO_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->OTHER_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE_RECEIVER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_GENERIC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_MASTER_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PLUS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PLUS_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PRINTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->READER_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->READER_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REDEMPTION_CODE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REFERRAL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REFUNDED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RELOAD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARD_TROPHY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->REWARDS_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIBBON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SAVE_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SEARCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPEECH_BUBBLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_DOLLAR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CASH_YEN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_CHIP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SPLIT_TENDER_OTHER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO_SW600:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_LOGO_SW720:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SQUARE_WALLET_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STACK_HUGE_ADD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STOPWATCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_CUSTOMER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_DEVICE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_HELP:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_ITEM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_REPORT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_TRANSACTION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_REGISTER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->SWITCHER_SETTINGS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->TAG_TINY:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->UNKNOWN_TENDER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->USER_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->VOLUME_HIGH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->VOLUME_LOW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->WARNING_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_INFO:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_1:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_2:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_3:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_4:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_WIFI_LOCK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_ETHERNET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_ETHERNET_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X2_NETWORK_ERROR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->YEN_BILL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->$VALUES:[Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 390
    invoke-static {}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->values()[Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICF)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CF)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p4

    .line 419
    invoke-direct/range {v0 .. v5}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICFF)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICFF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CFF)V"
        }
    .end annotation

    .line 422
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, -0x1

    .line 395
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    .line 396
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    .line 397
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    .line 423
    invoke-static {p3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->characterString:Ljava/lang/String;

    .line 424
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthId:I

    .line 425
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightId:I

    .line 426
    iput p4, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthDp:F

    .line 427
    iput p5, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightDp:F

    .line 428
    sget p1, Lcom/squareup/glyph/R$dimen;->glyph_font_size:I

    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizeId:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CIII)V"
        }
    .end annotation

    .line 405
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, -0x1

    .line 395
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    .line 396
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    .line 397
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    .line 406
    invoke-static {p3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->characterString:Ljava/lang/String;

    .line 407
    iput p4, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthId:I

    .line 408
    iput p5, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightId:I

    const/high16 p1, -0x40800000    # -1.0f

    .line 409
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthDp:F

    .line 410
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightDp:F

    .line 411
    iput p6, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizeId:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ICLcom/squareup/glyph/GlyphTypeface$Size;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C",
            "Lcom/squareup/glyph/GlyphTypeface$Size;",
            ")V"
        }
    .end annotation

    .line 415
    iget p4, p4, Lcom/squareup/glyph/GlyphTypeface$Size;->dimenDp:F

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/glyph/GlyphTypeface$Glyph;-><init>(Ljava/lang/String;ICF)V

    return-void
.end method

.method private static dpToPxRounded(FI)I
    .locals 1

    int-to-float p1, p1

    const/high16 v0, 0x43200000    # 160.0f

    div-float/2addr p1, v0

    mul-float p0, p0, p1

    const/high16 p1, 0x3f000000    # 0.5f

    add-float/2addr p0, p1

    float-to-int p0, p0

    return p0
.end method

.method public static logotype(Ljava/util/Locale;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 4

    .line 445
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p0

    .line 446
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xca9

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcae

    if-eq v0, v1, :cond_2

    const/16 v1, 0xccc

    if-eq v0, v1, :cond_1

    const/16 v1, 0xd37

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x2

    goto :goto_1

    :cond_1
    const-string v0, "fr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    const-string v0, "es"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x0

    goto :goto_1

    :cond_3
    const-string v0, "en"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x3

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p0, -0x1

    :goto_1
    if-eqz p0, :cond_7

    if-eq p0, v3, :cond_6

    if-eq p0, v2, :cond_5

    .line 455
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 452
    :cond_5
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 450
    :cond_6
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 448
    :cond_7
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static logotypeWorld(Ljava/util/Locale;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 4

    .line 460
    invoke-virtual {p0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object p0

    .line 461
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xca9

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq v0, v1, :cond_3

    const/16 v1, 0xcae

    if-eq v0, v1, :cond_2

    const/16 v1, 0xccc

    if-eq v0, v1, :cond_1

    const/16 v1, 0xd37

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "ja"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x2

    goto :goto_1

    :cond_1
    const-string v0, "fr"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    const-string v0, "es"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x0

    goto :goto_1

    :cond_3
    const-string v0, "en"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 p0, 0x3

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p0, -0x1

    :goto_1
    if-eqz p0, :cond_7

    if-eq p0, v3, :cond_6

    if-eq p0, v2, :cond_5

    .line 470
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_EN_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 467
    :cond_5
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_JA_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 465
    :cond_6
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_FR_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 463
    :cond_7
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOGOTYPE_ES_WORLD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static permission(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 3

    .line 475
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$1;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 485
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 487
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 481
    :cond_2
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 479
    :cond_3
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_LOCATION:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 477
    :cond_4
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static pinCard(Lcom/squareup/Card$Brand;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 432
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 440
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_GENERIC:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 438
    :cond_0
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_AMEX:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 436
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_MASTER_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 434
    :cond_2
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PIN_CARD_VISA:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static smallPermission(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 3

    .line 492
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$1;->$SwitchMap$com$squareup$systempermissions$SystemPermission:[I

    invoke-virtual {p0}, Lcom/squareup/systempermissions/SystemPermission;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 502
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 504
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown permission: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_1
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STORAGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 498
    :cond_2
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->MICROPHONE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 496
    :cond_3
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->LOCATION_CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0

    .line 494
    :cond_4
    sget-object p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CONTACTS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 157
    const-class v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public static values()[Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 157
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->$VALUES:[Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0}, [Lcom/squareup/glyph/GlyphTypeface$Glyph;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method


# virtual methods
.method public getCharacterAsString()Ljava/lang/String;
    .locals 1

    .line 509
    iget-object v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->characterString:Ljava/lang/String;

    return-object v0
.end method

.method public getFixedGlyphHeight()I
    .locals 2

    .line 549
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    .line 550
    :cond_0
    new-instance v0, Ljava/lang/IllegalAccessError;

    const-string v1, "Must call initGlyph() once before getting height!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFixedGlyphWidth()I
    .locals 2

    .line 542
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    .line 543
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call initGlyph() once before getting width!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFixedTextSize()I
    .locals 2

    .line 556
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return v0

    .line 557
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call initGlyph() once before getting text size!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public initGlyph(Landroid/content/res/Resources;)V
    .locals 4

    .line 513
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 514
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    .line 517
    :cond_0
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightId:I

    if-eq v0, v1, :cond_1

    .line 518
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    .line 521
    :cond_1
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightDp:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthDp:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    .line 522
    :cond_2
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 523
    iget v2, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightDp:F

    cmpl-float v3, v2, v1

    if-eqz v3, :cond_3

    .line 524
    invoke-static {v2, v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->dpToPxRounded(FI)I

    move-result v2

    iput v2, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    .line 526
    :cond_3
    iget v2, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthDp:F

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_4

    .line 527
    invoke-static {v2, v0}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->dpToPxRounded(FI)I

    move-result v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    .line 531
    :cond_4
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizeId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    .line 533
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 534
    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_scaling:I

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 535
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result p1

    .line 536
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    int-to-float v0, v0

    mul-float v0, v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->textSizePx:I

    .line 537
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    int-to-float v0, v0

    mul-float v0, v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->widthPx:I

    .line 538
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    int-to-float v0, v0

    mul-float v0, v0, p1

    float-to-int p1, v0

    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->heightPx:I

    return-void
.end method
