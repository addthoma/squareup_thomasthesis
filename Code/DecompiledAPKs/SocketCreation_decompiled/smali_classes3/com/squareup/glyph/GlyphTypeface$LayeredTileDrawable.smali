.class Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "GlyphTypeface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/GlyphTypeface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LayeredTileDrawable"
.end annotation


# instance fields
.field final dimen:I

.field final drawables:[Landroid/graphics/drawable/Drawable;


# direct methods
.method private constructor <init>(I[Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 572
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 573
    iput p1, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->dimen:I

    .line 574
    iput-object p2, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->drawables:[Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method synthetic constructor <init>(I[Landroid/graphics/drawable/Drawable;Lcom/squareup/glyph/GlyphTypeface$1;)V
    .locals 0

    .line 567
    invoke-direct {p0, p1, p2}, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;-><init>(I[Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 586
    iget-object v0, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->drawables:[Landroid/graphics/drawable/Drawable;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 587
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 582
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->dimen:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 578
    iget v0, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->dimen:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 4

    .line 592
    iget-object v0, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->drawables:[Landroid/graphics/drawable/Drawable;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 593
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 595
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 4

    .line 599
    iget-object v0, p0, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->drawables:[Landroid/graphics/drawable/Drawable;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 600
    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 602
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/glyph/GlyphTypeface$LayeredTileDrawable;->invalidateSelf()V

    return-void
.end method
