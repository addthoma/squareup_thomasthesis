.class public final Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;
.super Ljava/lang/Object;
.source "RealMailOrderAnalytics.kt"

# interfaces
.implements Lcom/squareup/mailorder/MailOrderAnalytics;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;",
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "logShippingDetailsContinueClick",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logShippingDetailsContinueClick()V
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/ordermagstripe/RealMailOrderAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->HELP_R4_ORDER_READER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressAlertScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressAlertScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressOriginalSubmitted()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressOriginalSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressRecommendedSubmitted()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressRecommendedSubmitted(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsCorrectedAddressScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsCorrectedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsMissingInfoErrorScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsMissingInfoErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsServerErrorScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsServerErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressErrorScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressProceedClick()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressProceedClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressReenterClick()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressReenterClick(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingDetailsUnverifiedAddressScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingDetailsUnverifiedAddressScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingOrderConfirmedScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithSignatureOnly()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithSignatureOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithStampsAndSignature()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithStampsAndSignature(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingOrderConfirmedWithStampsOnly()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderConfirmedWithStampsOnly(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method

.method public logShippingOrderErrorScreen()V
    .locals 0

    .line 9
    invoke-static {p0}, Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;->logShippingOrderErrorScreen(Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-void
.end method
