.class public final Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;
.super Ljava/lang/Object;
.source "OrderMagstripeServiceHelper.kt"

# interfaces
.implements Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J6\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00082\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J\u001c\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\t0\u00082\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;",
        "Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;",
        "shippingAddressService",
        "Lcom/squareup/server/shipping/ShippingAddressService;",
        "activationService",
        "Lcom/squareup/server/activation/ActivationService;",
        "(Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/server/activation/ActivationService;)V",
        "placeOrder",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        "itemToken",
        "",
        "verifiedAddressToken",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "globalAddress",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "verifyShippingAddress",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "body",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "verifyShippingAddressServiceError",
        "Lcom/squareup/widgets/warning/Warning;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field private final shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/server/activation/ActivationService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "shippingAddressService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activationService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;->shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;

    iput-object p2, p0, Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;->activationService:Lcom/squareup/server/activation/ActivationService;

    return-void
.end method


# virtual methods
.method public placeOrder(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/mailorder/ContactInfo;",
            "Lcom/squareup/protos/common/location/GlobalAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p1, "verifiedAddressToken"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "contactInfo"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 45
    invoke-virtual {p3}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p4}, Lcom/squareup/server/shipping/ShippingBody;->from(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/server/shipping/ShippingBody;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/server/activation/ActivationService;->createShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public placeOrderServerError()Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 20
    invoke-static {p0}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints$DefaultImpls;->placeOrderServerError(Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)Lcom/squareup/widgets/warning/Warning;

    move-result-object v0

    return-object v0
.end method

.method public verifyShippingAddress(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ordermagstripe/OrderMagstripeServiceHelper;->shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;

    invoke-interface {v0, p1}, Lcom/squareup/server/shipping/ShippingAddressService;->verify(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 35
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public verifyShippingAddressServerError()Lcom/squareup/widgets/warning/Warning;
    .locals 1

    .line 20
    invoke-static {p0}, Lcom/squareup/mailorder/OrderServiceHelper$Endpoints$DefaultImpls;->verifyShippingAddressServerError(Lcom/squareup/mailorder/OrderServiceHelper$Endpoints;)Lcom/squareup/widgets/warning/Warning;

    move-result-object v0

    return-object v0
.end method

.method public verifyShippingAddressServiceError()Lcom/squareup/widgets/warning/Warning;
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 27
    sget v1, Lcom/squareup/common/strings/R$string;->order_reader_magstripe_validation_system_error_title:I

    .line 28
    sget v2, Lcom/squareup/common/strings/R$string;->order_reader_magstripe_validation_system_error_text:I

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    return-object v0
.end method
