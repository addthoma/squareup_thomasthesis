.class public final Lcom/squareup/mailorder/OrderReactor;
.super Ljava/lang/Object;
.source "OrderReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/Reactor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$Configuration;,
        Lcom/squareup/mailorder/OrderReactor$OrderState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/Reactor<",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "Lcom/squareup/mailorder/OrderEvent;",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderReactor.kt\ncom/squareup/mailorder/OrderReactor\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,504:1\n32#2,12:505\n*E\n*S KotlinDebug\n*F\n+ 1 OrderReactor.kt\ncom/squareup/mailorder/OrderReactor\n*L\n81#1,12:505\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u000234B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ(\u0010\u000c\u001a\u00020\u00022\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u001bH\u0002J2\u0010\u001c\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u001e0\u001d2\u0006\u0010\u0017\u001a\u00020\u00022\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00030 H\u0016J8\u0010!\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u00162\u0006\u0010#\u001a\u00020$2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u001c\u0010\'\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020(0\u001d2\u0006\u0010\u0017\u001a\u00020)H\u0002J$\u0010*\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040+j\u0002`,2\u0006\u0010-\u001a\u00020.J$\u0010*\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040+j\u0002`,2\u0006\u0010/\u001a\u000200J\u001c\u00101\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020(0\u001d2\u0006\u0010\u0017\u001a\u000202H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor;",
        "Lcom/squareup/workflow/rx1/Reactor;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "Lcom/squareup/mailorder/OrderEvent;",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "orderServiceHelper",
        "Lcom/squareup/mailorder/OrderServiceHelper;",
        "configuration",
        "Lcom/squareup/mailorder/OrderReactor$Configuration;",
        "analytics",
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)V",
        "determineOnSendOrderState",
        "event",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;",
        "orderToken",
        "",
        "shippingToken",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "fetchContactInfo",
        "Lio/reactivex/Single;",
        "Lcom/squareup/mailorder/ContactInfo;",
        "state",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;",
        "logAddressError",
        "",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;",
        "onReact",
        "Lrx/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "events",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "sendAddressRequest",
        "contactInfo",
        "address",
        "Lcom/squareup/address/Address;",
        "originationFlow",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;",
        "sendOrder",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;",
        "startWorkflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/mailorder/OrderWorkflow;",
        "input",
        "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "verifyAddress",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;",
        "Configuration",
        "OrderState",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

.field private final configuration:Lcom/squareup/mailorder/OrderReactor$Configuration;

.field private final orderServiceHelper:Lcom/squareup/mailorder/OrderServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->orderServiceHelper:Lcom/squareup/mailorder/OrderServiceHelper;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor;->configuration:Lcom/squareup/mailorder/OrderReactor$Configuration;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    return-void
.end method

.method public static final synthetic access$determineOnSendOrderState(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;
    .locals 0

    .line 63
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/mailorder/OrderReactor;->determineOnSendOrderState(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fetchContactInfo(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;)Lio/reactivex/Single;
    .locals 0

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderReactor;->fetchContactInfo(Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getConfiguration$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/mailorder/OrderReactor;->configuration:Lcom/squareup/mailorder/OrderReactor$Configuration;

    return-object p0
.end method

.method public static final synthetic access$sendAddressRequest(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;
    .locals 0

    .line 63
    invoke-direct/range {p0 .. p6}, Lcom/squareup/mailorder/OrderReactor;->sendAddressRequest(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object p0

    return-object p0
.end method

.method private final determineOnSendOrderState(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;
    .locals 7

    .line 463
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->configuration:Lcom/squareup/mailorder/OrderReactor$Configuration;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$Configuration;->getVerifyAddressBeforeSendingOrder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    new-instance p3, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {p3, p2, p4, p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)V

    check-cast p3, Lcom/squareup/mailorder/OrderReactor$OrderState;

    goto :goto_0

    .line 467
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    .line 468
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    .line 471
    sget-object v5, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;->ENTER_ADDRESS:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    .line 466
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderReactor;->sendAddressRequest(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object p3

    :goto_0
    return-object p3
.end method

.method private final fetchContactInfo(Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/mailorder/ContactInfo;",
            ">;"
        }
    .end annotation

    .line 361
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 362
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "io.reactivex.Single.just(state.contactInfo)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 364
    :cond_1
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->orderServiceHelper:Lcom/squareup/mailorder/OrderServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderServiceHelper;->getMerchantProfile$mail_order_release()Lio/reactivex/Single;

    move-result-object p1

    .line 365
    sget-object v0, Lcom/squareup/mailorder/OrderReactor$fetchContactInfo$1;->INSTANCE:Lcom/squareup/mailorder/OrderReactor$fetchContactInfo$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "orderServiceHelper.getMe\u2026esponse.toContactInfo() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method private final logAddressError(Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;)V
    .locals 1

    .line 497
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsServerErrorScreen()V

    goto :goto_0

    .line 498
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsCorrectedAddressAlertScreen()V

    goto :goto_0

    .line 499
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsUnverifiedAddressErrorScreen()V

    goto :goto_0

    .line 500
    :cond_2
    instance-of p1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingOrderErrorScreen()V

    :cond_3
    :goto_0
    return-void
.end method

.method private final sendAddressRequest(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;
    .locals 8

    .line 485
    new-instance v7, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    .line 490
    iget-object v0, p2, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p2, v0}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v5

    move-object v0, v7

    move-object v1, p3

    move-object v2, p6

    move-object v3, p4

    move-object v4, p1

    move-object v6, p5

    .line 485
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;)V

    check-cast v7, Lcom/squareup/mailorder/OrderReactor$OrderState;

    return-object v7
.end method

.method private final sendOrder(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;)Lrx/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;>;"
        }
    .end annotation

    .line 438
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->orderServiceHelper:Lcom/squareup/mailorder/OrderServiceHelper;

    .line 439
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getShippingToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getGlobalAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v4

    .line 438
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/mailorder/OrderServiceHelper;->sendShippingAddress$mail_order_release(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/protos/common/location/GlobalAddress;)Lio/reactivex/Single;

    move-result-object v0

    .line 441
    new-instance v1, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;

    invoke-direct {v1, p1}, Lcom/squareup/mailorder/OrderReactor$sendOrder$1;-><init>(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "orderServiceHelper.sendS\u2026  )\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/SingleSource;

    .line 454
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method private final verifyAddress(Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;>;"
        }
    .end annotation

    .line 370
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->orderServiceHelper:Lcom/squareup/mailorder/OrderServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mailorder/OrderServiceHelper;->verifyShippingAddress$mail_order_release(Lcom/squareup/mailorder/ContactInfo;)Lio/reactivex/Single;

    move-result-object v0

    .line 371
    new-instance v1, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/mailorder/OrderReactor$verifyAddress$1;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "orderServiceHelper.verif\u2026tate(nextState)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/SingleSource;

    .line 434
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public onAbandoned(Lcom/squareup/mailorder/OrderReactor$OrderState;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {p0, p1}, Lcom/squareup/workflow/rx1/Reactor$DefaultImpls;->onAbandoned(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic onAbandoned(Ljava/lang/Object;)V
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor;->onAbandoned(Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    return-void
.end method

.method public onReact(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/workflow/rx1/EventChannel<",
            "Lcom/squareup/mailorder/OrderEvent;",
            ">;)",
            "Lrx/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$1;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 231
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;

    if-eqz v0, :cond_1

    .line 232
    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;

    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor;->logAddressError(Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;)V

    .line 233
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$2;

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$2;-><init>(Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 245
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {v0}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsScreen()V

    .line 247
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$3;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 278
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderReactor;->verifyAddress(Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 280
    :cond_3
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    if-eqz v0, :cond_4

    .line 281
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {v0}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsCorrectedAddressScreen()V

    .line 282
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$4;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 312
    :cond_4
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    if-eqz v0, :cond_5

    .line 313
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor;->analytics:Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-interface {v0}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsUnverifiedAddressScreen()V

    .line 314
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$5;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 341
    :cond_5
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderReactor;->sendOrder(Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;)Lrx/Single;

    move-result-object p1

    goto :goto_0

    .line 343
    :cond_6
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$onReact$6;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$6;-><init>(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderReactor$OrderState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/rx1/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lrx/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mailorder/OrderReactor;->onReact(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/rx1/EventChannel;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/mailorder/OrderEvent;",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    .line 73
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderWorkflowStartArgument;->getItemToken()Ljava/lang/String;

    move-result-object v1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderWorkflowStartArgument;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v2

    .line 75
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderWorkflowStartArgument;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p1

    .line 72
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)V

    .line 71
    invoke-static {p0, v0}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public final startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/mailorder/OrderEvent;",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 505
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v2

    :goto_1
    if-eqz p1, :cond_3

    .line 510
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 511
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 512
    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 513
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 514
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 515
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    :cond_3
    if-eqz v2, :cond_4

    .line 80
    invoke-static {p0, v2}, Lcom/squareup/workflow/rx1/ReactorKt;->startWorkflow(Lcom/squareup/workflow/rx1/Reactor;Ljava/lang/Object;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1

    .line 81
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
