.class public interface abstract Lcom/squareup/mailorder/OrderScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "OrderScreenWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001JD\u0010\u0002\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050\u0004\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0003j\u0002`\u000b2\u0006\u0010\u000c\u001a\u00020\rH&JD\u0010\u0002\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050\u0004\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0003j\u0002`\u000b2\u0006\u0010\u000e\u001a\u00020\u000fH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderScreenWorkflowStarter;",
        "",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "Lcom/squareup/mailorder/OrderScreenWorkflow;",
        "startArg",
        "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract start(Lcom/squareup/mailorder/OrderWorkflowStartArgument;)Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderWorkflowStartArgument;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation
.end method
