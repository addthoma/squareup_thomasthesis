.class public interface abstract Lcom/squareup/mailorder/MailOrderAnalytics;
.super Ljava/lang/Object;
.source "MailOrderAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/MailOrderAnalytics$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0011\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016J\u0008\u0010\u0005\u001a\u00020\u0003H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016J\u0008\u0010\u0007\u001a\u00020\u0003H\u0016J\u0008\u0010\u0008\u001a\u00020\u0003H\u0016J\u0008\u0010\t\u001a\u00020\u0003H\u0016J\u0008\u0010\n\u001a\u00020\u0003H\u0016J\u0008\u0010\u000b\u001a\u00020\u0003H\u0016J\u0008\u0010\u000c\u001a\u00020\u0003H\u0016J\u0008\u0010\r\u001a\u00020\u0003H\u0016J\u0008\u0010\u000e\u001a\u00020\u0003H\u0016J\u0008\u0010\u000f\u001a\u00020\u0003H\u0016J\u0008\u0010\u0010\u001a\u00020\u0003H\u0016J\u0008\u0010\u0011\u001a\u00020\u0003H\u0016J\u0008\u0010\u0012\u001a\u00020\u0003H\u0016J\u0008\u0010\u0013\u001a\u00020\u0003H\u0016\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/mailorder/MailOrderAnalytics;",
        "",
        "logShippingDetailsContinueClick",
        "",
        "logShippingDetailsCorrectedAddressAlertScreen",
        "logShippingDetailsCorrectedAddressOriginalSubmitted",
        "logShippingDetailsCorrectedAddressRecommendedSubmitted",
        "logShippingDetailsCorrectedAddressScreen",
        "logShippingDetailsMissingInfoErrorScreen",
        "logShippingDetailsScreen",
        "logShippingDetailsServerErrorScreen",
        "logShippingDetailsUnverifiedAddressErrorScreen",
        "logShippingDetailsUnverifiedAddressProceedClick",
        "logShippingDetailsUnverifiedAddressReenterClick",
        "logShippingDetailsUnverifiedAddressScreen",
        "logShippingOrderConfirmedScreen",
        "logShippingOrderConfirmedWithSignatureOnly",
        "logShippingOrderConfirmedWithStampsAndSignature",
        "logShippingOrderConfirmedWithStampsOnly",
        "logShippingOrderErrorScreen",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logShippingDetailsContinueClick()V
.end method

.method public abstract logShippingDetailsCorrectedAddressAlertScreen()V
.end method

.method public abstract logShippingDetailsCorrectedAddressOriginalSubmitted()V
.end method

.method public abstract logShippingDetailsCorrectedAddressRecommendedSubmitted()V
.end method

.method public abstract logShippingDetailsCorrectedAddressScreen()V
.end method

.method public abstract logShippingDetailsMissingInfoErrorScreen()V
.end method

.method public abstract logShippingDetailsScreen()V
.end method

.method public abstract logShippingDetailsServerErrorScreen()V
.end method

.method public abstract logShippingDetailsUnverifiedAddressErrorScreen()V
.end method

.method public abstract logShippingDetailsUnverifiedAddressProceedClick()V
.end method

.method public abstract logShippingDetailsUnverifiedAddressReenterClick()V
.end method

.method public abstract logShippingDetailsUnverifiedAddressScreen()V
.end method

.method public abstract logShippingOrderConfirmedScreen()V
.end method

.method public abstract logShippingOrderConfirmedWithSignatureOnly()V
.end method

.method public abstract logShippingOrderConfirmedWithStampsAndSignature()V
.end method

.method public abstract logShippingOrderConfirmedWithStampsOnly()V
.end method

.method public abstract logShippingOrderErrorScreen()V
.end method
