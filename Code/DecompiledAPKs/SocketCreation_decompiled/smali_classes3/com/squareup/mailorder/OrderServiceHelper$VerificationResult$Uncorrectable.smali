.class public final Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;
.super Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Uncorrectable"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\t\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "res",
        "Landroid/content/res/Resources;",
        "token",
        "",
        "(Landroid/content/res/Resources;Ljava/lang/String;)V",
        "message",
        "getMessage",
        "()Ljava/lang/String;",
        "title",
        "getTitle",
        "getToken",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;

.field private final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "token"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 133
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->token:Ljava/lang/String;

    .line 135
    sget p2, Lcom/squareup/common/strings/R$string;->order_validation_uncorrected_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "res.getString(com.square\u2026dation_uncorrected_title)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->title:Ljava/lang/String;

    .line 137
    sget p2, Lcom/squareup/common/strings/R$string;->order_validation_uncorrected_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "res.getString(com.square\u2026idation_uncorrected_text)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$Uncorrectable;->token:Ljava/lang/String;

    return-object v0
.end method
