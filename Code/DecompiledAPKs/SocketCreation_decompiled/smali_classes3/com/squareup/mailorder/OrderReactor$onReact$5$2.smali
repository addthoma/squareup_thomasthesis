.class final Lcom/squareup/mailorder/OrderReactor$onReact$5$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$5;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "it",
        "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$5;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$5;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsUnverifiedAddressProceedClick()V

    .line 327
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 328
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    iget-object v2, v1, Lcom/squareup/mailorder/OrderReactor$onReact$5;->this$0:Lcom/squareup/mailorder/OrderReactor;

    .line 329
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v3

    .line 330
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    iget-object v1, v1, Lcom/squareup/mailorder/OrderReactor$onReact$5;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v1}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v8

    .line 331
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;->getAddress()Lcom/squareup/address/Address;

    move-result-object v4

    .line 332
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$5;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getItemToken()Ljava/lang/String;

    move-result-object v5

    .line 333
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$5;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$5;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;->getVerifiedAddressToken()Ljava/lang/String;

    move-result-object v6

    .line 334
    sget-object v7, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;->CONFIRM_UNVERIFIED:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    .line 328
    invoke-static/range {v2 .. v8}, Lcom/squareup/mailorder/OrderReactor;->access$sendAddressRequest(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object p1

    .line 327
    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$5$2;->invoke(Lcom/squareup/mailorder/OrderEvent$UnverifiedAddressEvent$SubmitUnverifiedAddress;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
