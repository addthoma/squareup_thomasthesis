.class public final Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;
.super Ljava/lang/Object;
.source "OrderWorkflowViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u00020\u000c2\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
        "",
        "orderCoordinatorFactory",
        "Lcom/squareup/mailorder/OrderCoordinator$Factory;",
        "correctedAddressCoordinatorFactory",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;",
        "unverifiedAddressCoordinator",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
        "orderConfirmationCoordinator",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;",
        "(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)V",
        "create",
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final correctedAddressCoordinatorFactory:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

.field private final orderConfirmationCoordinator:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

.field private final orderCoordinatorFactory:Lcom/squareup/mailorder/OrderCoordinator$Factory;

.field private final unverifiedAddressCoordinator:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderCoordinatorFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "correctedAddressCoordinatorFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unverifiedAddressCoordinator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderConfirmationCoordinator"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->orderCoordinatorFactory:Lcom/squareup/mailorder/OrderCoordinator$Factory;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->correctedAddressCoordinatorFactory:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->unverifiedAddressCoordinator:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->orderConfirmationCoordinator:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Ljava/lang/Class;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderWorkflowViewFactory;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 56
    check-cast p1, Ljava/lang/Class;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/mailorder/OrderWorkflowViewFactory;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Ljava/lang/Class;)Lcom/squareup/mailorder/OrderWorkflowViewFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/mailorder/OrderWorkflowViewFactory;

    .line 57
    iget-object v1, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->orderCoordinatorFactory:Lcom/squareup/mailorder/OrderCoordinator$Factory;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->correctedAddressCoordinatorFactory:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;

    .line 58
    iget-object v3, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->unverifiedAddressCoordinator:Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->orderConfirmationCoordinator:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;

    const/4 v6, 0x0

    move-object v0, v7

    move-object v5, p1

    .line 56
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderWorkflowViewFactory;-><init>(Lcom/squareup/mailorder/OrderCoordinator$Factory;Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Factory;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;Ljava/lang/Class;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method
