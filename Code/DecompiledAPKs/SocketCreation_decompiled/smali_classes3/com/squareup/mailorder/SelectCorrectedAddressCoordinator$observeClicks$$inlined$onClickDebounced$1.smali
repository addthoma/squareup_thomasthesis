.class public final Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->observeClicks(Lcom/squareup/workflow/legacy/Screen;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 SelectCorrectedAddressCoordinator.kt\ncom/squareup/mailorder/SelectCorrectedAddressCoordinator\n*L\n1#1,1322:1\n131#2,14:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/workflow/legacy/Screen;

.field final synthetic $selectedId$inlined:I

.field final synthetic this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    iput-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iput p3, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$selectedId$inlined:I

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 8

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    instance-of v0, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    if-eqz p1, :cond_3

    .line 1325
    iget v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$selectedId$inlined:I

    .line 1326
    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    invoke-static {v1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$getAddressOriginal$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)Lcom/squareup/mailorder/SelectableAddressLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/mailorder/SelectableAddressLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getOriginalAddress()Lcom/squareup/address/Address;

    move-result-object v0

    :goto_0
    move-object v4, v0

    goto :goto_1

    .line 1327
    :cond_1
    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    invoke-static {v1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$getAddressSuggested$p(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)Lcom/squareup/mailorder/SelectableAddressLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/mailorder/SelectableAddressLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getCorrectedAddress()Lcom/squareup/address/Address;

    move-result-object v0

    goto :goto_0

    .line 1332
    :goto_1
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$screen$inlined:Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, v0, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1333
    new-instance v7, Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/mailorder/ContactInfo;->copy$default(Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/Address;ILjava/lang/Object;)Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v7, p1}, Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    .line 1332
    invoke-interface {v0, v7}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_2

    .line 1328
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Address with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$observeClicks$$inlined$onClickDebounced$1;->$selectedId$inlined:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " does not exist."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    :goto_2
    return-void
.end method
