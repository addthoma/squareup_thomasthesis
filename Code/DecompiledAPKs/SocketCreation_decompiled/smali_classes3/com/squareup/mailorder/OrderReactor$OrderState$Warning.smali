.class public abstract Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;
.super Lcom/squareup/mailorder/OrderReactor$OrderState;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor$OrderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Warning"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\u0082\u0001\u0005\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "()V",
        "message",
        "",
        "getMessage",
        "()Ljava/lang/String;",
        "title",
        "getTitle",
        "CorrectedAddress",
        "MissingFieldsError",
        "OrderError",
        "UncorrectableAddress",
        "VerificationError",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 165
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 165
    invoke-direct {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getMessage()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method
