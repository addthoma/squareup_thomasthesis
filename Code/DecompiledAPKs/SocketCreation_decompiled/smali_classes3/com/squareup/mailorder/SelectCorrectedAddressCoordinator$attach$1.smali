.class final Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;
.super Ljava/lang/Object;
.source "SelectCorrectedAddressCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        ">;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        "Lcom/squareup/mailorder/SelectCorrectedAddressScreen;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;)",
            "Lcom/squareup/register/widgets/GlassSpinnerState;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;

    invoke-static {v0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$spinnerData(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$1;->call(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
