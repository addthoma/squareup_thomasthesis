.class public final Lcom/squareup/mailorder/SelectableAddressLayout;
.super Landroid/widget/FrameLayout;
.source "SelectableAddressLayout.kt"

# interfaces
.implements Lcom/squareup/noho/ListenableCheckable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/squareup/noho/ListenableCheckable<",
        "Lcom/squareup/mailorder/SelectableAddressLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectableAddressLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectableAddressLayout.kt\ncom/squareup/mailorder/SelectableAddressLayout\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,137:1\n1642#2,2:138\n*E\n*S KotlinDebug\n*F\n+ 1 SelectableAddressLayout.kt\ncom/squareup/mailorder/SelectableAddressLayout\n*L\n103#1,2:138\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00000\u0002B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u001e\u001a\u00020\u000bH\u0016J\u000e\u0010\u001f\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020\u000bJ\u0008\u0010 \u001a\u00020\u0017H\u0014J\u0008\u0010!\u001a\u00020\u0017H\u0014J\u000e\u0010\"\u001a\u00020\u00172\u0006\u0010#\u001a\u00020$J\u0010\u0010%\u001a\u00020\u00172\u0006\u0010&\u001a\u00020\'H\u0002J\u000e\u0010(\u001a\u00020\u00172\u0006\u0010)\u001a\u00020\'J\u0010\u0010*\u001a\u00020\u00172\u0006\u0010+\u001a\u00020\u000bH\u0016J\u0010\u0010,\u001a\u00020\u00172\u0006\u0010-\u001a\u00020\u000bH\u0016J\u0008\u0010.\u001a\u00020\u0017H\u0016J\u0010\u0010/\u001a\u00020\u00172\u0006\u0010+\u001a\u00020\u000bH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \u000e*\u0004\u0018\u00010\r0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u000e*\u0004\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n \u000e*\u0004\u0018\u00010\u00120\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n \u000e*\u0004\u0018\u00010\u00120\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R:\u0010\u0015\u001a\"\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016j\n\u0012\u0004\u0012\u00020\u0000\u0018\u0001`\u0018X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\n \u000e*\u0004\u0018\u00010\u00100\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/mailorder/SelectableAddressLayout;",
        "Landroid/widget/FrameLayout;",
        "Lcom/squareup/noho/ListenableCheckable;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "_selected",
        "",
        "checkbox",
        "Lcom/squareup/marketfont/MarketRadioButton;",
        "kotlin.jvm.PlatformType",
        "container",
        "Landroid/view/ViewGroup;",
        "isRecommendedText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "isSelectable",
        "nameText",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "getOnCheckedChange",
        "()Lkotlin/jvm/functions/Function2;",
        "setOnCheckedChange",
        "(Lkotlin/jvm/functions/Function2;)V",
        "textContainer",
        "isChecked",
        "isRecommended",
        "onAttachedToWindow",
        "onDetachedFromWindow",
        "populateAddress",
        "address",
        "Lcom/squareup/address/Address;",
        "populateLine",
        "line",
        "",
        "populateName",
        "name",
        "setChecked",
        "checked",
        "setSelected",
        "selected",
        "toggle",
        "update",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private _selected:Z

.field private final checkbox:Lcom/squareup/marketfont/MarketRadioButton;

.field private final container:Landroid/view/ViewGroup;

.field private final isRecommendedText:Lcom/squareup/marketfont/MarketTextView;

.field private isSelectable:Z

.field private final nameText:Lcom/squareup/marketfont/MarketTextView;

.field private onCheckedChange:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mailorder/SelectableAddressLayout;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final textContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/SelectableAddressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/mailorder/SelectableAddressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x1

    .line 44
    iput-boolean p3, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isSelectable:Z

    .line 47
    sget v0, Lcom/squareup/mailorder/R$layout;->order_select_address_layout:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 49
    sget-object v0, Lcom/squareup/mailorder/R$styleable;->SelectableAddressLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 51
    :try_start_0
    sget p2, Lcom/squareup/mailorder/R$styleable;->SelectableAddressLayout_sqSelected:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    invoke-virtual {p0, p2}, Lcom/squareup/mailorder/SelectableAddressLayout;->setSelected(Z)V

    .line 52
    sget p2, Lcom/squareup/mailorder/R$styleable;->SelectableAddressLayout_sqSelectable:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isSelectable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    iget-boolean p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isSelectable:Z

    if-eqz p1, :cond_0

    .line 58
    new-instance p1, Lcom/squareup/mailorder/SelectableAddressLayout$1;

    invoke-direct {p1, p0}, Lcom/squareup/mailorder/SelectableAddressLayout$1;-><init>(Lcom/squareup/mailorder/SelectableAddressLayout;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    :cond_0
    sget p1, Lcom/squareup/mailorder/R$id;->address_selector_radio_button:I

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 65
    check-cast p1, Lcom/squareup/marketfont/MarketRadioButton;

    .line 67
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketRadioButton;->isSelected()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketRadioButton;->setChecked(Z)V

    .line 68
    move-object p2, p1

    check-cast p2, Landroid/view/View;

    iget-boolean p3, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isSelectable:Z

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 65
    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->checkbox:Lcom/squareup/marketfont/MarketRadioButton;

    .line 70
    sget p1, Lcom/squareup/mailorder/R$id;->address_selector_name:I

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->nameText:Lcom/squareup/marketfont/MarketTextView;

    .line 71
    sget p1, Lcom/squareup/mailorder/R$id;->address_selector_container:I

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->container:Landroid/view/ViewGroup;

    .line 72
    sget p1, Lcom/squareup/mailorder/R$id;->address_selector_text_container:I

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->textContainer:Landroid/view/ViewGroup;

    .line 73
    sget p1, Lcom/squareup/mailorder/R$id;->address_selector_is_recommended:I

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isRecommendedText:Lcom/squareup/marketfont/MarketTextView;

    return-void

    :catchall_0
    move-exception p2

    .line 54
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 39
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 40
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mailorder/SelectableAddressLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/mailorder/SelectableAddressLayout;Z)V
    .locals 0

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->update(Z)V

    return-void
.end method

.method private final populateLine(Ljava/lang/String;)V
    .locals 4

    .line 119
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->textContainer:Landroid/view/ViewGroup;

    .line 120
    new-instance v1, Lcom/squareup/marketfont/MarketTextView;

    const-string v2, "textContainer"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 122
    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/noho/R$style;->TextAppearance_Noho_Body:I

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 123
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    check-cast v1, Landroid/view/View;

    .line 119
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final update(Z)V
    .locals 1

    .line 129
    iget-boolean v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->_selected:Z

    if-eq v0, p1, :cond_2

    .line 130
    iput-boolean p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->_selected:Z

    .line 131
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->checkbox:Lcom/squareup/marketfont/MarketRadioButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketRadioButton;->setChecked(Z)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->container:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setSelected(Z)V

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/mailorder/SelectableAddressLayout;->getOnCheckedChange()Lkotlin/jvm/functions/Function2;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_2
    return-void
.end method


# virtual methods
.method public getOnCheckedChange()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/mailorder/SelectableAddressLayout;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->onCheckedChange:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->_selected:Z

    return v0
.end method

.method public final isRecommended(Z)V
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->isRecommendedText:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "isRecommendedText"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 76
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 77
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->checkbox:Lcom/squareup/marketfont/MarketRadioButton;

    new-instance v1, Lcom/squareup/mailorder/SelectableAddressLayout$onAttachedToWindow$1;

    invoke-direct {v1, p0}, Lcom/squareup/mailorder/SelectableAddressLayout$onAttachedToWindow$1;-><init>(Lcom/squareup/mailorder/SelectableAddressLayout;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public onCheckedChange(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mailorder/SelectableAddressLayout;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {p0, p1}, Lcom/squareup/noho/ListenableCheckable$DefaultImpls;->onCheckedChange(Lcom/squareup/noho/ListenableCheckable;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->checkbox:Lcom/squareup/marketfont/MarketRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketRadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 82
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public final populateAddress(Lcom/squareup/address/Address;)V
    .locals 3

    const-string v0, "address"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->textContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 102
    invoke-static {p1}, Lcom/squareup/address/AddressFormatter;->formatForShippingDisplay(Lcom/squareup/address/Address;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 138
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, p0

    check-cast v2, Lcom/squareup/mailorder/SelectableAddressLayout;

    .line 103
    invoke-direct {v2, v1}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateLine(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    if-eqz v0, :cond_1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/mailorder/SelectableAddressLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/address/Address;->country:Lcom/squareup/CountryCode;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->countryName(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(Countr\u2026tryName(address.country))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->populateLine(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final populateName(Ljava/lang/String;)V
    .locals 2

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->nameText:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "nameText"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->update(Z)V

    return-void
.end method

.method public setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/mailorder/SelectableAddressLayout;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 91
    iput-object p1, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->onCheckedChange:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .line 86
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setSelected(Z)V

    .line 88
    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectableAddressLayout;->setChecked(Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/squareup/mailorder/SelectableAddressLayout;->_selected:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/squareup/mailorder/SelectableAddressLayout;->update(Z)V

    return-void
.end method
