.class final Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;
.super Ljava/lang/Object;
.source "OrderConfirmationCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderConfirmationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderConfirmationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderConfirmationCoordinator.kt\ncom/squareup/mailorder/OrderConfirmationCoordinator$attach$4\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,101:1\n1103#2,7:102\n*E\n*S KotlinDebug\n*F\n+ 1 OrderConfirmationCoordinator.kt\ncom/squareup/mailorder/OrderConfirmationCoordinator$attach$4\n*L\n70#1,7:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        "Lcom/squareup/mailorder/OrderConfirmationScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;->this$0:Lcom/squareup/mailorder/OrderConfirmationCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
            ">;)V"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;->this$0:Lcom/squareup/mailorder/OrderConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->access$getOrderDoneButton$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)Landroid/view/View;

    move-result-object v0

    .line 102
    new-instance v1, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;->getShowEmailConfirmation()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;->this$0:Lcom/squareup/mailorder/OrderConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->access$getOrderGlyphMessage$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;->this$0:Lcom/squareup/mailorder/OrderConfirmationCoordinator;

    invoke-static {v1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->access$getOrderGlyphMessage$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget v2, Lcom/squareup/mailorder/R$string;->order_email_confirmation:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 75
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;->getEmail()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "email"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 73
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
