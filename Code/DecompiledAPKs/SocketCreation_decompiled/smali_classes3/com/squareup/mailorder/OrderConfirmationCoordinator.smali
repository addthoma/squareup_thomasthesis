.class public final Lcom/squareup/mailorder/OrderConfirmationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderConfirmationCoordinator$Factory;,
        Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002 !B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0010H\u0016J\u0010\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0010H\u0002J=\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u00192!\u0010\u001a\u001a\u001d\u0012\u0013\u0012\u00110\u001c\u00a2\u0006\u000c\u0008\u001d\u0012\u0008\u0008\u001e\u0012\u0004\u0008\u0008(\u001f\u0012\u0004\u0012\u00020\u00140\u001bH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
        "Lcom/squareup/mailorder/OrderConfirmationScreen;",
        "configuration",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
        "(Lio/reactivex/Observable;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V",
        "orderConfirmedHelp",
        "Lcom/squareup/marketfont/MarketTextView;",
        "orderConfirmedSubmessage",
        "Lcom/squareup/widgets/MessageView;",
        "orderDoneButton",
        "Landroid/view/View;",
        "orderGlyphMessage",
        "Lcom/squareup/marin/widgets/MarinGlyphMessage;",
        "attach",
        "",
        "view",
        "bindViews",
        "updateTextHoldingView",
        "resId",
        "",
        "textSetter",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "chars",
        "Configuration",
        "Factory",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

.field private orderConfirmedHelp:Lcom/squareup/marketfont/MarketTextView;

.field private orderConfirmedSubmessage:Lcom/squareup/widgets/MessageView;

.field private orderDoneButton:Landroid/view/View;

.field private orderGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$ConfirmationDoneClicked;",
            ">;>;",
            "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
            ")V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;)V

    return-void
.end method

.method public static final synthetic access$getOrderDoneButton$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)Landroid/view/View;
    .locals 1

    .line 21
    iget-object p0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderDoneButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "orderDoneButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOrderGlyphMessage$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;
    .locals 1

    .line 21
    iget-object p0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    if-nez p0, :cond_0

    const-string v0, "orderGlyphMessage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setOrderDoneButton$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;Landroid/view/View;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderDoneButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setOrderGlyphMessage$p(Lcom/squareup/mailorder/OrderConfirmationCoordinator;Lcom/squareup/marin/widgets/MarinGlyphMessage;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 95
    sget v0, Lcom/squareup/mailorder/R$id;->order_done:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderDoneButton:Landroid/view/View;

    .line 96
    sget v0, Lcom/squareup/mailorder/R$id;->order_glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderGlyphMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 97
    sget v0, Lcom/squareup/mailorder/R$id;->order_confirmed_submessage:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedSubmessage:Lcom/squareup/widgets/MessageView;

    .line 98
    sget v0, Lcom/squareup/mailorder/R$id;->order_confirmed_help:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedHelp:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final updateTextHoldingView(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/CharSequence;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 88
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    const-string/jumbo p2, "view.resources.getText(resId)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p3, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->bindViews(Landroid/view/View;)V

    .line 56
    sget-object v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$1;->INSTANCE:Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedSubmessage:Lcom/squareup/widgets/MessageView;

    const-string v1, "orderConfirmedSubmessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->getOrderConfirmedSubmessage()I

    move-result v2

    .line 60
    new-instance v3, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$2;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedSubmessage:Lcom/squareup/widgets/MessageView;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {v3, v4}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$2;-><init>(Lcom/squareup/widgets/MessageView;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 58
    invoke-direct {p0, v0, v2, v3}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->updateTextHoldingView(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedHelp:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "orderConfirmedHelp"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->configuration:Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;->getOrderConfirmedHelpMessage()I

    move-result v2

    new-instance v3, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$3;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->orderConfirmedHelp:Lcom/squareup/marketfont/MarketTextView;

    if-nez v4, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {v3, v4}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$3;-><init>(Lcom/squareup/marketfont/MarketTextView;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 63
    invoke-direct {p0, v0, v2, v3}, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->updateTextHoldingView(Landroid/view/View;ILkotlin/jvm/functions/Function1;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/mailorder/OrderConfirmationCoordinator;->screens:Lio/reactivex/Observable;

    .line 68
    new-instance v1, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$attach$4;-><init>(Lcom/squareup/mailorder/OrderConfirmationCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscri\u2026t()\n          )\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
