.class public final Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;
.super Lcom/squareup/container/SimpleWorkflowRunner;
.source "RealActivateEGiftCardWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/SimpleWorkflowRunner<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        ">;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u00020\u0004B\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;",
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "viewFactory",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
        "starter",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;)V",
        "start",
        "",
        "eGiftCardConfig",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "starter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    const-class v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "ActivateEGiftCardWorkflowRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 15
    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    .line 16
    move-object v5, p3

    check-cast v5, Lcom/squareup/container/PosWorkflowStarter;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    .line 12
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/egiftcard/activation/EGiftCardConfig;)V
    .locals 1

    const-string v0, "eGiftCardConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;->ensureWorkflow()V

    .line 21
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$InitEvent;

    invoke-direct {v0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$InitEvent;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;)V

    invoke-virtual {p0, v0}, Lcom/squareup/egiftcard/activation/RealActivateEGiftCardWorkflowRunner;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
