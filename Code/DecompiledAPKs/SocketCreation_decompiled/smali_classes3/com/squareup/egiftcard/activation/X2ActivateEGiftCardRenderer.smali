.class public final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\r\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJH\u0010\u000c\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\r\u001a\u00020\u00022\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "buyerActivatingScreenDataRenderer",
        "Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;",
        "(Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;)V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerActivatingScreenDataRenderer:Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;)V
    .locals 1

    const-string v0, "buyerActivatingScreenDataRenderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;->buyerActivatingScreenDataRenderer:Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;->render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    if-eqz p3, :cond_0

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialogScreenKt;->ActivatingErrorSellerDialogScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    sget-object p2, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 88
    :cond_0
    iget-object p3, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardRenderer;->buyerActivatingScreenDataRenderer:Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;

    invoke-virtual {p3, p1}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenDataRenderer;->render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)Lcom/squareup/egiftcard/activation/ScreenData;

    move-result-object p1

    .line 87
    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreenKt;->ActivatingEGiftCardSellerScreen(Lcom/squareup/egiftcard/activation/ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 89
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method
