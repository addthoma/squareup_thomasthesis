.class public final Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseAmountCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseAmountCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseAmountCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,81:1\n1103#2,7:82\n1103#2,7:90\n1103#2,7:98\n1642#3:89\n1643#3:97\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseAmountCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseAmountCoordinator\n*L\n54#1,7:82\n58#1,7:90\n63#1,7:98\n58#1:89\n58#1:97\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001cB1\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J&\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u00052\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001bH\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0011j\u0008\u0012\u0004\u0012\u00020\u000f`\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ChooseAmountScreen;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;)V",
        "backButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "customAmountButton",
        "Lcom/squareup/noho/NohoButton;",
        "presetButtons",
        "Ljava/util/ArrayList;",
        "Lkotlin/collections/ArrayList;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "state",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Factory",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private customAmountButton:Lcom/squareup/noho/NohoButton;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final presetButtons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/squareup/noho/NohoButton;",
            ">;"
        }
    .end annotation
.end field

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 28
    new-instance p1, Ljava/util/ArrayList;

    const/4 p2, 0x4

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->presetButtons:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 3

    .line 67
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_actionbarbutton:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Integer;

    .line 70
    sget v1, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_amount_0:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 71
    sget v1, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_amount_1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 72
    sget v1, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_amount_2:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 73
    sget v1, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_amount_3:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 69
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 75
    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->presetButtons:Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_0
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_amount_custom:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->customAmountButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez p1, :cond_0

    const-string v0, "backButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    .line 82
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/EGiftCardConfig;->getDenominationAmounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->presetButtons:Ljava/util/ArrayList;

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->zip(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 89
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lkotlin/Pair;

    invoke-virtual {p2}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    invoke-virtual {p2}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoButton;

    .line 59
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 60
    check-cast p2, Landroid/view/View;

    .line 90
    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;

    invoke-direct {v1, v0, p0, p3}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 63
    :cond_1
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->customAmountButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_2

    const-string p2, "customAmountButton"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    .line 98
    new-instance p2, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$onClickDebounced$2;

    invoke-direct {p2, p3}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->bindViews(Landroid/view/View;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$attach$1;-><init>(Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
