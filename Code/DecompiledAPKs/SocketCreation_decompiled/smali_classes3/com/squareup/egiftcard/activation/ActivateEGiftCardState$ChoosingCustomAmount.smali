.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;
.super Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.source "ActivateEGiftCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChoosingCustomAmount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000fH\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "config",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "designId",
        "",
        "(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;)V",
        "getConfig",
        "()Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "getDesignId",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

.field private final designId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount$Creator;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount$Creator;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "designId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "designId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-direct {v0, p1, p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Ljava/lang/String;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    return-object v0
.end method

.method public final getDesignId()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChoosingCustomAmount(config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", designId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;->designId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
