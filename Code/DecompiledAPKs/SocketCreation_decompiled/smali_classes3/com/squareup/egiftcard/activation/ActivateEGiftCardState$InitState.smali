.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;
.super Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.source "ActivateEGiftCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0007\u001a\u00020\u0008H\u00d6\u0001J\u0019\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008H\u00d6\u0001R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "()V",
        "config",
        "",
        "getConfig",
        "()Ljava/lang/Void;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

.field private static final config:Ljava/lang/Void;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState$Creator;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->getConfig()Ljava/lang/Void;

    move-result-object v0

    check-cast v0, Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    return-object v0
.end method

.method public getConfig()Ljava/lang/Void;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->config:Ljava/lang/Void;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
