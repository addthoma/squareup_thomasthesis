.class final Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialogFactory$create$1$3;
.super Ljava/lang/Object;
.source "ActivatingErrorSellerDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "onCancel"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialogFactory$create$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .line 31
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialogFactory$create$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$BackPressed;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
