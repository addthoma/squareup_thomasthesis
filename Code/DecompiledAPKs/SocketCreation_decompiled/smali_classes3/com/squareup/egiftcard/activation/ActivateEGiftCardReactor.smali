.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;
.super Ljava/lang/Object;
.source "ActivateEGiftCardReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B!\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ*\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J:\u0010\u0013\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u00022\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00182\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J$\u0010\u0019\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00150\u00142\u0006\u0010\u0016\u001a\u00020\u001aH\u0002J\u0016\u0010\u001b\u001a\u0010\u0012\u000c\u0012\n \u001d*\u0004\u0018\u00010\u001c0\u001c0\u0014H\u0002R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "giftCardServiceHelper",
        "Lcom/squareup/giftcard/GiftCardServiceHelper;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "(Lcom/squareup/giftcard/GiftCardServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V",
        "getGiftCardServiceHelper",
        "()Lcom/squareup/giftcard/GiftCardServiceHelper;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "registerEGiftCardToState",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
        "timerOneSecond",
        "",
        "kotlin.jvm.PlatformType",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/GiftCardServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "giftCardServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-void
.end method

.method public static final synthetic access$timerOneSecond(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;)Lio/reactivex/Single;
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->timerOneSecond()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final registerEGiftCardToState(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
            ">;>;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    .line 121
    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getDesignId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/giftcard/GiftCardServiceHelper;->registerEGiftCard(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;

    invoke-direct {v1, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$registerEGiftCardToState$1;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "giftCardServiceHelper\n  \u2026  }\n          )\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final timerOneSecond()Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 133
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lio/reactivex/Single;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.timer(1, SECONDS, mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final getGiftCardServiceHelper()Lcom/squareup/giftcard/GiftCardServiceHelper;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->giftCardServiceHelper:Lcom/squareup/giftcard/GiftCardServiceHelper;

    return-object v0
.end method

.method public launch(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 139
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 142
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->disableTimer(Lio/reactivex/Completable;)V

    .line 144
    sget-object p2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$launch$1;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$launch$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->launch(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$1;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto/16 :goto_0

    .line 58
    :cond_0
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;

    if-eqz v0, :cond_1

    new-instance p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$2;

    invoke-direct {p3, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$2;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_1
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;

    if-eqz v0, :cond_2

    new-instance p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$3;

    invoke-direct {p3, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$3;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 77
    :cond_2
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    if-eqz v0, :cond_3

    new-instance p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$4;

    invoke-direct {p3, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$4;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 85
    :cond_3
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    if-eqz v0, :cond_4

    new-instance p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;

    invoke-direct {p3, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$5;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_4
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->registerEGiftCardToState(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_5
    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    if-eqz v0, :cond_6

    .line 97
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;

    invoke-direct {v0, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    .line 98
    new-instance v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;

    invoke-direct {v1, p0, v0, p1, p3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$7;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$6;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 108
    :cond_6
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    if-eqz p3, :cond_7

    new-instance p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;

    invoke-direct {p3, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor$onReact$8;-><init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p3}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->onReact(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
