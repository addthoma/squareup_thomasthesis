.class public final Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;
.super Ljava/lang/Object;
.source "GiftCardActivationFlow.kt"

# interfaces
.implements Lcom/squareup/giftcardactivation/GiftCardActivationFlow;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcardactivation/GiftCardActivationFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NOT_SUPPORTED"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardActivationFlow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardActivationFlow.kt\ncom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,22:1\n151#2,2:23\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardActivationFlow.kt\ncom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED\n*L\n20#1,2:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001J\u0019\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0004H\u0096\u0001\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;",
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
        "()V",
        "activationScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "checkBalanceFor",
        "giftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "parentKey",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;

    invoke-direct {v0}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;-><init>()V

    sput-object v0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;->$$INSTANCE:Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 24
    const-class v2, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    iput-object v0, p0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    return-void
.end method


# virtual methods
.method public activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    iget-object v0, p0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    invoke-interface {v0}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public checkBalanceFor(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    const-string v0, "giftCard"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/giftcardactivation/GiftCardActivationFlow$NOT_SUPPORTED;->$$delegate_0:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    invoke-interface {v0, p1, p2}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->checkBalanceFor(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/ui/main/RegisterTreeKey;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    return-object p1
.end method
