.class public Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;
.super Ljava/lang/Object;
.source "LocaleCatalog.java"


# instance fields
.field private locale_country_code:Ljava/lang/String;

.field private locale_language:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setCountryCode(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;->locale_country_code:Ljava/lang/String;

    return-object p0
.end method

.method public setLanguage(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;->locale_language:Ljava/lang/String;

    return-object p0
.end method
