.class public final Lcom/squareup/eventstream/v2/EventstreamV2$AppState;
.super Ljava/lang/Object;
.source "EventstreamV2.java"

# interfaces
.implements Lcom/squareup/eventstream/CommonProperties;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/v2/EventstreamV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppState"
.end annotation


# instance fields
.field volatile advertisingId:Ljava/lang/String;

.field final commonProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field volatile locale:Ljava/util/Locale;

.field volatile location:Landroid/location/Location;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clearCommonProperty(Ljava/lang/String;)V
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 129
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method getCommonProperties()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 136
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 138
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAdvertisingId(Ljava/lang/String;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->advertisingId:Ljava/lang/String;

    return-void
.end method

.method public setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->locale:Ljava/util/Locale;

    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/eventstream/v2/EventstreamV2$AppState;->location:Landroid/location/Location;

    return-void
.end method
