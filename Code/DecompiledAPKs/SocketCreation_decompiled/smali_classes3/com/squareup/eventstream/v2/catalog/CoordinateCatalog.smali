.class public Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
.super Ljava/lang/Object;
.source "CoordinateCatalog.java"


# instance fields
.field private coordinate_altitude:Ljava/lang/Double;

.field private coordinate_geographic_accuracy:Ljava/lang/Double;

.field private coordinate_heading:Ljava/lang/Double;

.field private coordinate_latitude:Ljava/lang/Double;

.field private coordinate_longitude:Ljava/lang/Double;

.field private coordinate_speed:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setAltitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 15
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_altitude:Ljava/lang/Double;

    return-object p0
.end method

.method public setGeographicAccuracy(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 20
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_geographic_accuracy:Ljava/lang/Double;

    return-object p0
.end method

.method public setHeading(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 35
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_heading:Ljava/lang/Double;

    return-object p0
.end method

.method public setLatitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 25
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_latitude:Ljava/lang/Double;

    return-object p0
.end method

.method public setLongitude(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 30
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_longitude:Ljava/lang/Double;

    return-object p0
.end method

.method public setSpeed(D)Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;
    .locals 0

    .line 40
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;->coordinate_speed:Ljava/lang/Double;

    return-object p0
.end method
