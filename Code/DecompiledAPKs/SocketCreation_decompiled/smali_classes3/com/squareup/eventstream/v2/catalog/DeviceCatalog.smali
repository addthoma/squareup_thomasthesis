.class public Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
.super Ljava/lang/Object;
.source "DeviceCatalog.java"


# instance fields
.field private device_advertising_id:Ljava/lang/String;

.field private device_brand:Ljava/lang/String;

.field private device_build_device:Ljava/lang/String;

.field private device_build_product:Ljava/lang/String;

.field private device_cpu_abi:Ljava/lang/String;

.field private device_cpu_abi2:Ljava/lang/String;

.field private device_density_dpi:Ljava/lang/Integer;

.field private device_form_factor:Ljava/lang/String;

.field private device_manufacturer:Ljava/lang/String;

.field private device_model:Ljava/lang/String;

.field private device_orientation:Ljava/lang/String;

.field private device_screen_height:Ljava/lang/Long;

.field private device_screen_width:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setAdvertisingId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_advertising_id:Ljava/lang/String;

    return-object p0
.end method

.method public setBrand(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_brand:Ljava/lang/String;

    return-object p0
.end method

.method public setBuildDevice(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_build_device:Ljava/lang/String;

    return-object p0
.end method

.method public setBuildProduct(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_build_product:Ljava/lang/String;

    return-object p0
.end method

.method public setCpuAbi(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_cpu_abi:Ljava/lang/String;

    return-object p0
.end method

.method public setCpuAbi2(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_cpu_abi2:Ljava/lang/String;

    return-object p0
.end method

.method public setDensityDpi(I)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 71
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_density_dpi:Ljava/lang/Integer;

    return-object p0
.end method

.method public setFormFactor(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_form_factor:Ljava/lang/String;

    return-object p0
.end method

.method public setManufacturer(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_manufacturer:Ljava/lang/String;

    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_model:Ljava/lang/String;

    return-object p0
.end method

.method public setOrientation(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_orientation:Ljava/lang/String;

    return-object p0
.end method

.method public setScreenHeight(J)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 86
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_screen_height:Ljava/lang/Long;

    return-object p0
.end method

.method public setScreenWidth(J)Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;
    .locals 0

    .line 91
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;->device_screen_width:Ljava/lang/Long;

    return-object p0
.end method
