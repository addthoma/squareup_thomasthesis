.class public Lcom/squareup/eventstream/v2/AndroidEvent;
.super Ljava/lang/Object;
.source "AndroidEvent.java"


# instance fields
.field public androidDeviceCatalog:Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

.field public connectionCatalog:Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

.field public coordinateCatalog:Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

.field public deviceCatalog:Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

.field public localeCatalog:Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

.field public mobileAppCatalog:Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

.field public osCatalog:Lcom/squareup/eventstream/v2/catalog/OsCatalog;

.field public sessionCatalog:Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

.field public simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

.field public uCatalog:Lcom/squareup/eventstream/v2/catalog/UCatalog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->androidDeviceCatalog:Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;

    .line 32
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->deviceCatalog:Lcom/squareup/eventstream/v2/catalog/DeviceCatalog;

    .line 33
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->mobileAppCatalog:Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;

    .line 34
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/OsCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->osCatalog:Lcom/squareup/eventstream/v2/catalog/OsCatalog;

    .line 35
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/SimCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->simCatalog:Lcom/squareup/eventstream/v2/catalog/SimCatalog;

    .line 36
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->coordinateCatalog:Lcom/squareup/eventstream/v2/catalog/CoordinateCatalog;

    .line 37
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->connectionCatalog:Lcom/squareup/eventstream/v2/catalog/ConnectionCatalog;

    .line 38
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/UCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/UCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->uCatalog:Lcom/squareup/eventstream/v2/catalog/UCatalog;

    .line 39
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->localeCatalog:Lcom/squareup/eventstream/v2/catalog/LocaleCatalog;

    .line 40
    new-instance v0, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

    invoke-direct {v0}, Lcom/squareup/eventstream/v2/catalog/SessionCatalog;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v2/AndroidEvent;->sessionCatalog:Lcom/squareup/eventstream/v2/catalog/SessionCatalog;

    return-void
.end method
