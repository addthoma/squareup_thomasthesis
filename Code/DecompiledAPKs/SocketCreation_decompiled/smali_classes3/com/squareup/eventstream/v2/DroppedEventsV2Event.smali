.class Lcom/squareup/eventstream/v2/DroppedEventsV2Event;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "DroppedEventsV2Event.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "dropped_events"


# instance fields
.field private final dropped_events_batch_size:I

.field private final dropped_events_max_bytes_count:I

.field private final dropped_events_max_items_count:I

.field private final dropped_events_queue_creation_failure_count:I

.field private final dropped_events_queue_failure_count:I


# direct methods
.method constructor <init>(Ljava/util/Map;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "dropped_events"

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/squareup/eventstream/v2/DroppedEventsV2Event;->dropped_events_queue_failure_count:I

    .line 21
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_CREATION_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    .line 22
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/squareup/eventstream/v2/DroppedEventsV2Event;->dropped_events_queue_creation_failure_count:I

    .line 23
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_ITEMS:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/squareup/eventstream/v2/DroppedEventsV2Event;->dropped_events_max_items_count:I

    .line 24
    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_BYTES:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iput p1, p0, Lcom/squareup/eventstream/v2/DroppedEventsV2Event;->dropped_events_max_bytes_count:I

    .line 25
    iput p2, p0, Lcom/squareup/eventstream/v2/DroppedEventsV2Event;->dropped_events_batch_size:I

    return-void
.end method
