.class public Lcom/squareup/eventstream/v2/catalog/SimCatalog;
.super Ljava/lang/Object;
.source "SimCatalog.java"


# instance fields
.field public sim_country_iso:Ljava/lang/String;

.field public sim_mcc:Ljava/lang/String;

.field public sim_mnc:Ljava/lang/String;

.field public sim_operator_name:Ljava/lang/String;

.field public sim_serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setCountryIso(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->sim_country_iso:Ljava/lang/String;

    return-object p0
.end method

.method public setMcc(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->sim_mcc:Ljava/lang/String;

    return-object p0
.end method

.method public setMnc(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->sim_mnc:Ljava/lang/String;

    return-object p0
.end method

.method public setOperatorName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->sim_operator_name:Ljava/lang/String;

    return-object p0
.end method

.method public setSerialNumber(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/SimCatalog;
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/SimCatalog;->sim_serial_number:Ljava/lang/String;

    return-object p0
.end method
