.class final Lcom/squareup/eventstream/BaseEventstream$Builder$build$queueExecutor$1;
.super Ljava/lang/Object;
.source "BaseEventstream.kt"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/BaseEventstream$Builder;->build()Lcom/squareup/eventstream/BaseEventstream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0004\u0008\u0002\u0010\u0004\"\u001a\u0008\u0003\u0010\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006\"\u0004\u0008\u0004\u0010\u0007\".\u0008\u0005\u0010\u0008*(\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0008\u0018\u00010\t\"\u0004\u0008\u0006\u0010\n\"\u0004\u0008\u0007\u0010\u000b\"\u0004\u0008\u0008\u0010\u00042\u000e\u0010\u000c\u001a\n \u000e*\u0004\u0018\u00010\r0\rH\n\u00a2\u0006\u0002\u0008\u000f"
    }
    d2 = {
        "<anonymous>",
        "Ljava/lang/Thread;",
        "AppEventT",
        "ServerEventT",
        "StateT",
        "EventStreamT",
        "Lcom/squareup/eventstream/BaseEventstream;",
        "SerializerT",
        "BuilderT",
        "Lcom/squareup/eventstream/BaseEventstream$Builder;",
        "AppEventTypeT",
        "ServerEventTypeT",
        "runnable",
        "Ljava/lang/Runnable;",
        "kotlin.jvm.PlatformType",
        "newThread"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $threadName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder$build$queueExecutor$1;->$threadName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    .line 216
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/squareup/eventstream/BaseEventstream$Builder$build$queueExecutor$1;->$threadName:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method
