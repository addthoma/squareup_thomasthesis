.class final Lcom/squareup/eventstream/EventQueue$1;
.super Ljava/lang/Object;
.source "EventQueue.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/EventQueue;-><init>(Lcom/squareup/eventstream/QueueFactory;Lcom/squareup/eventstream/EventStreamLog;Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/DroppedEventCounter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/eventstream/EventQueue;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/EventQueue;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()I
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    .line 49
    :try_start_0
    invoke-static {v0}, Lcom/squareup/eventstream/EventQueue;->access$getQueueFactory$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/QueueFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/eventstream/QueueFactory;->create()Lcom/squareup/tape/FileObjectQueue;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 51
    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v2}, Lcom/squareup/eventstream/EventQueue;->access$getDroppedEventCounter$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/DroppedEventCounter;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_CREATION_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/eventstream/DroppedEventCounter;->add(ILcom/squareup/eventstream/DroppedEventCounter$DropType;)V

    .line 52
    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v2}, Lcom/squareup/eventstream/EventQueue;->access$getLogger$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/EventStreamLog;

    move-result-object v2

    check-cast v1, Ljava/lang/Throwable;

    invoke-interface {v2, v1}, Lcom/squareup/eventstream/EventStreamLog;->report(Ljava/lang/Throwable;)V

    .line 53
    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v2}, Lcom/squareup/eventstream/EventQueue;->access$getQueueFactory$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/QueueFactory;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/eventstream/QueueFactory;->recreate(Ljava/lang/Throwable;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object v1

    .line 48
    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/eventstream/EventQueue;->access$setQueue$p(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/tape/FileObjectQueue;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v0}, Lcom/squareup/eventstream/EventQueue;->access$getQueue$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/tape/FileObjectQueue;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/eventstream/EventQueue$1;->call()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
