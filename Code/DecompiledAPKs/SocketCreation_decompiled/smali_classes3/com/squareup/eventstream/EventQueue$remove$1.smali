.class final Lcom/squareup/eventstream/EventQueue$remove$1;
.super Ljava/lang/Object;
.source "EventQueue.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/eventstream/EventQueue;->remove(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $count:I

.field final synthetic this$0:Lcom/squareup/eventstream/EventQueue;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/EventQueue;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    iput p2, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->$count:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    const/4 v0, 0x0

    .line 160
    :try_start_0
    iget v1, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->$count:I

    :goto_0
    if-ge v0, v1, :cond_0

    .line 161
    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v2}, Lcom/squareup/eventstream/EventQueue;->access$getQueue$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/tape/FileObjectQueue;->remove()V

    .line 162
    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v2}, Lcom/squareup/eventstream/EventQueue;->access$getSize$p(Lcom/squareup/eventstream/EventQueue;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
    :try_end_0
    .catch Lcom/squareup/tape/FileException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    .line 165
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue$remove$1;->this$0:Lcom/squareup/eventstream/EventQueue;

    invoke-static {v1, v0}, Lcom/squareup/eventstream/EventQueue;->access$handleQueueFailure(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/tape/FileException;)V

    :cond_0
    return-void
.end method
