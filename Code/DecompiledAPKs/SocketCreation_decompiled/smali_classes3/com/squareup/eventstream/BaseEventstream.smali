.class public abstract Lcom/squareup/eventstream/BaseEventstream;
.super Ljava/lang/Object;
.source "BaseEventstream.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/BaseEventstream$BuildType;,
        Lcom/squareup/eventstream/BaseEventstream$LoggingProcessor;,
        Lcom/squareup/eventstream/BaseEventstream$Builder;,
        Lcom/squareup/eventstream/BaseEventstream$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AppEventTypeT:",
        "Ljava/lang/Object;",
        "ServerEventTypeT:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\n\u0008&\u0018\u0000 \u001a*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0004\u0008\u0002\u0010\u00032\u00020\u0004:\u0004\u0018\u0019\u001a\u001bB7\u0008\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00010\n\u00a2\u0006\u0002\u0010\u000bJ\u0013\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0014J\u0006\u0010\u000f\u001a\u00020\u0012J\u000b\u0010\u0016\u001a\u00028\u0002\u00a2\u0006\u0002\u0010\u0017R \u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00010\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/eventstream/BaseEventstream;",
        "AppEventTypeT",
        "ServerEventTypeT",
        "StateT",
        "",
        "eventFactoryExecutor",
        "Ljava/util/concurrent/ExecutorService;",
        "eventFactory",
        "Lcom/squareup/eventstream/EventFactory;",
        "eventStore",
        "Lcom/squareup/eventstream/EventStore;",
        "(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V",
        "isShutdown",
        "",
        "()Z",
        "shutdown",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "log",
        "",
        "eventToLog",
        "(Ljava/lang/Object;)V",
        "logSync",
        "state",
        "()Ljava/lang/Object;",
        "BuildType",
        "Builder",
        "Companion",
        "LoggingProcessor",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/eventstream/BaseEventstream$Companion;


# instance fields
.field private final eventFactory:Lcom/squareup/eventstream/EventFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventFactory<",
            "TServerEventTypeT;TAppEventTypeT;TStateT;>;"
        }
    .end annotation
.end field

.field private final eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

.field private final eventStore:Lcom/squareup/eventstream/EventStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventStore<",
            "TServerEventTypeT;>;"
        }
    .end annotation
.end field

.field private final shutdown:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/eventstream/BaseEventstream$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/BaseEventstream$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/BaseEventstream;->Companion:Lcom/squareup/eventstream/BaseEventstream$Companion;

    return-void
.end method

.method protected constructor <init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/EventFactory;Lcom/squareup/eventstream/EventStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/EventFactory<",
            "TServerEventTypeT;TAppEventTypeT;TStateT;>;",
            "Lcom/squareup/eventstream/EventStore<",
            "TServerEventTypeT;>;)V"
        }
    .end annotation

    const-string v0, "eventFactoryExecutor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventStore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    iput-object p3, p0, Lcom/squareup/eventstream/BaseEventstream;->eventStore:Lcom/squareup/eventstream/EventStore;

    .line 43
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/eventstream/BaseEventstream;->shutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic access$getEventFactory$p(Lcom/squareup/eventstream/BaseEventstream;)Lcom/squareup/eventstream/EventFactory;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    return-object p0
.end method

.method public static final synthetic access$getEventStore$p(Lcom/squareup/eventstream/BaseEventstream;)Lcom/squareup/eventstream/EventStore;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventStore:Lcom/squareup/eventstream/EventStore;

    return-object p0
.end method

.method public static final synthetic access$isShutdown$p(Lcom/squareup/eventstream/BaseEventstream;)Z
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream;->isShutdown()Z

    move-result p0

    return p0
.end method

.method private final isShutdown()Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->shutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final log(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAppEventTypeT;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 50
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 51
    iget-object v2, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/squareup/eventstream/BaseEventstream$log$1;

    invoke-direct {v3, p0, p1, v0, v1}, Lcom/squareup/eventstream/BaseEventstream$log$1;-><init>(Lcom/squareup/eventstream/BaseEventstream;Ljava/lang/Object;J)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final logSync(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAppEventTypeT;)V"
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Lcom/squareup/eventstream/BaseEventstream;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 71
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 72
    iget-object v2, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    invoke-interface {v2, p1, v0, v1}, Lcom/squareup/eventstream/EventFactory;->create(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object p1

    .line 73
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventStore:Lcom/squareup/eventstream/EventStore;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/EventStore;->logBlocking(Ljava/lang/Object;)V

    return-void
.end method

.method public final shutdown()V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->shutdown:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/eventstream/BaseEventstream$shutdown$1;

    iget-object v2, p0, Lcom/squareup/eventstream/BaseEventstream;->eventStore:Lcom/squareup/eventstream/EventStore;

    invoke-direct {v1, v2}, Lcom/squareup/eventstream/BaseEventstream$shutdown$1;-><init>(Lcom/squareup/eventstream/EventStore;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/eventstream/BaseEventstream$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/eventstream/BaseEventstream$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactoryExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method public final state()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStateT;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/eventstream/BaseEventstream;->eventFactory:Lcom/squareup/eventstream/EventFactory;

    invoke-interface {v0}, Lcom/squareup/eventstream/EventFactory;->state()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
