.class public final synthetic Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$TrJ6LlIrVkM8wWftYE96PRbb9q4;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function7;


# instance fields
.field private final synthetic f$0:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$TrJ6LlIrVkM8wWftYE96PRbb9q4;->f$0:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    iget-object v0, p0, Lcom/squareup/messagebar/-$$Lambda$ReaderStatusAndMessageBar$Presenter$TrJ6LlIrVkM8wWftYE96PRbb9q4;->f$0:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;

    move-object v1, p1

    check-cast v1, Ljava/util/List;

    move-object v2, p2

    check-cast v2, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-object v3, p3

    check-cast v3, Ljava/lang/Boolean;

    move-object v4, p4

    check-cast v4, Lcom/squareup/connectivity/InternetState;

    move-object v5, p5

    check-cast v5, Ljava/lang/Boolean;

    move-object v6, p6

    check-cast v6, Ljava/lang/Boolean;

    move-object v7, p7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$Presenter;->lambda$subscribeTheMessageAndStatusBarView$5$ReaderStatusAndMessageBar$Presenter(Ljava/util/List;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Ljava/lang/Boolean;Lcom/squareup/connectivity/InternetState;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageBarEvent;

    move-result-object p1

    return-object p1
.end method
