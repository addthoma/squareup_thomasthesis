.class Lcom/squareup/messagebar/ReaderMessageBarView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ReaderMessageBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/messagebar/ReaderMessageBarView;->showMessageAnimated(Ljava/lang/CharSequence;Lcom/squareup/cardreader/BatteryLevel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/messagebar/ReaderMessageBarView;


# direct methods
.method constructor <init>(Lcom/squareup/messagebar/ReaderMessageBarView;)V
    .locals 0

    .line 196
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView$2;->this$0:Lcom/squareup/messagebar/ReaderMessageBarView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 199
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView$2;->this$0:Lcom/squareup/messagebar/ReaderMessageBarView;

    invoke-static {p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->access$100(Lcom/squareup/messagebar/ReaderMessageBarView;)Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView$2;->this$0:Lcom/squareup/messagebar/ReaderMessageBarView;

    invoke-static {v0}, Lcom/squareup/messagebar/ReaderMessageBarView;->access$000(Lcom/squareup/messagebar/ReaderMessageBarView;)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 200
    iget-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView$2;->this$0:Lcom/squareup/messagebar/ReaderMessageBarView;

    invoke-static {p1}, Lcom/squareup/messagebar/ReaderMessageBarView;->access$100(Lcom/squareup/messagebar/ReaderMessageBarView;)Landroid/widget/TextView;

    move-result-object p1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
