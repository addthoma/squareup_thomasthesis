.class public final Lcom/squareup/onboarding/common/ActivationEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ActivationEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/onboarding/common/ActivationEvent;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "()V",
        "product_name",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final product_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRODUCT_ACTIVATION:Lcom/squareup/analytics/RegisterActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    const-string v0, "register"

    .line 10
    iput-object v0, p0, Lcom/squareup/onboarding/common/ActivationEvent;->product_name:Ljava/lang/String;

    return-void
.end method
