.class public final Lcom/squareup/onboarding/flow/PanelTimeout;
.super Ljava/lang/Object;
.source "PanelTimeout.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/PanelTimeout$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanelTimeout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PanelTimeout.kt\ncom/squareup/onboarding/flow/PanelTimeout\n*L\n1#1,79:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u000c\u001a\u00020\rJ\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u0005H\u0002J\"\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\t2\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\r0\u0014J\u0018\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelTimeout;",
        "Landroid/os/Parcelable;",
        "action",
        "",
        "delayMillis",
        "",
        "startTimeMillis",
        "(Ljava/lang/String;JJ)V",
        "timeoutExecutor",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        "timeoutRunnable",
        "Ljava/lang/Runnable;",
        "cancel",
        "",
        "describeContents",
        "",
        "remainingDelayFromNow",
        "schedule",
        "executor",
        "timeoutAction",
        "Lkotlin/Function1;",
        "writeToParcel",
        "dest",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/onboarding/flow/PanelTimeout;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/onboarding/flow/PanelTimeout$Companion;


# instance fields
.field private final action:Ljava/lang/String;

.field private final delayMillis:J

.field private final startTimeMillis:J

.field private timeoutExecutor:Lcom/squareup/thread/executor/SerialExecutor;

.field private timeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onboarding/flow/PanelTimeout$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/PanelTimeout$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelTimeout;->Companion:Lcom/squareup/onboarding/flow/PanelTimeout$Companion;

    .line 68
    new-instance v0, Lcom/squareup/onboarding/flow/PanelTimeout$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/PanelTimeout$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/onboarding/flow/PanelTimeout;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 1

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->action:Ljava/lang/String;

    iput-wide p2, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->delayMillis:J

    iput-wide p4, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->startTimeMillis:J

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;JJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x4

    if-eqz p6, :cond_0

    .line 22
    sget-object p4, Lcom/squareup/onboarding/flow/PanelTimeout;->Companion:Lcom/squareup/onboarding/flow/PanelTimeout$Companion;

    invoke-static {p4}, Lcom/squareup/onboarding/flow/PanelTimeout$Companion;->access$now(Lcom/squareup/onboarding/flow/PanelTimeout$Companion;)J

    move-result-wide p4

    :cond_0
    move-wide v4, p4

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/onboarding/flow/PanelTimeout;-><init>(Ljava/lang/String;JJ)V

    return-void
.end method

.method public static final synthetic access$getAction$p(Lcom/squareup/onboarding/flow/PanelTimeout;)Ljava/lang/String;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->action:Ljava/lang/String;

    return-object p0
.end method

.method private final remainingDelayFromNow()J
    .locals 4

    .line 54
    iget-wide v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->delayMillis:J

    sget-object v2, Lcom/squareup/onboarding/flow/PanelTimeout;->Companion:Lcom/squareup/onboarding/flow/PanelTimeout$Companion;

    invoke-static {v2}, Lcom/squareup/onboarding/flow/PanelTimeout$Companion;->access$now(Lcom/squareup/onboarding/flow/PanelTimeout$Companion;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->startTimeMillis:J

    add-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->cancel(Ljava/lang/Runnable;)V

    :cond_0
    const/4 v0, 0x0

    .line 50
    move-object v1, v0

    check-cast v1, Lcom/squareup/thread/executor/SerialExecutor;

    iput-object v1, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    .line 51
    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final schedule(Lcom/squareup/thread/executor/SerialExecutor;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "executor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "timeoutAction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 39
    iput-object p1, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    .line 40
    new-instance v0, Lcom/squareup/onboarding/flow/PanelTimeout$schedule$2;

    invoke-direct {v0, p0, p2}, Lcom/squareup/onboarding/flow/PanelTimeout$schedule$2;-><init>(Lcom/squareup/onboarding/flow/PanelTimeout;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutRunnable:Ljava/lang/Runnable;

    .line 44
    iget-object p2, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/squareup/onboarding/flow/PanelTimeout;->remainingDelayFromNow()J

    move-result-wide v0

    invoke-interface {p1, p2, v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void

    .line 37
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "A timeout has already been scheduled."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "dest"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object p2, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->action:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget-wide v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->delayMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 62
    iget-wide v0, p0, Lcom/squareup/onboarding/flow/PanelTimeout;->startTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
