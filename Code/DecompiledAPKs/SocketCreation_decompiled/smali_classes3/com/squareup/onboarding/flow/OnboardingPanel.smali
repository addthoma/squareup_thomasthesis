.class public final Lcom/squareup/onboarding/flow/OnboardingPanel;
.super Ljava/lang/Object;
.source "OnboardingPanelScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\n\u001a\u00020\u0005R\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingPanel;",
        "",
        "()V",
        "BINDING_KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "getBINDING_KEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "key",
        "screenData",
        "Submission",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BINDING_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingPanel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingPanel;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingPanel;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingPanel;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingPanel;

    .line 12
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/squareup/onboarding/flow/OnboardingPanel;->BINDING_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getBINDING_KEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;"
        }
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingPanel;->BINDING_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public final key(Lcom/squareup/onboarding/flow/OnboardingScreenData;)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
            ">;"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingPanel;->BINDING_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->name:Ljava/lang/String;

    const-string v1, "screenData.panel.name"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2, v1}, Lcom/squareup/workflow/legacy/Screen$Key;->copy$default(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    return-object p1
.end method
