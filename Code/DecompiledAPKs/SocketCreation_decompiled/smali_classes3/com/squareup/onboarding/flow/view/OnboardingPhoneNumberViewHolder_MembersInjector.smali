.class public final Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;
.super Ljava/lang/Object;
.source "OnboardingPhoneNumberViewHolder_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneScrubberProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->phoneScrubberProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->phoneHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/InsertingScrubber;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;",
            ">;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPhoneHelper(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method

.method public static injectPhoneScrubber(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->phoneScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->phoneScrubberProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/InsertingScrubber;

    invoke-static {p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->injectPhoneScrubber(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/text/InsertingScrubber;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/PhoneNumberHelper;

    invoke-static {p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->injectPhoneHelper(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/text/PhoneNumberHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder_MembersInjector;->injectMembers(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;)V

    return-void
.end method
