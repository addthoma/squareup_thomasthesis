.class public abstract Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "OnboardingComponentViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u001f\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0013\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u001cJ\u0015\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H$\u00a2\u0006\u0002\u0010\u001cJ\u0008\u0010\u001e\u001a\u00020\u001aH\u0016J\u000c\u0010\u001f\u001a\u00020\u001a*\u00020 H\u0004R&\u0010\u000b\u001a\u0004\u0018\u00010\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "T",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "layout",
        "",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V",
        "componentName",
        "",
        "componentName$annotations",
        "()V",
        "getComponentName",
        "()Ljava/lang/String;",
        "setComponentName",
        "(Ljava/lang/String;)V",
        "context",
        "Landroid/content/Context;",
        "getContext",
        "()Landroid/content/Context;",
        "getInputHandler",
        "()Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "bind",
        "",
        "component",
        "(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V",
        "onBindComponent",
        "onHighlightError",
        "setSelectionEnd",
        "Landroid/widget/EditText;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private componentName:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private final inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {p3, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    .line 28
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "itemView.context"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->context:Landroid/content/Context;

    return-void
.end method

.method public static synthetic componentName$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final bind(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->componentName:Ljava/lang/String;

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V

    return-void
.end method

.method public final getComponentName()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->componentName:Ljava/lang/String;

    return-object v0
.end method

.method protected final getContext()Landroid/content/Context;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->context:Landroid/content/Context;

    return-object v0
.end method

.method protected final getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->inputHandler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    return-object v0
.end method

.method protected abstract onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public onHighlightError()V
    .locals 0

    return-void
.end method

.method public final setComponentName(Ljava/lang/String;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->componentName:Ljava/lang/String;

    return-void
.end method

.method protected final setSelectionEnd(Landroid/widget/EditText;)V
    .locals 1

    const-string v0, "$this$setSelectionEnd"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method
