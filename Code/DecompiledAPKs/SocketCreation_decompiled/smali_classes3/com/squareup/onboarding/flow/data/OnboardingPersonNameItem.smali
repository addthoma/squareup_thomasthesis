.class public final Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingPersonNameItem.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\u0008J\u0006\u0010\t\u001a\u00020\u0008J\u0006\u0010\n\u001a\u00020\u0008J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0008J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0006\u0010\u0012\u001a\u00020\u0008J\u0006\u0010\u0013\u001a\u00020\u0008J\u000e\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u0008\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "componentData",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V",
        "defaultFirstName",
        "",
        "defaultLastName",
        "firstNameHint",
        "firstNameOutput",
        "Lcom/squareup/protos/client/onboard/Output;",
        "firstName",
        "getValidatorHelper",
        "Lcom/squareup/onboarding/flow/data/OnboardingValidator;",
        "outputs",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "label",
        "lastNameHint",
        "lastNameOutput",
        "lastName",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    return-void
.end method


# virtual methods
.method public final defaultFirstName()Ljava/lang/String;
    .locals 4

    .line 19
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_first_name"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "first_name"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final defaultLastName()Ljava/lang/String;
    .locals 4

    .line 22
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_last_name"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "last_name"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final firstNameHint()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "placeholder_first_name"

    const/4 v2, 0x2

    .line 14
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final firstNameOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "firstName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "first_name"

    .line 24
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"first_name\", firstName)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 30
    new-instance v1, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem$getValidatorHelper$1;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem$getValidatorHelper$1;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v3, v1

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 31
    new-instance v1, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem$getValidatorHelper$2;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem$getValidatorHelper$2;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v4, v1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x9

    const/4 v7, 0x0

    move-object v1, v0

    .line 29
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final label()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "label"

    const/4 v2, 0x2

    .line 12
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final lastNameHint()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "placeholder_last_name"

    const/4 v2, 0x2

    .line 16
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final lastNameOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "lastName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "last_name"

    .line 26
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"last_name\", lastName)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
