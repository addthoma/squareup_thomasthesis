.class public final Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingDatePickerViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingDatePickerViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingDatePickerViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder\n*L\n1#1,44:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0014J\u0008\u0010\u000f\u001a\u00020\rH\u0016J\u0008\u0010\u0010\u001a\u00020\u0011H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "field",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "userPreferredDateHint",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

.field private final header:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_date_picker:I

    .line 16
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 19
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_date_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 20
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_date_picker:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    return-void
.end method

.method private final userPreferredDateHint()Ljava/lang/String;
    .locals 3

    .line 41
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getDateFormat()Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const-string v2, "Calendar.getInstance()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "field.dateFormat.format(\u2026endar.getInstance().time)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;)V
    .locals 3

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->hint()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->userPreferredDateHint()Ljava/lang/String;

    move-result-object v1

    :goto_1
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setHint(Ljava/lang/CharSequence;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    new-instance v1, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder$onBindComponent$2;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setOnDateSelectedListener(Lkotlin/jvm/functions/Function1;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->defaultDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setDate(Lorg/threeten/bp/LocalDate;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->minDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setMinDate(Lorg/threeten/bp/LocalDate;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDatePickerItem;->maxDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setMaxDate(Lorg/threeten/bp/LocalDate;)V

    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDatePickerViewHolder;->field:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->requestFocus()Z

    return-void
.end method
