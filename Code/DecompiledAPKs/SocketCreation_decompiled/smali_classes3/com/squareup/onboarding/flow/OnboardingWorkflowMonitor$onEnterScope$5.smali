.class final Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;
.super Ljava/lang/Object;
.source "OnboardingWorkflowMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/activation/ActivationUrl;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;->this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/activation/ActivationUrl;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;->this$0:Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;

    invoke-static {p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;->access$getActivationServiceHelper$p(Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor;)Lcom/squareup/onboarding/ActivationServiceHelper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/onboarding/ActivationServiceHelper;->activationUrl()Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingWorkflowMonitor$onEnterScope$5;->apply(Lkotlin/Unit;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
