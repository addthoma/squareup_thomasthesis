.class final Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingBankAccountItem.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->caBankValidator(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingBankAccountItem.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingBankAccountItem.kt\ncom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->this$0:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 5

    .line 60
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->this$0:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    sget-object v2, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->INSTITUTION:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-static {v0, v1, v2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->access$get(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->this$0:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    sget-object v3, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->TRANSIT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-static {v1, v2, v3}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->access$get(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v1

    .line 62
    iget-object v2, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->this$0:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;

    iget-object v3, p0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    sget-object v4, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ACCOUNT:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-static {v2, v3, v4}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->access$get(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v2

    .line 64
    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const-string v3, ""

    .line 66
    :goto_1
    sget-object v4, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$1;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$1;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v4}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$3;

    invoke-direct {v0, v3}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2$3;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    return v0
.end method
