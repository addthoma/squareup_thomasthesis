.class public final Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$Statement;
.source "OnboardingComponentItem.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "OutputStatement"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$Statement<",
        "Lcom/squareup/protos/client/onboard/Output;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0000J\u0006\u0010\u0008\u001a\u00020\u0006J\u0006\u0010\t\u001a\u00020\u0006J\u000e\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$Statement;",
        "Lcom/squareup/protos/client/onboard/Output;",
        "output",
        "(Lcom/squareup/protos/client/onboard/Output;)V",
        "isEqualTo",
        "",
        "other",
        "isPresent",
        "isTrue",
        "matchesRegex",
        "regex",
        "Lkotlin/text/Regex;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Output;)V
    .locals 0

    .line 112
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$Statement;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final isEqualTo(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;)Z
    .locals 3

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Output;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Output;->integer_value:Ljava/lang/Integer;

    goto :goto_2

    :cond_2
    move-object v0, v1

    :goto_2
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/squareup/protos/client/onboard/Output;->integer_value:Ljava/lang/Integer;

    goto :goto_3

    :cond_3
    move-object v2, v1

    :goto_3
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Output;->boolean_value:Ljava/lang/Boolean;

    goto :goto_4

    :cond_4
    move-object v0, v1

    :goto_4
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/onboard/Output;

    if-eqz p1, :cond_5

    iget-object v1, p1, Lcom/squareup/protos/client/onboard/Output;->boolean_value:Ljava/lang/Boolean;

    :cond_5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    const/4 p1, 0x1

    goto :goto_5

    :cond_6
    const/4 p1, 0x0

    :goto_5
    return p1
.end method

.method public final isPresent()Z
    .locals 1

    .line 115
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$isPresent$1;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$isPresent$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    return v0
.end method

.method public final isTrue()Z
    .locals 1

    .line 121
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$isTrue$1;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$isTrue$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result v0

    return v0
.end method

.method public final matchesRegex(Lkotlin/text/Regex;)Z
    .locals 1

    const-string v0, "regex"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement$matchesRegex$1;-><init>(Lkotlin/text/Regex;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->check(Lkotlin/jvm/functions/Function1;)Z

    move-result p1

    return p1
.end method
