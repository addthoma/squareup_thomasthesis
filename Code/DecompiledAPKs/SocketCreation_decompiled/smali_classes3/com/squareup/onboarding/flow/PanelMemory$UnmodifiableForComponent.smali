.class final Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;
.super Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;
.source "PanelMemory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/PanelMemory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UnmodifiableForComponent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0004H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;",
        "Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;",
        "()V",
        "addOutput",
        "",
        "output",
        "Lcom/squareup/protos/client/onboard/Output;",
        "clearAllOutputs",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 156
    new-instance v0, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;->INSTANCE:Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/PanelMemory$ReadWriteForComponent;-><init>()V

    return-void
.end method


# virtual methods
.method public addOutput(Lcom/squareup/protos/client/onboard/Output;)Ljava/lang/Void;
    .locals 1

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "read-only instance"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic addOutput(Lcom/squareup/protos/client/onboard/Output;)V
    .locals 0

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;->addOutput(Lcom/squareup/protos/client/onboard/Output;)Ljava/lang/Void;

    return-void
.end method

.method public clearAllOutputs()Ljava/lang/Void;
    .locals 2

    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "read-only instance"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic clearAllOutputs()V
    .locals 0

    .line 156
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/PanelMemory$UnmodifiableForComponent;->clearAllOutputs()Ljava/lang/Void;

    return-void
.end method
