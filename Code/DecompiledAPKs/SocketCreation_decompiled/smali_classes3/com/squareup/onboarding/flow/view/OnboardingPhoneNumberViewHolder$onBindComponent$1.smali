.class public final Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "OnboardingPhoneNumberViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;",
            ")V"
        }
    .end annotation

    .line 49
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 52
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;->phoneNumberOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;

    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/view/OnboardingPhoneNumberViewHolder;->getPhoneHelper$onboarding_release()Lcom/squareup/text/PhoneNumberHelper;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPhoneNumberItem;->phoneNumberIsValidOutput(Z)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
