.class public interface abstract Lcom/squareup/onboarding/OnboardingStarter;
.super Ljava/lang/Object;
.source "OnboardingStarter.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;,
        Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;,
        Lcom/squareup/onboarding/OnboardingStarter$DestinationAfterOnboarding;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0003\u0006\u0007\u0008J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "Lmortar/Scoped;",
        "startActivation",
        "",
        "params",
        "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
        "ActivationLaunchMode",
        "DestinationAfterOnboarding",
        "OnboardingParams",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V
.end method
