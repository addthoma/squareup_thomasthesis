.class public final Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;
.super Ljava/lang/Object;
.source "ItemSorter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/itemsorter/ItemSorter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiningOptionGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I::",
        "Lcom/squareup/itemsorter/SortableItem<",
        "TI;TD;>;D:",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0005B\u001d\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00018\u0001\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u000f\u001a\u0004\u0018\u00018\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008H\u00c6\u0003J6\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00018\u00012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0012J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0005H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0015\u0010\u0006\u001a\u0004\u0018\u00018\u0001\u00a2\u0006\n\n\u0002\u0010\u000c\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;",
        "I",
        "Lcom/squareup/itemsorter/SortableItem;",
        "D",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "",
        "diningOption",
        "items",
        "",
        "(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)V",
        "getDiningOption",
        "()Lcom/squareup/itemsorter/SortableDiningOption;",
        "Lcom/squareup/itemsorter/SortableDiningOption;",
        "getItems",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "copy",
        "(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "itemsorter"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final diningOption:Lcom/squareup/itemsorter/SortableDiningOption;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TI;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;",
            "Ljava/util/List<",
            "+TI;>;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    iput-object p2, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->copy(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TI;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;",
            "Ljava/util/List<",
            "+TI;>;)",
            "Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup<",
            "TI;TD;>;"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    invoke-direct {v0, p1, p2}, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;-><init>(Lcom/squareup/itemsorter/SortableDiningOption;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;

    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    iget-object v1, p1, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDiningOption()Lcom/squareup/itemsorter/SortableDiningOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    return-object v0
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TI;>;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiningOptionGroup(diningOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->diningOption:Lcom/squareup/itemsorter/SortableDiningOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/itemsorter/ItemSorter$DiningOptionGroup;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
