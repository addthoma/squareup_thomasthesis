.class public final Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;
.super Ljava/lang/Object;
.source "ScopedObjectWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/leakcanary/ScopedObjectWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0007J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0001H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;",
        "",
        "()V",
        "SERVICE_NAME",
        "",
        "addService",
        "",
        "builder",
        "Lmortar/MortarScope$Builder;",
        "watchForLeaks",
        "scope",
        "Lmortar/MortarScope;",
        "ref",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/leakcanary/ScopedObjectWatcher$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final addService(Lmortar/MortarScope$Builder;)V
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;

    sget-object v1, Lleakcanary/AppWatcher;->INSTANCE:Lleakcanary/AppWatcher;

    invoke-virtual {v1}, Lleakcanary/AppWatcher;->getObjectWatcher()Lleakcanary/ObjectWatcher;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/leakcanary/ScopedObjectWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 29
    invoke-static {}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->access$getSERVICE_NAME$cp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method public final watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ref"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->access$getSERVICE_NAME$cp()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/leakcanary/ScopedObjectWatcher;

    .line 38
    invoke-virtual {v0, p1, p2}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->watch(Lmortar/MortarScope;Ljava/lang/Object;)V

    return-void
.end method
