.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideLibraryListPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/LibraryListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAdapterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAssistantProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    .line 47
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListManagerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->entryHandlerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->configurationProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p9, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListAdapterFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/librarylist/SimpleLibraryListModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
            ">;)",
            "Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;"
        }
    .end annotation

    .line 70
    new-instance v10, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static provideLibraryListPresenter(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/LibraryListPresenter;
    .locals 0

    .line 78
    invoke-virtual/range {p0 .. p8}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideLibraryListPresenter(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/LibraryListPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/LibraryListPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/LibraryListPresenter;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v4, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/librarylist/LibraryListManager;

    iget-object v5, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/librarylist/SimpleEntryHandler;

    iget-object v6, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;

    iget-object v7, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    iget-object v8, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->libraryListAdapterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/librarylist/LibraryListAdapter$Factory;

    invoke-static/range {v0 .. v8}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->provideLibraryListPresenter(Lcom/squareup/librarylist/SimpleLibraryListModule;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/LibraryListPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideLibraryListPresenterFactory;->get()Lcom/squareup/librarylist/LibraryListPresenter;

    move-result-object v0

    return-object v0
.end method
