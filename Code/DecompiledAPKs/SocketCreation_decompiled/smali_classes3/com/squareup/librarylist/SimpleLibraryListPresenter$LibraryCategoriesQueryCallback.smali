.class public Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;
.super Ljava/lang/Object;
.source "SimpleLibraryListPresenter.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/SimpleLibraryListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LibraryCategoriesQueryCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;",
        ">;"
    }
.end annotation


# instance fields
.field private canceled:Z

.field private final queryId:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Ljava/lang/String;)V
    .locals 0

    .line 678
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 679
    iput-object p2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->queryId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;",
            ">;)V"
        }
    .end annotation

    .line 683
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;

    .line 684
    iget-object v0, p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->categoryCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 685
    iget-boolean v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->canceled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    invoke-static {v1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->access$300(Lcom/squareup/librarylist/SimpleLibraryListPresenter;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 688
    :cond_0
    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    iget-object v2, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->queryId:Ljava/lang/String;

    iget-boolean p1, p1, Lcom/squareup/shared/catalog/synthetictables/CategoriesAndEmpty;->isLibraryEmpty:Z

    invoke-virtual {v1, v0, v2, p1}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->setCategoryQueryResults(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/lang/String;Z)V

    goto :goto_1

    .line 686
    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 690
    :goto_1
    iget-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->this$0:Lcom/squareup/librarylist/SimpleLibraryListPresenter;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/librarylist/SimpleLibraryListPresenter;->access$202(Lcom/squareup/librarylist/SimpleLibraryListPresenter;Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;)Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryQueryCallback;

    return-void
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 694
    iput-boolean v0, p0, Lcom/squareup/librarylist/SimpleLibraryListPresenter$LibraryCategoriesQueryCallback;->canceled:Z

    return-void
.end method
