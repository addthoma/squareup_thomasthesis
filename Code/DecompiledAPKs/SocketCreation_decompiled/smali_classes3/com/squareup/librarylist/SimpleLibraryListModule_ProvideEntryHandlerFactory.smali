.class public final Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule_ProvideEntryHandlerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/SimpleEntryHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/librarylist/SimpleLibraryListModule;


# direct methods
.method public constructor <init>(Lcom/squareup/librarylist/SimpleLibraryListModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    return-void
.end method

.method public static create(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;-><init>(Lcom/squareup/librarylist/SimpleLibraryListModule;)V

    return-object v0
.end method

.method public static provideEntryHandler(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleEntryHandler;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule;->provideEntryHandler()Lcom/squareup/librarylist/SimpleEntryHandler;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/SimpleEntryHandler;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/SimpleEntryHandler;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;->module:Lcom/squareup/librarylist/SimpleLibraryListModule;

    invoke-static {v0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;->provideEntryHandler(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/librarylist/SimpleEntryHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/librarylist/SimpleLibraryListModule_ProvideEntryHandlerFactory;->get()Lcom/squareup/librarylist/SimpleEntryHandler;

    move-result-object v0

    return-object v0
.end method
