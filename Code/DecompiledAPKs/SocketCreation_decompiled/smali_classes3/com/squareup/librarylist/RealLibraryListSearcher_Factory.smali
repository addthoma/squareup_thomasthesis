.class public final Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;
.super Ljava/lang/Object;
.source "RealLibraryListSearcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/RealLibraryListSearcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;->cogsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;)",
            "Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/RealLibraryListSearcher;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/RealLibraryListSearcher;-><init>(Lcom/squareup/cogs/Cogs;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/RealLibraryListSearcher;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    invoke-static {v0}, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;->newInstance(Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/RealLibraryListSearcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListSearcher_Factory;->get()Lcom/squareup/librarylist/RealLibraryListSearcher;

    move-result-object v0

    return-object v0
.end method
