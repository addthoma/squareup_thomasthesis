.class public final Lcom/squareup/librarylist/LibraryItemListNohoRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;,
        Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryItemListNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryItemListNohoRow.kt\ncom/squareup/librarylist/LibraryItemListNohoRow\n*L\n1#1,455:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0018\u00002\u00020\u0001:\u0002UVB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u001f\u001a\u00020 2\u0008\u0008\u0001\u0010!\u001a\u00020\u001e2\u0006\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$Je\u0010%\u001a\u00020 2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0010\u0008\u0002\u0010,\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-2\n\u0008\u0002\u0010/\u001a\u0004\u0018\u0001002\u0006\u00101\u001a\u0002022\n\u0008\u0002\u00103\u001a\u0004\u0018\u00010$2\u0006\u0010#\u001a\u00020$2\u0006\u00104\u001a\u000205\u00a2\u0006\u0002\u00106J.\u00107\u001a\u00020 2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u00103\u001a\u00020$2\u0006\u0010#\u001a\u00020$2\u0006\u00108\u001a\u000209JL\u0010:\u001a\u00020 2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-2\u0006\u00101\u001a\u0002022\u0006\u0010#\u001a\u00020$2\u0006\u0010;\u001a\u0002092\u0006\u00104\u001a\u000205J6\u0010<\u001a\u00020 2\u0006\u0010=\u001a\u0002092\u0006\u0010>\u001a\u0002092\u0006\u0010?\u001a\u00020@2\u0006\u00101\u001a\u0002022\u0006\u0010#\u001a\u00020$2\u0006\u0010*\u001a\u00020+J0\u0010A\u001a\u00020 2\u0006\u0010B\u001a\u00020C2\u0006\u0010\"\u001a\u00020\u001e2\u0008\u0010D\u001a\u0004\u0018\u0001092\u0006\u0010E\u001a\u00020F2\u0006\u0010#\u001a\u00020$J\u0010\u0010G\u001a\u00020 2\u0006\u0010H\u001a\u00020IH\u0014J\u0006\u0010J\u001a\u00020KJ\u0008\u0010L\u001a\u00020 H\u0014J\u0008\u0010M\u001a\u00020 H\u0014J\u0010\u0010N\u001a\u00020 2\u0006\u0010O\u001a\u00020$H\u0016J\u0010\u0010P\u001a\u00020 2\u0006\u0010Q\u001a\u00020\u0015H\u0002J\u000c\u0010R\u001a\u00020 *\u00020\u0018H\u0002J\u000c\u0010S\u001a\u00020 *\u00020\u0018H\u0002J\u000c\u0010T\u001a\u00020 *\u00020\u0018H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u00020\u000c8\u0006@\u0006X\u0087.\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0019\u001a\u00060\u001aR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006W"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "Lcom/squareup/marin/widgets/BorderedLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "amountTextHelper",
        "Lcom/squareup/librarylist/AmountTextHelper;",
        "durationTextHelper",
        "Lcom/squareup/librarylist/DurationTextHelper;",
        "itemListNohoRow",
        "Lcom/squareup/noho/NohoRow;",
        "itemListNohoRow$annotations",
        "()V",
        "getItemListNohoRow",
        "()Lcom/squareup/noho/NohoRow;",
        "setItemListNohoRow",
        "(Lcom/squareup/noho/NohoRow;)V",
        "itemRowStateRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "kotlin.jvm.PlatformType",
        "labelView",
        "Landroid/widget/TextView;",
        "textTileColorStrip",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;",
        "thumbnailLoader",
        "Lcom/squareup/librarylist/ThumbnailLoader;",
        "transitionTime",
        "",
        "bindActionItem",
        "",
        "drawableRes",
        "textResId",
        "isTextTile",
        "",
        "bindCatalogItem",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "categoriesHaveChevrons",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V",
        "bindCategoryItem",
        "itemCountInCategory",
        "",
        "bindDiscountItem",
        "discountNote",
        "bindItemSuggestion",
        "iconText",
        "itemName",
        "amount",
        "",
        "bindPlaceholderItem",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "subtitle",
        "chevronVisibility",
        "Lcom/squareup/marin/widgets/ChevronVisibility;",
        "dispatchDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "getItemThumbnail",
        "Landroid/widget/ImageView;",
        "onAttachedToWindow",
        "onFinishInflate",
        "setEnabled",
        "enabled",
        "updateView",
        "itemRowState",
        "setActionTextColor",
        "setMaxLinesAndEllipse",
        "setNormalTextColor",
        "ItemRowState",
        "TextTileColorStrip",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountTextHelper:Lcom/squareup/librarylist/AmountTextHelper;

.field private final durationTextHelper:Lcom/squareup/librarylist/DurationTextHelper;

.field public itemListNohoRow:Lcom/squareup/noho/NohoRow;

.field private final itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
            ">;"
        }
    .end annotation
.end field

.field private labelView:Landroid/widget/TextView;

.field private final textTileColorStrip:Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;

.field private final thumbnailLoader:Lcom/squareup/librarylist/ThumbnailLoader;

.field private final transitionTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    sget-object p2, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;->INSTANCE:Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DefaultItemRowState;

    invoke-static {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string v0, "BehaviorRelay.createDefa\u2026ate>(DefaultItemRowState)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 121
    new-instance p2, Lcom/squareup/librarylist/AmountTextHelper;

    invoke-direct {p2, p1}, Lcom/squareup/librarylist/AmountTextHelper;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->amountTextHelper:Lcom/squareup/librarylist/AmountTextHelper;

    .line 122
    new-instance p2, Lcom/squareup/librarylist/DurationTextHelper;

    invoke-direct {p2}, Lcom/squareup/librarylist/DurationTextHelper;-><init>()V

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->durationTextHelper:Lcom/squareup/librarylist/DurationTextHelper;

    .line 123
    new-instance p2, Lcom/squareup/librarylist/ThumbnailLoader;

    invoke-direct {p2, p1}, Lcom/squareup/librarylist/ThumbnailLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->thumbnailLoader:Lcom/squareup/librarylist/ThumbnailLoader;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->transitionTime:I

    .line 125
    new-instance p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;

    invoke-direct {p1, p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;-><init>(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->textTileColorStrip:Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 132
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setBorderWidth(I)V

    .line 135
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setBordersToMultiply()V

    const/16 p1, 0x8

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->addBorder(I)V

    return-void
.end method

.method public static final synthetic access$getItemRowStateRelay$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getThumbnailLoader$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/squareup/librarylist/ThumbnailLoader;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->thumbnailLoader:Lcom/squareup/librarylist/ThumbnailLoader;

    return-object p0
.end method

.method public static final synthetic access$getTransitionTime$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)I
    .locals 0

    .line 56
    iget p0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->transitionTime:I

    return p0
.end method

.method public static final synthetic access$updateView(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->updateView(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;)V

    return-void
.end method

.method public static synthetic bindCatalogItem$default(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;ILjava/lang/Object;)V
    .locals 12

    and-int/lit8 v0, p10, 0x8

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 318
    move-object v0, v1

    check-cast v0, Lcom/squareup/text/Formatter;

    move-object v6, v0

    goto :goto_0

    :cond_0
    move-object/from16 v6, p4

    :goto_0
    and-int/lit8 v0, p10, 0x10

    if-eqz v0, :cond_1

    .line 319
    move-object v0, v1

    check-cast v0, Lcom/squareup/text/DurationFormatter;

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object/from16 v7, p5

    :goto_1
    and-int/lit8 v0, p10, 0x40

    if-eqz v0, :cond_2

    .line 321
    move-object v0, v1

    check-cast v0, Ljava/lang/Boolean;

    move-object v9, v0

    goto :goto_2

    :cond_2
    move-object/from16 v9, p7

    :goto_2
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object/from16 v8, p6

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-virtual/range {v2 .. v11}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V

    return-void
.end method

.method public static synthetic itemListNohoRow$annotations()V
    .locals 0

    return-void
.end method

.method private final setActionTextColor(Landroid/widget/TextView;)V
    .locals 2

    .line 413
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$color;->library_item_color_action:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private final setMaxLinesAndEllipse(Landroid/widget/TextView;)V
    .locals 1

    const/4 v0, 0x1

    .line 408
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 409
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method

.method private final setNormalTextColor(Landroid/widget/TextView;)V
    .locals 2

    .line 417
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$color;->library_item_text_color_normal:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private final updateView(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;)V
    .locals 4

    .line 351
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    const-string v1, "itemListNohoRow"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->getItemName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->getOptionNote()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->getAmount()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->isChevronVisible()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/squareup/noho/AccessoryType;->DISCLOSURE:Lcom/squareup/noho/AccessoryType;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/squareup/noho/AccessoryType;->NONE:Lcom/squareup/noho/AccessoryType;

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setAccessory(Lcom/squareup/noho/AccessoryType;)V

    .line 356
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result v0

    .line 357
    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->isTextTile()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 358
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->textTileColorStrip:Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;

    .line 360
    instance-of v3, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    if-eqz v3, :cond_5

    move-object v3, p1

    check-cast v3, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    invoke-virtual {v3}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;->getEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result v0

    .line 358
    :cond_5
    invoke-virtual {v2, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->setColor(I)V

    .line 366
    :cond_6
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->labelView:Landroid/widget/TextView;

    const-string v2, "labelView"

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setMaxLinesAndEllipse(Landroid/widget/TextView;)V

    .line 367
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    :goto_1
    if-ne v0, v1, :cond_a

    .line 368
    sget v0, Lcom/squareup/noho/R$id;->description:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setMaxLinesAndEllipse(Landroid/widget/TextView;)V

    .line 371
    :cond_a
    instance-of p1, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;

    if-eqz p1, :cond_c

    .line 372
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->labelView:Landroid/widget/TextView;

    if-nez p1, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setActionTextColor(Landroid/widget/TextView;)V

    goto :goto_2

    .line 374
    :cond_c
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->labelView:Landroid/widget/TextView;

    if-nez p1, :cond_d

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-direct {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setNormalTextColor(Landroid/widget/TextView;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public final bindActionItem(IIZ)V
    .locals 3

    .line 220
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 221
    new-instance v1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v2, "resources.getString(textResId)"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-direct {v1, p2, p3, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;-><init>(Ljava/lang/String;ZI)V

    .line 220
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 p1, 0x1

    .line 227
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    return-void
.end method

.method public final bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/lang/Boolean;",
            "Z",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v7, p1

    move-object/from16 v6, p9

    const-string v1, "entry"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "itemPhotos"

    move-object v8, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "priceFormatter"

    move-object/from16 v3, p3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "currencyCode"

    move-object/from16 v5, p6

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    iget-object v9, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 326
    new-instance v10, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    .line 327
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v1, "entry.name"

    invoke-static {v11, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 328
    iget-object v1, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->durationTextHelper:Lcom/squareup/librarylist/DurationTextHelper;

    move-object/from16 v2, p5

    invoke-virtual {v1, p1, v2, v6}, Lcom/squareup/librarylist/DurationTextHelper;->getDurationText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/text/DurationFormatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 330
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 331
    iget-object v1, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->amountTextHelper:Lcom/squareup/librarylist/AmountTextHelper;

    move-object v2, p1

    move-object/from16 v4, p4

    .line 332
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/librarylist/AmountTextHelper;->getAmountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 335
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 336
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_1

    if-nez p7, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    move-object v1, v10

    move-object v2, v11

    move-object v3, v12

    move/from16 v6, p8

    move-object v7, p1

    move-object v8, p2

    .line 326
    invoke-direct/range {v1 .. v8}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 325
    invoke-virtual {v9, v10}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final bindCategoryItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;ZZLjava/lang/String;)V
    .locals 10

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemCountInCategory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 269
    new-instance v9, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    .line 270
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v1, "entry.name"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 273
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p3

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne p3, v1, :cond_0

    const/4 p3, 0x1

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v5, 0x0

    :goto_0
    const-string v3, ""

    move-object v1, v9

    move-object v4, p5

    move v6, p4

    move-object v7, p1

    move-object v8, p2

    .line 269
    invoke-direct/range {v1 .. v8}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 268
    invoke-virtual {v0, v9}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final bindDiscountItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;ZLjava/lang/String;Lcom/squareup/util/Res;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    const-string v1, "entry"

    move-object v8, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "itemPhotos"

    move-object v9, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "priceFormatter"

    move-object/from16 v4, p3

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "percentageFormatter"

    move-object/from16 v5, p4

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "currencyCode"

    move-object/from16 v6, p5

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "discountNote"

    move-object/from16 v10, p7

    invoke-static {v10, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v7, p8

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    iget-object v1, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 295
    new-instance v11, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    .line 296
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v2, "entry.name"

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    iget-object v2, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->amountTextHelper:Lcom/squareup/librarylist/AmountTextHelper;

    move-object v3, p1

    .line 299
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/librarylist/AmountTextHelper;->getAmountText(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 302
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, v11

    move-object v3, v12

    move-object/from16 v4, p7

    move/from16 v7, p6

    .line 295
    invoke-direct/range {v2 .. v9}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 294
    invoke-virtual {v1, v11}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final bindItemSuggestion(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/quantity/PerUnitFormatter;)V
    .locals 6

    const-string v0, "iconText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    new-instance v0, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    .line 243
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p3

    .line 244
    invoke-virtual {p3, p5}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p3

    .line 245
    invoke-virtual {p3}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 246
    iget-object p4, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 247
    new-instance p5, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    .line 249
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_note_suggested:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "resources.getString(R.st\u2026m_library_note_suggested)"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 250
    invoke-static {p7, p3, v0, v1, v0}, Lcom/squareup/quantity/PerUnitFormatter;->format$default(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/common/Money;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p5

    move-object v1, p2

    move v4, p6

    move-object v5, p1

    .line 247
    invoke-direct/range {v0 .. v5}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 246
    invoke-virtual {p4, p5}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const/4 p1, 0x1

    .line 255
    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    return-void
.end method

.method public final bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V
    .locals 9

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chevronVisibility"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 203
    new-instance v8, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    .line 204
    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string p2, "resources.getString(textResId)"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    check-cast p3, Ljava/lang/CharSequence;

    invoke-static {p3}, Lcom/squareup/util/Strings;->valueOrEmpty(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 207
    sget-object p2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    const/4 p3, 0x1

    if-eq p4, p2, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    const/4 v5, 0x0

    :goto_0
    const-string v3, ""

    move-object v1, v8

    move v6, p5

    move-object v7, p1

    .line 203
    invoke-direct/range {v1 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)V

    .line 202
    invoke-virtual {v0, v8}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 212
    invoke-virtual {p0, p3}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemRowStateRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;->isTextTile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->textTileColorStrip:Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$TextTileColorStrip;->drawBorders(Landroid/graphics/Canvas;)V

    .line 184
    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final getItemListNohoRow()Lcom/squareup/noho/NohoRow;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "itemListNohoRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getItemThumbnail()Landroid/widget/ImageView;
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "itemListNohoRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/noho/R$id;->icon:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 347
    :cond_1
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 146
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onAttachedToWindow()V

    .line 148
    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$1;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$1;-><init>(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 153
    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;-><init>(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 165
    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;

    invoke-direct {v0, p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$3;-><init>(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 140
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 141
    sget v0, Lcom/squareup/librarylist/R$id;->item_list_noho_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    .line 142
    sget v0, Lcom/squareup/noho/R$id;->label:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->labelView:Landroid/widget/TextView;

    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 188
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setEnabled(Z)V

    .line 189
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "itemListNohoRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    return-void
.end method

.method public final setItemListNohoRow(Lcom/squareup/noho/NohoRow;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow;->itemListNohoRow:Lcom/squareup/noho/NohoRow;

    return-void
.end method
