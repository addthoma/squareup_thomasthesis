.class final Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LibraryItemListNohoRow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRowKt;->loadIconIntoRow(Lcom/squareup/noho/NohoRow;Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/Context;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Landroid/graphics/drawable/Drawable;",
        "it",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;->$thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 449
    iget-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;->$thumbnailData:Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData;

    check-cast p1, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;

    invoke-virtual {p1}, Lcom/squareup/librarylist/ThumbnailLoader$ThumbnailData$LiteralThumbnailData;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRowKt$loadIconIntoRow$2;->invoke(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method
