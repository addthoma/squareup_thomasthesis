.class public final Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;
.super Lcom/squareup/librarylist/LibraryListView$Action;
.source "LibraryListView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryListView$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryItemClicked"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;",
        "Lcom/squareup/librarylist/LibraryListView$Action;",
        "view",
        "Landroid/view/View;",
        "position",
        "",
        "(Landroid/view/View;I)V",
        "getPosition",
        "()I",
        "getView",
        "()Landroid/view/View;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final position:I

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryListView$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    iput p2, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;Landroid/view/View;IILjava/lang/Object;)Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->copy(Landroid/view/View;I)Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    return v0
.end method

.method public final copy(Landroid/view/View;I)Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    invoke-direct {v0, p1, p2}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;-><init>(Landroid/view/View;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    iget-object v1, p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    iget p1, p1, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getPosition()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    return v0
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LibraryItemClicked(view="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
