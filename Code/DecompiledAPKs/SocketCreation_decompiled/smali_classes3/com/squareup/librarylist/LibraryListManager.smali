.class public interface abstract Lcom/squareup/librarylist/LibraryListManager;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0008\u0010\u000b\u001a\u00020\u0008H&J\u0010\u0010\u000c\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH&J\u0010\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0011H&J\u0010\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u0014H&J\u0010\u0010\u0015\u001a\u00020\u00082\u0006\u0010\u0016\u001a\u00020\u0017H&R\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListManager;",
        "Lmortar/Scoped;",
        "results",
        "Lrx/Observable;",
        "Lcom/squareup/librarylist/LibraryListResults;",
        "getResults",
        "()Lrx/Observable;",
        "catalogUpdate",
        "",
        "event",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "goBack",
        "search",
        "searchText",
        "",
        "viewCategory",
        "category",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "viewFilter",
        "filter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "viewPlaceholder",
        "placeholder",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract catalogUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
.end method

.method public abstract getResults()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/LibraryListResults;",
            ">;"
        }
    .end annotation
.end method

.method public abstract goBack()V
.end method

.method public abstract search(Ljava/lang/String;)V
.end method

.method public abstract viewCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
.end method

.method public abstract viewFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V
.end method

.method public abstract viewPlaceholder(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V
.end method
