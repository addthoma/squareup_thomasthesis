.class final Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;
.super Ljava/lang/Object;
.source "LibraryListSearcher.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListSearcher;->searchByName(Lcom/squareup/librarylist/LibraryListState;Ljava/util/List;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;",
        "it",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentState:Lcom/squareup/librarylist/LibraryListState;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/LibraryListState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;->$currentState:Lcom/squareup/librarylist/LibraryListState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;
    .locals 3

    .line 121
    new-instance v0, Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;

    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;->$currentState:Lcom/squareup/librarylist/LibraryListState;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;-><init>(Lcom/squareup/librarylist/LibraryListState;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-object v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher$searchByName$2;->call(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/librarylist/CatalogQueryResult$SearchByNameResult;

    move-result-object p1

    return-object p1
.end method
