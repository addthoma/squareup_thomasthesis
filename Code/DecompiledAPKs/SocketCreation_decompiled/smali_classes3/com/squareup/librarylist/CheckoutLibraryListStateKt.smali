.class public final Lcom/squareup/librarylist/CheckoutLibraryListStateKt;
.super Ljava/lang/Object;
.source "CheckoutLibraryListState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\"\u001c\u0010\u0000\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0002\u0010\u0003\u001a\u0004\u0008\u0004\u0010\u0005\"\u001c\u0010\u0006\u001a\u00020\u00018\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0007\u0010\u0003\u001a\u0004\u0008\u0008\u0010\u0005\u00a8\u0006\t"
    }
    d2 = {
        "DEFAULT_PHONE",
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "DEFAULT_PHONE$annotations",
        "()V",
        "getDEFAULT_PHONE",
        "()Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "DEFAULT_TABLET",
        "DEFAULT_TABLET$annotations",
        "getDEFAULT_TABLET",
        "library-list_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final DEFAULT_PHONE:Lcom/squareup/librarylist/CheckoutLibraryListState;

.field private static final DEFAULT_TABLET:Lcom/squareup/librarylist/CheckoutLibraryListState;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 10
    new-instance v8, Lcom/squareup/librarylist/CheckoutLibraryListState;

    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v8, Lcom/squareup/librarylist/CheckoutLibraryListStateKt;->DEFAULT_TABLET:Lcom/squareup/librarylist/CheckoutLibraryListState;

    .line 13
    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    sget-object v10, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1e

    const/16 v16, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/librarylist/CheckoutLibraryListStateKt;->DEFAULT_PHONE:Lcom/squareup/librarylist/CheckoutLibraryListState;

    return-void
.end method

.method public static synthetic DEFAULT_PHONE$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic DEFAULT_TABLET$annotations()V
    .locals 0

    return-void
.end method

.method public static final getDEFAULT_PHONE()Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListStateKt;->DEFAULT_PHONE:Lcom/squareup/librarylist/CheckoutLibraryListState;

    return-object v0
.end method

.method public static final getDEFAULT_TABLET()Lcom/squareup/librarylist/CheckoutLibraryListState;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/librarylist/CheckoutLibraryListStateKt;->DEFAULT_TABLET:Lcom/squareup/librarylist/CheckoutLibraryListState;

    return-object v0
.end method
