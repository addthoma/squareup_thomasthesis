.class public Lcom/squareup/opentickets/RealPredefinedTickets;
.super Ljava/lang/Object;
.source "RealPredefinedTickets.java"

# interfaces
.implements Lcom/squareup/opentickets/PredefinedTickets;
.implements Lcom/squareup/jailkeeper/JailKeeperService;


# instance fields
.field private final availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

.field private final tickets:Lcom/squareup/tickets/Tickets;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/tickets/Tickets;Lcom/squareup/cogs/Cogs;Lcom/squareup/opentickets/AvailableTemplateCountCache;Lcom/squareup/tickets/TicketGroupsCache;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 60
    iput-object p2, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->tickets:Lcom/squareup/tickets/Tickets;

    .line 61
    iput-object p3, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->cogs:Lcom/squareup/cogs/Cogs;

    .line 62
    iput-object p4, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    .line 63
    iput-object p5, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    .line 64
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method

.method private clearCache()V
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    invoke-interface {v0}, Lcom/squareup/opentickets/AvailableTemplateCountCache;->clear()V

    return-void
.end method

.method static filterAvailableTicketTemplates(Ljava/util/List;Lcom/squareup/tickets/TicketRowCursorList;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;"
        }
    .end annotation

    .line 250
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-object p0

    .line 255
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 258
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 259
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 262
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;

    .line 263
    invoke-interface {p1}, Lcom/squareup/tickets/TicketRowCursorList$TicketRow;->getOpenTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 265
    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getTemplateId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 271
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    if-eqz v3, :cond_2

    .line 273
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/tickets/OpenTicket;->getTemplateName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 274
    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 280
    :cond_3
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v1
.end method

.method private getAllTickets()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation

    .line 286
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$yt5PpOWTmLSGHWbCmQMme0VHFw0;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$yt5PpOWTmLSGHWbCmQMme0VHFw0;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$lXNQK9GbnnCROdvw_hPr02Cb25E(Lcom/squareup/opentickets/RealPredefinedTickets;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->updateAvailableTemplateCountsForPredefinedTicketsIfCogsIsReady()V

    return-void
.end method

.method static synthetic lambda$null$11(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I
    .locals 0

    .line 198
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getOrdinal()I

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getOrdinal()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method static synthetic lambda$null$13(Lio/reactivex/SingleEmitter;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 212
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$16(Lio/reactivex/SingleEmitter;Lcom/squareup/tickets/TicketsResult;)V
    .locals 0

    .line 288
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lio/reactivex/SingleEmitter;Lcom/squareup/tickets/TicketsResult;)V
    .locals 0

    .line 143
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$5(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 1

    .line 150
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 151
    invoke-interface {p0, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p0

    check-cast p0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllTicketGroups()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$6(Lio/reactivex/SingleEmitter;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2

    .line 154
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    .line 157
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 164
    :cond_2
    invoke-interface {p0, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$8(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 0

    .line 178
    invoke-interface {p1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findTicketTemplates(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$9(Lio/reactivex/SingleEmitter;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 179
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p0, p1}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method

.method private updateAvailableTemplateCountsForPredefinedTicketsIfCogsIsReady()V
    .locals 3

    .line 299
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 301
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->getAllTicketGroups()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$6TXKrTFN0JZbdhtdhsuDRJaMGBo;

    invoke-direct {v2, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$6TXKrTFN0JZbdhtdhsuDRJaMGBo;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    .line 302
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 300
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public getAllAvailableTicketTemplates()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    .line 216
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->getAllTicketTemplates()Lio/reactivex/Single;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->getAllTickets()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;->INSTANCE:Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;

    invoke-static {v0, v1, v2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAllTicketGroups()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$uRwS1aprzCMbpqxw3lwn1StGag8;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$uRwS1aprzCMbpqxw3lwn1StGag8;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAllTicketTemplates()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    .line 191
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$-2O11SmtfURnDaA7Zz24YQpgDCQ;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$-2O11SmtfURnDaA7Zz24YQpgDCQ;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTemplateCountCache()Lcom/squareup/opentickets/AvailableTemplateCountCache;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    return-object v0
.end method

.method public getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    .line 222
    invoke-virtual {p0, p1}, Lcom/squareup/opentickets/RealPredefinedTickets;->getTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/opentickets/RealPredefinedTickets;->getTicketsForGroup(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    sget-object v1, Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;->INSTANCE:Lcom/squareup/opentickets/-$$Lambda$wfEBNMVGHRDiSQRESm1M71ibeDU;

    .line 222
    invoke-static {v0, p1, v1}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$M3IhGxNAkcDTjlvUT_0vT1x3iOk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$M3IhGxNAkcDTjlvUT_0vT1x3iOk;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getTicketsForGroup(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation

    .line 141
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$zGP3LWqego_n3MHEmj5yPqrf7so;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$zGP3LWqego_n3MHEmj5yPqrf7so;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;Ljava/lang/String;)V

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getAllTicketGroups$7$RealPredefinedTickets(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->cogs:Lcom/squareup/cogs/Cogs;

    sget-object v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$uRnNFeigYm-PPWq3SIzBIQp2OgQ;->INSTANCE:Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$uRnNFeigYm-PPWq3SIzBIQp2OgQ;

    new-instance v2, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$LR4AjNZ4aF_u0VYMI1T1xJZ8bg8;

    invoke-direct {v2, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$LR4AjNZ4aF_u0VYMI1T1xJZ8bg8;-><init>(Lio/reactivex/SingleEmitter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$getAllTicketTemplates$14$RealPredefinedTickets(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 191
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$2PywyF1Bz2bFRmBeAeK8RLNp_cg;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$2PywyF1Bz2bFRmBeAeK8RLNp_cg;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    new-instance v2, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$z9IomSNYHUct0o798l0GJ9E5Img;

    invoke-direct {v2, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$z9IomSNYHUct0o798l0GJ9E5Img;-><init>(Lio/reactivex/SingleEmitter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$getAllTickets$17$RealPredefinedTickets(Lio/reactivex/SingleEmitter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->tickets:Lcom/squareup/tickets/Tickets;

    sget-object v2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    sget-object v5, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    new-instance v6, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$lA-L2NNpym3w0n6ydgNZkYkdpNo;

    invoke-direct {v6, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$lA-L2NNpym3w0n6ydgNZkYkdpNo;-><init>(Lio/reactivex/SingleEmitter;)V

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/squareup/tickets/Tickets;->getTicketList(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public synthetic lambda$getTicketTemplatesForGroup$10$RealPredefinedTickets(Ljava/lang/String;Lio/reactivex/SingleEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$A-UrFtMuCKe2GMCnzNV15fWUlPc;

    invoke-direct {v1, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$A-UrFtMuCKe2GMCnzNV15fWUlPc;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$GZDWc2YvqHG45FixwoDLd0J4fEc;

    invoke-direct {p1, p2}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$GZDWc2YvqHG45FixwoDLd0J4fEc;-><init>(Lio/reactivex/SingleEmitter;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$getTicketsForGroup$4$RealPredefinedTickets(Ljava/lang/String;Lio/reactivex/SingleEmitter;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->tickets:Lcom/squareup/tickets/Tickets;

    sget-object v2, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    sget-object v5, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    new-instance v6, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$9scmlkaTDno5hcMN-skCu60FjRc;

    invoke-direct {v6, p2}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$9scmlkaTDno5hcMN-skCu60FjRc;-><init>(Lio/reactivex/SingleEmitter;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/squareup/tickets/Tickets;->getGroupTicketList(Ljava/lang/String;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public synthetic lambda$null$12$RealPredefinedTickets(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 3

    .line 194
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->findAllTicketTemplatesByGroupId()Ljava/util/Map;

    move-result-object p1

    .line 197
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v0

    .line 198
    sget-object v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$7c14JM40TfKP0R0BvefiAV-X7hI;->INSTANCE:Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$7c14JM40TfKP0R0BvefiAV-X7hI;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 201
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 203
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 204
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_0

    .line 207
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public synthetic lambda$onEnterScope$0$RealPredefinedTickets(Ljava/util/List;)V
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 72
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 73
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    .line 74
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isClosedByUpdate()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->wasPredefinedTicket()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->getPreviousGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isDeletedByUpdate()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->wasPredefinedTicket()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->getPreviousGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isConvertedToCustomTicketByUpdate()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 79
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->getPreviousGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isOpenedByUpdate()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isPredefinedTicket()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    invoke-virtual {v1}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->getCurrentGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 84
    :cond_4
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    invoke-virtual {p0, v0}, Lcom/squareup/opentickets/RealPredefinedTickets;->updateAvailableTemplateCountForGroup(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$RealPredefinedTickets(Ljava/lang/Boolean;)V
    .locals 0

    .line 95
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 101
    invoke-direct {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->updateAvailableTemplateCountsForPredefinedTicketsIfCogsIsReady()V

    goto :goto_0

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/squareup/opentickets/RealPredefinedTickets;->clearCache()V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$2$RealPredefinedTickets(Ljava/lang/Integer;)V
    .locals 1

    .line 110
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 111
    iget-object p1, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/tickets/OpenTicketsSettings;->setPredefinedTicketsEnabled(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$updateAvailableTemplateCountForGroup$15$RealPredefinedTickets(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 231
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->availableTemplateCountCache:Lcom/squareup/opentickets/AvailableTemplateCountCache;

    .line 233
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 232
    invoke-interface {v0, p1, p2}, Lcom/squareup/opentickets/AvailableTemplateCountCache;->updateAvailableTemplateCountForGroup(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$updateAvailableTemplateCountsForPredefinedTicketsIfCogsIsReady$18$RealPredefinedTickets(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 303
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 305
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/opentickets/RealPredefinedTickets;->updateAvailableTemplateCountForGroup(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->tickets:Lcom/squareup/tickets/Tickets;

    .line 69
    invoke-interface {v0}, Lcom/squareup/tickets/Tickets;->onLocalTicketsUpdated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$At6r3wEIwubiCXtzcfMRaPSHJXI;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$At6r3wEIwubiCXtzcfMRaPSHJXI;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    .line 70
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 68
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 91
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->onPredefinedTicketsEnabled()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 92
    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$yuddmnZDxZzPzki2fkedxd1k4Go;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$yuddmnZDxZzPzki2fkedxd1k4Go;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    .line 94
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 90
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    .line 108
    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->onGroupCountChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$UFLDc-qMfeChsRcqToJSYMzUqFY;

    invoke-direct {v1, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$UFLDc-qMfeChsRcqToJSYMzUqFY;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    .line 109
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/opentickets/RealPredefinedTickets;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public preload()Lio/reactivex/Completable;
    .locals 1

    .line 121
    new-instance v0, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$lXNQK9GbnnCROdvw_hPr02Cb25E;

    invoke-direct {v0, p0}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$lXNQK9GbnnCROdvw_hPr02Cb25E;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;)V

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public reload()Lio/reactivex/Completable;
    .locals 1

    .line 126
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object v0

    return-object v0
.end method

.method public updateAvailableTemplateCountForGroup(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 229
    invoke-virtual {p0, p1}, Lcom/squareup/opentickets/RealPredefinedTickets;->getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$m9ElxDBT0n1R5wZnmFbU6TqGBt0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/opentickets/-$$Lambda$RealPredefinedTickets$m9ElxDBT0n1R5wZnmFbU6TqGBt0;-><init>(Lcom/squareup/opentickets/RealPredefinedTickets;Ljava/lang/String;)V

    .line 230
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    :cond_0
    return-void
.end method
