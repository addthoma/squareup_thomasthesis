.class public final Lcom/squareup/opentickets/NoOpTicketSweeperManager;
.super Ljava/lang/Object;
.source "NoOpTicketSweeperManager.kt"

# interfaces
.implements Lcom/squareup/opentickets/TicketsSweeperManager;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0004H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/opentickets/NoOpTicketSweeperManager;",
        "Lcom/squareup/opentickets/TicketsSweeperManager;",
        "()V",
        "schedulePeriodicSync",
        "",
        "stopSyncing",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/opentickets/NoOpTicketSweeperManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3
    new-instance v0, Lcom/squareup/opentickets/NoOpTicketSweeperManager;

    invoke-direct {v0}, Lcom/squareup/opentickets/NoOpTicketSweeperManager;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/NoOpTicketSweeperManager;->INSTANCE:Lcom/squareup/opentickets/NoOpTicketSweeperManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public schedulePeriodicSync()V
    .locals 0

    return-void
.end method

.method public stopSyncing()V
    .locals 0

    return-void
.end method
