.class public interface abstract Lcom/squareup/core/location/providers/LocationProvider;
.super Ljava/lang/Object;
.source "LocationProvider.java"


# static fields
.field public static final DEFAULT_DISTANCE_DELTA:F


# virtual methods
.method public abstract getLastKnownLocation()Landroid/location/Location;
.end method

.method public abstract removeUpdates()V
.end method

.method public abstract requestLocationUpdate(Landroid/location/LocationListener;J)V
.end method

.method public abstract requestLocationUpdate(Landroid/location/LocationListener;JF)V
.end method
