.class public Lcom/squareup/core/location/providers/NMEAEventListener;
.super Ljava/lang/Object;
.source "NMEAEventListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/core/location/providers/NMEAEventListener$Listener;
    }
.end annotation


# static fields
.field public static final NMEA_TYPE:Ljava/lang/String; = "$GPRMC"


# instance fields
.field private final listener:Lcom/squareup/core/location/providers/NMEAEventListener$Listener;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/core/location/providers/NMEAEventListener$Listener;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/core/location/providers/NMEAEventListener;->listener:Lcom/squareup/core/location/providers/NMEAEventListener$Listener;

    .line 50
    iput-object p2, p0, Lcom/squareup/core/location/providers/NMEAEventListener;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method private getChecksum(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    .line 124
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 125
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 126
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    .line 127
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v3

    const/16 v4, 0x24

    if-ne v3, v4, :cond_0

    goto :goto_1

    .line 129
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v3

    const/16 v4, 0x2a

    if-ne v3, v4, :cond_1

    goto :goto_2

    .line 134
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_2

    .line 136
    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    .line 139
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    xor-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_3
    :goto_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private isValid(Ljava/lang/String;)Z
    .locals 2

    const-string v0, ""

    const-string v1, "\r"

    .line 114
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "\n"

    .line 115
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    .line 117
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-direct {p0, p1}, Lcom/squareup/core/location/providers/NMEAEventListener;->getChecksum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private parse(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    .line 61
    :try_start_0
    invoke-direct {p0, p1}, Lcom/squareup/core/location/providers/NMEAEventListener;->isValid(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v1, ","

    .line 64
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 65
    aget-object v2, v1, v0

    const-string v3, "$GPRMC"

    .line 67
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    invoke-direct {p0, v1}, Lcom/squareup/core/location/providers/NMEAEventListener;->parseGPRMC([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    const-string p1, "Unable to parse nmea string %s"

    .line 71
    invoke-static {v1, p1, v2}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private parseGPRMC([Ljava/lang/String;)V
    .locals 13

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 77
    :try_start_0
    array-length v2, p1

    const/16 v3, 0x9

    if-le v2, v3, :cond_2

    .line 78
    aget-object v2, p1, v1

    .line 79
    aget-object v3, p1, v3

    .line 82
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_2

    const/4 v4, 0x2

    .line 83
    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    const/4 v5, 0x4

    .line 84
    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 86
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x6

    if-le v6, v7, :cond_0

    .line 87
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    :goto_0
    move v12, v2

    .line 93
    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 94
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    add-int/lit8 v2, v2, -0x1

    :cond_1
    move v8, v2

    .line 98
    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit16 v7, v2, 0x7d0

    const-string v2, "UTC"

    .line 101
    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    move-object v6, v2

    .line 102
    invoke-virtual/range {v6 .. v12}, Ljava/util/Calendar;->set(IIIIII)V

    .line 104
    iget-object v3, p0, Lcom/squareup/core/location/providers/NMEAEventListener;->listener:Lcom/squareup/core/location/providers/NMEAEventListener$Listener;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/squareup/core/location/providers/NMEAEventListener;->listener:Lcom/squareup/core/location/providers/NMEAEventListener$Listener;

    invoke-interface {v3, v2}, Lcom/squareup/core/location/providers/NMEAEventListener$Listener;->gotNMEADate(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 108
    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v0

    const-string p1, "Unable to parse date from nmeaParts %s"

    invoke-static {v2, p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public onNmeaMessage(Ljava/lang/String;J)V
    .locals 0

    .line 54
    iget-object p2, p0, Lcom/squareup/core/location/providers/NMEAEventListener;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p2}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/core/location/providers/NMEAEventListener;->parse(Ljava/lang/String;)V

    return-void
.end method
