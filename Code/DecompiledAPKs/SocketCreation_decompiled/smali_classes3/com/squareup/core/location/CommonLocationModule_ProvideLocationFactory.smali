.class public final Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;
.super Ljava/lang/Object;
.source "CommonLocationModule_ProvideLocationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# instance fields
.field private final validatedLocationCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/ValidatedLocationCacheProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/ValidatedLocationCacheProvider;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;->validatedLocationCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/location/ValidatedLocationCacheProvider;",
            ">;)",
            "Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;

    invoke-direct {v0, p0}, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocation(Lcom/squareup/location/ValidatedLocationCacheProvider;)Landroid/location/Location;
    .locals 0

    .line 40
    invoke-static {p0}, Lcom/squareup/core/location/CommonLocationModule;->provideLocation(Lcom/squareup/location/ValidatedLocationCacheProvider;)Landroid/location/Location;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public get()Landroid/location/Location;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;->validatedLocationCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/location/ValidatedLocationCacheProvider;

    invoke-static {v0}, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;->provideLocation(Lcom/squareup/location/ValidatedLocationCacheProvider;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/core/location/CommonLocationModule_ProvideLocationFactory;->get()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
