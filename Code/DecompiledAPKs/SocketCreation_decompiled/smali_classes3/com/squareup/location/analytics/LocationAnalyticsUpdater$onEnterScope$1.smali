.class final Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;
.super Ljava/lang/Object;
.source "LocationAnalyticsUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Landroid/location/Location;",
        "+",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Landroid/location/Location;",
        "kotlin.jvm.PlatformType",
        "",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/location/analytics/LocationAnalyticsUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;->this$0:Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 22
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Landroid/location/Location;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 33
    iget-object v1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;->this$0:Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    invoke-static {v1}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->access$getAnalytics$p(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    const-string v2, "sanitize"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater$onEnterScope$1;->this$0:Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    const-string v2, "location"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;->access$sanitize(Lcom/squareup/location/analytics/LocationAnalyticsUpdater;Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    :cond_0
    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->setLocation(Landroid/location/Location;)V

    return-void
.end method
