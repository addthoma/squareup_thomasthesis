.class public final Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;
.super Ljava/lang/Object;
.source "LocationAnalyticsUpdater_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/location/analytics/LocationAnalyticsUpdater;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final continuousLocationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->continuousLocationMonitorProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/location/analytics/LocationAnalyticsUpdater;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater;-><init>(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/location/analytics/LocationAnalyticsUpdater;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->continuousLocationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    iget-object v1, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->newInstance(Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/location/analytics/LocationAnalyticsUpdater_Factory;->get()Lcom/squareup/location/analytics/LocationAnalyticsUpdater;

    move-result-object v0

    return-object v0
.end method
