.class Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;
.super Lcom/squareup/analytics/event/v1/ViewEvent;
.source "LoyaltyAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StatusProgressRewardView"
.end annotation


# instance fields
.field final buyerToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "buyer_token"
    .end annotation
.end field

.field final clientPaymentToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "client_payment_token"
    .end annotation
.end field

.field final punchEarned:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "punch_earned"
    .end annotation
.end field

.field final unitToken:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "unit_token"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 1

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;-><init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/analytics/RegisterViewName;ZLcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 0

    .line 76
    invoke-direct {p0, p1, p4}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;Lcom/squareup/protos/eventstream/v1/Subject;)V

    .line 77
    invoke-virtual {p3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;->clientPaymentToken:Ljava/lang/String;

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    move-object p4, p1

    goto :goto_0

    .line 78
    :cond_0
    iget-object p4, p3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    :goto_0
    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;->unitToken:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 79
    invoke-static {p3}, Lcom/squareup/loyalty/LoyaltyAnalytics;->access$000(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Ljava/lang/String;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;->buyerToken:Ljava/lang/String;

    .line 80
    iget p1, p3, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    iput p1, p0, Lcom/squareup/loyalty/LoyaltyAnalytics$StatusProgressRewardView;->punchEarned:I

    return-void
.end method
