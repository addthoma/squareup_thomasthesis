.class public Lcom/squareup/loyalty/LoyaltyCalculator;
.super Ljava/lang/Object;
.source "LoyaltyCalculator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltyCalculator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltyCalculator.kt\ncom/squareup/loyalty/LoyaltyCalculator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,173:1\n704#2:174\n777#2,2:175\n1642#2,2:177\n1550#2,3:179\n1550#2,3:185\n123#3,3:182\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltyCalculator.kt\ncom/squareup/loyalty/LoyaltyCalculator\n*L\n82#1:174\n82#1,2:175\n83#1,2:177\n131#1,3:179\n152#1,3:185\n142#1,3:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u001a\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u001f\u0010\u0014\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\rH\u0002\u00a2\u0006\u0002\u0010\u0015J\u001a\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\u001f\u0010\u0019\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u001bH\u0002\u00a2\u0006\u0002\u0010\u001cJ\u001f\u0010\u001d\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002\u00a2\u0006\u0002\u0010\u001fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyCalculator;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "(Lcom/squareup/loyalty/LoyaltySettings;)V",
        "calculateCartPoints",
        "",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/payment/Transaction;)Ljava/lang/Integer;",
        "getLoyaltyApplicableTotal",
        "Lcom/squareup/protos/common/Money;",
        "accrualRules",
        "Lcom/squareup/server/account/protos/AccrualRules;",
        "isCartItemExcluded",
        "",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "accrualRulesOptions",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        "itemRules",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Ljava/lang/Integer;",
        "matchesCatalogObject",
        "catalogObject",
        "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
        "spendRateRules",
        "total",
        "Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Ljava/lang/Integer;",
        "visitRules",
        "Lcom/squareup/server/account/protos/AccrualRules$VisitRules;",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Ljava/lang/Integer;",
        "loyalty-calculator_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyCalculator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-void
.end method

.method private final itemRules(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Ljava/lang/Integer;
    .locals 7

    .line 81
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object p1

    const-string/jumbo v0, "transaction.items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 175
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v2, "cartItem"

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 82
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p2, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, v3, v2}, Lcom/squareup/loyalty/LoyaltyCalculator;->isCartItemExcluded(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 177
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 84
    iget-object v4, p2, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    iget-object v4, v4, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;->catalog_object_rules:Ljava/util/List;

    const-string v5, "accrualRules.item_rules.catalog_object_rules"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Iterable;

    .line 85
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v6, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->catalog_object:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    invoke-direct {p0, v3, v6}, Lcom/squareup/loyalty/LoyaltyCalculator;->matchesCatalogObject(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    check-cast v5, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;

    if-eqz v5, :cond_2

    .line 86
    iget-object v4, v5, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;->points:Ljava/lang/Long;

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v5, v4

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    :goto_3
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v3}, Ljava/math/BigDecimal;->intValue()I

    move-result v3

    mul-int v5, v5, v3

    add-int/2addr v1, v5

    goto :goto_1

    .line 89
    :cond_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method private final matchesCatalogObject(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 159
    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->catalog_object_type:Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    const/4 v2, 0x0

    if-nez v1, :cond_1

    goto :goto_3

    :cond_1
    sget-object v3, Lcom/squareup/loyalty/LoyaltyCalculator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject$CatalogObjectType;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eq v1, v3, :cond_4

    if-eq v1, v4, :cond_2

    goto :goto_3

    .line 165
    :cond_2
    iget-object p2, p2, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->token:Ljava/lang/String;

    .line 166
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->category:Lcom/squareup/api/items/MenuCategory;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/api/items/MenuCategory;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object p1, v0

    .line 165
    :goto_1
    invoke-static {p2, p1, v2, v4, v0}, Lkotlin/text/StringsKt;->equals$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    goto :goto_3

    .line 161
    :cond_4
    iget-object p2, p2, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->token:Ljava/lang/String;

    .line 162
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const-string v1, "cartItem.selectedVariation"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getItemVariation()Lcom/squareup/api/items/ItemVariation;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz p1, :cond_5

    iget-object p1, p1, Lcom/squareup/api/items/MerchantCatalogObjectReference;->catalog_object_token:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move-object p1, v0

    .line 161
    :goto_2
    invoke-static {p2, p1, v2, v4, v0}, Lkotlin/text/StringsKt;->equals$default(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    :goto_3
    return v2
.end method

.method private final spendRateRules(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Ljava/lang/Integer;
    .locals 4

    .line 63
    iget-object v0, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->spend:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->points:Ljava/lang/Long;

    if-nez v0, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->spend:Lcom/squareup/protos/common/Money;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v2, "spendRateRules.spend!!.amount"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long/2addr v0, v2

    .line 69
    iget-object p1, p2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->points:Ljava/lang/Long;

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    long-to-int p2, p1

    long-to-int p1, v0

    mul-int p2, p2, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private final visitRules(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Ljava/lang/Integer;
    .locals 1

    .line 50
    iget-object v0, p2, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 52
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 55
    :cond_0
    iget-object p1, p2, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    long-to-int p2, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method


# virtual methods
.method public calculateCartPoints(Lcom/squareup/payment/Transaction;)Ljava/lang/Integer;
    .locals 4

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyCalculator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->accrualRules()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 28
    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const-string v3, "it"

    if-eqz v2, :cond_0

    .line 29
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->getLoyaltyApplicableTotal(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 30
    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    const-string v1, "it.visit_rules"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->visitRules(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$VisitRules;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 32
    :cond_0
    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v2, :cond_1

    .line 33
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->getLoyaltyApplicableTotal(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 34
    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    const-string v1, "it.spend_rate_rules"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->spendRateRules(Lcom/squareup/protos/common/Money;Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 36
    :cond_1
    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    if-eqz v2, :cond_2

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->itemRules(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v1
.end method

.method public getLoyaltyApplicableTotal(Lcom/squareup/payment/Transaction;Lcom/squareup/server/account/protos/AccrualRules;)Lcom/squareup/protos/common/Money;
    .locals 5

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accrualRules"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 106
    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->include_tax:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->include_tax:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 108
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    const-string v4, "cartItem"

    .line 109
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p2, Lcom/squareup/server/account/protos/AccrualRules;->accrual_rules_options:Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, v3, v4}, Lcom/squareup/loyalty/LoyaltyCalculator;->isCartItemExcluded(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 112
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v4

    if-nez v4, :cond_3

    .line 114
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/payment/Order;->getAdjustedTotalForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_2

    :cond_4
    if-nez v1, :cond_3

    .line 118
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/squareup/payment/Order;->getTaxAmountForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_2

    :cond_5
    const-string/jumbo p1, "total"

    .line 122
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isCartItemExcluded(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Z
    .locals 6

    const-string v0, "cartItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 126
    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz p2, :cond_1

    .line 127
    iget-object v3, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    goto :goto_1

    :cond_1
    move-object v3, v0

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz p2, :cond_2

    .line 128
    iget-object v4, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    goto :goto_2

    :cond_2
    move-object v4, v0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz p2, :cond_3

    .line 129
    iget-object v0, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    :cond_3
    if-eqz v0, :cond_6

    .line 131
    move-object p2, v0

    check-cast p2, Ljava/lang/Iterable;

    .line 179
    instance-of v5, p2, Ljava/util/Collection;

    if-eqz v5, :cond_4

    move-object v5, p2

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    goto :goto_3

    .line 180
    :cond_4
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    .line 131
    invoke-direct {p0, p1, v5}, Lcom/squareup/loyalty/LoyaltyCalculator;->matchesCatalogObject(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 135
    :cond_6
    :goto_3
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result p2

    const/4 v5, 0x0

    if-eqz p2, :cond_7

    goto/16 :goto_6

    .line 138
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result p2

    if-eqz p2, :cond_8

    goto/16 :goto_6

    :cond_8
    if-eqz v1, :cond_c

    .line 142
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p2

    const-string v1, "cartItem.appliedDiscounts()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    const/4 p2, 0x0

    goto :goto_4

    .line 183
    :cond_a
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_b
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 142
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 p2, 0x1

    :goto_4
    if-eqz p2, :cond_c

    goto :goto_6

    :cond_c
    if-eqz v3, :cond_d

    .line 145
    iget-object p2, p1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    const-string v1, "cartItem.appliedDiscounts"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v2

    if-eqz p2, :cond_d

    goto :goto_6

    :cond_d
    if-eqz v4, :cond_e

    .line 148
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p2

    if-eqz p2, :cond_e

    goto :goto_6

    :cond_e
    if-eqz v0, :cond_12

    .line 152
    check-cast v0, Ljava/lang/Iterable;

    .line 185
    instance-of p2, v0, Ljava/util/Collection;

    if-eqz p2, :cond_10

    move-object p2, v0

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_10

    :cond_f
    const/4 p1, 0x0

    goto :goto_5

    .line 186
    :cond_10
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_11
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;

    .line 152
    invoke-direct {p0, p1, v0}, Lcom/squareup/loyalty/LoyaltyCalculator;->matchesCatalogObject(Lcom/squareup/checkout/CartItem;Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;)Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 p1, 0x1

    :goto_5
    if-eqz p1, :cond_12

    goto :goto_6

    :cond_12
    const/4 v2, 0x0

    :goto_6
    return v2
.end method
