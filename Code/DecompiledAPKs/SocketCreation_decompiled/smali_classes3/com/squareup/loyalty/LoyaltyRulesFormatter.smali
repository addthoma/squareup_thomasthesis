.class public Lcom/squareup/loyalty/LoyaltyRulesFormatter;
.super Ljava/lang/Object;
.source "LoyaltyRulesFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000e\u0008\u0001\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ$\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\nH\u0002J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0012\u0010\u0017\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u0019H\u0016J\n\u0010\u001a\u001a\u0004\u0018\u00010\u000fH\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
        "",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/CountryCode;)V",
        "format",
        "",
        "resId",
        "",
        "points",
        "",
        "min",
        "generateTosAcceptance",
        "Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;",
        "legalText",
        "shortVersion",
        "",
        "loyaltyProgramRequirements",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final countryCode:Lcom/squareup/CountryCode;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/CountryCode;)V
    .locals 1
    .param p4    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/Shorter;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltySettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p2, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iput-object p3, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->countryCode:Lcom/squareup/CountryCode;

    return-void
.end method

.method private final format(IJLcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 56
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual {v0, p2, p3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(J)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "points"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 57
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    long-to-int p3, p2

    invoke-virtual {v0, p3}, Lcom/squareup/loyalty/PointsTermsFormatter;->getTerm(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "points_terminology"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 58
    iget-object p2, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "minimum_amount"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic legalText$default(Lcom/squareup/loyalty/LoyaltyRulesFormatter;ZILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 67
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->legalText(Z)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: legalText"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public generateTosAcceptance()Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;
    .locals 2

    .line 93
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 95
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyTermsOfService;

    move-result-object v0

    const-string v1, "LoyaltyTermsOfService.Bu\u2026rsion(2)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public legalText(Z)Ljava/lang/String;
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->res:Lcom/squareup/util/Res;

    .line 69
    iget-object v1, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->countryCode:Lcom/squareup/CountryCode;

    sget-object v2, Lcom/squareup/loyalty/LoyaltyRulesFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    if-eqz p1, :cond_0

    .line 77
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_enroll_button_help_shorter:I

    goto :goto_0

    .line 79
    :cond_0
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_enroll_button_help:I

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 71
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_enroll_button_help_shorter_marketing_integration:I

    goto :goto_0

    .line 73
    :cond_2
    sget p1, Lcom/squareup/loyalty/R$string;->loyalty_enroll_button_help_marketing_integration:I

    .line 68
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public loyaltyProgramRequirements()Ljava/lang/String;
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->accrualRules()Lcom/squareup/server/account/protos/AccrualRules;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 35
    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_2

    .line 37
    sget v1, Lcom/squareup/loyalty/R$string;->loyalty_rule_visit:I

    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iget-object v2, v2, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->points:Ljava/lang/Long;

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v3, "rules.visit_rules.points!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules;->visit_rules:Lcom/squareup/server/account/protos/AccrualRules$VisitRules;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules$VisitRules;->minimum_spend:Lcom/squareup/protos/common/Money;

    .line 36
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->format(IJLcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    if-eqz v0, :cond_3

    .line 40
    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    goto :goto_1

    :cond_3
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_5

    .line 42
    sget v1, Lcom/squareup/loyalty/R$string;->loyalty_rule_spend:I

    iget-object v2, v0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iget-object v2, v2, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->points:Ljava/lang/Long;

    if-nez v2, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    const-string v3, "rules.spend_rate_rules.points!!"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 43
    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules;->spend_rate_rules:Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccrualRules$SpendRateRules;->spend:Lcom/squareup/protos/common/Money;

    .line 41
    invoke-direct {p0, v1, v2, v3, v0}, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->format(IJLcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_6

    .line 46
    iget-object v1, v0, Lcom/squareup/server/account/protos/AccrualRules;->item_rules:Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    :cond_6
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v1, Lcom/squareup/loyalty/R$string;->loyalty_rule_item:I

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 47
    :cond_7
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRulesFormatter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->qualifyingPurchaseDescription()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0
.end method
