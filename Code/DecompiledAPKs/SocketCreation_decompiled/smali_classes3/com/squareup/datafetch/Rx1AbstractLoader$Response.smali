.class public Lcom/squareup/datafetch/Rx1AbstractLoader$Response;
.super Ljava/lang/Object;
.source "Rx1AbstractLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/Rx1AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final error:Lcom/squareup/datafetch/LoaderError;

.field public final fetchedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TTItem;>;"
        }
    .end annotation
.end field

.field public final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field public final nextPagingKey:Ljava/lang/String;

.field public final pagingParams:Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Lcom/squareup/datafetch/LoaderError;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Lcom/squareup/datafetch/LoaderError;",
            ")V"
        }
    .end annotation

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->input:Ljava/lang/Object;

    .line 222
    iput-object p2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->pagingParams:Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;

    .line 223
    iput-object p3, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->error:Lcom/squareup/datafetch/LoaderError;

    const/4 p1, 0x0

    .line 224
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->fetchedItems:Ljava/util/List;

    .line 225
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->nextPagingKey:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            "Ljava/util/List<",
            "TTItem;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->input:Ljava/lang/Object;

    .line 214
    iput-object p2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->pagingParams:Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;

    const/4 p1, 0x0

    .line 215
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->error:Lcom/squareup/datafetch/LoaderError;

    .line 216
    iput-object p3, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->fetchedItems:Ljava/util/List;

    .line 217
    iput-object p4, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Response;->nextPagingKey:Ljava/lang/String;

    return-void
.end method
