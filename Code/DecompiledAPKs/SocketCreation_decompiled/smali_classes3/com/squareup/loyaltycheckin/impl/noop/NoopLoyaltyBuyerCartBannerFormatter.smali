.class public final Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyBuyerCartBannerFormatter;
.super Ljava/lang/Object;
.source "NoopLoyaltyBuyerCartBannerFormatter.kt"

# interfaces
.implements Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/impl/noop/NoopLoyaltyBuyerCartBannerFormatter;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBannerFormatter;",
        "()V",
        "banner",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public banner()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
            ">;"
        }
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;->INSTANCE:Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(NoBanner)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
