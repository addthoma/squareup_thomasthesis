.class final Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;
.super Ljava/lang/Object;
.source "ConfigureItemViewFactory.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createVariablePriceOnlySectionCustomItemVariation(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Lcom/squareup/text/EmptyTextWatcher;Lcom/squareup/money/PriceLocaleHelper;)Lcom/squareup/widgets/OnScreenRectangleEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onFocusChange",
        "com/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $priceLocaleHelper$inlined:Lcom/squareup/money/PriceLocaleHelper;

.field final synthetic $text$inlined:Ljava/lang/CharSequence;

.field final synthetic $textWatcher$inlined:Lcom/squareup/text/EmptyTextWatcher;

.field final synthetic $this_apply:Lcom/squareup/widgets/OnScreenRectangleEditText;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/OnScreenRectangleEditText;Ljava/lang/CharSequence;Lcom/squareup/text/EmptyTextWatcher;Lcom/squareup/money/PriceLocaleHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$text$inlined:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$textWatcher$inlined:Lcom/squareup/text/EmptyTextWatcher;

    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$priceLocaleHelper$inlined:Lcom/squareup/money/PriceLocaleHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .line 603
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$priceLocaleHelper$inlined:Lcom/squareup/money/PriceLocaleHelper;

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt$createVariablePriceOnlySectionCustomItemVariation$$inlined$apply$lambda$1;->$this_apply:Lcom/squareup/widgets/OnScreenRectangleEditText;

    check-cast p2, Landroid/widget/TextView;

    sget v0, Lcom/squareup/configure/item/R$string;->item_variation_price_hint_unfocused:I

    invoke-virtual {p1, p2, v0}, Lcom/squareup/money/PriceLocaleHelper;->setHint(Landroid/widget/TextView;I)V

    return-void
.end method
