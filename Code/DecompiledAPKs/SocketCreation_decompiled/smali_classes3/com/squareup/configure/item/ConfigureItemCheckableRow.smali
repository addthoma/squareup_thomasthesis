.class public final Lcom/squareup/configure/item/ConfigureItemCheckableRow;
.super Landroid/widget/LinearLayout;
.source "ConfigureItemCheckableRow.kt"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;,
        Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;,
        Lcom/squareup/configure/item/ConfigureItemCheckableRow$CheckableRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0003-./B\u001b\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u001f\u001a\u00020\u000bH\u0016J\u0008\u0010 \u001a\u00020!H\u0014J\u0010\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u000bH\u0016J\u0012\u0010$\u001a\u00020!2\u0008\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u000e\u0010\'\u001a\u00020!2\u0006\u0010(\u001a\u00020\u000bJ\u0012\u0010)\u001a\u00020!2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0016J\u0008\u0010,\u001a\u00020!H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000b8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R$\u0010\u0012\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u00118F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R$\u0010\u0018\u001a\u00020\u00172\u0006\u0010\n\u001a\u00020\u00178F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0019\u0010\u001a\"\u0004\u0008\u001b\u0010\u001cR(\u0010\n\u001a\u0004\u0018\u00010\u00112\u0008\u0010\n\u001a\u0004\u0018\u00010\u00118F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001d\u0010\u0014\"\u0004\u0008\u001e\u0010\u0016\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
        "Landroid/widget/LinearLayout;",
        "Landroid/widget/Checkable;",
        "context",
        "Landroid/content/Context;",
        "attributeSet",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "adapter",
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;",
        "value",
        "",
        "childEnabled",
        "getChildEnabled",
        "()Z",
        "setChildEnabled",
        "(Z)V",
        "",
        "label",
        "getLabel",
        "()Ljava/lang/String;",
        "setLabel",
        "(Ljava/lang/String;)V",
        "",
        "rowId",
        "getRowId",
        "()I",
        "setRowId",
        "(I)V",
        "getValue",
        "setValue",
        "isChecked",
        "onFinishInflate",
        "",
        "setChecked",
        "checked",
        "setOnClickListener",
        "l",
        "Landroid/view/View$OnClickListener;",
        "setSingleSelect",
        "isSingleSelect",
        "startAnimation",
        "animation",
        "Landroid/view/animation/Animation;",
        "toggle",
        "CheckableRow",
        "ConfigureItemCheckableRowAdapter",
        "SelectableRow",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final getChildEnabled()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->getLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRowId()I
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->getRowId()I

    move-result v0

    return v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isChecked()Z
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->isChecked()Z

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 25
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 26
    sget v0, Lcom/squareup/configure/item/R$id;->checkable_row:I

    invoke-virtual {p0, v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 27
    instance-of v1, v0, Lcom/squareup/noho/NohoSelectable;

    if-eqz v1, :cond_0

    .line 28
    new-instance v1, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;

    check-cast v0, Lcom/squareup/noho/NohoSelectable;

    invoke-direct {v1, v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;-><init>(Lcom/squareup/noho/NohoSelectable;)V

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    goto :goto_0

    .line 30
    :cond_0
    instance-of v1, v0, Lcom/squareup/noho/NohoCheckableRow;

    if-eqz v1, :cond_1

    .line 31
    new-instance v1, Lcom/squareup/configure/item/ConfigureItemCheckableRow$CheckableRow;

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    invoke-direct {v1, v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$CheckableRow;-><init>(Lcom/squareup/noho/NohoCheckableRow;)V

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    .line 26
    :goto_0
    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    return-void

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected one of NohoSelectable or NohoCheckableRow"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public setChecked(Z)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setChecked(Z)V

    return-void
.end method

.method public final setChildEnabled(Z)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setEnabled(Z)V

    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setRowId(I)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setRowId(I)V

    return-void
.end method

.method public final setSingleSelect(Z)V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setSingleSelect(Z)V

    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public startAnimation(Landroid/view/animation/Animation;)V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public toggle()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->adapter:Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;

    if-nez v0, :cond_0

    const-string v1, "adapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->toggle()V

    return-void
.end method
