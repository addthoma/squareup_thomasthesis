.class final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->success()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/protos/client/Employee;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "optionalEmployee",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/Employee;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1355
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->invoke(Lcom/squareup/util/Optional;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/util/Optional;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/Employee;",
            ">;)V"
        }
    .end annotation

    .line 1362
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    .line 1363
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-object v1, v1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$discount:Lcom/squareup/checkout/Discount;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-boolean v2, v2, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$applied:Z

    .line 1364
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/Employee;

    .line 1362
    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/configure/item/ConfigureItemState;->setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V

    .line 1366
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$hasView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1367
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-object p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemDetailView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1$success$2;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDiscountRowChanged$1;->$discountId:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailView;->updateDiscountCheckedState(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method
