.class public Lcom/squareup/configure/item/ConfigureItemDetailView;
.super Landroid/widget/LinearLayout;
.source "ConfigureItemDetailView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/VisualTransitionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;,
        Lcom/squareup/configure/item/ConfigureItemDetailView$Component;
    }
.end annotation


# static fields
.field public static final ITEM_OPTION_GROUP_ID_ORIGIN:I = 0x7d0

.field public static final MODIFIER_GROUP_ID_ORIGIN:I = 0x3e8

.field public static final VARIATION_GROUP_ID_ORIGIN:I = 0x3e7


# instance fields
.field private bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

.field private content:Landroid/view/ViewGroup;

.field currencyCode:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private diningOptionsGroup:Lcom/squareup/widgets/CheckableGroup;

.field private discountRowsByDiscountId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/widgets/list/ToggleButtonRow;",
            ">;"
        }
    .end annotation
.end field

.field private durationRow:Lcom/squareup/noho/NohoRow;

.field private durationRowContainer:Landroid/view/View;

.field private finalDurationRow:Lcom/squareup/noho/NohoRow;

.field private fixedPriceOverrideButton:Landroid/widget/Button;

.field private fixedPriceOverrideContainer:Landroid/view/ViewGroup;

.field private gapDurationRow:Lcom/squareup/noho/NohoRow;

.field private gapTimeRowContainer:Landroid/view/View;

.field private gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private final groupToIndexToItemOptionValue:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private hideablePlugin:Lcom/squareup/noho/HideablePlugin;

.field private initialDurationRow:Lcom/squareup/noho/NohoRow;

.field protected isRestoringState:Z

.field private final itemOptionGroups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/widgets/CheckableGroup;",
            ">;"
        }
    .end annotation
.end field

.field private final itemOptionValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/configure/item/ItemOptionsValueRowIndex;",
            ">;"
        }
    .end annotation
.end field

.field localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final modifierGroups:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/widgets/CheckableGroup;",
            ">;"
        }
    .end annotation
.end field

.field private modifierReboundAnimation:Lcom/squareup/register/widgets/validation/ReboundAnimation;

.field private final modifierViews:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/configure/item/ConfigureItemCheckableRow;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;"
        }
    .end annotation
.end field

.field private noteRow:Lcom/squareup/marketfont/MarketEditText;

.field presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private quantityFooter:Lcom/squareup/marketfont/MarketTextView;

.field private quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field private quantitySectionHeader:Lcom/squareup/marketfont/MarketTextView;

.field private final requestUnitQuantityRowFocusAndKeyboard:Ljava/lang/Runnable;

.field scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private title:Landroid/view/View;

.field private unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

.field private unitQuantityTextWatcher:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;

.field private unitSuffix:Lcom/squareup/ui/SuffixPlugin;

.field private variableAmountButton:Landroid/widget/Button;

.field private variableAmountEditText:Lcom/squareup/widgets/OnScreenRectangleEditText;

.field private variationGroup:Lcom/squareup/widgets/CheckableGroup;

.field private warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 185
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    .line 127
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierViews:Ljava/util/Map;

    .line 128
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->itemOptionGroups:Ljava/util/Map;

    .line 129
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->itemOptionValues:Ljava/util/Map;

    .line 130
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->groupToIndexToItemOptionValue:Ljava/util/Map;

    .line 171
    new-instance p2, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$D-Bi_JTY9Qw8t74vfVgF7Q4nggI;

    invoke-direct {p2, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$D-Bi_JTY9Qw8t74vfVgF7Q4nggI;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->requestUnitQuantityRowFocusAndKeyboard:Ljava/lang/Runnable;

    .line 186
    const-class p2, Lcom/squareup/configure/item/ConfigureItemDetailView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemDetailView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$Component;->inject(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    return-void
.end method

.method private buildQuantityRow(Landroid/view/View;)V
    .locals 1

    .line 333
    sget v0, Lcom/squareup/configure/item/R$id;->edit_quantity_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 334
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 335
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->isZeroQuantityAllowed()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setAllowZero(Z)V

    .line 336
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$AedpX87aZPQd6i-hJAK12byDf6o;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$AedpX87aZPQd6i-hJAK12byDf6o;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    return-void
.end method

.method private createLockIconHideablePlugin()Lcom/squareup/noho/HideablePlugin;
    .locals 5

    .line 740
    new-instance v0, Landroid/view/ContextThemeWrapper;

    .line 741
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$style;->Theme_Noho:I

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 742
    sget v1, Lcom/squareup/configure/item/R$drawable;->ui_lock_24_with_inset:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 743
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    .line 744
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 745
    new-instance v1, Lcom/squareup/configure/item/BaselineAlignmentPlugin;

    new-instance v2, Lcom/squareup/noho/IconPlugin;

    sget-object v3, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v0, v4}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;I)V

    invoke-direct {v1, v2}, Lcom/squareup/configure/item/BaselineAlignmentPlugin;-><init>(Lcom/squareup/noho/IconPlugin;)V

    .line 747
    new-instance v0, Lcom/squareup/noho/HideablePlugin;

    invoke-direct {v0, v1, v4}, Lcom/squareup/noho/HideablePlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V

    return-object v0
.end method


# virtual methods
.method protected buildButtonsContainer(Z)V
    .locals 2

    .line 536
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1, p1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createButtonsContainer(Landroid/content/Context;Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method protected buildConfigurationDisplayRow(Ljava/lang/CharSequence;)V
    .locals 2

    .line 274
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1, p1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createConfigurationRow(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected buildDiningOptionsList(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;)V"
        }
    .end annotation

    .line 496
    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$0eyyGCteHb2UVacHJ0l8v1N7O98;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$0eyyGCteHb2UVacHJ0l8v1N7O98;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 499
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    const/16 v3, 0x3e7

    invoke-static {v1, v2, p1, p2, v3}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createDiningOptionsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Ljava/util/List;I)Lcom/squareup/widgets/CheckableGroup;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->diningOptionsGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 501
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->diningOptionsGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method protected buildDiscountRows(Ljava/util/List;ILcom/squareup/quantity/PerUnitFormatter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;I",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")V"
        }
    .end annotation

    .line 518
    new-instance v3, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$FsN9Mp3VQcxzBiISyRx4V8Yr7V8;

    invoke-direct {v3, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$FsN9Mp3VQcxzBiISyRx4V8Yr7V8;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 520
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    add-int/lit16 v4, p2, 0x3e8

    move-object v2, p1

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createDiscountSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILcom/squareup/quantity/PerUnitFormatter;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->discountRowsByDiscountId:Ljava/util/Map;

    return-void
.end method

.method protected buildDurationRow()V
    .locals 2

    .line 480
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createDurationRow(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 481
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_duration_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->durationRowContainer:Landroid/view/View;

    .line 482
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_duration:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->durationRow:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method protected buildFixedPriceOverridePriceButton()V
    .locals 3

    .line 255
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$4;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$4;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 261
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v1, v2, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createFixedPriceOverrideSection(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->fixedPriceOverrideButton:Landroid/widget/Button;

    .line 262
    sget v0, Lcom/squareup/configure/item/R$id;->fixed_price_override_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->fixedPriceOverrideContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method protected buildGapTimeRows()V
    .locals 2

    .line 486
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createGapTimeRows(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 487
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_gap_time_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeRowContainer:Landroid/view/View;

    .line 488
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_gap_time_toggle:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 489
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_initial_duration:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoRow;

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    .line 490
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_gap_duration:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoRow;

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    .line 491
    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_final_duration:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method protected buildItemDescriptionSection(Ljava/lang/String;)V
    .locals 2

    .line 525
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1, p1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createItemDescriptionSection(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;)V

    return-void
.end method

.method protected buildMultiChoiceModifierList(ILjava/util/SortedMap;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/quantity/PerUnitFormatter;ILjava/lang/CharSequence;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "I",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 314
    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$ACbyyYFPxitg1iguOmaeowzIGIo;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$ACbyyYFPxitg1iguOmaeowzIGIo;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 319
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    move/from16 v11, p6

    add-int/lit16 v8, v11, 0x3e8

    iget-object v10, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierViews:Ljava/util/Map;

    move-object/from16 v4, p5

    move-object v5, p3

    move-object v6, p2

    move-object/from16 v7, p4

    move-object/from16 v9, p7

    invoke-static/range {v2 .. v10}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createMultiChoiceGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/util/Map;)Lcom/squareup/widgets/CheckableGroup;

    move-result-object v2

    .line 322
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/CheckableGroup;->setTag(Ljava/lang/Object;)V

    .line 323
    invoke-virtual {v2, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 324
    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected buildNotesRow()V
    .locals 3

    .line 447
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$5;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$5;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 453
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v1, v2, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createNotesRow(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/text/TextWatcher;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$id;->configure_item_note:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketEditText;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    .line 459
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->setImeOptions(I)V

    .line 467
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$f6bE7sHFLWw3SYLkUAojXIlIHok;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$f6bE7sHFLWw3SYLkUAojXIlIHok;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method protected buildQuantityAndUnitQuantityRows()V
    .locals 5

    .line 345
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createQuantitySection(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 347
    sget v1, Lcom/squareup/configure/item/R$id;->quantity_section_header:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketTextView;

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantitySectionHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 349
    invoke-direct {p0, v0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->buildQuantityRow(Landroid/view/View;)V

    .line 351
    sget v1, Lcom/squareup/configure/item/R$id;->edit_unit_quantity_row:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoEditRow;

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    .line 359
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const v2, 0x10000005

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setImeOptions(I)V

    .line 360
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v2, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$X1MnSg-2SJyR2Fn_CNExMOxyeHc;

    invoke-direct {v2, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$X1MnSg-2SJyR2Fn_CNExMOxyeHc;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 373
    new-instance v1, Lcom/squareup/ui/SuffixPlugin;

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$style;->TextAppearance_ConfigureItem_EditTextSuffix:I

    sget v4, Lcom/squareup/configure/item/R$style;->TextAppearance_ConfigureItem_EditTextSuffix_Hint:I

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/SuffixPlugin;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitSuffix:Lcom/squareup/ui/SuffixPlugin;

    .line 376
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitSuffix:Lcom/squareup/ui/SuffixPlugin;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 378
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->createLockIconHideablePlugin()Lcom/squareup/noho/HideablePlugin;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideablePlugin:Lcom/squareup/noho/HideablePlugin;

    .line 379
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideablePlugin:Lcom/squareup/noho/HideablePlugin;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 381
    sget v1, Lcom/squareup/configure/item/R$id;->quantity_footer_text:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityFooter:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method protected buildSingleChoiceModifierList(ILjava/util/SortedMap;Ljava/lang/CharSequence;Ljava/lang/String;Lcom/squareup/quantity/PerUnitFormatter;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "I)V"
        }
    .end annotation

    .line 297
    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$sLnDTQeLxHGDsaz5CCYugojXw48;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$sLnDTQeLxHGDsaz5CCYugojXw48;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 302
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    add-int/lit16 v7, p6, 0x3e8

    iget-object v8, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierViews:Ljava/util/Map;

    move-object v3, p5

    move-object v4, p3

    move-object v5, p2

    move-object v6, p4

    invoke-static/range {v1 .. v8}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createSingleChoiceGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/SortedMap;Ljava/lang/String;ILjava/util/Map;)Lcom/squareup/widgets/CheckableGroup;

    move-result-object p2

    .line 305
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/CheckableGroup;->setTag(Ljava/lang/Object;)V

    .line 306
    invoke-virtual {p2, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 307
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected buildTaxRows(Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;I)V"
        }
    .end annotation

    .line 505
    new-instance v3, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$DVpZjKtdUZ4Emc4OMjpsPuxeQVo;

    invoke-direct {v3, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$DVpZjKtdUZ4Emc4OMjpsPuxeQVo;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 507
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    add-int/lit16 v4, p2, 0x3e8

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    .line 509
    invoke-virtual {p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getConditionalTaxesHelpText()Ljava/lang/CharSequence;

    move-result-object v5

    move-object v2, p1

    .line 507
    invoke-static/range {v0 .. v5}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createTaxSwitchGroup(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/util/List;Landroid/widget/CompoundButton$OnCheckedChangeListener;ILjava/lang/CharSequence;)Ljava/util/Map;

    return-void
.end method

.method protected buildTitleForCustomAmount(Ljava/lang/CharSequence;)V
    .locals 3

    .line 220
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$1;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$1;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 226
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v1, v2, v0, p1}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createTitle(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/text/EmptyTextWatcher;Ljava/lang/CharSequence;)Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->title:Landroid/view/View;

    return-void
.end method

.method protected buildVariablePriceButton(Ljava/lang/CharSequence;)V
    .locals 3

    .line 230
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$2;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$2;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 235
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-static {v1, v2, p1, v0}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createVariablePriceOnlySection(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variableAmountButton:Landroid/widget/Button;

    return-void
.end method

.method protected buildVariablePriceEditText(Ljava/lang/CharSequence;)V
    .locals 4

    .line 239
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$3;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$3;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 250
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    invoke-static {v1, v2, p1, v0, v3}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createVariablePriceOnlySectionCustomItemVariation(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/CharSequence;Lcom/squareup/text/EmptyTextWatcher;Lcom/squareup/money/PriceLocaleHelper;)Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variableAmountEditText:Lcom/squareup/widgets/OnScreenRectangleEditText;

    return-void
.end method

.method protected buildVariationsRow(Ljava/lang/String;Ljava/util/List;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ")V"
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$KsHbASZ0A4lxe3ydcgakaFjWsb4;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$KsHbASZ0A4lxe3ydcgakaFjWsb4;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 285
    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$_jOzLqcf7P2-jUbFpG7FCY3fkwU;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$_jOzLqcf7P2-jUbFpG7FCY3fkwU;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 288
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    const/16 v7, 0x3e7

    move-object v4, p3

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v2 .. v7}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->createVariationsGroup(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/CharSequence;Ljava/util/List;I)Lcom/squareup/widgets/CheckableGroup;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variationGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 290
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variationGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 291
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variationGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedClickListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;)V

    return-void
.end method

.method protected clearContents()V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public clearDiningOptionSelected()V
    .locals 1

    .line 670
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->diningOptionsGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    return-void
.end method

.method public clearUnitQuantityRowContent()V
    .locals 2

    .line 415
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    .line 416
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected configureCompButton()V
    .locals 3

    .line 555
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$6;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$6;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 561
    sget v1, Lcom/squareup/configure/item/R$id;->item_comp_button_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketButton;

    const/4 v2, 0x0

    .line 562
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 563
    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected configureDeleteButton()V
    .locals 4

    .line 540
    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$aydmCPu2RHMXTMAu3LOkc2GCxZc;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$aydmCPu2RHMXTMAu3LOkc2GCxZc;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 542
    sget v1, Lcom/squareup/configure/item/R$id;->item_remove_button_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ConfirmButton;

    .line 543
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->isService()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 544
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$string;->cart_remove_service:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 545
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$string;->cart_remove_service_confirm:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 547
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$string;->cart_remove_item:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setInitialText(Ljava/lang/CharSequence;)V

    .line 548
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/configure/item/R$string;->cart_remove_item_confirm:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setConfirmText(Ljava/lang/CharSequence;)V

    :goto_0
    const/4 v2, 0x0

    .line 550
    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 551
    invoke-virtual {v1, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method protected configureUncompButton()V
    .locals 3

    .line 567
    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$PRrVWSXYmEznc4Cc1gDIFNMG7BE;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$PRrVWSXYmEznc4Cc1gDIFNMG7BE;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 569
    sget v1, Lcom/squareup/configure/item/R$id;->item_uncomp_button_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ConfirmButton;

    const/4 v2, 0x0

    .line 570
    invoke-virtual {v1, v2}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 571
    invoke-virtual {v1, v0}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    return-void
.end method

.method protected configureVoidButton()V
    .locals 3

    .line 575
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemDetailView$7;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailView$7;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    .line 581
    sget v1, Lcom/squareup/configure/item/R$id;->item_void_button_row:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/marketfont/MarketButton;

    const/4 v2, 0x0

    .line 582
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 583
    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public disableUnitQuantityRowAndShowLockIcon()V
    .locals 2

    .line 715
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setEnabled(Z)V

    .line 716
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideablePlugin:Lcom/squareup/noho/HideablePlugin;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/HideablePlugin;->setVisible(Z)V

    return-void
.end method

.method public enableUnitQuantityRowAndHideLockIcon()V
    .locals 2

    .line 720
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 721
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setEnabled(Z)V

    .line 722
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideablePlugin:Lcom/squareup/noho/HideablePlugin;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/HideablePlugin;->setVisible(Z)V

    :cond_0
    return-void
.end method

.method protected getSelectableNoteText()Lcom/squareup/text/SelectableTextScrubber$SelectableText;
    .locals 4

    .line 602
    new-instance v0, Lcom/squareup/text/SelectableTextScrubber$SelectableText;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v2}, Lcom/squareup/marketfont/MarketEditText;->getSelectionStart()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    .line 603
    invoke-virtual {v3}, Lcom/squareup/marketfont/MarketEditText;->getSelectionEnd()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/text/SelectableTextScrubber$SelectableText;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public hideAllQuantityRows()V
    .locals 2

    .line 438
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 439
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    .line 441
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->clearFocus()V

    return-void
.end method

.method protected hideKeyboard()V
    .locals 0

    .line 711
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected hideOrShowFixedPriceOverridePrice(Z)V
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->fixedPriceOverrideContainer:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public hideQuantityFooter()V
    .locals 2

    .line 734
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityFooter:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 735
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public hideQuantitySectionHeader()V
    .locals 2

    .line 434
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantitySectionHeader:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$buildDiningOptionsList$8$ConfigureItemDetailView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 497
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDiningOptionSelected(I)V

    return-void
.end method

.method public synthetic lambda$buildDiscountRows$10$ConfigureItemDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 519
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDiscountRowChanged(Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$buildMultiChoiceModifierList$4$ConfigureItemDetailView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 315
    invoke-virtual {p1}, Lcom/squareup/widgets/CheckableGroup;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 316
    iget-object p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onModifierSelected(II)V

    return-void
.end method

.method public synthetic lambda$buildNotesRow$7$ConfigureItemDetailView(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .line 468
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketEditText;->hasFocus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 470
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p2

    and-int/lit16 p2, p2, 0xff

    const/16 v0, 0x8

    if-ne p2, v0, :cond_0

    .line 471
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return v2

    :cond_0
    return v1
.end method

.method public synthetic lambda$buildQuantityAndUnitQuantityRows$6$ConfigureItemDetailView(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x5

    if-ne p2, p1, :cond_0

    .line 367
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    if-eqz p1, :cond_0

    .line 368
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketEditText;->requestFocus()Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$buildQuantityRow$5$ConfigureItemDetailView(I)V
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onQuantityChanged(I)V

    return-void
.end method

.method public synthetic lambda$buildSingleChoiceModifierList$3$ConfigureItemDetailView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 1

    .line 298
    invoke-virtual {p1}, Lcom/squareup/widgets/CheckableGroup;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p1, p3}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onModifierSelected(II)V

    .line 300
    :cond_0
    iget-object p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p3, p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onModifierSelected(II)V

    return-void
.end method

.method public synthetic lambda$buildTaxRows$9$ConfigureItemDetailView(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 506
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onTaxRowChanged(Landroid/widget/Checkable;Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic lambda$buildVariationsRow$1$ConfigureItemDetailView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 281
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onVariationCheckChanged(II)V

    return-void
.end method

.method public synthetic lambda$buildVariationsRow$2$ConfigureItemDetailView(Lcom/squareup/widgets/CheckableGroup;I)Z
    .locals 0

    .line 286
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onSelectedVariationClicked(I)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$configureDeleteButton$11$ConfigureItemDetailView()V
    .locals 1

    .line 540
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDeleteButtonClicked()V

    return-void
.end method

.method public synthetic lambda$configureUncompButton$12$ConfigureItemDetailView()V
    .locals 1

    .line 567
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onUncompButtonClicked()V

    return-void
.end method

.method public synthetic lambda$new$0$ConfigureItemDetailView()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    .line 173
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$setGapRowContainerVisibility$13$ConfigureItemDetailView(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 632
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onGapRowCheckChanged(Z)V

    .line 633
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public synthetic lambda$setGapRowContainerVisibility$14$ConfigureItemDetailView(Landroid/view/View;)V
    .locals 0

    .line 636
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onInitialDurationRowClicked()V

    return-void
.end method

.method public synthetic lambda$setGapRowContainerVisibility$15$ConfigureItemDetailView(Landroid/view/View;)V
    .locals 0

    .line 637
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onGapDurationRowClicked()V

    return-void
.end method

.method public synthetic lambda$setGapRowContainerVisibility$16$ConfigureItemDetailView(Landroid/view/View;)V
    .locals 0

    .line 638
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onFinalDurationRowClicked()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onCancelSelected()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    if-eqz v0, :cond_0

    .line 203
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->requestUnitQuantityRowFocusAndKeyboard:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 207
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 190
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 191
    sget v0, Lcom/squareup/configure/item/R$id;->content:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    .line 194
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 195
    new-instance v1, Lcom/squareup/register/widgets/validation/ReboundAnimation;

    neg-float v0, v0

    invoke-direct {v1, v0}, Lcom/squareup/register/widgets/validation/ReboundAnimation;-><init>(F)V

    iput-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierReboundAnimation:Lcom/squareup/register/widgets/validation/ReboundAnimation;

    .line 196
    new-instance v0, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    .line 197
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 198
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 751
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method

.method public restoreHierarchyState(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 769
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->isRestoringState:Z

    .line 770
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    const/4 p1, 0x0

    .line 771
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->isRestoringState:Z

    .line 775
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->getQuantity()Ljava/math/BigDecimal;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 776
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->clearUnitQuantityRowContent()V

    :cond_0
    return-void
.end method

.method public setDiningOptionSelected(I)V
    .locals 1

    .line 666
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->diningOptionsGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method protected setDuration(Ljava/lang/String;)V
    .locals 1

    .line 617
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->durationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setDurationOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 621
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->durationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected setDurationRowVisibility(Z)V
    .locals 1

    .line 625
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->durationRowContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected setGapRowContainerVisibility(Z)V
    .locals 1

    .line 629
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeRowContainer:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 631
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$QP8OA3ojn3N_w_HLeRRQrcf7mzY;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$QP8OA3ojn3N_w_HLeRRQrcf7mzY;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 636
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$cHDeoKs8nPbtBsCAn_VoWyzJfJE;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$cHDeoKs8nPbtBsCAn_VoWyzJfJE;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 637
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$EV8IcefWZcNxuVyEePG0H-6KJ60;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$EV8IcefWZcNxuVyEePG0H-6KJ60;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$qKnvOhT4c9SvxKotEEZwL4r5KHY;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemDetailView$qKnvOhT4c9SvxKotEEZwL4r5KHY;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setGapRowInfo(Lcom/squareup/intermission/GapTimeInfo;)V
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 643
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 645
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 646
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 647
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    goto :goto_0

    .line 649
    :cond_0
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 651
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 652
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 653
    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 655
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/intermission/GapTimeInfo;->getInitialDuration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 656
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/intermission/GapTimeInfo;->getGapDuration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 657
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/intermission/GapTimeInfo;->getFinalDuration()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public setModifierChecked(II)V
    .locals 1

    .line 674
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method public setModifierUnchecked(II)V
    .locals 1

    .line 678
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/CheckableGroup;->uncheck(I)V

    return-void
.end method

.method protected setNoteText(Lcom/squareup/text/SelectableTextScrubber$SelectableText;)V
    .locals 2

    .line 597
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    iget-object v1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    .line 598
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    iget v1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionStart:I

    iget p1, p1, Lcom/squareup/text/SelectableTextScrubber$SelectableText;->selectionEnd:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marketfont/MarketEditText;->setSelection(II)V

    return-void
.end method

.method protected setNoteText(Ljava/lang/String;)V
    .locals 1

    .line 593
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->noteRow:Lcom/squareup/marketfont/MarketEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setQuantity(Ljava/math/BigDecimal;)V
    .locals 2

    .line 607
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->scopeRunner:Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemState;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-virtual {v1, p1}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 609
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    goto :goto_0

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 612
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->clearFocus()V

    :goto_0
    return-void
.end method

.method public setQuantitySectionHeader(I)V
    .locals 2

    .line 429
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantitySectionHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 430
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantitySectionHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    return-void
.end method

.method public setVariableAmountButton(Ljava/lang/CharSequence;)V
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variableAmountButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVariableAmountEditText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 703
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variableAmountEditText:Lcom/squareup/widgets/OnScreenRectangleEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setVariation(ILjava/lang/CharSequence;)V
    .locals 1

    .line 587
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variationGroup:Lcom/squareup/widgets/CheckableGroup;

    .line 588
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    .line 589
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setValue(Ljava/lang/String;)V

    return-void
.end method

.method public setVariationSelected(I)V
    .locals 1

    .line 662
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->variationGroup:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method public showQuantityFooterText(I)V
    .locals 2

    .line 727
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityFooter:Lcom/squareup/marketfont/MarketTextView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 728
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 729
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityFooter:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    :cond_0
    return-void
.end method

.method public showQuantityRow()V
    .locals 2

    .line 421
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailView;->hideKeyboard()V

    .line 422
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    .line 424
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->clearFocus()V

    return-void
.end method

.method public showShakeAnimationOnModifier(II)V
    .locals 1

    .line 698
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierGroups:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierReboundAnimation:Lcom/squareup/register/widgets/validation/ReboundAnimation;

    .line 699
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/CheckableGroup;->startAnimationOnCheckable(ILandroid/view/animation/Animation;)V

    return-void
.end method

.method public showUnitQuantityRow(Ljava/lang/String;IZ)V
    .locals 3

    .line 386
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitSuffix:Lcom/squareup/ui/SuffixPlugin;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/SuffixPlugin;->setText(Ljava/lang/String;)V

    .line 388
    new-instance p1, Lcom/squareup/quantity/BigDecimalFormatter;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->localeProvider:Ljavax/inject/Provider;

    sget-object v1, Lcom/squareup/quantity/BigDecimalFormatter$Format;->ROUNDING_SCALE:Lcom/squareup/quantity/BigDecimalFormatter$Format;

    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/quantity/BigDecimalFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/quantity/BigDecimalFormatter$Format;I)V

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 390
    new-instance p1, Lcom/squareup/text/DecimalScrubber;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    invoke-direct {p1, v0}, Lcom/squareup/text/DecimalScrubber;-><init>(Lcom/squareup/quantity/BigDecimalFormatter;)V

    .line 391
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {v1, p1, v0}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 393
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityTextWatcher:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;

    if-eqz p1, :cond_0

    .line 394
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 396
    :cond_0
    new-instance p1, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;

    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    .line 397
    invoke-virtual {v0}, Lcom/squareup/quantity/BigDecimalFormatter;->getDecimalSeparator()C

    move-result v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->presenter:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/configure/item/-$$Lambda$cM990Qe-GXnf5uqAPJ7khs9o8cM;

    invoke-direct {v2, v1}, Lcom/squareup/configure/item/-$$Lambda$cM990Qe-GXnf5uqAPJ7khs9o8cM;-><init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    invoke-direct {p1, v0, p2, v2}, Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;-><init>(CILcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher$OnUnitQuantityChangedListener;)V

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityTextWatcher:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;

    .line 399
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityTextWatcher:Lcom/squareup/configure/item/ConfigureItemDetailView$UnitQuantityTextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 401
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->bigDecimalFormatter:Lcom/squareup/quantity/BigDecimalFormatter;

    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p2, v0}, Lcom/squareup/quantity/BigDecimalFormatter;->format(Ljava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 403
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const-string p2, ""

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setVisibility(I)V

    .line 406
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->quantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->clearFocus()V

    .line 407
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    if-eqz p3, :cond_1

    .line 410
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->unitQuantityRow:Lcom/squareup/noho/NohoEditRow;

    iget-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->requestUnitQuantityRowFocusAndKeyboard:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public updateDiscountCheckedState(Ljava/lang/String;Z)V
    .locals 1

    .line 529
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->discountRowsByDiscountId:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 530
    invoke-virtual {p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    if-eq v0, p2, :cond_0

    const/4 v0, 0x0

    .line 531
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    :cond_0
    return-void
.end method

.method protected updateFixedPriceOverridePriceButton(Ljava/lang/CharSequence;)V
    .locals 1

    .line 266
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->fixedPriceOverrideButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateModifiers(Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)V
    .locals 4

    .line 683
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->modifierViews:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 684
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/OrderModifier;

    .line 685
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderModifier;->isFreeModifier()Z

    move-result v3

    if-nez v3, :cond_0

    .line 686
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/ConfigureItemCheckableRow;

    .line 689
    invoke-virtual {v2}, Lcom/squareup/checkout/OrderModifier;->getBasePriceTimesModifierQuantityOrNull()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 688
    invoke-virtual {p1, v2, p2}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 691
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 687
    invoke-virtual {v1, v2}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->setValue(Ljava/lang/String;)V

    .line 692
    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow;->invalidate()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected updateTaxRows(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/configure/item/AdjustmentPair;",
            ">;I)V"
        }
    .end annotation

    .line 513
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailView;->content:Landroid/view/ViewGroup;

    add-int/lit16 p2, p2, 0x3e8

    invoke-static {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemViewFactoryKt;->updateTaxesSwitchGroup(Landroid/view/ViewGroup;Ljava/util/List;I)V

    return-void
.end method
