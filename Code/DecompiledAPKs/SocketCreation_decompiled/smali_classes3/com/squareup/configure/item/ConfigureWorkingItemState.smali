.class public Lcom/squareup/configure/item/ConfigureWorkingItemState;
.super Lcom/squareup/configure/item/ConfigureItemState;
.source "ConfigureWorkingItemState.java"


# instance fields
.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final host:Lcom/squareup/configure/item/ConfigureItemHost;

.field public workingItem:Lcom/squareup/configure/item/WorkingItem;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/squareup/configure/item/ConfigureItemState;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    .line 58
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 59
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    return-void
.end method

.method public static emptyWorkingItemState(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureWorkingItemState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureWorkingItemState;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/configure/item/ConfigureWorkingItemState;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/configure/item/ConfigureWorkingItemState;-><init>(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V

    return-object v0
.end method

.method public static loadedWorkingItemState(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)Lcom/squareup/configure/item/ConfigureWorkingItemState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/configure/item/WorkingItem;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureWorkingItemState;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/configure/item/ConfigureWorkingItemState;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/configure/item/ConfigureWorkingItemState;-><init>(Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;)V

    .line 51
    invoke-virtual {v0, p2}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->load(Lcom/squareup/configure/item/WorkingItem;)V

    return-object v0
.end method


# virtual methods
.method public cacheHasHiddenModifier(Z)V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setHasHiddenModifier(Z)V

    return-void
.end method

.method public clearQuantityPrecisionOverride()V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v1, v1, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 162
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v1

    .line 161
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    return-void
.end method

.method public commit()Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
    .locals 2

    .line 115
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->NO_SELECTED_VARIATION:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->host:Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v1}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/configure/item/ConfigureItemHost;->commitCartItem(Lcom/squareup/checkout/CartItem;)V

    .line 117
    sget-object v0, Lcom/squareup/configure/item/ConfigureItemState$CommitResult;->SUCCESS:Lcom/squareup/configure/item/ConfigureItemState$CommitResult;

    return-object v0
.end method

.method public compItem(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    return-void
.end method

.method public delete()V
    .locals 0

    return-void
.end method

.method public getAppliedDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 265
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->appliedDiscounts:Ljava/util/Map;

    return-object v0
.end method

.method public getAppliedTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 257
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingItem;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getBackingType()Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public getCurrentVariablePrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 223
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getDuration()Lorg/threeten/bp/Duration;
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public getIntermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation

    .line 318
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getIntermissions()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItemDescription()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->itemDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getItemOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation

    .line 326
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->itemOptions:Ljava/util/List;

    return-object v0
.end method

.method public getItemType()Lcom/squareup/api/items/Item$Type;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->itemType:Lcom/squareup/api/items/Item$Type;

    return-object v0
.end method

.method public getModifierLists()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation

    .line 245
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->modifierLists:Ljava/util/SortedMap;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 277
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->note:Ljava/lang/String;

    return-object v0
.end method

.method public getOverridePrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->overridePrice:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getQuantity()Ljava/math/BigDecimal;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    return-object v0
.end method

.method public getQuantityPrecision()I
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getQuantityPrecision()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingItem;->getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedModifiers()Ljava/util/SortedMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation

    .line 249
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedModifiers:Ljava/util/SortedMap;

    return-object v0
.end method

.method public getSelectedVariation()Lcom/squareup/checkout/OrderVariation;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0}, Lcom/squareup/configure/item/WorkingItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 171
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->variations:Ljava/util/List;

    return-object v0
.end method

.method public isCompable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isConfigurationLockedFromScale()Z
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->quantityEntryType:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->READ_FROM_SCALE:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConfigurationLockedFromTicket()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isDeletable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isGiftCard()Z
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/squareup/configure/item/WorkingItem;->isGiftCard:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 294
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should be using ConfigureGiftCardState!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public isService()Z
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->itemType:Lcom/squareup/api/items/Item$Type;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isUncompable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isUnitPriced()Z
    .locals 1

    .line 167
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVoidable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public load(Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->cacheCurrentVariationAndVariablePrice()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 281
    invoke-super {p0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 288
    invoke-super {p0, p1}, Lcom/squareup/configure/item/ConfigureItemState;->onSave(Landroid/os/Bundle;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public orderDestination()Lcom/squareup/checkout/OrderDestination;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;Z)V
    .locals 1

    .line 273
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/configure/item/WorkingItem;->setDiscountApplied(Lcom/squareup/checkout/Discount;Z)V

    return-void
.end method

.method public setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/configure/item/WorkingItem;->setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public setDuration(Lorg/threeten/bp/Duration;)V
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/OrderVariation;->setDuration(Lorg/threeten/bp/Duration;)V

    return-void
.end method

.method public setIntermissions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)V"
        }
    .end annotation

    .line 322
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/OrderVariation;->setIntermissions(Ljava/util/List;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setOrderItemName(Ljava/lang/String;)Lcom/squareup/configure/item/WorkingItem;

    return-void
.end method

.method public setNote(Ljava/lang/String;)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->note:Ljava/lang/String;

    return-void
.end method

.method public setOverridePrice(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 235
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Override price can only be set for fixed-price variations."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->overridePrice:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public setQuantity(Ljava/math/BigDecimal;)V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->quantity:Ljava/math/BigDecimal;

    return-void
.end method

.method public setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V

    return-void
.end method

.method public setQuantityPrecisionOverride(I)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v0, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/OrderVariation;->setQuantityPrecisionOverride(I)V

    return-void
.end method

.method public setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1}, Lcom/squareup/configure/item/WorkingItem;->setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V

    return-void
.end method

.method public setSelectedVariation(Lcom/squareup/checkout/OrderVariation;)V
    .locals 2

    .line 198
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getPreviousVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureWorkingItemState;->getPreviousVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->clearQuantityUnitOverride()V

    :cond_0
    if-eqz p1, :cond_1

    .line 203
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->getQuantityUnit()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/OrderVariation;->setQuantityUnitOverride(Lcom/squareup/orders/model/Order$QuantityUnit;)V

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object p1, v0, Lcom/squareup/configure/item/WorkingItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    const/4 v1, 0x0

    .line 207
    iput-object v1, v0, Lcom/squareup/configure/item/WorkingItem;->overridePrice:Lcom/squareup/protos/common/Money;

    if-eqz p1, :cond_2

    .line 209
    invoke-virtual {p1}, Lcom/squareup/checkout/OrderVariation;->isFixedPriced()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 210
    :cond_2
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iput-object v1, p1, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    :cond_3
    return-void
.end method

.method public setTaxApplied(Lcom/squareup/checkout/Tax;Z)V
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureWorkingItemState;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/configure/item/WorkingItem;->setTaxApplied(Lcom/squareup/checkout/Tax;Z)V

    return-void
.end method

.method public uncompItem()V
    .locals 0

    return-void
.end method

.method public voidItem(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    return-void
.end method
