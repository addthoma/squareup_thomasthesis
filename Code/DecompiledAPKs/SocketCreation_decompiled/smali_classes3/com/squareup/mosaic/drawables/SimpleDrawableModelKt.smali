.class public final Lcom/squareup/mosaic/drawables/SimpleDrawableModelKt;
.super Ljava/lang/Object;
.source "SimpleDrawableModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "resource",
        "",
        "Lcom/squareup/mosaic/core/DrawableModelContext;",
        "drawableId",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final resource(Lcom/squareup/mosaic/core/DrawableModelContext;I)V
    .locals 1

    const-string v0, "$this$resource"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    new-instance v0, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/drawables/SimpleDrawableModel;-><init>(I)V

    check-cast v0, Lcom/squareup/mosaic/core/DrawableModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/DrawableModelContext;->add(Lcom/squareup/mosaic/core/DrawableModel;)V

    return-void
.end method
