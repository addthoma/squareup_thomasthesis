.class public final Lcom/squareup/mosaic/drawables/NotificationDrawableModel;
.super Lcom/squareup/mosaic/core/DrawableModel;
.source "NotificationDrawableModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0013\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B1\u0008\u0001\u0012\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J3\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00032\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0018\u0010\u001a\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0013\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001J\t\u0010#\u001a\u00020\u0007H\u00d6\u0001R\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\n\"\u0004\u0008\u000e\u0010\u000cR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\n\"\u0004\u0008\u0010\u0010\u000cR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/NotificationDrawableModel;",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "defStyleAttr",
        "",
        "defStyleRes",
        "baseDrawableId",
        "text",
        "",
        "(IIILjava/lang/String;)V",
        "getBaseDrawableId",
        "()I",
        "setBaseDrawableId",
        "(I)V",
        "getDefStyleAttr",
        "setDefStyleAttr",
        "getDefStyleRes",
        "setDefStyleRes",
        "getText",
        "()Ljava/lang/String;",
        "setText",
        "(Ljava/lang/String;)V",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "createDrawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private baseDrawableId:I

.field private defStyleAttr:I

.field private defStyleRes:I

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;-><init>(IIILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/mosaic/core/DrawableModel;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    iput p2, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    iput p3, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    iput-object p4, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(IIILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 28
    sget p2, Lcom/squareup/noho/R$style;->Widget_Noho_Notification:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 30
    check-cast p4, Ljava/lang/String;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;-><init>(IIILjava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/drawables/NotificationDrawableModel;IIILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/mosaic/drawables/NotificationDrawableModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->copy(IIILjava/lang/String;)Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(IIILjava/lang/String;)Lcom/squareup/mosaic/drawables/NotificationDrawableModel;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;-><init>(IIILjava/lang/String;)V

    return-object v0
.end method

.method public createDrawableRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/drawables/NotificationDrawableRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/DrawableRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    iget v1, p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    iget v1, p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    iget v1, p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBaseDrawableId()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    return v0
.end method

.method public final getDefStyleAttr()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    return v0
.end method

.method public final getDefStyleRes()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public final setBaseDrawableId(I)V
    .locals 0

    .line 29
    iput p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    return-void
.end method

.method public final setDefStyleAttr(I)V
    .locals 0

    .line 27
    iput p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    return-void
.end method

.method public final setDefStyleRes(I)V
    .locals 0

    .line 28
    iput p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotificationDrawableModel(defStyleAttr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleAttr:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", defStyleRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->defStyleRes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", baseDrawableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->baseDrawableId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
