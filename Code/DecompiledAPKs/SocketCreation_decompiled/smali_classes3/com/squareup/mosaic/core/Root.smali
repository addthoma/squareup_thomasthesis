.class public final Lcom/squareup/mosaic/core/Root;
.super Landroid/widget/FrameLayout;
.source "Root.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRoot.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Root.kt\ncom/squareup/mosaic/core/Root\n*L\n1#1,103:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0016\u001a\u00020\u000b2\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0018H\u0014J\u0016\u0010\u0019\u001a\u00020\u000b2\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\r0\u0018H\u0014J\u0018\u0010\u001a\u001a\u00020\u000b2\u000e\u0010\u000e\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000fH\u0002J%\u0010\u001b\u001a\u00020\u000b2\u001d\u0010\u001c\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000b0\u001e\u0012\u0004\u0012\u00020\u000b0\u001d\u00a2\u0006\u0002\u0008\u001fR\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R.\u0010\u000e\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u000f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0010\u0010\u0011\u001a\u0004\u0008\u0012\u0010\u0013\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/mosaic/core/Root;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "androidView",
        "Landroid/view/View;",
        "model",
        "Lcom/squareup/mosaic/core/UiModel;",
        "",
        "pendingState",
        "Landroid/os/Parcelable;",
        "viewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "viewRef$annotations",
        "()V",
        "getViewRef",
        "()Lcom/squareup/mosaic/core/ViewRef;",
        "setViewRef",
        "(Lcom/squareup/mosaic/core/ViewRef;)V",
        "dispatchRestoreInstanceState",
        "container",
        "Landroid/util/SparseArray;",
        "dispatchSaveInstanceState",
        "restorePendingState",
        "update",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "Lkotlin/ExtensionFunctionType;",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private androidView:Landroid/view/View;

.field private model:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private pendingState:Landroid/os/Parcelable;

.field private viewRef:Lcom/squareup/mosaic/core/ViewRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 20
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/mosaic/core/Root;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final restorePendingState(Lcom/squareup/mosaic/core/ViewRef;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;)V"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/mosaic/core/Root;->pendingState:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/squareup/mosaic/core/ViewRef;->restoreInstanceState(Landroid/os/Parcelable;)V

    :cond_0
    const/4 p1, 0x0

    .line 100
    check-cast p1, Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/squareup/mosaic/core/Root;->pendingState:Landroid/os/Parcelable;

    return-void
.end method

.method public static synthetic viewRef$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/Root;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/Root;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/Parcelable;

    iput-object p1, p0, Lcom/squareup/mosaic/core/Root;->pendingState:Landroid/os/Parcelable;

    .line 95
    iget-object p1, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz p1, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/mosaic/core/Root;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/core/Root;->restorePendingState(Lcom/squareup/mosaic/core/ViewRef;)V

    :cond_1
    return-void

    .line 93
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Root object needs an id."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/Root;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 89
    iget-object v0, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/mosaic/core/ViewRef;->saveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mosaic/core/Root;->getId()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    return-void

    .line 88
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Root object needs an id."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final getViewRef()Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public final setViewRef(Lcom/squareup/mosaic/core/ViewRef;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;)V"
        }
    .end annotation

    .line 26
    iput-object p1, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    return-void
.end method

.method public final update(Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/mosaic/core/OneUiModelContext;

    sget-object v1, Lcom/squareup/mosaic/core/Root$update$newModel$1;->INSTANCE:Lcom/squareup/mosaic/core/Root$update$newModel$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {v0, v1}, Lcom/squareup/mosaic/core/OneUiModelContext;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 60
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/squareup/mosaic/core/OneUiModelContext;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 66
    iget-object v0, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0, p1}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 70
    invoke-static {v0, p1}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    goto :goto_0

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/Root;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/squareup/mosaic/core/Root;->androidView:Landroid/view/View;

    if-eqz v1, :cond_1

    move-object v2, p0

    check-cast v2, Lcom/squareup/mosaic/core/Root;

    invoke-virtual {v2, v1}, Lcom/squareup/mosaic/core/Root;->removeView(Landroid/view/View;)V

    .line 75
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/mosaic/core/Root;->androidView:Landroid/view/View;

    .line 76
    iget-object v1, p0, Lcom/squareup/mosaic/core/Root;->androidView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/squareup/mosaic/core/Root;->addView(Landroid/view/View;)V

    .line 77
    invoke-direct {p0, v0}, Lcom/squareup/mosaic/core/Root;->restorePendingState(Lcom/squareup/mosaic/core/ViewRef;)V

    .line 73
    iput-object v0, p0, Lcom/squareup/mosaic/core/Root;->viewRef:Lcom/squareup/mosaic/core/ViewRef;

    .line 80
    :goto_0
    iput-object p1, p0, Lcom/squareup/mosaic/core/Root;->model:Lcom/squareup/mosaic/core/UiModel;

    return-void

    .line 58
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Expected block to provide exactly one model, but none was provided."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
