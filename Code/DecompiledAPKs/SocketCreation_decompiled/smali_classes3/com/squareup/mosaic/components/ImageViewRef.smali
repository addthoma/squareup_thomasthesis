.class public final Lcom/squareup/mosaic/components/ImageViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "ImageViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/ImageUiModel<",
        "*>;",
        "Landroid/widget/ImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nImageViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ImageViewRef.kt\ncom/squareup/mosaic/components/ImageViewRef\n+ 2 DrawableRef.kt\ncom/squareup/mosaic/core/DrawableRefKt\n*L\n1#1,36:1\n45#2,8:37\n*E\n*S KotlinDebug\n*F\n+ 1 ImageViewRef.kt\ncom/squareup/mosaic/components/ImageViewRef\n*L\n28#1,8:37\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\n\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016R\u0018\u0010\u0007\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/ImageViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/ImageUiModel;",
        "Landroid/widget/ImageView;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "drawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private drawableRef:Lcom/squareup/mosaic/core/DrawableRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 14
    check-cast p2, Lcom/squareup/mosaic/components/ImageUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ImageViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/ImageUiModel;)Landroid/widget/ImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/ImageUiModel;)Landroid/widget/ImageView;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/ImageUiModel<",
            "*>;)",
            "Landroid/widget/ImageView;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    new-instance p2, Landroid/widget/ImageView;

    invoke-direct {p2, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/ImageUiModel;Lcom/squareup/mosaic/components/ImageUiModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/ImageUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/ImageUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 28
    iget-object p1, p0, Lcom/squareup/mosaic/components/ImageViewRef;->drawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ImageViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "androidView.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/ImageUiModel;->getDrawable()Lcom/squareup/mosaic/core/DrawableModel;

    move-result-object p2

    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/core/DrawableRef;->canAccept(Lcom/squareup/mosaic/core/DrawableModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-static {p1, p2}, Lcom/squareup/mosaic/core/DrawableRefKt;->castAndBind(Lcom/squareup/mosaic/core/DrawableRef;Lcom/squareup/mosaic/core/DrawableModel;)V

    goto :goto_0

    .line 42
    :cond_0
    invoke-static {p2, v0}, Lcom/squareup/mosaic/core/DrawableModelKt;->toDrawableRef(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;

    move-result-object p2

    if-eqz p1, :cond_1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/ImageViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object p1, p2

    .line 44
    :goto_0
    iput-object p1, p0, Lcom/squareup/mosaic/components/ImageViewRef;->drawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/mosaic/components/ImageUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/ImageUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ImageViewRef;->doBind(Lcom/squareup/mosaic/components/ImageUiModel;Lcom/squareup/mosaic/components/ImageUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/mosaic/components/ImageUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/ImageUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/ImageViewRef;->doBind(Lcom/squareup/mosaic/components/ImageUiModel;Lcom/squareup/mosaic/components/ImageUiModel;)V

    return-void
.end method
