.class public final Lcom/squareup/mosaic/components/DividerEdge$Companion;
.super Ljava/lang/Object;
.source "VerticalDividersStackUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/components/DividerEdge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006R\u0016\u0010\u0008\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\t\u0010\u0006R\u0016\u0010\n\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u000b\u0010\u0006R\u0016\u0010\u000c\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\r\u0010\u0006R\u0016\u0010\u000e\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u000f\u0010\u0006R\u0016\u0010\u0010\u001a\u00020\u0004\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0011\u0010\u0006\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/DividerEdge$Companion;",
        "",
        "()V",
        "ALL",
        "Lcom/squareup/mosaic/components/DividerEdge;",
        "getALL",
        "()I",
        "I",
        "BOTTOM",
        "getBOTTOM",
        "LEFT",
        "getLEFT",
        "NONE",
        "getNONE",
        "RIGHT",
        "getRIGHT",
        "TOP",
        "getTOP",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/mosaic/components/DividerEdge$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getALL()I
    .locals 1

    .line 83
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getALL$cp()I

    move-result v0

    return v0
.end method

.method public final getBOTTOM()I
    .locals 1

    .line 82
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getBOTTOM$cp()I

    move-result v0

    return v0
.end method

.method public final getLEFT()I
    .locals 1

    .line 79
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getLEFT$cp()I

    move-result v0

    return v0
.end method

.method public final getNONE()I
    .locals 1

    .line 78
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getNONE$cp()I

    move-result v0

    return v0
.end method

.method public final getRIGHT()I
    .locals 1

    .line 81
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getRIGHT$cp()I

    move-result v0

    return v0
.end method

.method public final getTOP()I
    .locals 1

    .line 80
    invoke-static {}, Lcom/squareup/mosaic/components/DividerEdge;->access$getTOP$cp()I

    move-result v0

    return v0
.end method
