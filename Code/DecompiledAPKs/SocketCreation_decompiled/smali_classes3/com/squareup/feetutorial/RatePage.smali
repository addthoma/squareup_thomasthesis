.class Lcom/squareup/feetutorial/RatePage;
.super Ljava/lang/Object;
.source "RatePage.java"


# static fields
.field private static final CROP:Z = true

.field private static final DONT_CROP:Z = false

.field private static final PAGE_BOTTOM:Z = true

.field private static final PAGE_CENTER:Z


# instance fields
.field final category:Lcom/squareup/feetutorial/RateCategory;

.field final cropImageInLandscape:Z

.field final heading:I

.field final imageGravityBottom:Z

.field final imageResId:I

.field final message:I

.field final rate:Lcom/squareup/server/account/protos/ProcessingFee;

.field final subtitle:I


# direct methods
.method private constructor <init>(Lcom/squareup/feetutorial/RateCategory;IZZLcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/feetutorial/RatePage;->category:Lcom/squareup/feetutorial/RateCategory;

    .line 27
    iput p2, p0, Lcom/squareup/feetutorial/RatePage;->imageResId:I

    .line 28
    iput-boolean p3, p0, Lcom/squareup/feetutorial/RatePage;->imageGravityBottom:Z

    .line 29
    iput-boolean p4, p0, Lcom/squareup/feetutorial/RatePage;->cropImageInLandscape:Z

    .line 30
    iget p1, p5, Lcom/squareup/feetutorial/RateText;->subtitle:I

    iput p1, p0, Lcom/squareup/feetutorial/RatePage;->subtitle:I

    .line 31
    iget p1, p5, Lcom/squareup/feetutorial/RateText;->heading:I

    iput p1, p0, Lcom/squareup/feetutorial/RatePage;->heading:I

    .line 32
    iget p1, p5, Lcom/squareup/feetutorial/RateText;->message:I

    iput p1, p0, Lcom/squareup/feetutorial/RatePage;->message:I

    .line 33
    iput-object p6, p0, Lcom/squareup/feetutorial/RatePage;->rate:Lcom/squareup/server/account/protos/ProcessingFee;

    return-void
.end method

.method static pageBottom(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;
    .locals 8

    .line 45
    new-instance v7, Lcom/squareup/feetutorial/RatePage;

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, v7

    move-object v1, p0

    move v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/feetutorial/RatePage;-><init>(Lcom/squareup/feetutorial/RateCategory;IZZLcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)V

    return-object v7
.end method

.method static pageCenter(Lcom/squareup/feetutorial/RateCategory;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;
    .locals 8

    .line 39
    new-instance v7, Lcom/squareup/feetutorial/RatePage;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, v7

    move-object v1, p0

    move v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/feetutorial/RatePage;-><init>(Lcom/squareup/feetutorial/RateCategory;IZZLcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)V

    return-object v7
.end method

.method static pageMaybeBottomCropOrCenter(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/util/Device;ILcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/feetutorial/RatePage;
    .locals 10

    .line 54
    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    .line 55
    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v6, 0x0

    goto :goto_0

    :cond_0
    const/4 v6, 0x1

    :goto_0
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v7, 0x1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    .line 60
    :goto_1
    new-instance p1, Lcom/squareup/feetutorial/RatePage;

    move-object v3, p1

    move-object v4, p0

    move v5, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v3 .. v9}, Lcom/squareup/feetutorial/RatePage;-><init>(Lcom/squareup/feetutorial/RateCategory;IZZLcom/squareup/feetutorial/RateText;Lcom/squareup/server/account/protos/ProcessingFee;)V

    return-object p1
.end method
