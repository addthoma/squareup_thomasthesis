.class public final Lcom/squareup/feetutorial/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feetutorial/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final rate_tour_cnp_heading:I = 0x7f12153b

.field public static final rate_tour_cnp_heading_credit:I = 0x7f12153c

.field public static final rate_tour_cnp_heading_type:I = 0x7f12153d

.field public static final rate_tour_cnp_message:I = 0x7f12153e

.field public static final rate_tour_cnp_message_higher:I = 0x7f12153f

.field public static final rate_tour_cnp_message_rate:I = 0x7f121540

.field public static final rate_tour_cnp_subtitle:I = 0x7f121541

.field public static final rate_tour_cnp_subtitle_credit:I = 0x7f121542

.field public static final rate_tour_cnp_subtitle_type:I = 0x7f121543

.field public static final rate_tour_cp_heading:I = 0x7f121544

.field public static final rate_tour_cp_heading_credit:I = 0x7f121545

.field public static final rate_tour_cp_message:I = 0x7f121546

.field public static final rate_tour_cp_subtitle:I = 0x7f121547

.field public static final rate_tour_cp_subtitle_credit:I = 0x7f121548

.field public static final rate_tour_dip_jcb_heading:I = 0x7f121549

.field public static final rate_tour_dip_jcb_message:I = 0x7f12154a

.field public static final rate_tour_dipswipe_heading:I = 0x7f12154b

.field public static final rate_tour_dipswipe_message:I = 0x7f12154c

.field public static final rate_tour_dipswipe_message_brands:I = 0x7f12154d

.field public static final rate_tour_dipswipe_subtitle:I = 0x7f12154e

.field public static final rate_tour_diptap_heading:I = 0x7f12154f

.field public static final rate_tour_diptap_heading_credit:I = 0x7f121550

.field public static final rate_tour_diptap_heading_insert:I = 0x7f121551

.field public static final rate_tour_diptap_message:I = 0x7f121552

.field public static final rate_tour_diptap_message_all:I = 0x7f121553

.field public static final rate_tour_diptap_message_credit:I = 0x7f121554

.field public static final rate_tour_diptap_subtitle:I = 0x7f121555

.field public static final rate_tour_diptap_subtitle_credit:I = 0x7f121556

.field public static final rate_tour_diptap_subtitle_insert:I = 0x7f121557

.field public static final rate_tour_help_button_description:I = 0x7f121558

.field public static final rate_tour_help_url:I = 0x7f121559

.field public static final rate_tour_interac_heading:I = 0x7f12155a

.field public static final rate_tour_interac_heading_with_limit:I = 0x7f12155b

.field public static final rate_tour_interac_message:I = 0x7f12155c

.field public static final rate_tour_interac_message_with_limit:I = 0x7f12155d

.field public static final rate_tour_interac_subtitle:I = 0x7f12155e

.field public static final rate_tour_interac_subtitle_with_limit:I = 0x7f12155f

.field public static final rate_tour_tapdip_heading:I = 0x7f121560

.field public static final rate_tour_tapdip_message:I = 0x7f121561

.field public static final rate_tour_tapdip_subtitle:I = 0x7f121562


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
