.class public Lcom/squareup/feetutorial/RateText;
.super Ljava/lang/Object;
.source "RateText.java"


# instance fields
.field final heading:I

.field final message:I

.field final subtitle:I


# direct methods
.method private constructor <init>(III)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/squareup/feetutorial/RateText;->subtitle:I

    .line 17
    iput p2, p0, Lcom/squareup/feetutorial/RateText;->heading:I

    .line 18
    iput p3, p0, Lcom/squareup/feetutorial/RateText;->message:I

    return-void
.end method

.method private static cnpTextFor(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;
    .locals 3

    .line 95
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p0

    .line 98
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->getCpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object v1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/settings/server/PaymentSettings;->getCnpFee()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p0

    .line 100
    iget-object p0, p0, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    iget-object v1, v1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le p0, v1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    .line 101
    sget p0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_message_higher:I

    goto :goto_1

    :cond_1
    sget p0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_message:I

    .line 104
    :goto_1
    sget-object v1, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    .line 115
    new-instance v0, Lcom/squareup/feetutorial/RateText;

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_subtitle:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_heading:I

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object v0

    .line 112
    :cond_2
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_subtitle:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_heading:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_message_rate:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 109
    :cond_3
    new-instance v0, Lcom/squareup/feetutorial/RateText;

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_subtitle_type:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_heading_type:I

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object v0

    .line 106
    :cond_4
    new-instance v0, Lcom/squareup/feetutorial/RateText;

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_subtitle_credit:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cnp_heading_credit:I

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object v0
.end method

.method private static dipTextFor(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/feetutorial/RateText;
    .locals 3

    .line 64
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    .line 78
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_subtitle:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_heading:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_message:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 75
    :cond_0
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_subtitle:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_heading:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_message_brands:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 72
    :cond_1
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_subtitle_insert:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_heading_insert:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_message_all:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 69
    :cond_2
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_subtitle_credit:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_heading_credit:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_diptap_message_credit:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 66
    :cond_3
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_subtitle:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_heading:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_message:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0
.end method

.method private static swipeTextFor(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/feetutorial/RateText;
    .locals 3

    .line 84
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    .line 89
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_subtitle:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_heading:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_message:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 86
    :cond_0
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_subtitle_credit:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_heading_credit:I

    sget v2, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_cp_message:I

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0
.end method

.method private static tapTextFor(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;
    .locals 2

    .line 43
    sget-object v0, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getTutorialSettings()Lcom/squareup/settings/server/TutorialSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/TutorialSettings;->getFeeTutorialInfo()Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;

    move-result-object p0

    iget-object p0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Fees;->tutorial_show_interac_limit:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 50
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget p1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_subtitle_with_limit:I

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_heading_with_limit:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_message_with_limit:I

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 55
    :cond_0
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget p1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_subtitle:I

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_heading:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_interac_message:I

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 59
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected currency: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 45
    :cond_2
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget p1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_tapdip_subtitle:I

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_tapdip_heading:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_tapdip_message:I

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0
.end method

.method public static textFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;
    .locals 3

    .line 22
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCurrency()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 23
    sget-object v1, Lcom/squareup/feetutorial/RateText$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 38
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected category: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 36
    :pswitch_0
    invoke-static {v0}, Lcom/squareup/feetutorial/RateText;->swipeTextFor(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    return-object p0

    .line 34
    :pswitch_1
    invoke-static {p1}, Lcom/squareup/feetutorial/RateText;->cnpTextFor(Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    return-object p0

    .line 32
    :pswitch_2
    invoke-static {v0, p1}, Lcom/squareup/feetutorial/RateText;->tapTextFor(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    return-object p0

    .line 28
    :pswitch_3
    new-instance p0, Lcom/squareup/feetutorial/RateText;

    sget p1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dipswipe_subtitle:I

    sget v0, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dip_jcb_heading:I

    sget v1, Lcom/squareup/feetutorial/impl/R$string;->rate_tour_dip_jcb_message:I

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/feetutorial/RateText;-><init>(III)V

    return-object p0

    .line 26
    :pswitch_4
    invoke-static {v0}, Lcom/squareup/feetutorial/RateText;->dipTextFor(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/feetutorial/RateText;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
