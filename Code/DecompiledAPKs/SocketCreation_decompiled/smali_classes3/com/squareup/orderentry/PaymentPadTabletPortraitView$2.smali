.class Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PaymentPadTabletPortraitView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->animateToEditMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 81
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->access$002(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 82
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 83
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->endAnimation()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 75
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->editFrame:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 77
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    iget-object p1, p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->startAnimation()V

    return-void
.end method
