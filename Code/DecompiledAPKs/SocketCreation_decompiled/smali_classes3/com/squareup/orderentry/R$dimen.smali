.class public final Lcom/squareup/orderentry/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final cart_footer_banner_margin:I = 0x7f0700b4

.field public static final cart_line_height:I = 0x7f0700bc

.field public static final cart_swipe_controller_delete_button_padding:I = 0x7f0700be

.field public static final cash_drawer_modal_content_size:I = 0x7f0700c1

.field public static final cash_drawer_modal_gap:I = 0x7f0700c2

.field public static final cash_drawer_modal_padding:I = 0x7f0700c3

.field public static final cash_drawer_modal_title_size:I = 0x7f0700c4

.field public static final cash_drawer_modal_width:I = 0x7f0700c5

.field public static final favorite_tooltip_bottom_arrow:I = 0x7f070165

.field public static final home_view_sales_frame_shadow_width:I = 0x7f07019b

.field public static final library_create_new_item_dialog_button_margin:I = 0x7f0701e7

.field public static final library_create_new_item_dialog_padding:I = 0x7f0701e8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
