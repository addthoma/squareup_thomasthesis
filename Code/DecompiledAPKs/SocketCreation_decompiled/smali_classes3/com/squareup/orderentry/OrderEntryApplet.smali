.class public final Lcom/squareup/orderentry/OrderEntryApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "OrderEntryApplet.kt"

# interfaces
.implements Lcom/squareup/ui/main/Home;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryApplet$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0001\u0018B\u0015\u0008\u0007\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00102\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0008H\u0016J\u0010\u0010\u0015\u001a\u00020\u00082\u0006\u0010\u0016\u001a\u00020\u0017H\u0016R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/orderentry/OrderEntryApplet;",
        "Lcom/squareup/applet/HistoryFactoryApplet;",
        "Lcom/squareup/ui/main/Home;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Ldagger/Lazy;)V",
        "analyticsName",
        "",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "appletId",
        "",
        "getAppletId",
        "()Ljava/lang/Integer;",
        "getHomeScreens",
        "",
        "Lcom/squareup/container/ContainerTreeKey;",
        "currentHistory",
        "Lflow/History;",
        "getIntentScreenExtra",
        "getText",
        "resources",
        "Landroid/content/res/Resources;",
        "Companion",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/orderentry/OrderEntryApplet$Companion;

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "REGISTER"


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final appletId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/orderentry/OrderEntryApplet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/OrderEntryApplet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryApplet;->Companion:Lcom/squareup/orderentry/OrderEntryApplet$Companion;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    const-string p1, "checkout"

    .line 24
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryApplet;->analyticsName:Ljava/lang/String;

    .line 26
    sget p1, Lcom/squareup/orderentry/R$id;->applets_drawer_order_entry_applet:I

    iput p1, p0, Lcom/squareup/orderentry/OrderEntryApplet;->appletId:I

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryApplet;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public getAppletId()Ljava/lang/Integer;
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/orderentry/OrderEntryApplet;->appletId:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getHomeScreens(Lflow/History;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/History;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation

    .line 33
    sget-object p1, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "REGISTER"

    return-object v0
.end method

.method public getText(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget v0, Lcom/squareup/registerlib/R$string;->titlecase_register:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(com.\u2026tring.titlecase_register)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
