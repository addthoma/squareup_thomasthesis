.class public Lcom/squareup/orderentry/ClockSkew;
.super Ljava/lang/Object;
.source "ClockSkew.java"


# static fields
.field private static final FALLBACK_TIME:J = 0x159590ad800L

.field private static final MAX_CLOCK_SKEW_MILLIS:J = 0x36ee80L

.field private static clockIsSkewed:Z

.field public static previousServerTime:J


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final isOffline:Z

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/orderentry/ClockSkew;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 35
    iput-object p2, p0, Lcom/squareup/orderentry/ClockSkew;->clock:Lcom/squareup/util/Clock;

    .line 36
    invoke-interface {p3}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/orderentry/ClockSkew;->isOffline:Z

    .line 37
    iput-object p4, p0, Lcom/squareup/orderentry/ClockSkew;->flow:Lflow/Flow;

    .line 38
    iput-object p5, p0, Lcom/squareup/orderentry/ClockSkew;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public clockIsSkewed()Z
    .locals 12

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkew;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CLOCK_SKEW_LOCKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkew;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkew;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getServerTimeMillis()Ljava/lang/Long;

    move-result-object v0

    .line 71
    iget-boolean v4, p0, Lcom/squareup/orderentry/ClockSkew;->isOffline:Z

    const-wide v5, 0x159590ad800L

    const/4 v7, 0x1

    if-nez v4, :cond_7

    if-nez v0, :cond_1

    goto :goto_0

    .line 81
    :cond_1
    sget-wide v8, Lcom/squareup/orderentry/ClockSkew;->previousServerTime:J

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-nez v4, :cond_3

    .line 82
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sput-wide v8, Lcom/squareup/orderentry/ClockSkew;->previousServerTime:J

    cmp-long v0, v2, v5

    if-gez v0, :cond_2

    const/4 v1, 0x1

    .line 83
    :cond_2
    sput-boolean v1, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed:Z

    .line 84
    sget-boolean v0, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed:Z

    return v0

    .line 92
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v8, v4

    if-nez v6, :cond_4

    .line 93
    sget-boolean v0, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed:Z

    return v0

    .line 96
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sput-wide v4, Lcom/squareup/orderentry/ClockSkew;->previousServerTime:J

    .line 102
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v8, 0x36ee80

    sub-long/2addr v4, v8

    cmp-long v6, v2, v4

    if-lez v6, :cond_5

    .line 103
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v4, v8

    cmp-long v0, v2, v4

    if-ltz v0, :cond_6

    :cond_5
    const/4 v1, 0x1

    :cond_6
    sput-boolean v1, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed:Z

    .line 105
    sget-boolean v0, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed:Z

    return v0

    :cond_7
    :goto_0
    cmp-long v0, v2, v5

    if-gez v0, :cond_8

    const/4 v1, 0x1

    :cond_8
    return v1
.end method

.method public showClockSkewLockoutIfNeeded()V
    .locals 2

    .line 42
    invoke-virtual {p0}, Lcom/squareup/orderentry/ClockSkew;->clockIsSkewed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkew;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/orderentry/ClockSkewScreen;->INSTANCE:Lcom/squareup/orderentry/ClockSkewScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
