.class public final Lcom/squareup/orderentry/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final EmployeeLockButton:[I

.field public static final EmployeeLockButton_android_textColor:I

.field public static final OrderEntryDrawerButton:[I

.field public static final OrderEntryDrawerButton_customLayout:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    const v3, 0x1010098

    aput v3, v1, v2

    .line 562
    sput-object v1, Lcom/squareup/orderentry/R$styleable;->EmployeeLockButton:[I

    new-array v0, v0, [I

    const v1, 0x7f040112

    aput v1, v0, v2

    .line 564
    sput-object v0, Lcom/squareup/orderentry/R$styleable;->OrderEntryDrawerButton:[I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
