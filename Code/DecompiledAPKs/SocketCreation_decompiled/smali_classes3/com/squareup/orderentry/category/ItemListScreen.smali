.class public final Lcom/squareup/orderentry/category/ItemListScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "ItemListScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/orderentry/category/ItemListScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/category/ItemListScreen$Component;,
        Lcom/squareup/orderentry/category/ItemListScreen$Presenter;
    }
.end annotation


# static fields
.field private static final ALL_DISCOUNTS_ID:Ljava/lang/String; = "all-discounts"

.field private static final ALL_ITEMS_ID:Ljava/lang/String; = "all-items"

.field private static final CATEGORY_ID_PREFIX:Ljava/lang/String; = "category-"

.field private static final CREATE_TILE_PREFIX:Ljava/lang/String; = "create-tile"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/category/ItemListScreen;",
            ">;"
        }
    .end annotation
.end field

.field private static final TILE_ID_SEPARATOR:C = '/'

.field private static final TILE_POSITION_SEPARATOR:C = '_'


# instance fields
.field final categoryName:Ljava/lang/String;

.field final id:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 416
    sget-object v0, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$DpB2323NcnzS1u0A7mO7HhEMGAY;->INSTANCE:Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$DpB2323NcnzS1u0A7mO7HhEMGAY;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/category/ItemListScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen;->categoryName:Ljava/lang/String;

    return-void
.end method

.method public static allDiscounts()Lcom/squareup/orderentry/category/ItemListScreen;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/orderentry/category/ItemListScreen;

    const-string v1, "all-discounts"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/orderentry/category/ItemListScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static allItems()Lcom/squareup/orderentry/category/ItemListScreen;
    .locals 3

    .line 85
    new-instance v0, Lcom/squareup/orderentry/category/ItemListScreen;

    const-string v1, "all-items"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/orderentry/category/ItemListScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static category(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/orderentry/category/ItemListScreen;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/orderentry/category/ItemListScreen;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "category-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/category/ItemListScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createTile(Ljava/lang/String;II)Lcom/squareup/orderentry/category/ItemListScreen;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/orderentry/category/ItemListScreen;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "create-tile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x2f

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p0, 0x5f

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/category/ItemListScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/orderentry/category/ItemListScreen;
    .locals 2

    .line 417
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 419
    new-instance v1, Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/orderentry/category/ItemListScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 412
    iget-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 413
    iget-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen;->categoryName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_ITEM_LIST:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method getCategoryId()Ljava/lang/String;
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCreateTilePageId()Ljava/lang/String;
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    .line 143
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCreateTilePosition()Landroid/graphics/Point;
    .locals 4

    .line 134
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    .line 136
    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 137
    new-instance v2, Landroid/graphics/Point;

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v1, v1, 0x1

    .line 138
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method isAllDiscounts()Z
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const-string v1, "all-discounts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method isAllItems()Z
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const-string v1, "all-items"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method isCategory()Z
    .locals 2

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const-string v1, "category-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method isCreateTile()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    const-string v1, "create-tile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 423
    sget v0, Lcom/squareup/orderentry/R$layout;->favorite_item_list_view:I

    return v0
.end method
