.class public Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;
.super Landroidx/cursoradapter/widget/CursorAdapter;
.source "ItemListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/category/ItemListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemAdapter"
.end annotation


# instance fields
.field extraRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

.field final synthetic this$0:Lcom/squareup/orderentry/category/ItemListView;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/category/ItemListView;)V
    .locals 2

    .line 233
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 234
    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListView;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroidx/cursoradapter/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 225
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    return-void
.end method

.method private maybeGetPlaceholderCount(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/PlaceholderCounts;->getItemCountString(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    .line 345
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "We overrode getView()"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public changeCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 408
    new-instance v0, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    invoke-super {p0, v0}, Landroidx/cursoradapter/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public getCount()I
    .locals 2

    .line 238
    invoke-super {p0}, Landroidx/cursoradapter/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 362
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItemType(I)I

    move-result v0

    if-eqz v0, :cond_4

    const/4 p1, 0x1

    if-eq v0, p1, :cond_3

    const/4 p1, 0x2

    if-eq v0, p1, :cond_2

    const/4 p1, 0x3

    if-eq v0, p1, :cond_1

    const/4 p1, 0x4

    if-eq v0, p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 378
    :cond_0
    sget-object p1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p1

    .line 381
    :cond_1
    sget-object p1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p1

    .line 375
    :cond_2
    sget-object p1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p1

    .line 372
    :cond_3
    sget-object p1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p1

    .line 365
    :cond_4
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    .line 366
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-super {p0, p1}, Landroidx/cursoradapter/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;

    .line 367
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/android/AndroidLibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p1

    return-object p1
.end method

.method public getItemType(I)I
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->extraRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 349
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItemType(I)I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15

    move-object v0, p0

    .line 242
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItemType(I)I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x5

    if-ne v1, v4, :cond_1

    if-nez p2, :cond_0

    .line 245
    new-instance v1, Lcom/squareup/ui/ReorientingLinearLayout;

    iget-object v4, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 246
    invoke-virtual {v4}, Lcom/squareup/orderentry/category/ItemListView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/squareup/ui/ReorientingLinearLayout;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    move-object/from16 v1, p2

    check-cast v1, Lcom/squareup/ui/ReorientingLinearLayout;

    .line 248
    :goto_0
    iget-object v4, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 249
    invoke-virtual {v4}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/marin/R$dimen;->marin_gap_large:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 250
    iget-object v5, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 251
    invoke-virtual {v5}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/squareup/marin/R$dimen;->marin_gap_medium:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 252
    invoke-virtual {v1, v4, v5, v4, v4}, Lcom/squareup/ui/ReorientingLinearLayout;->setPadding(IIII)V

    .line 253
    invoke-virtual {v1, v2}, Lcom/squareup/ui/ReorientingLinearLayout;->setShowDividers(I)V

    .line 254
    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 255
    invoke-virtual {v2}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/squareup/marin/R$drawable;->marin_divider_square_clear_medium:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 254
    invoke-virtual {v1, v2}, Lcom/squareup/ui/ReorientingLinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 258
    invoke-virtual {v1}, Lcom/squareup/ui/ReorientingLinearLayout;->removeAllViews()V

    .line 260
    new-instance v2, Lcom/squareup/marketfont/MarketButton;

    iget-object v4, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v4}, Lcom/squareup/orderentry/category/ItemListView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    const/16 v4, 0x11

    .line 261
    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 262
    sget v5, Lcom/squareup/registerlib/R$string;->create_item:I

    invoke-virtual {v2, v5}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 263
    sget v5, Lcom/squareup/orderentry/R$id;->item_list_create_item:I

    invoke-virtual {v2, v5}, Lcom/squareup/marketfont/MarketButton;->setId(I)V

    .line 264
    new-instance v5, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter$1;

    invoke-direct {v5, p0}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter$1;-><init>(Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;)V

    invoke-virtual {v2, v5}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, -0x2

    invoke-direct {v5, v3, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2, v5}, Lcom/squareup/ui/ReorientingLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    new-instance v2, Lcom/squareup/marketfont/MarketButton;

    iget-object v5, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v5}, Lcom/squareup/orderentry/category/ItemListView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/squareup/marketfont/MarketButton;-><init>(Landroid/content/Context;)V

    .line 273
    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketButton;->setGravity(I)V

    .line 274
    sget v4, Lcom/squareup/registerlib/R$string;->create_discount:I

    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 275
    new-instance v4, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter$2;

    invoke-direct {v4, p0}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter$2;-><init>(Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;)V

    invoke-virtual {v2, v4}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v3, v7, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2, v4}, Lcom/squareup/ui/ReorientingLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1

    :cond_1
    if-nez p2, :cond_2

    .line 285
    sget v4, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    move-object/from16 v5, p3

    .line 286
    invoke-static {v4, v5}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    goto :goto_1

    :cond_2
    move-object/from16 v4, p2

    check-cast v4, Lcom/squareup/librarylist/LibraryItemListNohoRow;

    :goto_1
    const/4 v5, 0x1

    if-nez p1, :cond_3

    const/4 v6, 0x1

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    .line 288
    :goto_2
    invoke-virtual {v4, v6, v5}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setHorizontalBorders(ZZ)V

    if-eqz v1, :cond_8

    if-eq v1, v5, :cond_7

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_4

    goto/16 :goto_4

    .line 320
    :cond_4
    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forReward(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 321
    sget v7, Lcom/squareup/librarylist/R$string;->item_library_redeem_rewards:I

    const/4 v8, 0x0

    sget-object v9, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    .line 323
    invoke-static {v1}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v10

    move-object v5, v4

    .line 321
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    goto/16 :goto_4

    :cond_5
    const/4 v1, 0x0

    .line 328
    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v2}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forGiftCard(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 329
    sget v7, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 331
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->maybeGetPlaceholderCount(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {v1}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v10

    move-object v5, v4

    .line 329
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    goto/16 :goto_4

    .line 312
    :cond_6
    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 313
    sget v7, Lcom/squareup/librarylist/R$string;->item_library_all_discounts:I

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 315
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->maybeGetPlaceholderCount(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {v1}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v10

    move-object v5, v4

    .line 313
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    goto/16 :goto_4

    .line 304
    :cond_7
    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forAllItems(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 305
    sget v7, Lcom/squareup/librarylist/R$string;->item_library_all_items:I

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 307
    invoke-direct {p0, v1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->maybeGetPlaceholderCount(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-object v1, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {v1}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v10

    move-object v5, v4

    .line 305
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindPlaceholderItem(Landroid/graphics/drawable/Drawable;ILjava/lang/String;Lcom/squareup/marin/widgets/ChevronVisibility;Z)V

    goto :goto_4

    .line 291
    :cond_8
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 292
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v2

    sget-object v5, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v2, v5, :cond_9

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    if-eqz v2, :cond_9

    .line 293
    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v7, v2, Lcom/squareup/orderentry/category/ItemListView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    const/4 v8, 0x0

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {v2}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v9

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->placeholderCounts:Lcom/squareup/orderentry/PlaceholderCounts;

    .line 294
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/orderentry/PlaceholderCounts;->categoryCount(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object v5, v4

    move-object v6, v1

    .line 293
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCategoryItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;ZZLjava/lang/String;)V

    goto :goto_3

    .line 296
    :cond_9
    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v7, v2, Lcom/squareup/orderentry/category/ItemListView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v8, v2, Lcom/squareup/orderentry/category/ItemListView;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v9, v2, Lcom/squareup/orderentry/category/ItemListView;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v10, v2, Lcom/squareup/orderentry/category/ItemListView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v11, v2, Lcom/squareup/orderentry/category/ItemListView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 297
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    invoke-static {v2}, Lcom/squareup/orderentry/category/ItemListView;->access$100(Lcom/squareup/orderentry/category/ItemListView;)Z

    move-result v13

    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v14, v2, Lcom/squareup/orderentry/category/ItemListView;->res:Lcom/squareup/util/Res;

    move-object v5, v4

    move-object v6, v1

    .line 296
    invoke-virtual/range {v5 .. v14}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;)V

    .line 299
    :goto_3
    iget-object v2, v0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v2, v2, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {v2, v1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->isEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result v1

    invoke-virtual {v4, v1}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->setEnabled(Z)V

    :goto_4
    return-object v4
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 397
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 400
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItemType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->this$0:Lcom/squareup/orderentry/category/ItemListView;

    iget-object v0, v0, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListView$ItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->isEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 341
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "We overrode getView()"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
