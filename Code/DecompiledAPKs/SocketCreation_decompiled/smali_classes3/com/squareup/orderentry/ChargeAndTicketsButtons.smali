.class public Lcom/squareup/orderentry/ChargeAndTicketsButtons;
.super Landroid/widget/FrameLayout;
.source "ChargeAndTicketsButtons.java"


# static fields
.field private static final ALERT_PAUSE_MS:I = 0x1f4

.field private static final ALERT_PHRASE_DURATION_MS:I = 0x15e

.field private static final HALF_TRANSITION_DURATION:J = 0x4bL

.field private static final TRANSITION_DURATION:J = 0x96L


# instance fields
.field private final buttonsContainer:Landroid/view/ViewGroup;

.field private final chargeContainer:Landroid/view/ViewGroup;

.field private final chargeHeight:I

.field private final chargeOverlayView:Landroid/widget/ImageView;

.field private final chargeSubtitleView:Landroid/widget/TextView;

.field private final chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

.field private final confirmOverlay:Landroid/widget/ImageView;

.field private currentChargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

.field private currentTicketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

.field presenter:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final ticketSavedAlert:Landroid/widget/TextView;

.field private final ticketsContainer:Landroid/view/ViewGroup;

.field private final ticketsOverlayView:Landroid/widget/ImageView;

.field private final ticketsSubtitleView:Landroid/widget/TextView;

.field private final ticketsTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p2, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    .line 74
    sget p2, Lcom/squareup/orderentry/R$layout;->charge_and_tickets_button_content:I

    invoke-static {p1, p2, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 76
    sget p2, Lcom/squareup/marin/R$dimen;->marin_charge_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeHeight:I

    .line 78
    sget p2, Lcom/squareup/orderentry/R$id;->charge_and_tickets_buttons_container:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buttonsContainer:Landroid/view/ViewGroup;

    .line 79
    sget p2, Lcom/squareup/orderentry/R$id;->charge_and_tickets_buttons_confirm_overlay:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    .line 80
    sget p2, Lcom/squareup/orderentry/R$id;->tickets_button_confirm_overlay:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    .line 81
    sget p2, Lcom/squareup/orderentry/R$id;->charge_button_confirm_overlay:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeOverlayView:Landroid/widget/ImageView;

    .line 83
    sget p2, Lcom/squareup/orderentry/R$id;->tickets_button_container:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    .line 84
    sget p2, Lcom/squareup/orderentry/R$id;->tickets_button_title:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsTitleView:Landroid/widget/TextView;

    .line 85
    sget p2, Lcom/squareup/orderentry/R$id;->tickets_button_subtitle:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsSubtitleView:Landroid/widget/TextView;

    .line 86
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$1;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$1;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    sget p2, Lcom/squareup/orderentry/R$id;->charge_button_container:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    .line 93
    sget p2, Lcom/squareup/orderentry/R$id;->charge_button_title:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

    .line 94
    sget p2, Lcom/squareup/orderentry/R$id;->charge_button_subtitle:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeSubtitleView:Landroid/widget/TextView;

    .line 95
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$2;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$2;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    sget p2, Lcom/squareup/orderentry/R$id;->charge_and_ticket_saved_alert:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketSavedAlert:Landroid/widget/TextView;

    .line 103
    sget p2, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    if-nez p1, :cond_0

    .line 104
    new-instance p1, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketsButtons$hnbjYD_Biu5pw95ieiYVIY0xFGc;

    invoke-direct {p1, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketsButtons$hnbjYD_Biu5pw95ieiYVIY0xFGc;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    .line 109
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/view/ViewGroup;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/ImageView;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/ImageView;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/TextView;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketSavedAlert:Landroid/widget/TextView;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/view/ViewGroup;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buttonsContainer:Landroid/view/ViewGroup;

    return-object p0
.end method

.method private static animateButtonOverlay(Landroid/view/ViewGroup;Landroid/widget/ImageView;)V
    .locals 2

    .line 318
    invoke-static {p0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 321
    invoke-virtual {p1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 322
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 p0, 0x0

    .line 323
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 325
    new-instance p0, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 326
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 327
    new-instance v0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$6;

    invoke-direct {v0, p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$6;-><init>(Landroid/widget/ImageView;)V

    invoke-virtual {p0, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const-wide/16 v0, 0x96

    .line 332
    invoke-virtual {p0, v0, v1}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 333
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private buildTicketSavedButtonsInAnimator()Landroid/animation/AnimatorSet;
    .locals 10

    .line 277
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 278
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketSavedAlert:Landroid/widget/TextView;

    sget-object v2, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v4, v3, [F

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v5, v4, v6

    iget v7, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeHeight:I

    neg-int v7, v7

    int-to-float v7, v7

    const/4 v8, 0x1

    aput v7, v4, v8

    invoke-static {v1, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 279
    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buttonsContainer:Landroid/view/ViewGroup;

    sget-object v4, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->TRANSLATION_Y:Landroid/util/Property;

    new-array v7, v3, [F

    iget v9, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeHeight:I

    int-to-float v9, v9

    aput v9, v7, v6

    aput v5, v7, v8

    invoke-static {v2, v4, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    .line 280
    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    const-wide/16 v4, 0x15e

    .line 281
    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 282
    new-instance v4, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;

    invoke-direct {v4, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 294
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v6

    aput-object v2, v3, v8

    .line 295
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    return-object v0
.end method

.method private buildTicketSavedButtonsOutAnimator()Landroid/animation/AnimatorSet;
    .locals 9

    .line 253
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 254
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketSavedAlert:Landroid/widget/TextView;

    sget-object v2, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v4, v3, [F

    iget v5, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeHeight:I

    neg-int v5, v5

    int-to-float v5, v5

    const/4 v6, 0x0

    aput v5, v4, v6

    const/4 v5, 0x0

    const/4 v7, 0x1

    aput v5, v4, v7

    invoke-static {v1, v2, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 255
    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    sget-object v4, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->TRANSLATION_Y:Landroid/util/Property;

    new-array v8, v3, [F

    aput v5, v8, v6

    iget v5, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeHeight:I

    int-to-float v5, v5

    aput v5, v8, v7

    invoke-static {v2, v4, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v4, 0x15e

    .line 256
    invoke-virtual {v0, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 257
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 258
    new-instance v4, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;

    invoke-direct {v4, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$4;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    .line 272
    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    return-object v0
.end method

.method private static buildTransition()Landroid/animation/LayoutTransition;
    .locals 7

    .line 338
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 v1, 0x3

    const-wide/16 v2, 0x4b

    .line 339
    invoke-virtual {v0, v1, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v4, 0x2

    .line 340
    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v5, 0x0

    .line 341
    invoke-virtual {v0, v5, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v6, 0x1

    .line 342
    invoke-virtual {v0, v6, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 344
    invoke-virtual {v0, v1, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 345
    invoke-virtual {v0, v4, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    const-wide/16 v1, 0x96

    .line 346
    invoke-virtual {v0, v5, v1, v2}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 347
    invoke-virtual {v0, v6, v1, v2}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    .line 349
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5, v1}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 350
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v6, v1}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    return-object v0
.end method

.method private setButtonAppearance(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;)V
    .locals 3

    .line 301
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 302
    sget v1, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 303
    invoke-interface {p4, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;->getTextColorId(Z)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 304
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 305
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 306
    invoke-interface {p4, v1}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;->getBackgroundId(Z)I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method private static setButtonText(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .line 311
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p3, p0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 313
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method animateConfirmChargeBeforeSettingText()V
    .locals 3

    .line 195
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeOverlayView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buttonsContainer:Landroid/view/ViewGroup;

    .line 199
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    const/16 v2, 0x8

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 205
    :cond_0
    invoke-static {v0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 208
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 209
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 211
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 212
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 213
    new-instance v1, Lcom/squareup/orderentry/ChargeAndTicketsButtons$3;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons$3;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const-wide/16 v1, 0x96

    .line 225
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 226
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    .line 201
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method animateToTicketSavedAndBack()V
    .locals 3

    .line 240
    invoke-static {p0}, Lcom/squareup/util/Views;->tryCopyToBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 241
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 242
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->confirmOverlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 244
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTicketSavedButtonsOutAnimator()Landroid/animation/AnimatorSet;

    move-result-object v0

    .line 245
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTicketSavedButtonsInAnimator()Landroid/animation/AnimatorSet;

    move-result-object v1

    .line 247
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 248
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 249
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method hideTicketsButton()V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method initializeToTicketSavedAnimation()V
    .locals 1

    .line 236
    invoke-direct {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTicketSavedButtonsInAnimator()Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method installGlassCancel(Lcom/squareup/widgets/glass/GlassConfirmController;)V
    .locals 3

    .line 189
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buttonsContainer:Landroid/view/ViewGroup;

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketsButtons$XOi9YNTYE_WYSu0n7-miKY8FF1g;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/-$$Lambda$ChargeAndTicketsButtons$XOi9YNTYE_WYSu0n7-miKY8FF1g;-><init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/widgets/glass/GlassConfirmController;->installGlass(Landroid/view/View;Landroid/view/View;Lcom/squareup/widgets/glass/GlassConfirmView$OnCancelListener;)V

    return-void
.end method

.method public synthetic lambda$installGlassCancel$1$ChargeAndTicketsButtons()V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->presenter:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->onCancelConfirmClicked()V

    return-void
.end method

.method public synthetic lambda$new$0$ChargeAndTicketsButtons()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTransition()Landroid/animation/LayoutTransition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 114
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 115
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->presenter:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->presenter:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter;->dropView(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method showTicketsButton()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method updateChargeButton(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "charge button title"

    .line 150
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 156
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->currentChargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    if-eq p1, v0, :cond_2

    if-eqz p2, :cond_1

    .line 159
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeOverlayView:Landroid/widget/ImageView;

    invoke-static {p2, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->animateButtonOverlay(Landroid/view/ViewGroup;Landroid/widget/ImageView;)V

    .line 161
    :cond_1
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeSubtitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, p2, v0, v1, p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setButtonAppearance(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;)V

    .line 164
    :cond_2
    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->currentChargeState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ChargeState;

    .line 166
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeSubtitleView:Landroid/widget/TextView;

    invoke-static {p3, p4, p1, p2}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setButtonText(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-void
.end method

.method updateTicketsButton(Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    sget-object v1, Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;->DISABLED:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    if-eq p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->currentTicketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    if-eq p1, v0, :cond_2

    if-eqz p2, :cond_1

    .line 178
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsOverlayView:Landroid/widget/ImageView;

    invoke-static {p2, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->animateButtonOverlay(Landroid/view/ViewGroup;Landroid/widget/ImageView;)V

    .line 180
    :cond_1
    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsTitleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsSubtitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, p2, v0, v1, p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setButtonAppearance(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/view/View;Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$ButtonState;)V

    .line 183
    :cond_2
    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->currentTicketState:Lcom/squareup/orderentry/ChargeAndTicketButtonsPresenter$TicketState;

    .line 185
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsTitleView:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->ticketsSubtitleView:Landroid/widget/TextView;

    invoke-static {p3, p4, p1, p2}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setButtonText(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-void
.end method

.method useLargerChargeTextAppearance()V
    .locals 4

    .line 128
    invoke-virtual {p0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 129
    sget v1, Lcom/squareup/marin/R$dimen;->marin_charge_no_tickets_phone_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 131
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_charge_below:I

    .line 132
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 133
    iget-object v2, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 134
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeTitleView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 135
    iget-object v1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->chargeSubtitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v0, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method
