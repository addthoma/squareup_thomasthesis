.class public Lcom/squareup/orderentry/KeypadPanelPresenter;
.super Lmortar/ViewPresenter;
.source "KeypadPanelPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/KeypadPanel;",
        ">;"
    }
.end annotation


# instance fields
.field private final addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final clearCardPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/flowlegacy/YesNo;",
            ">;"
        }
    .end annotation
.end field

.field private final clearSaleOrCardPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
            ">;"
        }
    .end annotation
.end field

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private flyBySize:I

.field private final flyBySource:Lcom/squareup/orderentry/FlyBySource;

.field private final isTablet:Z

.field private keypadListener:Lcom/squareup/padlock/MoneyKeypadListener;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

.field private final runner:Lcom/squareup/ui/seller/SellerScopeRunner;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final vibrator:Landroid/os/Vibrator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/seller/SellerScopeRunner;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/orderentry/FlyBySource;Landroid/os/Vibrator;Lcom/squareup/util/Device;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/seller/SellerScopeRunner;",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/orderentry/FlyBySource;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 59
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->buildClearCardPopupPresenter()Lcom/squareup/mortar/PopupPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->buildClearCardOrSalePopupPresenter()Lcom/squareup/mortar/PopupPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearSaleOrCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 82
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->runner:Lcom/squareup/ui/seller/SellerScopeRunner;

    .line 83
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

    .line 84
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->bus:Lcom/squareup/badbus/BadBus;

    .line 85
    iput-object p4, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 86
    iput-object p5, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 87
    iput-object p6, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 88
    iput-object p7, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySource:Lcom/squareup/orderentry/FlyBySource;

    .line 89
    iput-object p8, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->vibrator:Landroid/os/Vibrator;

    .line 90
    invoke-interface {p9}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->isTablet:Z

    .line 91
    iput-object p10, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 92
    iput-object p11, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 93
    iput-object p12, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    .line 94
    iput-object p13, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 95
    iput-object p14, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/KeypadPanelPresenter;)Lcom/squareup/log/cart/TransactionInteractionsLogger;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/KeypadPanelPresenter;)Ljava/lang/Object;
    .locals 0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/KeypadPanelPresenter;Landroid/view/View;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->commitKeypadItem(Landroid/view/View;)V

    return-void
.end method

.method private commitKeypadItem(Landroid/view/View;)V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->commitKeypadItem()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->keypadListener:Lcom/squareup/padlock/MoneyKeypadListener;

    if-eqz v0, :cond_1

    .line 192
    invoke-virtual {v0}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    .line 194
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->showFlyAnimation(Landroid/view/View;)V

    return-void
.end method

.method private onPaymentChanged()V
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 125
    :cond_0
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateAmount()V

    .line 126
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateNote()V

    return-void
.end method

.method private updateAmount()V
    .locals 7

    .line 130
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/KeypadPanel;

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/squareup/orderentry/KeypadPanel;->setPriceText(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method private updateNote()V
    .locals 3

    .line 135
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadNote()Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 138
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/orderentry/KeypadPanel;->setNoteText(Ljava/lang/CharSequence;Z)V

    return-void
.end method


# virtual methods
.method buildClearCardOrSalePopupPresenter()Lcom/squareup/mortar/PopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
            ">;"
        }
    .end annotation

    .line 274
    new-instance v0, Lcom/squareup/orderentry/KeypadPanelPresenter$3;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/KeypadPanelPresenter$3;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    return-object v0
.end method

.method buildClearCardPopupPresenter()Lcom/squareup/mortar/PopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/flowlegacy/YesNo;",
            ">;"
        }
    .end annotation

    .line 257
    new-instance v0, Lcom/squareup/orderentry/KeypadPanelPresenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/KeypadPanelPresenter$2;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    return-object v0
.end method

.method public dropView(Lcom/squareup/orderentry/KeypadPanel;)V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/KeypadPanel;->getClearCardPopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearSaleOrCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/KeypadPanel;->getClearCardOrSalePopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 118
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->dropView(Lcom/squareup/orderentry/KeypadPanel;)V

    return-void
.end method

.method initializeKeypad(Lcom/squareup/padlock/Padlock;)V
    .locals 7

    .line 198
    invoke-virtual {p1}, Lcom/squareup/padlock/Padlock;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, v2}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 199
    new-instance v0, Lcom/squareup/orderentry/KeypadPanelPresenter$1;

    iget-object v4, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->vibrator:Landroid/os/Vibrator;

    sget-wide v5, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/orderentry/KeypadPanelPresenter$1;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;J)V

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->keypadListener:Lcom/squareup/padlock/MoneyKeypadListener;

    .line 228
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->keypadListener:Lcom/squareup/padlock/MoneyKeypadListener;

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    return-void
.end method

.method public synthetic lambda$null$2$KeypadPanelPresenter(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/quickamounts/ui/UiQuickAmount;
    .locals 3

    .line 235
    new-instance v0, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/quickamounts/ui/UiQuickAmount;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$KeypadPanelPresenter(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 99
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->onPaymentChanged()V

    return-void
.end method

.method public synthetic lambda$quickAmounts$3$KeypadPanelPresenter(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 234
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$xPa-z8D3c5NJvM7I5aiAaOID2kY;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$xPa-z8D3c5NJvM7I5aiAaOID2kY;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->map(Ljava/lang/Iterable;Lkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object p1

    .line 236
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    invoke-direct {v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public synthetic lambda$showFlyAnimation$1$KeypadPanelPresenter()V
    .locals 4

    .line 159
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;II)V

    return-void
.end method

.method noteClicked()V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/main/AddNoteScreenRunner;->goToNoteScreen()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->bus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$inpcrq1IKkwWsr9f011PeRUjGUI;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$inpcrq1IKkwWsr9f011PeRUjGUI;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 104
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {p1}, Lcom/squareup/orderentry/KeypadPanel;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$dimen;->marin_library_thumbnail_size:I

    .line 105
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySize:I

    .line 107
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanel;->getClearCardPopup()Lcom/squareup/mortar/Popup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearSaleOrCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanel;->getClearCardOrSalePopup()Lcom/squareup/mortar/Popup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 110
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateAmount()V

    .line 111
    invoke-direct {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateNote()V

    .line 112
    iget-boolean p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->isTablet:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadPanel;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/KeypadPanel;->updateBackspaceEnabled(Z)V

    :cond_1
    return-void
.end method

.method onQuickAmountsVisible(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 147
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Quick Amounts Displayed"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Quick Amounts Hidden"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method quickAmountClicked(Lcom/squareup/quickamounts/ui/ClickEvent;)V
    .locals 5

    .line 164
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 167
    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/ClickEvent;->getAmount()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateKeypadAmount(J)V

    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Quick Amounts Itemization Client Token: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->peekItemClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 169
    iget-object v3, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v4, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v4, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 170
    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/ClickEvent;->getView()Landroid/view/View;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->commitKeypadItem(Landroid/view/View;)V

    .line 173
    invoke-virtual {p0, v0, v1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->updateKeypadAmount(J)V

    return-void
.end method

.method quickAmounts()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
            ">;"
        }
    .end annotation

    .line 232
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {v0}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$U0jQSIe83izdbVc82iRgDx4LA0U;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$U0jQSIe83izdbVc82iRgDx4LA0U;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    .line 233
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method showAppropriateClearDialog()V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasNonLockedItems()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 243
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->runner:Lcom/squareup/ui/seller/SellerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeRemoveNonLockedItems()V

    goto :goto_0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearSaleOrCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/ui/Showing;

    invoke-direct {v1}, Lcom/squareup/ui/Showing;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->clearCardPresenter:Lcom/squareup/mortar/PopupPresenter;

    new-instance v1, Lcom/squareup/ui/Showing;

    invoke-direct {v1}, Lcom/squareup/ui/Showing;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 251
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 252
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->runner:Lcom/squareup/ui/seller/SellerScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/seller/SellerScopeRunner;->maybeClearCart()V

    :cond_3
    :goto_0
    return-void
.end method

.method protected showFlyAnimation(Landroid/view/View;)V
    .locals 9

    .line 154
    new-instance v4, Lcom/squareup/ui/main/CurrencyTileDrawable;

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/KeypadPanel;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanel;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 155
    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySize:I

    invoke-direct {v4, v0, v1, v2}, Lcom/squareup/ui/main/CurrencyTileDrawable;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySource:Lcom/squareup/orderentry/FlyBySource;

    invoke-interface {v0}, Lcom/squareup/orderentry/FlyBySource;->getCoordinator()Lcom/squareup/orderentry/FlyByCoordinator;

    move-result-object v0

    iget v1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySize:I

    iget-object v2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->flyBySource:Lcom/squareup/orderentry/FlyBySource;

    .line 157
    invoke-interface {v2}, Lcom/squareup/orderentry/FlyBySource;->getDestination()Landroid/widget/TextView;

    move-result-object v3

    new-instance v8, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$pH9DpsHFiS7ZVtE1T3OyZmNmiXU;

    invoke-direct {v8, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanelPresenter$pH9DpsHFiS7ZVtE1T3OyZmNmiXU;-><init>(Lcom/squareup/orderentry/KeypadPanelPresenter;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v8}, Lcom/squareup/orderentry/FlyByCoordinator;->addFlyByView(ILandroid/view/View;Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;ZZILcom/squareup/orderentry/FlyByListener;)V

    return-void
.end method

.method protected updateKeypadAmount(J)V
    .locals 5

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getKeypadPrice()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Transaction;->setKeypadPrice(J)V

    .line 182
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanelPresenter;->keypadListener:Lcom/squareup/padlock/MoneyKeypadListener;

    if-eqz p1, :cond_1

    .line 183
    invoke-virtual {p1}, Lcom/squareup/padlock/MoneyKeypadListener;->updateKeyStates()V

    :cond_1
    return-void
.end method
