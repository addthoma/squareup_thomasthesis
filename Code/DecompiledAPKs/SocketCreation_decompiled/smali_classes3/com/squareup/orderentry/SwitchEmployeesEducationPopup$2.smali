.class Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;
.super Ljava/lang/Object;
.source "SwitchEmployeesEducationPopup.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;->createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

.field final synthetic val$glyph:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;Landroid/view/View;Lcom/squareup/glyph/SquareGlyphView;)V
    .locals 0

    .line 60
    iput-object p1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->this$0:Lcom/squareup/orderentry/SwitchEmployeesEducationPopup;

    iput-object p2, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->val$view:Landroid/view/View;

    iput-object p3, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->val$glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->val$view:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/common/R$id;->message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 67
    iget-object v1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPopup$2;->val$glyph:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1}, Lcom/squareup/glyph/SquareGlyphView;->getWidth()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const/4 v0, 0x1

    return v0
.end method
