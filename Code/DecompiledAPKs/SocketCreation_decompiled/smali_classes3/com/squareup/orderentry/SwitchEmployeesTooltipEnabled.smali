.class public final enum Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;
.super Ljava/lang/Enum;
.source "SwitchEmployeesTooltipEnabled.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

.field public static final enum DISABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

.field public static final enum ENABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 5
    new-instance v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    const/4 v1, 0x0

    const-string v2, "ENABLED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->ENABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    new-instance v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    const/4 v2, 0x1

    const-string v3, "DISABLED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->DISABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    .line 4
    sget-object v3, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->ENABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->DISABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->$VALUES:[Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;
    .locals 1

    .line 4
    const-class v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;
    .locals 1

    .line 4
    sget-object v0, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->$VALUES:[Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    return-object v0
.end method
