.class Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;
.super Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;
.source "PaymentPadTabletPortraitPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/DropDownContainer$DropDownListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropDownHidden(Landroid/view/View;)V
    .locals 0

    .line 67
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->access$000(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->access$100(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->showBottomSaleBorder()V

    :cond_0
    return-void
.end method

.method public onDropDownOpening(Landroid/view/View;)V
    .locals 0

    .line 73
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->access$200(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 74
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter$1;->this$0:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->access$300(Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->removeBottomSaleBorder()V

    :cond_0
    return-void
.end method
