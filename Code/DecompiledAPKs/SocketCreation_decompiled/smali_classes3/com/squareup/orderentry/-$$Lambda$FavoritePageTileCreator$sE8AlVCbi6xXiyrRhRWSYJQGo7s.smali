.class public final synthetic Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/orderentry/FavoritePageTileCreator;

.field private final synthetic f$1:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Ljava/lang/String;

.field private final synthetic f$4:Landroid/graphics/Point;

.field private final synthetic f$5:I


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/orderentry/FavoritePageTileCreator;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$0:Lcom/squareup/orderentry/FavoritePageTileCreator;

    iput-object p2, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$1:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iput-object p3, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$3:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$4:Landroid/graphics/Point;

    iput p6, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$5:I

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$0:Lcom/squareup/orderentry/FavoritePageTileCreator;

    iget-object v1, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$1:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    iget-object v2, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$3:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$4:Landroid/graphics/Point;

    iget v5, p0, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;->f$5:I

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/squareup/orderentry/FavoritePageTileCreator;->lambda$createTile$1$FavoritePageTileCreator(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method
