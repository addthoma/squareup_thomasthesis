.class abstract Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
.super Lmortar/Presenter;
.source "NavigationBarAbstractPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/orderentry/NavigationBarView;",
        ">;"
    }
.end annotation


# instance fields
.field private analytics:Lcom/squareup/analytics/Analytics;

.field private currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private final device:Lcom/squareup/util/Device;

.field private latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

.field private final libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/analytics/Analytics;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->device:Lcom/squareup/util/Device;

    .line 43
    iput-object p2, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 44
    iput-object p3, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 45
    iput-object p4, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    .line 46
    iput-object p5, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 47
    iput-object p6, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private updateSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 2

    .line 105
    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 106
    iget-object p1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    if-eqz p1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/NavigationBarView;

    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->tabForPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/librarylist/LibraryListState$Filter;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/NavigationBarView;->updateSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    :cond_0
    return-void
.end method

.method private updateTabs(Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 3

    .line 92
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/NavigationBarView;

    .line 93
    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/NavigationBarView;->updateTabs(Lcom/squareup/orderentry/pages/OrderEntryPageList;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->tabForPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/librarylist/LibraryListState$Filter;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/NavigationBarView;->updateSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    return-void
.end method

.method private updateTabsList(Lcom/squareup/orderentry/pages/OrderEntryPageList;Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->latestPages:Lcom/squareup/orderentry/pages/OrderEntryPageList;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 100
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->updateTabs(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/orderentry/NavigationBarView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/orderentry/NavigationBarView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/orderentry/NavigationBarView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->extractBundleService(Lcom/squareup/orderentry/NavigationBarView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected getCurrentPage()Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->currentPage:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object v0
.end method

.method isThreeColumn()Z
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$null$0$NavigationBarAbstractPresenter(Lkotlin/Pair;)V
    .locals 1

    .line 62
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->updateSelectedTab(Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/librarylist/LibraryListState$Filter;)V

    return-void
.end method

.method public synthetic lambda$null$2$NavigationBarAbstractPresenter(Lkotlin/Pair;)V
    .locals 1

    .line 66
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/pages/OrderEntryPageList;

    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-direct {p0, v0, p1}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->updateTabsList(Lcom/squareup/orderentry/pages/OrderEntryPageList;Lcom/squareup/librarylist/LibraryListState$Filter;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$NavigationBarAbstractPresenter(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->observeCurrentPage()Lrx/Observable;

    move-result-object v0

    .line 60
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    .line 61
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$Pj8EiAe-o3u4xDgbDfuuBQ9ULpQ;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$Pj8EiAe-o3u4xDgbDfuuBQ9ULpQ;-><init>(Lcom/squareup/orderentry/NavigationBarAbstractPresenter;)V

    .line 62
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$NavigationBarAbstractPresenter(Lrx/Observable;)Lrx/Subscription;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->observe()Lrx/Observable;

    move-result-object v0

    .line 65
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$oAGs4erNxK_8UbtlahkSuXkqbJA;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$oAGs4erNxK_8UbtlahkSuXkqbJA;-><init>(Lcom/squareup/orderentry/NavigationBarAbstractPresenter;)V

    .line 66
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 55
    iget-object p1, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->libraryListStateManager:Lcom/squareup/librarylist/LibraryListStateManager;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListStateManager;->holder()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/-$$Lambda$n7zB753C3oQPlNRAli3Q5wz6hWY;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$n7zB753C3oQPlNRAli3Q5wz6hWY;

    .line 56
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$o362w10IaqSLjbb8UdyF7BGt0Ug;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$o362w10IaqSLjbb8UdyF7BGt0Ug;-><init>(Lcom/squareup/orderentry/NavigationBarAbstractPresenter;Lrx/Observable;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$miKa7Rfro2aoY74R792W89XNWw4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$NavigationBarAbstractPresenter$miKa7Rfro2aoY74R792W89XNWw4;-><init>(Lcom/squareup/orderentry/NavigationBarAbstractPresenter;Lrx/Observable;)V

    invoke-static {v0, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method abstract tabLongClicked(Lcom/squareup/orderentry/pages/OrderEntryPage;)V
.end method

.method tabSelected(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 2

    .line 80
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_KEYPAD_TAB:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    .line 82
    :cond_0
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->LIBRARY:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne p1, v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_LIBRARY_TAB:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 85
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 86
    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq p1, v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarAbstractPresenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Favorites Tab Selected"

    invoke-interface {v0, v1, p1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    return-void
.end method
