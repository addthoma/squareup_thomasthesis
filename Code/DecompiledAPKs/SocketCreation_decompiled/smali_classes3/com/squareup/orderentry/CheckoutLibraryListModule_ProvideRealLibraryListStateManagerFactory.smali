.class public final Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;
.super Ljava/lang/Object;
.source "CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/RealLibraryListStateManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutLibraryListConfigurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardActivationFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final stateSaverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->giftCardActivationFlowProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->checkoutLibraryListConfigurationProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->stateSaverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;",
            ">;)",
            "Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;"
        }
    .end annotation

    .line 70
    new-instance v9, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static provideRealLibraryListStateManager(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;)Lcom/squareup/librarylist/RealLibraryListStateManager;
    .locals 0

    .line 78
    invoke-static/range {p0 .. p7}, Lcom/squareup/orderentry/CheckoutLibraryListModule;->provideRealLibraryListStateManager(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;)Lcom/squareup/librarylist/RealLibraryListStateManager;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/librarylist/RealLibraryListStateManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/RealLibraryListStateManager;
    .locals 9

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->redeemRewardsFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->giftCardActivationFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->checkoutLibraryListConfigurationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->stateSaverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;

    invoke-static/range {v1 .. v8}, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->provideRealLibraryListStateManager(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;)Lcom/squareup/librarylist/RealLibraryListStateManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListModule_ProvideRealLibraryListStateManagerFactory;->get()Lcom/squareup/librarylist/RealLibraryListStateManager;

    move-result-object v0

    return-object v0
.end method
