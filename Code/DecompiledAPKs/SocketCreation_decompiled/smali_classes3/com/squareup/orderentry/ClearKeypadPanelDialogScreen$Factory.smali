.class public Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ClearKeypadPanelDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;Lcom/squareup/ui/seller/SellerScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 47
    sget-object p2, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$1;->$SwitchMap$com$squareup$ui$seller$SellerScopeRunner$ClearOptions:[I

    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->ordinal()I

    move-result p0

    aget p0, p2, p0

    const/4 p2, 0x1

    if-eq p0, p2, :cond_1

    const/4 p2, 0x2

    if-eq p0, p2, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->confirmRemoveNonLockedItems()V

    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/seller/SellerScopeRunner;->confirmClearCart()V

    :goto_0
    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/DialogInterface;I)V
    .locals 0

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 35
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;

    .line 37
    const-class v1, Lcom/squareup/ui/seller/SellerScope$BaseComponent;

    .line 38
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/seller/SellerScope$BaseComponent;

    .line 39
    invoke-interface {v1}, Lcom/squareup/ui/seller/SellerScope$BaseComponent;->scopeRunner()Lcom/squareup/ui/seller/SellerScopeRunner;

    move-result-object v1

    .line 41
    invoke-static {v0}, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->access$000(Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;)Lcom/squareup/register/widgets/Confirmation;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/register/widgets/Confirmation;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;

    move-result-object v2

    .line 42
    invoke-static {v0}, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->access$100(Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;)Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    move-result-object v0

    .line 44
    new-instance v3, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, v2, Lcom/squareup/register/widgets/Confirmation$Strings;->title:Ljava/lang/String;

    invoke-virtual {v3, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v3, v2, Lcom/squareup/register/widgets/Confirmation$Strings;->body:Ljava/lang/String;

    .line 45
    invoke-virtual {p1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v3, v2, Lcom/squareup/register/widgets/Confirmation$Strings;->confirm:Ljava/lang/String;

    new-instance v4, Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$Factory$YubvBWX1ypqi4M2wSUE35nNC1uE;

    invoke-direct {v4, v0, v1}, Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$Factory$YubvBWX1ypqi4M2wSUE35nNC1uE;-><init>(Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;Lcom/squareup/ui/seller/SellerScopeRunner;)V

    .line 46
    invoke-virtual {p1, v3, v4}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v0, v2, Lcom/squareup/register/widgets/Confirmation$Strings;->cancel:Ljava/lang/String;

    sget-object v1, Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$Factory$IzuPSUjocmZu88ktojsYA_B23_s;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$Factory$IzuPSUjocmZu88ktojsYA_B23_s;

    .line 56
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 44
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
