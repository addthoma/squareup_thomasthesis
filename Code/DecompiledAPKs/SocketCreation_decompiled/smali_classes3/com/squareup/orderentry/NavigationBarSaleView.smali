.class public Lcom/squareup/orderentry/NavigationBarSaleView;
.super Lcom/squareup/orderentry/NavigationBarView;
.source "NavigationBarSaleView.java"


# instance fields
.field presenter:Lcom/squareup/orderentry/NavigationBarSalePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/NavigationBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/NavigationBarSaleView;)V

    return-void
.end method


# virtual methods
.method protected getNarrowTabGlyphLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_sale_tab_glyph:I

    return v0
.end method

.method protected getNarrowTabTextLayout()I
    .locals 1

    .line 35
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_sale_tab_text:I

    return v0
.end method

.method protected bridge synthetic getPresenter()Lcom/squareup/orderentry/NavigationBarAbstractPresenter;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/orderentry/NavigationBarSaleView;->getPresenter()Lcom/squareup/orderentry/NavigationBarSalePresenter;

    move-result-object v0

    return-object v0
.end method

.method protected getPresenter()Lcom/squareup/orderentry/NavigationBarSalePresenter;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSaleView;->presenter:Lcom/squareup/orderentry/NavigationBarSalePresenter;

    return-object v0
.end method

.method protected getWideTabLayout()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_actionbar_tab_text_wide:I

    return v0
.end method

.method protected isEditor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
