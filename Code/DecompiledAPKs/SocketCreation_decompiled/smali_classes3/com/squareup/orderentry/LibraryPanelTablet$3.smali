.class Lcom/squareup/orderentry/LibraryPanelTablet$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "LibraryPanelTablet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/LibraryPanelTablet;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/LibraryPanelTablet;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/LibraryPanelTablet;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet$3;->this$0:Lcom/squareup/orderentry/LibraryPanelTablet;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 92
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet$3;->this$0:Lcom/squareup/orderentry/LibraryPanelTablet;

    invoke-static {p1}, Lcom/squareup/orderentry/LibraryPanelTablet;->access$000(Lcom/squareup/orderentry/LibraryPanelTablet;)Ljava/lang/Runnable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/orderentry/LibraryPanelTablet;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 94
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet$3;->this$0:Lcom/squareup/orderentry/LibraryPanelTablet;

    iget-object p1, p1, Lcom/squareup/orderentry/LibraryPanelTablet;->presenter:Lcom/squareup/orderentry/LibraryPanelTabletPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/LibraryPanelTabletPresenter;->isSearching()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 95
    iget-object p1, p0, Lcom/squareup/orderentry/LibraryPanelTablet$3;->this$0:Lcom/squareup/orderentry/LibraryPanelTablet;

    invoke-static {p1}, Lcom/squareup/orderentry/LibraryPanelTablet;->access$000(Lcom/squareup/orderentry/LibraryPanelTablet;)Ljava/lang/Runnable;

    move-result-object p2

    const-wide/16 p3, 0x64

    invoke-virtual {p1, p2, p3, p4}, Lcom/squareup/orderentry/LibraryPanelTablet;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
