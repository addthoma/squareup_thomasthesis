.class Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "OrderEntryViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OrderEntryViewPagerAdapter"
.end annotation


# static fields
.field private static final FAVORITE_PAGE_ID_PREFIX:Ljava/lang/String; = "favorite-"

.field private static final KEYPAD_PAGE_ID:Ljava/lang/String; = "keypad"

.field private static final LIBRARY_PAGE_ID:Ljava/lang/String; = "library"

.field private static final PAGING_KEY:Ljava/lang/String; = "paging"


# instance fields
.field private final pathContextFactory:Lflow/path/PathContextFactory;

.field private state:Landroid/os/Bundle;

.field final synthetic this$0:Lcom/squareup/orderentry/OrderEntryViewPager;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryViewPager;)V
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    .line 128
    invoke-static {}, Lcom/squareup/container/ContainerTreeKeyContextFactory;->forLegacyFlows()Lcom/squareup/container/ContainerTreeKeyContextFactory;

    move-result-object p1

    invoke-static {p1}, Lflow/path/Path;->contextFactory(Lflow/path/PathContextFactory;)Lflow/path/PathContextFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->pathContextFactory:Lflow/path/PathContextFactory;

    .line 132
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    return-void
.end method

.method private extractFavoritePageId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x9

    .line 231
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private isFavoritePage(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "favorite-"

    .line 227
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private isKeypad(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "keypad"

    .line 223
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private isLibrary(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "library"

    .line 219
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private restorePageState(Landroid/view/View;)V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->getPageId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p1, v0}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method private savePageState(Landroid/view/View;)V
    .locals 2

    .line 258
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 259
    invoke-virtual {p1, v0}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 261
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->getPageId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .line 239
    invoke-virtual {p0, p3}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->getPageId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 240
    check-cast p3, Landroid/view/View;

    .line 241
    invoke-direct {p0, p3}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->savePageState(Landroid/view/View;)V

    .line 242
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 244
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isKeypad(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 245
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$102(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;

    goto :goto_0

    .line 246
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isLibrary(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 247
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$202(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;

    goto :goto_0

    .line 248
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isFavoritePage(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 251
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->pathContextFactory:Lflow/path/PathContextFactory;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {p1, p2}, Lflow/path/PathContextFactory;->tearDownContext(Landroid/content/Context;)V

    :goto_0
    return-void

    .line 253
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown id: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getCount()I
    .locals 4

    .line 136
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, v0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->panelCount()I

    move-result v0

    const/16 v1, 0x9

    if-gt v0, v1, :cond_0

    return v0

    .line 138
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Only 9 pages allowed, not "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    .line 206
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->getPageId(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 207
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isKeypad(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object p1, p1, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->keypadPanelIndex()I

    move-result p1

    return p1

    .line 209
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isLibrary(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object p1, p1, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->libraryPanelIndex()I

    move-result p1

    return p1

    .line 211
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->isFavoritePage(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, v0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->extractFavoritePageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->pageIdIndex(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 214
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method getPageId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .line 235
    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->order_entry_page_id:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .line 170
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v0

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    new-instance p2, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 172
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object p2

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, v0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->keypadPanelIndex()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 179
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    sget v0, Lcom/squareup/orderentry/R$layout;->keypad_panel:I

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$102(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;

    .line 180
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$100(Lcom/squareup/orderentry/OrderEntryViewPager;)Landroid/view/View;

    move-result-object p2

    const-string v0, "keypad"

    goto :goto_1

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, v0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->libraryPanelIndex()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 183
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    sget v0, Lcom/squareup/orderentry/R$layout;->library_panel:I

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$202(Lcom/squareup/orderentry/OrderEntryViewPager;Landroid/view/View;)Landroid/view/View;

    .line 184
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$200(Lcom/squareup/orderentry/OrderEntryViewPager;)Landroid/view/View;

    move-result-object p2

    const-string v0, "library"

    goto :goto_1

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object v0, v0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->getPageAt(I)Lcom/squareup/orderentry/pages/OrderEntryPage;

    move-result-object p2

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "favorite-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getFavoritesPageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    new-instance v1, Lcom/squareup/orderentry/FavoritePageScreen;

    .line 190
    invoke-virtual {p2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getFavoritesPageId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getKey()Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/squareup/orderentry/FavoritePageScreen;-><init>(Ljava/lang/String;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 192
    invoke-virtual {v1}, Lcom/squareup/orderentry/FavoritePageScreen;->isAppRoot()Z

    move-result p2

    if-eqz p2, :cond_3

    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lflow/path/PathContext;->root(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object p2

    goto :goto_0

    :cond_3
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lflow/path/PathContext;->get(Landroid/content/Context;)Lflow/path/PathContext;

    move-result-object p2

    .line 193
    :goto_0
    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->pathContextFactory:Lflow/path/PathContextFactory;

    const/4 v3, 0x0

    new-array v3, v3, [Lflow/path/PathContext;

    invoke-static {v1, v2, p2, v3}, Lflow/path/PathContext;->create(Lflow/path/Path;Lflow/path/PathContextFactory;Lflow/path/PathContext;[Lflow/path/PathContext;)Lflow/path/PathContext;

    move-result-object p2

    .line 194
    invoke-static {p2, v1}, Lcom/squareup/flowlegacy/Layouts;->createView(Landroid/content/Context;Lcom/squareup/container/LayoutScreen;)Landroid/view/View;

    move-result-object p2

    .line 197
    :goto_1
    sget v1, Lcom/squareup/orderentry/R$id;->order_entry_page_id:I

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 199
    invoke-direct {p0, p2}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->restorePageState(Landroid/view/View;)V

    .line 201
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object p2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    .line 163
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    .line 164
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 165
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    const-string v0, "paging"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$002(Lcom/squareup/orderentry/OrderEntryViewPager;Z)Z

    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 3

    .line 148
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryViewPager;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 150
    iget-object v2, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {v2, v1}, Lcom/squareup/orderentry/OrderEntryViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->savePageState(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->this$0:Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-static {v1}, Lcom/squareup/orderentry/OrderEntryViewPager;->access$000(Lcom/squareup/orderentry/OrderEntryViewPager;)Z

    move-result v1

    const-string v2, "paging"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 154
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager$OrderEntryViewPagerAdapter;->state:Landroid/os/Bundle;

    return-object v0
.end method
