.class public final Lcom/squareup/orderentry/FavoritePageScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "FavoritePageScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/orderentry/FavoritePageScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/FavoritePageScreen$Component;,
        Lcom/squareup/orderentry/FavoritePageScreen$Presenter;
    }
.end annotation


# instance fields
.field private final orderEntryPageKey:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private final pageId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 94
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageScreen;->orderEntryPageKey:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    const-string p2, "pageId"

    .line 95
    invoke-static {p1, p2}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageScreen;->pageId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/FavoritePageScreen;)Ljava/lang/String;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/orderentry/FavoritePageScreen;->pageId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/FavoritePageScreen;)Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/orderentry/FavoritePageScreen;->orderEntryPageKey:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object p0
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_FAVORITE_PAGE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/seller/InSellerScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritePageScreen;->pageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 553
    sget v0, Lcom/squareup/orderentry/R$layout;->favorite_page:I

    return v0
.end method
