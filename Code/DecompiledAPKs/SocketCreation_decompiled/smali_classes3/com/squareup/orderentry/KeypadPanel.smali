.class public Lcom/squareup/orderentry/KeypadPanel;
.super Landroid/widget/LinearLayout;
.source "KeypadPanel.java"


# instance fields
.field private clearCardOrSalePopup:Lcom/squareup/orderentry/ClearCardOrSalePopup;

.field private clearCardPopup:Lcom/squareup/orderentry/ClearCardPopup;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private keypad:Lcom/squareup/padlock/Padlock;

.field private note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private price:Landroid/widget/TextView;

.field private quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/KeypadPanel;)V

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;->getAmounts()Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;->getAmounts()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method getClearCardOrSalePopup()Lcom/squareup/mortar/Popup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->clearCardOrSalePopup:Lcom/squareup/orderentry/ClearCardOrSalePopup;

    return-object v0
.end method

.method getClearCardPopup()Lcom/squareup/mortar/Popup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/flowlegacy/YesNo;",
            ">;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->clearCardPopup:Lcom/squareup/orderentry/ClearCardPopup;

    return-object v0
.end method

.method getKeypad()Lcom/squareup/padlock/Padlock;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->keypad:Lcom/squareup/padlock/Padlock;

    return-object v0
.end method

.method getPriceView()Landroid/view/View;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    return-object v0
.end method

.method public synthetic lambda$null$1$KeypadPanel(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    invoke-virtual {v0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->setQuickAmounts(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->onQuickAmountsVisible(Z)V

    return-void
.end method

.method public synthetic lambda$null$3$KeypadPanel(Lcom/squareup/quickamounts/ui/ClickEvent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->quickAmountClicked(Lcom/squareup/quickamounts/ui/ClickEvent;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$KeypadPanel()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->quickAmounts()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$yJ3D-lyulpEbmm4DVWzOtkfG5Jo;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$yJ3D-lyulpEbmm4DVWzOtkfG5Jo;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$1Jh6Mma44VWQ93zEd9etSxw2nTU;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$1Jh6Mma44VWQ93zEd9etSxw2nTU;-><init>(Lcom/squareup/orderentry/KeypadPanel;)V

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$4$KeypadPanel()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->quickAmountClickedView()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$joBsWqbxAgt1RWCzRdWeO7SK2vg;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$joBsWqbxAgt1RWCzRdWeO7SK2vg;-><init>(Lcom/squareup/orderentry/KeypadPanel;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    if-eqz v0, :cond_0

    .line 75
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$9mlL5yEYfpp4wa5eisai9WsaT54;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$9mlL5yEYfpp4wa5eisai9WsaT54;-><init>(Lcom/squareup/orderentry/KeypadPanel;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 84
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$JUzSVvNawDMGiEcsL4GXhvd7VJI;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/-$$Lambda$KeypadPanel$JUzSVvNawDMGiEcsL4GXhvd7VJI;-><init>(Lcom/squareup/orderentry/KeypadPanel;)V

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->onQuickAmountsVisible(Z)V

    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->onQuickAmountsVisible(Z)V

    .line 94
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->dropView(Lcom/squareup/orderentry/KeypadPanel;)V

    .line 95
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 47
    sget v0, Lcom/squareup/orderentry/R$id;->note_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 48
    sget v0, Lcom/squareup/orderentry/R$id;->home_panel_keypad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->keypad:Lcom/squareup/padlock/Padlock;

    .line 49
    sget v0, Lcom/squareup/quickamounts/ui/R$id;->quick_amounts:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->maybeFindById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/ui/QuickAmountsView;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->quickAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    .line 50
    sget v0, Lcom/squareup/orderentry/R$id;->price_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    .line 52
    new-instance v0, Lcom/squareup/orderentry/ClearCardPopup;

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/ClearCardPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->clearCardPopup:Lcom/squareup/orderentry/ClearCardPopup;

    .line 53
    new-instance v0, Lcom/squareup/orderentry/ClearCardOrSalePopup;

    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/ClearCardOrSalePopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->clearCardOrSalePopup:Lcom/squareup/orderentry/ClearCardOrSalePopup;

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/orderentry/KeypadPanel$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/KeypadPanel$1;-><init>(Lcom/squareup/orderentry/KeypadPanel;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget v0, Lcom/squareup/orderentry/R$drawable;->panel_background_clear_bottom_border:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/KeypadPanel;->setBackgroundResource(I)V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/KeypadPanelPresenter;->takeView(Ljava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->presenter:Lcom/squareup/orderentry/KeypadPanelPresenter;

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanel;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/KeypadPanelPresenter;->initializeKeypad(Lcom/squareup/padlock/Padlock;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .line 99
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/orderentry/KeypadPanel;->getOrientation()I

    move-result p1

    if-nez p1, :cond_0

    .line 101
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanel;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p1}, Lcom/squareup/padlock/Padlock;->getMeasuredHeight()I

    move-result p1

    int-to-float p1, p1

    const/high16 v0, 0x40800000    # 4.0f

    div-float/2addr p1, v0

    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr p1, v0

    float-to-int p1, p1

    const/high16 v0, 0x40000000    # 2.0f

    .line 102
    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 103
    iget-object v2, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 105
    iget-object v3, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 106
    iget-object v3, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 107
    iget-object v3, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v3, v2, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->measure(II)V

    .line 108
    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setMinHeight(I)V

    .line 110
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    sub-int/2addr p2, p1

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 112
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    sub-int/2addr p2, p1

    .line 114
    sget p1, Lcom/squareup/orderentry/R$id;->keypad_landscape_shim:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 115
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, v2, p2}, Landroid/view/View;->measure(II)V

    :cond_0
    return-void
.end method

.method setNoteText(Ljava/lang/CharSequence;Z)V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    .line 127
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeGlyph(I)V

    goto :goto_0

    .line 129
    :cond_0
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->NOTE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/orderentry/KeypadPanel;->note:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {p2, v0, p1, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    :goto_0
    return-void
.end method

.method setPriceText(Ljava/lang/CharSequence;Z)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/orderentry/KeypadPanel;->price:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public updateBackspaceEnabled(Z)V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadPanel;->keypad:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    return-void
.end method
