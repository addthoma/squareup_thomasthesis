.class Lcom/squareup/orderentry/NavigationBarSalePresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "NavigationBarSalePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/NavigationBarSalePresenter;->tabLongClicked(Lcom/squareup/orderentry/pages/OrderEntryPage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/NavigationBarSalePresenter;

.field final synthetic val$tab:Lcom/squareup/orderentry/pages/OrderEntryPage;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/NavigationBarSalePresenter;Lcom/squareup/orderentry/pages/OrderEntryPage;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->this$0:Lcom/squareup/orderentry/NavigationBarSalePresenter;

    iput-object p2, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->val$tab:Lcom/squareup/orderentry/pages/OrderEntryPage;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->this$0:Lcom/squareup/orderentry/NavigationBarSalePresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/NavigationBarSalePresenter;->access$000(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/PageTabLongPress;

    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->val$tab:Lcom/squareup/orderentry/pages/OrderEntryPage;

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->hasGlyph()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/squareup/orderentry/PageTabLongPress;-><init>(ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->this$0:Lcom/squareup/orderentry/NavigationBarSalePresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/NavigationBarSalePresenter;->access$100(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lcom/squareup/orderentry/OrderEntryScreenState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->startEditing()V

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->this$0:Lcom/squareup/orderentry/NavigationBarSalePresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/NavigationBarSalePresenter;->access$200(Lcom/squareup/orderentry/NavigationBarSalePresenter;)Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/PageLabelEditScreen;

    iget-object v2, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->val$tab:Lcom/squareup/orderentry/pages/OrderEntryPage;

    invoke-virtual {v2}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getFavoritesPageId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/orderentry/NavigationBarSalePresenter$1;->val$tab:Lcom/squareup/orderentry/pages/OrderEntryPage;

    invoke-virtual {v3}, Lcom/squareup/orderentry/pages/OrderEntryPage;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/orderentry/PageLabelEditScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
