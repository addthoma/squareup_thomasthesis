.class public final Lcom/squareup/orderentry/OrderEntryScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "OrderEntryScreen.java"

# interfaces
.implements Lcom/squareup/ui/seller/EnablesCardSwipes;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasWideTabletLayout;
.implements Lcom/squareup/orderentry/HasChargeButton;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/container/layer/HidesMaster;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$Responsive;
    phone = Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;
    tablet = Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;,
        Lcom/squareup/orderentry/OrderEntryScreen$TabletScope;,
        Lcom/squareup/orderentry/OrderEntryScreen$PhoneComponent;,
        Lcom/squareup/orderentry/OrderEntryScreen$PhoneScope;,
        Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;,
        Lcom/squareup/orderentry/OrderEntryScreen$TabletModule;,
        Lcom/squareup/orderentry/OrderEntryScreen$PhoneModule;,
        Lcom/squareup/orderentry/OrderEntryScreen$Module;,
        Lcom/squareup/orderentry/OrderEntryScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/OrderEntryScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryScreen;

.field public static final FAVORITES:Lcom/squareup/orderentry/OrderEntryScreen;

.field public static final KEYPAD:Lcom/squareup/orderentry/OrderEntryScreen;

.field public static final LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

.field private static final PULSE_DESTINATION:Z = true

.field private static final ROTATE_AND_FADE:Z = true


# instance fields
.field final mode:Lcom/squareup/orderentry/OrderEntryMode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreen;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreen;-><init>(Lcom/squareup/orderentry/OrderEntryMode;)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 107
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreen;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreen;-><init>(Lcom/squareup/orderentry/OrderEntryMode;)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->FAVORITES:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 108
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreen;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->KEYPAD:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreen;-><init>(Lcom/squareup/orderentry/OrderEntryMode;)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->KEYPAD:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 109
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreen;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryMode;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-direct {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreen;-><init>(Lcom/squareup/orderentry/OrderEntryMode;)V

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->EDIT_FAVORITES_FROM_ITEMS_APPLET:Lcom/squareup/orderentry/OrderEntryScreen;

    .line 629
    sget-object v0, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$PY2MsvROaEaIkzfOXZKS3h-kZvU;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$PY2MsvROaEaIkzfOXZKS3h-kZvU;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/orderentry/OrderEntryMode;)V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/orderentry/OrderEntryScreen;
    .locals 1

    .line 630
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 631
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreen;

    invoke-static {p0}, Lcom/squareup/orderentry/OrderEntryMode;->valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/OrderEntryMode;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen;-><init>(Lcom/squareup/orderentry/OrderEntryMode;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 626
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {p2}, Lcom/squareup/orderentry/OrderEntryMode;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 129
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_MAIN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/seller/InSellerScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreen;->mode:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 141
    const-class v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    .line 142
    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->homeScreenEmvCardStatusProcessor()Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 635
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_view:I

    return v0
.end method

.method public wideTabletLayout()I
    .locals 1

    .line 639
    sget v0, Lcom/squareup/orderentry/R$layout;->order_entry_view_wide:I

    return v0
.end method
