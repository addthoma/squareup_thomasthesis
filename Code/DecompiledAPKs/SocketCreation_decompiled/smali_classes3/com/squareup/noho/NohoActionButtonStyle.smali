.class public final enum Lcom/squareup/noho/NohoActionButtonStyle;
.super Ljava/lang/Enum;
.source "NohoActionButtonStyle.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoActionButtonStyle;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/noho/NohoActionButtonStyle;",
        "",
        "buttonType",
        "Lcom/squareup/noho/NohoButtonType;",
        "showsBottomEdge",
        "",
        "minimumWidth",
        "Lcom/squareup/resources/DimenModel;",
        "(Ljava/lang/String;ILcom/squareup/noho/NohoButtonType;ZLcom/squareup/resources/DimenModel;)V",
        "getButtonType",
        "()Lcom/squareup/noho/NohoButtonType;",
        "getMinimumWidth",
        "()Lcom/squareup/resources/DimenModel;",
        "getShowsBottomEdge",
        "()Z",
        "PRIMARY",
        "SECONDARY",
        "DESTRUCTIVE",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoActionButtonStyle;

.field public static final enum DESTRUCTIVE:Lcom/squareup/noho/NohoActionButtonStyle;

.field public static final enum PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

.field public static final enum SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;


# instance fields
.field private final buttonType:Lcom/squareup/noho/NohoButtonType;

.field private final minimumWidth:Lcom/squareup/resources/DimenModel;

.field private final showsBottomEdge:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v7, Lcom/squareup/noho/NohoActionButtonStyle;

    .line 21
    sget-object v4, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    .line 23
    new-instance v1, Lcom/squareup/resources/ResourceDimen;

    sget v2, Lcom/squareup/noho/R$dimen;->noho_topbar_action_width:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    move-object v6, v1

    check-cast v6, Lcom/squareup/resources/DimenModel;

    const-string v2, "PRIMARY"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, v7

    .line 20
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoActionButtonStyle;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoButtonType;ZLcom/squareup/resources/DimenModel;)V

    sput-object v7, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    const/4 v1, 0x0

    aput-object v7, v0, v1

    new-instance v2, Lcom/squareup/noho/NohoActionButtonStyle;

    .line 26
    sget-object v11, Lcom/squareup/noho/NohoButtonType;->LINK:Lcom/squareup/noho/NohoButtonType;

    .line 28
    invoke-static {v1}, Lcom/squareup/resources/DimenModelsKt;->getPx(I)Lcom/squareup/resources/FixedDimen;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/resources/DimenModel;

    const-string v9, "SECONDARY"

    const/4 v10, 0x1

    const/4 v12, 0x1

    move-object v8, v2

    .line 25
    invoke-direct/range {v8 .. v13}, Lcom/squareup/noho/NohoActionButtonStyle;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoButtonType;ZLcom/squareup/resources/DimenModel;)V

    sput-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    const/4 v1, 0x1

    aput-object v2, v0, v1

    new-instance v1, Lcom/squareup/noho/NohoActionButtonStyle;

    .line 31
    sget-object v6, Lcom/squareup/noho/NohoButtonType;->DESTRUCTIVE:Lcom/squareup/noho/NohoButtonType;

    .line 33
    new-instance v2, Lcom/squareup/resources/ResourceDimen;

    sget v3, Lcom/squareup/noho/R$dimen;->noho_topbar_action_width:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceDimen;-><init>(I)V

    move-object v8, v2

    check-cast v8, Lcom/squareup/resources/DimenModel;

    const-string v4, "DESTRUCTIVE"

    const/4 v5, 0x2

    const/4 v7, 0x0

    move-object v3, v1

    .line 30
    invoke-direct/range {v3 .. v8}, Lcom/squareup/noho/NohoActionButtonStyle;-><init>(Ljava/lang/String;ILcom/squareup/noho/NohoButtonType;ZLcom/squareup/resources/DimenModel;)V

    sput-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->DESTRUCTIVE:Lcom/squareup/noho/NohoActionButtonStyle;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoActionButtonStyle;->$VALUES:[Lcom/squareup/noho/NohoActionButtonStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/noho/NohoButtonType;ZLcom/squareup/resources/DimenModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoButtonType;",
            "Z",
            "Lcom/squareup/resources/DimenModel;",
            ")V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/noho/NohoActionButtonStyle;->buttonType:Lcom/squareup/noho/NohoButtonType;

    iput-boolean p4, p0, Lcom/squareup/noho/NohoActionButtonStyle;->showsBottomEdge:Z

    iput-object p5, p0, Lcom/squareup/noho/NohoActionButtonStyle;->minimumWidth:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoActionButtonStyle;
    .locals 1

    const-class v0, Lcom/squareup/noho/NohoActionButtonStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoActionButtonStyle;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoActionButtonStyle;
    .locals 1

    sget-object v0, Lcom/squareup/noho/NohoActionButtonStyle;->$VALUES:[Lcom/squareup/noho/NohoActionButtonStyle;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoActionButtonStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoActionButtonStyle;

    return-object v0
.end method


# virtual methods
.method public final getButtonType()Lcom/squareup/noho/NohoButtonType;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/noho/NohoActionButtonStyle;->buttonType:Lcom/squareup/noho/NohoButtonType;

    return-object v0
.end method

.method public final getMinimumWidth()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/noho/NohoActionButtonStyle;->minimumWidth:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getShowsBottomEdge()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionButtonStyle;->showsBottomEdge:Z

    return v0
.end method
