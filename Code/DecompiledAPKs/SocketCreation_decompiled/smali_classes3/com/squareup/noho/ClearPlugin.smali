.class public final Lcom/squareup/noho/ClearPlugin;
.super Lcom/squareup/noho/HideablePlugin;
.source "NohoEditRowPlugins.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/ClearPlugin$Visibility;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRowPlugins.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRowPlugins.kt\ncom/squareup/noho/ClearPlugin\n*L\n1#1,261:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001\u001eB9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0010\u0008\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u000bH\u0016J\u0008\u0010\u001c\u001a\u00020\u000eH\u0016J\u0008\u0010\u001d\u001a\u00020\u000bH\u0002R\u0014\u0010\r\u001a\u00020\u000e8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000fR\u0019\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/noho/ClearPlugin;",
        "Lcom/squareup/noho/HideablePlugin;",
        "context",
        "Landroid/content/Context;",
        "tintColor",
        "",
        "visibility",
        "",
        "Lcom/squareup/noho/ClearPlugin$Visibility;",
        "onClear",
        "Lkotlin/Function0;",
        "",
        "(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;)V",
        "isClickable",
        "",
        "()Z",
        "getOnClear",
        "()Lkotlin/jvm/functions/Function0;",
        "getVisibility",
        "()Ljava/util/Set;",
        "attach",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "description",
        "",
        "editDescription",
        "",
        "focusChanged",
        "onClick",
        "updateVisibility",
        "Visibility",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onClear:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final visibility:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/noho/ClearPlugin$Visibility;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/noho/ClearPlugin$Visibility;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "visibility"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-static {p1, p2}, Lcom/squareup/noho/NohoEditRowPluginsKt;->access$iconPluginForClear(Landroid/content/Context;I)Lcom/squareup/noho/IconPlugin;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    const/4 p2, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/noho/HideablePlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Plugin;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p3, p0, Lcom/squareup/noho/ClearPlugin;->visibility:Ljava/util/Set;

    iput-object p4, p0, Lcom/squareup/noho/ClearPlugin;->onClear:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    .line 144
    sget-object p3, Lcom/squareup/noho/ClearPlugin$Visibility;->ALWAYS:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-static {p3}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p3

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 145
    check-cast p4, Lkotlin/jvm/functions/Function0;

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$updateVisibility(Lcom/squareup/noho/ClearPlugin;)V
    .locals 0

    .line 141
    invoke-direct {p0}, Lcom/squareup/noho/ClearPlugin;->updateVisibility()V

    return-void
.end method

.method private final updateVisibility()V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/squareup/noho/ClearPlugin;->visibility:Ljava/util/Set;

    .line 165
    sget-object v1, Lcom/squareup/noho/ClearPlugin$Visibility;->ALWAYS:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_3

    .line 166
    sget-object v1, Lcom/squareup/noho/ClearPlugin$Visibility;->FOCUSED:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/noho/ClearPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditRow;->isFocused()Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    :cond_0
    sget-object v1, Lcom/squareup/noho/ClearPlugin$Visibility;->WITH_TEXT:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/squareup/noho/ClearPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    :cond_3
    const/4 v2, 0x1

    .line 164
    :cond_4
    invoke-virtual {p0, v2}, Lcom/squareup/noho/ClearPlugin;->setVisible(Z)V

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 2

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-super {p0, p1}, Lcom/squareup/noho/HideablePlugin;->attach(Lcom/squareup/noho/NohoEditRow;)V

    .line 157
    invoke-direct {p0}, Lcom/squareup/noho/ClearPlugin;->updateVisibility()V

    .line 158
    iget-object p1, p0, Lcom/squareup/noho/ClearPlugin;->visibility:Ljava/util/Set;

    sget-object v0, Lcom/squareup/noho/ClearPlugin$Visibility;->WITH_TEXT:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/squareup/noho/ClearPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    new-instance v0, Lcom/squareup/noho/SimpleTextWatcher;

    new-instance v1, Lcom/squareup/noho/ClearPlugin$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/noho/ClearPlugin$attach$1;-><init>(Lcom/squareup/noho/ClearPlugin;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/noho/SimpleTextWatcher;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_0
    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 2

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/squareup/noho/ClearPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    sget v1, Lcom/squareup/noho/R$string;->noho_editrow_plugin_clear:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "edit"

    .line 183
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 184
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 185
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public focusChanged()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lcom/squareup/noho/ClearPlugin;->updateVisibility()V

    return-void
.end method

.method public final getOnClear()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/noho/ClearPlugin;->onClear:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getVisibility()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/noho/ClearPlugin$Visibility;",
            ">;"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/noho/ClearPlugin;->visibility:Ljava/util/Set;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onClick()Z
    .locals 1

    .line 174
    invoke-virtual {p0}, Lcom/squareup/noho/ClearPlugin;->getEdit()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/ClearPlugin;->onClear:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    :cond_1
    const/4 v0, 0x0

    return v0
.end method
