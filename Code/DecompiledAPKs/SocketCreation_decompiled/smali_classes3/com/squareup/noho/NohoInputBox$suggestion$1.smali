.class public final Lcom/squareup/noho/NohoInputBox$suggestion$1;
.super Lkotlin/jvm/internal/Lambda;
.source "NohoInputBox.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoInputBox;->suggestion(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/noho/NohoInputBox$Suggestion;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoInputBox.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoInputBox.kt\ncom/squareup/noho/NohoInputBox$suggestion$1\n*L\n1#1,330:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoInputBox$Suggestion;",
        "text",
        "",
        "isFocused",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $checkAlways:Z

.field final synthetic $checker:Lkotlin/jvm/functions/Function1;

.field final synthetic $message:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLkotlin/jvm/functions/Function1;Ljava/lang/String;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$checkAlways:Z

    iput-object p2, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$checker:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$message:Ljava/lang/String;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/CharSequence;Z)Lcom/squareup/noho/NohoInputBox$Suggestion;
    .locals 2

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    iget-boolean v0, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$checkAlways:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 210
    :cond_0
    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$checker:Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/noho/NohoInputBox$Suggestion;

    iget-object p2, p0, Lcom/squareup/noho/NohoInputBox$suggestion$1;->$message:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-direct {p1, p2, v1, v0, v1}, Lcom/squareup/noho/NohoInputBox$Suggestion;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v1, p1

    :cond_1
    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoInputBox$suggestion$1;->invoke(Ljava/lang/CharSequence;Z)Lcom/squareup/noho/NohoInputBox$Suggestion;

    move-result-object p1

    return-object p1
.end method
