.class public final Lcom/squareup/noho/NohoDropdown;
.super Landroidx/appcompat/widget/AppCompatSpinner;
.source "NohoDropdown.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoDropdown$AndroidSpinnerAdapter;,
        Lcom/squareup/noho/NohoDropdown$Config;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDropdown.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDropdown.kt\ncom/squareup/noho/NohoDropdown\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,226:1\n159#1,2:227\n37#2,6:229\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDropdown.kt\ncom/squareup/noho/NohoDropdown\n*L\n148#1,2:227\n65#1,6:229\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010\u0015\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0002 !B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J4\u0010\u0010\u001a\u00020\u0011\"\u0004\u0008\u0000\u0010\u00122#\u0008\u0004\u0010\u0013\u001a\u001d\u0012\u000e\u0012\u000c\u0012\u0004\u0012\u0002H\u00120\u0015R\u00020\u0000\u0012\u0004\u0012\u00020\u00110\u0014\u00a2\u0006\u0002\u0008\u0016H\u0086\u0008J\u001a\u0010\u0010\u001a\u00020\u0011\"\u0004\u0008\u0000\u0010\u00122\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00120\u0018J\u0018\u0010\u0019\u001a\u000c\u0012\u0004\u0012\u0002H\u00120\u0015R\u00020\u0000\"\u0004\u0008\u0000\u0010\u0012H\u0001J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0007H\u0014J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\nH\u0016J\u0008\u0010\u001f\u001a\u00020\nH\u0016R\u001e\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0012\u0010\r\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/noho/NohoDropdown;",
        "Landroidx/appcompat/widget/AppCompatSpinner;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "<set-?>",
        "",
        "isOpen",
        "()Z",
        "menuViewLayoutId",
        "openInitiated",
        "selectedViewLayoutId",
        "config",
        "",
        "T",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/NohoDropdown$Config;",
        "Lkotlin/ExtensionFunctionType;",
        "data",
        "",
        "createConfig",
        "onCreateDrawableState",
        "",
        "extraSpace",
        "onWindowFocusChanged",
        "hasFocus",
        "performClick",
        "AndroidSpinnerAdapter",
        "Config",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private isOpen:Z

.field private menuViewLayoutId:I

.field private openInitiated:Z

.field private selectedViewLayoutId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoDropdown;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoDropdown;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatSpinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoDropdown:[I

    const-string v1, "R.styleable.NohoDropdown"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget v1, Lcom/squareup/noho/R$style;->Widget_Noho_Dropdown:I

    .line 229
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 231
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget p2, Lcom/squareup/noho/R$styleable;->NohoDropdown_sqSelectedViewLayoutId:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NohoDropdown;->access$setSelectedViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;I)V

    .line 72
    sget p2, Lcom/squareup/noho/R$styleable;->NohoDropdown_sqMenuViewLayoutId:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NohoDropdown;->access$setMenuViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;I)V

    .line 73
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 233
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 47
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 48
    sget p3, Lcom/squareup/noho/R$attr;->sqDropdownStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoDropdown;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getMenuViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;)I
    .locals 0

    .line 44
    iget p0, p0, Lcom/squareup/noho/NohoDropdown;->menuViewLayoutId:I

    return p0
.end method

.method public static final synthetic access$getSelectedViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;)I
    .locals 0

    .line 44
    iget p0, p0, Lcom/squareup/noho/NohoDropdown;->selectedViewLayoutId:I

    return p0
.end method

.method public static final synthetic access$mergeDrawableStates$s2666181([I[I)[I
    .locals 0

    .line 44
    invoke-static {p0, p1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setMenuViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/noho/NohoDropdown;->menuViewLayoutId:I

    return-void
.end method

.method public static final synthetic access$setSelectedViewLayoutId$p(Lcom/squareup/noho/NohoDropdown;I)V
    .locals 0

    .line 44
    iput p1, p0, Lcom/squareup/noho/NohoDropdown;->selectedViewLayoutId:I

    return-void
.end method


# virtual methods
.method public final config(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown;->createConfig()Lcom/squareup/noho/NohoDropdown$Config;

    move-result-object v0

    .line 148
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoDropdown$Config;->setData(Ljava/util/List;)V

    .line 227
    invoke-virtual {v0}, Lcom/squareup/noho/NohoDropdown$Config;->createAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoDropdown;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public final config(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/NohoDropdown$Config<",
            "TT;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown;->createConfig()Lcom/squareup/noho/NohoDropdown$Config;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoDropdown$Config;->createAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoDropdown;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public final createConfig()Lcom/squareup/noho/NohoDropdown$Config;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/noho/NohoDropdown$Config<",
            "TT;>;"
        }
    .end annotation

    .line 164
    new-instance v0, Lcom/squareup/noho/NohoDropdown$Config;

    iget v1, p0, Lcom/squareup/noho/NohoDropdown;->selectedViewLayoutId:I

    iget v2, p0, Lcom/squareup/noho/NohoDropdown;->menuViewLayoutId:I

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/noho/NohoDropdown$Config;-><init>(Lcom/squareup/noho/NohoDropdown;II)V

    return-object v0
.end method

.method public final isOpen()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/squareup/noho/NohoDropdown;->isOpen:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    .line 77
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatSpinner;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/noho/NohoDropdown;->isOpen:Z

    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/squareup/noho/NohoDropdownKt;->access$getSTATES_OPEN$p()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoDropdown;->access$mergeDrawableStates$s2666181([I[I)[I

    move-result-object p1

    const-string v0, "View.mergeDrawableStates(before, STATES_OPEN)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "before"

    .line 80
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 93
    iget-boolean v0, p0, Lcom/squareup/noho/NohoDropdown;->openInitiated:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 94
    iput-boolean p1, p0, Lcom/squareup/noho/NohoDropdown;->openInitiated:Z

    .line 95
    iput-boolean p1, p0, Lcom/squareup/noho/NohoDropdown;->isOpen:Z

    .line 96
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown;->refreshDrawableState()V

    :cond_0
    return-void
.end method

.method public performClick()Z
    .locals 1

    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lcom/squareup/noho/NohoDropdown;->openInitiated:Z

    .line 87
    iput-boolean v0, p0, Lcom/squareup/noho/NohoDropdown;->isOpen:Z

    .line 88
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown;->refreshDrawableState()V

    .line 89
    invoke-super {p0}, Landroidx/appcompat/widget/AppCompatSpinner;->performClick()Z

    move-result v0

    return v0
.end method
