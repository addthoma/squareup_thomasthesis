.class public final Lcom/squareup/noho/dsl/StickyHeadersSpec;
.super Ljava/lang/Object;
.source "StickyHeaders.kt"

# interfaces
.implements Lcom/squareup/cycler/ExtensionSpec;


# annotations
.annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;,
        Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "H:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/cycler/ExtensionSpec<",
        "TI;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStickyHeaders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeadersSpec\n*L\n1#1,193:1\n152#1,6:194\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0008\u0012\u0004\u0012\u0002H\u00010\u0006:\u0002 !B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0016H\u0016J1\u0010\u0015\u001a\u00020\u000c2)\u0010\u0017\u001a%\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\t\u00a2\u0006\u0002\u0008\rJF\u0010\u0015\u001a\u00020\u000c2\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u00192\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u001b2%\u0008\u0004\u0010\u0017\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\n\u0012\u0004\u0012\u00020\u000c0\u000f\u00a2\u0006\u0002\u0008\rH\u0086\u0008J\u001a\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u000bJ*\u0010\u001f\u001a\u00020\u000c2\"\u0010\u0017\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000fj\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0010R1\u0010\u0008\u001a%\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\n\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000c0\t\u00a2\u0006\u0002\u0008\rX\u0082.\u00a2\u0006\u0002\n\u0000R6\u0010\u000e\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000fj\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\u0010X\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "I",
        "",
        "H",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/ExtensionSpec;",
        "()V",
        "createBlock",
        "Lkotlin/Function2;",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;",
        "Landroid/content/Context;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "groupByBlock",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/dsl/GroupByLambda;",
        "getGroupByBlock$public_release",
        "()Lkotlin/jvm/functions/Function1;",
        "setGroupByBlock$public_release",
        "(Lkotlin/jvm/functions/Function1;)V",
        "create",
        "Lcom/squareup/cycler/Extension;",
        "block",
        "layoutId",
        "",
        "root",
        "Landroid/view/ViewGroup;",
        "createViewHolder",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;",
        "context",
        "groupBy",
        "Creator",
        "ViewHolder",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private createBlock:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator<",
            "TH;TV;>;-",
            "Landroid/content/Context;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field public groupByBlock:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+TH;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/noho/dsl/StickyHeadersSpec;ILandroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 150
    check-cast p2, Landroid/view/ViewGroup;

    :cond_0
    const-string p4, "block"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    new-instance p4, Lcom/squareup/noho/dsl/StickyHeadersSpec$create$1;

    invoke-direct {p4, p1, p2, p3}, Lcom/squareup/noho/dsl/StickyHeadersSpec$create$1;-><init>(ILandroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)V

    check-cast p4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, p4}, Lcom/squareup/noho/dsl/StickyHeadersSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method


# virtual methods
.method public create()Lcom/squareup/cycler/Extension;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Extension<",
            "TI;>;"
        }
    .end annotation

    .line 186
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeadersExtension;

    invoke-direct {v0, p0}, Lcom/squareup/noho/dsl/StickyHeadersExtension;-><init>(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V

    check-cast v0, Lcom/squareup/cycler/Extension;

    return-object v0
.end method

.method public final create(ILandroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/view/ViewGroup;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator<",
            "TH;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeadersSpec$create$1;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/noho/dsl/StickyHeadersSpec$create$1;-><init>(ILandroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final create(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator<",
            "TH;TV;>;-",
            "Landroid/content/Context;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersSpec;->createBlock:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final createViewHolder(Landroid/content/Context;)Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder<",
            "TH;TV;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/noho/dsl/StickyHeadersSpec;->createBlock:Lkotlin/jvm/functions/Function2;

    if-nez v1, :cond_0

    const-string v2, "createBlock"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v1, v0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-virtual {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    invoke-static {}, Lcom/squareup/noho/dsl/StickyHeadersKt;->access$getRECYCLER_ITEM_LAYOUT$p()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    new-instance p1, Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;

    invoke-virtual {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec$Creator;->getBindBlock$public_release()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-direct {p1, v1, v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V

    return-object p1
.end method

.method public final getGroupByBlock$public_release()Lkotlin/jvm/functions/Function1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TI;TH;>;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeadersSpec;->groupByBlock:Lkotlin/jvm/functions/Function1;

    if-nez v0, :cond_0

    const-string v1, "groupByBlock"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final groupBy(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+TH;>;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersSpec;->groupByBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setGroupByBlock$public_release(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+TH;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeadersSpec;->groupByBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method
