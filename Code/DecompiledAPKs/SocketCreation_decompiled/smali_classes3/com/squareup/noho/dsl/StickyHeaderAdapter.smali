.class public final Lcom/squareup/noho/dsl/StickyHeaderAdapter;
.super Ljava/lang/Object;
.source "StickyHeaders.kt"

# interfaces
.implements Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "H:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter<",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder<",
        "TH;TV;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStickyHeaders.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeaderAdapter\n+ 2 RecyclerData.kt\ncom/squareup/cycler/RecyclerData\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,193:1\n57#2,6:194\n63#2:207\n57#2,7:208\n347#3,7:200\n33#4,3:215\n*E\n*S KotlinDebug\n*F\n+ 1 StickyHeaders.kt\ncom/squareup/noho/dsl/StickyHeaderAdapter\n*L\n81#1,6:194\n81#1:207\n105#1,7:208\n81#1,7:200\n70#1,3:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010!\n\u0000\n\u0002\u0010%\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00070\u0006B\u001f\u0012\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J$\u0010\u001e\u001a\u00020\u001f2\u0012\u0010 \u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u001c\u0010!\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00072\u0006\u0010\"\u001a\u00020#H\u0016R#\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR7\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000e2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000e8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/StickyHeaderAdapter;",
        "I",
        "",
        "H",
        "V",
        "Landroid/view/View;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;",
        "config",
        "Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V",
        "getConfig",
        "()Lcom/squareup/noho/dsl/StickyHeadersSpec;",
        "<set-?>",
        "Lcom/squareup/cycler/RecyclerData;",
        "data",
        "getData",
        "()Lcom/squareup/cycler/RecyclerData;",
        "setData",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "data$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "headers",
        "",
        "positionMap",
        "",
        "",
        "getHeaderId",
        "position",
        "",
        "onBindHeaderViewHolder",
        "",
        "holder",
        "onCreateHeaderViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final config:Lcom/squareup/noho/dsl/StickyHeadersSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;"
        }
    .end annotation
.end field

.field private final data$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TH;>;"
        }
    .end annotation
.end field

.field private final positionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TH;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/dsl/StickyHeaderAdapter;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "data"

    const-string v4, "getData()Lcom/squareup/cycler/RecyclerData;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/dsl/StickyHeadersSpec;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;)V"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    .line 70
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    sget-object p1, Lcom/squareup/cycler/RecyclerData;->Companion:Lcom/squareup/cycler/RecyclerData$Companion;

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData$Companion;->empty()Lcom/squareup/cycler/RecyclerData;

    move-result-object p1

    .line 215
    new-instance v0, Lcom/squareup/noho/dsl/StickyHeaderAdapter$$special$$inlined$observable$1;

    invoke-direct {v0, p1, p1, p0}, Lcom/squareup/noho/dsl/StickyHeaderAdapter$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/dsl/StickyHeaderAdapter;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 217
    iput-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 77
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->positionMap:Ljava/util/Map;

    .line 78
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->headers:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$getHeaders$p(Lcom/squareup/noho/dsl/StickyHeaderAdapter;)Ljava/util/List;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->headers:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getPositionMap$p(Lcom/squareup/noho/dsl/StickyHeaderAdapter;)Ljava/util/Map;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->positionMap:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public final getConfig()Lcom/squareup/noho/dsl/StickyHeadersSpec;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec<",
            "TI;TH;TV;>;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    return-object v0
.end method

.method public final getData()Lcom/squareup/cycler/RecyclerData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cycler/RecyclerData;

    return-object v0
.end method

.method public getHeaderId(I)J
    .locals 4

    .line 81
    invoke-virtual {p0}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    .line 195
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    const-wide/16 v2, -0x1

    if-ne p1, v1, :cond_0

    .line 196
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 199
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-gez p1, :cond_1

    goto :goto_0

    :cond_1
    if-le v1, p1, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    .line 84
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    invoke-virtual {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec;->getGroupByBlock$public_release()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->positionMap:Ljava/util/Map;

    .line 200
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->headers:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v1, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->headers:Ljava/util/List;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 203
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    :cond_2
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    :cond_3
    :goto_0
    return-wide v2
.end method

.method public bridge synthetic onBindHeaderViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->onBindHeaderViewHolder(Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;I)V

    return-void
.end method

.method public onBindHeaderViewHolder(Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder<",
            "TH;TV;>;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItemIndex()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 210
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getHasExtraItem()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    goto :goto_0

    .line 213
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v1

    if-gez p2, :cond_1

    goto :goto_0

    :cond_1
    if-le v1, p2, :cond_2

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p2

    .line 109
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    invoke-virtual {v0}, Lcom/squareup/noho/dsl/StickyHeadersSpec;->getGroupByBlock$public_release()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 110
    invoke-virtual {p1, p2}, Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;->bind(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder<",
            "TH;TV;>;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->config:Lcom/squareup/noho/dsl/StickyHeadersSpec;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "parent.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/dsl/StickyHeadersSpec;->createViewHolder(Landroid/content/Context;)Lcom/squareup/noho/dsl/StickyHeadersSpec$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public final setData(Lcom/squareup/cycler/RecyclerData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/dsl/StickyHeaderAdapter;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
