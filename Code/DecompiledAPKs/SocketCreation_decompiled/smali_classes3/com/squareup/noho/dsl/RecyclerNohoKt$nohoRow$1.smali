.class public final Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/dsl/RecyclerNohoKt;->nohoRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "Lcom/squareup/noho/NohoRow;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1\n*L\n1#1,173:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/NohoRow;",
        "I",
        "",
        "S",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;-><init>()V

    sput-object v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoRow;
    .locals 7

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    new-instance v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$CreatorContext;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/cycler/Recycler$CreatorContext;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;->invoke(Lcom/squareup/cycler/Recycler$CreatorContext;)Lcom/squareup/noho/NohoRow;

    move-result-object p1

    return-object p1
.end method
