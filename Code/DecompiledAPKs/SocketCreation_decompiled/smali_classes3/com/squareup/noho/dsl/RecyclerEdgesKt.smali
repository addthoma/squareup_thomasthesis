.class public final Lcom/squareup/noho/dsl/RecyclerEdgesKt;
.super Ljava/lang/Object;
.source "RecyclerEdges.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerEdges.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 2 Recycler.kt\ncom/squareup/cycler/Recycler$RowSpec\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,186:1\n527#2:187\n534#2:188\n534#2:196\n527#2:204\n534#2:205\n305#3,7:189\n305#3,7:197\n305#3,7:206\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n52#1:187\n54#1:188\n70#1:196\n74#1:204\n76#1:205\n54#1,7:189\n70#1,7:197\n76#1,7:206\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a<\u0010\u0002\u001a\u00020\u0013\"\u0008\u0008\u0000\u0010\u0014*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\u00140\u00152\u001d\u0010\u0016\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00140\u0018\u0012\u0004\u0012\u00020\u00130\u0017\u00a2\u0006\u0002\u0008\u0019H\u0086\u0008\u001aY\u0010\u0002\u001a\u00020\u0013\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u0002H\u0003\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082\u001d\u0010\u0016\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00050\u001a\u0012\u0004\u0012\u00020\u00130\u0017\u00a2\u0006\u0002\u0008\u0019\"^\u0010\u0002\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u0002H\u0003\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u000e\u00a2\u0006\u0012\u0012\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000e\"^\u0010\u000f\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u0002H\u0003\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u00082\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u000e\u00a2\u0006\u0012\u0012\u0004\u0008\u0010\u0010\n\u001a\u0004\u0008\u0011\u0010\u000c\"\u0004\u0008\u0012\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "value",
        "",
        "edges",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/Recycler$RowSpec;",
        "edges$annotations",
        "(Lcom/squareup/cycler/Recycler$RowSpec;)V",
        "getEdges",
        "(Lcom/squareup/cycler/Recycler$RowSpec;)I",
        "setEdges",
        "(Lcom/squareup/cycler/Recycler$RowSpec;I)V",
        "edgesStyleRes",
        "edgesStyleRes$annotations",
        "getEdgesStyleRes",
        "setEdgesStyleRes",
        "",
        "T",
        "Lcom/squareup/cycler/Recycler$Config;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/dsl/EdgesExtensionSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final edges(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/dsl/EdgesExtensionSpec<",
            "TT;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$edges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    return-void
.end method

.method public static final edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;+TS;+TV;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/dsl/EdgesExtensionRowSpec<",
            "TS;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$edges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p0

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    .line 197
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 70
    new-instance v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-direct {v1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;-><init>()V

    .line 200
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_0
    check-cast v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static synthetic edges$annotations(Lcom/squareup/cycler/Recycler$RowSpec;)V
    .locals 0

    return-void
.end method

.method public static synthetic edgesStyleRes$annotations(Lcom/squareup/cycler/Recycler$RowSpec;)V
    .locals 0

    return-void
.end method

.method public static final getEdges(Lcom/squareup/cycler/Recycler$RowSpec;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;+TS;+TV;>;)I"
        }
    .end annotation

    const-string v0, "$this$edges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p0

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->getEdges()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final getEdgesStyleRes(Lcom/squareup/cycler/Recycler$RowSpec;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;+TS;+TV;>;)I"
        }
    .end annotation

    const-string v0, "$this$edgesStyleRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p0

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->getDefStyleRes()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;+TS;+TV;>;I)V"
        }
    .end annotation

    const-string v0, "$this$edges"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p0

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    .line 189
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 54
    new-instance v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-direct {v1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;-><init>()V

    .line 192
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_0
    check-cast v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->setEdges(I)V

    return-void
.end method

.method public static final setEdgesStyleRes(Lcom/squareup/cycler/Recycler$RowSpec;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/Recycler$RowSpec<",
            "TI;+TS;+TV;>;I)V"
        }
    .end annotation

    const-string v0, "$this$edgesStyleRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$RowSpec;->getExtensions()Ljava/util/Map;

    move-result-object p0

    const-class v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    .line 206
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 76
    new-instance v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-direct {v1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;-><init>()V

    .line 209
    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    :cond_0
    check-cast v1, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->setDefStyleRes(I)V

    return-void
.end method
