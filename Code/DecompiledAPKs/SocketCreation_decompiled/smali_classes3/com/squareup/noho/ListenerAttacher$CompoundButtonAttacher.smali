.class public final Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;
.super Lcom/squareup/noho/ListenerAttacher;
.source "CheckableGroups.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/ListenerAttacher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CompoundButtonAttacher"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/widget/CompoundButton;",
        ">",
        "Lcom/squareup/noho/ListenerAttacher<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u0008\u0008\u0001\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J9\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u00012\"\u0010\u0008\u001a\u001e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\tj\u0008\u0012\u0004\u0012\u00028\u0001`\u000bH\u0016\u00a2\u0006\u0002\u0010\u000cJ\u0015\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;",
        "T",
        "Landroid/widget/CompoundButton;",
        "Lcom/squareup/noho/ListenerAttacher;",
        "()V",
        "attach",
        "",
        "item",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "(Landroid/widget/CompoundButton;Lkotlin/jvm/functions/Function2;)V",
        "detach",
        "(Landroid/widget/CompoundButton;)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 271
    invoke-direct {p0, v0}, Lcom/squareup/noho/ListenerAttacher;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/widget/CompoundButton;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCheckedChange"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    new-instance v0, Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher$attach$1;

    invoke-direct {v0, p2}, Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher$attach$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public bridge synthetic attach(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 271
    check-cast p1, Landroid/widget/CompoundButton;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;->attach(Landroid/widget/CompoundButton;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public detach(Landroid/widget/CompoundButton;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 281
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public bridge synthetic detach(Ljava/lang/Object;)V
    .locals 0

    .line 271
    check-cast p1, Landroid/widget/CompoundButton;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/ListenerAttacher$CompoundButtonAttacher;->detach(Landroid/widget/CompoundButton;)V

    return-void
.end method
