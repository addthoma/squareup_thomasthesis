.class public Lcom/squareup/noho/NohoMasterDetailLayout;
.super Landroid/view/ViewGroup;
.source "NohoMasterDetailLayout.java"


# static fields
.field private static final DIVIDER_WIDTH_DP:I = 0x1

.field private static final MASTER_COL_RATIO:F = 0.3f


# instance fields
.field private detailView:Landroid/view/View;

.field private dividerPaint:Landroid/graphics/Paint;

.field private dividerWidth:I

.field private masterView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->densityDpi:I

    const/high16 v0, 0x3f800000    # 1.0f

    .line 39
    invoke-static {v0, p2}, Lcom/squareup/noho/Views;->dpToPxRounded(FI)I

    move-result p2

    iput p2, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerWidth:I

    .line 40
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerPaint:Landroid/graphics/Paint;

    .line 41
    sget p2, Lcom/squareup/noho/R$color;->noho_divider_hairline:I

    .line 42
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 43
    iget-object p2, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 p1, 0x0

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMasterDetailLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method private masterViewIsVisible()Z
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .line 96
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 97
    invoke-direct {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->masterViewIsVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v2, v0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerWidth:I

    add-int/2addr v0, v1

    int-to-float v4, v0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getHeight()I

    move-result v0

    int-to-float v5, v0

    iget-object v6, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 49
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const/4 v0, 0x0

    .line 50
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    const/4 v0, 0x1

    .line 51
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->detailView:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getMeasuredWidth()I

    move-result p1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->getMeasuredHeight()I

    move-result p2

    .line 82
    invoke-direct {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->masterViewIsVisible()Z

    move-result p3

    const/4 p4, 0x0

    if-nez p3, :cond_0

    .line 83
    iget-object p3, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->detailView:Landroid/view/View;

    invoke-virtual {p3, p4, p4, p1, p2}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object p3, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    .line 87
    iget-object p5, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    invoke-virtual {p5, p4, p4, p3, p2}, Landroid/view/View;->layout(IIII)V

    .line 90
    iget-object p5, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->detailView:Landroid/view/View;

    iget v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerWidth:I

    add-int/2addr p3, v0

    invoke-virtual {p5, p3, p4, p1, p2}, Landroid/view/View;->layout(IIII)V

    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 55
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 56
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 58
    invoke-direct {p0}, Lcom/squareup/noho/NohoMasterDetailLayout;->masterViewIsVisible()Z

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->detailView:Landroid/view/View;

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 61
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 60
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_0

    :cond_0
    int-to-float v0, p1

    const v2, 0x3e99999a    # 0.3f

    mul-float v2, v2, v0

    float-to-int v2, v2

    .line 65
    iget-object v3, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->masterView:Landroid/view/View;

    iget v4, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->dividerWidth:I

    sub-int/2addr v2, v4

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 66
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 65
    invoke-virtual {v3, v2, v4}, Landroid/view/View;->measure(II)V

    const v2, 0x3f333333    # 0.7f

    mul-float v0, v0, v2

    float-to-int v0, v0

    .line 70
    iget-object v2, p0, Lcom/squareup/noho/NohoMasterDetailLayout;->detailView:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 71
    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 70
    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 75
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoMasterDetailLayout;->setMeasuredDimension(II)V

    return-void
.end method
