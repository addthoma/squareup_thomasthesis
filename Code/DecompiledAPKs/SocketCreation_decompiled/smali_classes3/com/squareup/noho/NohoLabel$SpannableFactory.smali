.class final Lcom/squareup/noho/NohoLabel$SpannableFactory;
.super Landroid/text/Spannable$Factory;
.source "NohoLabel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoLabel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SpannableFactory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoLabel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoLabel.kt\ncom/squareup/noho/NohoLabel$SpannableFactory\n*L\n1#1,187:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/noho/NohoLabel$SpannableFactory;",
        "Landroid/text/Spannable$Factory;",
        "(Lcom/squareup/noho/NohoLabel;)V",
        "newSpannable",
        "Landroid/text/Spannable;",
        "source",
        "",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoLabel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 126
    iput-object p1, p0, Lcom/squareup/noho/NohoLabel$SpannableFactory;->this$0:Lcom/squareup/noho/NohoLabel;

    invoke-direct {p0}, Landroid/text/Spannable$Factory;-><init>()V

    return-void
.end method


# virtual methods
.method public newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 3

    .line 128
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/squareup/noho/NohoLabel$SpannableFactory;->this$0:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getFontSelection()Lcom/squareup/textappearance/FontSelection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v1, p0, Lcom/squareup/noho/NohoLabel$SpannableFactory;->this$0:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/text/Spannable;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/textappearance/FontSelection;->transform(Landroid/content/Context;Landroid/text/Spannable;)Landroid/text/Spannable;

    move-result-object p1

    return-object p1

    .line 133
    :cond_0
    invoke-super {p0, p1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p1

    const-string v0, "super.newSpannable(source)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
