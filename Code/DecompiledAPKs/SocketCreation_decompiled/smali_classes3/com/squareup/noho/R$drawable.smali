.class public final Lcom/squareup/noho/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final icon_arrow_left_40:I = 0x7f080237

.field public static final icon_menu_3_40:I = 0x7f08025c

.field public static final icon_x_40:I = 0x7f08026f

.field public static final noho_actionbar_icon_back_arrow:I = 0x7f0803a1

.field public static final noho_actionbar_icon_burger:I = 0x7f0803a2

.field public static final noho_actionbar_icon_x:I = 0x7f0803a3

.field public static final noho_background:I = 0x7f0803a4

.field public static final noho_border_light:I = 0x7f0803a5

.field public static final noho_bordered_selector_edit_text:I = 0x7f0803a6

.field public static final noho_button_destructive_background_disabled:I = 0x7f0803a7

.field public static final noho_button_destructive_background_enabled:I = 0x7f0803a8

.field public static final noho_button_destructive_background_pressed:I = 0x7f0803a9

.field public static final noho_button_link_background_disabled:I = 0x7f0803aa

.field public static final noho_button_link_background_enabled:I = 0x7f0803ab

.field public static final noho_button_link_background_pressed:I = 0x7f0803ac

.field public static final noho_button_primary_background_disabled:I = 0x7f0803ad

.field public static final noho_button_primary_background_enabled:I = 0x7f0803ae

.field public static final noho_button_primary_background_pressed:I = 0x7f0803af

.field public static final noho_button_secondary_background_disabled:I = 0x7f0803b0

.field public static final noho_button_secondary_background_enabled:I = 0x7f0803b1

.field public static final noho_button_secondary_background_pressed:I = 0x7f0803b2

.field public static final noho_button_tertiary_background_disabled:I = 0x7f0803b3

.field public static final noho_button_tertiary_background_enabled:I = 0x7f0803b4

.field public static final noho_button_tertiary_background_pressed:I = 0x7f0803b5

.field public static final noho_check_vector_on:I = 0x7f0803b6

.field public static final noho_check_vector_selector:I = 0x7f0803b7

.field public static final noho_clear:I = 0x7f0803b8

.field public static final noho_divider_both_hairline:I = 0x7f0803b9

.field public static final noho_divider_column_left:I = 0x7f0803ba

.field public static final noho_divider_column_right:I = 0x7f0803bb

.field public static final noho_divider_gutter:I = 0x7f0803bc

.field public static final noho_divider_gutter_half:I = 0x7f0803bd

.field public static final noho_divider_horizontal_hairline:I = 0x7f0803be

.field public static final noho_divider_horizontal_hairline_padding:I = 0x7f0803bf

.field public static final noho_divider_vertical_hairline:I = 0x7f0803c0

.field public static final noho_dropdown_button:I = 0x7f0803c1

.field public static final noho_dropdown_button_closed:I = 0x7f0803c2

.field public static final noho_dropdown_button_open:I = 0x7f0803c3

.field public static final noho_dropdown_menu_background:I = 0x7f0803c4

.field public static final noho_dropdown_menu_item_background:I = 0x7f0803c5

.field public static final noho_dropdown_menu_item_background_normal:I = 0x7f0803c6

.field public static final noho_dropdown_menu_item_background_pressed:I = 0x7f0803c7

.field public static final noho_edit_background:I = 0x7f0803c8

.field public static final noho_edit_background_base:I = 0x7f0803c9

.field public static final noho_edit_background_base_first:I = 0x7f0803ca

.field public static final noho_edit_background_base_middle:I = 0x7f0803cb

.field public static final noho_edit_background_disabled:I = 0x7f0803cc

.field public static final noho_edit_background_disabled_first:I = 0x7f0803cd

.field public static final noho_edit_background_disabled_middle:I = 0x7f0803ce

.field public static final noho_edit_background_errored:I = 0x7f0803cf

.field public static final noho_edit_background_selected:I = 0x7f0803d0

.field public static final noho_edit_background_validated:I = 0x7f0803d1

.field public static final noho_edit_cursor_drawable:I = 0x7f0803d2

.field public static final noho_edit_text_background:I = 0x7f0803d3

.field public static final noho_edit_text_background_disabled:I = 0x7f0803d4

.field public static final noho_edit_text_background_enabled:I = 0x7f0803d5

.field public static final noho_edit_text_blank:I = 0x7f0803d6

.field public static final noho_edit_text_focused:I = 0x7f0803d7

.field public static final noho_icon_button_background:I = 0x7f0803d8

.field public static final noho_icon_button_background_normal:I = 0x7f0803d9

.field public static final noho_icon_button_background_pressed:I = 0x7f0803da

.field public static final noho_notification_balloon:I = 0x7f0803db

.field public static final noho_notification_fatal_balloon:I = 0x7f0803dc

.field public static final noho_notification_fatal_filterout:I = 0x7f0803dd

.field public static final noho_notification_filterout:I = 0x7f0803de

.field public static final noho_notification_highpri_balloon:I = 0x7f0803df

.field public static final noho_radio_vector_off:I = 0x7f0803e0

.field public static final noho_radio_vector_on:I = 0x7f0803e1

.field public static final noho_radio_vector_selector:I = 0x7f0803e2

.field public static final noho_right_caret:I = 0x7f0803e3

.field public static final noho_row_background:I = 0x7f0803e4

.field public static final noho_row_background_activated:I = 0x7f0803e5

.field public static final noho_row_background_pressed:I = 0x7f0803e6

.field public static final noho_selectable_background:I = 0x7f0803e7

.field public static final noho_selectable_background_checked:I = 0x7f0803e8

.field public static final noho_selectable_background_disabled:I = 0x7f0803e9

.field public static final noho_selectable_background_normal:I = 0x7f0803ea

.field public static final noho_selector_destructive_button_background:I = 0x7f0803eb

.field public static final noho_selector_dialog_button_background:I = 0x7f0803ec

.field public static final noho_selector_link_button_background:I = 0x7f0803ed

.field public static final noho_selector_primary_button_background:I = 0x7f0803ee

.field public static final noho_selector_primary_tutorial_button_background:I = 0x7f0803ef

.field public static final noho_selector_row_background:I = 0x7f0803f0

.field public static final noho_selector_secondary_button_background:I = 0x7f0803f1

.field public static final noho_selector_tertiary_button_background:I = 0x7f0803f2

.field public static final noho_standard_cyan:I = 0x7f0803f3

.field public static final noho_standard_cyan_dark:I = 0x7f0803f4

.field public static final noho_standard_gray_10:I = 0x7f0803f5

.field public static final noho_standard_gray_20:I = 0x7f0803f6

.field public static final noho_standard_night_almost_black:I = 0x7f0803f7

.field public static final noho_standard_night_dark:I = 0x7f0803f8

.field public static final noho_standard_night_extra_dark:I = 0x7f0803f9

.field public static final noho_standard_night_gray:I = 0x7f0803fa

.field public static final noho_standard_white:I = 0x7f0803fb

.field public static final noho_switch_base:I = 0x7f0803fc

.field public static final noho_switch_base_off:I = 0x7f0803fd

.field public static final noho_switch_base_on:I = 0x7f0803fe

.field public static final noho_switch_thumb:I = 0x7f0803ff

.field public static final noho_tutorial_button_primary_background_disabled:I = 0x7f080400

.field public static final noho_tutorial_button_primary_background_enabled:I = 0x7f080401

.field public static final noho_tutorial_button_primary_background_pressed:I = 0x7f080402

.field public static final noho_white_border_bottom_light_gray_1px:I = 0x7f080403

.field public static final transparent_selector:I = 0x7f0804d1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
