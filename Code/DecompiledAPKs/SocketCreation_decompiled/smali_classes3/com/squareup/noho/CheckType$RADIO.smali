.class public final Lcom/squareup/noho/CheckType$RADIO;
.super Lcom/squareup/noho/CheckType$ImageCheckType;
.source "NohoCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/CheckType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RADIO"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/noho/CheckType$RADIO;",
        "Lcom/squareup/noho/CheckType$ImageCheckType;",
        "()V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/CheckType$RADIO;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 163
    new-instance v0, Lcom/squareup/noho/CheckType$RADIO;

    invoke-direct {v0}, Lcom/squareup/noho/CheckType$RADIO;-><init>()V

    sput-object v0, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 165
    sget v0, Lcom/squareup/noho/R$drawable;->noho_radio_vector_selector:I

    .line 166
    sget v1, Lcom/squareup/noho/R$attr;->nohoRadioColors:I

    const/4 v2, 0x0

    .line 163
    invoke-direct {p0, v2, v0, v1}, Lcom/squareup/noho/CheckType$ImageCheckType;-><init>(ZII)V

    return-void
.end method
