.class public final Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;
.super Lcom/squareup/noho/NohoActionBar$ActionConfig;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoActionBar$ActionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ActionIconConfig"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0080\u0008\u0018\u00002\u00020\u0001B;\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0002\u0010\rJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u00c6\u0003JG\u0010\u001f\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u00032\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u00c6\u0001J\u0013\u0010 \u001a\u00020\u00082\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u00d6\u0003J\t\u0010#\u001a\u00020\u0003H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0017R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;",
        "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
        "iconRes",
        "",
        "tooltip",
        "Lcom/squareup/resources/TextModel;",
        "",
        "isEnabled",
        "",
        "badgeCount",
        "command",
        "Lkotlin/Function0;",
        "",
        "(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)V",
        "badge",
        "Lcom/squareup/noho/Badge;",
        "getBadge",
        "()Lcom/squareup/noho/Badge;",
        "getBadgeCount",
        "()I",
        "getCommand",
        "()Lkotlin/jvm/functions/Function0;",
        "getIconRes",
        "()Z",
        "getTooltip",
        "()Lcom/squareup/resources/TextModel;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badgeCount:I

.field private final command:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final iconRes:I

.field private final isEnabled:Z

.field private final tooltip:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;ZI",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 328
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoActionBar$ActionConfig;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    iput-object p2, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    iput-boolean p3, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    iput p4, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    iput-object p5, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget p1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->copy(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    return v0
.end method

.method public final component2()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    return v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;ZI",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;"
        }
    .end annotation

    const-string/jumbo v0, "tooltip"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "command"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    move-object v1, v0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;-><init>(ILcom/squareup/resources/TextModel;ZILkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    iget v1, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    iget v1, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBadge()Lcom/squareup/noho/Badge;
    .locals 4

    .line 329
    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    const/4 v1, 0x0

    if-ltz v0, :cond_0

    new-instance v2, Lcom/squareup/noho/Badge;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    invoke-direct {v2, v0, v1, v3, v1}, Lcom/squareup/noho/Badge;-><init>(Ljava/lang/String;Lcom/squareup/noho/Badge$Type;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v1, v2

    :cond_0
    return-object v1
.end method

.method public final getBadgeCount()I
    .locals 1

    .line 326
    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    return v0
.end method

.method public final getCommand()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 327
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getIconRes()I
    .locals 1

    .line 323
    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    return v0
.end method

.method public final getTooltip()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 324
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    .line 325
    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActionIconConfig(iconRes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->iconRes:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tooltip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->tooltip:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", badgeCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->badgeCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", command="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->command:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
