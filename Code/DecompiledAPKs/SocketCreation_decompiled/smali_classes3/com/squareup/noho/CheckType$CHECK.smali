.class public final Lcom/squareup/noho/CheckType$CHECK;
.super Lcom/squareup/noho/CheckType$ImageCheckType;
.source "NohoCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/CheckType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CHECK"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/noho/CheckType$CHECK;",
        "Lcom/squareup/noho/CheckType$ImageCheckType;",
        "()V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/CheckType$CHECK;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 169
    new-instance v0, Lcom/squareup/noho/CheckType$CHECK;

    invoke-direct {v0}, Lcom/squareup/noho/CheckType$CHECK;-><init>()V

    sput-object v0, Lcom/squareup/noho/CheckType$CHECK;->INSTANCE:Lcom/squareup/noho/CheckType$CHECK;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 171
    sget v0, Lcom/squareup/noho/R$drawable;->noho_check_vector_selector:I

    .line 172
    sget v1, Lcom/squareup/noho/R$attr;->nohoCheckColors:I

    const/4 v2, 0x1

    .line 169
    invoke-direct {p0, v2, v0, v1}, Lcom/squareup/noho/CheckType$ImageCheckType;-><init>(ZII)V

    return-void
.end method
