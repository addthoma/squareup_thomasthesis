.class public final Lcom/squareup/noho/NohoButtonKt;
.super Ljava/lang/Object;
.source "NohoButton.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoButton.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoButton.kt\ncom/squareup/noho/NohoButtonKt\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,219:1\n37#2,6:220\n*E\n*S KotlinDebug\n*F\n+ 1 NohoButton.kt\ncom/squareup/noho/NohoButtonKt\n*L\n210#1,6:220\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a$\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0001H\u0002\u00a8\u0006\u0007"
    }
    d2 = {
        "defStyleFromButtonType",
        "",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$defStyleFromButtonType(Landroid/content/Context;Landroid/util/AttributeSet;I)I
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/noho/NohoButtonKt;->defStyleFromButtonType(Landroid/content/Context;Landroid/util/AttributeSet;I)I

    move-result p0

    return p0
.end method

.method private static final defStyleFromButtonType(Landroid/content/Context;Landroid/util/AttributeSet;I)I
    .locals 3

    .line 212
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoButton:[I

    const-string v1, "R.styleable.NohoButton"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    sget v1, Lcom/squareup/noho/R$attr;->nohoButtonStyle:I

    .line 214
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_PrimaryButton:I

    .line 220
    invoke-virtual {p0, p1, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p0

    :try_start_0
    const-string p1, "a"

    .line 222
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    sget p1, Lcom/squareup/noho/R$styleable;->NohoButton_sqButtonType:I

    invoke-static {}, Lcom/squareup/noho/NohoButtonType;->values()[Lcom/squareup/noho/NohoButtonType;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;I[Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButtonType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p1, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/squareup/noho/NohoButtonType;->getStyleAttr()I

    move-result p2

    :cond_0
    return p2

    :catchall_0
    move-exception p1

    .line 224
    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method
