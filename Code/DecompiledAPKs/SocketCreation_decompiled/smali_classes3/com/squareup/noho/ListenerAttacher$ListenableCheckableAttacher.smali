.class public final Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;
.super Lcom/squareup/noho/ListenerAttacher;
.source "CheckableGroups.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/ListenerAttacher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListenableCheckableAttacher"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/noho/ListenableCheckable<",
        "TT;>;>",
        "Lcom/squareup/noho/ListenerAttacher<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u000e\u0008\u0001\u0010\u0001*\u0008\u0012\u0004\u0012\u0002H\u00010\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J9\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u00012\"\u0010\u0008\u001a\u001e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00060\tj\u0008\u0012\u0004\u0012\u00028\u0001`\u000bH\u0016\u00a2\u0006\u0002\u0010\u000cJ\u0015\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;",
        "T",
        "Lcom/squareup/noho/ListenableCheckable;",
        "Lcom/squareup/noho/ListenerAttacher;",
        "()V",
        "attach",
        "",
        "item",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "(Lcom/squareup/noho/ListenableCheckable;Lkotlin/jvm/functions/Function2;)V",
        "detach",
        "(Lcom/squareup/noho/ListenableCheckable;)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 250
    invoke-direct {p0, v0}, Lcom/squareup/noho/ListenerAttacher;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/ListenableCheckable;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCheckedChange"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-interface {p1, p2}, Lcom/squareup/noho/ListenableCheckable;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public bridge synthetic attach(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/noho/ListenableCheckable;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;->attach(Lcom/squareup/noho/ListenableCheckable;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public detach(Lcom/squareup/noho/ListenableCheckable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 256
    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-interface {p1, v0}, Lcom/squareup/noho/ListenableCheckable;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public bridge synthetic detach(Ljava/lang/Object;)V
    .locals 0

    .line 250
    check-cast p1, Lcom/squareup/noho/ListenableCheckable;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;->detach(Lcom/squareup/noho/ListenableCheckable;)V

    return-void
.end method
