.class public final Lcom/squareup/noho/NohoRow$FeatureInstance;
.super Ljava/lang/Object;
.source "NohoRow.kt"

# interfaces
.implements Lcom/squareup/noho/NohoRow$Arrangeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FeatureInstance"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/noho/NohoRow$Arrangeable;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0010\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u00020\u0004B)\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00018\u0001\u0012\u0006\u0010\u0006\u001a\u00028\u0000\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nR\u0014\u0010\u000b\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0010R\u0014\u0010\t\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\rR\u001a\u0010\u0007\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0012\u0010\r\"\u0004\u0008\u0013\u0010\u0014R\u001c\u0010\u0006\u001a\u00028\u0000X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\u0008\u0015\u0010\u0016\"\u0004\u0008\u0017\u0010\u0018R\u001e\u0010\u0005\u001a\u0004\u0018\u00018\u0001X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001e\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001d\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$FeatureInstance;",
        "T",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "view",
        "value",
        "styleId",
        "",
        "margin",
        "(Landroid/view/View;Ljava/lang/Object;II)V",
        "id",
        "getId",
        "()I",
        "isEnabled",
        "",
        "()Z",
        "getMargin",
        "getStyleId",
        "setStyleId",
        "(I)V",
        "getValue",
        "()Ljava/lang/Object;",
        "setValue",
        "(Ljava/lang/Object;)V",
        "Ljava/lang/Object;",
        "getView",
        "()Landroid/view/View;",
        "setView",
        "(Landroid/view/View;)V",
        "Landroid/view/View;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final margin:I

.field private styleId:I

.field private value:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private view:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/Object;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TT;II)V"
        }
    .end annotation

    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->value:Ljava/lang/Object;

    iput p3, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->styleId:I

    iput p4, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->margin:I

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 480
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoRow$Arrangeable$DefaultImpls;->get(Lcom/squareup/noho/NohoRow$Arrangeable;Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object p1

    return-object p1
.end method

.method public getId()I
    .locals 1

    .line 487
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->view:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    return v0
.end method

.method public getMargin()I
    .locals 1

    .line 484
    iget v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->margin:I

    return v0
.end method

.method public final getStyleId()I
    .locals 1

    .line 483
    iget v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->styleId:I

    return v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 482
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public final getView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .line 481
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->view:Landroid/view/View;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 488
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final setStyleId(I)V
    .locals 0

    .line 483
    iput p1, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->styleId:I

    return-void
.end method

.method public final setValue(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 482
    iput-object p1, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->value:Ljava/lang/Object;

    return-void
.end method

.method public final setView(Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .line 481
    iput-object p1, p0, Lcom/squareup/noho/NohoRow$FeatureInstance;->view:Landroid/view/View;

    return-void
.end method
