.class final Lcom/squareup/instantdeposit/PriceChangeDialog$Factory$create$1;
.super Ljava/lang/Object;
.source "PriceChangeDialog.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/instantdeposit/PriceChangeDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "onDismiss"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $runner:Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/instantdeposit/PriceChangeDialog$Factory$create$1;->$runner:Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .line 39
    iget-object p1, p0, Lcom/squareup/instantdeposit/PriceChangeDialog$Factory$create$1;->$runner:Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;

    invoke-interface {p1}, Lcom/squareup/instantdeposit/PriceChangeDialog$Runner;->onPriceChangeDismissed()V

    return-void
.end method
