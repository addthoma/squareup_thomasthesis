.class public final Lcom/squareup/instantdeposit/RealInstantDepositAnalyticsKt;
.super Ljava/lang/Object;
.source "RealInstantDepositAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\"\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0015\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0016\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0017\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0018\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0019\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001a\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001b\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001c\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001d\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001e\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u001f\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010 \u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010!\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\"\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "ADD_DEBIT_CARD_ATTEMPT",
        "",
        "ADD_DEBIT_CARD_CANCEL",
        "ADD_DEBIT_CARD_RESULT",
        "ALLOW",
        "CHANGE_DEBIT_CARD",
        "CHANGE_DEBIT_CARD_ATTEMPT",
        "CHANGE_DEBIT_CARD_CANCEL",
        "CHANGE_DEBIT_CARD_RESULT",
        "DEPOSITS_APPLET_ADD_DEBIT_CARD_ATTEMPT",
        "DEPOSITS_APPLET_ADD_DEBIT_CARD_RESULT",
        "DEPOSITS_APPLET_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS_CLICK",
        "DEPOSITS_APPLET_DEPOSITS_REPORT_DEPOSIT_SUCCESSFUL",
        "DEPOSITS_APPLET_DEPOSITS_REPORT_SHOW_BALANCE",
        "DEPOSITS_APPLET_DEPOSIT_AVAILABLE_FUNDS_CLICK",
        "DEPOSITS_APPLET_DEPOSIT_ESTIMATED_FEES",
        "DEPOSITS_APPLET_DEPOSIT_ESTIMATED_FEES_LEARN_MORE",
        "DEPOSITS_APPLET_DEPOSIT_SUCCESSFUL",
        "DEPOSITS_APPLET_LINK_DEBIT_CARD",
        "DEPOSITS_APPLET_LINK_DEBIT_CARD_CANCEL",
        "DEPOSITS_APPLET_SET_UP_INSTANT_DEPOSIT",
        "DEPOSITS_APPLET_SHOW_BALANCE",
        "DEPOSITS_REPORT_ACTIVE_SALES",
        "DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS_CLICK",
        "DEPOSITS_REPORT_PENDING_DEPOSIT",
        "DEPOSITS_REPORT_SENT_DEPOSIT",
        "DEPOSITS_REPORT_SHOW_BALANCE",
        "DEPOSITS_REPORT_SHOW_ERROR",
        "DEPOSITS_REPORT_SUMMARY",
        "DISALLOW",
        "OFF",
        "ON",
        "TRANSACTIONS_DEPOSIT_AVAILABLE_FUNDS_CLICK",
        "TRANSACTIONS_SHOW_BALANCE",
        "TRANSACTIONS_SHOW_ERROR",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ADD_DEBIT_CARD_ATTEMPT:Ljava/lang/String; = "Settings Instant Deposit: Add Debit Card Attempt"

.field private static final ADD_DEBIT_CARD_CANCEL:Ljava/lang/String; = "Settings Instant Deposit: Add Debit Card Cancel"

.field private static final ADD_DEBIT_CARD_RESULT:Ljava/lang/String; = "Settings Instant Deposit: Add Debit Card Result"

.field private static final ALLOW:Ljava/lang/String; = "Settings Instant Deposit: Allow Instant Deposit"

.field private static final CHANGE_DEBIT_CARD:Ljava/lang/String; = "Settings Instant Deposit: Change Debit Card"

.field private static final CHANGE_DEBIT_CARD_ATTEMPT:Ljava/lang/String; = "Settings Instant Deposit: Change Debit Card Attempt"

.field private static final CHANGE_DEBIT_CARD_CANCEL:Ljava/lang/String; = "Settings Instant Deposit: Change Debit Card Cancel"

.field private static final CHANGE_DEBIT_CARD_RESULT:Ljava/lang/String; = "Settings Instant Deposit: Change Debit Card Result"

.field private static final DEPOSITS_APPLET_ADD_DEBIT_CARD_ATTEMPT:Ljava/lang/String; = "Deposits: Add Debit Card Attempt"

.field private static final DEPOSITS_APPLET_ADD_DEBIT_CARD_RESULT:Ljava/lang/String; = "Deposits: Add Debit Card Result"

.field private static final DEPOSITS_APPLET_DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS_CLICK:Ljava/lang/String; = "Deposits: Deposit Reports Deposit Available Funds Click"

.field private static final DEPOSITS_APPLET_DEPOSITS_REPORT_DEPOSIT_SUCCESSFUL:Ljava/lang/String; = "Deposits: Deposit Reports Deposit Successful"

.field private static final DEPOSITS_APPLET_DEPOSITS_REPORT_SHOW_BALANCE:Ljava/lang/String; = "Deposits: Deposit Reports Showing Available Balance"

.field private static final DEPOSITS_APPLET_DEPOSIT_AVAILABLE_FUNDS_CLICK:Ljava/lang/String; = "Deposits: Deposit Available Funds Click"

.field private static final DEPOSITS_APPLET_DEPOSIT_ESTIMATED_FEES:Ljava/lang/String; = "Deposits: Deposit Reports Estimated Fees Info"

.field private static final DEPOSITS_APPLET_DEPOSIT_ESTIMATED_FEES_LEARN_MORE:Ljava/lang/String; = "Deposits: Deposit Reports Learn More From Estimated Fees Info"

.field private static final DEPOSITS_APPLET_DEPOSIT_SUCCESSFUL:Ljava/lang/String; = "Deposits: Deposit Successful"

.field private static final DEPOSITS_APPLET_LINK_DEBIT_CARD:Ljava/lang/String; = "Deposits: Link Debit Card"

.field private static final DEPOSITS_APPLET_LINK_DEBIT_CARD_CANCEL:Ljava/lang/String; = "Deposits: Link Debit Card Cancel"

.field private static final DEPOSITS_APPLET_SET_UP_INSTANT_DEPOSIT:Ljava/lang/String; = "Deposits: Set Up Instant Deposit"

.field private static final DEPOSITS_APPLET_SHOW_BALANCE:Ljava/lang/String; = "Deposits: Showing Available Balance"

.field private static final DEPOSITS_REPORT_ACTIVE_SALES:Ljava/lang/String; = "Deposits: Deposit Reports Active Sales"

.field private static final DEPOSITS_REPORT_DEPOSIT_AVAILABLE_FUNDS_CLICK:Ljava/lang/String; = "Deposits Report: Deposit Available Funds Click"

.field private static final DEPOSITS_REPORT_PENDING_DEPOSIT:Ljava/lang/String; = "Deposits: Deposit Reports Pending Deposit"

.field private static final DEPOSITS_REPORT_SENT_DEPOSIT:Ljava/lang/String; = "Deposits: Deposits Reports Sent Deposit"

.field private static final DEPOSITS_REPORT_SHOW_BALANCE:Ljava/lang/String; = "Deposits Report: Showing Available Balance"

.field private static final DEPOSITS_REPORT_SHOW_ERROR:Ljava/lang/String; = "Deposits Report: Showing Error"

.field private static final DEPOSITS_REPORT_SUMMARY:Ljava/lang/String; = "Deposits: Deposit Reports Summary"

.field private static final DISALLOW:Ljava/lang/String; = "Settings Instant Deposit: Disallow Instant Deposit"

.field private static final OFF:Ljava/lang/String; = "OFF"

.field private static final ON:Ljava/lang/String; = "ON"

.field private static final TRANSACTIONS_DEPOSIT_AVAILABLE_FUNDS_CLICK:Ljava/lang/String; = "Transactions: Deposit Available Funds Click"

.field private static final TRANSACTIONS_SHOW_BALANCE:Ljava/lang/String; = "Transactions: Showing Available Balance"

.field private static final TRANSACTIONS_SHOW_ERROR:Ljava/lang/String; = "Transactions: Showing Error"
