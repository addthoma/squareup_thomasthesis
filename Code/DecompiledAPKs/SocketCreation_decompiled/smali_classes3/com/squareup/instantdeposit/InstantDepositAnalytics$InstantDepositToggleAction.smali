.class public final Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "InstantDepositAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstantDepositToggleAction"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "detail",
        "",
        "(Ljava/lang/String;)V",
        "getDetail",
        "()Ljava/lang/String;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final detail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "detail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->INSTANT_DEPOSIT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    iput-object p1, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;->detail:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getDetail()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$InstantDepositToggleAction;->detail:Ljava/lang/String;

    return-object v0
.end method
