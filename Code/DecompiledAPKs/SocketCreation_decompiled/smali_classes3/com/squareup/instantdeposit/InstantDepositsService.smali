.class public interface abstract Lcom/squareup/instantdeposit/InstantDepositsService;
.super Ljava/lang/Object;
.source "InstantDepositsService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/instantdeposit/InstantDepositsService$GetBalanceSummaryStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0006J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositsService;",
        "",
        "getBalanceSummary",
        "Lcom/squareup/instantdeposit/InstantDepositsService$GetBalanceSummaryStandardResponse;",
        "request",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;",
        "GetBalanceSummaryStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getBalanceSummary(Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;)Lcom/squareup/instantdeposit/InstantDepositsService$GetBalanceSummaryStandardResponse;
    .param p1    # Lcom/squareup/protos/client/deposits/GetBalanceSummaryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/deposits/get-balance-summary"
    .end annotation
.end method
