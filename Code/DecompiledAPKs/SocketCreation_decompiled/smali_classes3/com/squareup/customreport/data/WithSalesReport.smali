.class public final Lcom/squareup/customreport/data/WithSalesReport;
.super Lcom/squareup/customreport/data/SalesReport;
.source "SalesReport.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/WithSalesReport$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0018\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\"\u001a\u00020\tH\u00c6\u0003J\t\u0010#\u001a\u00020\u000bH\u00c6\u0003J\t\u0010$\u001a\u00020\rH\u00c6\u0003J\t\u0010%\u001a\u00020\u000fH\u00c6\u0003JO\u0010&\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000fH\u00c6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\u0013\u0010)\u001a\u00020*2\u0008\u0010+\u001a\u0004\u0018\u00010,H\u00d6\u0003J\t\u0010-\u001a\u00020(H\u00d6\u0001J\t\u0010.\u001a\u00020/H\u00d6\u0001J\u0019\u00100\u001a\u0002012\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u00020(H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001e\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/customreport/data/WithSalesReport;",
        "Lcom/squareup/customreport/data/SalesReport;",
        "reportDate",
        "Lorg/threeten/bp/ZonedDateTime;",
        "salesSummaryReport",
        "Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "salesChartReport",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "discountsReport",
        "Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "topCategoriesReport",
        "Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "topItemsReport",
        "Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "paymentMethodsReport",
        "Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)V",
        "getDiscountsReport",
        "()Lcom/squareup/customreport/data/SalesDiscountsReport;",
        "getPaymentMethodsReport",
        "()Lcom/squareup/customreport/data/SalesPaymentMethodsReport;",
        "getReportDate",
        "()Lorg/threeten/bp/ZonedDateTime;",
        "getSalesChartReport",
        "()Lcom/squareup/customreport/data/SalesChartReport;",
        "getSalesSummaryReport",
        "()Lcom/squareup/customreport/data/WithSalesSummaryReport;",
        "getTopCategoriesReport",
        "()Lcom/squareup/customreport/data/SalesTopCategoriesReport;",
        "getTopItemsReport",
        "()Lcom/squareup/customreport/data/SalesTopItemsReport;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

.field private final paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

.field private final reportDate:Lorg/threeten/bp/ZonedDateTime;

.field private final salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

.field private final salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

.field private final topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

.field private final topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/WithSalesReport$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/WithSalesReport$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/WithSalesReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)V
    .locals 1

    const-string v0, "reportDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSummaryReport"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesChartReport"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountsReport"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topCategoriesReport"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topItemsReport"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentMethodsReport"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/customreport/data/SalesReport;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    iput-object p2, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    iput-object p3, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    iput-object p4, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    iput-object p5, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    iput-object p6, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    iput-object p7, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/WithSalesReport;Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;ILjava/lang/Object;)Lcom/squareup/customreport/data/WithSalesReport;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/customreport/data/WithSalesReport;->copy(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public final component2()Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    return-object v0
.end method

.method public final component3()Lcom/squareup/customreport/data/SalesChartReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    return-object v0
.end method

.method public final component4()Lcom/squareup/customreport/data/SalesDiscountsReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    return-object v0
.end method

.method public final component5()Lcom/squareup/customreport/data/SalesTopCategoriesReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    return-object v0
.end method

.method public final component6()Lcom/squareup/customreport/data/SalesTopItemsReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    return-object v0
.end method

.method public final component7()Lcom/squareup/customreport/data/SalesPaymentMethodsReport;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    return-object v0
.end method

.method public final copy(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)Lcom/squareup/customreport/data/WithSalesReport;
    .locals 9

    const-string v0, "reportDate"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesSummaryReport"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "salesChartReport"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountsReport"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topCategoriesReport"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "topItemsReport"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentMethodsReport"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/WithSalesReport;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/customreport/data/WithSalesReport;-><init>(Lorg/threeten/bp/ZonedDateTime;Lcom/squareup/customreport/data/WithSalesSummaryReport;Lcom/squareup/customreport/data/SalesChartReport;Lcom/squareup/customreport/data/SalesDiscountsReport;Lcom/squareup/customreport/data/SalesTopCategoriesReport;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/customreport/data/SalesPaymentMethodsReport;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/WithSalesReport;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/WithSalesReport;

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    iget-object v1, p1, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    iget-object p1, p1, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDiscountsReport()Lcom/squareup/customreport/data/SalesDiscountsReport;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    return-object v0
.end method

.method public final getPaymentMethodsReport()Lcom/squareup/customreport/data/SalesPaymentMethodsReport;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    return-object v0
.end method

.method public final getReportDate()Lorg/threeten/bp/ZonedDateTime;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    return-object v0
.end method

.method public final getSalesChartReport()Lcom/squareup/customreport/data/SalesChartReport;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    return-object v0
.end method

.method public final getSalesSummaryReport()Lcom/squareup/customreport/data/WithSalesSummaryReport;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    return-object v0
.end method

.method public final getTopCategoriesReport()Lcom/squareup/customreport/data/SalesTopCategoriesReport;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    return-object v0
.end method

.method public final getTopItemsReport()Lcom/squareup/customreport/data/SalesTopItemsReport;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WithSalesReport(reportDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", salesSummaryReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", salesChartReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discountsReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", topCategoriesReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", topItemsReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", paymentMethodsReport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->reportDate:Lorg/threeten/bp/ZonedDateTime;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesSummaryReport:Lcom/squareup/customreport/data/WithSalesSummaryReport;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->salesChartReport:Lcom/squareup/customreport/data/SalesChartReport;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->discountsReport:Lcom/squareup/customreport/data/SalesDiscountsReport;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topCategoriesReport:Lcom/squareup/customreport/data/SalesTopCategoriesReport;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->topItemsReport:Lcom/squareup/customreport/data/SalesTopItemsReport;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/WithSalesReport;->paymentMethodsReport:Lcom/squareup/customreport/data/SalesPaymentMethodsReport;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
