.class public final Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;
.super Ljava/lang/Object;
.source "RealRemoteCustomReportStore_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/customreport/data/RealRemoteCustomReportStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p4, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p5, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;)",
            "Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;"
        }
    .end annotation

    .line 43
    new-instance v6, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/CustomReportService;)Lcom/squareup/customreport/data/RealRemoteCustomReportStore;
    .locals 7

    .line 48
    new-instance v6, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/CustomReportService;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/customreport/data/RealRemoteCustomReportStore;
    .locals 5

    .line 37
    iget-object v0, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/DeviceIdProvider;

    iget-object v3, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/time/CurrentTimeZone;

    iget-object v4, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/customreport/data/service/CustomReportService;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/CustomReportService;)Lcom/squareup/customreport/data/RealRemoteCustomReportStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore_Factory;->get()Lcom/squareup/customreport/data/RealRemoteCustomReportStore;

    move-result-object v0

    return-object v0
.end method
