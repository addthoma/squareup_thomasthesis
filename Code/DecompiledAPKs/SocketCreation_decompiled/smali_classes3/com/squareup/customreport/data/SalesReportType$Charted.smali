.class public abstract Lcom/squareup/customreport/data/SalesReportType$Charted;
.super Lcom/squareup/customreport/data/SalesReportType;
.source "SalesReportType.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/customreport/data/SalesReportType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Charted"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;,
        Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;,
        Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;,
        Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "Lcom/squareup/customreport/data/SalesReportType;",
        "groupByType",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)V",
        "getGroupByType",
        "()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "SalesByDay",
        "SalesByDayOfWeek",
        "SalesByHourOfDay",
        "SalesByMonth",
        "Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;",
        "Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;",
        "Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;",
        "Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)V
    .locals 2

    .line 90
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/customreport/data/SalesReportType;-><init>(Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/customreport/data/SalesReportType$Charted;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/SalesReportType$Charted;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)V

    return-void
.end method


# virtual methods
.method public final getGroupByType()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/customreport/data/SalesReportType$Charted;->groupByType:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object v0
.end method
