.class public abstract Lcom/squareup/customreport/data/SalesReportType;
.super Ljava/lang/Object;
.source "SalesReportType.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/SalesReportType$Aggregate;,
        Lcom/squareup/customreport/data/SalesReportType$CategorizedItemSales;,
        Lcom/squareup/customreport/data/SalesReportType$CategorizedItemWithMeasurementUnitSales;,
        Lcom/squareup/customreport/data/SalesReportType$Discounts;,
        Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;,
        Lcom/squareup/customreport/data/SalesReportType$ItemSales;,
        Lcom/squareup/customreport/data/SalesReportType$ItemWithMeasurementUnitSales;,
        Lcom/squareup/customreport/data/SalesReportType$PaymentMethods;,
        Lcom/squareup/customreport/data/SalesReportType$Charted;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportType.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportType.kt\ncom/squareup/customreport/data/SalesReportType\n*L\n1#1,132:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\t\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010B\u0015\u0008\u0002\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u0082\u0001\t\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/customreport/data/SalesReportType;",
        "Landroid/os/Parcelable;",
        "groupByTypes",
        "",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "(Ljava/util/List;)V",
        "getGroupByTypes",
        "()Ljava/util/List;",
        "Aggregate",
        "CategorizedItemSales",
        "CategorizedItemWithMeasurementUnitSales",
        "Charted",
        "Discounts",
        "ItemCategorySales",
        "ItemSales",
        "ItemWithMeasurementUnitSales",
        "PaymentMethods",
        "Lcom/squareup/customreport/data/SalesReportType$Aggregate;",
        "Lcom/squareup/customreport/data/SalesReportType$CategorizedItemSales;",
        "Lcom/squareup/customreport/data/SalesReportType$CategorizedItemWithMeasurementUnitSales;",
        "Lcom/squareup/customreport/data/SalesReportType$Discounts;",
        "Lcom/squareup/customreport/data/SalesReportType$ItemCategorySales;",
        "Lcom/squareup/customreport/data/SalesReportType$ItemSales;",
        "Lcom/squareup/customreport/data/SalesReportType$ItemWithMeasurementUnitSales;",
        "Lcom/squareup/customreport/data/SalesReportType$PaymentMethods;",
        "Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final groupByTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/SalesReportType;->groupByTypes:Ljava/util/List;

    .line 24
    iget-object p1, p0, Lcom/squareup/customreport/data/SalesReportType;->groupByTypes:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Grouping types can\'t be empty."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/customreport/data/SalesReportType;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final getGroupByTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/customreport/data/SalesReportType;->groupByTypes:Ljava/util/List;

    return-object v0
.end method
