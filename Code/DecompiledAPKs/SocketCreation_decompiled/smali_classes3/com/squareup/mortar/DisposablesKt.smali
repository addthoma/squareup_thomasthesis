.class public final Lcom/squareup/mortar/DisposablesKt;
.super Ljava/lang/Object;
.source "Disposables.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "disposeOnExit",
        "",
        "Lio/reactivex/disposables/Disposable;",
        "scope",
        "Lmortar/MortarScope;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "$this$disposeOnExit"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    invoke-interface {p0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void

    .line 18
    :cond_0
    new-instance v0, Lcom/squareup/mortar/DisposablesKt$disposeOnExit$1;

    invoke-direct {v0, p0}, Lcom/squareup/mortar/DisposablesKt$disposeOnExit$1;-><init>(Lio/reactivex/disposables/Disposable;)V

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
