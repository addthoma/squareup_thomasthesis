.class public abstract Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action;
.super Ljava/lang/Object;
.source "RealVerifyCardChangeWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowSuccessMessage;,
        Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;,
        Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/debitcard/VerifyCardChangeState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0003*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0006H\u0016\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/debitcard/VerifyCardChangeState;",
        "",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Finish",
        "ShowFailureMessage",
        "ShowSuccessMessage",
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowSuccessMessage;",
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;",
        "Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 74
    invoke-direct {p0}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 74
    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/debitcard/VerifyCardChangeState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/debitcard/VerifyCardChangeState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    sget-object v0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowSuccessMessage;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowSuccessMessage;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    sget-object v0, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingSuccessMessage;->INSTANCE:Lcom/squareup/debitcard/VerifyCardChangeState$ShowingSuccessMessage;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :cond_0
    instance-of v0, p0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;

    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;

    .line 81
    move-object v1, p0

    check-cast v1, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;

    invoke-virtual {v1}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {v1}, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$ShowFailureMessage;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-direct {v0, v2, v1}, Lcom/squareup/debitcard/VerifyCardChangeState$ShowingFailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 85
    :cond_1
    sget-object v0, Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;->INSTANCE:Lcom/squareup/debitcard/RealVerifyCardChangeWorkflow$Action$Finish;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
