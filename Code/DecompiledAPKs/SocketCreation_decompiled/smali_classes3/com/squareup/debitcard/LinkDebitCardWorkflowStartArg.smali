.class public abstract Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;
.super Ljava/lang/Object;
.source "LinkDebitCardWorkflowStartArg.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartedFrom;,
        Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;,
        Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u0082\u0001\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;",
        "Landroid/os/Parcelable;",
        "()V",
        "describeContents",
        "",
        "StartWithPrepareToLinkCard",
        "StartWithResendingEmail",
        "StartedFrom",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithPrepareToLinkCard;",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg$StartWithResendingEmail;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardWorkflowStartArg;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
