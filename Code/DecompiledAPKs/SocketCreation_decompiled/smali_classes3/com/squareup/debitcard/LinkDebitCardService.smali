.class public interface abstract Lcom/squareup/debitcard/LinkDebitCardService;
.super Ljava/lang/Object;
.source "LinkDebitCardService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardService$VerifyCardChangeStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u000eJ\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'J\u0018\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0001\u0010\u0004\u001a\u00020\tH\'J\u0018\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b2\u0008\u0008\u0001\u0010\u0004\u001a\u00020\rH\'\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardService;",
        "",
        "confirmCard",
        "Lcom/squareup/debitcard/LinkDebitCardService$VerifyCardChangeStandardResponse;",
        "request",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest;",
        "linkCard",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        "resendVerificationEmail",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse;",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;",
        "VerifyCardChangeStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract confirmCard(Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest;)Lcom/squareup/debitcard/LinkDebitCardService$VerifyCardChangeStandardResponse;
    .param p1    # Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/instant-deposits/cards/confirm-card"
    .end annotation
.end method

.method public abstract linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/instant-deposits/link-card"
    .end annotation
.end method

.method public abstract resendVerificationEmail(Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/instant-deposits/resend-verification-email"
    .end annotation
.end method
