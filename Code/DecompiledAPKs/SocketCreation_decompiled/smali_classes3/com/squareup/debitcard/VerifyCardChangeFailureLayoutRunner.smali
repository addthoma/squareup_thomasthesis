.class public final Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;
.super Ljava/lang/Object;
.source "VerifyCardChangeFailureLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u000e2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u000eB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u000c\u001a\u00020\rH\u0016R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;


# instance fields
.field private final messageView:Lcom/squareup/noho/NohoMessageView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->Companion:Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->view:Landroid/view/View;

    .line 18
    iget-object p1, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/debitcard/impl/R$id;->verify_card_change_failure_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object p2, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$showRendering$1;-><init>(Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 25
    iget-object p2, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 26
    iget-object p2, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;->getMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    .line 27
    iget-object p2, p0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->messageView:Lcom/squareup/noho/NohoMessageView;

    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner$showRendering$2;-><init>(Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v0, "debounce { rendering.onDone() }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/VerifyCardChangeFailureLayoutRunner;->showRendering(Lcom/squareup/debitcard/VerifyCardChangeFailureScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
