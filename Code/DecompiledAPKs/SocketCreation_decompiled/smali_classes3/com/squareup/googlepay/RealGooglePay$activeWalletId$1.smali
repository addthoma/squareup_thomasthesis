.class final Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay;->activeWalletId(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/SingleSource<",
        "+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a*\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0014\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/SingleSource;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $createOnFailure:Z

.field final synthetic this$0:Lcom/squareup/googlepay/RealGooglePay;


# direct methods
.method constructor <init>(Lcom/squareup/googlepay/RealGooglePay;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    iput-boolean p2, p0, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;->$createOnFailure:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Throwable;)Lio/reactivex/SingleSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/SingleSource<",
            "+",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, p1, Lcom/squareup/googlepay/GooglePayException$WalletIdNotFoundException;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;->$createOnFailure:Z

    if-eqz v0, :cond_0

    .line 77
    new-instance p1, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1$1;

    invoke-direct {p1, p0}, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1$1;-><init>(Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;)V

    check-cast p1, Ljava/util/concurrent/Callable;

    invoke-static {p1}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p1

    .line 78
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;->this$0:Lcom/squareup/googlepay/RealGooglePay;

    invoke-static {v0}, Lcom/squareup/googlepay/RealGooglePay;->access$getMainScheduler$p(Lcom/squareup/googlepay/RealGooglePay;)Lio/reactivex/Scheduler;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 79
    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1$2;-><init>(Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    goto :goto_0

    .line 83
    :cond_0
    invoke-static {p1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;->apply(Ljava/lang/Throwable;)Lio/reactivex/SingleSource;

    move-result-object p1

    return-object p1
.end method
