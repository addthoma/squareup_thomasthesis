.class public final Lcom/squareup/googlepay/GooglePay$DefaultImpls;
.super Ljava/lang/Object;
.source "GooglePay.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/googlepay/GooglePay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic pushTokenize$default(Lcom/squareup/googlepay/GooglePay;[BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p6, :cond_1

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    .line 82
    sget-object p4, Lcom/squareup/googlepay/GooglePayAddress;->Companion:Lcom/squareup/googlepay/GooglePayAddress$Companion;

    invoke-virtual {p4}, Lcom/squareup/googlepay/GooglePayAddress$Companion;->getEMPTY()Lcom/squareup/googlepay/GooglePayAddress;

    move-result-object p4

    :cond_0
    invoke-interface {p0, p1, p2, p3, p4}, Lcom/squareup/googlepay/GooglePay;->pushTokenize([BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: pushTokenize"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
