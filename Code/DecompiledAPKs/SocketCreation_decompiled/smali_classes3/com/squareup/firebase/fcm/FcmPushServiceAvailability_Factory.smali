.class public final Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;
.super Ljava/lang/Object;
.source "FcmPushServiceAvailability_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;",
        ">;"
    }
.end annotation


# instance fields
.field private final firebaseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/common/Firebase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/common/Firebase;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;->firebaseProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/common/Firebase;",
            ">;)",
            "Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/firebase/common/Firebase;)Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;

    invoke-direct {v0, p0}, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;-><init>(Lcom/squareup/firebase/common/Firebase;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;->firebaseProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/firebase/common/Firebase;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;->newInstance(Lcom/squareup/firebase/common/Firebase;)Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/firebase/fcm/FcmPushServiceAvailability_Factory;->get()Lcom/squareup/firebase/fcm/FcmPushServiceAvailability;

    move-result-object v0

    return-object v0
.end method
