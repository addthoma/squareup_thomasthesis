.class public abstract Lcom/squareup/firebase/common/RealFirebaseModule;
.super Ljava/lang/Object;
.source "RealFirebaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/firebase/common/SharedFirebaseModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindFirebase(Lcom/squareup/firebase/common/RealFirebase;)Lcom/squareup/firebase/common/Firebase;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
