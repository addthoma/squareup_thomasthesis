.class public final Lcom/squareup/dipper/events/CardReaderDataEventFilters;
.super Ljava/lang/Object;
.source "CardReaderDataEventFilters.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0004\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/dipper/events/CardReaderDataEventFilters;",
        "",
        "()V",
        "aclEventFilter",
        "Lrx/Observable;",
        "Lcom/squareup/dipper/events/AclEvent;",
        "observable",
        "Lcom/squareup/dipper/events/CardReaderDataEvent;",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/dipper/events/CardReaderDataEventFilters;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/dipper/events/CardReaderDataEventFilters;

    invoke-direct {v0}, Lcom/squareup/dipper/events/CardReaderDataEventFilters;-><init>()V

    sput-object v0, Lcom/squareup/dipper/events/CardReaderDataEventFilters;->INSTANCE:Lcom/squareup/dipper/events/CardReaderDataEventFilters;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aclEventFilter(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/CardReaderDataEvent;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/dipper/events/AclEvent;",
            ">;"
        }
    .end annotation

    const-string v0, "observable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    const-class v0, Lcom/squareup/dipper/events/AclEvent;

    invoke-virtual {p1, v0}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object p1

    const-string v0, "observable.ofType(AclEvent::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
