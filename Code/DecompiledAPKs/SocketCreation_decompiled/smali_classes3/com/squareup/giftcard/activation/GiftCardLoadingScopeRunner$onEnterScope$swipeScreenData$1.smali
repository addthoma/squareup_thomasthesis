.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        "card",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/Card;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/GiftCards;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 287
    sget-object p1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;->loading()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1

    .line 289
    :cond_0
    sget-object p1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->Companion:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;

    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$Companion;->swipeError()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipeScreenData$1;->call(Lcom/squareup/Card;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    move-result-object p1

    return-object p1
.end method
