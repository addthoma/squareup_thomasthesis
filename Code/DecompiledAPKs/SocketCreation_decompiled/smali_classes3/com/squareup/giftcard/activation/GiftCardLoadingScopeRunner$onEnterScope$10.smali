.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002 \u0004*.\u0012(\u0012&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012&\u0010\u0005\u001a\"\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0008 \u0004*\u0010\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;->call(Lkotlin/Pair;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lkotlin/Pair;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "+",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    .line 257
    sget-object v1, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;->OTHER:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    if-ne p1, v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCardService$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getClearReasonText$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    const-string v3, "clearReasonText"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v0, p1, v2}, Lcom/squareup/giftcard/GiftCardServiceHelper;->clearBalanceWithReasonText(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 260
    :cond_0
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getGiftCardService$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/GiftCardServiceHelper;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/giftcard/GiftCardServiceHelper;->clearBalance(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    const-string v0, "(if (second == ClearBala\u2026 second)\n              })"

    .line 257
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/SingleSource;

    .line 261
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 262
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    .line 263
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10$1;

    invoke-direct {v0, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;)V

    check-cast v0, Lrx/functions/Action0;

    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    .line 264
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10$2;

    invoke-direct {v0, p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$10;)V

    check-cast v0, Lrx/functions/Action0;

    invoke-virtual {p1, v0}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
