.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $invalidChipCard:Lcom/squareup/Card;


# direct methods
.method constructor <init>(Lcom/squareup/Card;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;->$invalidChipCard:Lcom/squareup/Card;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;)Lcom/squareup/Card;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;->$invalidChipCard:Lcom/squareup/Card;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$x2SwipedChipCard$1;->apply(Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;)Lcom/squareup/Card;

    move-result-object p1

    return-object p1
.end method
