.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardAddValueCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardAddValueCoordinator.kt\ncom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2\n*L\n1#1,262:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)V
    .locals 9

    const/4 v0, 0x0

    if-eqz p1, :cond_8

    .line 113
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    const-string v2, "response.status.success"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 114
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v1, v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 115
    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    .line 117
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v2}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v2

    .line 118
    iget-object v3, v1, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    if-eqz v3, :cond_0

    .line 119
    iget-object v3, v1, Lcom/squareup/protos/client/giftcards/GiftCard;->balance_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    :cond_0
    const-wide/16 v3, 0x0

    .line 121
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v5}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getCurrencyCode$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 117
    :goto_0
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 124
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v3, v3, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 125
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 126
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v5}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/util/Res;

    move-result-object v5

    sget v6, Lcom/squareup/giftcard/activation/R$string;->gift_card_name_balance_format:I

    invoke-interface {v5, v6}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    .line 129
    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v6}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/util/Res;

    move-result-object v6

    sget-object v7, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    iget-object v8, v1, Lcom/squareup/protos/client/giftcards/GiftCard;->pan_suffix:Ljava/lang/String;

    .line 128
    invoke-static {v6, v7, v8}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    const-string v7, "name"

    .line 127
    invoke-virtual {v5, v7, v6}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v5

    const-string v6, "balance"

    .line 132
    invoke-virtual {v5, v6, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 133
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 124
    invoke-virtual {v3, v4, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 137
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$presetAmountContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 138
    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->preset:Lcom/squareup/protos/client/giftcards/Preset;

    if-eqz v2, :cond_7

    .line 139
    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->preset:Lcom/squareup/protos/client/giftcards/Preset;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/giftcards/Preset;->preset_values:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 140
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/Money;

    .line 141
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v5}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMarketButtonFactory$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v7, "view.context"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/marketfont/MarketButton;

    .line 142
    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$presetAmountContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v6

    const/4 v7, -0x1

    if-nez v6, :cond_3

    .line 143
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v6, v0, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    check-cast v6, Landroid/view/ViewGroup$LayoutParams;

    goto :goto_4

    .line 145
    :cond_3
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast v6, Landroid/view/ViewGroup$LayoutParams;

    .line 142
    :goto_4
    invoke-virtual {v5, v6}, Lcom/squareup/marketfont/MarketButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$presetAmountContainer:Landroid/widget/LinearLayout;

    move-object v7, v5

    check-cast v7, Landroid/view/View;

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v6, 0x1

    .line 147
    invoke-virtual {v5, v6}, Lcom/squareup/marketfont/MarketButton;->setMaxLines(I)V

    .line 149
    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v6}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getShortMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v6

    invoke-interface {v6, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 153
    invoke-virtual {v5}, Lcom/squareup/marketfont/MarketButton;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/4 v7, 0x6

    if-le v6, v7, :cond_4

    const v6, 0x3f666666    # 0.9f

    .line 154
    invoke-virtual {v5, v6}, Lcom/squareup/marketfont/MarketButton;->setTextScaleX(F)V

    .line 158
    :cond_4
    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v6, v6, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    new-instance v7, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;

    invoke-direct {v7, p0, v5, v4, v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;Lcom/squareup/marketfont/MarketButton;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    invoke-static {v6, v7}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto/16 :goto_3

    .line 164
    :cond_5
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->preset:Lcom/squareup/protos/client/giftcards/Preset;

    if-eqz p1, :cond_6

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/Preset;->allow_custom_value:Ljava/lang/Boolean;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_5

    :cond_6
    const/4 p1, 0x0

    :goto_5
    if-eqz p1, :cond_7

    .line 166
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMoneyLocaleHelper$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customAmountField:Lcom/squareup/widgets/OnScreenRectangleEditText;

    check-cast v2, Lcom/squareup/text/HasSelectableText;

    const/4 v4, 0x2

    invoke-static {p1, v2, v3, v4, v3}, Lcom/squareup/money/MoneyLocaleHelper;->configure$default(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;ILjava/lang/Object;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    .line 168
    new-instance v2, Lcom/squareup/money/MaxMoneyScrubber;

    .line 169
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v3, v3, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v3}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMoneyLocaleHelper$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v3

    check-cast v3, Lcom/squareup/money/MoneyExtractor;

    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v4, v4, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v4}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v4

    .line 170
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v5}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMaxAmountCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J

    move-result-wide v5

    iget-object v7, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v7, v7, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v7}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getCurrencyCode$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 168
    invoke-direct {v2, v3, v4, v5}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    check-cast v2, Lcom/squareup/text/Scrubber;

    .line 167
    invoke-virtual {p1, v2}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 174
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customAmountField:Lcom/squareup/widgets/OnScreenRectangleEditText;

    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedShortText(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object p1

    .line 175
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$customAmount$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {p1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 176
    invoke-virtual {p1}, Lrx/Observable;->publish()Lrx/observables/ConnectableObservable;

    move-result-object p1

    .line 179
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;Lrx/observables/ConnectableObservable;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {v2, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 187
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    invoke-direct {v3, p0, p1, v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;Lrx/observables/ConnectableObservable;Lcom/squareup/protos/client/giftcards/GiftCard;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {v2, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 211
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v1, v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$4;

    invoke-direct {v2, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$4;-><init>(Lrx/observables/ConnectableObservable;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 212
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 216
    :cond_7
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$contents:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    .line 217
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 216
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_6

    .line 220
    :cond_8
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$invalidCardView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    :goto_6
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)V

    return-void
.end method
