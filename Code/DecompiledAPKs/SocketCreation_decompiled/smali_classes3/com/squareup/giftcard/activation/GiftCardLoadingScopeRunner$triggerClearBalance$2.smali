.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->triggerClearBalance()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "+",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 96
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;->call(Lkotlin/Pair;)V

    return-void
.end method

.method public final call(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "+",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ">;)V"
        }
    .end annotation

    .line 550
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$triggerClearBalance$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getOnClearBalance$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
