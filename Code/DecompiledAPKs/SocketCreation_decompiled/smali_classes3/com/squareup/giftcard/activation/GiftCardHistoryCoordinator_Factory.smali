.class public final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;
.super Ljava/lang/Object;
.source "GiftCardHistoryCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;)",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/ui/ErrorsBarPresenter;)Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
            "Lcom/squareup/util/Res;",
            "Ljava/util/Locale;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ")",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;"
        }
    .end annotation

    .line 61
    new-instance v7, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/ui/ErrorsBarPresenter;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-static/range {v1 .. v6}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->newInstance(Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/ui/ErrorsBarPresenter;)Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator_Factory;->get()Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    move-result-object v0

    return-object v0
.end method
