.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/common/Money;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/common/Money;)V
    .locals 5

    if-eqz p1, :cond_1

    .line 197
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getTransaction$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 198
    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v2}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getCartMaxCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 199
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    .line 200
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    .line 202
    sget v1, Lcom/squareup/configure/item/R$string;->gift_card_purchase_limit_exceeded_message:I

    .line 200
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 199
    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;

    iget-object v1, v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3;->$giftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    const-string v2, "giftCard"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1, v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$addGiftCardToTransaction(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V

    :goto_0
    return-void

    .line 195
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "amount should not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$3$2;->call(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
