.class final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;
.super Ljava/lang/Object;
.source "GiftCardHistoryCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "historyResponse",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;)V
    .locals 8

    if-nez p1, :cond_0

    .line 85
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/giftcard/activation/R$string;->gift_card_history_load_failure:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 89
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$balanceValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v1, v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    invoke-static {v1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;->current_balance:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;->current_balance:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$clearBalanceButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 98
    sget v2, Lcom/squareup/giftcard/activation/R$layout;->gift_card_history_header_row:I

    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v3, v3, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    check-cast v3, Landroid/view/ViewGroup;

    .line 97
    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type android.widget.LinearLayout"

    if-eqz v0, :cond_5

    check-cast v0, Landroid/widget/LinearLayout;

    .line 100
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v3, v3, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 102
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;->history_events:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/giftcards/HistoryEvent;

    .line 103
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v3, v3, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 104
    sget v4, Lcom/squareup/giftcard/activation/R$layout;->gift_card_history_row:I

    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v3, v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 106
    check-cast v3, Landroid/view/View;

    sget v4, Lcom/squareup/giftcard/activation/R$id;->gc_history_row_amount:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 107
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    iget-object v6, v0, Lcom/squareup/protos/client/giftcards/HistoryEvent;->amount:Lcom/squareup/protos/common/Money;

    const-string v7, "event.amount"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v6}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getMoneyString(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    sget v4, Lcom/squareup/giftcard/activation/R$id;->gc_history_row_description:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 109
    iget-object v5, v0, Lcom/squareup/protos/client/giftcards/HistoryEvent;->description:Ljava/lang/String;

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    sget v4, Lcom/squareup/giftcard/activation/R$id;->gc_history_row_date:I

    invoke-static {v3, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 111
    iget-object v5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v5, v5, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    iget-object v0, v0, Lcom/squareup/protos/client/giftcards/HistoryEvent;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const-string v6, "event.created_at"

    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getDateString(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 104
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 114
    :cond_4
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    .line 115
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 114
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 117
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$screenContent:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 97
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;->call(Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;)V

    return-void
.end method
