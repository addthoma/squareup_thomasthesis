.class public final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardItemizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardItemizer;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeBusWhenVisibleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardItemizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->swipeBusWhenVisibleProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardServiceProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardItemizerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p14, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardItemizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;)",
            "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;"
        }
    .end annotation

    .line 98
    new-instance v15, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/Transaction;Lcom/squareup/giftcard/activation/GiftCardItemizer;Lcom/squareup/ui/main/TopScreenChecker;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;
    .locals 16

    .line 107
    new-instance v15, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/Transaction;Lcom/squareup/giftcard/activation/GiftCardItemizer;Lcom/squareup/ui/main/TopScreenChecker;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;
    .locals 15

    .line 85
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->swipeBusWhenVisibleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/giftcard/GiftCardServiceHelper;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/giftcard/GiftCards;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->giftCardItemizerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/giftcard/activation/GiftCardItemizer;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/ui/main/TopScreenChecker;

    invoke-static/range {v1 .. v14}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/payment/Transaction;Lcom/squareup/giftcard/activation/GiftCardItemizer;Lcom/squareup/ui/main/TopScreenChecker;)Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner_Factory;->get()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    move-result-object v0

    return-object v0
.end method
