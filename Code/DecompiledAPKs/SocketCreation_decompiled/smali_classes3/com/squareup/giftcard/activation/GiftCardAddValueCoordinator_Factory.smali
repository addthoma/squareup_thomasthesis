.class public final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final controllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->controllerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;"
        }
    .end annotation

    .line 80
    new-instance v11, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")",
            "Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;"
        }
    .end annotation

    .line 88
    new-instance v11, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;
    .locals 11

    .line 68
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->controllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->accountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static/range {v1 .. v10}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->newInstance(Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator_Factory;->get()Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    move-result-object v0

    return-object v0
.end method
