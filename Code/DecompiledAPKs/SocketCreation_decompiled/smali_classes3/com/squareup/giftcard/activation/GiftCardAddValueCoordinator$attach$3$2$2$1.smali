.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2$1;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "money",
        "Lcom/squareup/protos/common/Money;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/protos/common/Money;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2$1;->call(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    if-eqz p1, :cond_0

    .line 181
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getMinAmountCents$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
