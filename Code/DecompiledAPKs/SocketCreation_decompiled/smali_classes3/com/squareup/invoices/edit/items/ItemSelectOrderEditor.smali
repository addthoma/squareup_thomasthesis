.class public interface abstract Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;
.super Ljava/lang/Object;
.source "ItemSelectOrderEditor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u000cH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "getOrder",
        "()Lcom/squareup/payment/Order;",
        "addDiscountToCart",
        "",
        "discount",
        "Lcom/squareup/checkout/Discount;",
        "addItemToCart",
        "cartItem",
        "Lcom/squareup/checkout/CartItem;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addDiscountToCart(Lcom/squareup/checkout/Discount;)V
.end method

.method public abstract addItemToCart(Lcom/squareup/checkout/CartItem;)V
.end method

.method public abstract getOrder()Lcom/squareup/payment/Order;
.end method
