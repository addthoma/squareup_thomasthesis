.class final Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;
.super Ljava/lang/Object;
.source "EditInvoiceGateway.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToFirstScreen(Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "showV2",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

.field final synthetic $needsInvoiceDefaults:Z

.field final synthetic $type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    iput-object p3, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$context:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    iput-boolean p4, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$needsInvoiceDefaults:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Boolean;)V
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    invoke-static {v0}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->access$getFlow$p(Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lflow/Flow;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->this$0:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    .line 113
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$type:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$context:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    const-string v4, "showV2"

    .line 114
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    .line 115
    iget-boolean v4, p0, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->$needsInvoiceDefaults:Z

    .line 112
    invoke-static {v1, v2, v3, p1, v4}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->access$firstScreen(Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/invoices/edit/EditInvoiceScope$Type;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    .line 111
    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway$goToFirstScreen$2;->accept(Ljava/lang/Boolean;)V

    return-void
.end method
