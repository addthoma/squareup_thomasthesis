.class public final Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;
.super Ljava/lang/Object;
.source "EditInvoiceContext.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0007J\u0008\u0010\n\u001a\u00020\u000bH\u0007J\u0008\u0010\u000c\u001a\u00020\rH\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;",
        "",
        "()V",
        "duplicateSingleInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "invoiceDisplayDetails",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "editExistingInvoiceContext",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "newRecurringSeriesContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;",
        "newSingleInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 158
    invoke-direct {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$Factory;-><init>()V

    return-void
.end method


# virtual methods
.method public final duplicateSingleInvoiceContext(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "invoiceDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    return-object v0
.end method

.method public final editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "displayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 170
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 174
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 175
    :cond_3
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    :goto_0
    return-object v0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final newRecurringSeriesContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    return-object v0
.end method

.method public final newSingleInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 160
    sget-object v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    return-object v0
.end method
