.class public Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog$Factory;
.super Ljava/lang/Object;
.source "DiscardInvoiceChangesDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 36
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 37
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->discardInvoice()V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 40
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 24
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 25
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 26
    invoke-interface {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;->editInvoiceScopeRunner()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isNewInvoice()Z

    move-result v1

    .line 29
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz v1, :cond_0

    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_create_discard:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_edit_discard:I

    .line 30
    :goto_0
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    if-eqz v1, :cond_1

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_create_discard_message:I

    goto :goto_1

    :cond_1
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_edit_discard_message:I

    .line 32
    :goto_1
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->confirmation_popup_discard:I

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$Factory$8uBBogbbt6yeriHuboyfqwQIWk0;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$Factory$8uBBogbbt6yeriHuboyfqwQIWk0;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 34
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    sget-object v1, Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$Factory$u5yTQGHHBU0t9IgEmmOkKGSC7Rs;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$Factory$u5yTQGHHBU0t9IgEmmOkKGSC7Rs;

    .line 39
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 41
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 42
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
