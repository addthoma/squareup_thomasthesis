.class public Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;
.super Ljava/lang/Object;
.source "RealEditInvoiceInTenderRunner.java"

# interfaces
.implements Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final mainScheduler:Lrx/Scheduler;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/PermissionGatekeeper;Lrx/Scheduler;Lcom/squareup/invoices/edit/EditInvoiceGateway;)V
    .locals 0
    .param p7    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    .line 42
    iput-object p2, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->features:Lcom/squareup/settings/server/Features;

    .line 43
    iput-object p3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 44
    iput-object p4, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->flow:Lflow/Flow;

    .line 45
    iput-object p5, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 46
    iput-object p6, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 47
    iput-object p7, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->mainScheduler:Lrx/Scheduler;

    .line 48
    iput-object p8, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lrx/Scheduler;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->mainScheduler:Lrx/Scheduler;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lcom/squareup/invoices/edit/EditInvoiceGateway;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    return-object p0
.end method


# virtual methods
.method public start()V
    .locals 5

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    .line 58
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasDiscounts()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 59
    :goto_0
    iget-object v3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->INVOICES_WITH_MODIFIERS:Lcom/squareup/settings/server/Features$Feature;

    .line 60
    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->hasAtLeastOneModifier()Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 61
    :goto_1
    iget-object v2, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasGiftCardItem()Z

    move-result v2

    if-eqz v0, :cond_3

    .line 66
    iget-object v3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_DISCOUNT_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    :cond_3
    if-eqz v1, :cond_4

    .line 69
    iget-object v3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_MODIFIER_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    :cond_4
    if-eqz v2, :cond_5

    .line 72
    iget-object v3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterErrorName;->INVOICE_GIFT_CARD_UNSUPPORTED:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    :cond_5
    if-eqz v0, :cond_6

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_unsupported_title:I

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_unsupported_message_discounts:I

    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    :cond_6
    if-eqz v1, :cond_7

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_unsupported_title:I

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_unsupported_message_modifiers:I

    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    :cond_7
    if-eqz v2, :cond_8

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v2, Lcom/squareup/widgets/warning/WarningIds;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_unsupported_title:I

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_gift_card_unspported_message:I

    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 98
    :cond_8
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->INVOICES_APPLET:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;-><init>(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
