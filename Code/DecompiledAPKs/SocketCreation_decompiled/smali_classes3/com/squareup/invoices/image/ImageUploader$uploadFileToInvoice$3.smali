.class final Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;
.super Ljava/lang/Object;
.source "ImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;->$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/image/FileUploadResult;)V
    .locals 1

    .line 138
    iget-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    invoke-static {p1}, Lcom/squareup/invoices/image/ImageUploader;->access$getInvoiceFileHelper$p(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/invoices/image/InvoiceFileHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;->$file:Ljava/io/File;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->delete(Ljava/io/File;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$3;->accept(Lcom/squareup/invoices/image/FileUploadResult;)V

    return-void
.end method
