.class final Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/InvoiceFileHelper;->writeResponseBodyToFile(Lokhttp3/ResponseBody;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Ljava/io/File;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $responseBody:Lokhttp3/ResponseBody;

.field final synthetic this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Lokhttp3/ResponseBody;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;->$responseBody:Lokhttp3/ResponseBody;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/io/File;
    .locals 4

    .line 117
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;->this$0:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v0}, Lcom/squareup/invoices/image/InvoiceFileHelper;->access$getTempPhotoDir$p(Lcom/squareup/invoices/image/InvoiceFileHelper;)Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "TEMP"

    invoke-static {v2, v1, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    const-string v2, "tempFile"

    .line 118
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v1}, Lokio/Okio;->sink$default(Ljava/io/File;ZILjava/lang/Object;)Lokio/Sink;

    move-result-object v1

    .line 119
    invoke-static {v1}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    move-result-object v1

    .line 120
    iget-object v2, p0, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;->$responseBody:Lokhttp3/ResponseBody;

    invoke-virtual {v2}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    move-result-object v2

    check-cast v2, Lokio/Source;

    invoke-interface {v1, v2}, Lokio/BufferedSink;->writeAll(Lokio/Source;)J

    .line 121
    invoke-interface {v1}, Lokio/BufferedSink;->close()V

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/invoices/image/InvoiceFileHelper$writeResponseBodyToFile$1;->call()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
