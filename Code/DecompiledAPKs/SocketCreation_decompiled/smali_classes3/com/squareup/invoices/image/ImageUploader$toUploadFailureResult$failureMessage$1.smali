.class final Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ImageUploader.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->toUploadFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileUploadResult$Failure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "it",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/server/invoices/InvoiceFileUploadResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    new-instance p1, Lcom/squareup/receiving/FailureMessage$Parts;

    .line 169
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    invoke-static {v0}, Lcom/squareup/invoices/image/ImageUploader;->access$getRes$p(Lcom/squareup/invoices/image/ImageUploader;)Lcom/squareup/util/Res;

    move-result-object v0

    .line 170
    sget v1, Lcom/squareup/features/invoices/R$string;->upload_failed_default:I

    .line 169
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 168
    invoke-direct {p1, v0, v1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/server/invoices/InvoiceFileUploadResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$toUploadFailureResult$failureMessage$1;->invoke(Lcom/squareup/server/invoices/InvoiceFileUploadResponse;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
