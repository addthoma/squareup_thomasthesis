.class public final Lcom/squareup/invoices/InvoicesRxPreferencesModule;
.super Ljava/lang/Object;
.source "InvoicesRxPreferencesModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicesRxPreferencesModule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicesRxPreferencesModule.kt\ncom/squareup/invoices/InvoicesRxPreferencesModule\n+ 2 ProtoPreferenceConverter.kt\ncom/squareup/settings/ProtoPreferenceConverterKt\n*L\n1#1,49:1\n56#2:50\n60#2:51\n60#2:52\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicesRxPreferencesModule.kt\ncom/squareup/invoices/InvoicesRxPreferencesModule\n*L\n25#1:50\n35#1:51\n45#1:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0007J&\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000c0\u00050\u00042\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0007J(\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u00042\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/InvoicesRxPreferencesModule;",
        "",
        "()V",
        "provideInvoiceDefaultsList",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "res",
        "Lcom/squareup/util/Res;",
        "provideInvoiceMetrics",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "provideInvoiceUnitMetadata",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/InvoicesRxPreferencesModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/invoices/InvoicesRxPreferencesModule;

    invoke-direct {v0}, Lcom/squareup/invoices/InvoicesRxPreferencesModule;-><init>()V

    sput-object v0, Lcom/squareup/invoices/InvoicesRxPreferencesModule;->INSTANCE:Lcom/squareup/invoices/InvoicesRxPreferencesModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideInvoiceDefaultsList(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/util/Res;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultInvoiceDefaults(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 52
    new-instance v0, Lcom/squareup/settings/ProtoListPreferenceConverter;

    const-class v1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    invoke-direct {v0, v1}, Lcom/squareup/settings/ProtoListPreferenceConverter;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lcom/f2prateek/rx/preferences2/Preference$Converter;

    const-string v1, "invoice-unit-defaults-list-proto"

    .line 42
    invoke-virtual {p0, v1, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "preferences.getObject(\n \u2026PreferenceAdapter()\n    )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final provideInvoiceMetrics(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultInvoiceMetrics(Lcom/squareup/protos/common/CurrencyCode;)Ljava/util/List;

    move-result-object p1

    .line 51
    new-instance v0, Lcom/squareup/settings/ProtoListPreferenceConverter;

    const-class v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    invoke-direct {v0, v1}, Lcom/squareup/settings/ProtoListPreferenceConverter;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lcom/f2prateek/rx/preferences2/Preference$Converter;

    const-string v1, "invoice-metrics-proto"

    .line 32
    invoke-virtual {p0, v1, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "preferences.getObject(\n \u2026PreferenceAdapter()\n    )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final provideInvoiceUnitMetadata(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p1, p2}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultUnitMetadataDisplayDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    .line 50
    new-instance p2, Lcom/squareup/settings/ProtoPreferenceConverter;

    const-class v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-direct {p2, v0}, Lcom/squareup/settings/ProtoPreferenceConverter;-><init>(Ljava/lang/Class;)V

    check-cast p2, Lcom/f2prateek/rx/preferences2/Preference$Converter;

    const-string v0, "invoice-unit-metadata-display-details-proto"

    .line 22
    invoke-virtual {p0, v0, p1, p2}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "preferences.getObject(\n \u2026PreferenceAdapter()\n    )"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
