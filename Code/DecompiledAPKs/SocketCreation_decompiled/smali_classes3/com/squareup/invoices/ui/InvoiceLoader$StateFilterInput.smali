.class public Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
.super Ljava/lang/Object;
.source "InvoiceLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StateFilterInput"
.end annotation


# instance fields
.field public final invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

.field public final parentSeriesToken:Ljava/lang/String;

.field public final seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/invoice/StateFilter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Ljava/lang/String;)V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 81
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 82
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->parentSeriesToken:Ljava/lang/String;

    return-void
.end method

.method public static createParentSeriesTokenInput(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p0}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;-><init>(Lcom/squareup/protos/client/invoice/StateFilter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createRecurringListStateFilterInput(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, v1}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;-><init>(Lcom/squareup/protos/client/invoice/StateFilter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createSingleListStateFilterInput(Lcom/squareup/protos/client/invoice/StateFilter;)Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1}, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;-><init>(Lcom/squareup/protos/client/invoice/StateFilter;Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 91
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 93
    :cond_1
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;

    .line 94
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    iget-object v3, p1, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    iget-object v3, p1, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    .line 95
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->parentSeriesToken:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->parentSeriesToken:Ljava/lang/String;

    .line 96
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 100
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->invoiceStateFilter:Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->parentSeriesToken:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isRecurring()Z
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceLoader$StateFilterInput;->seriesStateFilter:Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
