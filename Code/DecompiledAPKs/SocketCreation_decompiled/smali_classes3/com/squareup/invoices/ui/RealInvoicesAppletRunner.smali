.class public Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;
.super Ljava/lang/Object;
.source "RealInvoicesAppletRunner.java"

# interfaces
.implements Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;


# instance fields
.field private final applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private invoiceConfirmationCanceled:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Ldagger/Lazy;Lcom/squareup/payment/Transaction;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceConfirmationCanceled:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    .line 31
    iput-object p2, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->flow:Ldagger/Lazy;

    .line 32
    iput-object p3, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 33
    iput-object p4, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    return-void
.end method


# virtual methods
.method public cancelInvoiceConfirmation()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceConfirmationCanceled:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getInstanceOfHistoryScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 50
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    return-object v0
.end method

.method public historyBehindInvoiceTenderScreen(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 4

    .line 77
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Histories;->popUntil(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lflow/History$Builder;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->createInvoiceHistoryToSingleInvoiceDetail(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    return-object p1

    .line 83
    :cond_0
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public invoicePaymentCanceled(Ljava/lang/String;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;->DELAY:Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->setInfo(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {p1}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->returnAfterInvoicePaymentCanceled()V

    return-void
.end method

.method public invoicePaymentSucceeded()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {v0}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->returnAfterInvoicePayment()V

    return-void
.end method

.method public isInvoiceHistoryScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 68
    const-class v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public isTakingInvoicePayment()Z
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    return v0
.end method

.method public onInvoiceConfirmationCanceled()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceConfirmationCanceled:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public start()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {v0}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->activate()V

    return-void
.end method

.method public viewFullInvoiceDetail(Ljava/lang/String;)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;->NO_DELAY:Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->setInfo(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->applet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {p1}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->showFullInvoiceDetailFromReadOnly()V

    return-void
.end method
