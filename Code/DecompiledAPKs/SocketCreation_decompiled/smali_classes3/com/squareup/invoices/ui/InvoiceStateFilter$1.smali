.class synthetic Lcom/squareup/invoices/ui/InvoiceStateFilter$1;
.super Ljava/lang/Object;
.source "InvoiceStateFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceStateFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

.field static final synthetic $SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 104
    invoke-static {}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->values()[Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v2, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v3}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v4, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v4}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v5, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v5}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    const/4 v4, 0x5

    :try_start_4
    sget-object v5, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v6, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v6}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v6

    aput v4, v5, v6
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    const/4 v5, 0x6

    :try_start_5
    sget-object v6, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v7, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v7}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v7

    aput v5, v6, v7
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    const/4 v6, 0x7

    :try_start_6
    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v8}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v8

    aput v6, v7, v8
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v8}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v8

    const/16 v9, 0x8

    aput v9, v7, v8
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v8}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v8

    const/16 v9, 0x9

    aput v9, v7, v8
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    :try_start_9
    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    invoke-virtual {v8}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result v8

    const/16 v9, 0xa

    aput v9, v7, v8
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    .line 45
    :catch_9
    invoke-static {}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->values()[Lcom/squareup/invoices/ui/InvoiceStateFilter;

    move-result-object v7

    array-length v7, v7

    new-array v7, v7, [I

    sput-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    :try_start_a
    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v8, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v8}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v8

    aput v0, v7, v8
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v7, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v7}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v7

    aput v1, v0, v7
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->PAID:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->SCHEDULED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aput v4, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :catch_e
    :try_start_f
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aput v5, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceStateFilter:[I

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->UNKNOWN:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ordinal()I

    move-result v1

    aput v6, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    return-void
.end method
