.class public abstract Lcom/squareup/invoices/ui/recordpayment/InRecordPaymentScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InRecordPaymentScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/InRecordPaymentScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "()V",
        "getParentKey",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public final getParentKey()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 5
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/recordpayment/InRecordPaymentScope;->getParentKey()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;

    move-result-object v0

    return-object v0
.end method
