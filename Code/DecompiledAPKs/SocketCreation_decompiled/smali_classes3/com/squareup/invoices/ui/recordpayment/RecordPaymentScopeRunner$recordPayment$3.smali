.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;
.super Ljava/lang/Object;
.source "RecordPaymentScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->recordPayment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $recordInfo:Lcom/squareup/invoices/RecordPaymentInfo;

.field final synthetic this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;Lcom/squareup/invoices/RecordPaymentInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->$recordInfo:Lcom/squareup/invoices/RecordPaymentInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
            ">;)V"
        }
    .end annotation

    .line 92
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getInvoicesAppletScopeRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setCurrentDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getConfirmationScreenData$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getRecordPaymentConfirmationScreenDataFactory$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->$recordInfo:Lcom/squareup/invoices/RecordPaymentInfo;

    invoke-virtual {v1}, Lcom/squareup/invoices/RecordPaymentInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->$recordInfo:Lcom/squareup/invoices/RecordPaymentInfo;

    invoke-virtual {v2}, Lcom/squareup/invoices/RecordPaymentInfo;->getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v2

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->createSuccessful(Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;

    move-result-object v0

    .line 94
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getConfirmationScreenData$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {v1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getRecordPaymentConfirmationScreenDataFactory$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;

    move-result-object v1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->createFailed(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;

    move-result-object p1

    .line 101
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 106
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;

    invoke-static {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;->access$getFlow$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;)Lflow/Flow;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner$recordPayment$3;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
