.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaymentAmountWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,289:1\n310#2,7:290\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaymentAmountWorkflow.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt\n*L\n284#1,7:290\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u0004*\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0002*\"\u0010\u0007\"\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "getOtherAmountAndIndex",
        "Lkotlin/Pair;",
        "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;",
        "",
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountAndIndex;",
        "",
        "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
        "OtherAmountAndIndex",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getOtherAmountAndIndex(Ljava/util/List;)Lkotlin/Pair;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowKt;->getOtherAmountAndIndex(Ljava/util/List;)Lkotlin/Pair;

    move-result-object p0

    return-object p0
.end method

.method private static final getOtherAmountAndIndex(Ljava/util/List;)Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;",
            ">;)",
            "Lkotlin/Pair<",
            "Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 291
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 292
    check-cast v2, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType;

    .line 284
    instance-of v2, v2, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    .line 285
    :goto_1
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_2

    check-cast p0, Lcom/squareup/invoices/ui/payinvoice/PaymentAmountType$OtherAmount;

    .line 287
    new-instance v0, Lkotlin/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 285
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.ui.payinvoice.PaymentAmountType.OtherAmount"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
