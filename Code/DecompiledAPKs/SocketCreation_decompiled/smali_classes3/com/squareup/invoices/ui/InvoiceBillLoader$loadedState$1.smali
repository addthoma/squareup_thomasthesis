.class final Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;
.super Ljava/lang/Object;
.source "InvoiceBillLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadedState(Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
        "kotlin.jvm.PlatformType",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $defaultBillToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceBillLoader;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;->$defaultBillToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    instance-of v0, p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Empty;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;->this$0:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;->$defaultBillToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadFromToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 116
    :cond_0
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader$loadedState$1;->apply(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
