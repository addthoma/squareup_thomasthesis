.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;
.super Ljava/lang/Object;
.source "RecordPaymentScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u001e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0004R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/text/Formatter;)V",
        "create",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "info",
        "Lcom/squareup/invoices/RecordPaymentInfo;",
        "isBusy",
        "",
        "maxMoney",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/invoices/RecordPaymentInfo;ZLcom/squareup/protos/common/Money;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;
    .locals 8

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxMoney"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;

    .line 50
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v3

    .line 52
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getSendReceipt()Z

    move-result v4

    .line 55
    invoke-virtual {p1}, Lcom/squareup/invoices/RecordPaymentInfo;->getNote()Ljava/lang/String;

    move-result-object v7

    move-object v1, v0

    move v5, p2

    move-object v6, p3

    .line 49
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;ZZLcom/squareup/protos/common/Money;Ljava/lang/String;)V

    return-object v0
.end method
