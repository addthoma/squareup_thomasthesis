.class public final Lcom/squareup/invoices/ui/InvoicePaymentTypeView;
.super Landroid/widget/LinearLayout;
.source "InvoicePaymentTypeView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaymentTypeView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaymentTypeView.kt\ncom/squareup/invoices/ui/InvoicePaymentTypeView\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,118:1\n1103#2,7:119\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaymentTypeView.kt\ncom/squareup/invoices/ui/InvoicePaymentTypeView\n*L\n80#1,7:119\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0018\u0000  2\u00020\u0001:\u0001 B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0014\u0010\u0011\u001a\u00020\u00102\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0013J\u000e\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u0016J\u0010\u0010\u0017\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u001a\u001a\u00020\u00102\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0016J\u000e\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0016J\u000e\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicePaymentTypeView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "chargeButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "helperText",
        "Lcom/squareup/widgets/MessageView;",
        "paymentTypeImage",
        "Landroid/widget/ImageView;",
        "subtitleMessageView",
        "titleMessageView",
        "bindViews",
        "",
        "setButtonOnClickListener",
        "action",
        "Lkotlin/Function0;",
        "setButtonText",
        "buttonText",
        "",
        "setDrawable",
        "drawableRes",
        "",
        "setHelperTextOrGone",
        "text",
        "setSubtitleText",
        "subtitle",
        "setTitleText",
        "title",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Factory:Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;


# instance fields
.field private chargeButton:Lcom/squareup/marketfont/MarketButton;

.field private helperText:Lcom/squareup/widgets/MessageView;

.field private paymentTypeImage:Landroid/widget/ImageView;

.field private subtitleMessageView:Lcom/squareup/widgets/MessageView;

.field private titleMessageView:Lcom/squareup/widgets/MessageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->Factory:Lcom/squareup/invoices/ui/InvoicePaymentTypeView$Factory;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget p2, Lcom/squareup/features/invoices/R$layout;->invoice_payment_type_view:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->bindViews()V

    const/4 p1, 0x1

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setOrientation(I)V

    const/16 p1, 0x11

    .line 48
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->setGravity(I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 35
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 88
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_title:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.payment_type_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->titleMessageView:Lcom/squareup/widgets/MessageView;

    .line 89
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_subtitle:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.payment_type_subtitle)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->subtitleMessageView:Lcom/squareup/widgets/MessageView;

    .line 90
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_button:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.payment_type_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->chargeButton:Lcom/squareup/marketfont/MarketButton;

    .line 91
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_image:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.payment_type_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->paymentTypeImage:Landroid/widget/ImageView;

    .line 92
    sget v0, Lcom/squareup/features/invoices/R$id;->payment_type_helper_text:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.payment_type_helper_text)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->helperText:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public final setButtonOnClickListener(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->chargeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "chargeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 119
    new-instance v1, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$setButtonOnClickListener$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/InvoicePaymentTypeView$setButtonOnClickListener$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    const-string v0, "buttonText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->chargeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "chargeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setDrawable(I)V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->paymentTypeImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "paymentTypeImage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public final setHelperTextOrGone(Ljava/lang/CharSequence;)V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->helperText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "helperText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 2

    const-string v0, "subtitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->subtitleMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "subtitleMessageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTitleText(Ljava/lang/CharSequence;)V
    .locals 2

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicePaymentTypeView;->titleMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "titleMessageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
