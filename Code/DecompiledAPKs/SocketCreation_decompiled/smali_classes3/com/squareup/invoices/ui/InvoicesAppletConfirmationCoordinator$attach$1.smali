.class final Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;
.super Ljava/lang/Object;
.source "InvoicesAppletConfirmationCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;)V
    .locals 4

    .line 27
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->access$getGlyphMessageView$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->access$getGlyphMessageView$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->access$getGlyphMessageView$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/marin/widgets/MarinGlyphMessage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;->getSubtitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;->getActionBarHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;

    invoke-static {v2}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->access$getRunner$p(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;->getSuccess()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;->access$updateActionBar(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator;Ljava/lang/String;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;Z)V

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1$1;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationCoordinator$attach$1;->accept(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;)V

    return-void
.end method
