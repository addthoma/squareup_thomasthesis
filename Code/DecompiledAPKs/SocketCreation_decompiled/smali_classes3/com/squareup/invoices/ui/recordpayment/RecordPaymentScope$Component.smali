.class public interface abstract Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;
.super Ljava/lang/Object;
.source "RecordPaymentScope.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$AddPaymentModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScope$Component;",
        "",
        "addPaymentScopeRunner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;",
        "recordPaymentConfirmationCoordinator",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;",
        "recordPaymentCoordinator",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;",
        "recordPaymentMethodCoordinator",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addPaymentScopeRunner()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScopeRunner;
.end method

.method public abstract recordPaymentConfirmationCoordinator()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationCoordinator;
.end method

.method public abstract recordPaymentCoordinator()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;
.end method

.method public abstract recordPaymentMethodCoordinator()Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;
.end method
