.class public final synthetic Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/invoices/InvoiceDisplayState;->values()[Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x4

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x5

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x6

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/4 v4, 0x7

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/16 v4, 0x8

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/16 v4, 0x9

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/16 v4, 0xa

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->DRAFT:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/16 v4, 0xb

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceDisplayState;->ordinal()I

    move-result v1

    const/16 v4, 0xc

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->values()[Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceButtonDataFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ACTIVE:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
