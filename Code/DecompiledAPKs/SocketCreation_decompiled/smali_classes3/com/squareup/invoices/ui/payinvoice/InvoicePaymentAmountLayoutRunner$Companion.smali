.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005JQ\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0012\u0010\u0014\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000bj\u0002`\u00152\u0018\u0010\u0016\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0018j\u0002`\u00190\u00172\u0006\u0010\u001a\u001a\u00020\u001bH\u0096\u0001R\u0012\u0010\u0006\u001a\u00020\u0007X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u000bX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;",
        "",
        "Lcom/squareup/workflow/V2LayoutBinding;",
        "()V",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "inflate",
        "Landroid/view/View;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;


# direct methods
.method private constructor <init>()V
    .locals 8

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 72
    const-class v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    sget v2, Lcom/squareup/features/invoices/R$layout;->pay_invoice_keypad_layout:I

    .line 73
    sget-object v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion$1;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion$1;

    move-object v5, v3

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    .line 71
    invoke-static/range {v0 .. v7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method

.method public inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "contextForNewView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
