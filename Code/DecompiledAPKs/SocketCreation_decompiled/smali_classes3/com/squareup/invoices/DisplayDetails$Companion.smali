.class public final Lcom/squareup/invoices/DisplayDetails$Companion;
.super Ljava/lang/Object;
.source "DisplayDetails.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/DisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayDetails.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayDetails.kt\ncom/squareup/invoices/DisplayDetails$Companion\n+ 2 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,109:1\n61#2:110\n61#2:111\n*E\n*S KotlinDebug\n*F\n+ 1 DisplayDetails.kt\ncom/squareup/invoices/DisplayDetails$Companion\n*L\n100#1:110\n101#1:111\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0006\u001a\u0004\u0018\u00010\u0005*\u00020\u0007J\u0014\u0010\u0008\u001a\u00020\t*\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0005R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/DisplayDetails$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/invoices/DisplayDetails;",
        "readDisplayDetails",
        "Lokio/BufferedSource;",
        "writeDisplayDetails",
        "",
        "Lokio/BufferedSink;",
        "displayDetails",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Lcom/squareup/invoices/DisplayDetails$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final readDisplayDetails(Lokio/BufferedSource;)Lcom/squareup/invoices/DisplayDetails;
    .locals 3

    const-string v0, "$this$readDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readOptionalUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    .line 100
    const-class v2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 100
    new-instance p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    check-cast p1, Lcom/squareup/invoices/DisplayDetails;

    goto :goto_0

    .line 101
    :cond_2
    const-class v2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 111
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    .line 101
    new-instance p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    check-cast p1, Lcom/squareup/invoices/DisplayDetails;

    :goto_0
    return-object p1

    .line 102
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 103
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " does not support being read from a BufferedSource."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_6
    return-object v1
.end method

.method public final writeDisplayDetails(Lokio/BufferedSink;Lcom/squareup/invoices/DisplayDetails;)V
    .locals 1

    const-string v0, "$this$writeDisplayDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 91
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeOptionalUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 93
    instance-of v0, p2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    goto :goto_1

    .line 94
    :cond_1
    instance-of v0, p2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-virtual {p2}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_2
    :goto_1
    return-void
.end method
