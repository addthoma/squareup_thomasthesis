.class public final enum Lcom/squareup/invoices/PaymentRequestsConfig;
.super Ljava/lang/Enum;
.source "PaymentRequestsConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/PaymentRequestsConfig$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/PaymentRequestsConfig;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u0000 \u00082\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/PaymentRequestsConfig;",
        "",
        "(Ljava/lang/String;I)V",
        "FULL",
        "DEPOSIT_REMAINDER",
        "DEPOSIT_INSTALLMENTS",
        "INSTALLMENTS",
        "INVALID",
        "Companion",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/PaymentRequestsConfig;

.field public static final Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

.field public static final enum DEPOSIT_INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

.field public static final enum DEPOSIT_REMAINDER:Lcom/squareup/invoices/PaymentRequestsConfig;

.field public static final enum FULL:Lcom/squareup/invoices/PaymentRequestsConfig;

.field public static final enum INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

.field public static final enum INVALID:Lcom/squareup/invoices/PaymentRequestsConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/invoices/PaymentRequestsConfig;

    new-instance v1, Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v2, 0x0

    const-string v3, "FULL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsConfig;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->FULL:Lcom/squareup/invoices/PaymentRequestsConfig;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v2, 0x1

    const-string v3, "DEPOSIT_REMAINDER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsConfig;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->DEPOSIT_REMAINDER:Lcom/squareup/invoices/PaymentRequestsConfig;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v2, 0x2

    const-string v3, "DEPOSIT_INSTALLMENTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsConfig;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->DEPOSIT_INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v2, 0x3

    const-string v3, "INSTALLMENTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsConfig;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->INSTALLMENTS:Lcom/squareup/invoices/PaymentRequestsConfig;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v2, 0x4

    const-string v3, "INVALID"

    invoke-direct {v1, v3, v2}, Lcom/squareup/invoices/PaymentRequestsConfig;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/invoices/PaymentRequestsConfig;->INVALID:Lcom/squareup/invoices/PaymentRequestsConfig;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->$VALUES:[Lcom/squareup/invoices/PaymentRequestsConfig;

    new-instance v0, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/PaymentRequestsConfig$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->Companion:Lcom/squareup/invoices/PaymentRequestsConfig$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/PaymentRequestsConfig;
    .locals 1

    const-class v0, Lcom/squareup/invoices/PaymentRequestsConfig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/PaymentRequestsConfig;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/PaymentRequestsConfig;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/PaymentRequestsConfig;->$VALUES:[Lcom/squareup/invoices/PaymentRequestsConfig;

    invoke-virtual {v0}, [Lcom/squareup/invoices/PaymentRequestsConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/PaymentRequestsConfig;

    return-object v0
.end method
