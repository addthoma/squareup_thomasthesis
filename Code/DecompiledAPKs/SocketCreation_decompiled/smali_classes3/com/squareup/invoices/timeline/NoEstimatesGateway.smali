.class public final Lcom/squareup/invoices/timeline/NoEstimatesGateway;
.super Ljava/lang/Object;
.source "EstimatesGateway.kt"

# interfaces
.implements Lcom/squareup/invoices/timeline/EstimatesGateway;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEstimatesGateway.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EstimatesGateway.kt\ncom/squareup/invoices/timeline/NoEstimatesGateway\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,18:1\n151#2,2:19\n*E\n*S KotlinDebug\n*F\n+ 1 EstimatesGateway.kt\ncom/squareup/invoices/timeline/NoEstimatesGateway\n*L\n17#1,2:19\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/timeline/NoEstimatesGateway;",
        "Lcom/squareup/invoices/timeline/EstimatesGateway;",
        "()V",
        "goToEstimateDetail",
        "",
        "token",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/invoices/timeline/EstimatesGateway;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 19
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 20
    const-class v2, Lcom/squareup/invoices/timeline/EstimatesGateway;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/timeline/EstimatesGateway;

    iput-object v0, p0, Lcom/squareup/invoices/timeline/NoEstimatesGateway;->$$delegate_0:Lcom/squareup/invoices/timeline/EstimatesGateway;

    return-void
.end method


# virtual methods
.method public goToEstimateDetail(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/invoices/timeline/NoEstimatesGateway;->$$delegate_0:Lcom/squareup/invoices/timeline/EstimatesGateway;

    invoke-interface {v0, p1}, Lcom/squareup/invoices/timeline/EstimatesGateway;->goToEstimateDetail(Ljava/lang/String;)V

    return-void
.end method
