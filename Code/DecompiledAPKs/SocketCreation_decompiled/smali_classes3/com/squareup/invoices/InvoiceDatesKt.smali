.class public final Lcom/squareup/invoices/InvoiceDatesKt;
.super Ljava/lang/Object;
.source "InvoiceDates.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceDates.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceDates.kt\ncom/squareup/invoices/InvoiceDatesKt\n*L\n1#1,49:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0014\u0010\u0005\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0006\u001a\u00020\u0002*\u00020\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "isBeforeOrEqualToday",
        "",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "isToday",
        "yearMonthDay",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z
    .locals 1

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p0, p1}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/util/YearMonthDaysKt;->isBefore(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z
    .locals 1

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_1

    .line 31
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/util/YearMonthDaysKt;->isSameDayAs(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    const-string v0, "$this$yearMonthDay"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-interface {p0}, Lcom/squareup/time/CurrentTime;->localDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    .line 10
    new-instance v0, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    .line 11
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    .line 12
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->month_of_year(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object v0

    .line 13
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->day_of_month(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    move-result-object p0

    .line 14
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    const-string v0, "YearMonthDay.Builder()\n \u2026OfMonth)\n        .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "with(localDate()) {\n    \u2026nth)\n        .build()\n  }"

    .line 9
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
