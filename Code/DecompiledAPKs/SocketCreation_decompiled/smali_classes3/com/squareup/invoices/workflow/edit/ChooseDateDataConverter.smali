.class public interface abstract Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;
.super Ljava/lang/Object;
.source "ChooseDateDataConverter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0003H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/ChooseDateDataConverter;",
        "",
        "toChooseDateInfo",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "customDateInfo",
        "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
        "toCustomDateInfo",
        "chooseDateInfo",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract toChooseDateInfo(Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
.end method

.method public abstract toCustomDateInfo(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
.end method
