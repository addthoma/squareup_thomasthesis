.class final Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RemindersListCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "Lcom/squareup/noho/NohoRow;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemindersListCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,141:1\n17#2,2:142\n*E\n*S KotlinDebug\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1\n*L\n97#1,2:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "Lcom/squareup/noho/NohoRow;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1",
        "com/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$$special$$inlined$row$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
            "Lcom/squareup/noho/NohoRow;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/noho/NohoRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x1

    .line 98
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setClickable(Z)V

    .line 99
    sget v2, Lcom/squareup/common/strings/R$string;->edit:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(com.sq\u2026on.strings.R.string.edit)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/text/style/CharacterStyle;

    .line 101
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    .line 103
    sget v5, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    .line 102
    invoke-static {p2, v5}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v5

    .line 101
    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    check-cast v4, Landroid/text/style/CharacterStyle;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 106
    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 143
    new-instance v6, Lcom/squareup/fonts/FontSpan;

    invoke-static {v4, v5}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v4

    invoke-direct {v6, p2, v4}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v6, Landroid/text/style/CharacterStyle;

    aput-object v6, v3, v1

    .line 100
    invoke-static {v2, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 97
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 109
    new-instance p2, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/cycler/StandardRowSpec$Creator;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
