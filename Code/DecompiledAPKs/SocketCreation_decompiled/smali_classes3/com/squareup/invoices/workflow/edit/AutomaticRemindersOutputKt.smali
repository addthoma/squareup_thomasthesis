.class public final Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutputKt;
.super Ljava/lang/Object;
.source "AutomaticRemindersOutput.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticRemindersOutput.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticRemindersOutput.kt\ncom/squareup/invoices/workflow/edit/AutomaticRemindersOutputKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,18:1\n1360#2:19\n1429#2,3:20\n1360#2:23\n1429#2,3:24\n*E\n*S KotlinDebug\n*F\n+ 1 AutomaticRemindersOutput.kt\ncom/squareup/invoices/workflow/edit/AutomaticRemindersOutputKt\n*L\n12#1:19\n12#1,3:20\n16#1:23\n16#1,3:24\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003\u001a\u0010\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0001*\u00020\u0003\u00a8\u0006\u0006"
    }
    d2 = {
        "remindersAsConfigProto",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "remindersAsInstanceProto",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "invoices-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final remindersAsConfigProto(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$remindersAsConfigProto"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->getReminders()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 20
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 21
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    if-eqz v1, :cond_0

    .line 12
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.InvoiceReminder.Config"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 22
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final remindersAsInstanceProto(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$remindersAsInstanceProto"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->getReminders()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 24
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 25
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    if-eqz v1, :cond_0

    .line 16
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toProto(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.InvoiceReminder.Instance"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 26
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
