.class public final Lcom/squareup/invoices/workflow/edit/DateOption$Companion;
.super Ljava/lang/Object;
.source "DateOption.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/DateOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDateOption.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DateOption.kt\ncom/squareup/invoices/workflow/edit/DateOption$Companion\n+ 2 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,85:1\n56#2:86\n*E\n*S KotlinDebug\n*F\n+ 1 DateOption.kt\ncom/squareup/invoices/workflow/edit/DateOption$Companion\n*L\n76#1:86\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005J\u0012\u0010\u0006\u001a\u00020\u0007*\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/DateOption$Companion;",
        "",
        "()V",
        "readDateOption",
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "Lokio/BufferedSource;",
        "writeDateOption",
        "",
        "Lokio/BufferedSink;",
        "option",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final readDateOption(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/DateOption;
    .locals 4

    const-string v0, "$this$readDateOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 75
    const-class v2, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-interface {p1}, Lokio/BufferedSource;->readLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;-><init>(JLjava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/DateOption;

    goto :goto_0

    .line 76
    :cond_0
    const-class v2, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 86
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 76
    new-instance v0, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-direct {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/DateOption;

    goto :goto_0

    .line 77
    :cond_1
    const-class p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    new-instance p1, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;

    invoke-direct {p1, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;-><init>(Ljava/lang/String;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/DateOption;

    :goto_0
    return-object v0

    .line 78
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unable to create a DateOption from this BufferedSource."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final writeDateOption(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/DateOption;)V
    .locals 2

    const-string v0, "$this$writeDateOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "option"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    const-class v0, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RelativeDate::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 64
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 66
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;->getRelativeDays()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lokio/BufferedSink;->writeLong(J)Lokio/BufferedSink;

    goto :goto_0

    .line 67
    :cond_0
    instance-of v0, p2, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;->getDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    check-cast p2, Lcom/squareup/wire/Message;

    invoke-static {p1, p2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_1
    :goto_0
    return-void
.end method
