.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;
.super Ljava/lang/Object;
.source "EditPaymentRequestInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;",
        "",
        "()V",
        "EMPTY_BALANCE",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;",
        "getEMPTY_BALANCE",
        "()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEMPTY_BALANCE()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;
    .locals 1

    .line 51
    invoke-static {}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->access$getEMPTY_BALANCE$cp()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    move-result-object v0

    return-object v0
.end method
