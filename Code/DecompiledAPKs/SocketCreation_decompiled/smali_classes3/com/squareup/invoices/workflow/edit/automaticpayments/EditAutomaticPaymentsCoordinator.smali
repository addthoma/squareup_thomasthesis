.class public final Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditAutomaticPaymentsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001eB1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0018\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0017\u001a\u00020\u0005H\u0002J$\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001a2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00120\u001cH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "cnpFeesMessageHelper",
        "Lcom/squareup/cnp/CnpFeesMessageHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/cnp/CnpFeesMessageHelper;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "allowAutoPaymentToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "helperText",
        "Lcom/squareup/widgets/MessageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "data",
        "updateActionBar",
        "resources",
        "Landroid/content/res/Resources;",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private allowAutoPaymentToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

.field private helperText:Lcom/squareup/widgets/MessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/cnp/CnpFeesMessageHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cnpFeesMessageHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 92
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 94
    sget v0, Lcom/squareup/features/invoices/R$id;->opt_in_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    .line 95
    sget v0, Lcom/squareup/features/invoices/R$id;->allow_automatic_payment_toggle:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.a\u2026automatic_payment_toggle)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->allowAutoPaymentToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;)V
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "helperText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-virtual {v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->invoiceAutomaticPaymentOptInMessage()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->allowAutoPaymentToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    const-string v1, "allowAutoPaymentToggle"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;->getAllowAutoPayments()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->allowAutoPaymentToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$update$1;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$update$1;-><init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 74
    new-instance v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$update$2;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v0, "view.resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen;->getEventHandler()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 85
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 86
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payments:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 87
    new-instance v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$updateActionBar$1;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$updateActionBar$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->bindViews(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;->screens:Lio/reactivex/Observable;

    .line 60
    sget-object v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$attach$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$attach$2;-><init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .map { i\u2026ribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
