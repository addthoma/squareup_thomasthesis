.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final unitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->unitFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->moneyLocaleDigitsKeyListenerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyDigitsKeyListenerFactory;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->unitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator_Factory_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
