.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->onReact(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceFileAttachmentReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceFileAttachmentReactor.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,589:1\n85#2,2:590\n85#2,2:592\n85#2,2:594\n85#2,2:596\n85#2,2:598\n123#2,5:600\n85#2,2:605\n123#2,5:607\n85#2,2:612\n125#2,3:614\n85#2,2:617\n85#2,2:619\n85#2,2:621\n123#2,5:623\n85#2,2:628\n85#2,2:630\n85#2,2:632\n85#2,2:634\n85#2,2:636\n85#2,2:638\n85#2,2:640\n85#2,2:642\n85#2,2:644\n85#2,2:646\n85#2,2:648\n85#2,2:650\n85#2,2:652\n123#2,5:654\n123#2,5:659\n85#2,2:664\n85#2,2:666\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceFileAttachmentReactor.kt\ncom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1\n*L\n129#1,2:590\n134#1,2:592\n143#1,2:594\n152#1,2:596\n161#1,2:598\n166#1,5:600\n186#1,2:605\n191#1,5:607\n212#1,2:612\n217#1,3:614\n238#1,2:617\n241#1,2:619\n245#1,2:621\n248#1,5:623\n253#1,2:628\n261#1,2:630\n264#1,2:632\n269#1,2:634\n274#1,2:636\n289#1,2:638\n299#1,2:640\n303#1,2:642\n314#1,2:644\n328#1,2:646\n337#1,2:648\n349#1,2:650\n350#1,2:652\n353#1,5:654\n368#1,5:659\n394#1,2:664\n406#1,2:666\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

.field final synthetic $workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    .line 128
    sget-object v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Idle;->INSTANCE:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$Idle;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 590
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 133
    :cond_0
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$AddAttachment;

    if-eqz v1, :cond_1

    .line 134
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$2;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 592
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 143
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$3;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 594
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 152
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$4;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$4;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 596
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 161
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$5;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 598
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 165
    :cond_1
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingImageSource;

    if-eqz v1, :cond_2

    .line 166
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingImageSource;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingImageSource;->getPhotoSource()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$launchCameraHelper(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$6;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$6;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 602
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$1;

    const/4 v7, 0x0

    const-string v6, ""

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 186
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$7;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$7;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 605
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 190
    :cond_2
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ChoosingPdf;

    if-eqz v1, :cond_3

    .line 191
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    const-string v1, "application/pdf"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$launchFilePicker(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;[Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$8;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 609
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$2;

    const/4 v7, 0x0

    const-string v6, ""

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 212
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$9;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$9;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 612
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 216
    :cond_3
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    if-eqz v1, :cond_5

    .line 217
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$compressImage(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Landroid/net/Uri;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$CompressingPhoto;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    const-string v0, "state.uri.path!!"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$10;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 614
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$3;

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 238
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$11;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 617
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$8;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$8;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 241
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$12;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 619
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$9;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 245
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$13;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$13;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 621
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$10;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$10;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 247
    :cond_5
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    if-eqz v1, :cond_6

    .line 248
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    .line 249
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$DownloadingImage;->getAttachmentToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$downloadFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 250
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$14;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$14;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 625
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$4;

    const/4 v7, 0x0

    const-string v6, ""

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$4;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 253
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$15;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$15;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 628
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$11;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$11;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 261
    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$16;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$16;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 630
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$12;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 264
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$17;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$17;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 632
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$13;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$13;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 269
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$18;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$18;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 634
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$14;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$14;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 273
    :cond_6
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorDownloading;

    if-eqz v1, :cond_7

    .line 274
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$19;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$19;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 636
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$15;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$15;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 288
    :cond_7
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewingImage;

    if-eqz v1, :cond_8

    .line 289
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$20;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$20;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 638
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$16;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$16;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 299
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$21;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$21;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 640
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$17;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$17;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 303
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$22;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$22;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 642
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$18;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$18;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 314
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$23;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$23;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 644
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$19;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$19;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 328
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$24;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$24;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 646
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$20;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$20;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 337
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$25;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$25;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 648
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$21;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$21;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 349
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$26;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$26;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 650
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$22;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$22;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 350
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$27;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$27;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 652
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$23;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$23;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 352
    :cond_8
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    if-eqz v1, :cond_9

    .line 353
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    .line 354
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$viewExternalFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ViewExternally;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 355
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$28;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$28;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 656
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$5;

    const/4 v7, 0x0

    const-string v6, ""

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$5;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 367
    :cond_9
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    if-eqz v1, :cond_a

    .line 368
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    .line 369
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    .line 370
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v1

    .line 371
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v2

    .line 372
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v4

    .line 373
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v5, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v5}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    move-result-object v5

    .line 369
    invoke-static {v0, v1, v2, v4, v5}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$uploadFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    .line 374
    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 375
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 661
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$6;

    const/4 v7, 0x0

    const-string v6, ""

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onWorkerResult$6;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 393
    :cond_a
    instance-of v1, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    if-eqz v1, :cond_b

    .line 394
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$30;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$30;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 664
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$24;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$24;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 405
    :cond_b
    instance-of v0, v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadSuccess;

    if-eqz v0, :cond_c

    .line 406
    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$31;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$31;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 666
    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$25;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$$special$$inlined$onEvent$25;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    :cond_c
    :goto_0
    return-void
.end method
