.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;
.super Ljava/lang/Object;
.source "AutomaticReminderErrorDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticReminderErrorDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticReminderErrorDialogScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "screen",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;Landroid/content/Context;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;)Landroid/app/Dialog;
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;)Landroid/app/Dialog;
    .locals 3

    .line 44
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->getTitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->getBody()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 48
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->getPositiveButton()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;->getText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$createDialog$1$1$1;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$createDialog$1$1$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 52
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen;->getNegativeButton()Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 53
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$createDialog$1$2$1;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$createDialog$1$2$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogScreen$DialogButton;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    :cond_0
    const/4 p2, 0x0

    .line 56
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026(false)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;->screens:Lio/reactivex/Observable;

    .line 36
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory$create$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticReminderErrorDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .firstOr\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
