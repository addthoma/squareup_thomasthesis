.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditPaymentScheduleWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,516:1\n149#2,5:517\n149#2,5:522\n149#2,5:527\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow\n*L\n446#1,5:517\n455#1,5:522\n461#1,5:527\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 %2<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0002$%B9\u0008\u0007\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016Jt\u0010\u0017\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0002\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u00050\u0019\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0018j6\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t`\u001aJ\u001a\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00022\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016JN\u0010\u001f\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010 \u001a\u00020\u00032\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\"H\u0016J\u0010\u0010#\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020\u0003H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "editPaymentScheduleScreenFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;",
        "scrubberUtils",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;",
        "editPaymentRequestWorkflow",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;",
        "validator",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;Lcom/squareup/analytics/Analytics;)V",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final editPaymentRequestWorkflow:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;

.field private final editPaymentScheduleScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final scrubberUtils:Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;

.field private final validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlinx/coroutines/CoroutineDispatcher;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p1    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentScheduleScreenFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubberUtils"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validator"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->editPaymentScheduleScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->scrubberUtils:Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->editPaymentRequestWorkflow:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getScrubberUtils$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->scrubberUtils:Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;

    return-object p0
.end method

.method public static final synthetic access$getValidator$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    return-object p0
.end method


# virtual methods
.method public final asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
            ">;"
        }
    .end annotation

    .line 488
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public initialState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getPaymentRequests()Ljava/util/List;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;-><init>(Ljava/util/List;)V

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->initialState(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "props"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    move-object/from16 v4, p3

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 385
    invoke-interface/range {p3 .. p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v3

    .line 388
    instance-of v5, v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    const-string v6, ""

    if-eqz v5, :cond_0

    iget-object v7, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->editPaymentScheduleScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    .line 389
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v8

    .line 390
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v9

    .line 391
    move-object v4, v2

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$Viewing;->getPaymentRequestList()Ljava/util/List;

    move-result-object v10

    .line 392
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$1;

    invoke-direct {v4, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    move-object v11, v4

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 395
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$2;

    invoke-direct {v4, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    move-object v12, v4

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 398
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;

    invoke-direct {v4, v0, v1, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/workflow/Sink;)V

    move-object v13, v4

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 415
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;

    invoke-direct {v4, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v14, v4

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 423
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;

    invoke-direct {v4, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    move-object v15, v4

    check-cast v15, Lkotlin/jvm/functions/Function2;

    .line 426
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;

    invoke-direct {v4, v0, v3, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$6;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    move-object/from16 v16, v4

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 430
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$7;

    invoke-direct {v4, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$7;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object/from16 v17, v4

    check-cast v17, Lkotlin/jvm/functions/Function0;

    .line 433
    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;

    invoke-direct {v4, v0, v3, v2, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$8;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    move-object/from16 v18, v4

    check-cast v18, Lkotlin/jvm/functions/Function0;

    .line 388
    invoke-virtual/range {v7 .. v18}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->fromPaymentRequests(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 518
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 519
    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 520
    sget-object v4, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v4}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v4

    .line 518
    invoke-direct {v2, v3, v1, v4}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 446
    sget-object v1, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v2, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 448
    :cond_0
    instance-of v5, v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;

    if-eqz v5, :cond_1

    .line 449
    sget-object v4, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 450
    iget-object v7, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->editPaymentScheduleScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;

    .line 451
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v8

    .line 452
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v9

    .line 453
    move-object v1, v2

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;->getPaymentRequestList()Ljava/util/List;

    move-result-object v10

    .line 454
    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$9;

    move-object v11, v2

    check-cast v11, Lkotlin/jvm/functions/Function1;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$10;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$10;

    move-object v12, v2

    check-cast v12, Lkotlin/jvm/functions/Function1;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$11;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$11;

    move-object v13, v2

    check-cast v13, Lkotlin/jvm/functions/Function1;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$12;

    move-object v14, v2

    check-cast v14, Lkotlin/jvm/functions/Function1;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$13;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$13;

    move-object v15, v2

    check-cast v15, Lkotlin/jvm/functions/Function2;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$14;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$14;

    move-object/from16 v16, v2

    check-cast v16, Lkotlin/jvm/functions/Function0;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$15;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$15;

    move-object/from16 v17, v2

    check-cast v17, Lkotlin/jvm/functions/Function0;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$16;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$16;

    move-object/from16 v18, v2

    check-cast v18, Lkotlin/jvm/functions/Function0;

    .line 450
    invoke-virtual/range {v7 .. v18}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreenFactory;->fromPaymentRequests(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 523
    new-instance v5, Lcom/squareup/workflow/legacy/Screen;

    .line 524
    const-class v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v7

    .line 525
    sget-object v8, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v8}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v8

    .line 523
    invoke-direct {v5, v7, v2, v8}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 457
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleErrorDialogScreen;

    .line 458
    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;->getErrorTitle()Ljava/lang/String;

    move-result-object v7

    .line 459
    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$ShowingError;->getErrorBody()Ljava/lang/String;

    move-result-object v1

    .line 460
    new-instance v8, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$17;

    invoke-direct {v8, v3}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$17;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 457
    invoke-direct {v2, v7, v1, v8}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleErrorDialogScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 528
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 529
    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleErrorDialogScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 530
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 528
    invoke-direct {v1, v3, v2, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 449
    invoke-virtual {v4, v5, v1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object v1

    .line 463
    sget-object v2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    sget-object v3, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2, v3}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    .line 466
    :cond_1
    instance-of v3, v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    if-eqz v3, :cond_2

    .line 468
    iget-object v3, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->editPaymentRequestWorkflow:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;

    move-object v5, v3

    check-cast v5, Lcom/squareup/workflow/Workflow;

    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;

    .line 469
    move-object v6, v2

    check-cast v6, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    invoke-virtual {v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getIndex()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v9

    invoke-virtual {v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getPaymentRequestList()Ljava/util/List;

    move-result-object v10

    .line 470
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getDefaultConfigs()Ljava/util/List;

    move-result-object v11

    move-object v6, v3

    .line 468
    invoke-direct/range {v6 .. v11}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;-><init>(ILcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;)V

    const/4 v7, 0x0

    .line 472
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;)V

    move-object v8, v1

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object/from16 v4, p3

    .line 467
    invoke-static/range {v4 .. v10}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 492
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
