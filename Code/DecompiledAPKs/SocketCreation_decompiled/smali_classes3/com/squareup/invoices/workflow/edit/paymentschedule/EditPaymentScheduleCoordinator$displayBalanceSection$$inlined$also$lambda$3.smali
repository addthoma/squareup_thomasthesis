.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;->displayBalanceSection(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "index",
        "",
        "invoke",
        "com/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$1$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $resources$inlined:Landroid/content/res/Resources;

.field final synthetic $screen$inlined:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;->$resources$inlined:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;->$screen$inlined:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleCoordinator$displayBalanceSection$$inlined$also$lambda$3;->$screen$inlined:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen;->getPaymentRequestClicked()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$InstallmentClicked;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestClicked$InstallmentClicked;-><init>(I)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
