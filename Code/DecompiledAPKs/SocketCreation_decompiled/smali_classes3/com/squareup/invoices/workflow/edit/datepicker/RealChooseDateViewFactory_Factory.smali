.class public final Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealChooseDateViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final chooseDateFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final customDateFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->chooseDateFactoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->customDateFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->chooseDateFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->customDateFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;

    move-result-object v0

    return-object v0
.end method
