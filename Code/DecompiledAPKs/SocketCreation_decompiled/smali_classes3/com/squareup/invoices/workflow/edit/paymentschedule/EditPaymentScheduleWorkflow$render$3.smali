.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->invoke(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 399
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;->isPercentage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->access$getScrubberUtils$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->scrubAndExtractPercentage(Ljava/lang/String;)J

    move-result-wide v0

    .line 402
    instance-of v2, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Deposit;

    if-eqz v2, :cond_0

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;

    invoke-direct {p1, v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositPercentageChanged;-><init>(J)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    goto :goto_0

    .line 403
    :cond_0
    instance-of v2, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;

    if-eqz v2, :cond_1

    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;->getIndex()I

    move-result p1

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentPercentageChanged;-><init>(IJ)V

    move-object p1, v2

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->this$0:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->access$getScrubberUtils$p(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->$props:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/ScrubberUtils;->scrubAndExtractMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 408
    instance-of v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Deposit;

    if-eqz v1, :cond_3

    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;

    invoke-direct {p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$DepositMoneyChanged;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    goto :goto_0

    .line 409
    :cond_3
    instance-of v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;

    if-eqz v1, :cond_4

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$TextUpdatedEvent$Installment;->getIndex()I

    move-result p1

    invoke-direct {v1, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$InstallmentMoneyChanged;-><init>(ILcom/squareup/protos/common/Money;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;

    .line 413
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$3;->$sink:Lcom/squareup/workflow/Sink;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 409
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
