.class final synthetic Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealFileAttachmentsViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/AddInvoiceImageAttachmentDialogFactory$Factory;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$Factory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lkotlin/Unit;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        ">;>;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012+\u0010\u0002\u001a\'\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;",
        "p1",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationScreen;",
        "Lkotlin/ParameterName;",
        "name",
        "screens",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(Lio/reactivex/Observable;)V"

    return-object v0
.end method

.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;

    .line 24
    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;-><init>(Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory$4;->invoke(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/fileattachments/UploadSuccessConfirmationCoordinator;

    move-result-object p1

    return-object p1
.end method
