.class final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;
.super Lkotlin/jvm/internal/Lambda;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        ">;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        "instruments",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/util/List;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;)",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
            ">;"
        }
    .end annotation

    const-string v0, "instruments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 54
    new-instance v9, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    .line 55
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v3

    .line 56
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v4, v4, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v4, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v4}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentInstrumentToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, p1, v4}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->access$indexWithToken(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Ljava/util/List;Ljava/lang/String;)I

    move-result v5

    .line 57
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v7

    .line 58
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getCurrentPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    sget-object v4, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    aget v1, v4, v1

    const/4 v4, 0x1

    if-eq v1, v4, :cond_2

    const/4 v4, 0x2

    if-eq v1, v4, :cond_1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_0

    .line 61
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    invoke-static {v1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->access$getCnpFeesMessageHelper$p(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;)Lcom/squareup/cnp/CnpFeesMessageHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->cardOnFileMessage()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    .line 62
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 60
    :cond_1
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, ""

    .line 59
    check-cast v1, Ljava/lang/CharSequence;

    :goto_0
    move-object v8, v1

    const-string/jumbo v1, "when (state.currentPayme\u2026ption()\n                }"

    .line 58
    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x1

    move-object v1, v9

    move-object v4, p1

    .line 54
    invoke-direct/range {v1 .. v8}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/util/List;IZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 53
    invoke-direct {v0, v9}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$4;->invoke(Ljava/util/List;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
