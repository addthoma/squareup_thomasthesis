.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
        "it",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/image/FileUploadResult;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 377
    instance-of v0, p1, Lcom/squareup/invoices/image/FileUploadResult$Failure;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 378
    new-instance v8, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;

    .line 379
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getUploadValidationInfo()Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v3

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getFileMetadata()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;

    move-result-object v4

    .line 380
    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult$Failure;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileUploadResult$Failure;->getErrorTitle()Ljava/lang/String;

    move-result-object v5

    .line 381
    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileUploadResult$Failure;->getErrorBody()Ljava/lang/String;

    move-result-object v6

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getInvoiceTokenType()Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    move-result-object v7

    move-object v1, v8

    .line 378
    invoke-direct/range {v1 .. v7}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$ShowError$ErrorUploading;-><init>(Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/FileMetadata;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V

    .line 377
    invoke-direct {v0, v8}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 384
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/image/FileUploadResult$Success;

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadingImage;->getAttachmentStatus()Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$attemptDeleteFile(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus;)V

    .line 386
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 387
    new-instance v1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadSuccess;

    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult$Success;

    invoke-virtual {p1}, Lcom/squareup/invoices/image/FileUploadResult$Success;->getFileAttachmentMetadata()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState$UploadSuccess;-><init>(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    .line 386
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/invoices/image/FileUploadResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$onReact$1$29;->invoke(Lcom/squareup/invoices/image/FileUploadResult;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
