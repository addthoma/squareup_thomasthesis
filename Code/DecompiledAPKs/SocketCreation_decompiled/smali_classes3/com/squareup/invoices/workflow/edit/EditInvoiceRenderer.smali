.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;
.super Ljava/lang/Object;
.source "EditInvoiceRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\tJH\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceRenderer;->render(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p2, "workflows"

    invoke-static {p3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object p2, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_13

    .line 38
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    if-eqz p2, :cond_0

    sget-object p2, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    .line 39
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->getDeliveryMethodWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 38
    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 42
    :cond_0
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    if-eqz p2, :cond_1

    sget-object p2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    .line 43
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;->getEditRecurringWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 42
    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 46
    :cond_1
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    if-eqz p2, :cond_2

    sget-object p2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 47
    :cond_2
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    if-eqz p2, :cond_3

    sget-object p2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 48
    :cond_3
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    if-eqz p2, :cond_4

    sget-object p2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 49
    :cond_4
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    if-eqz p2, :cond_5

    sget-object p2, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->getInvoiceMessageWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 50
    :cond_5
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    if-eqz p2, :cond_6

    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentRenderer;

    check-cast p2, Lcom/squareup/workflow/legacy/Renderer;

    .line 51
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;->getInvoiceAttachmentWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 50
    invoke-static {p2, p1, p3}, Lcom/squareup/workflow/legacy/RendererKt;->render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 53
    :cond_6
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    if-eqz p2, :cond_8

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;->getEditInvoiceDetailsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 54
    :cond_8
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    if-eqz p2, :cond_a

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;->getAutoRemindersWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    check-cast p1, Ljava/util/Map;

    goto/16 :goto_0

    .line 55
    :cond_a
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    if-eqz p2, :cond_c

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->getAdditionalRecipientsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_b

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_b
    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 56
    :cond_c
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    if-eqz p2, :cond_e

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->getEditAutomaticPaymentsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_d

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_d
    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 57
    :cond_e
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    if-eqz p2, :cond_10

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;->getPaymentRequestWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_f

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_f
    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 58
    :cond_10
    instance-of p2, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    if-eqz p2, :cond_12

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;->getEditPaymentScheduleWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_11

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_11
    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_12
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 37
    :cond_13
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
