.class public final Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "AdditionalRecipientScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AdditionalEmailTextWatcher"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "canAddExtraView",
        "",
        "(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Z)V",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private canAddExtraView:Z

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 152
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;->this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    iput-boolean p2, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;->canAddExtraView:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 155
    iget-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;->canAddExtraView:Z

    if-eqz p1, :cond_0

    .line 156
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;->this$0:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v0, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->addEmailView$default(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Ljava/lang/String;ZILjava/lang/Object;)V

    const/4 p1, 0x0

    .line 157
    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;->canAddExtraView:Z

    :cond_0
    return-void
.end method
