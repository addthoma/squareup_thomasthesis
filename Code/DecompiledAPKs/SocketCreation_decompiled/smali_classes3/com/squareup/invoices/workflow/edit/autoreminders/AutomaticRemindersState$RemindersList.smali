.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
.super Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
.source "AutomaticRemindersState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RemindersList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\u0013\u0010\u0018\u001a\u00020\u00032\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\u0019\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0017H\u00d6\u0001R\u0014\u0010\u0006\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "remindersEnabled",
        "",
        "remindersListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "defaultListInfo",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V",
        "getDefaultListInfo",
        "()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "getReminderSettings",
        "()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "getRemindersEnabled",
        "()Z",
        "getRemindersListInfo",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

.field private final reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

.field private final remindersEnabled:Z

.field private final remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList$Creator;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList$Creator;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V
    .locals 1

    const-string v0, "remindersListInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultListInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersEnabled:Z

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p4

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->copy(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v0

    return v0
.end method

.method public final component2()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    return-object v0
.end method

.method public final copy(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
    .locals 1

    const-string v0, "remindersListInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultListInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reminderSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object v0
.end method

.method public getRemindersEnabled()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersEnabled:Z

    return v0
.end method

.method public getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RemindersList(remindersEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", remindersListInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", defaultListInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", reminderSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersEnabled:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->remindersListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->defaultListInfo:Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->reminderSettings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
