.class final Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RemindersListCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemindersListCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1$2\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,141:1\n1103#2,7:142\n*E\n*S KotlinDebug\n*F\n+ 1 RemindersListCoordinator.kt\ncom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1$2\n*L\n111#1,7:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "i",
        "",
        "reminderRow",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "invoke",
        "com/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$1$1$1$2",
        "com/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$$special$$inlined$row$lambda$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/cycler/StandardRowSpec$Creator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->invoke(ILcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;)V
    .locals 2

    const-string v0, "reminderRow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;I)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;->getSubtitle()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 117
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;->getIcon()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 118
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;

    invoke-direct {v0, p1}, Lcom/squareup/noho/NohoRow$Icon$SimpleIcon;-><init>(I)V

    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoRow;->setIcon(Lcom/squareup/noho/NohoRow$Icon;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$createRecycler$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    sget p2, Lcom/squareup/features/invoices/R$style;->Widget_InvoiceReminder_Row_Icon:I

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setIconStyleId(I)V

    :cond_1
    return-void
.end method
