.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditPaymentRequestCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->setListeners(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $eventHandler:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1;",
            ")V"
        }
    .end annotation

    .line 181
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;->this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;->$eventHandler:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 183
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;->this$0:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$setListeners$4;->$eventHandler:Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x1

    invoke-static {p1, v1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;->access$updateAmount(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;ZLkotlin/jvm/functions/Function1;)V

    return-void
.end method
