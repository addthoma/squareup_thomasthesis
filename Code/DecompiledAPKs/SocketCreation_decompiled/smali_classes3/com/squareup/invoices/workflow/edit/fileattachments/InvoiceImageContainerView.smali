.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;
.super Landroid/widget/LinearLayout;
.source "InvoiceImageContainerView.kt"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0006\u0010\u0014\u001a\u00020\u0013J\u0006\u0010\u0015\u001a\u00020\u0013J\u000e\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0018J\u0012\u0010\u0019\u001a\u00020\u00132\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\u001c\u0010\u001c\u001a\u00020\u00132\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016J\u0012\u0010!\u001a\u00020\u00132\u0008\u0010\"\u001a\u0004\u0018\u00010\u001bH\u0016J*\u0010\u000e\u001a&\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010 \u0011*\u0012\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010\u0018\u00010#0#J\u0014\u0010$\u001a\u00020\u00132\u000c\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u00130&J\u0006\u0010\'\u001a\u00020\u0013J\u0006\u0010(\u001a\u00020\u0013J\u001a\u0010)\u001a\u00020\u0013*\u00020\t2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020\u00130&H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0010\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;",
        "Landroid/widget/LinearLayout;",
        "Lcom/squareup/picasso/Target;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "blankIcon",
        "Landroid/widget/ImageView;",
        "image",
        "pdfIcon",
        "photoLoadedState",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        "kotlin.jvm.PlatformType",
        "bindViews",
        "",
        "displayError",
        "displayProgressBar",
        "loadPhoto",
        "requestCreator",
        "Lcom/squareup/picasso/RequestCreator;",
        "onBitmapFailed",
        "errorDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "onBitmapLoaded",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "from",
        "Lcom/squareup/picasso/Picasso$LoadedFrom;",
        "onPrepareLoad",
        "placeHolderDrawable",
        "Lio/reactivex/Observable;",
        "setImageAndIconOnClicks",
        "onClick",
        "Lkotlin/Function0;",
        "showBlankFilePlaceHolderImage",
        "showPdfPlaceHolderImage",
        "waitForAnimatorToMeasure",
        "block",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private blankIcon:Landroid/widget/ImageView;

.field private image:Landroid/widget/ImageView;

.field private pdfIcon:Landroid/widget/ImageView;

.field private final photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADING:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-static {p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string v0, "BehaviorRelay.createDefault(LOADING)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 43
    sget p2, Lcom/squareup/features/invoices/R$layout;->preview_attachment_content:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->bindViews()V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 113
    sget v0, Lcom/squareup/features/invoices/R$id;->image_icon_animator:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.image_icon_animator)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 114
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_photo_image:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.edit_photo_image)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->image:Landroid/widget/ImageView;

    .line 115
    sget v0, Lcom/squareup/features/invoices/R$id;->pdf_icon:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.pdf_icon)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->pdfIcon:Landroid/widget/ImageView;

    .line 116
    sget v0, Lcom/squareup/features/invoices/R$id;->blank_icon:I

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.blank_icon)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->blankIcon:Landroid/widget/ImageView;

    return-void
.end method

.method private final waitForAnimatorToMeasure(Lcom/squareup/widgets/SquareViewAnimator;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/SquareViewAnimator;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 101
    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$waitForAnimatorToMeasure$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$waitForAnimatorToMeasure$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method


# virtual methods
.method public final displayError()V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$id;->edit_photo_error_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->FAILED:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final displayProgressBar()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$id;->edit_photo_progress:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADING:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final loadPhoto(Lcom/squareup/picasso/RequestCreator;)V
    .locals 2

    const-string v0, "requestCreator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$loadPhoto$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$loadPhoto$1;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;Lcom/squareup/picasso/RequestCreator;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->waitForAnimatorToMeasure(Lcom/squareup/widgets/SquareViewAnimator;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->displayError()V

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 1

    .line 57
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->image:Landroid/widget/ImageView;

    const-string v0, "image"

    if-nez p2, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p1, :cond_1

    const-string p2, "animator"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->image:Landroid/widget/ImageView;

    if-nez p2, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Landroid/widget/ImageView;->getId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 59
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADED:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public final photoLoadedState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ">;"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final setImageAndIconOnClicks(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onClick"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->image:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "image"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->pdfIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v1, "pdfIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$2;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->blankIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "blankIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$3;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView$setImageAndIconOnClicks$3;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final showBlankFilePlaceHolderImage()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$id;->tap_to_preview_container:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->blankIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v1, "blankIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADED:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final showPdfPlaceHolderImage()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_0

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$id;->tap_to_preview_container:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->pdfIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v1, "pdfIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageContainerView;->photoLoadedState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;->LOADED:Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
