.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;
.super Ljava/lang/Object;
.source "EditPaymentRequestState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestState.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,259:1\n180#2:260\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestState.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion\n*L\n91#1:260\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J2\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015J\u0014\u0010\u0016\u001a\u00020\u000e*\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u000eH\u0002\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;",
        "",
        "()V",
        "createInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "isNewPaymentRequest",
        "",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "fromByteString",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;",
        "bytes",
        "Lokio/ByteString;",
        "restoreSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "depositDueDate",
        "firstSentDate",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;-><init>()V

    return-void
.end method

.method private final depositDueDate(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 0

    .line 164
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 165
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->getDeposit(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 166
    invoke-static {p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateDueDate(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    if-eqz p1, :cond_0

    return-object p1

    .line 167
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Can\'t edit a Balance PaymentRequest that doesn\'t have a deposit."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public final createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 4
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoice"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "today"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p3, :cond_1

    if-eqz p3, :cond_0

    .line 117
    move-object v2, p3

    check-cast v2, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v2}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v2

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.invoices.DisplayDetails.Invoice"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    move-object v2, v1

    .line 118
    :goto_0
    iget-object v3, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 116
    invoke-static {v2, v3, p5}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p5

    .line 121
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eqz v2, :cond_8

    sget-object v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    const/4 p3, 0x3

    if-ne v2, p3, :cond_8

    .line 146
    new-instance p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    .line 148
    iget-object p4, p2, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p4, v0}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateRemainderAmount(Ljava/util/List;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p4

    if-eqz p4, :cond_2

    .line 150
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    invoke-direct {v1, p2, p5}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;->depositDueDate(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 146
    invoke-direct {p3, p1, p4, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    goto :goto_3

    .line 149
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "No PaymentRequests on this invoice."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 123
    :cond_3
    instance-of v2, p3, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz v2, :cond_4

    .line 124
    check-cast p3, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-static {p1, p3}, Lcom/squareup/invoices/PaymentRequestsKt;->getCompletedAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/DisplayDetails$Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    :cond_4
    if-nez p4, :cond_6

    .line 129
    iget-object p3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq p3, v2, :cond_6

    if-eqz v1, :cond_5

    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result p3

    if-eqz p3, :cond_6

    .line 131
    :cond_5
    new-instance p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    invoke-static {p1, v0}, Lcom/squareup/invoices/PaymentRequestsKt;->calculateAmount(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {p3, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    goto :goto_1

    .line 133
    :cond_6
    sget-object p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$NonRemovable;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$NonRemovable;

    check-cast p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    .line 135
    :goto_1
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz p4, :cond_7

    .line 140
    sget-object p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$None;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$None;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    goto :goto_2

    .line 142
    :cond_7
    new-instance p4, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    invoke-static {p2, p5}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    invoke-direct {p4, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    move-object p2, p4

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    .line 135
    :goto_2
    invoke-direct {v2, p1, v1, p3, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V

    move-object p3, v2

    check-cast p3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    .line 157
    :goto_3
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-direct {p1, p3, v0, p5}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object p1

    .line 153
    :cond_8
    new-instance p2, Ljava/lang/IllegalArgumentException;

    .line 154
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Can\'t edit a "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " payment request."

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 153
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;
    .locals 3

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 92
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 93
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Editing;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    goto :goto_0

    .line 94
    :cond_0
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;

    .line 95
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 94
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ConfirmingRemoval;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    goto :goto_0

    .line 97
    :cond_1
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Validating;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    goto :goto_0

    .line 98
    :cond_2
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;

    .line 99
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readValidationErrorInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    move-result-object p1

    .line 98
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ShowingValidationError;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    goto :goto_0

    .line 101
    :cond_3
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$ChoosingDate;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    :goto_0
    return-object v0

    .line 102
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final restoreSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;

    move-result-object p1

    return-object p1
.end method
