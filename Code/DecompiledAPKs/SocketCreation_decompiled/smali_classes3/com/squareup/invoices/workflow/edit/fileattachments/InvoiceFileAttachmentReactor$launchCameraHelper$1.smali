.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;
.super Ljava/lang/Object;
.source "InvoiceFileAttachmentReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->launchCameraHelper(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)Lcom/squareup/workflow/legacy/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $photoSource:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->$photoSource:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 1

    .line 481
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$getCameraHelper$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$getInvoiceImageChooser$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/invoices/image/InvoiceImageChooser;

    move-result-object v0

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper$Listener;

    invoke-interface {p1, v0}, Lcom/squareup/camerahelper/CameraHelper;->setListener(Lcom/squareup/camerahelper/CameraHelper$Listener;)V

    .line 482
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->$photoSource:Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;

    sget-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/PhotoSource;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 484
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$getCameraHelper$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper;->startGallery()V

    goto :goto_0

    .line 483
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;->access$getCameraHelper$p(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper;->startCamera()V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 97
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor$launchCameraHelper$1;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
