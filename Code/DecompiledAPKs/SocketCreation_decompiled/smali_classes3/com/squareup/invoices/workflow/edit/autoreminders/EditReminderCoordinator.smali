.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditReminderScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditReminderScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditReminderScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,169:1\n215#2,2:170\n*E\n*S KotlinDebug\n*F\n+ 1 EditReminderScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator\n*L\n97#1,2:170\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\r\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001%B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0008\u0010\u001a\u001a\u00020\u0016H\u0002J\u0010\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u0004H\u0002J\u0010\u0010 \u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u0004H\u0002J\u0018\u0010!\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0014\u0010\"\u001a\u00020\u0016*\u00020\u00102\u0006\u0010#\u001a\u00020$H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "afterDateOption",
        "Lcom/squareup/marketfont/MarketCheckedTextView;",
        "beforeDateOption",
        "dateOptions",
        "Lcom/squareup/widgets/CheckableGroup;",
        "daysOffsetRow",
        "Lcom/squareup/register/widgets/list/EditQuantityRow;",
        "reminderMessage",
        "Lcom/squareup/widgets/SelectableEditText;",
        "reminderMessageTextChangedListener",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "removeReminderButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "clearListeners",
        "setOptionsText",
        "days",
        "",
        "setupActionBar",
        "data",
        "setupListeners",
        "update",
        "updateText",
        "value",
        "",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private afterDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private beforeDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private dateOptions:Lcom/squareup/widgets/CheckableGroup;

.field private daysOffsetRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field private reminderMessage:Lcom/squareup/widgets/SelectableEditText;

.field private reminderMessageTextChangedListener:Lcom/squareup/text/EmptyTextWatcher;

.field private removeReminderButton:Lcom/squareup/marketfont/MarketButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;Landroid/view/View;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->update(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 160
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 161
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_remove_reminder:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->removeReminderButton:Lcom/squareup/marketfont/MarketButton;

    .line 162
    sget v0, Lcom/squareup/features/invoices/R$id;->choose_days_offset:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->daysOffsetRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 163
    sget v0, Lcom/squareup/features/invoices/R$id;->date_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->dateOptions:Lcom/squareup/widgets/CheckableGroup;

    .line 164
    sget v0, Lcom/squareup/features/invoices/R$id;->option_before_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->beforeDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 165
    sget v0, Lcom/squareup/features/invoices/R$id;->option_after_date:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->afterDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 166
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_reminder_custom_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessage:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method private final clearListeners()V
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->daysOffsetRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v0, :cond_0

    const-string v1, "daysOffsetRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    move-object v2, v1

    check-cast v2, Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    iput-object v2, v0, Lcom/squareup/register/widgets/list/EditQuantityRow;->quantityListener:Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->dateOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_1

    const-string v2, "dateOptions"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessageTextChangedListener:Lcom/squareup/text/EmptyTextWatcher;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessage:Lcom/squareup/widgets/SelectableEditText;

    if-nez v1, :cond_2

    const-string v2, "reminderMessage"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_3
    return-void
.end method

.method private final setOptionsText(I)V
    .locals 3

    const-string v0, "afterDateOption"

    const-string v1, "beforeDateOption"

    const/4 v2, 0x1

    if-le p1, v2, :cond_2

    .line 112
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->beforeDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_before_due_date_plural_option:I

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 113
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->afterDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_after_due_date_plural_option:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->beforeDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_before_due_date_option:I

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 116
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->afterDateOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_send_after_due_date_option:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private final setupActionBar(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
    .locals 6

    .line 146
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 139
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4}, Lcom/squareup/marin/widgets/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/features/invoices/R$string;->invoice_reminder:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 140
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupActionBar$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 141
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 142
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupActionBar$2;

    invoke-direct {v3, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupActionBar$2;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 144
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 143
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setupListeners(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->removeReminderButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "removeReminderButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->daysOffsetRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v0, :cond_1

    const-string v1, "daysOffsetRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$2;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$2;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v1, Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->dateOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_2

    const-string v1, "dateOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$3;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 129
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$4;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$4;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessageTextChangedListener:Lcom/squareup/text/EmptyTextWatcher;

    .line 134
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessage:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_3

    const-string v0, "reminderMessage"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessageTextChangedListener:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final update(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;Landroid/view/View;)V
    .locals 5

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->setupActionBar(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->clearListeners()V

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->daysOffsetRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v0, :cond_0

    const-string v1, "daysOffsetRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getDays()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->dateOptions:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_1

    const-string v1, "dateOptions"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->Companion:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType$Companion;->getOPTION_ID_MAP()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 170
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 97
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getOffsetType()Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    move-result-object v4

    if-ne v3, v4, :cond_3

    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_2

    .line 171
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 97
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessage:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "reminderMessage"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getMessage()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v2}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->updateText(Lcom/squareup/widgets/SelectableEditText;Ljava/lang/CharSequence;)V

    .line 100
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$update$2;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$update$2;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 102
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getDays()I

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->setOptionsText(I)V

    .line 103
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->reminderMessage:Lcom/squareup/widgets/SelectableEditText;

    if-nez p2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getCharacterLimit()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setCharacterLimit(I)V

    .line 105
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->removeReminderButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_6

    const-string v0, "removeReminderButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p2, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getShowRemoveButton()Z

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->setupListeners(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V

    return-void

    .line 171
    :cond_7
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string p2, "Collection contains no element matching the predicate."

    invoke-direct {p1, p2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final updateText(Lcom/squareup/widgets/SelectableEditText;Ljava/lang/CharSequence;)V
    .locals 3

    .line 156
    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2, p2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->bindViews(Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(it, view) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
