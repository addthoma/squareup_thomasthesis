.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final editInvoiceScreensViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final realAdditionalRecipientsViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final realDeliveryMethodViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final realEditDetailsViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final realFileAttachmentsViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editInvoiceScreensViewFactoryProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editPaymentRequestViewFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realDeliveryMethodViewFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realFileAttachmentsViewFactoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realAdditionalRecipientsViewFactoryProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realEditDetailsViewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;)Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;
    .locals 9

    .line 77
    new-instance v8, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;
    .locals 8

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editInvoiceScreensViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editPaymentRequestViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->editPaymentScheduleViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realDeliveryMethodViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realFileAttachmentsViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realAdditionalRecipientsViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->realEditDetailsViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;

    invoke-static/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestViewFactory;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleViewFactory;Lcom/squareup/invoices/workflow/edit/delivery/RealDeliveryMethodViewFactory;Lcom/squareup/invoices/workflow/edit/fileattachments/RealFileAttachmentsViewFactory;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsViewFactory;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/RealEditDetailsViewFactory;)Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/EditInvoiceViewFactory;

    move-result-object v0

    return-object v0
.end method
