.class public abstract Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;
.super Ljava/lang/Object;
.source "PaymentRequestValidator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ValidationResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;,
        Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0005\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;",
        "",
        "()V",
        "Failed",
        "Succeeded",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Succeeded;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult$Failed;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestValidator$ValidationResult;-><init>()V

    return-void
.end method
