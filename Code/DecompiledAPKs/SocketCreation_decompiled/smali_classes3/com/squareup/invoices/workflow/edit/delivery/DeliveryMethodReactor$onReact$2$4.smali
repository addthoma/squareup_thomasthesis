.class final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;
.super Lkotlin/jvm/internal/Lambda;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 91
    new-instance v9, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    .line 92
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getTitle()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getInstruments()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;->getInstrumentIndex()I

    move-result v5

    .line 93
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v7

    .line 94
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->access$getCnpFeesMessageHelper$p(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;)Lcom/squareup/cnp/CnpFeesMessageHelper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->cardOnFileMessage()Ljava/lang/CharSequence;

    move-result-object v8

    const-string p1, "cnpFeesMessageHelper.cardOnFileMessage()"

    invoke-static {v8, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, v9

    .line 91
    invoke-direct/range {v1 .. v8}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$SelectingMethod;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/util/List;IZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 90
    invoke-direct {v0, v9}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$2$4;->invoke(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent$InstrumentChecked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
