.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final additionalRecipientsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final automaticRemindersWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseDateReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final deliveryMethodReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final editAutomaticPaymentsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceDetailsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentScheduleWorflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final editRecurringReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceAttachmentReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceMessageReactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->deliveryMethodReactorProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editRecurringReactorProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->chooseDateReactorProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->invoiceMessageReactorProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->invoiceAttachmentReactorProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editInvoiceDetailsWorkflowProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->automaticRemindersWorkflowProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->additionalRecipientsWorkflowProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editAutomaticPaymentsWorkflowProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editPaymentRequestWorkflowProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editPaymentScheduleWorflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;"
        }
    .end annotation

    .line 90
    new-instance v12, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;
    .locals 13

    .line 103
    new-instance v12, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;-><init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;
    .locals 12

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->deliveryMethodReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editRecurringReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->chooseDateReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->invoiceMessageReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->invoiceAttachmentReactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editInvoiceDetailsWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->automaticRemindersWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->additionalRecipientsWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editAutomaticPaymentsWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editPaymentRequestWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->editPaymentScheduleWorflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;

    invoke-static/range {v1 .. v11}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor_Factory;->get()Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;

    move-result-object v0

    return-object v0
.end method
