.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;
.super Ljava/lang/Object;
.source "RemindersListScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRemindersListScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RemindersListScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,136:1\n1360#2:137\n1429#2,3:138\n1360#2:141\n1429#2,3:142\n*E\n*S KotlinDebug\n*F\n+ 1 RemindersListScreen.kt\ncom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory\n*L\n55#1:137\n55#1,3:138\n58#1:141\n58#1,3:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00122\u0006\u0010\u0013\u001a\u00020\u0014J \u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0018H\u0002J\u0018\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bH\u0002J \u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u000c\u0010!\u001a\u00020\u001b*\u00020\nH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;)V",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "getToday",
        "()Lcom/squareup/protos/common/time/YearMonthDay;",
        "fromConfig",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
        "reminder",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;",
        "fromInfo",
        "",
        "info",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "fromInstance",
        "invoiceFirstSentDate",
        "dueDate",
        "Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;",
        "invalidRow",
        "title",
        "",
        "subtitle",
        "validRow",
        "date",
        "sent",
        "",
        "format",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->calendarFromYmd(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Calendar;

    move-result-object p1

    const-string v1, "ProtoDates.calendarFromYmd(this)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dateFormat.format(ProtoD\u2026lendarFromYmd(this).time)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final fromConfig(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;
    .locals 7

    .line 65
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->getReminderText(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    .line 66
    new-instance p1, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final fromInstance(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;
    .locals 2

    .line 74
    move-object v0, p3

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->getReminderText(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-virtual {p3, p2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->sendDate(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 77
    invoke-static {p2, p1}, Lcom/squareup/util/YearMonthDaysKt;->compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result v1

    if-gez v1, :cond_0

    .line 78
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invalid_reminder_prior_to_send_date:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->invalidRow(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    goto :goto_0

    .line 80
    :cond_0
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 82
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invalid_reminder_on_invoice_send_date:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->invalidRow(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    goto :goto_0

    .line 85
    :cond_1
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/util/YearMonthDaysKt;->compareTo(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p1

    if-gez p1, :cond_3

    .line 86
    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getSentOn()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    .line 87
    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->validRow(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    goto :goto_0

    .line 89
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invalid_reminder_in_past:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->invalidRow(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    goto :goto_0

    .line 92
    :cond_3
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->getToday()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 93
    invoke-virtual {p3}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;->getSentOn()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->validRow(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    goto :goto_0

    :cond_4
    const/4 p1, 0x0

    .line 96
    invoke-direct {p0, v0, p2, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->validRow(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getToday()Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    const-string v1, "InvoiceDateUtility.getToday(clock)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final invalidRow(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    .line 108
    sget v1, Lcom/squareup/features/invoices/R$drawable;->crossed_out_bell:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 105
    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final validRow(Ljava/lang/String;Lcom/squareup/protos/common/time/YearMonthDay;Z)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;
    .locals 2

    if-eqz p3, :cond_0

    .line 118
    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_sent_on:I

    goto :goto_0

    .line 119
    :cond_0
    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_schedule_for:I

    .line 121
    :goto_0
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    .line 123
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 124
    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->format(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "date"

    invoke-virtual {p3, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 125
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 126
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 127
    sget p3, Lcom/squareup/features/invoices/R$drawable;->bell:I

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 121
    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public final fromInfo(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;",
            ">;"
        }
    .end annotation

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    const/16 v1, 0xa

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;->getConfigs()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 138
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 139
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    .line 56
    invoke-direct {p0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->fromConfig(Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_0
    check-cast v0, Ljava/util/List;

    goto :goto_2

    .line 58
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInstances()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 141
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 142
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 143
    check-cast v1, Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;->getPaymentRequestDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    invoke-direct {p0, v3, v4, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;->fromInstance(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 144
    :cond_2
    move-object v0, v2

    check-cast v0, Ljava/util/List;

    :goto_2
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
