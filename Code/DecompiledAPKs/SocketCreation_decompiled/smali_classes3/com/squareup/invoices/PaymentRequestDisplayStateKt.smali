.class public final Lcom/squareup/invoices/PaymentRequestDisplayStateKt;
.super Ljava/lang/Object;
.source "PaymentRequestDisplayState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a.\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0002\u00a8\u0006\t"
    }
    d2 = {
        "getOverdueText",
        "",
        "completedAmount",
        "Lcom/squareup/protos/common/Money;",
        "overdueAmount",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getOverdueText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/invoices/PaymentRequestDisplayStateKt;->getOverdueText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method private static final getOverdueText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v1, "completedAmount.currency_code"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 91
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_amount_overdue:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 92
    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "amount_overdue"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 93
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "res.phrase(R.string.part\u2026mount))\n        .format()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_0
    sget p0, Lcom/squareup/common/invoices/R$string;->partial_payments_overdue:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    :goto_0
    return-object p0
.end method
