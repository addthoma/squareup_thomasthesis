.class final Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;
.super Ljava/lang/Object;
.source "PaymentRequests.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/PaymentRequestsKt;->sortedPaymentRequests(Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "first",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "kotlin.jvm.PlatformType",
        "second",
        "compare"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;

    invoke-direct {v0}, Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;->INSTANCE:Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/PaymentRequest;)I
    .locals 3

    .line 135
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 136
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p1, v0, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    .line 137
    :cond_1
    iget-object p1, p2, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object p2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne p1, p2, :cond_2

    const/4 v2, -0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/PaymentRequestsKt$sortedPaymentRequests$1;->compare(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/PaymentRequest;)I

    move-result p1

    return p1
.end method
