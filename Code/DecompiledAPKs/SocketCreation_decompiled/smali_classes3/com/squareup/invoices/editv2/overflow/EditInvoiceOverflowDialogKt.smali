.class public final Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt;
.super Ljava/lang/Object;
.source "EditInvoiceOverflowDialog.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0000\u00a8\u0006\t"
    }
    d2 = {
        "showOverflowMenu",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
        "resources",
        "Landroid/content/res/Resources;",
        "context",
        "Landroid/content/Context;",
        "onOverflowMenuClicked",
        "Lkotlin/Function0;",
        "",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final showOverflowMenu(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Landroid/content/res/Resources;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;",
            "Landroid/content/res/Resources;",
            "Landroid/content/Context;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;"
        }
    .end annotation

    const-string v0, "$this$showOverflowMenu"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onOverflowMenuClicked"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget v0, Lcom/squareup/features/invoices/R$layout;->overflow_view:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->inflate(Landroid/content/Context;I)Landroid/view/View;

    move-result-object p2

    .line 76
    new-instance v0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt$showOverflowMenu$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt$showOverflowMenu$1;-><init>(Landroid/view/View;)V

    check-cast v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    .line 77
    sget p2, Lcom/squareup/features/invoices/R$string;->open_menu:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 75
    invoke-virtual {p0, v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 79
    new-instance p1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt$sam$java_lang_Runnable$0;

    invoke-direct {p1, p3}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p0

    const-string p1, "showCustomView(onOverflowMenuClicked)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
