.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handleAttachmentRowClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeRunnerV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1\n*L\n1#1,1749:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $attachmentClicked:Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;->$attachmentClicked:Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 8

    .line 1606
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;->$attachmentClicked:Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$AttachmentClicked;->getAttachmentToken()Ljava/lang/String;

    move-result-object v3

    .line 1608
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    const-string v1, "invoice.attachment"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    if-eqz v1, :cond_2

    .line 1611
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    .line 1612
    new-instance v7, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;

    .line 1613
    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    .line 1614
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v4, "invoice.id_pair.server_id"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1613
    invoke-direct {v2, p1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    .line 1617
    invoke-static {v1}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->toFileDisplayName(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Ljava/lang/String;

    move-result-object v4

    .line 1618
    iget-object v5, v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    const-string p1, "attachment.extension"

    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1619
    iget-object v6, v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    const-string p1, "attachment.mime_type"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v7

    .line 1612
    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;

    .line 1611
    invoke-interface {v0, v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V

    return-void

    .line 1609
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Could not find attachment"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handleAttachmentRowClicked$1;->accept(Lcom/squareup/protos/client/invoice/Invoice;)V

    return-void
.end method
