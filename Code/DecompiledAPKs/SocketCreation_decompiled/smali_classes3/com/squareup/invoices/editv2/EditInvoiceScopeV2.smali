.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EditInvoiceScopeV2.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$ParentComponent;,
        Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;,
        Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$EditInvoiceScopeV2Module;,
        Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeV2\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,189:1\n24#2,4:190\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceScopeV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeV2\n*L\n164#1,4:190\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0004\u0018\u0019\u001a\u001bB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u000fH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "inCheckout",
        "",
        "(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V",
        "getEditInvoiceContext",
        "()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "getInCheckout",
        "()Z",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "register",
        "scope",
        "Companion",
        "Component",
        "EditInvoiceScopeV2Module",
        "ParentComponent",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;


# instance fields
.field private final editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

.field private final inCheckout:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->Companion:Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Companion;

    .line 190
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 193
    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)V
    .locals 1

    const-string v0, "editInvoiceContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    iput-boolean p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->inCheckout:Z

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 62
    const-class v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 61
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;

    .line 65
    invoke-interface {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;->editInvoiceWorkflowRunner()Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 66
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 159
    iget-object p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-static {p1, p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->writeEditInvoiceContext(Landroid/os/Parcel;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    .line 160
    iget-boolean p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->inCheckout:Z

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeBooleanAsInt(Landroid/os/Parcel;Z)V

    return-void
.end method

.method public final getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    return-object v0
.end method

.method public final getInCheckout()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->inCheckout:Z

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    const-class v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;

    .line 73
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v1

    .line 74
    invoke-interface {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;->editInvoiceScopeRunnerV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    move-result-object v2

    check-cast v2, Lmortar/bundler/Bundler;

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 75
    invoke-interface {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;->workingInvoiceEditor()Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 76
    invoke-interface {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2$Component;->workingOrderEditor()Lcom/squareup/invoices/order/WorkingOrderEditor;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
