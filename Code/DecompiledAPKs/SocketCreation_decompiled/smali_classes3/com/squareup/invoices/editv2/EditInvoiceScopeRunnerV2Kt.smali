.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0001*\u00020\u0002H\u0002\u001aT\u0010\u0004\u001a\u00020\u0005\"\u0004\u0008\u0000\u0010\u0006*\u0008\u0012\u0004\u0012\u0002H\u00060\u00072\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u00020\n0\t2\u0014\u0008\u0002\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\n0\t2\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000eH\u0002\u00a8\u0006\u000f"
    }
    d2 = {
        "canPreview",
        "",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "shouldDisableEditingId",
        "subscribe",
        "Lio/reactivex/disposables/Disposable;",
        "T",
        "Lio/reactivex/Maybe;",
        "onSuccess",
        "Lkotlin/Function1;",
        "",
        "onError",
        "",
        "onComplete",
        "Lkotlin/Function0;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$canPreview(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;->canPreview(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$shouldDisableEditingId(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;->shouldDisableEditingId(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p0

    return p0
.end method

.method private static final canPreview(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 1717
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static final shouldDisableEditingId(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 1715
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSingleInvoiceInRecurringSeries()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSeries()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static final subscribe(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lio/reactivex/disposables/Disposable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Maybe<",
            "TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/disposables/Disposable;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1723
    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v0

    :cond_0
    check-cast p1, Lio/reactivex/functions/Consumer;

    if-eqz p2, :cond_1

    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, v0

    :cond_1
    check-cast p2, Lio/reactivex/functions/Consumer;

    if-eqz p3, :cond_2

    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Action$0;

    invoke-direct {v0, p3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$sam$io_reactivex_functions_Action$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p3, v0

    :cond_2
    check-cast p3, Lio/reactivex/functions/Action;

    invoke-virtual {p0, p1, p2, p3}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    const-string p1, "subscribe(onSuccess, onError, onComplete)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method static synthetic subscribe$default(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lio/reactivex/disposables/Disposable;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 1720
    sget-object p1, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 1721
    sget-object p2, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$2;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$2;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 1722
    sget-object p3, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$3;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt$subscribe$3;

    check-cast p3, Lkotlin/jvm/functions/Function0;

    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2Kt;->subscribe(Lio/reactivex/Maybe;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    return-object p0
.end method
