.class final Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;
.super Ljava/lang/Object;
.source "EditInvoice2Coordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoice2Coordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoice2Coordinator.kt\ncom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3\n+ 2 InvoiceSectionContainerViewFactory.kt\ncom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactoryKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,132:1\n74#2:133\n1550#3,3:134\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoice2Coordinator.kt\ncom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3\n*L\n74#1:133\n74#1,3:134\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;)V
    .locals 6

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getShowOverflowMenuButton()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->$view:Landroid/view/View;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$updateActionBar(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    .line 64
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getUseBigHeader()Z

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 63
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$updateHeader(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;ZLjava/lang/CharSequence;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getBottomButtonText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$updateBottomButton(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getInvoiceSectionDataList()Ljava/util/List;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$getSectionsContainer$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Landroid/widget/LinearLayout;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 70
    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$getInvoiceSectionContainerViewFactory$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;

    .line 71
    iget-object v3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$getRunner$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

    move-result-object v3

    check-cast v3, Lcom/squareup/features/invoices/widgets/EventHandler;

    .line 72
    iget-object v4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-static {v4}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$getFeatures$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/settings/server/Features;

    move-result-object v4

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    .line 68
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewKt;->setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;Lcom/squareup/features/invoices/widgets/EventHandler;Z)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getInvoiceSectionDataList()Ljava/util/List;

    move-result-object p1

    .line 133
    check-cast p1, Ljava/lang/Iterable;

    .line 134
    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_2

    .line 135
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/features/invoices/widgets/SectionElement;

    instance-of v4, v4, Lcom/squareup/features/invoices/widgets/SectionElement$PreviewData;

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :cond_5
    :goto_2
    if-eqz v2, :cond_6

    .line 75
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->this$0:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->access$getAnalytics$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_PREVIEW:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_6
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;->accept(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;)V

    return-void
.end method
