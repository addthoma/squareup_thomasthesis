.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->addAttachment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/invoices/image/FileValidationResult;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/invoices/image/FileValidationResult;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    .line 1540
    sget-object v1, Lcom/squareup/invoices/image/FileValidationResult$Success;->INSTANCE:Lcom/squareup/invoices/image/FileValidationResult$Success;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1542
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 1543
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$saveDraftSingle(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lio/reactivex/Single;

    move-result-object p1

    .line 1544
    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "saveDraftSingle()\n      \u2026  }\n                    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 1553
    :cond_1
    new-instance v0, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftSuccess;

    const-string v1, "invoice"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$SaveDraftSuccess;-><init>(Lcom/squareup/protos/client/invoice/Invoice;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(SaveDraftSuccess(invoice))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 1556
    :cond_2
    instance-of p1, v0, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    if-eqz p1, :cond_3

    .line 1557
    new-instance p1, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$ValidationError;

    const-string/jumbo v1, "validationResult"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/invoices/image/FileValidationResult$Failure;

    invoke-direct {p1, v0}, Lcom/squareup/invoices/editv2/AddAttachmentPreparationResult$ValidationError;-><init>(Lcom/squareup/invoices/image/FileValidationResult$Failure;)V

    .line 1556
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n           \u2026tionResult)\n            )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$addAttachment$2;->apply(Lkotlin/Pair;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
