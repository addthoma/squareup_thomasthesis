.class public final Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;
.super Ljava/lang/Object;
.source "EditInvoice1ScreenData_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bottomButtonTextFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final cnpFeesMessageHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentRequestDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final v2WidgetsEditPaymentRequestsDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;)V"
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p4, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->actionBarDataFactoryProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p5, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->bottomButtonTextFactoryProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p6, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p7, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p8, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p9, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->v2WidgetsEditPaymentRequestsDataFactoryProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p10, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p11, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p12, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/PaymentRequestData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cnp/CnpFeesMessageHelper;",
            ">;)",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;"
        }
    .end annotation

    .line 91
    new-instance v13, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Landroid/app/Application;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Lcom/squareup/cnp/CnpFeesMessageHelper;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;
    .locals 14

    .line 101
    new-instance v13, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;-><init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Landroid/app/Application;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Lcom/squareup/cnp/CnpFeesMessageHelper;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;
    .locals 13

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->actionBarDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->bottomButtonTextFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->paymentRequestDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/PaymentRequestData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->editPaymentRequestRowDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->v2WidgetsEditPaymentRequestsDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->cnpFeesMessageHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-static/range {v1 .. v12}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Landroid/app/Application;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Lcom/squareup/cnp/CnpFeesMessageHelper;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData_Factory_Factory;->get()Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    move-result-object v0

    return-object v0
.end method
