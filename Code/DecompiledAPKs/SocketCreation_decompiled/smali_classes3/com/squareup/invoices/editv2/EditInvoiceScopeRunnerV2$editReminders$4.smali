.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editReminders()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/util/tuple/Quartet<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "Ljava/util/List<",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\u00020\u00012\u00a7\u0001\u0010\u0002\u001a\u00a2\u0001\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007\u0012(\u0012&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t\u0018\u00010\n0\u0008 \u0005*P\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0004\u0012\u00020\u0006\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00070\u0007\u0012(\u0012&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\t0\t\u0018\u00010\n0\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u000b"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Quartet;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/util/tuple/Quartet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Quartet<",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component2()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component3()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component4()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 1455
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getReminderInfoFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    move-result-object v3

    const-string v4, "isRecurring"

    .line 1456
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 1458
    iget-object v5, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v5}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v5

    .line 1455
    invoke-virtual {v3, v4, v1, v5}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createForInvoice(ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    .line 1460
    iget-object v4, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v4}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getReminderInfoFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    move-result-object v4

    const-string v5, "defaultConfigs"

    .line 1461
    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1462
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 1464
    iget-object v5, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v5}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v5

    .line 1460
    invoke-virtual {v4, p1, v0, v1, v5}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createDefaultListForInvoice(Ljava/util/List;ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p1

    .line 1467
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    const-string v1, "reminderSettings"

    .line 1470
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1467
    invoke-interface {v0, v3, p1, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startReminders(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/util/tuple/Quartet;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$4;->accept(Lcom/squareup/util/tuple/Quartet;)V

    return-void
.end method
