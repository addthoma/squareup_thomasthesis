.class public final Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "CreateItemTutorial.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Seed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0014\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "priority",
        "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
        "(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tutorialv2/TutorialSeed$Priority;",
            ")V"
        }
    .end annotation

    const-string v0, "priority"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    invoke-direct {p0, p2}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 4

    const-string v0, "CREATE_ITEM_TUTORIAL"

    .line 235
    invoke-static {v0}, Ltimber/log/Timber;->tag(Ljava/lang/String;)Ltimber/log/Timber$Tree;

    move-result-object v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Creating tutorial"

    .line 236
    invoke-virtual {v0, v3, v2}, Ltimber/log/Timber$Tree;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    invoke-static {v0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;->access$getTutorialProvider$p(Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/tutorial/CreateItemTutorial;

    .line 238
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator$Seed;->getPriority()Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    move-result-object v2

    sget-object v3, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->MERCHANT_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial;->init(Z)V

    const-string v1, "itemsTutorial"

    .line 239
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method
