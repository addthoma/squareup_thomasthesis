.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;
.super Ljava/lang/Object;
.source "ChangeOptionValuesProps.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J)\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0006\u0010\u0016\u001a\u00020\u0014J\u0006\u0010\u0017\u001a\u00020\u0014J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;",
        "",
        "itemName",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "optionValueToExtend",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getItemName",
        "()Ljava/lang/String;",
        "getOptionValueToExtend",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hasDeletes",
        "hasSelectionsToMake",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final itemName:Ljava/lang/String;

.field private final optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 9
    check-cast p3, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->copy(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final component3()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getItemName()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final hasDeletes()Z
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->needsToDeleteVariations()Z

    move-result v0

    return v0
.end method

.method public final hasSelectionsToMake()Z
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->canGenerateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChangeOptionValuesProps(itemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionValueToExtend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
