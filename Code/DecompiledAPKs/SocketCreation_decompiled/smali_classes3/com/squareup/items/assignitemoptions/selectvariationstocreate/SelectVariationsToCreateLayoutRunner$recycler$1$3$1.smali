.class final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectVariationsToCreateLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsHelpText;",
        "Lcom/squareup/noho/NohoLabel;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectVariationsToCreateLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectVariationsToCreateLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1\n+ 2 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator\n*L\n1#1,251:1\n64#2,2:252\n*E\n*S KotlinDebug\n*F\n+ 1 SelectVariationsToCreateLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1\n*L\n86#1,2:252\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsHelpText;",
        "Lcom/squareup/noho/NohoLabel;",
        "context",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow;",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$SelectVariationsToCreateRow$UpdateExistingVariationsHelpText;",
            "Lcom/squareup/noho/NohoLabel;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/noho/NohoLabel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 73
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    sget-object v1, Lcom/squareup/noho/NohoLabel$Type;->BODY_2:Lcom/squareup/noho/NohoLabel$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/noho/NohoLabel$Type;)V

    .line 74
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    .line 75
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 74
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 77
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setGravity(I)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    .line 80
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 81
    sget v2, Lcom/squareup/noho/R$dimen;->noho_spacing_before_helper_text:I

    .line 80
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 84
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v3, 0x0

    .line 78
    invoke-virtual {v0, v3, v1, v3, v2}, Lcom/squareup/noho/NohoLabel;->setPadding(IIII)V

    .line 252
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1$$special$$inlined$bind$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$recycler$1$3$1$$special$$inlined$bind$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
