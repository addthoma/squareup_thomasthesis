.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;
.super Ljava/lang/Object;
.source "SelectVariationsToCreateProps.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0017\u0008\u0086\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000bH\u00c6\u0003J=\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u000b2\u0008\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020\u0007H\u00d6\u0001J\t\u0010!\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;",
        "",
        "itemName",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "numberOfExistingVariations",
        "",
        "optionValueToExtend",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "isIncremental",
        "",
        "(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "()Z",
        "getItemName",
        "()Ljava/lang/String;",
        "maxNumberOfCombinationsToSelect",
        "getMaxNumberOfCombinationsToSelect",
        "()I",
        "getNumberOfExistingVariations",
        "getOptionValueToExtend",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final isIncremental:Z

.field private final itemName:Ljava/lang/String;

.field private final maxNumberOfCombinationsToSelect:I

.field private final numberOfExistingVariations:I

.field private final optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iput p3, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p5, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    .line 13
    iget p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    rsub-int p1, p1, 0xfa

    iput p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->maxNumberOfCombinationsToSelect:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p7, p6, 0x8

    if-eqz p7, :cond_0

    const/4 p4, 0x0

    .line 10
    check-cast p4, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    move-object v4, p4

    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    .line 11
    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;ZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->copy(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    return v0
.end method

.method public final component4()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;
    .locals 7

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    iget v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getItemName()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMaxNumberOfCombinationsToSelect()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->maxNumberOfCombinationsToSelect:I

    return v0
.end method

.method public final getNumberOfExistingVariations()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    return v0
.end method

.method public final getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final isIncremental()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectVariationsToCreateProps(itemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->itemName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->numberOfExistingVariations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", optionValueToExtend="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->optionValueToExtend:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isIncremental="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->isIncremental:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
