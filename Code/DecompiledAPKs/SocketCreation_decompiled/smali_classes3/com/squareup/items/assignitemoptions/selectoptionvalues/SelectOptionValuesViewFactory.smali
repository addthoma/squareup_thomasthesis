.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "SelectOptionValuesViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "selectOptionsLayoutRunnerFactory",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;",
        "(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;)V
    .locals 12
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectOptionsLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 11
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory$1;

    const/4 v2, 0x1

    new-array v3, v2, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 12
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    const-class v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    .line 14
    sget v6, Lcom/squareup/items/assignitemoptions/impl/R$layout;->select_option_values:I

    .line 15
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory$2;

    invoke-direct {v7, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory$2;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;)V

    move-object v9, v7

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0xc

    const/4 v11, 0x0

    .line 12
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v4

    check-cast v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-direct {v1, p1, v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesViewFactory$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;[Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    check-cast v1, Lcom/squareup/workflow/WorkflowViewFactory;

    aput-object v1, v0, v5

    .line 18
    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameViewFactory;

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    aput-object p1, v0, v2

    .line 19
    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitViewFactory;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/WarnReachingVariationNumberLimitViewFactory;

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
