.class final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "+",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,468:1\n1360#2:469\n1429#2,3:470\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1\n*L\n111#1:469\n111#1,3:470\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        "output",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

.field final synthetic $state:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->$state:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput$Cancel;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$Select;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$Select;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 108
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput$Confirm;

    if-eqz p1, :cond_2

    .line 109
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->$state:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$WarnChanges;->isOptionSetRemoved()Z

    move-result p1

    .line 110
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOptionValueSelections()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 469
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 470
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 471
    check-cast v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 111
    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 472
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 108
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;

    invoke-direct {v1, p1, v0, v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;-><init>(ZLjava/lang/String;Ljava/util/List;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$render$1;->invoke(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
