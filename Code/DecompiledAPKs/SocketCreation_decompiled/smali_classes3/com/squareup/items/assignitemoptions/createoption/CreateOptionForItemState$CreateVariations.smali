.class public final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;
.super Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;
.source "CreateOptionForItemState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateVariations"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u000e\u001a\u00020\u0005H\u00d6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0019\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "createdOption",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "numberOfExistingVariations",
        "",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;I)V",
        "getCreatedOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "getNumberOfExistingVariations",
        "()I",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

.field private final numberOfExistingVariations:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;I)V
    .locals 1

    const-string v0, "createdOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;Lcom/squareup/cogs/itemoptions/ItemOption;IILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;I)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    return v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;I)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;
    .locals 1

    const-string v0, "createdOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;I)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    iget p1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCreatedOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final getNumberOfExistingVariations()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateVariations(createdOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfExistingVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->createdOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->numberOfExistingVariations:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
