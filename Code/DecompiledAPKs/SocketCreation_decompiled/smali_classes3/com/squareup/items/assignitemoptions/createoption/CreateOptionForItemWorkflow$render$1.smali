.class final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateOptionForItemWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->render(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "+",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "selectVariationsToCreateOutput",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

.field final synthetic $state:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->$state:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "selectVariationsToCreateOutput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$CancelSelection;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$DiscardVariationsCreation;

    .line 96
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->$state:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;

    check-cast v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->getCreatedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v1

    .line 95
    invoke-direct {p1, v0, v1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$DiscardVariationsCreation;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 99
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;

    .line 100
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;->getCombinations()Ljava/util/List;

    move-result-object v1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput$VariationsToCreateSelected;->getChosenValueUsedToExtendExistingVariations()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 102
    :cond_1
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->$props:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v2

    .line 99
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformVariationsCreation;-><init>(Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;->invoke(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
