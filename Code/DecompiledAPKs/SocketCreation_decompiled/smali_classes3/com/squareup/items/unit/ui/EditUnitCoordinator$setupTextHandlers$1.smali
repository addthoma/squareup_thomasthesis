.class final Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;
.super Ljava/lang/Object;
.source "EditUnitCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupTextHandlers(Lcom/squareup/items/unit/EditUnitScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/items/unit/EditUnitScreen;

.field final synthetic this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->accept(Ljava/lang/String;)V

    return-void
.end method

.method public final accept(Ljava/lang/String;)V
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-static {v0, v1}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->access$setupActionBar(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupTextHandlers$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setCustomUnitName(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    return-void
.end method
