.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$1;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogOnlineTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->runPublisher()Lorg/reactivestreams/Publisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/lang/Void;",
        "kotlin.jvm.PlatformType",
        "online",
        "Lcom/squareup/shared/catalog/Catalog$Online;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Online;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync$runPublisher$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;

    invoke-static {v0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;->access$getUnitId$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$DeleteUnitAndSync;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Online;->deleteCatalogConnectV2Object(Ljava/lang/String;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method
