.class public final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;
.super Ljava/lang/Object;
.source "StandardUnitsListCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final screensProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->screensProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->screensProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/recycler/RecyclerFactory;

    iget-object v2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->newInstance(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator_Factory;->get()Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    move-result-object v0

    return-object v0
.end method
