.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "saveUnitResult",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Lcom/squareup/items/unit/EditUnitInput;

.field final synthetic $state:Lcom/squareup/items/unit/EditUnitState;

.field final synthetic this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/items/unit/EditUnitInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    iput-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$state:Lcom/squareup/items/unit/EditUnitState;

    iput-object p3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$input:Lcom/squareup/items/unit/EditUnitInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/SaveUnitResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/SaveUnitResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "saveUnitResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    instance-of v0, p1, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->this$0:Lcom/squareup/items/unit/RealEditUnitWorkflow;

    .line 325
    check-cast p1, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;

    .line 326
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    .line 327
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$input:Lcom/squareup/items/unit/EditUnitInput;

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitInput;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v2

    .line 324
    invoke-static {v0, p1, v1, v2}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->access$logMeasurementUnitSave(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;Lcom/squareup/items/unit/EditUnitState$Saving;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V

    .line 329
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/items/unit/EditUnitResult$EditSaved;

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/EditUnitResult$EditSaved;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 332
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/unit/SaveUnitResult$SaveDiscarded;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 333
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 334
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$Saving;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 335
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v3, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v3}, Lcom/squareup/items/unit/EditUnitState$Saving;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 336
    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v3, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v3}, Lcom/squareup/items/unit/EditUnitState$Saving;->isDefaultUnit()Z

    move-result v3

    .line 333
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 332
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/SaveUnitResult;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;->invoke(Lcom/squareup/items/unit/SaveUnitResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
