.class final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardUnitsListCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;

    invoke-direct {v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;-><init>()V

    sput-object v0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;->INSTANCE:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;)J
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    instance-of v0, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "label|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;

    invoke-virtual {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;->getUnitFamily()Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    :goto_0
    int-to-long v0, p1

    goto :goto_1

    .line 170
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unit|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$StandardUnit;->getStandardUnit()Lcom/squareup/items/unit/LocalizedStandardUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    goto :goto_0

    :goto_1
    return-wide v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .line 54
    check-cast p1, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$1;->invoke(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method
