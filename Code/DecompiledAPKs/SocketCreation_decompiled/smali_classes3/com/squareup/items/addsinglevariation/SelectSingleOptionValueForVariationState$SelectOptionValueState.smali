.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
.super Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;
.source "SelectSingleOptionValueForVariationState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectOptionValueState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B9\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0008H\u00c6\u0003J=\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008H\u00c6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0013\u0010\u001b\u001a\u00020\u00082\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u00d6\u0003J\u0006\u0010\u001e\u001a\u00020\u001fJ\u000e\u0010 \u001a\u00020!2\u0006\u0010\u0005\u001a\u00020\u0006J\t\u0010\"\u001a\u00020\u001aH\u00d6\u0001J\t\u0010#\u001a\u00020\u0003H\u00d6\u0001J\u000e\u0010$\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0003J\u000e\u0010%\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0003J\u0019\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u001aH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\t\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "searchText",
        "",
        "newValueInEdit",
        "selectedValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "shouldHighlightDuplicateName",
        "",
        "shouldFocusOnNewValueRowAndShowKeyboard",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V",
        "getNewValueInEdit",
        "()Ljava/lang/String;",
        "getSearchText",
        "getSelectedValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "getShouldFocusOnNewValueRowAndShowKeyboard",
        "()Z",
        "getShouldHighlightDuplicateName",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "gotoDuplicateOptionValueNameState",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;",
        "gotoDuplicateVariationState",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;",
        "hashCode",
        "toString",
        "updateNewValue",
        "updateSearchText",
        "newSearchText",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final newValueInEdit:Ljava/lang/String;

.field private final searchText:Ljava/lang/String;

.field private final selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

.field private final shouldFocusOnNewValueRowAndShowKeyboard:Z

.field private final shouldHighlightDuplicateName:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState$Creator;

    invoke-direct {v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V
    .locals 1

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->searchText:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->newValueInEdit:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p4, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldHighlightDuplicateName:Z

    iput-boolean p5, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const-string v0, ""

    if-eqz p7, :cond_0

    move-object p7, v0

    goto :goto_0

    :cond_0
    move-object p7, p1

    :goto_0
    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move-object v0, p2

    :goto_1
    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    .line 18
    move-object p3, p1

    check-cast p3, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_2
    move-object v1, p3

    and-int/lit8 p1, p6, 0x8

    const/4 p2, 0x0

    if-eqz p1, :cond_3

    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    move v2, p4

    :goto_2
    and-int/lit8 p1, p6, 0x10

    if-eqz p1, :cond_4

    const/4 p6, 0x0

    goto :goto_3

    :cond_4
    move p6, p5

    :goto_3
    move-object p1, p0

    move-object p2, p7

    move-object p3, v0

    move-object p4, v1

    move p5, v2

    .line 20
    invoke-direct/range {p1 .. p6}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result p5

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v0

    return v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v0

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 7

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newValueInEdit"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result p1

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getNewValueInEdit()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->newValueInEdit:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchText()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->searchText:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public getShouldFocusOnNewValueRowAndShowKeyboard()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    return v0
.end method

.method public getShouldHighlightDuplicateName()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldHighlightDuplicateName:Z

    return v0
.end method

.method public final gotoDuplicateOptionValueNameState()Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;
    .locals 5

    .line 34
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    .line 35
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v2

    .line 37
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v3

    .line 38
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v4

    .line 34
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public final gotoDuplicateVariationState(Lcom/squareup/cogs/itemoptions/ItemOptionValue;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;
    .locals 4

    const-string v0, "selectedValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;

    .line 42
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v3

    .line 41
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;Z)V

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectOptionValueState(searchText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", newValueInEdit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldHighlightDuplicateName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldHighlightDuplicateName()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", shouldFocusOnNewValueRowAndShowKeyboard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final updateNewValue(Ljava/lang/String;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 9

    const-string v0, "newValueInEdit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x5

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p1

    .line 28
    invoke-static/range {v1 .. v8}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-result-object p1

    return-object p1
.end method

.method public final updateSearchText(Ljava/lang/String;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 9

    const-string v0, "newSearchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xe

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    .line 23
    invoke-static/range {v1 .. v8}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->copy$default(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-result-object p1

    return-object p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->searchText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->newValueInEdit:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->selectedValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldHighlightDuplicateName:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;->shouldFocusOnNewValueRowAndShowKeyboard:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
