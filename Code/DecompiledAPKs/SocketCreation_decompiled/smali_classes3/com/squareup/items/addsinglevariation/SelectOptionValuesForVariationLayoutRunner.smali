.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;
.super Ljava/lang/Object;
.source "SelectOptionValuesForVariationLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$Factory;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesForVariationLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,217:1\n1360#2:218\n1429#2,3:219\n1360#2:222\n1429#2,3:223\n49#3:226\n50#3,3:232\n53#3:267\n599#4,4:227\n601#4:231\n310#5,6:235\n310#5,6:241\n310#5,6:247\n310#5,6:253\n310#5,6:259\n43#6,2:265\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesForVariationLayoutRunner.kt\ncom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner\n*L\n153#1:218\n153#1,3:219\n159#1:222\n159#1,3:223\n43#1:226\n43#1,3:232\n43#1:267\n43#1,4:227\n43#1:231\n43#1,6:235\n43#1,6:241\n43#1,6:247\n43#1,6:253\n43#1,6:259\n43#1,2:265\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0017\u0018B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateRecycler",
        "Factory",
        "SelectOptionValuesForVariationRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->view:Landroid/view/View;

    .line 40
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 42
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_option_values_for_variation_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 43
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 227
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 232
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 233
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 44
    sget-object p2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 236
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 50
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 236
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 235
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 242
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 66
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 77
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$3$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 241
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 248
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 81
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 248
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 247
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 254
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 100
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$5$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 254
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 253
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 260
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$$special$$inlined$row$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 116
    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$6$1;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$6$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 126
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$6$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$recycler$1$6$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 259
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 265
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 129
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 265
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 230
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 227
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final updateActionBar(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V
    .locals 4

    .line 147
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 144
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 145
    new-instance v2, Lcom/squareup/resources/ResourceCharSequence;

    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_option_values_for_variation_add_variation:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 146
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateRecycler(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V
    .locals 8

    .line 153
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;->getOptionValueSelectionsForCustomVariation()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 218
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 219
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 220
    check-cast v3, Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;

    .line 154
    new-instance v4, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$OptionWithSelectedValueRow;

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;->getValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    new-instance v7, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$1;

    invoke-direct {v7, v3, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$1;-><init>(Lcom/squareup/items/addsinglevariation/ItemOptionWithSelectedValue;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    invoke-direct {v4, v5, v6, v7}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$OptionWithSelectedValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 156
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 159
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;->getPotentialVariations()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 222
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 223
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 224
    check-cast v2, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;

    .line 160
    new-instance v4, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$AvailableVariationRow;

    invoke-virtual {v2}, Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;->getName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;

    invoke-direct {v6, v2, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$$inlined$map$lambda$2;-><init>(Lcom/squareup/items/addsinglevariation/ItemOptionValueCombination;Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    invoke-direct {v4, v5, v6}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$AvailableVariationRow;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    .line 162
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 225
    :cond_2
    check-cast v3, Ljava/util/List;

    .line 165
    new-instance v0, Lkotlin/jvm/internal/Ref$ObjectRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$ObjectRef;-><init>()V

    sget-object v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$AddCustomVariationSectionHeader;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$AddCustomVariationSectionHeader;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    .line 166
    check-cast v1, Ljava/lang/Iterable;

    .line 165
    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    .line 168
    new-instance v2, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$CreateVariationButton;

    .line 169
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;->getShouldEnableCreateVariationButton()Z

    move-result v4

    .line 170
    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;->getCreateCustomVariation()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    .line 168
    invoke-direct {v2, v4, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$CreateVariationButton;-><init>(ZLkotlin/jvm/functions/Function0;)V

    .line 167
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 166
    invoke-static {v1, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    .line 165
    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 173
    move-object p1, v3

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_3

    .line 174
    iget-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    sget-object v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$ChooseFromOptionValuesSectionHeader;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$SelectOptionValuesForVariationRow$ChooseFromOptionValuesSectionHeader;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    iput-object p1, v0, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    .line 176
    :cond_3
    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$1;

    invoke-direct {v1, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$updateRecycler$1;-><init>(Lkotlin/jvm/internal/Ref$ObjectRef;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->updateActionBar(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->updateRecycler(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    .line 138
    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationLayoutRunner;->showRendering(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
