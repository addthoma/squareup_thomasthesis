.class public final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "SelectSingleOptionValueForVariationWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectSingleOptionValueForVariationWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,211:1\n32#2,12:212\n704#3:224\n777#3,2:225\n149#4,5:227\n149#4,5:232\n149#4,5:237\n*E\n*S KotlinDebug\n*F\n+ 1 SelectSingleOptionValueForVariationWorkflow.kt\ncom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow\n*L\n42#1,12:212\n52#1:224\n52#1,2:225\n96#1,5:227\n105#1,5:232\n116#1,5:237\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u0015B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016JN\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\u00032\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0003H\u0016\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "initialState",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;
    .locals 11

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 212
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 217
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 219
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 220
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 221
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 223
    :cond_3
    check-cast v2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    if-eqz v2, :cond_4

    move-object p2, v2

    goto :goto_2

    .line 42
    :cond_4
    new-instance p2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x1b

    const/4 v10, 0x0

    move-object v3, p2

    invoke-direct/range {v3 .. v10}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_2
    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;->initialState(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    check-cast p2, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;->render(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "props"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "state"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v5

    .line 52
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getAvailableValues()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 224
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/Collection;

    .line 225
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v15, 0x0

    const/4 v14, 0x2

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 53
    invoke-virtual {v7}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "null cannot be cast to non-null type java.lang.String"

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v9, "(this as java.lang.String).toLowerCase()"

    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "null cannot be cast to non-null type kotlin.CharSequence"

    if-eqz v7, :cond_3

    .line 54
    check-cast v7, Ljava/lang/CharSequence;

    invoke-static {v7}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    .line 55
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getSearchText()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v8, :cond_1

    check-cast v8, Ljava/lang/CharSequence;

    invoke-static {v8}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    invoke-static {v7, v8, v15, v14, v9}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v10}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v8}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v10}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v8}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_5
    move-object v6, v4

    check-cast v6, Ljava/util/List;

    .line 58
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getSearchText()Ljava/lang/String;

    move-result-object v3

    .line 60
    sget-object v4, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$2;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$2;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const-string v7, "SelectSingleOptionValueForVariation-Search"

    .line 57
    invoke-static {v2, v3, v7, v4}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v7

    .line 62
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getNewValueInEdit()Ljava/lang/String;

    move-result-object v3

    .line 64
    sget-object v4, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$3;->INSTANCE:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const-string v8, "NewOptionValueField"

    .line 61
    invoke-static {v2, v3, v8, v4}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v8

    .line 65
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v9

    .line 66
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getShouldHighlightDuplicateName()Z

    move-result v10

    .line 67
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v11

    .line 69
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;

    invoke-direct {v3, v2, v0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$4;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;)V

    move-object v12, v3

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 77
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$5;

    invoke-direct {v3, v2, v0, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$5;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)V

    move-object v13, v3

    check-cast v13, Lkotlin/jvm/functions/Function0;

    .line 86
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;

    invoke-direct {v3, v2, v0, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)V

    move-object v0, v3

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 95
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$7;

    invoke-direct {v3, v2, v1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$7;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 50
    new-instance v16, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    move-object/from16 v4, v16

    move-object v14, v0

    const/4 v0, 0x0

    move-object v15, v3

    invoke-direct/range {v4 .. v15}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    move-object/from16 v3, v16

    check-cast v3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 228
    new-instance v4, Lcom/squareup/workflow/legacy/Screen;

    .line 229
    const-class v5, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    const-string v6, ""

    invoke-static {v5, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 230
    sget-object v7, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 228
    invoke-direct {v4, v5, v3, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 99
    instance-of v3, v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;

    if-eqz v3, :cond_6

    sget-object v0, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v4, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_1

    .line 100
    :cond_6
    instance-of v3, v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;

    const/4 v5, 0x1

    if-eqz v3, :cond_7

    .line 101
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;

    .line 102
    new-instance v3, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$duplicateOptionDialogScreen$1;

    invoke-direct {v3, v2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$duplicateOptionDialogScreen$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 101
    invoke-direct {v1, v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 233
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 234
    const-class v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/DuplicateOptionValueNameScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 235
    sget-object v6, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v6}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v6

    .line 233
    invoke-direct {v2, v3, v1, v6}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 106
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    const/4 v3, 0x2

    new-array v3, v3, [Lkotlin/Pair;

    .line 107
    new-instance v6, Lkotlin/Pair;

    sget-object v7, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-direct {v6, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v3, v0

    .line 108
    new-instance v0, Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-direct {v0, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v0, v3, v5

    .line 106
    invoke-virtual {v1, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    goto :goto_1

    :cond_7
    const/4 v3, 0x2

    .line 111
    instance-of v1, v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;

    if-eqz v1, :cond_8

    .line 112
    new-instance v1, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;

    .line 113
    new-instance v7, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$duplicateVariationDialogScreen$1;

    invoke-direct {v7, v2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$duplicateVariationDialogScreen$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 112
    invoke-direct {v1, v7}, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 238
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 239
    const-class v7, Lcom/squareup/items/addsinglevariation/WarnDuplicateVariationDialogScreen;

    invoke-static {v7}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 240
    sget-object v7, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v7}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v7

    .line 238
    invoke-direct {v2, v6, v1, v7}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 117
    sget-object v1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    new-array v3, v3, [Lkotlin/Pair;

    .line 118
    new-instance v6, Lkotlin/Pair;

    sget-object v7, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-direct {v6, v7, v4}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v3, v0

    .line 119
    new-instance v0, Lkotlin/Pair;

    sget-object v4, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-direct {v0, v4, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v0, v3, v5

    .line 117
    invoke-virtual {v1, v3}, Lcom/squareup/container/PosLayering$Companion;->partial([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_8
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;->snapshotState(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
