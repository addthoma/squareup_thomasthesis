.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;
.super Ljava/lang/Object;
.source "EditOptionValueLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\'\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;",
        "",
        "()V",
        "EditOptionValueLayoutBinding",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;",
        "",
        "Lcom/squareup/workflow/V2LayoutBinding;",
        "getEditOptionValueLayoutBinding",
        "()Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "standardColorIdAndNames",
        "",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 76
    invoke-direct {p0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getEditOptionValueLayoutBinding()Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
    .locals 1

    .line 77
    invoke-static {}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->access$getEditOptionValueLayoutBinding$cp()Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    return-object v0
.end method
