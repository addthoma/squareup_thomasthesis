.class final Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditOptionWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow;->render(Lcom/squareup/items/editoption/EditOptionProps;Lcom/squareup/items/editoption/EditOptionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $hasDuplicates:Z

.field final synthetic $props:Lcom/squareup/items/editoption/EditOptionProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/items/editoption/RealEditOptionWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/editoption/RealEditOptionWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/items/editoption/EditOptionProps;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->this$0:Lcom/squareup/items/editoption/RealEditOptionWorkflow;

    iput-object p2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$props:Lcom/squareup/items/editoption/EditOptionProps;

    iput-boolean p4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$hasDuplicates:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 42
    invoke-virtual {p0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 7

    .line 121
    iget-object v0, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;

    .line 122
    iget-object v2, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$props:Lcom/squareup/items/editoption/EditOptionProps;

    invoke-virtual {v2}, Lcom/squareup/items/editoption/EditOptionProps;->getExistingOptionNames()Ljava/util/Set;

    move-result-object v2

    .line 123
    iget-object v3, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$props:Lcom/squareup/items/editoption/EditOptionProps;

    .line 124
    instance-of v4, v3, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    .line 125
    :cond_0
    instance-of v4, v3, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    if-eqz v4, :cond_1

    check-cast v3, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;

    invoke-virtual {v3}, Lcom/squareup/items/editoption/EditOptionProps$EditExistingOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v3

    .line 127
    :goto_0
    iget-boolean v4, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->$hasDuplicates:Z

    .line 128
    iget-object v5, p0, Lcom/squareup/items/editoption/RealEditOptionWorkflow$render$5;->this$0:Lcom/squareup/items/editoption/RealEditOptionWorkflow;

    invoke-static {v5}, Lcom/squareup/items/editoption/RealEditOptionWorkflow;->access$getLocaleProvider$p(Lcom/squareup/items/editoption/RealEditOptionWorkflow;)Ljavax/inject/Provider;

    move-result-object v5

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    const-string v6, "localeProvider.get()"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/util/Locale;

    .line 121
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;-><init>(Ljava/util/Set;Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/Locale;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void

    .line 125
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
