.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditOptionValueWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditOptionValueWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditOptionValueWorkflow.kt\ncom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,87:1\n32#2,12:88\n149#3,5:100\n*E\n*S KotlinDebug\n*F\n+ 1 EditOptionValueWorkflow.kt\ncom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow\n*L\n30#1,12:88\n56#1,5:100\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002@\u0012\u0004\u0012\u00020\u0002\u0012\u0008\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0001:\u0001\u0015B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u000bJ\u001a\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u00022\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016JV\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\r\u001a\u00020\u00022\n\u0010\u0011\u001a\u00060\u0003j\u0002`\u00042\u0016\u0010\u0012\u001a\u0012\u0012\u0008\u0012\u00060\u0003j\u0002`\u0004\u0012\u0004\u0012\u00020\u00050\u0013H\u0016J\u0014\u0010\u0014\u001a\u00020\u000f2\n\u0010\u0011\u001a\u00060\u0003j\u0002`\u0004H\u0016\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueState;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 88
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 93
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 95
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 96
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 97
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 99
    :cond_3
    check-cast v2, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 30
    :cond_4
    new-instance v2, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    .line 31
    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->getColor()Ljava/lang/String;

    move-result-object p1

    .line 30
    invoke-direct {v2, p2, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;->initialState(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;

    check-cast p2, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;->render(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "-",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 41
    invoke-virtual {p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$1;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "OptionValueNameField"

    invoke-static {p3, v1, v3, v2}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v5

    .line 45
    invoke-virtual {p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;->getColor()Ljava/lang/String;

    move-result-object p2

    .line 47
    sget-object v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$2;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const-string v2, "OptionValueColorField"

    .line 44
    invoke-static {p3, p2, v2, v1}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v6

    .line 48
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->getOptionInfo()Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;->getOptionName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->getOptionInfo()Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;->getOptionDisplayName()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result p3

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p3, 0x1

    :goto_1
    const-string v2, ""

    if-eqz p3, :cond_2

    move-object p3, v2

    goto :goto_2

    .line 50
    :cond_2
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " ("

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->getOptionInfo()Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/editoption/editoptionvalue/OptionInfo;->getOptionDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 49
    :goto_2
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 51
    invoke-virtual {p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueProps;->isNew()Z

    move-result p1

    xor-int/lit8 v8, p1, 0x1

    .line 52
    new-instance p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$3;

    invoke-direct {p1, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$3;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v9, p1

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 53
    new-instance p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$4;

    invoke-direct {p1, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v10, p1

    check-cast v10, Lkotlin/jvm/functions/Function0;

    .line 54
    new-instance p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$5;

    invoke-direct {p1, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v11, p1

    check-cast v11, Lkotlin/jvm/functions/Function0;

    .line 40
    new-instance p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    move-object v4, p1

    invoke-direct/range {v4 .. v11}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 101
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 102
    const-class p3, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 103
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 101
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 56
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow;->snapshotState(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
