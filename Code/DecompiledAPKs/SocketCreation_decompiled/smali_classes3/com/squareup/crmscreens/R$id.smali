.class public final Lcom/squareup/crmscreens/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crmscreens/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_coupon_row:I = 0x7f0a016c

.field public static final adjust_points_amount:I = 0x7f0a018c

.field public static final adjust_points_content:I = 0x7f0a018d

.field public static final adjust_points_reason_1:I = 0x7f0a018e

.field public static final adjust_points_reason_2:I = 0x7f0a018f

.field public static final adjust_points_reason_3:I = 0x7f0a0190

.field public static final adjust_points_reason_title:I = 0x7f0a0191

.field public static final crm_activity_list:I = 0x7f0a03dd

.field public static final crm_activity_progress_bar:I = 0x7f0a03df

.field public static final crm_add_coupon_button:I = 0x7f0a03e0

.field public static final crm_add_points_button:I = 0x7f0a03e4

.field public static final crm_all_frequent_items_header:I = 0x7f0a03e9

.field public static final crm_all_frequent_items_rows:I = 0x7f0a03ea

.field public static final crm_all_frequent_items_view:I = 0x7f0a03eb

.field public static final crm_all_notes_rows:I = 0x7f0a03ec

.field public static final crm_bill_history_button:I = 0x7f0a03ef

.field public static final crm_birthday_picker:I = 0x7f0a03f2

.field public static final crm_birthday_picker_date_picker:I = 0x7f0a03f3

.field public static final crm_birthday_picker_provide_year:I = 0x7f0a03f4

.field public static final crm_birthday_picker_provide_year_check:I = 0x7f0a03f5

.field public static final crm_conversation:I = 0x7f0a0414

.field public static final crm_conversation_card_screen:I = 0x7f0a0415

.field public static final crm_conversation_rows:I = 0x7f0a0416

.field public static final crm_coupon_check_row:I = 0x7f0a0417

.field public static final crm_coupon_container:I = 0x7f0a0418

.field public static final crm_coupon_row:I = 0x7f0a0419

.field public static final crm_create_note:I = 0x7f0a041d

.field public static final crm_create_note_reminder:I = 0x7f0a041e

.field public static final crm_current_balance_calculation:I = 0x7f0a041f

.field public static final crm_delete_note_button:I = 0x7f0a0437

.field public static final crm_expiring_points:I = 0x7f0a0443

.field public static final crm_expiring_points_row:I = 0x7f0a0444

.field public static final crm_group_edit:I = 0x7f0a0452

.field public static final crm_manage_coupons:I = 0x7f0a046e

.field public static final crm_message_divider:I = 0x7f0a0489

.field public static final crm_message_edit:I = 0x7f0a048a

.field public static final crm_message_input:I = 0x7f0a048b

.field public static final crm_note_message_left:I = 0x7f0a04a4

.field public static final crm_note_message_right:I = 0x7f0a04a5

.field public static final crm_points_balance_title:I = 0x7f0a04b0

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_reminder_one_month:I = 0x7f0a04d6

.field public static final crm_reminder_one_week:I = 0x7f0a04d7

.field public static final crm_reminder_tomorrow:I = 0x7f0a04d8

.field public static final crm_remove_points_button:I = 0x7f0a04dc

.field public static final crm_remove_reminder:I = 0x7f0a04dd

.field public static final crm_see_all_loyalty_tiers:I = 0x7f0a04f5

.field public static final crm_see_all_loyalty_tiers_list:I = 0x7f0a04f6

.field public static final crm_send_message_button:I = 0x7f0a04f8

.field public static final crm_send_message_warning:I = 0x7f0a04f9

.field public static final crm_view_all_coupons_and_rewards:I = 0x7f0a050c

.field public static final crm_view_note_body:I = 0x7f0a0511

.field public static final crm_view_note_creator_timestamp:I = 0x7f0a0512

.field public static final crm_view_note_reminder:I = 0x7f0a0513

.field public static final expiring_points_list:I = 0x7f0a073a

.field public static final expiring_points_policy:I = 0x7f0a073b

.field public static final hud_glyph:I = 0x7f0a0813

.field public static final hud_progress:I = 0x7f0a0815

.field public static final hud_text:I = 0x7f0a0816

.field public static final manage_coupons_and_rewards_list:I = 0x7f0a09a8

.field public static final message_container:I = 0x7f0a09d6

.field public static final message_container_bottom:I = 0x7f0a09d7

.field public static final price_input_field:I = 0x7f0a0c54

.field public static final reason_checkable_group:I = 0x7f0a0cfa

.field public static final reminder_date:I = 0x7f0a0d56

.field public static final reminder_time:I = 0x7f0a0d57

.field public static final reward_row:I = 0x7f0a0d7c

.field public static final reward_row_action_layout:I = 0x7f0a0d7e

.field public static final reward_row_action_text:I = 0x7f0a0d7f

.field public static final reward_row_counter_view:I = 0x7f0a0d80

.field public static final reward_row_subtitle:I = 0x7f0a0d83

.field public static final reward_row_title:I = 0x7f0a0d84

.field public static final sms_disclaimer:I = 0x7f0a0eac

.field public static final view_all_coupons_and_rewards_list:I = 0x7f0a10ef

.field public static final view_all_coupons_and_rewards_row:I = 0x7f0a10f0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
