.class public final Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;
.super Lretrofit2/CallAdapter$Factory;
.source "StandardResponseCallAdapterFactory.kt"

# interfaces
.implements Lcom/squareup/receiving/retrofit/StandardResponseHelper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J<\u0010\t\u001a\u0012\u0012\u0002\u0008\u0003\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000b\u0018\u00010\n2\u0006\u0010\u000c\u001a\u00020\r2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0096\u0002\u00a2\u0006\u0002\u0010\u0013J\u000c\u0010\u0014\u001a\u00020\r*\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u0006\u0012\u0002\u0008\u00030\u0017*\u00020\rH\u0016R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;",
        "Lretrofit2/CallAdapter$Factory;",
        "Lcom/squareup/receiving/retrofit/StandardResponseHelper;",
        "rpcScheduler",
        "Lio/reactivex/Scheduler;",
        "mainScheduler",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V",
        "get",
        "Lretrofit2/CallAdapter;",
        "Lcom/squareup/server/StandardResponse;",
        "returnType",
        "Ljava/lang/reflect/Type;",
        "annotations",
        "",
        "",
        "retrofit",
        "Lretrofit2/Retrofit;",
        "(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lretrofit2/Retrofit;)Lretrofit2/CallAdapter;",
        "getParameterUpperBound",
        "Ljava/lang/reflect/ParameterizedType;",
        "getRawTypeHelper",
        "Ljava/lang/Class;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final rpcScheduler:Lio/reactivex/Scheduler;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/receiving/StandardReceiver;)V
    .locals 1

    const-string v0, "rpcScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lretrofit2/CallAdapter$Factory;-><init>()V

    iput-object p1, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->rpcScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-void
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lio/reactivex/Scheduler;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getParameterUpperBound$s572770538(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;
    .locals 0

    .line 20
    invoke-static {p0, p1}, Lretrofit2/CallAdapter$Factory;->getParameterUpperBound(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRawType$s572770538(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .locals 0

    .line 20
    invoke-static {p0}, Lretrofit2/CallAdapter$Factory;->getRawType(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRpcScheduler$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lio/reactivex/Scheduler;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->rpcScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getStandardReceiver$p(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-object p0
.end method


# virtual methods
.method public fail(Ljava/lang/String;)Ljava/lang/Void;
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseHelper$DefaultImpls;->fail(Lcom/squareup/receiving/retrofit/StandardResponseHelper;Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public findResponseType(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseHelper$DefaultImpls;->findResponseType(Lcom/squareup/receiving/retrofit/StandardResponseHelper;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lretrofit2/Retrofit;)Lretrofit2/CallAdapter;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/Retrofit;",
            ")",
            "Lretrofit2/CallAdapter<",
            "*",
            "Lcom/squareup/server/StandardResponse<",
            "*>;>;"
        }
    .end annotation

    const-string v0, "returnType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retrofit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->isExpectedType(Ljava/lang/reflect/Type;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 37
    :cond_0
    sget-object v0, Lcom/squareup/server/DefaultStandardResponseFactory;->INSTANCE:Lcom/squareup/server/DefaultStandardResponseFactory;

    invoke-virtual {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->getRawTypeHelper(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 38
    invoke-virtual {v0, v2}, Lcom/squareup/server/DefaultStandardResponseFactory;->findConstructor(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->findResponseType(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v6

    .line 46
    new-instance v0, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;

    move-object v3, v0

    move-object v4, p0

    move-object v7, p3

    move-object v8, p2

    move-object v9, p1

    invoke-direct/range {v3 .. v9}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory$get$1;-><init>(Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;Ljava/lang/reflect/Constructor;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Ljava/lang/reflect/Type;)V

    check-cast v0, Lretrofit2/CallAdapter;

    return-object v0

    .line 40
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " requires a public constructor with "

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    const-class p1, Lcom/squareup/server/StandardResponse$Factory;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " as its only argument."

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->fail(Ljava/lang/String;)Ljava/lang/Void;

    throw v1

    .line 37
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.Class<com.squareup.server.StandardResponse<kotlin.Any>>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getParameterUpperBound(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;
    .locals 1

    const-string v0, "$this$getParameterUpperBound"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 75
    invoke-static {v0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->access$getParameterUpperBound$s572770538(ILjava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/Type;

    move-result-object p1

    const-string v0, "getParameterUpperBound(0, this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getRawTypeHelper(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            ")",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    const-string v0, "$this$getRawTypeHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p1}, Lcom/squareup/receiving/retrofit/StandardResponseCallAdapterFactory;->access$getRawType$s572770538(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p1

    const-string v0, "getRawType(this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public isExpectedType(Ljava/lang/reflect/Type;)Z
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, p1}, Lcom/squareup/receiving/retrofit/StandardResponseHelper$DefaultImpls;->isExpectedType(Lcom/squareup/receiving/retrofit/StandardResponseHelper;Ljava/lang/reflect/Type;)Z

    move-result p1

    return p1
.end method
