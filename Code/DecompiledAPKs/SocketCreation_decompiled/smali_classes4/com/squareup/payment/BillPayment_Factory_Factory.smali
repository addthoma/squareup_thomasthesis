.class public final Lcom/squareup/payment/BillPayment_Factory_Factory;
.super Ljava/lang/Object;
.source "BillPayment_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/BillPayment$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final advancedModifierLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final backgroundCaptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;"
        }
    .end annotation
.end field

.field private final billPaymentEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final localStrategyFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineStrategyFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineStrategyFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentAccuracyLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentReceipt$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serverClockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final voidMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentReceipt$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 93
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 94
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 95
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 96
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 97
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 98
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->receiptFactoryProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 99
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 100
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 101
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 102
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->gsonProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 103
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 104
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 105
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 106
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->advancedModifierLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 107
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->onlineStrategyFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 108
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->offlineStrategyFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 109
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->localStrategyFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 110
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 111
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 112
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 113
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 114
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 115
    iput-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/BillPayment_Factory_Factory;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentReceipt$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentAccuracyLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BillPaymentEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/VoidMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/payment/BillPayment_Factory_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 142
    new-instance v24, Lcom/squareup/payment/BillPayment_Factory_Factory;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/payment/BillPayment_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v24
.end method

.method public static newInstance(Lcom/squareup/payment/ledger/TransactionLedgerManager;Ljava/lang/Object;Lcom/squareup/protos/common/CurrencyCode;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/payment/PaymentReceipt$Factory;Lrx/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/BillPayment$Factory;
    .locals 25

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 156
    new-instance v24, Lcom/squareup/payment/BillPayment$Factory;

    move-object/from16 v0, v24

    move-object/from16 v2, p1

    check-cast v2, Lcom/squareup/payment/BackgroundCaptor;

    move-object/from16 v15, p14

    check-cast v15, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-object/from16 v16, p15

    check-cast v16, Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;

    move-object/from16 v17, p16

    check-cast v17, Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;

    invoke-direct/range {v0 .. v23}, Lcom/squareup/payment/BillPayment$Factory;-><init>(Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/payment/BackgroundCaptor;Lcom/squareup/protos/common/CurrencyCode;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/payment/PaymentReceipt$Factory;Lrx/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;Lcom/squareup/payment/BillPaymentOfflineStrategy$Factory;Lcom/squareup/payment/BillPaymentLocalStrategy$Factory;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/settings/server/Features;)V

    return-object v24
.end method


# virtual methods
.method public get()Lcom/squareup/payment/BillPayment$Factory;
    .locals 25

    move-object/from16 v0, p0

    .line 120
    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lrx/Scheduler;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/receiving/StandardReceiver;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->receiptFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/payment/PaymentReceipt$Factory;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lrx/Scheduler;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/account/ServerClock;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/google/gson/Gson;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->paymentAccuracyLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/PaymentAccuracyLogger;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->advancedModifierLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->onlineStrategyFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->offlineStrategyFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v17

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->localStrategyFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v18

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->billPaymentEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/payment/BillPaymentEvents;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->voidMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/payment/VoidMonitor;

    iget-object v1, v0, Lcom/squareup/payment/BillPayment_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/settings/server/Features;

    invoke-static/range {v2 .. v24}, Lcom/squareup/payment/BillPayment_Factory_Factory;->newInstance(Lcom/squareup/payment/ledger/TransactionLedgerManager;Ljava/lang/Object;Lcom/squareup/protos/common/CurrencyCode;Lrx/Scheduler;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/payment/PaymentReceipt$Factory;Lrx/Scheduler;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/google/gson/Gson;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/payment/PaymentAccuracyLogger;Lcom/squareup/log/advancedmodifiers/AdvancedModifierLogger;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/payment/VoidMonitor;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/BillPayment$Factory;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment_Factory_Factory;->get()Lcom/squareup/payment/BillPayment$Factory;

    move-result-object v0

    return-object v0
.end method
