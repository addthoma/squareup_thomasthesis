.class Lcom/squareup/payment/ledger/LedgerEntry;
.super Ljava/lang/Object;
.source "LedgerEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/ledger/LedgerEntry$Builder;
    }
.end annotation


# instance fields
.field final card_brand:Ljava/lang/String;

.field final card_pan_suffix:Ljava/lang/String;

.field final client_unique_key:Ljava/lang/String;

.field final details:Lcom/squareup/wire/Message;

.field final detailsClass:Ljava/lang/String;

.field final failure_reason:Ljava/lang/String;

.field final merchant_id:Ljava/lang/String;

.field final message:Ljava/lang/String;

.field final other_tender_name:Ljava/lang/String;

.field final payment_amount:Ljava/lang/Long;

.field final server_unique_key:Ljava/lang/String;

.field final timestamp:Ljava/lang/String;

.field final type:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;


# direct methods
.method constructor <init>(Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/wire/Message;Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry;->type:Lcom/squareup/payment/ledger/LoggedInTransactionLedgerManager$TransactionType;

    .line 31
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerEntry;->merchant_id:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/squareup/payment/ledger/LedgerEntry;->client_unique_key:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/squareup/payment/ledger/LedgerEntry;->server_unique_key:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/squareup/payment/ledger/LedgerEntry;->timestamp:Ljava/lang/String;

    .line 35
    iput-object p6, p0, Lcom/squareup/payment/ledger/LedgerEntry;->failure_reason:Ljava/lang/String;

    .line 36
    iput-object p7, p0, Lcom/squareup/payment/ledger/LedgerEntry;->payment_amount:Ljava/lang/Long;

    .line 37
    iput-object p8, p0, Lcom/squareup/payment/ledger/LedgerEntry;->card_brand:Ljava/lang/String;

    .line 38
    iput-object p9, p0, Lcom/squareup/payment/ledger/LedgerEntry;->card_pan_suffix:Ljava/lang/String;

    .line 39
    iput-object p10, p0, Lcom/squareup/payment/ledger/LedgerEntry;->other_tender_name:Ljava/lang/String;

    const/4 p1, 0x0

    if-nez p11, :cond_0

    move-object p2, p1

    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lcom/squareup/payment/ledger/LedgerEntry;->detailsClass:Ljava/lang/String;

    if-nez p11, :cond_1

    goto :goto_1

    .line 41
    :cond_1
    invoke-static {p11}, Lcom/squareup/payment/ledger/LedgerEntry;->redact(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object p1

    :goto_1
    iput-object p1, p0, Lcom/squareup/payment/ledger/LedgerEntry;->details:Lcom/squareup/wire/Message;

    .line 42
    iput-object p12, p0, Lcom/squareup/payment/ledger/LedgerEntry;->message:Ljava/lang/String;

    return-void
.end method

.method private static redact(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(TT;)TT;"
        }
    .end annotation

    .line 46
    invoke-static {p0}, Lcom/squareup/wire/ProtoAdapter;->get(Lcom/squareup/wire/Message;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    return-object p0
.end method
