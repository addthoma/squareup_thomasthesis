.class final Lcom/squareup/payment/ledger/LedgerKey$1;
.super Ljava/lang/Object;
.source "LedgerKey.java"

# interfaces
.implements Lcom/squareup/encryption/CryptoKeyAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/LedgerKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/encryption/CryptoKeyAdapter<",
        "Lcom/squareup/payment/ledger/LedgerKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyId(Lcom/squareup/payment/ledger/LedgerKey;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic getKeyId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/payment/ledger/LedgerKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/ledger/LedgerKey$1;->getKeyId(Lcom/squareup/payment/ledger/LedgerKey;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRawKey(Lcom/squareup/payment/ledger/LedgerKey;)[B
    .locals 0

    .line 46
    invoke-static {}, Lcom/squareup/payment/ledger/LedgerKey;->access$000()[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getRawKey(Ljava/lang/Object;)[B
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/payment/ledger/LedgerKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/ledger/LedgerKey$1;->getRawKey(Lcom/squareup/payment/ledger/LedgerKey;)[B

    move-result-object p1

    return-object p1
.end method

.method public isExpired(Lcom/squareup/payment/ledger/LedgerKey;)Z
    .locals 0

    .line 50
    invoke-virtual {p1}, Lcom/squareup/payment/ledger/LedgerKey;->isExpired()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isExpired(Ljava/lang/Object;)Z
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/payment/ledger/LedgerKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/ledger/LedgerKey$1;->isExpired(Lcom/squareup/payment/ledger/LedgerKey;)Z

    move-result p1

    return p1
.end method
