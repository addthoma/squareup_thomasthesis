.class public Lcom/squareup/payment/OrderAdjustment;
.super Ljava/lang/Object;
.source "OrderAdjustment.java"


# instance fields
.field public final appliedMoney:Lcom/squareup/protos/common/Money;

.field public final decimalPercentage:Lcom/squareup/util/Percentage;

.field public final discountApplicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

.field public final historicalTaxTypeName:Ljava/lang/String;

.field public final historicalTaxTypeNumber:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final subtotalType:Lcom/squareup/checkout/SubtotalType;

.field public final taxTypeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V
    .locals 0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    .line 146
    iput-object p2, p0, Lcom/squareup/payment/OrderAdjustment;->name:Ljava/lang/String;

    .line 147
    iput-object p4, p0, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    .line 148
    iput-object p3, p0, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    .line 149
    iput-object p5, p0, Lcom/squareup/payment/OrderAdjustment;->taxTypeId:Ljava/lang/String;

    .line 150
    iput-object p6, p0, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeName:Ljava/lang/String;

    .line 151
    iput-object p7, p0, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeNumber:Ljava/lang/String;

    .line 152
    iput-object p8, p0, Lcom/squareup/payment/OrderAdjustment;->discountApplicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    return-void
.end method

.method private static buildCompoundAdjustmentFrom(Ljava/util/List;Lcom/squareup/protos/client/bills/DiscountLineItem;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ")V"
        }
    .end annotation

    .line 176
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 177
    invoke-static {p1}, Lcom/squareup/payment/OrderAdjustment;->getDiscountPercentage(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {p1, v0, v1}, Lcom/squareup/payment/OrderAdjustment;->newAdjustmentFromDiscount(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static buildSplitAdjustmentsFrom(Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Lcom/squareup/protos/client/bills/DiscountLineItem;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ")V"
        }
    .end annotation

    .line 256
    sget-object v0, Lcom/squareup/protos/client/bills/ApplicationScope;->ITEMIZATION_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ApplicationScope;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization;

    .line 259
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 263
    :cond_1
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    .line 265
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 267
    invoke-static {p2, v1}, Lcom/squareup/payment/OrderAdjustment;->discountsMatch(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/client/bills/DiscountLineItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    iget-object v2, v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 270
    invoke-static {v1}, Lcom/squareup/payment/OrderAdjustment;->getDiscountPercentage(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/lang/String;

    move-result-object v3

    .line 268
    invoke-static {v1, v2, v3}, Lcom/squareup/payment/OrderAdjustment;->newAdjustmentFromDiscount(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 278
    :cond_3
    iget-object p0, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    const/4 v0, 0x1

    if-nez p0, :cond_4

    .line 279
    iget-object p0, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->discount_quantity:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    goto :goto_2

    :cond_4
    const/4 p0, 0x1

    .line 283
    :goto_2
    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 284
    iget-object v2, p2, Lcom/squareup/protos/client/bills/DiscountLineItem;->amounts:Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/DiscountLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    int-to-long v3, p0

    .line 285
    invoke-static {v3, v4}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    sget-object v4, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x0

    :goto_3
    add-int/lit8 v4, p0, -0x1

    if-ge v3, v4, :cond_5

    .line 289
    invoke-static {p2}, Lcom/squareup/payment/OrderAdjustment;->getDiscountPercentage(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/lang/String;

    move-result-object v4

    .line 288
    invoke-static {p2, v2, v4}, Lcom/squareup/payment/OrderAdjustment;->newAdjustmentFromDiscount(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 293
    :cond_5
    invoke-static {p2}, Lcom/squareup/payment/OrderAdjustment;->getDiscountPercentage(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/lang/String;

    move-result-object p0

    .line 292
    invoke-static {p2, v1, p0}, Lcom/squareup/payment/OrderAdjustment;->newAdjustmentFromDiscount(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    return-void
.end method

.method private static discountsMatch(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/client/bills/DiscountLineItem;)Z
    .locals 0

    .line 332
    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->cogs_object_id:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static fromDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;
    .locals 3

    .line 71
    iget-object v0, p0, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    sget-object v1, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    iget-object v2, p0, Lcom/squareup/checkout/Discount;->percentage:Lcom/squareup/util/Percentage;

    iget-object p0, p0, Lcom/squareup/checkout/Discount;->applicationMethod:Lcom/squareup/api/items/Discount$ApplicationMethod;

    invoke-static {p1, v0, v1, v2, p0}, Lcom/squareup/payment/OrderAdjustment;->of(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object p0

    return-object p0
.end method

.method public static fromTax(Lcom/squareup/checkout/Tax;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;
    .locals 6

    .line 76
    iget-object v1, p0, Lcom/squareup/checkout/Tax;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/checkout/Tax;->subtotalType()Lcom/squareup/checkout/SubtotalType;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkout/Tax;->percentage:Lcom/squareup/util/Percentage;

    iget-object v4, p0, Lcom/squareup/checkout/Tax;->cogsTypeId:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/payment/OrderAdjustment;->of(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object p0

    return-object p0
.end method

.method private static getDiscountPercentage(Lcom/squareup/protos/client/bills/DiscountLineItem;)Ljava/lang/String;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 307
    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->configuration:Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    return-object p0

    .line 309
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method private static getSubtotalType(Ljava/lang/String;)Lcom/squareup/checkout/SubtotalType;
    .locals 0

    .line 156
    invoke-static {p0}, Lcom/squareup/checkout/SubtotalType;->fromString(Ljava/lang/String;)Lcom/squareup/checkout/SubtotalType;

    move-result-object p0

    if-nez p0, :cond_0

    .line 157
    sget-object p0, Lcom/squareup/checkout/SubtotalType;->UNKNOWN:Lcom/squareup/checkout/SubtotalType;

    :cond_0
    return-object p0
.end method

.method private static getSubtotalType(Ljava/lang/String;Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/SubtotalType;
    .locals 1

    .line 162
    invoke-static {p0}, Lcom/squareup/payment/OrderAdjustment;->getSubtotalType(Ljava/lang/String;)Lcom/squareup/checkout/SubtotalType;

    move-result-object p0

    .line 163
    sget-object v0, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne p1, v0, :cond_0

    .line 164
    sget-object p0, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    :cond_0
    return-object p0
.end method

.method private static newAdjustmentFromDiscount(Lcom/squareup/protos/client/bills/DiscountLineItem;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lcom/squareup/payment/OrderAdjustment;
    .locals 10

    .line 322
    new-instance v9, Lcom/squareup/payment/OrderAdjustment;

    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->name:Ljava/lang/String;

    sget-object v3, Lcom/squareup/checkout/SubtotalType;->DISCOUNT:Lcom/squareup/checkout/SubtotalType;

    const/4 v0, 0x0

    .line 323
    invoke-static {p2, v0}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v4

    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;

    iget-object v8, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$DisplayDetails;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/OrderAdjustment;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V

    return-object v9
.end method

.method public static of(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/payment/OrderAdjustment;
    .locals 6

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 99
    invoke-static/range {v0 .. v5}, Lcom/squareup/payment/OrderAdjustment;->of(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object p0

    return-object p0
.end method

.method public static of(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)Lcom/squareup/payment/OrderAdjustment;
    .locals 10

    .line 105
    new-instance v9, Lcom/squareup/payment/OrderAdjustment;

    const-string v0, "appliedMoney"

    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v1, p0

    check-cast v1, Lcom/squareup/protos/common/Money;

    const-string p0, "type"

    .line 106
    invoke-static {p2, p0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    move-object v3, p0

    check-cast v3, Lcom/squareup/checkout/SubtotalType;

    const-string v6, ""

    const-string v7, ""

    move-object v0, v9

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/OrderAdjustment;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V

    return-object v9
.end method

.method public static of(Lcom/squareup/protos/client/bills/Bill;Z)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Bill;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_4

    .line 112
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/DiscountLineItem;

    if-eqz p1, :cond_0

    .line 118
    invoke-static {p0, v0, v2}, Lcom/squareup/payment/OrderAdjustment;->buildSplitAdjustmentsFrom(Lcom/squareup/protos/client/bills/Cart$LineItems;Ljava/util/List;Lcom/squareup/protos/client/bills/DiscountLineItem;)V

    goto :goto_0

    .line 120
    :cond_0
    invoke-static {v0, v2}, Lcom/squareup/payment/OrderAdjustment;->buildCompoundAdjustmentFrom(Ljava/util/List;Lcom/squareup/protos/client/bills/DiscountLineItem;)V

    goto :goto_0

    .line 124
    :cond_1
    iget-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    if-eqz p1, :cond_3

    .line 125
    iget-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    .line 126
    iget-object v2, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 127
    new-instance v12, Lcom/squareup/payment/OrderAdjustment;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iget-object v4, v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    iget-object v5, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->name:Ljava/lang/String;

    iget-object v1, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v3, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v1, v3, :cond_2

    sget-object v1, Lcom/squareup/checkout/SubtotalType;->INCLUSIVE_TAX:Lcom/squareup/checkout/SubtotalType;

    goto :goto_2

    :cond_2
    sget-object v1, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    :goto_2
    move-object v6, v1

    iget-object v1, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->percentage:Ljava/lang/String;

    const/4 v3, 0x0

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object v7

    iget-object v8, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_id:Ljava/lang/String;

    iget-object v9, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_name:Ljava/lang/String;

    iget-object v10, v2, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->tax_type_number:Ljava/lang/String;

    const/4 v11, 0x0

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Lcom/squareup/payment/OrderAdjustment;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V

    .line 127
    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 133
    :cond_3
    iget-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz p1, :cond_4

    .line 134
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 135
    new-instance p1, Lcom/squareup/payment/OrderAdjustment;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->amounts:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x0

    sget-object v4, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/squareup/payment/OrderAdjustment;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v0
.end method

.method public static of(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/value/Adjustment;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 89
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/value/Adjustment;

    .line 90
    new-instance v11, Lcom/squareup/payment/OrderAdjustment;

    invoke-virtual {v1}, Lcom/squareup/server/payment/value/Adjustment;->getApplied()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/server/payment/value/Adjustment;->getName()Ljava/lang/String;

    move-result-object v4

    .line 91
    invoke-virtual {v1}, Lcom/squareup/server/payment/value/Adjustment;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/server/payment/value/Adjustment;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/squareup/payment/OrderAdjustment;->getSubtotalType(Ljava/lang/String;Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/checkout/SubtotalType;

    move-result-object v5

    invoke-virtual {v1}, Lcom/squareup/server/payment/value/Adjustment;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v6

    const/4 v10, 0x0

    const-string v7, ""

    const-string v8, ""

    const-string v9, ""

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/squareup/payment/OrderAdjustment;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Lcom/squareup/checkout/SubtotalType;Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Discount$ApplicationMethod;)V

    .line 90
    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 94
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getPercentage(Lcom/squareup/text/Formatter;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/squareup/payment/OrderAdjustment;->decimalPercentage:Lcom/squareup/util/Percentage;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 171
    :cond_0
    invoke-interface {p1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
