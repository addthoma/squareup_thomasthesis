.class public Lcom/squareup/payment/tender/InstrumentTender;
.super Lcom/squareup/payment/tender/BaseCardTender;
.source "InstrumentTender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/tender/InstrumentTender$Builder;
    }
.end annotation


# instance fields
.field private final card:Lcom/squareup/Card;

.field private final contactToken:Ljava/lang/String;

.field private final instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/InstrumentTender$Builder;)V
    .locals 1

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseCardTender;-><init>(Lcom/squareup/payment/tender/BaseCardTender$Builder;)V

    .line 64
    invoke-static {p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->access$000(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    .line 65
    invoke-static {p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->access$100(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender;->contactToken:Ljava/lang/String;

    .line 66
    invoke-static {p1}, Lcom/squareup/payment/tender/InstrumentTender$Builder;->access$200(Lcom/squareup/payment/tender/InstrumentTender$Builder;)Lcom/squareup/Card;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/tender/InstrumentTender;->card:Lcom/squareup/Card;

    return-void
.end method


# virtual methods
.method public askForSignature()Z
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/payment/tender/InstrumentTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/tender/InstrumentTender;->shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    return v0
.end method

.method public canQuickCapture()Z
    .locals 2

    .line 108
    invoke-virtual {p0}, Lcom/squareup/payment/tender/InstrumentTender;->isAuthorizationSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender;->card:Lcom/squareup/Card;

    invoke-virtual {v0}, Lcom/squareup/Card;->isManual()Z

    move-result v0

    const-string v1, "InstrumentTender::canQuickCapture card is not manual"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/payment/tender/InstrumentTender;->card:Lcom/squareup/Card;

    return-object v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 4

    .line 78
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/InstrumentTender;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 81
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/InstrumentTender;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->display_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails$DisplayDetails;->last_four:Ljava/lang/String;

    .line 82
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 83
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 84
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v2

    .line 80
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/payment/tender/InstrumentTender;->contactToken:Ljava/lang/String;

    .line 100
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/StoredInstrument$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/InstrumentTender;->instrument:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;->instrument_token:Ljava/lang/String;

    .line 101
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/StoredInstrument$Builder;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->build()Lcom/squareup/protos/client/bills/StoredInstrument;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->stored_instrument(Lcom/squareup/protos/client/bills/StoredInstrument;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 94
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ON_FILE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public offlineCompatible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
