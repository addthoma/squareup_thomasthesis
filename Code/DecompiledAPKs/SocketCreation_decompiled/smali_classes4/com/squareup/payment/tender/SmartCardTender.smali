.class public Lcom/squareup/payment/tender/SmartCardTender;
.super Lcom/squareup/payment/tender/BaseCardTender;
.source "SmartCardTender.java"


# instance fields
.field private final accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

.field private final buyerSelectedAccountName:Ljava/lang/String;

.field private final cardholderName:Ljava/lang/String;

.field private final entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field private final fallbackCard:Lcom/squareup/Card;

.field private final fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

.field protected messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

.field protected sigRequested:Z

.field private final smartCardDataAuth:[B

.field protected smartCardDataCapture:[B

.field protected final usePreAuthTipping:Z

.field protected wasTenderApprovedOffline:Z


# direct methods
.method protected constructor <init>(Lcom/squareup/payment/tender/SmartCardTenderBuilder;)V
    .locals 1

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/payment/tender/BaseCardTender;-><init>(Lcom/squareup/payment/tender/BaseCardTender$Builder;)V

    .line 81
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 82
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->fallbackCard:Lcom/squareup/Card;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    .line 83
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->smartCardAuthRequestData:[B

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataAuth:[B

    .line 84
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->cardholderName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->cardholderName:Ljava/lang/String;

    .line 85
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 86
    iget-boolean v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->usePreAuthTipping:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->usePreAuthTipping:Z

    .line 87
    iget-boolean v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->wasTenderApprovedOffline:Z

    iput-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->wasTenderApprovedOffline:Z

    .line 88
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    .line 89
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->buyerSelectedAccountName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->buyerSelectedAccountName:Ljava/lang/String;

    .line 90
    iget-object v0, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->tipAmount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->tipPercentage:Lcom/squareup/util/Percentage;

    invoke-super {p0, v0, p1}, Lcom/squareup/payment/tender/BaseCardTender;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$O1Card;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/tender/SmartCardTender;->buildO1CardForFallback()Lcom/squareup/protos/client/bills/CardData$O1Card;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$R4Card;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/tender/SmartCardTender;->buildR4CardForFallback()Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$R6Card;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/tender/SmartCardTender;->buildR6CardForFallback()Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$X2Card;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/tender/SmartCardTender;->buildX2CardForFallback()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/tender/SmartCardTender;)Lcom/squareup/protos/client/bills/CardData$T2Card;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/payment/tender/SmartCardTender;->buildT2CardForFallback()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object p0

    return-object p0
.end method

.method private buildO1CardForFallback()Lcom/squareup/protos/client/bills/CardData$O1Card;
    .locals 2

    .line 418
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$O1Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$O1Card$Builder;-><init>()V

    .line 419
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$O1Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$O1Card$Builder;

    .line 420
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$O1Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$O1Card;

    move-result-object v0

    return-object v0
.end method

.method private buildR4CardForFallback()Lcom/squareup/protos/client/bills/CardData$R4Card;
    .locals 2

    .line 412
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;-><init>()V

    .line 413
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;

    .line 414
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-result-object v0

    return-object v0
.end method

.method private buildR6CardForFallback()Lcom/squareup/protos/client/bills/CardData$R6Card;
    .locals 2

    .line 406
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;-><init>()V

    .line 407
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    .line 408
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object v0

    return-object v0
.end method

.method private buildT2CardForFallback()Lcom/squareup/protos/client/bills/CardData$T2Card;
    .locals 2

    .line 400
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;-><init>()V

    .line 401
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    .line 402
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object v0

    return-object v0
.end method

.method private buildX2CardForFallback()Lcom/squareup/protos/client/bills/CardData$X2Card;
    .locals 2

    .line 394
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;-><init>()V

    .line 395
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    .line 396
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object v0

    return-object v0
.end method

.method public static cardreaderAccountTypeOrDefault(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;
    .locals 3

    if-nez p0, :cond_0

    .line 452
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-object p0

    .line 454
    :cond_0
    sget-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$protos$client$bills$CardTender$AccountType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardTender$AccountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 462
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_CREDIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-object p0

    .line 464
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown AccountType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEBIT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-object p0

    .line 458
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_SAVINGS:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-object p0

    .line 456
    :cond_4
    sget-object p0, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    return-object p0
.end method

.method private smartCardTenderShouldAskForSignature(Lcom/squareup/protos/common/Money;)Z
    .locals 3

    .line 332
    new-instance v0, Lcom/squareup/signature/SignatureDeterminer$Builder;

    invoke-direct {v0}, Lcom/squareup/signature/SignatureDeterminer$Builder;-><init>()V

    .line 333
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->hasIssuerRequestForSignature()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->hasIssuerRequestForSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->merchantAlwaysSkipSignature:Z

    .line 334
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantAlwaysSkipSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->merchantIsAllowedToNSR:Z

    .line 335
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantIsAllowedToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->merchantOptedInToNSR:Z

    .line 336
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->merchantOptedInToNSR(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->signatureOptionalMaxMoney:Lcom/squareup/protos/common/Money;

    .line 337
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->signatureRequiredThreshold(Lcom/squareup/protos/common/Money;)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 338
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->isGiftCard(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 339
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->isFallbackSwipe(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 340
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->requireCardTender()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->cardEntryMethod(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->showSignatureForCardNotPresent:Z

    .line 341
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardNotPresent(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->showSignatureForCardOnFile:Z

    .line 342
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->showSignatureForCardOnFile(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->sigRequested:Z

    .line 343
    invoke-virtual {v0, v1}, Lcom/squareup/signature/SignatureDeterminer$Builder;->smartReaderPresentAndRequestsSignature(Z)Lcom/squareup/signature/SignatureDeterminer$Builder;

    move-result-object v0

    .line 345
    invoke-virtual {v0}, Lcom/squareup/signature/SignatureDeterminer$Builder;->build()Lcom/squareup/signature/SignatureDeterminer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/signature/SignatureDeterminer;->shouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method static tenderAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/protos/client/bills/CardTender$AccountType;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 435
    :cond_0
    sget-object v0, Lcom/squareup/payment/tender/SmartCardTender$2;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentAccountType:[I

    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 443
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CREDIT:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 445
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown CrPaymentAccountType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 441
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->CHECKING:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 439
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->SAVINGS:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0

    .line 437
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$AccountType;->UNKNOWN_ACCOUNT_TYPE:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    return-object p0
.end method


# virtual methods
.method protected appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;
    .locals 2

    .line 193
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseCardTender;->appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->buyer_selected_account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    .line 196
    iget-boolean v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->wasTenderApprovedOffline:Z

    if-eqz v1, :cond_0

    .line 197
    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;->APPROVED_BY_READER_OFFLINE:Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->write_only_processing_type(Lcom/squareup/protos/client/bills/CardTender$Emv$ProcessingType;)Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    :cond_0
    return-object v0
.end method

.method public askForSignature()Z
    .locals 1

    .line 328
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/tender/SmartCardTender;->smartCardTenderShouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    return v0
.end method

.method public askForTip()Z
    .locals 1

    .line 150
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseCardTender;->askForTip()Z

    move-result v0

    return v0
.end method

.method public canAutoFlush()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canQuickCapture()Z
    .locals 4

    .line 248
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isAuthorizationSuccessful()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ON_FILE:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v3, 0x1

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v2, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->KEYED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    xor-int/2addr v0, v3

    const-string v2, "SmartCardTender::canQuickCapture card not present"

    .line 253
    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 256
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->getAutoCaptureSignatureThreshold()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 257
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->getAutoCaptureSignatureThreshold()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_4

    .line 259
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->hasSignature()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method public getARPC()[B
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_encrypted_emv_data:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public getApplicationId()Ljava/lang/String;
    .locals 2

    .line 365
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    const-string v1, "ApplicationId is received in auth response."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 367
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_chip_card_application_id:Ljava/lang/String;

    return-object v0

    .line 365
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No application on fallback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getApplicationPreferredName()Ljava/lang/String;
    .locals 2

    .line 371
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    const-string v1, "ApplicationName is received in auth response."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 373
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_application_preferred_name:Ljava/lang/String;

    return-object v0

    .line 371
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No application on fallback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAuthorization()Lcom/squareup/protos/client/bills/CardTender$Authorization;
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    return-object v0
.end method

.method public getBuyerSelectedAccountName()Ljava/lang/String;
    .locals 2

    .line 377
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    const-string v1, "AccountName is received in auth response."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 379
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->buyer_selected_account_name:Ljava/lang/String;

    return-object v0

    .line 377
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No account on fallback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCard()Lcom/squareup/Card;
    .locals 3

    .line 127
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    .line 135
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    iget-object v2, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 136
    invoke-static {v2}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    .line 137
    invoke-virtual {v1, v0}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v0

    return-object v0

    .line 130
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Smart cards(EMV and contactless) do not have Card info until Authorization is done."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCardholderName()Ljava/lang/String;
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->cardholderName:Ljava/lang/String;

    return-object v0
.end method

.method public getCardholderVerificationMethodUsed()Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    const-string v1, "CardholderVerificationMethodUsed is received in auth response."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 384
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->emv:Lcom/squareup/protos/client/bills/CardTender$Emv;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Emv;->read_only_cardholder_verification_method_used:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    return-object v0
.end method

.method protected getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;
    .locals 2

    .line 263
    invoke-super {p0}, Lcom/squareup/payment/tender/BaseCardTender;->getCompleteCardTender()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteCardTender;->newBuilder()Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 264
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->was_customer_present(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    move-result-object v0

    .line 265
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataCapture:[B

    invoke-static {v1}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CompleteCardTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteCardTender;

    move-result-object v0

    return-object v0
.end method

.method public getDeclinedMessageResources(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/squareup/cardreader/StandardMessageResources;->forMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method getMethod()Lcom/squareup/protos/client/bills/Tender$Method;
    .locals 4

    .line 164
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->SWIPED:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 168
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    .line 169
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->chip_card_fallback_indicator(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    .line 171
    invoke-virtual {v3}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/card/CardBrands;->toBillsBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    .line 172
    invoke-virtual {v3}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 173
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v2

    .line 167
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 174
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Emv;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv(Lcom/squareup/protos/client/bills/CardTender$Emv;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/SmartCardTender;->accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    .line 175
    invoke-static {v2}, Lcom/squareup/payment/tender/SmartCardTender;->tenderAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/protos/client/bills/CardTender$AccountType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->account_type(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 176
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    .line 166
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0

    .line 179
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/bills/CardTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/tender/SmartCardTender;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 182
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardTender$Card$Builder;

    move-result-object v2

    .line 183
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Card;

    move-result-object v2

    .line 181
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->appendEmvSection()Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;

    move-result-object v2

    .line 185
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Emv$Builder;->build()Lcom/squareup/protos/client/bills/CardTender$Emv;

    move-result-object v2

    .line 184
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->emv(Lcom/squareup/protos/client/bills/CardTender$Emv;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/SmartCardTender;->accountType:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    .line 186
    invoke-static {v2}, Lcom/squareup/payment/tender/SmartCardTender;->tenderAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/protos/client/bills/CardTender$AccountType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardTender$Builder;->account_type(Lcom/squareup/protos/client/bills/CardTender$AccountType;)Lcom/squareup/protos/client/bills/CardTender$Builder;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Builder;->build()Lcom/squareup/protos/client/bills/CardTender;

    move-result-object v1

    .line 180
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->card_tender(Lcom/squareup/protos/client/bills/CardTender;)Lcom/squareup/protos/client/bills/Tender$Method$Builder;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Method$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Method;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentInstrument()Lcom/squareup/protos/client/bills/PaymentInstrument;
    .locals 3

    .line 204
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isFallback()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackCard:Lcom/squareup/Card;

    new-instance v1, Lcom/squareup/payment/tender/SmartCardTender$1;

    invoke-direct {v1, p0}, Lcom/squareup/payment/tender/SmartCardTender$1;-><init>(Lcom/squareup/payment/tender/SmartCardTender;)V

    invoke-virtual {v0, v1}, Lcom/squareup/Card;->handleInputType(Lcom/squareup/Card$InputType$InputTypeHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/CardData;

    .line 238
    new-instance v1, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    return-object v0

    .line 241
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/tender/SmartCardTender;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 242
    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataAuth:[B

    invoke-static {v1, v2}, Lcom/squareup/payment/CardConverterUtils;->createCardDataFromBytes(Lcom/squareup/protos/client/bills/CardData$ReaderType;[B)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/PaymentInstrument$Builder;->build()Lcom/squareup/protos/client/bills/PaymentInstrument;

    move-result-object v0

    return-object v0
.end method

.method public getSmartCardDataAuth()[B
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataAuth:[B

    return-object v0
.end method

.method public getTenderTypeForLogging()Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    .line 123
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method

.method public hasSmartCardCaptureArgs()Z
    .locals 1

    .line 314
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataCapture:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isContactless()Z
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->CONTACTLESS:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isDip()Z
    .locals 1

    .line 357
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isContactless()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isFallback()Z
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->fallbackType:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->NONE:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public offlineCompatible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setPinBlocked()V
    .locals 4

    .line 388
    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_NOT_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/transaction/R$string;->emv_pin_blocked_title:I

    sget v3, Lcom/squareup/transaction/R$string;->emv_pin_blocked_msg:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    return-void
.end method

.method public setServerError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 303
    iget-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    if-eqz v0, :cond_0

    return-void

    .line 306
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    return-void
.end method

.method public setSignatureRequested()V
    .locals 1

    const/4 v0, 0x1

    .line 285
    iput-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->sigRequested:Z

    return-void
.end method

.method public setSmartCardCaptureArgs([B)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/payment/tender/SmartCardTender;->smartCardDataCapture:[B

    return-void
.end method

.method public setTenderOnFailure(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)V
    .locals 4

    .line 101
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender;->read_only_authorization:Lcom/squareup/protos/client/bills/CardTender$Authorization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/CardTender$Authorization;->decline_reason:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;->CHIP_CARD_INSERTION_REQUIRED:Lcom/squareup/protos/client/bills/CardTender$Authorization$DeclineReason;

    if-ne v0, v1, :cond_0

    .line 106
    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CHIP_CARD_USABLE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v2, Lcom/squareup/cardreader/R$string;->contactless_chip_card_insertion_required_title:I

    sget v3, Lcom/squareup/cardreader/R$string;->contactless_chip_card_insertion_required_description:I

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    goto :goto_0

    .line 113
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/StandardMessageResources$MessageResources;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->messageResources:Lcom/squareup/cardreader/StandardMessageResources$MessageResources;

    .line 118
    :goto_0
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    .line 119
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/payment/tender/BaseCardTender;->setTenderOnFailure(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 95
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    iput-object v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->responseCardTender:Lcom/squareup/protos/client/bills/CardTender;

    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/payment/tender/BaseCardTender;->setTenderOnSuccess(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V
    .locals 1

    .line 154
    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->usePreAuthTipping:Z

    if-nez v0, :cond_0

    .line 159
    invoke-super {p0, p1, p2}, Lcom/squareup/payment/tender/BaseCardTender;->setTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    return-void

    .line 155
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "SmartCardTender only supports tipping if usePreAuthTipping is false. Otherwise, tips are set on the Builder."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public supportsPaperSigAndTip()Z
    .locals 1

    .line 274
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->isContactless()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public useSeparateTippingScreen()Z
    .locals 1

    .line 144
    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->usePreAuthTipping:Z

    if-nez v0, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/payment/tender/SmartCardTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/payment/tender/SmartCardTender;->smartCardTenderShouldAskForSignature(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->useSeparateTippingScreen:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public wasTenderApprovedOffline()Z
    .locals 1

    .line 361
    iget-boolean v0, p0, Lcom/squareup/payment/tender/SmartCardTender;->wasTenderApprovedOffline:Z

    return v0
.end method
