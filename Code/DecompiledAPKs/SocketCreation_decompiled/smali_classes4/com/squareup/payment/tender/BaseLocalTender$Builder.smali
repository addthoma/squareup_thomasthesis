.class public abstract Lcom/squareup/payment/tender/BaseLocalTender$Builder;
.super Lcom/squareup/payment/tender/BaseTender$Builder;
.source "BaseLocalTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/BaseLocalTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field private final localTenderCache:Lcom/squareup/print/LocalTenderCache;


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/client/bills/Tender$Type;)V
    .locals 1

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, p1, p3, v0}, Lcom/squareup/payment/tender/BaseTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/bills/Tender$Type;Ljava/lang/String;)V

    .line 76
    iget-object p3, p1, Lcom/squareup/payment/tender/TenderFactory;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iput-object p3, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    .line 77
    iput-object p2, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    .line 78
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    iput-object p1, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->eventType:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)Lcom/squareup/print/LocalTenderCache;
    .locals 0

    .line 67
    iget-object p0, p0, Lcom/squareup/payment/tender/BaseLocalTender$Builder;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-object p0
.end method
