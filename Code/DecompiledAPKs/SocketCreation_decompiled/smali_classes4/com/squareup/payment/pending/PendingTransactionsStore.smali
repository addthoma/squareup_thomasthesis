.class public interface abstract Lcom/squareup/payment/pending/PendingTransactionsStore;
.super Ljava/lang/Object;
.source "PendingTransactionsStore.kt"

# interfaces
.implements Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0014\u0010\u0003\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004H&J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00050\u0004H&J\u001c\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010H&J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0004H&J\u0008\u0010\u0013\u001a\u00020\u0014H&J\u0014\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\u00050\u0004H&J\u0014\u0010\u0016\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00050\u0004H&J\u000e\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0004H&J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004H&J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004H&J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0004H&\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/payment/pending/PendingTransactionsStore;",
        "Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;",
        "Lmortar/Scoped;",
        "allPendingTransactionsAsBillHistory",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "allPendingTransactionsCount",
        "",
        "allTransactionSummaries",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "fetchTransaction",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "transactionId",
        "",
        "hasPendingTransactions",
        "",
        "oldestStoredTransactionOrPendingCaptureTransactionTimestamp",
        "",
        "onProcessedForwardedTransactionSummaries",
        "onProcessedForwardedTransactionsAsBillHistory",
        "processingOfflineTransactions",
        "ripenedAllPendingTransactionsCount",
        "smoothedAllPendingTransactionsCount",
        "smoothedRipenedAllPendingTransactionsCount",
        "payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract allPendingTransactionsAsBillHistory()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract allPendingTransactionsCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract allTransactionSummaries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract fetchTransaction(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract hasPendingTransactions()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract oldestStoredTransactionOrPendingCaptureTransactionTimestamp()J
.end method

.method public abstract onProcessedForwardedTransactionSummaries()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract onProcessedForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract processingOfflineTransactions()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract ripenedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract smoothedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract smoothedRipenedAllPendingTransactionsCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
