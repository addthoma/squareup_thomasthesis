.class public final Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;
.super Ljava/lang/Object;
.source "PendingPaymentsNotificationsSource_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final localNotificationAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationStateStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->resProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->localNotificationAnalyticsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->notificationStateStoreProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;)",
            "Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/payment/pending/PendingTransactionsStore;)Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;-><init>(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/payment/pending/PendingTransactionsStore;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/time/CurrentTime;

    iget-object v2, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->localNotificationAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iget-object v3, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->notificationStateStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notificationcenterdata/NotificationStateStore;

    iget-object v4, p0, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/payment/pending/PendingTransactionsStore;)Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource_Factory;->get()Lcom/squareup/payment/pending/PendingPaymentsNotificationsSource;

    move-result-object v0

    return-object v0
.end method
