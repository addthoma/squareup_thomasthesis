.class public Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;
.super Lcom/squareup/backgroundjob/ExecutorBackgroundJob;
.source "PaymentNotifierJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/pending/PaymentNotifierJobCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdatePaymentNotifierJob"
.end annotation


# static fields
.field private static final DEBUG_NOTIFICATION_TEXT_FORMAT_STRING:Ljava/lang/String; = "Payment notification job scheduled to be run in ~%d min"

.field private static final DEBUG_NOTIFICATION_TITLE:Ljava/lang/String; = "Debug: Payment Notifier"

.field public static final QUALIFIED_NAME:Ljava/lang/String;

.field public static final UPDATE_TAG:Ljava/lang/String;

.field private static final WAKE_LOCK_TAG:Ljava/lang/String;


# instance fields
.field private final paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 54
    const-class v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->QUALIFIED_NAME:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":update.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->UPDATE_TAG:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":wake.lock.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/payment/pending/PaymentNotifier;)V
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;)V

    .line 67
    iput-object p4, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;

    return-void
.end method

.method public static updatePaymentNotifierRequest(JJ)Lcom/evernote/android/job/JobRequest;
    .locals 4

    .line 83
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->UPDATE_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p0

    const-wide/16 v2, 0x3c

    div-long/2addr p0, v2

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 90
    new-instance p1, Lcom/evernote/android/job/JobRequest$Builder;

    invoke-direct {p1, p0}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 96
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-virtual {p1, p2, p3, v1, v2}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 108
    invoke-virtual {p1, v1}, Lcom/evernote/android/job/JobRequest$Builder;->setUpdateCurrent(Z)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    .line 109
    invoke-virtual {p1, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p1

    .line 111
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 112
    invoke-virtual {v2, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const/4 p3, 0x0

    aput-object p2, v1, p3

    const-string p2, "Payment notification job scheduled to be run in ~%d min"

    invoke-static {v0, p2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 113
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result p0

    const-string p3, "Debug: Payment Notifier"

    invoke-static {p1, p0, p3, p2}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;ILjava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 0

    .line 119
    invoke-static {}, Lcom/squareup/AppDelegate$Locator;->getAppDelegate()Lcom/squareup/AppDelegate;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/AppDelegate;->isLoggedIn()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 120
    iget-object p1, p0, Lcom/squareup/payment/pending/PaymentNotifierJobCreator$UpdatePaymentNotifierJob;->paymentNotifier:Lcom/squareup/payment/pending/PaymentNotifier;

    invoke-virtual {p1}, Lcom/squareup/payment/pending/PaymentNotifier;->updateNotification()V

    .line 122
    :cond_0
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
