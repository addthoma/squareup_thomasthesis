.class final Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;
.super Ljava/lang/Object;
.source "RealPendingTransactionsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore;-><init>(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/queue/CaptureTask;",
        "storedTransactionOptional",
        "Lcom/squareup/payment/offline/StoredPayment;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;

    invoke-direct {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;-><init>()V

    sput-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/queue/CaptureTask;",
            ">;"
        }
    .end annotation

    const-string v0, "storedTransactionOptional"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1$1;->INSTANCE:Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$oldestStoredTransactionOrPendingCaptureTransaction$1;->apply(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
