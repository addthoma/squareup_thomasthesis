.class public Lcom/squareup/payment/CrmBillPaymentListener;
.super Ljava/lang/Object;
.source "CrmBillPaymentListener.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

.field private final loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/BillPaymentEvents;Lcom/squareup/loyalty/LoyaltyEventPublisher;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/payment/CrmBillPaymentListener;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    .line 33
    iput-object p2, p0, Lcom/squareup/payment/CrmBillPaymentListener;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    .line 34
    iput-object p3, p0, Lcom/squareup/payment/CrmBillPaymentListener;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method private static getFirstUniqueCoupon(Lcom/squareup/payment/BillPayment;Ljava/util/List;)Lcom/squareup/protos/client/coupons/Coupon;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/BillPayment;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/Coupon;"
        }
    .end annotation

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->getOrder()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p0

    .line 65
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSnapshot;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object p0

    .line 66
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    .line 64
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 67
    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 70
    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    iget-object v1, v1, Lcom/squareup/api/items/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object p1

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$2(Lcom/squareup/protos/client/bills/AddedTender;Lcom/squareup/payment/BillPayment;)V
    .locals 1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->isSingleTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object p0, p0, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    invoke-static {p1, p0}, Lcom/squareup/payment/CrmBillPaymentListener;->getFirstUniqueCoupon(Lcom/squareup/payment/BillPayment;Ljava/util/List;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 53
    invoke-virtual {p1, p0}, Lcom/squareup/payment/BillPayment;->setAvailableCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$CrmBillPaymentListener(Lcom/squareup/protos/client/bills/AddedTender;)Ljava/lang/Boolean;
    .locals 1

    .line 45
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender;->coupon:Ljava/util/List;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/payment/CrmBillPaymentListener;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 47
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseRewards()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 45
    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$1$CrmBillPaymentListener(Lcom/squareup/payment/BillPayment;)Lrx/Observable;
    .locals 2

    .line 44
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->onAddTendersResponse()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$593b-BEketO9oGJz4ymb9vJejiI;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$593b-BEketO9oGJz4ymb9vJejiI;-><init>(Lcom/squareup/payment/CrmBillPaymentListener;)V

    .line 45
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 48
    invoke-static {p1}, Lcom/squareup/util/RxTuples;->pairWithSecond(Ljava/lang/Object;)Lrx/functions/Func1;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/payment/CrmBillPaymentListener;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPaymentEvents;->onAddedTenderEvent()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/-$$Lambda$y6qVq5O9T_zKgJ3pzoEyPQLDG14;->INSTANCE:Lcom/squareup/payment/-$$Lambda$y6qVq5O9T_zKgJ3pzoEyPQLDG14;

    .line 40
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/CrmBillPaymentListener;->loyaltyEventPublisher:Lcom/squareup/loyalty/LoyaltyEventPublisher;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/payment/-$$Lambda$vWcOatGfCWiW786TQV961_btc9s;

    invoke-direct {v2, v1}, Lcom/squareup/payment/-$$Lambda$vWcOatGfCWiW786TQV961_btc9s;-><init>(Lcom/squareup/loyalty/LoyaltyEventPublisher;)V

    .line 41
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 38
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/payment/CrmBillPaymentListener;->billPaymentEvents:Lcom/squareup/payment/BillPaymentEvents;

    invoke-virtual {v0}, Lcom/squareup/payment/BillPaymentEvents;->onBillPaymentCreated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$Ds1Wt3u9xaDbDEOw6wbLT6_aUOY;

    invoke-direct {v1, p0}, Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$Ds1Wt3u9xaDbDEOw6wbLT6_aUOY;-><init>(Lcom/squareup/payment/CrmBillPaymentListener;)V

    .line 44
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$VVPUN3KNHT9C8Sdp4rkh_qGL4is;->INSTANCE:Lcom/squareup/payment/-$$Lambda$CrmBillPaymentListener$VVPUN3KNHT9C8Sdp4rkh_qGL4is;

    .line 49
    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
