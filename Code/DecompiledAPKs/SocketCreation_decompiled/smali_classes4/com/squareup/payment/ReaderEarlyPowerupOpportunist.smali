.class public Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;
.super Ljava/lang/Object;
.source "ReaderEarlyPowerupOpportunist.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;


# static fields
.field private static final CAN_RESEND_HINT_AFTER_MILLIS:I = 0x1388

.field static final READER_POWERUP_TIMEOUT_SEC:I = 0xf


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final clock:Lcom/squareup/util/Clock;

.field private everythingChanged:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private lastHintSentTime:J


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1388

    .line 34
    iput-wide v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->lastHintSentTime:J

    const/4 v0, 0x1

    .line 35
    iput-boolean v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->everythingChanged:Z

    .line 39
    iput-object p1, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 40
    iput-object p2, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->clock:Lcom/squareup/util/Clock;

    .line 41
    iput-object p3, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private canSendHintNow()Z
    .locals 6

    .line 80
    iget-object v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->R12_EARLY_K400_POWERUP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->lastHintSentTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1388

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->lastHintSentTime:J

    const/4 v0, 0x1

    return v0

    :cond_1
    return v1
.end method


# virtual methods
.method onCartChanged(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)V
    .locals 7

    .line 54
    invoke-virtual {p1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->everythingChanged()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 55
    iput-boolean v1, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->everythingChanged:Z

    return-void

    .line 60
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;->itemsChanged:Z

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->canSendHintNow()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-boolean p1, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->everythingChanged:Z

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    .line 61
    iput-boolean p1, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->everythingChanged:Z

    .line 63
    iget-object v0, p0, Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReader;

    .line 64
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v3

    .line 66
    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->isCapabilitiesSupported()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 68
    invoke-virtual {v3}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v3

    aput-object v3, v4, p1

    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v3

    aput-object v3, v4, v1

    const/4 v3, 0x2

    const/16 v5, 0xf

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v3

    const-string v3, "Sending powerup hint to %s reader %s (timeout = %ds)"

    .line 67
    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-interface {v2, v5}, Lcom/squareup/cardreader/CardReader;->sendPowerupHint(I)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 45
    const-class v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/payment/-$$Lambda$5J23B8mrPZIs2K_MvOHTv-l04to;

    invoke-direct {v0, p0}, Lcom/squareup/payment/-$$Lambda$5J23B8mrPZIs2K_MvOHTv-l04to;-><init>(Lcom/squareup/payment/ReaderEarlyPowerupOpportunist;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
