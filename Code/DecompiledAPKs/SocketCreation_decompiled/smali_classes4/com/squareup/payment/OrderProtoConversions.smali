.class public final Lcom/squareup/payment/OrderProtoConversions;
.super Ljava/lang/Object;
.source "OrderProtoConversions.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderProtoConversions.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderProtoConversions.kt\ncom/squareup/payment/OrderProtoConversions\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n*L\n1#1,425:1\n1360#2:426\n1429#2,3:427\n1642#2,2:430\n1642#2,2:432\n1642#2:441\n704#2:442\n777#2,2:443\n1642#2,2:445\n1643#2:447\n501#3:434\n486#3,6:435\n*E\n*S KotlinDebug\n*F\n+ 1 OrderProtoConversions.kt\ncom/squareup/payment/OrderProtoConversions\n*L\n358#1:426\n358#1,3:427\n383#1,2:430\n397#1,2:432\n416#1:441\n416#1:442\n416#1,2:443\n416#1,2:445\n416#1:447\n402#1:434\n402#1,6:435\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010%\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a6\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00080\u0001H\u0007\u001a\"\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00080\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0007\u001av\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00140\u00012\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00052\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\u0008\u0008\u0002\u0010\u001e\u001a\u00020\u001cH\u0007\u001a&\u0010\u001f\u001a\u00020\u000b2\u0006\u0010 \u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c\u001a\"\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00140\"2\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0007\u001a:\u0010#\u001a\u00020\u000b2\u0006\u0010$\u001a\u00020%2\u0008\u0010&\u001a\u0004\u0018\u00010\'2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010(\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001cH\u0002\u001at\u0010)\u001a\u00020\u000b2\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010(\u001a\u00020\u001c2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00022\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00140\u00012\u0006\u0010\u001d\u001a\u00020\u001c2\u001c\u0008\u0002\u0010+\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010,j\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`-H\u0007\u001a.\u0010.\u001a\u00020\u000b2\u0006\u0010/\u001a\u0002002\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010(\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001c\u001a.\u00101\u001a\u00020\u000b2\u0006\u00102\u001a\u0002032\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010(\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001c\u001ar\u00104\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005*\u0002052\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00140\u00012\u0006\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\u000c\u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u00052\u001c\u0008\u0002\u0010+\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010,j\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`-H\u0002\u00a8\u00068"
    }
    d2 = {
        "deriveAddedCouponsAndCartScopeDiscountEvents",
        "",
        "",
        "Lcom/squareup/protos/client/bills/Itemization$Event;",
        "loadedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "derivedAddedCouponsAndCartScopeDiscounts",
        "Lcom/squareup/checkout/Discount;",
        "deriveAddedCouponsAndCartScopeDiscounts",
        "forTicketFromCartProto",
        "Lcom/squareup/payment/Order;",
        "ticket",
        "Lcom/squareup/tickets/OpenTicket;",
        "userId",
        "currency",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "roundingType",
        "Lcom/squareup/calc/constants/RoundingType;",
        "cartAvailableTaxes",
        "Lcom/squareup/checkout/Tax;",
        "cartAvailableTaxRules",
        "Lcom/squareup/payment/OrderTaxRule;",
        "defaultDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "res",
        "Lcom/squareup/util/Res;",
        "readVoidedItems",
        "",
        "discountApplicationIdEnabled",
        "defaultLockConfiguration",
        "forTicketFromOrder",
        "order",
        "getTaxesFromCartItems",
        "",
        "makeOrderFromCartAndInvoiceContact",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "payer",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "lockConfiguration",
        "makeOrderFromCartProto",
        "allAvailableTaxes",
        "cartItemSortComparator",
        "Ljava/util/Comparator;",
        "Lkotlin/Comparator;",
        "makeOrderFromEstimateProto",
        "estimate",
        "Lcom/squareup/protos/client/estimate/Estimate;",
        "makeOrderFromInvoiceProto",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "toCartItems",
        "Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "seats",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "checkout_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final deriveAddedCouponsAndCartScopeDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation

    const-string v0, "loadedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "derivedAddedCouponsAndCartScopeDiscounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 415
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 417
    check-cast p0, Ljava/lang/Iterable;

    .line 441
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 418
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    const-string v2, "item.events"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 442
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 443
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 420
    iget-object v5, v4, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v6, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DISCOUNT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v5, v6, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 444
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 445
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 421
    move-object v3, v0

    check-cast v3, Ljava/util/Map;

    iget-object v4, v2, Lcom/squareup/protos/client/bills/Itemization$Event;->reason:Ljava/lang/String;

    const-string v5, "event.reason"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "event"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 416
    :cond_4
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method public static final deriveAddedCouponsAndCartScopeDiscounts(Ljava/util/List;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    const-string v0, "loadedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 396
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 398
    check-cast p0, Ljava/lang/Iterable;

    .line 432
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 399
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    const-string v3, "item.appliedDiscounts"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 397
    :cond_0
    check-cast v0, Ljava/util/Map;

    .line 434
    new-instance p0, Ljava/util/LinkedHashMap;

    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p0, Ljava/util/Map;

    .line 435
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 436
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 403
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v3

    sget-object v4, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    if-eq v3, v4, :cond_3

    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_1

    .line 437
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    return-object p0
.end method

.method public static final forTicketFromCartProto(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZ)Lcom/squareup/payment/Order;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            "Lcom/squareup/util/Res;",
            "ZZ)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    const/4 v10, 0x0

    const/16 v11, 0x400

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-static/range {v0 .. v12}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromCartProto$default(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZILjava/lang/Object;)Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public static final forTicketFromCartProto(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZ)Lcom/squareup/payment/Order;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            "Lcom/squareup/util/Res;",
            "ZZZ)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    const-string v0, "ticket"

    move-object/from16 v12, p0

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "userId"

    move-object/from16 v13, p1

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    move-object/from16 v14, p2

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingType"

    move-object/from16 v15, p3

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartAvailableTaxes"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cartAvailableTaxRules"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v9

    .line 79
    iget-object v0, v9, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v0

    :goto_0
    move-object v8, v0

    if-eqz v8, :cond_1

    .line 81
    iget-object v0, v8, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object/from16 v7, p6

    :goto_1
    const-string v0, "lineItems"

    .line 84
    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v2, v9, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v0, v2}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v6

    const/16 v16, 0x0

    const/16 v17, 0x40

    const/16 v18, 0x0

    move-object v0, v8

    move-object/from16 v1, p7

    move/from16 v2, p8

    move-object/from16 v3, p4

    move/from16 v4, p10

    move/from16 v5, p9

    move-object/from16 v19, v7

    move-object/from16 v7, v16

    move-object/from16 v20, v8

    move/from16 v8, v17

    move-object v12, v9

    move-object/from16 v9, v18

    .line 84
    invoke-static/range {v0 .. v9}, Lcom/squareup/payment/OrderProtoConversions;->toCartItems$default(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/util/Res;ZLjava/util/Map;ZZLjava/util/List;Ljava/util/Comparator;ILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 93
    invoke-static {v0}, Lcom/squareup/payment/OrderProtoConversions;->getTaxesFromCartItems(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    .line 94
    invoke-interface {v1, v10}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 96
    sget-object v2, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    move-object/from16 v3, v20

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    const-string v4, "lineItems.surcharge"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/Surcharge$Companion;->fromProto(Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    .line 98
    iget-object v3, v12, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    goto :goto_2

    :cond_2
    move-object v3, v4

    :goto_2
    if-eqz v3, :cond_3

    .line 99
    iget-object v5, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->available_instrument_details:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object v5, v4

    .line 100
    :goto_3
    iget-object v6, v12, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v6, :cond_4

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    goto :goto_4

    :cond_4
    move-object v6, v4

    .line 101
    :goto_4
    iget-object v7, v12, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v7, :cond_5

    iget-object v4, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    .line 104
    :cond_5
    iget-object v7, v12, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v7, :cond_6

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    if-eqz v7, :cond_6

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    if-eqz v7, :cond_6

    goto :goto_5

    :cond_6
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    check-cast v7, Ljava/util/List;

    .line 105
    :goto_5
    check-cast v7, Ljava/lang/Iterable;

    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v7

    .line 106
    invoke-static {v0}, Lcom/squareup/payment/OrderProtoConversions;->deriveAddedCouponsAndCartScopeDiscounts(Ljava/util/List;)Ljava/util/Map;

    move-result-object v8

    .line 108
    invoke-static/range {p1 .. p3}, Lcom/squareup/payment/Order$Builder;->forPayment(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v9

    .line 109
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/squareup/payment/Order$Builder;->ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;

    move-result-object v9

    .line 110
    move-object v10, v0

    check-cast v10, Ljava/util/Collection;

    invoke-virtual {v9, v10}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object v9

    .line 111
    invoke-virtual {v9, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 112
    invoke-virtual {v1, v11}, Lcom/squareup/payment/Order$Builder;->availableTaxRules(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 113
    invoke-virtual {v1, v8}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 115
    invoke-static {v0, v8}, Lcom/squareup/payment/OrderProtoConversions;->deriveAddedCouponsAndCartScopeDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 114
    invoke-virtual {v1, v0}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 116
    invoke-virtual {v0, v7}, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 117
    invoke-virtual {v0, v2}, Lcom/squareup/payment/Order$Builder;->surcharges(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 119
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->openTicketNote(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 120
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->predefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getProductsOpenedOn()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->productsOpenTicketOpenedOn(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 122
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getEmployeeFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeFirstName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/tickets/OpenTicket;->getEmployeeLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeLastName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0, v3}, Lcom/squareup/payment/Order$Builder;->customerSummary(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0, v5}, Lcom/squareup/payment/Order$Builder;->customerInstruments(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    move-object/from16 v1, v19

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->diningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 128
    invoke-virtual {v0, v6}, Lcom/squareup/payment/Order$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v4}, Lcom/squareup/payment/Order$Builder;->coursing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    move/from16 v1, p9

    .line 130
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string v1, "Order.Builder.forPayment\u2026IdEnabled)\n      .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic forTicketFromCartProto$default(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZILjava/lang/Object;)Lcom/squareup/payment/Order;
    .locals 12

    move/from16 v0, p11

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v11, 0x1

    goto :goto_0

    :cond_0
    move/from16 v11, p10

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    .line 75
    invoke-static/range {v1 .. v11}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromCartProto(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZ)Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public static final forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;
    .locals 15

    move-object v0, p0

    const-string v1, "order"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "ticket"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v9, p2

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getUserId()Ljava/lang/String;

    move-result-object v3

    const-string v1, "order.userId"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v1

    const-string v4, "order.cart"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    const-string v1, "order.cart.currencyCode"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v5, v0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    const-string v1, "order.roundingType"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxes()Ljava/util/Map;

    move-result-object v6

    const-string v1, "order.getAvailableTaxes()"

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v7

    const-string v1, "order.availableTaxRules"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDiscountApplicationIdEnabled()Z

    move-result v11

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x400

    const/4 v14, 0x0

    move/from16 v10, p3

    .line 42
    invoke-static/range {v2 .. v14}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromCartProto$default(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;Ljava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;Lcom/squareup/util/Res;ZZZILjava/lang/Object;)Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public static final getTaxesFromCartItems(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    const-string v0, "loadedItems"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 382
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 384
    check-cast p0, Ljava/lang/Iterable;

    .line 430
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 385
    move-object v2, v0

    check-cast v2, Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    const-string v3, "item.appliedTaxes"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 383
    :cond_0
    check-cast v0, Ljava/util/Map;

    return-object v0
.end method

.method private static final makeOrderFromCartAndInvoiceContact(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/invoice/InvoiceContact;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;
    .locals 13

    move-object v0, p1

    .line 197
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    move-object v8, v1

    check-cast v8, Ljava/util/Map;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x100

    const/4 v12, 0x0

    move-object v2, p0

    move-object v3, p2

    move-object/from16 v4, p4

    move/from16 v6, p3

    move/from16 v9, p5

    .line 190
    invoke-static/range {v2 .. v12}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartProto$default(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;ZLjava/util/Comparator;ILjava/lang/Object;)Lcom/squareup/payment/Order;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 203
    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    if-nez v3, :cond_0

    goto :goto_0

    .line 209
    :cond_0
    new-instance v3, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 210
    iget-object v4, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v3

    .line 211
    iget-object v4, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v3

    .line 213
    new-instance v4, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;-><init>()V

    .line 214
    iget-object v5, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v4

    .line 215
    iget-object v5, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v4

    .line 216
    iget-object v5, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_company_name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->company_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v4

    .line 217
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_phone_number:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/CustomerProfile$Builder;->build()Lcom/squareup/protos/client/rolodex/CustomerProfile;

    move-result-object v0

    .line 212
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->profile(Lcom/squareup/protos/client/rolodex/CustomerProfile;)Lcom/squareup/protos/client/rolodex/Contact$Builder;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 221
    invoke-virtual {v1, v0, v2, v2}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    goto :goto_1

    .line 207
    :cond_1
    :goto_0
    invoke-virtual {v1, v2, v2, v2}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    :goto_1
    return-object v1
.end method

.method public static final makeOrderFromCartProto(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;Z)Lcom/squareup/payment/Order;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;Z)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    const/4 v8, 0x0

    const/16 v9, 0x100

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    invoke-static/range {v0 .. v10}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartProto$default(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;ZLjava/util/Comparator;ILjava/lang/Object;)Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public static final makeOrderFromCartProto(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;ZLjava/util/Comparator;)Lcom/squareup/payment/Order;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "ZZ",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;Z",
            "Ljava/util/Comparator<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/payment/Order;"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    const-string v4, "cart"

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "roundingType"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "res"

    move-object/from16 v6, p2

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "allAvailableTaxes"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v4, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 249
    iget-object v5, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_0
    new-instance v5, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v5

    :goto_0
    move-object v13, v5

    const-string v5, "lineItems"

    .line 251
    invoke-static {v13, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v8

    .line 257
    sget-object v5, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v5, v7}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v11

    move-object v5, v13

    move-object/from16 v6, p2

    move/from16 v7, p3

    move/from16 v9, p4

    move/from16 v10, p7

    move-object/from16 v12, p8

    .line 251
    invoke-static/range {v5 .. v12}, Lcom/squareup/payment/OrderProtoConversions;->toCartItems(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/util/Res;ZLjava/util/Map;ZZLjava/util/List;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v5

    .line 261
    invoke-static {v5}, Lcom/squareup/payment/OrderProtoConversions;->getTaxesFromCartItems(Ljava/util/List;)Ljava/util/Map;

    move-result-object v6

    .line 262
    invoke-interface {v6, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 264
    sget-object v3, Lcom/squareup/checkout/Surcharge;->Companion:Lcom/squareup/checkout/Surcharge$Companion;

    iget-object v7, v13, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    const-string v8, "lineItems.surcharge"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lcom/squareup/checkout/Surcharge$Companion;->fromProto(Ljava/util/List;)Ljava/util/Map;

    move-result-object v3

    .line 266
    iget-object v7, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    const/4 v8, 0x0

    if-eqz v7, :cond_1

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->buyer_info:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    goto :goto_1

    :cond_1
    move-object v7, v8

    .line 267
    :goto_1
    iget-object v9, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v9, :cond_2

    iget-object v9, v9, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->available_instrument_details:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v9, v8

    .line 268
    :goto_2
    iget-object v10, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v10, :cond_3

    iget-object v10, v10, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    goto :goto_3

    :cond_3
    move-object v10, v8

    .line 269
    :goto_3
    iget-object v11, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v11, :cond_4

    iget-object v11, v11, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->coursing_options:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    goto :goto_4

    :cond_4
    move-object v11, v8

    .line 270
    :goto_4
    iget-object v12, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v12, :cond_5

    iget-object v8, v12, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->appointments_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 273
    :cond_5
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->pricing_engine_state:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;->blacklisted_discount_ids:Ljava/util/List;

    if-eqz v0, :cond_6

    goto :goto_5

    .line 274
    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 275
    :goto_5
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    if-nez v2, :cond_7

    .line 280
    new-instance v2, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v2}, Lcom/squareup/payment/Order$Builder;-><init>()V

    .line 281
    invoke-virtual {v2, v4}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object v2

    .line 282
    invoke-virtual {v2, p1}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    const-string v2, "Order.Builder()\n        \u2026oundingType(roundingType)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_6

    .line 284
    :cond_7
    invoke-static {v2, v4, p1}, Lcom/squareup/payment/Order$Builder;->forPayment(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    const-string v2, "Order.Builder.forPayment\u2026, currency, roundingType)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    :goto_6
    invoke-static {v5}, Lcom/squareup/payment/OrderProtoConversions;->deriveAddedCouponsAndCartScopeDiscounts(Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    .line 288
    move-object v4, v5

    check-cast v4, Ljava/util/Collection;

    invoke-virtual {v1, v4}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 289
    invoke-virtual {v1, v6}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 290
    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 292
    invoke-static {v5, v2}, Lcom/squareup/payment/OrderProtoConversions;->deriveAddedCouponsAndCartScopeDiscountEvents(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    .line 291
    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 293
    invoke-virtual {v1, v3}, Lcom/squareup/payment/Order$Builder;->surcharges(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 294
    invoke-virtual {v1, v7}, Lcom/squareup/payment/Order$Builder;->customerSummary(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 295
    invoke-virtual {v1, v9}, Lcom/squareup/payment/Order$Builder;->customerInstruments(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 296
    invoke-virtual {v1, v10}, Lcom/squareup/payment/Order$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 297
    invoke-virtual {v1, v11}, Lcom/squareup/payment/Order$Builder;->coursing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 298
    invoke-virtual {v1, v8}, Lcom/squareup/payment/Order$Builder;->appointmentDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    move/from16 v2, p7

    .line 299
    invoke-virtual {v1, v2}, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;

    move-result-object v1

    .line 300
    invoke-virtual {v1, v0}, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 301
    invoke-virtual {v0}, Lcom/squareup/payment/Order$Builder;->build()Lcom/squareup/payment/Order;

    move-result-object v0

    const-string v1, "builder\n      .items(ite\u2026Discounts)\n      .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic makeOrderFromCartProto$default(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;ZLjava/util/Comparator;ILjava/lang/Object;)Lcom/squareup/payment/Order;
    .locals 10

    move/from16 v0, p9

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    move-object v9, v0

    goto :goto_0

    :cond_0
    move-object/from16 v9, p8

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-static/range {v1 .. v9}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartProto(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;ZZLjava/lang/String;Ljava/util/Map;ZLjava/util/Comparator;)Lcom/squareup/payment/Order;

    move-result-object v0

    return-object v0
.end method

.method public static final makeOrderFromEstimateProto(Lcom/squareup/protos/client/estimate/Estimate;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;
    .locals 7

    const-string v0, "estimate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/Estimate;->cart:Lcom/squareup/protos/client/bills/Cart;

    const-string v0, "estimate.cart"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v2, p0, Lcom/squareup/protos/client/estimate/Estimate;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    .line 172
    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartAndInvoiceContact(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/invoice/InvoiceContact;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p0

    return-object p0
.end method

.method public static final makeOrderFromInvoiceProto(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;
    .locals 7

    const-string v0, "invoice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    const-string v0, "invoice.cart"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v2, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    .line 148
    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromCartAndInvoiceContact(Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/protos/client/invoice/InvoiceContact;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p0

    return-object p0
.end method

.method private static final toCartItems(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/util/Res;ZLjava/util/Map;ZZLjava/util/List;Ljava/util/Comparator;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            "Lcom/squareup/util/Res;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lcom/squareup/checkout/Tax;",
            ">;ZZ",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;",
            "Ljava/util/Comparator<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p7

    .line 317
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 320
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    if-eqz v3, :cond_3

    .line 321
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move/from16 v4, p4

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, Lcom/squareup/protos/client/bills/Itemization;

    const/4 v5, 0x0

    if-eqz v7, :cond_1

    .line 323
    iget-object v6, v7, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    :goto_1
    if-nez v6, :cond_0

    .line 332
    iget-object v6, v7, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Itemization$Configuration;->backing_type:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v8, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v6, v8, :cond_2

    iget-object v6, v7, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item:Lcom/squareup/api/items/Item;

    iget-object v6, v6, Lcom/squareup/api/items/Item;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v8, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-eq v6, v8, :cond_2

    const/4 v4, 0x0

    .line 337
    :cond_2
    move-object v5, v2

    check-cast v5, Ljava/util/Collection;

    new-instance v6, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v6}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 340
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v8

    .line 342
    sget-object v9, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    move-object v10, v9

    check-cast v10, Lcom/squareup/checkout/OrderVariationNamer;

    move-object v9, p1

    move/from16 v11, p5

    move-object/from16 v12, p6

    .line 338
    invoke-virtual/range {v6 .. v12}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Ljava/util/Map;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 346
    invoke-virtual {v6, v4}, Lcom/squareup/checkout/CartItem$Builder;->lockConfiguration(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 347
    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v6

    .line 337
    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move/from16 v4, p4

    :cond_4
    if-eqz p2, :cond_6

    .line 354
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-eqz v3, :cond_6

    .line 358
    move-object v3, v2

    check-cast v3, Ljava/util/Collection;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    const-string/jumbo v5, "void_itemization"

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 426
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v0, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 427
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 428
    move-object v8, v6

    check-cast v8, Lcom/squareup/protos/client/bills/Itemization;

    .line 359
    new-instance v7, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v7}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    .line 364
    sget-object v6, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    move-object v11, v6

    check-cast v11, Lcom/squareup/checkout/OrderVariationNamer;

    move-object/from16 v9, p3

    move-object v10, p1

    move/from16 v12, p5

    move-object/from16 v13, p6

    .line 360
    invoke-virtual/range {v7 .. v13}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Ljava/util/Map;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 368
    invoke-virtual {v6, v4}, Lcom/squareup/checkout/CartItem$Builder;->lockConfiguration(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    const/4 v7, 0x1

    .line 369
    invoke-virtual {v6, v7}, Lcom/squareup/checkout/CartItem$Builder;->isVoided(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v6

    .line 370
    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 429
    :cond_5
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 358
    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    :cond_6
    if-eqz v1, :cond_7

    .line 375
    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->sortWith(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_7
    return-object v2
.end method

.method static synthetic toCartItems$default(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/util/Res;ZLjava/util/Map;ZZLjava/util/List;Ljava/util/Comparator;ILjava/lang/Object;)Ljava/util/List;
    .locals 9

    and-int/lit8 v0, p8, 0x40

    if-eqz v0, :cond_0

    .line 315
    sget-object v0, Lcom/squareup/payment/DefaultCartSort;->Comparator:Ljava/util/Comparator;

    move-object v8, v0

    goto :goto_0

    :cond_0
    move-object/from16 v8, p7

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-static/range {v1 .. v8}, Lcom/squareup/payment/OrderProtoConversions;->toCartItems(Lcom/squareup/protos/client/bills/Cart$LineItems;Lcom/squareup/util/Res;ZLjava/util/Map;ZZLjava/util/List;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
