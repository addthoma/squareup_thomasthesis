.class public Lcom/squareup/payment/offline/AuthorizationRequest;
.super Ljava/lang/Object;
.source "AuthorizationRequest.java"


# instance fields
.field public final amount_cents:J

.field public final currency_code:Ljava/lang/String;

.field public final description:Ljava/lang/String;

.field public final encrypted_card_data:Ljava/lang/String;

.field public final encrypted_track_2:Ljava/lang/String;

.field public final to:Ljava/lang/String;

.field public final track_2:Ljava/lang/String;

.field public final unique_key:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->unique_key:Ljava/lang/String;

    .line 18
    iput-wide p2, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->amount_cents:J

    .line 19
    iput-object p4, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->currency_code:Ljava/lang/String;

    .line 20
    iput-object p5, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->track_2:Ljava/lang/String;

    .line 21
    iput-object p6, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->encrypted_track_2:Ljava/lang/String;

    .line 22
    iput-object p7, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->encrypted_card_data:Ljava/lang/String;

    .line 23
    iput-object p8, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->description:Ljava/lang/String;

    .line 24
    iput-object p9, p0, Lcom/squareup/payment/offline/AuthorizationRequest;->to:Ljava/lang/String;

    return-void
.end method

.method public static fromEncryptedCardData(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/payment/offline/AuthorizationRequest;
    .locals 11

    .line 41
    new-instance v10, Lcom/squareup/payment/offline/AuthorizationRequest;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v7, p4

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/offline/AuthorizationRequest;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v10
.end method

.method public static fromEncryptedTrack2(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/payment/offline/AuthorizationRequest;
    .locals 11

    .line 35
    new-instance v10, Lcom/squareup/payment/offline/AuthorizationRequest;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, p4

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/offline/AuthorizationRequest;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v10
.end method

.method public static fromTrack2(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/payment/offline/AuthorizationRequest;
    .locals 11

    .line 29
    new-instance v10, Lcom/squareup/payment/offline/AuthorizationRequest;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/squareup/payment/offline/AuthorizationRequest;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v10
.end method
