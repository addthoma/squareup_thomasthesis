.class Lcom/squareup/payment/offline/ForwardedPaymentManager$1;
.super Ljava/lang/Object;
.source "ForwardedPaymentManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/ForwardedPaymentManager;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/badbus/BadBus;Lcom/squareup/server/bills/StoreAndForwardBillService;Lcom/squareup/settings/LocalSetting;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

.field final synthetic val$mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    iput-object p2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;->val$mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 108
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-virtual {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->updateStatus()V

    .line 109
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;->val$mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$1;->this$0:Lcom/squareup/payment/offline/ForwardedPaymentManager;

    invoke-static {v1}, Lcom/squareup/payment/offline/ForwardedPaymentManager;->access$000(Lcom/squareup/payment/offline/ForwardedPaymentManager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
