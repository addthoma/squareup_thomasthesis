.class interface abstract Lcom/squareup/payment/offline/ForwardedPayment$Helper;
.super Ljava/lang/Object;
.source "ForwardedPayment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/ForwardedPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "Helper"
.end annotation


# virtual methods
.method public abstract asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
.end method

.method public abstract getBillId()Lcom/squareup/billhistory/model/BillHistoryId;
.end method

.method public abstract getUniqueKey()Ljava/lang/String;
.end method

.method public abstract isPending()Z
.end method

.method public abstract withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;
.end method

.method public abstract withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;
.end method
