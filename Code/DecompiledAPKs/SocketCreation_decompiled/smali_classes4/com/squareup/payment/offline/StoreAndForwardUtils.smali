.class public Lcom/squareup/payment/offline/StoreAndForwardUtils;
.super Ljava/lang/Object;
.source "StoreAndForwardUtils.java"


# static fields
.field private static final OFFLINE_PREFIX:Ljava/lang/String; = "offline-"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clientErrorIfStoreAndForwardPayment(Lcom/squareup/queue/retrofit/RetrofitTask;Ljava/lang/String;Lcom/squareup/server/SquareCallback;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)Z"
        }
    .end annotation

    .line 31
    invoke-static {p1}, Lcom/squareup/payment/offline/StoreAndForwardUtils;->isOfflineCapture(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 32
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " task should not be enqueued for store and forward payments!"

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 34
    new-instance p1, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1, p0}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    const/16 p0, 0x190

    invoke-virtual {p2, p1, p0}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    const/4 p0, 0x1

    return p0

    :cond_0
    return v0
.end method

.method public static generateOfflineAuthorizationId()Ljava/lang/String;
    .locals 2

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "offline-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isOfflineCapture(Lcom/squareup/queue/Capture;)Z
    .locals 0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getAuthorizationId()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/payment/offline/StoreAndForwardUtils;->isOfflineCapture(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static isOfflineCapture(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, "offline-"

    .line 49
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
