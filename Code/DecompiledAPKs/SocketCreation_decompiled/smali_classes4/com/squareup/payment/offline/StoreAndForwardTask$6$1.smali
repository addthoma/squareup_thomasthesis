.class Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;
.super Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;
.source "StoreAndForwardTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask$6;->doRun(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

.field final synthetic val$queueEmpty:Z


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask$6;Lcom/squareup/server/SquareCallback;Z)V
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iput-boolean p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->val$queueEmpty:Z

    iget-object p1, p1, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method


# virtual methods
.method protected doRun(Lcom/squareup/server/SquareCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 366
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-boolean v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$doRescheduleTask:Z

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->crossSessionQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v2, v2, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v2, v2, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v3, v3, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v3, v3, Lcom/squareup/payment/offline/StoreAndForwardTask;->merchantName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-boolean v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$excludeLastPayment:Z

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v1, v1, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->val$lastPaymentInBatch:Lcom/squareup/payment/offline/StoredPayment;

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->access$900(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoredPayment;)V

    .line 375
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->val$queueEmpty:Z

    if-eqz v0, :cond_2

    .line 376
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$6$1;->this$1:Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask$6;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iget-object v0, v0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    invoke-interface {v0}, Lcom/squareup/notifications/StoredPaymentNotifier;->hideNotification()V

    .line 379
    :cond_2
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method
