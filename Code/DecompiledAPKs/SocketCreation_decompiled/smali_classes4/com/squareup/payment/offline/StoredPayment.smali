.class public Lcom/squareup/payment/offline/StoredPayment;
.super Ljava/lang/Object;
.source "StoredPayment.java"

# interfaces
.implements Lcom/squareup/queue/PendingPayment;


# instance fields
.field public final captureForDisplay:Lcom/squareup/queue/Capture;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

.field public final payment:Lcom/squareup/protos/queuebert/model/Payment;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final paymentCompleted:Z

.field public final paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

.field public final storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Z)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move v6, p3

    .line 73
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/Payment;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/queuebert/model/Payment;Z)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v6, p3

    .line 69
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/Payment;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/Payment;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/queue/Capture;->secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/Capture;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-nez p2, :cond_1

    goto :goto_1

    .line 59
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/queue/bills/CompleteBill;->secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    .line 60
    iput-object p3, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    .line 61
    iput-object p4, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    .line 62
    iput-object p5, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    .line 63
    iput-boolean p6, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentCompleted:Z

    return-void
.end method

.method public constructor <init>(Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V
    .locals 7

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, p2

    move v6, p3

    .line 78
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/Payment;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-void
.end method

.method public static asV2Payment(Lcom/squareup/protos/queuebert/model/Payment;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;
    .locals 2

    .line 222
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->unique_key:Ljava/lang/String;

    .line 223
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->bletchley_key_id:Ljava/lang/String;

    .line 224
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->bletchley_key_id(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->encrypted_payload:Lokio/ByteString;

    .line 225
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->encrypted_payload(Lokio/ByteString;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->client_timestamp:Ljava/lang/String;

    .line 226
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->client_timestamp(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->total_money:Lcom/squareup/protos/common/Money;

    .line 227
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->card_brand:Ljava/lang/String;

    .line 228
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_brand(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->card_last_four:Ljava/lang/String;

    .line 229
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->card_last_four(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/protos/queuebert/model/Payment;->merchant_token:Ljava/lang/String;

    .line 230
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/protos/queuebert/model/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    .line 231
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->auth_code(Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;)Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;

    move-result-object p0

    .line 232
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/payments/Payment$Builder;->build()Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object p0

    return-object p0
.end method

.method public static fromEnqueuedBytes(Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/payment/offline/StoredPayment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)",
            "Lcom/squareup/payment/offline/StoredPayment;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 270
    iget-boolean v0, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentCompleted:Z

    .line 272
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->captureBytes:[B

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->completeBillBytes:[B

    if-eqz v1, :cond_0

    goto :goto_0

    .line 274
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "One of captureBytes/completeBillBytes must be non-null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 277
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentBytes:[B

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentV2Bytes:[B

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->storeAndForwardBillBytes:[B

    if-eqz v1, :cond_2

    goto :goto_1

    .line 280
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "One of paymentBytes/paymentV2Bytes/storeAndForwardBillBytes must be non-null"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 285
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->captureBytes:[B

    const/4 v2, 0x0

    if-nez v1, :cond_4

    move-object v1, v2

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->captureBytes:[B

    .line 286
    invoke-interface {p1, v1}, Lcom/squareup/tape/FileObjectQueue$Converter;->from([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/Capture;

    .line 287
    :goto_2
    iget-object v3, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->completeBillBytes:[B

    if-nez v3, :cond_5

    goto :goto_3

    :cond_5
    iget-object v2, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->completeBillBytes:[B

    .line 288
    invoke-interface {p1, v2}, Lcom/squareup/tape/FileObjectQueue$Converter;->from([B)Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/queue/bills/CompleteBill;

    .line 291
    :goto_3
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentBytes:[B

    if-eqz p1, :cond_6

    .line 292
    sget-object p1, Lcom/squareup/protos/queuebert/model/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object p0, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentBytes:[B

    .line 293
    invoke-virtual {p1, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/queuebert/model/Payment;

    .line 295
    new-instance p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-direct {p1, v1, p0, v0}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/queuebert/model/Payment;Z)V

    return-object p1

    .line 296
    :cond_6
    iget-object p1, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentV2Bytes:[B

    if-eqz p1, :cond_7

    .line 297
    sget-object p1, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object p0, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->paymentV2Bytes:[B

    invoke-virtual {p1, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    .line 298
    new-instance p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-direct {p1, v1, p0, v0}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Z)V

    return-object p1

    .line 300
    :cond_7
    sget-object p1, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object p0, p0, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;->storeAndForwardBillBytes:[B

    .line 301
    invoke-virtual {p1, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    .line 302
    new-instance p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-direct {p1, v2, p0, v0}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-object p1
.end method

.method public static queueRequest(Ljava/util/Collection;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;"
        }
    .end annotation

    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/offline/StoredPayment;

    .line 210
    invoke-virtual {v2}, Lcom/squareup/payment/offline/StoredPayment;->isBillPayment()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    iget-object v2, v2, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    :cond_0
    iget-object v3, v2, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v3, :cond_1

    .line 214
    invoke-static {v3}, Lcom/squareup/payment/offline/StoredPayment;->asV2Payment(Lcom/squareup/protos/queuebert/model/Payment;)Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v2, v2, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    .line 215
    :goto_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 218
    :cond_2
    new-instance p0, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->payment(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;

    move-result-object p0

    invoke-virtual {p0, v1}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->bill(Ljava/util/List;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest$Builder;->build()Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/payment/offline/StoredPayment;->isBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/bills/CompleteBill;->asBillBuilder(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 89
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setStoreAndForward(Lcom/squareup/transactionhistory/pending/StoreAndForwardState;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/squareup/activity/BillHistoryHelper;->asCaptureBuilder(Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->STORED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 93
    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setStoreAndForward(Lcom/squareup/transactionhistory/pending/StoreAndForwardState;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public asCaptureTask()Lcom/squareup/queue/CaptureTask;
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    :goto_0
    return-object v0
.end method

.method public forwarded()Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/payment/offline/ForwardedPayment;->fromStoredPaymentCapture(Lcom/squareup/queue/Capture;Ljava/lang/String;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object v0

    return-object v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-static {v0}, Lcom/squareup/payment/offline/ForwardedPayment;->fromStoredPaymentCompleteBill(Lcom/squareup/queue/bills/CompleteBill;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object v0

    return-object v0
.end method

.method public getAuthCode()Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-object v0

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-object v0

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->auth_code:Lcom/squareup/crypto/merchantsecret/MerchantAuthCode;

    return-object v0
.end method

.method public getBillPayloadEncryptionKeyToken()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->bill_payload_encryption_key_token:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBletchleyKeyId()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/Payment;->bletchley_key_id:Ljava/lang/String;

    return-object v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->bletchley_key_id:Ljava/lang/String;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCardBrand()Ljava/lang/String;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/Payment;->card_brand:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_brand:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getCardLastFour()Ljava/lang/String;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/Payment;->card_last_four:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->card_last_four:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getPaymentInstrumentEncryptionKeyToken()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->payment_instrument_encryption_key_token:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {v0}, Lcom/squareup/queue/Capture;->getTime()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-virtual {v0}, Lcom/squareup/queue/bills/CompleteBill;->getTime()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public getTotalMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-virtual {v0}, Lcom/squareup/queue/bills/CompleteBill;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/Payment;->unique_key:Ljava/lang/String;

    return-object v0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->unique_key:Ljava/lang/String;

    return-object v0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->bill_id:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    return-object v0
.end method

.method public isBillPayment()Z
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPaymentCompleted()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentCompleted:Z

    return v0
.end method

.method public isV1Payment()Z
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isV2Payment()Z
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public paymentComplete()Lcom/squareup/payment/offline/StoredPayment;
    .locals 8

    .line 100
    new-instance v7, Lcom/squareup/payment/offline/StoredPayment;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    iget-object v4, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    iget-object v5, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    const/4 v6, 0x1

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoredPayment;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/queue/bills/CompleteBill;Lcom/squareup/protos/queuebert/model/Payment;Lcom/squareup/protos/client/store_and_forward/payments/Payment;Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;Z)V

    return-object v7
.end method

.method public toEnqueuedBytes(Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/queuebert/model/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPayment;->payment:Lcom/squareup/protos/queuebert/model/Payment;

    .line 240
    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, v1

    .line 242
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/store_and_forward/payments/Payment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentV2:Lcom/squareup/protos/client/store_and_forward/payments/Payment;

    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, v1

    .line 243
    :goto_1
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPayment;->storeAndForwardBill:Lcom/squareup/protos/client/store_and_forward/bills/StoreAndForwardBill;

    .line 244
    invoke-virtual {v0, v2}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    :cond_2
    move-object v7, v1

    .line 248
    :goto_2
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    if-eqz v0, :cond_3

    .line 249
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 250
    iget-object v2, p0, Lcom/squareup/payment/offline/StoredPayment;->captureForDisplay:Lcom/squareup/queue/Capture;

    invoke-interface {p1, v2, v0}, Lcom/squareup/tape/FileObjectQueue$Converter;->toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V

    .line 251
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    move-object v3, v0

    goto :goto_3

    :cond_3
    move-object v3, v1

    .line 256
    :goto_3
    iget-object v0, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    if-eqz v0, :cond_4

    .line 257
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 258
    iget-object v1, p0, Lcom/squareup/payment/offline/StoredPayment;->completeBillForDisplay:Lcom/squareup/queue/bills/CompleteBill;

    invoke-interface {p1, v1, v0}, Lcom/squareup/tape/FileObjectQueue$Converter;->toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V

    .line 259
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    :cond_4
    move-object v4, v1

    .line 262
    new-instance p1, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;

    iget-boolean v8, p0, Lcom/squareup/payment/offline/StoredPayment;->paymentCompleted:Z

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;-><init>([B[B[B[B[BZ)V

    return-object p1
.end method
