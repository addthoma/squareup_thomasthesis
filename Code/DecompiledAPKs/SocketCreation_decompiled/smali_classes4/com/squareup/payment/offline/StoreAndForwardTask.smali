.class public Lcom/squareup/payment/offline/StoreAndForwardTask;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;,
        Lcom/squareup/payment/offline/StoreAndForwardTask$ErrorCallbackRunnable;,
        Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;,
        Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/retrofit/RetrofitTask<",
        "Lcom/squareup/queue/QueueRootModule$Component;",
        ">;"
    }
.end annotation


# static fields
.field static final MAX_BATCH_SIZE:I = 0x14

.field static final PAYMENT_COMPLETED_TIMEOUT_MS:I = 0x1499700


# instance fields
.field transient application:Landroid/app/Application;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient bus:Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient clock:Lcom/squareup/util/Clock;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient crossSessionQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient currentUserId:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient dataDirectory:Ljava/io/File;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient fileExecutor:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient forwardedPaymentsProvider:Lcom/squareup/payment/offline/ForwardedPaymentsProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient mainScheduler:Lrx/Scheduler;
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient mainThread:Lcom/squareup/thread/executor/MainThread;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final merchantName:Ljava/lang/String;

.field transient notificationManager:Landroid/app/NotificationManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient queueCache:Lcom/squareup/queue/retrofit/QueueCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient transactionLedgerManagerFactory:Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private transient userDataOrNull:Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

.field public final userId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 99
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    .line 105
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->merchantName:Ljava/lang/String;

    .line 106
    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userDataOrNull:Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/offline/StoreAndForwardTask;)Z
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->isUserLoggedOut()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getNextBatch(Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/offline/StoreAndForwardTask;)Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/payment/offline/StoreAndForwardTask;->sendNextBatchToServer(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/server/SquareCallback;Ljava/lang/Throwable;)V
    .locals 0

    .line 61
    invoke-static {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->handleCallbackError(Lcom/squareup/server/SquareCallback;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V
    .locals 0

    .line 61
    invoke-direct/range {p0 .. p6}, Lcom/squareup/payment/offline/StoreAndForwardTask;->handleResponse(Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->scheduleTaskWhenPaymentExpired(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method private cleanupAndEnqueue(ZLcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;ZLcom/squareup/payment/offline/StoredPayment;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ")V"
        }
    .end annotation

    move-object v8, p0

    .line 347
    iget-object v9, v8, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v10, Lcom/squareup/payment/offline/StoreAndForwardTask$6;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p1

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/payment/offline/StoreAndForwardTask$6;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;ZZLcom/squareup/payment/offline/StoredPayment;)V

    invoke-interface {v9, v10}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getMutableForwardedPayments(Lcom/squareup/settings/LocalSetting;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;>;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;"
        }
    .end annotation

    .line 484
    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_0

    .line 486
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    return-object p1

    .line 489
    :cond_0
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method private getNextBatch(Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    const-string v1, "Batch should only be read on the file thread"

    invoke-virtual {v0, v1}, Lcom/squareup/FileThreadEnforcer;->enforceOnFileThread(Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask$1;)V

    .line 195
    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardTask$3;

    invoke-direct {v1, p0, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$3;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)V

    invoke-virtual {p1, v1}, Lcom/squareup/queue/StoredPaymentsQueue;->readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-object v0
.end method

.method private getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;
    .locals 2

    .line 465
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userDataOrNull:Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    if-nez v0, :cond_0

    .line 466
    new-instance v0, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/offline/StoreAndForwardTask$DefaultUserData;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$1;)V

    iput-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userDataOrNull:Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userDataOrNull:Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    return-object v0
.end method

.method private static handleCallbackError(Lcom/squareup/server/SquareCallback;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 110
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 111
    check-cast p1, Lretrofit/RetrofitError;

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->failure(Lretrofit/RetrofitError;)V

    goto :goto_0

    :cond_0
    const-string v0, "/1.0/bills/enqueue"

    .line 113
    invoke-static {v0, p1}, Lretrofit/RetrofitError;->unexpectedError(Ljava/lang/String;Ljava/lang/Throwable;)Lretrofit/RetrofitError;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/server/SquareCallback;->failure(Lretrofit/RetrofitError;)V

    :goto_0
    return-void
.end method

.method private handleResponse(Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Lcom/squareup/payment/offline/StoredPayment;",
            "Z)V"
        }
    .end annotation

    move-object v7, p0

    move-object v0, p1

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v4, "QueueBert v2: response = %s"

    .line 284
    invoke-static {v4, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_3

    .line 287
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v4, v4, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v2, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 292
    iget-object v2, v7, Lcom/squareup/payment/offline/StoreAndForwardTask;->forwardedPaymentsProvider:Lcom/squareup/payment/offline/ForwardedPaymentsProvider;

    iget-object v4, v7, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    .line 293
    invoke-virtual {v2, v4}, Lcom/squareup/payment/offline/ForwardedPaymentsProvider;->getForwardedPayments(Ljava/lang/String;)Lcom/squareup/settings/LocalSetting;

    move-result-object v2

    .line 295
    invoke-direct {p0, v2}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getMutableForwardedPayments(Lcom/squareup/settings/LocalSetting;)Ljava/util/Map;

    move-result-object v4

    .line 297
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 299
    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;->queue_bill_result:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v6, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;

    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/Object;

    .line 300
    iget-object v11, v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;->id:Lcom/squareup/protos/client/IdPair;

    aput-object v11, v10, v3

    iget-object v11, v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    invoke-virtual {v11}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->name()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v1

    const-string v11, "QueueBert v2: bill %s: %s"

    invoke-static {v11, v10}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-object v10, v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;->id:Lcom/squareup/protos/client/IdPair;

    iget-object v10, v10, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 303
    invoke-static/range {p3 .. p3}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->access$300(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Ljava/util/Map;

    move-result-object v11

    invoke-interface {v11, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/squareup/payment/offline/StoredPayment;

    .line 304
    sget-object v12, Lcom/squareup/payment/offline/StoreAndForwardTask$7;->$SwitchMap$com$squareup$protos$client$store_and_forward$bills$QueueBillResult$Status:[I

    iget-object v8, v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    sget-object v13, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    invoke-static {v8, v13}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;

    invoke-virtual {v8}, Lcom/squareup/protos/client/store_and_forward/bills/QueueBillResult$Status;->ordinal()I

    move-result v8

    aget v8, v12, v8

    if-eq v8, v1, :cond_1

    if-eq v8, v9, :cond_0

    .line 330
    invoke-virtual {v11}, Lcom/squareup/payment/offline/StoredPayment;->forwarded()Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object v8

    .line 331
    invoke-interface {v4, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-array v8, v1, [Ljava/lang/Object;

    aput-object v10, v8, v3

    const-string v9, "Server rejected payment (unique_key=%s)"

    .line 323
    invoke-static {v9, v8}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 324
    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 312
    :cond_1
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v6

    invoke-interface {v6}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Stored payment requires retry: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 313
    invoke-interface {v6, v8}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    const/4 v6, 0x1

    goto :goto_0

    .line 336
    :cond_2
    invoke-interface {v2, v4}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 337
    new-instance v0, Lcom/squareup/payment/offline/ForwardedPaymentManager$OnPaymentsForwarded;

    invoke-direct {v0}, Lcom/squareup/payment/offline/ForwardedPaymentManager$OnPaymentsForwarded;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->postIfLoggedIn(Ljava/lang/Object;)V

    move-object v0, p0

    move v1, v6

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object v4, v5

    move/from16 v5, p6

    move-object/from16 v6, p5

    .line 339
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoreAndForwardTask;->cleanupAndEnqueue(ZLcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;ZLcom/squareup/payment/offline/StoredPayment;)V

    return-void

    .line 288
    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "QueueBert v2 server failure, will retry"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    const-string v1, "/1.0/bills/enqueue"

    invoke-static {v1, v0}, Lretrofit/RetrofitError;->networkError(Ljava/lang/String;Ljava/io/IOException;)Lretrofit/RetrofitError;

    move-result-object v0

    throw v0
.end method

.method private isUserLoggedOut()Z
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->currentUserId:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private postIfLoggedIn(Ljava/lang/Object;)V
    .locals 1

    .line 477
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->isUserLoggedOut()Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->bus:Lcom/squareup/badbus/BadEventSink;

    invoke-interface {v0, p1}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private scheduleTaskWhenPaymentExpired(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 4

    .line 459
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0x1499700

    add-long/2addr v0, v2

    .line 460
    iget-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 461
    iget-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->userId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->merchantName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->enqueueTaskRequest(JLjava/lang/String;Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/backgroundjob/BackgroundJobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    return-void
.end method

.method private sendBatch(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoredPayment;ZLjava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/payment/offline/StoredPayment;",
            "Z",
            "Ljava/util/Collection<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    .line 253
    invoke-static {p6}, Lcom/squareup/payment/offline/StoredPayment;->queueRequest(Ljava/util/Collection;)Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;

    move-result-object p6

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 255
    invoke-static {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->access$300(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "QueueBert v2: sending payment batch, size = %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storeAndForwardBillService:Lcom/squareup/server/bills/StoreAndForwardBillService;

    invoke-interface {v0, p6}, Lcom/squareup/server/bills/StoreAndForwardBillService;->enqueue(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)Lrx/Observable;

    move-result-object p6

    .line 260
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {p6, v0}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object p6

    new-instance v7, Lcom/squareup/payment/offline/StoreAndForwardTask$5;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoreAndForwardTask$5;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/payment/offline/StoredPayment;Z)V

    invoke-virtual {p6, v7}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    return-void
.end method

.method private sendNextBatchToServer(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 212
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->requireLastPayment()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v4

    .line 215
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->isUserLoggedOut()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->merchantName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/squareup/notifications/StoredPaymentNotifier;->showNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->storedPaymentNotifier:Lcom/squareup/notifications/StoredPaymentNotifier;

    invoke-interface {v1}, Lcom/squareup/notifications/StoredPaymentNotifier;->hideNotification()V

    :goto_0
    if-nez v0, :cond_1

    .line 229
    invoke-static {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->access$600(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/squareup/payment/offline/StoreAndForwardTask;->isPaymentPendingReceiptInfo(Lcom/squareup/payment/offline/StoredPayment;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_2

    .line 232
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->removeLastPayment()V

    .line 233
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 234
    invoke-direct {p0, v4}, Lcom/squareup/payment/offline/StoreAndForwardTask;->scheduleTaskWhenPaymentExpired(Lcom/squareup/payment/offline/StoredPayment;)V

    .line 235
    iget-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance p2, Lcom/squareup/payment/offline/StoreAndForwardTask$4;

    invoke-direct {p2, p0, p3}, Lcom/squareup/payment/offline/StoreAndForwardTask$4;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {p1, p2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void

    .line 245
    :cond_2
    invoke-static {p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->access$300(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 246
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/offline/StoreAndForwardTask;->sendBatch(Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;Lcom/squareup/payment/offline/StoredPayment;ZLjava/util/Collection;)V

    return-void
.end method


# virtual methods
.method cleanup(Lcom/squareup/queue/StoredPaymentsQueue;Ljava/util/Set;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 406
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    const-string v1, "Stored payments should be cleaned up on the file thread"

    invoke-virtual {v0, v1}, Lcom/squareup/FileThreadEnforcer;->enforceOnFileThread(Ljava/lang/String;)V

    .line 413
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v0

    .line 414
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 416
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v5, 0x1

    aput-object v3, v2, v5

    const-string v3, "Cleaning up %d payments, queue contains %d stored payments"

    .line 415
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 414
    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    .line 419
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 421
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result p2

    if-lez p2, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result p2

    if-lez p2, :cond_2

    .line 422
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->peek()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p2

    .line 423
    invoke-virtual {p2}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v2

    .line 424
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 425
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->remove()V

    .line 426
    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 429
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v3

    invoke-interface {v3}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v3

    .line 430
    invoke-interface {v3, p2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardPaymentProcessed(Lcom/squareup/payment/offline/StoredPayment;)V

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    const-string v6, "QueueBert: completed payment %s"

    .line 431
    invoke-static {v6, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    invoke-virtual {p2}, Lcom/squareup/payment/offline/StoredPayment;->isPaymentCompleted()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result p2

    if-lez p2, :cond_0

    .line 437
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->peek()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p2

    .line 441
    invoke-virtual {p2}, Lcom/squareup/payment/offline/StoredPayment;->isPaymentCompleted()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 442
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->remove()V

    goto :goto_0

    .line 448
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot cleanup head payment: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    .line 454
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cleanup complete, queue contains "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    invoke-virtual {p1}, Lcom/squareup/queue/StoredPaymentsQueue;->size()I

    move-result p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " stored payments"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 454
    invoke-interface {v0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    return-void
.end method

.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 122
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getStoredPayments()Lcom/squareup/queue/StoredPaymentsQueue;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v1}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/squareup/payment/offline/StoreAndForwardTask$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/payment/offline/StoreAndForwardTask$1;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "QueueBert: about to send payments to server"

    .line 146
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    invoke-direct {p0}, Lcom/squareup/payment/offline/StoreAndForwardTask;->getUserData()Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/payment/offline/StoreAndForwardTask$UserData;->getTransactionLedgerManager()Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object v1

    const-string v2, "About to send payments to server"

    .line 148
    invoke-interface {v1, v2}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logStoreAndForwardTaskStatus(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p0, v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->sendQueueToServer(Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/queue/QueueRootModule$Component;)V
    .locals 0

    .line 494
    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueRootModule$Component;->inject(Lcom/squareup/payment/offline/StoreAndForwardTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;->inject(Lcom/squareup/queue/QueueRootModule$Component;)V

    return-void
.end method

.method isPaymentPendingReceiptInfo(Lcom/squareup/payment/offline/StoredPayment;)Z
    .locals 6

    .line 392
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->isPaymentCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->clock:Lcom/squareup/util/Clock;

    .line 393
    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x1499700

    add-long/2addr v2, v4

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    .line 396
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->isV1Payment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->isV2Payment()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 2

    .line 118
    new-instance v0, Lcom/squareup/payment/offline/StoreAndForwardTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method sendQueueToServer(Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/server/SquareCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardTask;->fileExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/payment/offline/StoreAndForwardTask$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask$2;-><init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/server/SquareCallback;Lcom/squareup/queue/StoredPaymentsQueue;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
