.class final Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;
.super Ljava/lang/Object;
.source "ParsedStoreAndForwardKey.java"

# interfaces
.implements Lcom/squareup/encryption/CryptoKeyAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/encryption/CryptoKeyAdapter<",
        "Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyId(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p1, p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->bletchleyKeyId:Ljava/lang/String;

    return-object p1
.end method

.method public bridge synthetic getKeyId(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;->getKeyId(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRawKey(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)[B
    .locals 0

    .line 13
    iget-object p1, p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->rawCertificate:[B

    return-object p1
.end method

.method public bridge synthetic getRawKey(Ljava/lang/Object;)[B
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;->getRawKey(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)[B

    move-result-object p1

    return-object p1
.end method

.method public isExpired(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)Z
    .locals 0

    .line 17
    invoke-virtual {p1}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;->isExpired()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isExpired(Ljava/lang/Object;)Z
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ParsedStoreAndForwardKey$1;->isExpired(Lcom/squareup/payment/offline/ParsedStoreAndForwardKey;)Z

    move-result p1

    return p1
.end method
