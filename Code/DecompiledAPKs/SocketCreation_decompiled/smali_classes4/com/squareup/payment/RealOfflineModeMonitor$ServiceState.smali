.class Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
.super Ljava/lang/Object;
.source "RealOfflineModeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealOfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ServiceState"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor$1;)V
    .locals 0

    .line 288
    invoke-direct {p0}, Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;-><init>()V

    return-void
.end method


# virtual methods
.method public init()V
    .locals 0

    return-void
.end method

.method public isUnavailable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public recordServiceAvailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 0

    return-object p0
.end method

.method public recordServiceUnavailable(Z)Lcom/squareup/payment/RealOfflineModeMonitor$ServiceState;
    .locals 0

    return-object p0
.end method
