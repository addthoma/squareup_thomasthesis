.class public final enum Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;
.super Ljava/lang/Enum;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemChangeType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

.field public static final enum ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

.field public static final enum DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

.field public static final enum EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 36
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    const/4 v1, 0x0

    const-string v2, "ADD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 37
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    const/4 v2, 0x1

    const-string v3, "DELETE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 38
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    const/4 v3, 0x2

    const-string v4, "EDIT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    .line 35
    sget-object v4, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->ADD:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->DELETE:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->EDIT:Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->$VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;
    .locals 1

    .line 35
    const-class v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->$VALUES:[Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    invoke-virtual {v0}, [Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;

    return-object v0
.end method
