.class public final synthetic Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/checkout/CartItem$Transform;


# instance fields
.field private final synthetic f$0:Lcom/squareup/payment/Order;

.field private final synthetic f$1:Ljava/lang/String;

.field private final synthetic f$2:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final synthetic f$3:Lcom/squareup/protos/client/Employee;

.field private final synthetic f$4:Z


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/protos/client/Employee;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$0:Lcom/squareup/payment/Order;

    iput-object p2, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$2:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p4, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$3:Lcom/squareup/protos/client/Employee;

    iput-boolean p5, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$4:Z

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 6

    iget-object v0, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$0:Lcom/squareup/payment/Order;

    iget-object v1, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$2:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-object v3, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$3:Lcom/squareup/protos/client/Employee;

    iget-boolean v4, p0, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;->f$4:Z

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->lambda$removeDiscountFromAllItemsInternal$2$Order(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/protos/client/Employee;ZLcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method
