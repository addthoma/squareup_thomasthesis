.class public final Lcom/squareup/payment/OrderSeatingHandler;
.super Ljava/lang/Object;
.source "OrderSeatingHandler.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSeatingHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSeatingHandler.kt\ncom/squareup/payment/OrderSeatingHandler\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,140:1\n704#2:141\n777#2,2:142\n1529#2,3:144\n1713#2,14:147\n1713#2,14:161\n1360#2:175\n1429#2,3:176\n1360#2:179\n1429#2,3:180\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSeatingHandler.kt\ncom/squareup/payment/OrderSeatingHandler\n*L\n37#1:141\n37#1,2:142\n76#1,3:144\n88#1,14:147\n102#1,14:161\n104#1:175\n104#1,3:176\n138#1:179\n138#1,3:180\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u000e\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u0007J(\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\u0019\u001a\u00020\u001aJ\u0008\u0010\"\u001a\u00020\u0018H\u0002J\u0008\u0010#\u001a\u00020$H\u0002J(\u0010%\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\u0019\u001a\u00020\u001aJ*\u0010\'\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0014\u0010(\u001a\n )*\u0004\u0018\u00010\u00070\u0007*\u00020\u0007H\u0002R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\tR\u0013\u0010\u000c\u001a\u0004\u0018\u00010\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0003@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/payment/OrderSeatingHandler;",
        "",
        "seating",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;",
        "(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)V",
        "allSeats",
        "",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "getAllSeats",
        "()Ljava/util/List;",
        "enabledSeats",
        "getEnabledSeats",
        "latestCoverCountEvent",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
        "getLatestCoverCountEvent",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
        "nextSeatOrdinal",
        "",
        "getNextSeatOrdinal",
        "()I",
        "<set-?>",
        "getSeating",
        "()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;",
        "addNewSeat",
        "",
        "currentDate",
        "Ljava/util/Date;",
        "addSeat",
        "seat",
        "addSeatAndUpdateCoverCount",
        "eventType",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "disableHighestNumberSeat",
        "seatingBuilder",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;",
        "setSeatCount",
        "newCount",
        "updateCoverCount",
        "disabled",
        "kotlin.jvm.PlatformType",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-void
.end method

.method private final addNewSeat(Ljava/util/Date;)V
    .locals 4

    .line 93
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;-><init>()V

    .line 94
    new-instance v1, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->seat_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    .line 95
    invoke-direct {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getNextSeatOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 97
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object p1

    const-string v0, "Seat.Builder()\n         \u2026(true)\n          .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderSeatingHandler;->addSeat(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)V

    return-void
.end method

.method private final disableHighestNumberSeat()V
    .locals 7

    .line 102
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 161
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    .line 163
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 164
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 165
    :cond_1
    move-object v3, v1

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 102
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    check-cast v3, Ljava/lang/Comparable;

    .line 167
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 168
    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 102
    iget-object v5, v5, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    check-cast v5, Ljava/lang/Comparable;

    .line 169
    invoke-interface {v3, v5}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v6

    if-gez v6, :cond_3

    move-object v1, v4

    move-object v3, v5

    .line 173
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 102
    :goto_0
    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 103
    invoke-direct {p0}, Lcom/squareup/payment/OrderSeatingHandler;->seatingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object v0

    .line 104
    iget-object v3, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v3, :cond_6

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->seat:Ljava/util/List;

    if-eqz v3, :cond_6

    check-cast v3, Ljava/lang/Iterable;

    .line 175
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v3, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 176
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 177
    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 104
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "it"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/squareup/payment/OrderSeatingHandler;->disabled(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object v4

    :cond_4
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 178
    :cond_5
    check-cast v2, Ljava/util/List;

    .line 104
    :cond_6
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->seat(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-void
.end method

.method private final disabled(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;
    .locals 1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object p1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    move-result-object p1

    return-object p1
.end method

.method private final getNextSeatOrdinal()I
    .locals 3

    .line 138
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 180
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 181
    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 138
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->ordinal:Ljava/lang/Integer;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 182
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 138
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->max(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final seatingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;-><init>()V

    :goto_0
    return-object v0
.end method

.method private final updateCoverCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V
    .locals 2

    .line 114
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;-><init>()V

    .line 115
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object v0

    .line 116
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p2

    .line 117
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p1

    .line 118
    iget-object p2, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->cover_count_change_log:Ljava/util/List;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p1

    if-eqz p3, :cond_1

    .line 121
    new-instance p2, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 122
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p2

    .line 123
    invoke-virtual {p2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    .line 119
    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p1

    .line 125
    new-instance p2, Lcom/squareup/protos/client/ISO8601Date;

    invoke-static {p4}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;

    move-result-object p1

    .line 126
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object p1

    .line 128
    invoke-direct {p0}, Lcom/squareup/payment/OrderSeatingHandler;->seatingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object p2

    .line 129
    iget-object p3, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz p3, :cond_2

    iget-object p3, p3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->cover_count_change_log:Ljava/util/List;

    if-eqz p3, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :goto_1
    check-cast p3, Ljava/util/Collection;

    invoke-static {p3, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->cover_count_change_log(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-void
.end method


# virtual methods
.method public final addSeat(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)V
    .locals 4

    const-string v0, "seat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getAllSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 144
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 76
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->seat_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    .line 78
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getAllSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 79
    invoke-direct {p0}, Lcom/squareup/payment/OrderSeatingHandler;->seatingBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->seat(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-void

    .line 76
    :cond_3
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final addSeatAndUpdateCoverCount(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V
    .locals 1

    const-string v0, "seat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0, p1}, Lcom/squareup/payment/OrderSeatingHandler;->addSeat(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;)V

    .line 69
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/payment/OrderSeatingHandler;->updateCoverCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    return-void
.end method

.method public final getAllSeats()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->seat:Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getEnabledSeats()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getAllSeats()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 141
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 142
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;

    .line 37
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;->enabled:Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getLatestCoverCountEvent()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
    .locals 6

    .line 88
    iget-object v0, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;->cover_count_change_log:Ljava/util/List;

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Iterable;

    .line 147
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 148
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 149
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 150
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 151
    :cond_1
    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    .line 88
    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    check-cast v2, Ljava/lang/Comparable;

    .line 153
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 154
    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    .line 88
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;->ordinal:Ljava/lang/Integer;

    check-cast v4, Ljava/lang/Comparable;

    .line 155
    invoke-interface {v2, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v5

    if-gez v5, :cond_3

    move-object v1, v3

    move-object v2, v4

    .line 159
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 160
    :goto_0
    check-cast v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    :cond_4
    return-object v1
.end method

.method public final getSeating()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/payment/OrderSeatingHandler;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-object v0
.end method

.method public final setSeatCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V
    .locals 1

    const-string v0, "eventType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 49
    invoke-direct {p0, p4}, Lcom/squareup/payment/OrderSeatingHandler;->addNewSeat(Ljava/util/Date;)V

    goto :goto_0

    .line 52
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_1

    .line 53
    invoke-direct {p0}, Lcom/squareup/payment/OrderSeatingHandler;->disableHighestNumberSeat()V

    goto :goto_1

    .line 56
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/payment/OrderSeatingHandler;->updateCoverCount(ILcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Lcom/squareup/protos/client/Employee;Ljava/util/Date;)V

    return-void
.end method
