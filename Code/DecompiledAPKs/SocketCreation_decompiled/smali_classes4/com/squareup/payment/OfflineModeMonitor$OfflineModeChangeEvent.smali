.class public Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;
.super Ljava/lang/Object;
.source "OfflineModeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OfflineModeChangeEvent"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final isInOfflineMode:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-boolean p1, p0, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 61
    instance-of v0, p1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 62
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    check-cast p1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;

    iget-boolean p1, p1, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 66
    iget-boolean v0, p0, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 70
    iget-boolean v1, p0, Lcom/squareup/payment/OfflineModeMonitor$OfflineModeChangeEvent;->isInOfflineMode:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "OfflineModeChangeEvent{isInOfflineMode=%s}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
