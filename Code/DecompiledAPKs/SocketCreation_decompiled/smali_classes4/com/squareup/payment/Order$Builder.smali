.class public Lcom/squareup/payment/Order$Builder;
.super Ljava/lang/Object;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private addedCouponsAndCartScopeDiscountEvents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field private addedCouponsAndCartScopeDiscounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private appointmentDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

.field private availableTaxRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation
.end field

.field private availableTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final cart:Lcom/squareup/checkout/Cart$Builder;

.field private coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private customerInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field private deletedTaxes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private diningOption:Lcom/squareup/checkout/DiningOption;

.field private discountApplicationIdEnabled:Z

.field private employeeFirstName:Ljava/lang/String;

.field private employeeLastName:Ljava/lang/String;

.field private employeeToken:Ljava/lang/String;

.field private openTicketName:Ljava/lang/String;

.field private openTicketNote:Ljava/lang/String;

.field private orderTicketName:Ljava/lang/String;

.field private predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

.field private productsOpenTicketOpenedOn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private returnCart:Lcom/squareup/checkout/ReturnCart;

.field private roundingType:Lcom/squareup/calc/constants/RoundingType;

.field private seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

.field private surcharges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation
.end field

.field private ticketIdPair:Lcom/squareup/protos/client/IdPair;

.field private userId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->availableTaxes:Ljava/util/Map;

    .line 173
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->availableTaxRules:Ljava/util/List;

    .line 174
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->deletedTaxes:Ljava/util/Map;

    .line 175
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscounts:Ljava/util/Map;

    .line 176
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEvents:Ljava/util/Map;

    .line 177
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts:Ljava/util/Set;

    .line 178
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->surcharges:Ljava/util/Map;

    .line 195
    new-instance v0, Lcom/squareup/checkout/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/Cart$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method

.method static synthetic access$2300(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->userId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2400(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method static synthetic access$2500(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->employeeFirstName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2700(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->employeeLastName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$2800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/calc/constants/RoundingType;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    return-object p0
.end method

.method static synthetic access$2900(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->orderTicketName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3000(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->openTicketName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3100(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->openTicketNote:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3200(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object p0
.end method

.method static synthetic access$3300(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->productsOpenTicketOpenedOn:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$3400(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/DiningOption;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->diningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method static synthetic access$3500(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object p0
.end method

.method static synthetic access$3600(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method static synthetic access$3700(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->customerInstruments:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$3800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-object p0
.end method

.method static synthetic access$3900(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-object p0
.end method

.method static synthetic access$4000(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->appointmentDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    return-object p0
.end method

.method static synthetic access$4100(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/Cart$Builder;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method static synthetic access$4200(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscounts:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$4300(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->surcharges:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$4400(Lcom/squareup/payment/Order$Builder;)Ljava/util/Set;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$4500(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->availableTaxes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$4600(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->availableTaxRules:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$4700(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->deletedTaxes:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$4800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/ReturnCart;
    .locals 0

    .line 166
    iget-object p0, p0, Lcom/squareup/payment/Order$Builder;->returnCart:Lcom/squareup/checkout/ReturnCart;

    return-object p0
.end method

.method static synthetic access$4900(Lcom/squareup/payment/Order$Builder;)Z
    .locals 0

    .line 166
    iget-boolean p0, p0, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled:Z

    return p0
.end method

.method public static cloneState(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;
    .locals 2

    .line 246
    new-instance v0, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/payment/Order$Builder;-><init>()V

    .line 247
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$2100(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->userId(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 248
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$600(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->diningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 249
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1900(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 250
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1800(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 251
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1700(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeFirstName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 252
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1600(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeLastName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 253
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1500(Lcom/squareup/payment/Order;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 254
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1400(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxRules(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 255
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$800(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->predefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 256
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 257
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$100(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/ReturnCart;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->returnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 258
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$000(Lcom/squareup/payment/Order;)Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static forPayment(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;
    .locals 1

    .line 200
    new-instance v0, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/payment/Order$Builder;-><init>()V

    .line 201
    invoke-virtual {v0, p0}, Lcom/squareup/payment/Order$Builder;->userId(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 202
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    .line 203
    invoke-virtual {p0, p2}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static fromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/Order$Builder;
    .locals 2

    .line 207
    new-instance v0, Lcom/squareup/payment/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/payment/Order$Builder;-><init>()V

    .line 208
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$2100(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->userId(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 209
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$2000(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 210
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1900(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 211
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1800(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 212
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1700(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeFirstName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 213
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1600(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->employeeLastName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 214
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 215
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDeletedItems()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->deletedItems(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 216
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1500(Lcom/squareup/payment/Order;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 217
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1400(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->availableTaxRules(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 218
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDeletedTaxesById()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->deletedTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 221
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;

    move-result-object v1

    .line 220
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 222
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1300(Lcom/squareup/payment/Order;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 223
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1200(Lcom/squareup/payment/Order;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->surcharges(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 224
    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 225
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1100(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->orderTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 226
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$1000(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 227
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$900(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->openTicketNote(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 228
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$800(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->predefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 229
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$700(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->productsOpenTicketOpenedOn(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 230
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$600(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->diningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 231
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$500(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->customerContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 232
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$400(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->customerSummary(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 233
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$300(Lcom/squareup/payment/Order;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->customerInstruments(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 234
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/OrderSeatingHandler;->getSeating()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 235
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/OrderCoursingHandler;->getCoursing()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->coursing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 236
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$200(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->appointmentDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 237
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$100(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/ReturnCart;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Order$Builder;->returnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/payment/Order$Builder;

    move-result-object v0

    .line 238
    invoke-static {p0}, Lcom/squareup/payment/Order;->access$000(Lcom/squareup/payment/Order;)Z

    move-result p0

    invoke-virtual {v0, p0}, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public addBlacklistedDiscount(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 1

    .line 420
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addedCouponsAndCartScopeDiscountEventsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 357
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscountEvents:Ljava/util/Map;

    return-object p0
.end method

.method public addedCouponsAndCartScopeDiscountsById(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 352
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->addedCouponsAndCartScopeDiscounts:Ljava/util/Map;

    return-object p0
.end method

.method public appointmentDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 399
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->appointmentDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    return-object p0
.end method

.method public availableTaxRules(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 342
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->availableTaxRules:Ljava/util/List;

    return-object p0
.end method

.method public availableTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 337
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->availableTaxes:Ljava/util/Map;

    return-object p0
.end method

.method public blacklistedDiscounts(Ljava/util/Set;)Lcom/squareup/payment/Order$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 414
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 415
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/payment/Order;
    .locals 2

    .line 425
    new-instance v0, Lcom/squareup/payment/Order;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/Order;-><init>(Lcom/squareup/payment/Order$Builder;Lcom/squareup/payment/Order$1;)V

    return-object v0
.end method

.method public coursing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->coursing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    return-object p0
.end method

.method public currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/payment/Order$Builder;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public customerContact(Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 322
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method public customerInstruments(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 332
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->customerInstruments:Ljava/util/List;

    return-object p0
.end method

.method public customerSummary(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object p0
.end method

.method public deletedItems(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 394
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->deletedItems(Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public deletedTaxes(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 347
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->deletedTaxes:Ljava/util/Map;

    return-object p0
.end method

.method public diningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->diningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method public discountApplicationIdEnabled(Z)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 409
    iput-boolean p1, p0, Lcom/squareup/payment/Order$Builder;->discountApplicationIdEnabled:Z

    return-object p0
.end method

.method public employeeFirstName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->employeeFirstName:Ljava/lang/String;

    return-object p0
.end method

.method public employeeLastName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 312
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->employeeLastName:Ljava/lang/String;

    return-object p0
.end method

.method public employeeToken(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method public items(Ljava/util/Collection;)Lcom/squareup/payment/Order$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 388
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->clearItems()Lcom/squareup/checkout/Cart$Builder;

    .line 389
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->addItems(Ljava/lang/Iterable;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public varargs items([Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/Order$Builder;
    .locals 1

    .line 382
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->clearItems()Lcom/squareup/checkout/Cart$Builder;

    .line 383
    iget-object v0, p0, Lcom/squareup/payment/Order$Builder;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->addItems([Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public openTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->openTicketName:Ljava/lang/String;

    return-object p0
.end method

.method public openTicketNote(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 287
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->openTicketNote:Ljava/lang/String;

    return-object p0
.end method

.method public orderTicketName(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 277
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->orderTicketName:Ljava/lang/String;

    return-object p0
.end method

.method public predefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object p0
.end method

.method public productsOpenTicketOpenedOn(Ljava/util/List;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 297
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->productsOpenTicketOpenedOn:Ljava/util/List;

    return-object p0
.end method

.method public returnCart(Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->returnCart:Lcom/squareup/checkout/ReturnCart;

    return-object p0
.end method

.method public roundingType(Lcom/squareup/calc/constants/RoundingType;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    return-object p0
.end method

.method public seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->seating:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    return-object p0
.end method

.method public surcharges(Ljava/util/Map;)Lcom/squareup/payment/Order$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;)",
            "Lcom/squareup/payment/Order$Builder;"
        }
    .end annotation

    .line 362
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->surcharges:Ljava/util/Map;

    return-object p0
.end method

.method public ticketIdPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/payment/Order$Builder;
    .locals 0

    .line 267
    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public userId(Ljava/lang/String;)Lcom/squareup/payment/Order$Builder;
    .locals 1

    const-string v0, "userId"

    .line 262
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/payment/Order$Builder;->userId:Ljava/lang/String;

    return-object p0
.end method
