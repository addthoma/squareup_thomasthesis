.class public Lcom/squareup/payment/OrderVariationNames;
.super Ljava/lang/Object;
.source "OrderVariationNames.java"

# interfaces
.implements Lcom/squareup/checkout/OrderVariationNamer;


# static fields
.field public static final INSTANCE:Lcom/squareup/payment/OrderVariationNames;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/payment/OrderVariationNames;

    invoke-direct {v0}, Lcom/squareup/payment/OrderVariationNames;-><init>()V

    sput-object v0, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static displayName(Lcom/squareup/util/Res;Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    .line 69
    invoke-static {p0, p1}, Lcom/squareup/payment/OrderVariationNames;->getVariableDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 71
    :cond_0
    invoke-static {p0, p1, p3}, Lcom/squareup/payment/OrderVariationNames;->getFixedDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static displayNameFromCatalogVariation(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/models/CatalogItemVariation;)Ljava/lang/String;
    .locals 2

    .line 28
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->isVariablePricing()Z

    move-result v1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getOrdinal()I

    move-result p1

    .line 28
    invoke-static {p0, v0, v1, p1}, Lcom/squareup/payment/OrderVariationNames;->displayName(Lcom/squareup/util/Res;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getFixedDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .line 78
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    .line 80
    sget p1, Lcom/squareup/checkout/R$string;->item_variation_default_name_regular:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 82
    :cond_0
    sget p1, Lcom/squareup/checkout/R$string;->item_variation_default_name:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    add-int/lit8 p2, p2, 0x1

    const-string p1, "ordinal"

    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 85
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    :goto_0
    return-object p1
.end method

.method public static getVariableDisplayName(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 34
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    sget p1, Lcom/squareup/common/strings/R$string;->default_itemization_name:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public fromItemVariation(Lcom/squareup/util/Res;Lcom/squareup/api/items/ItemVariation;)Ljava/lang/String;
    .locals 4

    .line 23
    iget-object v0, p2, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    sget-object v2, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object p2, p2, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    .line 24
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 23
    invoke-static {p1, v0, v1, p2}, Lcom/squareup/payment/OrderVariationNames;->displayName(Lcom/squareup/util/Res;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public fromItemVariationDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Ljava/lang/String;
    .locals 4

    .line 46
    iget-object v0, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    .line 47
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;

    if-eqz v0, :cond_1

    .line 51
    iget-object p2, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    .line 52
    iget-object v0, p2, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    sget-object v2, Lcom/squareup/api/items/PricingType;->VARIABLE_PRICING:Lcom/squareup/api/items/PricingType;

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object p2, p2, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    .line 53
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 52
    invoke-static {p1, v0, v1, p2}, Lcom/squareup/payment/OrderVariationNames;->displayName(Lcom/squareup/util/Res;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-eqz p2, :cond_2

    .line 56
    iget-object p1, p2, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$DisplayDetails;->name:Ljava/lang/String;

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method
