.class public final Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;
.super Ljava/lang/Object;
.source "GreedyItemBasedCouponMatcher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;",
        "",
        "()V",
        "Companion",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;->Companion:Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;)V
    .locals 7
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;->Companion:Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v6}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily$default(Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;ILjava/lang/Object;)V

    return-void
.end method

.method public static final applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;->Companion:Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher$Companion;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method
