.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;
.super Ljava/lang/Object;
.source "LoyaltyTourScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0006\u001a\u00020\u0005H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;",
        "",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;",
        "loyaltyEnrollTour",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final loyaltyEnrollTour()Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;

    sget-object v1, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;->ENROLL:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;

    invoke-direct {v0, v1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourType;)V

    return-object v0
.end method
