.class public final Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;
.super Ljava/lang/Object;
.source "FirstPaymentCashTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountFreezeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;"
        }
    .end annotation
.end field

.field private final activityAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;"
        }
    .end annotation
.end field

.field private final homeIdentifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final openTicketsSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final payCashScreenIdentifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingTransactionsStoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final textRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 108
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->accountFreezeProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 109
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 110
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 111
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 112
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 113
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 114
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 115
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 116
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 117
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->homeIdentifierProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 118
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 119
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 120
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 121
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 122
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 123
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 124
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->activityAppletProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 125
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 126
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 127
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 128
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 129
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->textRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 130
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 131
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 132
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->payCashScreenIdentifierProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
            ">;)",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    .line 161
    new-instance v26, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;

    move-object/from16 v0, v26

    invoke-direct/range {v0 .. v25}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v26
.end method

.method public static newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/tenderpayment/PayCashScreenIdentifier;)Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Landroid/app/Application;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
            ")",
            "Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    .line 178
    new-instance v26, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    move-object/from16 v0, v26

    invoke-direct/range {v0 .. v25}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/tenderpayment/PayCashScreenIdentifier;)V

    return-object v26
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;
    .locals 27

    move-object/from16 v0, p0

    .line 137
    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->accountFreezeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/accountfreeze/AccountFreeze;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->pendingTransactionsStoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/payment/pending/PendingTransactionsStore;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->openTicketsSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->homeIdentifierProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->activityAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/activity/ActivityApplet;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->firstPaymentTooltipStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/settings/LocalSetting;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->textRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->payCashScreenIdentifierProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    invoke-static/range {v2 .. v26}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/tenderpayment/PayCashScreenIdentifier;)Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial_Factory;->get()Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;

    move-result-object v0

    return-object v0
.end method
