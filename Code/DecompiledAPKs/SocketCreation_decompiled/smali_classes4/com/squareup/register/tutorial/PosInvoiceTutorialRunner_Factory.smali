.class public final Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;
.super Ljava/lang/Object;
.source "PosInvoiceTutorialRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final firstInvoiceTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->firstInvoiceTutorialProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
            ">;)",
            "Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;-><init>(Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->firstInvoiceTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;

    iget-object v1, p0, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->newInvoiceFeaturesTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    invoke-static {v0, v1}, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->newInstance(Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;)Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner_Factory;->get()Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;

    move-result-object v0

    return-object v0
.end method
