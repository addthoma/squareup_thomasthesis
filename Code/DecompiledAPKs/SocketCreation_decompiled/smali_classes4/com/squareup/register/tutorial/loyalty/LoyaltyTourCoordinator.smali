.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "LoyaltyTourCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0016\u0010\u0011\u001a\u00020\u000e2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u000c\u0010\u0015\u001a\u00020\u000e*\u00020\u0010H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenRunner",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;",
        "(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;)V",
        "adapter",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;",
        "closeButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "pageIndicator",
        "Lcom/squareup/marin/widgets/MarinPageIndicator;",
        "pager",
        "Landroidx/viewpager/widget/ViewPager;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "updateContent",
        "pages",
        "",
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
        "bindViews",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field private pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

.field private pager:Landroidx/viewpager/widget/ViewPager;

.field private final screenRunner:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "screenRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->screenRunner:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    return-void
.end method

.method public static final synthetic access$getScreenRunner$p(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;)Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->screenRunner:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    return-object p0
.end method

.method public static final synthetic access$updateContent(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;Ljava/util/List;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->updateContent(Ljava/util/List;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 49
    sget v0, Lcom/squareup/pos/tutorials/R$id;->tour_pager:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pager:Landroidx/viewpager/widget/ViewPager;

    .line 50
    sget v0, Lcom/squareup/pos/tutorials/R$id;->tour_close_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 51
    sget v0, Lcom/squareup/pos/tutorials/R$id;->page_indicator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinPageIndicator;

    iput-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    return-void
.end method

.method private final updateContent(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourPage$ScreenData;",
            ">;)V"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->adapter:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

    const-string v1, "adapter"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;->setPages$pos_tutorials_release(Ljava/util/List;)V

    .line 60
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pager:Landroidx/viewpager/widget/ViewPager;

    if-nez p1, :cond_1

    const-string v0, "pager"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 61
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    if-nez p1, :cond_2

    const-string v2, "pageIndicator"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    iget-object v2, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->adapter:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->bindViews(Landroid/view/View;)V

    .line 33
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->adapter:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

    .line 34
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pager:Landroidx/viewpager/widget/ViewPager;

    const-string v1, "pager"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->adapter:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourAdapter;

    if-nez v2, :cond_1

    const-string v3, "adapter"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroidx/viewpager/widget/PagerAdapter;

    invoke-virtual {v0, v2}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->screenRunner:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->pages()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$1;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v2, "screenRunner.pages().sub\u2026ibe { updateContent(it) }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    if-nez v0, :cond_2

    const-string v2, "closeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$2;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$2;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v2, "closeButton.debouncedOnC\u2026er.closeLoyaltyTour(it) }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->screenRunner:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreenRunner;->tourType()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator$attach$3;-><init>(Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v2, "screenRunner.tourType().\u2026er.postTutorialCore(it) }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    if-nez p1, :cond_3

    const-string v0, "pageIndicator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourCoordinator;->pager:Landroidx/viewpager/widget/ViewPager;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    return-void
.end method
