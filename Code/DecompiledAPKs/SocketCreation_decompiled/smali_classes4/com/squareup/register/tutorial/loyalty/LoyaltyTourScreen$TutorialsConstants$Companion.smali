.class public final Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;
.super Ljava/lang/Object;
.source "LoyaltyTourScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;",
        "",
        "()V",
        "ADJUSTPOINTS_LEAVING",
        "",
        "ADJUSTPOINTS_SHOWN",
        "ENROLL_LEAVING",
        "ENROLL_SHOWN",
        "REDEMPTION_LEAVING",
        "REDEMPTION_SHOWN",
        "pos-tutorials_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

.field public static final ADJUSTPOINTS_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Adjust Points"

.field public static final ADJUSTPOINTS_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Adjust Points"

.field public static final ENROLL_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Enroll"

.field public static final ENROLL_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Enroll"

.field public static final REDEMPTION_LEAVING:Ljava/lang/String; = "Leaving LoyaltyTourScreen - Redemption"

.field public static final REDEMPTION_SHOWN:Ljava/lang/String; = "Shown LoyaltyTourScreen - Redemption"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

    invoke-direct {v0}, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;-><init>()V

    sput-object v0, Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;->$$INSTANCE:Lcom/squareup/register/tutorial/loyalty/LoyaltyTourScreen$TutorialsConstants$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
