.class public Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;
.super Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;
.source "FirstPaymentCashTutorial.java"


# instance fields
.field private final activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

.field private final application:Landroid/app/Application;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private defaultMoney:Lcom/squareup/protos/common/Money;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final payCashScreenIdentifier:Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/ui/activity/ActivityApplet;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/LocalSetting;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/tenderpayment/PayCashScreenIdentifier;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Landroid/app/Application;",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/payment/pending/PendingTransactionsStore;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;",
            "Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/activity/ActivityApplet;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/FirstPaymentTooltipStatus;",
            ">;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/tenderpayment/PayCashScreenIdentifier;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    move-object/from16 v8, p12

    move-object/from16 v19, p13

    move-object/from16 v9, p14

    move-object/from16 v10, p19

    move-object/from16 v18, p20

    move-object/from16 v20, p21

    move-object/from16 v21, p22

    move-object/from16 v22, p23

    .line 87
    invoke-interface/range {p11 .. p11}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v7

    sget-object v16, Lcom/squareup/analytics/RegisterActionName;->CASH_PAYMENT_TUTORIAL_DECLINED:Lcom/squareup/analytics/RegisterActionName;

    move-object/from16 v15, v16

    sget-object v16, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_DECLINE_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    sget-object v17, Lcom/squareup/analytics/RegisterViewName;->CASH_PAYMENT_TUTORIAL_FINISH_DIALOG:Lcom/squareup/analytics/RegisterViewName;

    .line 91
    invoke-interface/range {p24 .. p24}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v23

    .line 86
    invoke-direct/range {v0 .. v23}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Landroid/app/Application;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/payment/pending/PendingTransactionsStore;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/tickets/OpenTicketsSettings;ZLcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;Lcom/squareup/register/tutorial/V1TutorialHomeIdentifier;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/settings/LocalSetting;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Z)V

    move-object/from16 v1, p2

    .line 92
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->application:Landroid/app/Application;

    move-object/from16 v1, p14

    .line 93
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p15

    .line 94
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p16

    .line 95
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p17

    .line 96
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    move-object/from16 v1, p18

    .line 97
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object/from16 v1, p5

    .line 98
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p25

    .line 99
    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->payCashScreenIdentifier:Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    .line 100
    iget-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 101
    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v1

    int-to-long v1, v1

    iget-object v3, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->defaultMoney:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public bridge synthetic clearOnEndAction()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->clearOnEndAction()V

    return-void
.end method

.method completeButtonAction()V
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->activityApplet:Lcom/squareup/ui/activity/ActivityApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/ActivityApplet;->activate()V

    return-void
.end method

.method public bridge synthetic forceStart()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->forceStart()V

    return-void
.end method

.method protected getFinishPrompt()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/register/tutorial/TutorialDialog$Prompt;",
            ">;"
        }
    .end annotation

    .line 109
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;

    sget v1, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_content_history:I

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_link_history:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;-><init>(II)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V
    .locals 0

    .line 47
    invoke-super {p0, p1, p2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->handlePromptTap(Lcom/squareup/register/tutorial/TutorialDialog$Prompt;Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;)V

    return-void
.end method

.method public bridge synthetic isTriggered()Z
    .locals 1

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->isTriggered()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onEnd()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEnd()V

    return-void
.end method

.method public bridge synthetic onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public bridge synthetic onFinishedReceipt()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onFinishedReceipt()V

    return-void
.end method

.method onHandlePaymentScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 4

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->isPaymentTypeScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 116
    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object p1, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 117
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_tap_cash_tablet:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 118
    invoke-static {p1}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 120
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_cash:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 122
    :cond_0
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3_cash:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_tap_cash_phone:I

    .line 123
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    :goto_0
    return v1

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->payCashScreenIdentifier:Lcom/squareup/tenderpayment/PayCashScreenIdentifier;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/PayCashScreenIdentifier;->isPayCashScreen(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 128
    sget-object p1, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_3a_enter_cash_amount:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_cash_payment_screen:I

    .line 129
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Ljava/lang/CharSequence;)V

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method protected onHomeUpdate()V
    .locals 6

    .line 141
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->defaultMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_2_tap_charge:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->application:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_tap_charge:I

    sget v4, Lcom/squareup/checkout/R$string;->charge:I

    const-string v5, "charge"

    .line 143
    invoke-static {v1, v2, v3, v5, v4}, Lcom/squareup/register/tutorial/TutorialPhrases;->addMediumWeight(Landroid/content/Context;Lcom/squareup/util/Res;ILjava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 142
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Lcom/squareup/phrase/Phrase;)V

    goto :goto_0

    .line 146
    :cond_0
    sget-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;->payment_tutorial_1_start:Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;

    iget-object v1, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_content_start:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->defaultMoney:Lcom/squareup/protos/common/Money;

    .line 147
    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 146
    invoke-virtual {p0, v0, v1}, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->setContent(Lcom/squareup/register/tutorial/FirstPaymentTutorialPanelEvent$PanelName;Lcom/squareup/phrase/Phrase;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic onRequestExitTutorial()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onRequestExitTutorial()V

    return-void
.end method

.method public bridge synthetic onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method public bridge synthetic onShowingThanks()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->onShowingThanks()V

    return-void
.end method

.method public bridge synthetic setOnEndAction(Ljava/lang/Runnable;)V
    .locals 0

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->setOnEndAction(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected shouldTrigger()Z
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstPaymentCashTutorial;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
