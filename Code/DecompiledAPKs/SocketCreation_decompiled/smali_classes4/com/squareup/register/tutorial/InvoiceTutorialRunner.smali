.class public interface abstract Lcom/squareup/register/tutorial/InvoiceTutorialRunner;
.super Ljava/lang/Object;
.source "InvoiceTutorialRunner.java"

# interfaces
.implements Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/InvoiceTutorialRunner$NoInvoiceTutorialRunner;
    }
.end annotation


# virtual methods
.method public abstract customerOnInvoice(Z)V
.end method

.method public abstract forceStartFirstInvoiceTutorial()V
.end method

.method public abstract invoiceCustomAmountUpdated(Z)V
.end method

.method public abstract invoiceCustomerUpdated(Z)V
.end method

.method public abstract isDraftInvoice(Z)V
.end method

.method public abstract itemOnInvoice(Z)V
.end method

.method public abstract readyToFinishInvoices()V
.end method

.method public abstract readyToFinishInvoicesQuietly()V
.end method

.method public abstract setInvoicesAppletActiveState(Z)V
.end method

.method public abstract startInvoiceTutorialIfPossible()V
.end method

.method public abstract updateInvoicePreviewBanner(Ljava/lang/String;)V
.end method
