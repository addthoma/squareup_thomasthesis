.class public abstract Lcom/squareup/register/tutorial/CommonTutorialModule;
.super Ljava/lang/Object;
.source "CommonTutorialModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideInvoiceTutorialApi(Lcom/squareup/register/tutorial/PosInvoiceTutorialRunner;)Lcom/squareup/register/tutorial/InvoiceTutorialRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTutorialCoordinator(Lcom/squareup/tutorialv2/RealTutorialCoordinator;)Lcom/squareup/tutorialv2/TutorialCoordinator;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
