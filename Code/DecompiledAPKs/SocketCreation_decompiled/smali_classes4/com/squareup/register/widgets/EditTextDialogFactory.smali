.class public Lcom/squareup/register/widgets/EditTextDialogFactory;
.super Ljava/lang/Object;
.source "EditTextDialogFactory.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/EditTextDialogFactory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final editorId:I

.field private final hint:Ljava/lang/String;

.field private final inputType:I

.field private final layoutId:I

.field private final text:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 139
    new-instance v0, Lcom/squareup/register/widgets/EditTextDialogFactory$2;

    invoke-direct {v0}, Lcom/squareup/register/widgets/EditTextDialogFactory$2;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/EditTextDialogFactory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    .line 50
    iput p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->editorId:I

    .line 51
    iput p3, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    .line 52
    iput-object p4, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    .line 53
    iput-object p5, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$createEditTextDialog$0(Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;Lcom/squareup/widgets/SelectableEditText;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 77
    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;->onDone(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public createEditTextDialog(Landroid/content/Context;Lcom/squareup/text/Scrubber;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;)Landroid/app/AlertDialog;
    .locals 3

    .line 59
    iget v0, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 60
    iget v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->editorId:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/SelectableEditText;

    .line 61
    iget-object v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 65
    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {v2, p2, v1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 68
    :cond_0
    iget p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    const/4 v2, -0x1

    if-eq p2, v2, :cond_1

    .line 70
    invoke-virtual {v1, p2}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 73
    :cond_1
    new-instance p2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {p2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    .line 74
    invoke-virtual {p2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 75
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->done:I

    new-instance v2, Lcom/squareup/register/widgets/-$$Lambda$EditTextDialogFactory$tPn8WbqiHEVd_teZadL4xKsRmYI;

    invoke-direct {v2, p3, v1}, Lcom/squareup/register/widgets/-$$Lambda$EditTextDialogFactory$tPn8WbqiHEVd_teZadL4xKsRmYI;-><init>(Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;Lcom/squareup/widgets/SelectableEditText;)V

    .line 76
    invoke-virtual {p1, p2, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 81
    new-instance p2, Lcom/squareup/register/widgets/EditTextDialogFactory$1;

    invoke-direct {p2, p0, p3, v1, p1}, Lcom/squareup/register/widgets/EditTextDialogFactory$1;-><init>(Lcom/squareup/register/widgets/EditTextDialogFactory;Lcom/squareup/register/widgets/EditTextDialogFactory$EditTextDialogListener;Lcom/squareup/widgets/SelectableEditText;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, p2}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 91
    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 94
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object p2

    const/4 p3, 0x4

    invoke-virtual {p2, p3}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 112
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    .line 114
    iget v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->editorId:I

    .line 115
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    .line 116
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    .line 117
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    .line 118
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    .line 119
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 123
    iget v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->editorId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 131
    iget p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->layoutId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->editorId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->inputType:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-object p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->text:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object p2, p0, Lcom/squareup/register/widgets/EditTextDialogFactory;->hint:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
