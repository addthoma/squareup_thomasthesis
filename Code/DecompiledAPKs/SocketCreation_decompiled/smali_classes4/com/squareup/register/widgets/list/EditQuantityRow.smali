.class public Lcom/squareup/register/widgets/list/EditQuantityRow;
.super Landroid/widget/LinearLayout;
.source "EditQuantityRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_MAX_QUANTITY:J = 0x98967fL


# instance fields
.field private allowZero:Z

.field private final listener:Landroid/text/method/DigitsKeyListener;

.field private maxQuantity:J

.field private final minus:Lcom/squareup/glyph/SquareGlyphView;

.field private final plus:Lcom/squareup/glyph/SquareGlyphView;

.field public quantityListener:Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

.field private final scrubber:Lcom/squareup/text/QuantityScrubber;

.field private final text:Lcom/squareup/widgets/SelectableEditText;

.field private verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-wide/32 v0, 0x98967f

    .line 42
    iput-wide v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->maxQuantity:J

    const/4 p2, 0x0

    .line 49
    invoke-virtual {p0, p2}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOrientation(I)V

    .line 50
    sget p2, Lcom/squareup/widgets/pos/R$layout;->edit_quantity_row:I

    invoke-static {p1, p2, p0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 52
    new-instance p1, Lcom/squareup/text/QuantityScrubber;

    invoke-direct {p1, v0, v1}, Lcom/squareup/text/QuantityScrubber;-><init>(J)V

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->scrubber:Lcom/squareup/text/QuantityScrubber;

    .line 53
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "0123456789"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object p2

    invoke-virtual {p2}, Ljava/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 53
    invoke-static {p1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->listener:Landroid/text/method/DigitsKeyListener;

    .line 56
    sget p1, Lcom/squareup/widgets/pos/R$id;->quantity_text:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    .line 57
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    iget-object p2, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->listener:Landroid/text/method/DigitsKeyListener;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->scrubber:Lcom/squareup/text/QuantityScrubber;

    invoke-direct {p2, v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    new-instance p2, Lcom/squareup/register/widgets/list/EditQuantityRow$1;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/list/EditQuantityRow$1;-><init>(Lcom/squareup/register/widgets/list/EditQuantityRow;)V

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    sget p1, Lcom/squareup/widgets/pos/R$id;->quantity_decrease:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    .line 66
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/register/widgets/list/EditQuantityRow$2;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/list/EditQuantityRow$2;-><init>(Lcom/squareup/register/widgets/list/EditQuantityRow;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    sget p1, Lcom/squareup/widgets/pos/R$id;->quantity_increase:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->plus:Lcom/squareup/glyph/SquareGlyphView;

    .line 75
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->plus:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/register/widgets/list/EditQuantityRow$3;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/list/EditQuantityRow$3;-><init>(Lcom/squareup/register/widgets/list/EditQuantityRow;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    new-instance p1, Lcom/squareup/marin/widgets/BorderPainter;

    sget p2, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    invoke-direct {p1, p0, p2}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/list/EditQuantityRow;I)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->updatePlusMinus(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/list/EditQuantityRow;)Lcom/squareup/widgets/SelectableEditText;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    return-object p0
.end method

.method private update(I)V
    .locals 1

    .line 130
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->updatePlusMinus(I)V

    .line 132
    iget-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setSelection(I)V

    return-void
.end method

.method private updatePlusMinus(I)V
    .locals 8

    .line 136
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->allowZero:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    if-lez p1, :cond_1

    goto :goto_0

    :cond_0
    if-le p1, v3, :cond_1

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->plus:Lcom/squareup/glyph/SquareGlyphView;

    int-to-long v4, p1

    iget-wide v6, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->maxQuantity:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-virtual {v0, v2}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->quantityListener:Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    if-eqz v0, :cond_3

    .line 140
    invoke-interface {v0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;->onQuantityChanged(I)V

    :cond_3
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 92
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->clearBorders()V

    .line 94
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 95
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    iget-object v1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->plus:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/BorderPainter;->drawChildBorders(Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->clearBorders()V

    .line 97
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->verticalDividerPainter:Lcom/squareup/marin/widgets/BorderPainter;

    iget-object v1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/marin/widgets/BorderPainter;->drawChildBorders(Landroid/graphics/Canvas;Landroid/view/View;)V

    return-void
.end method

.method public getValue()I
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    .line 120
    :cond_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public setAllowZero(Z)V
    .locals 1

    .line 145
    iput-boolean p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->allowZero:Z

    .line 146
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->scrubber:Lcom/squareup/text/QuantityScrubber;

    invoke-virtual {v0, p1}, Lcom/squareup/text/QuantityScrubber;->setAllowZero(Z)V

    return-void
.end method

.method public setEnabledWithButtons(Z)V
    .locals 2

    .line 102
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 104
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->plus:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 105
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    if-eqz p1, :cond_0

    .line 107
    iget-boolean v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->allowZero:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->getValue()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->minus:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/list/EditQuantityRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_1

    sget p1, Lcom/squareup/widgets/R$color;->text_color:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/marin/R$color;->marin_light_gray:I

    :goto_0
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setTextColor(I)V

    return-void
.end method

.method public setMaxQuantity(J)V
    .locals 1

    .line 150
    iput-wide p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->maxQuantity:J

    .line 151
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->scrubber:Lcom/squareup/text/QuantityScrubber;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/text/QuantityScrubber;->setMaxValue(J)V

    return-void
.end method

.method public setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->quantityListener:Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    return-void
.end method

.method public setValue(I)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->scrubber:Lcom/squareup/text/QuantityScrubber;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/text/QuantityScrubber;->scrub(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/squareup/register/widgets/list/EditQuantityRow;->text:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 126
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->update(I)V

    return-void
.end method
