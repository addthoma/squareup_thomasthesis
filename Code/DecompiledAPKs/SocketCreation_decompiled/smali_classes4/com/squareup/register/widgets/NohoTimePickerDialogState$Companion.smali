.class public final Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;
.super Ljava/lang/Object;
.source "NohoTimePickerDialogState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/NohoTimePickerDialogState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoTimePickerDialogState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoTimePickerDialogState.kt\ncom/squareup/register/widgets/NohoTimePickerDialogState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,62:1\n180#2:63\n*E\n*S KotlinDebug\n*F\n+ 1 NohoTimePickerDialogState.kt\ncom/squareup/register/widgets/NohoTimePickerDialogState$Companion\n*L\n52#1:63\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J.\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00042\u0008\u0008\u0002\u0010\t\u001a\u00020\u00042\u0008\u0008\u0002\u0010\n\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000cJ\u000e\u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;",
        "",
        "()V",
        "DEFAULT_MAX_TIME",
        "Lorg/threeten/bp/LocalTime;",
        "DEFAULT_MIN_TIME",
        "create",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "currentTime",
        "minTime",
        "maxTime",
        "allowClear",
        "",
        "fromSnapshot",
        "bytes",
        "Lokio/ByteString;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;-><init>()V

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    .line 41
    invoke-static {}, Lorg/threeten/bp/LocalTime;->now()Lorg/threeten/bp/LocalTime;

    move-result-object p1

    sget-object p6, Lorg/threeten/bp/temporal/ChronoUnit;->MINUTES:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast p6, Lorg/threeten/bp/temporal/TemporalUnit;

    invoke-virtual {p1, p6}, Lorg/threeten/bp/LocalTime;->truncatedTo(Lorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalTime;

    move-result-object p1

    const-string p6, "LocalTime.now().truncatedTo(MINUTES)"

    invoke-static {p1, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 42
    invoke-static {}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->access$getDEFAULT_MIN_TIME$cp()Lorg/threeten/bp/LocalTime;

    move-result-object p2

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 43
    invoke-static {}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->access$getDEFAULT_MAX_TIME$cp()Lorg/threeten/bp/LocalTime;

    move-result-object p3

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    const/4 p4, 0x0

    .line 44
    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;->create(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 1

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;-><init>(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)V

    return-object v0
.end method

.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 4

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 53
    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    .line 54
    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalTimesKt;->readLocalTime(Lokio/BufferedSource;)Lorg/threeten/bp/LocalTime;

    move-result-object v1

    .line 55
    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalTimesKt;->readLocalTime(Lokio/BufferedSource;)Lorg/threeten/bp/LocalTime;

    move-result-object v2

    .line 56
    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalTimesKt;->readLocalTime(Lokio/BufferedSource;)Lorg/threeten/bp/LocalTime;

    move-result-object v3

    .line 57
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 53
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;-><init>(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)V

    return-object v0
.end method
