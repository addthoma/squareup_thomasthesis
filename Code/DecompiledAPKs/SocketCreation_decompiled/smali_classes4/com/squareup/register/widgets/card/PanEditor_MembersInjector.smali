.class public final Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;
.super Ljava/lang/Object;
.source "PanEditor_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/register/widgets/card/PanEditor;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->giftCardsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/register/widgets/card/PanEditor;",
            ">;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCurrency(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectGiftCards(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/giftcard/GiftCards;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor;->giftCards:Lcom/squareup/giftcard/GiftCards;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/register/widgets/card/PanEditor;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/giftcard/GiftCards;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->injectGiftCards(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/giftcard/GiftCards;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->injectCurrency(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->injectFeatures(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/card/PanEditor_MembersInjector;->injectMembers(Lcom/squareup/register/widgets/card/PanEditor;)V

    return-void
.end method
