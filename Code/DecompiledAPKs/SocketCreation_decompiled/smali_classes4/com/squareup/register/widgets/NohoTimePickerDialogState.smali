.class public final Lcom/squareup/register/widgets/NohoTimePickerDialogState;
.super Ljava/lang/Object;
.source "NohoTimePickerDialogState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0007H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00072\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\u0006\u0010\u0018\u001a\u00020\u0019J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000c\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "",
        "currentTime",
        "Lorg/threeten/bp/LocalTime;",
        "minTime",
        "maxTime",
        "allowClear",
        "",
        "(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)V",
        "getAllowClear",
        "()Z",
        "getCurrentTime",
        "()Lorg/threeten/bp/LocalTime;",
        "getMaxTime",
        "getMinTime",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toString",
        "",
        "Companion",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

.field private static final DEFAULT_MAX_TIME:Lorg/threeten/bp/LocalTime;

.field private static final DEFAULT_MIN_TIME:Lorg/threeten/bp/LocalTime;


# instance fields
.field private final allowClear:Z

.field private final currentTime:Lorg/threeten/bp/LocalTime;

.field private final maxTime:Lorg/threeten/bp/LocalTime;

.field private final minTime:Lorg/threeten/bp/LocalTime;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->Companion:Lcom/squareup/register/widgets/NohoTimePickerDialogState$Companion;

    .line 36
    sget-object v0, Lorg/threeten/bp/LocalTime;->MIN:Lorg/threeten/bp/LocalTime;

    const-string v1, "LocalTime.MIN"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->DEFAULT_MIN_TIME:Lorg/threeten/bp/LocalTime;

    const/16 v0, 0x17

    const/16 v1, 0x3b

    .line 38
    invoke-static {v0, v1}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-string v1, "LocalTime.of(23, 59)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->DEFAULT_MAX_TIME:Lorg/threeten/bp/LocalTime;

    return-void
.end method

.method public constructor <init>(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)V
    .locals 1

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    iput-object p3, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    iput-boolean p4, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    return-void
.end method

.method public static final synthetic access$getDEFAULT_MAX_TIME$cp()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->DEFAULT_MAX_TIME:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public static final synthetic access$getDEFAULT_MIN_TIME$cp()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->DEFAULT_MIN_TIME:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->copy(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lorg/threeten/bp/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final component2()Lorg/threeten/bp/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final component3()Lorg/threeten/bp/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    return v0
.end method

.method public final copy(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 1

    const-string v0, "currentTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/register/widgets/NohoTimePickerDialogState;-><init>(Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    iget-boolean p1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowClear()Z
    .locals 1

    .line 25
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    return v0
.end method

.method public final getCurrentTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final getMaxTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final getMinTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 27
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/register/widgets/NohoTimePickerDialogState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/NohoTimePickerDialogState$toSnapshot$1;-><init>(Lcom/squareup/register/widgets/NohoTimePickerDialogState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NohoTimePickerDialogState(currentTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->currentTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", minTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->minTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", maxTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->maxTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowClear="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogState;->allowClear:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
