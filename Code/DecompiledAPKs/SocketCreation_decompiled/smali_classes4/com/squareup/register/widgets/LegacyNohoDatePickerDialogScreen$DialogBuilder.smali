.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DialogBuilder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyNohoDatePickerDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyNohoDatePickerDialogScreen.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,101:1\n66#2:102\n*E\n*S KotlinDebug\n*F\n+ 1 LegacyNohoDatePickerDialogScreen.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder\n*L\n64#1:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u00020\u00088\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;",
        "",
        "context",
        "Landroid/content/Context;",
        "args",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        "(Landroid/content/Context;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V",
        "runner",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
        "getRunner",
        "()Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;",
        "setRunner",
        "(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V",
        "build",
        "Landroid/app/Dialog;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

.field private final context:Landroid/content/Context;

.field public runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "args"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    .line 64
    iget-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    .line 102
    const-class p2, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Component;

    .line 65
    invoke-interface {p1, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Component;->inject(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V

    return-void
.end method

.method public static final synthetic access$getArgs$p(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    return-object p0
.end method


# virtual methods
.method public final build()Landroid/app/Dialog;
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->noho_date_picker_dialog:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    iget-object v2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    check-cast v3, Ljava/lang/Comparable;

    invoke-static {v2, v3}, Lkotlin/comparisons/ComparisonsKt;->maxOf(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/comparisons/ComparisonsKt;->minOf(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/LocalDate;

    const-string/jumbo v2, "view"

    .line 73
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v2, Lcom/squareup/widgets/pos/R$id;->date_picker:I

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoDatePicker;

    .line 74
    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoDatePicker;->setMinDate(Lorg/threeten/bp/LocalDate;)V

    .line 75
    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v3}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoDatePicker;->setMaxDate(Lorg/threeten/bp/LocalDate;)V

    .line 76
    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoDatePicker;->setCurrentDate(Lorg/threeten/bp/LocalDate;)V

    .line 78
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->context:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$builder$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$builder$1;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$builder$2;

    invoke-direct {v1, p0, v2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$builder$2;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;Lcom/squareup/noho/NohoDatePicker;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->args:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->getAllowClear()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    sget v1, Lcom/squareup/widgets/pos/R$string;->date_picker_remove:I

    new-instance v2, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$1;

    invoke-direct {v2, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$1;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    goto :goto_0

    :cond_0
    const/high16 v1, 0x1040000

    .line 92
    new-instance v2, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$2;

    invoke-direct {v2, p0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder$build$2;-><init>(Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 97
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    const-string v1, "builder.create()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Dialog;

    return-object v0
.end method

.method public final getRunner()Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    if-nez v0, :cond_0

    const-string v1, "runner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final setRunner(Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iput-object p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$DialogBuilder;->runner:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;

    return-void
.end method
