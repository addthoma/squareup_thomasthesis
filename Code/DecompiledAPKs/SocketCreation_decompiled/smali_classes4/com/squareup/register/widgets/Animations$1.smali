.class final Lcom/squareup/register/widgets/Animations$1;
.super Ljava/lang/Object;
.source "Animations.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private easeInEaseOut:Landroid/view/animation/Interpolator;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/squareup/register/widgets/Animations$1;->easeInEaseOut:Landroid/view/animation/Interpolator;

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 6

    .line 32
    iget-object v0, p0, Lcom/squareup/register/widgets/Animations$1;->easeInEaseOut:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    float-to-double v0, p1

    const v2, 0x3ea8f5c3    # 0.33f

    const-wide v3, 0x3fd5c28f5c28f5c3L    # 0.34

    cmpg-double v5, v0, v3

    if-gez v5, :cond_0

    div-float/2addr p1, v2

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v1, 0x3e800000    # 0.25f

    :goto_0
    mul-float p1, p1, v1

    add-float/2addr p1, v0

    goto :goto_1

    :cond_0
    const-wide v3, 0x3fe570a3d70a3d71L    # 0.67

    cmpg-double v5, v0, v3

    if-gez v5, :cond_1

    sub-float/2addr p1, v2

    div-float/2addr p1, v2

    const/high16 v0, 0x3fa00000    # 1.25f

    const v1, 0x3ea3d70a    # 0.32f

    mul-float p1, p1, v1

    sub-float p1, v0, p1

    goto :goto_1

    :cond_1
    const v0, 0x3f28f5c3    # 0.66f

    sub-float/2addr p1, v0

    div-float/2addr p1, v2

    const v0, 0x3f6e147b    # 0.93f

    const v1, 0x3d8f5c28    # 0.06999999f

    goto :goto_0

    :goto_1
    return p1
.end method
