.class final Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;
.super Ljava/lang/Object;
.source "NohoDatePickerDialogFactory.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $datePicker:Lcom/squareup/noho/NohoDatePicker;

.field final synthetic $screen:Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;Lcom/squareup/noho/NohoDatePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;->$screen:Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 79
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;->$screen:Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;->getOnUpdate()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoDatePicker;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;->$datePicker:Lcom/squareup/noho/NohoDatePicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoDatePicker;->getYearEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
