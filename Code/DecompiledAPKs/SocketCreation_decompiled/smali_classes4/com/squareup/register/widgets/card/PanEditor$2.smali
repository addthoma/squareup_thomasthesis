.class Lcom/squareup/register/widgets/card/PanEditor$2;
.super Lcom/squareup/text/ScrubbingTextWatcher;
.source "PanEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/PanEditor;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/PanEditor;

.field final synthetic val$shakeAnimationListener:Lcom/squareup/register/widgets/validation/ShakeAnimationListener;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;Lcom/squareup/register/widgets/validation/ShakeAnimationListener;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    iput-object p4, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->val$shakeAnimationListener:Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    invoke-direct {p0, p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 98
    invoke-super {p0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 99
    invoke-static {p1}, Lcom/squareup/util/Strings;->removeSpaces(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 100
    iget-object v0, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {v0}, Lcom/squareup/register/widgets/card/PanEditor;->access$000(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->getBrand(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {v1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->access$300(Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/Card$Brand;)V

    .line 102
    iget-object v1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {v1}, Lcom/squareup/register/widgets/card/PanEditor;->access$000(Lcom/squareup/register/widgets/card/PanEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, p1, v0, v2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->determineCardNumberState(Ljava/lang/String;Lcom/squareup/Card$Brand;Z)Lcom/squareup/register/widgets/card/CardNumberState;

    move-result-object v1

    .line 103
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Brand;->checkForPanWarning(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 104
    :goto_0
    sget-object p1, Lcom/squareup/register/widgets/card/CardNumberState;->INVALID:Lcom/squareup/register/widgets/card/CardNumberState;

    if-eq v1, p1, :cond_2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 108
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->val$shakeAnimationListener:Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;->restoreOriginalTextColor()V

    goto :goto_2

    .line 105
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/PanEditor;->access$200(Lcom/squareup/register/widgets/card/PanEditor;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/PanEditor;->startAnimation(Landroid/view/animation/Animation;)V

    .line 110
    :goto_2
    iget-object p1, p0, Lcom/squareup/register/widgets/card/PanEditor$2;->this$0:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/PanEditor;->access$400(Lcom/squareup/register/widgets/card/PanEditor;)V

    return-void
.end method
