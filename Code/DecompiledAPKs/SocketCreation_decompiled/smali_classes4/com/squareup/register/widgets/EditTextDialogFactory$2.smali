.class final Lcom/squareup/register/widgets/EditTextDialogFactory$2;
.super Ljava/lang/Object;
.source "EditTextDialogFactory.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/EditTextDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/register/widgets/EditTextDialogFactory;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/EditTextDialogFactory;
    .locals 7

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 148
    new-instance p1, Lcom/squareup/register/widgets/EditTextDialogFactory;

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/register/widgets/EditTextDialogFactory;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 140
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/EditTextDialogFactory$2;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/register/widgets/EditTextDialogFactory;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/register/widgets/EditTextDialogFactory;
    .locals 0

    .line 152
    new-array p1, p1, [Lcom/squareup/register/widgets/EditTextDialogFactory;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 140
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/EditTextDialogFactory$2;->newArray(I)[Lcom/squareup/register/widgets/EditTextDialogFactory;

    move-result-object p1

    return-object p1
.end method
