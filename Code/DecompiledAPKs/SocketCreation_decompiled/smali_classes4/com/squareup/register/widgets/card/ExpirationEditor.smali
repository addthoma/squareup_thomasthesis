.class public Lcom/squareup/register/widgets/card/ExpirationEditor;
.super Landroid/widget/FrameLayout;
.source "ExpirationEditor.java"


# static fields
.field public static final ADVANCE_LEN:I = 0x5


# instance fields
.field private final expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

.field private expirationValid:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private focusChange:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private ignoreFocusChange:Z

.field private strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationValid:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 35
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 41
    sget p2, Lcom/squareup/widgets/pos/R$layout;->expiration_editor:I

    invoke-static {p1, p2, p0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 42
    sget p1, Lcom/squareup/widgets/pos/R$id;->expiration_input:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/card/CardEditorEditText;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    .line 43
    new-instance p1, Lcom/squareup/text/ExpirationDateScrubber;

    invoke-direct {p1}, Lcom/squareup/text/ExpirationDateScrubber;-><init>()V

    .line 44
    iget-object p2, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/text/ExpirationDateScrubber;->setOnInvalidContentListener(Lcom/squareup/text/OnInvalidContentListener;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {v0, p1, p2}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p2, v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance p2, Lcom/squareup/register/widgets/card/-$$Lambda$ExpirationEditor$Hji_HHKYnvNYJWcrp6-uZg8-gvA;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/-$$Lambda$ExpirationEditor$Hji_HHKYnvNYJWcrp6-uZg8-gvA;-><init>(Lcom/squareup/register/widgets/card/ExpirationEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    new-instance p2, Lcom/squareup/register/widgets/card/ExpirationEditor$1;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/card/ExpirationEditor$1;-><init>(Lcom/squareup/register/widgets/card/ExpirationEditor;)V

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/card/ExpirationEditor;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->assertStrategyIsSet()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/register/widgets/card/ExpirationEditor;)Lcom/squareup/register/widgets/card/CardEditorEditText;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    return-object p0
.end method

.method private assertStrategyIsSet()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "setStrategy needs to be called after view creation."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public extractCardExpiration()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText()Landroid/text/Editable;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public isFocused()Z
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->isFocused()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$new$0$ExpirationEditor(Landroid/view/View;Z)V
    .locals 0

    .line 48
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->assertStrategyIsSet()V

    .line 50
    iget-boolean p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->ignoreFocusChange:Z

    if-nez p1, :cond_0

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    .line 51
    invoke-virtual {p0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->extractCardExpiration()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->expirationValid(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 52
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->onInvalidContent()V

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onExpirationValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationValid:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onFocusChange()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->focusChange:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/register/widgets/card/CardEditorEditText;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result p1

    return p1
.end method

.method public setIgnoreFocusChange()V
    .locals 1

    const/4 v0, 0x1

    .line 109
    iput-boolean v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->ignoreFocusChange:Z

    return-void
.end method

.method public setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    return-void
.end method

.method public setSaveEnabled(Z)V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setSaveEnabled(Z)V

    return-void
.end method

.method public setSelection(Landroid/text/Editable;I)V
    .locals 0

    .line 121
    invoke-static {p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    return-void
.end method

.method public setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->strategy:Lcom/squareup/register/widgets/card/PanValidationStrategy;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/register/widgets/card/ExpirationEditor;->expirationInput:Lcom/squareup/register/widgets/card/CardEditorEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
