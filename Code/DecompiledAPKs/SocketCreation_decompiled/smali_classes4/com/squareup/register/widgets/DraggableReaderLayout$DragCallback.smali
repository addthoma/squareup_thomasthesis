.class Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;
.super Landroidx/customview/widget/ViewDragHelper$Callback;
.source "DraggableReaderLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/DraggableReaderLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DragCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/DraggableReaderLayout;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-direct {p0}, Landroidx/customview/widget/ViewDragHelper$Callback;-><init>()V

    return-void
.end method

.method private settleReaderIn(Landroid/view/View;)V
    .locals 3

    .line 166
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 168
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    .line 169
    iget-object v1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getHeight()I

    move-result v1

    sub-int/2addr v1, v0

    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$100(Lcom/squareup/register/widgets/DraggableReaderLayout;)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_0

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$100(Lcom/squareup/register/widgets/DraggableReaderLayout;)I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    .line 172
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    move p1, v0

    .line 174
    :goto_0
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$500(Lcom/squareup/register/widgets/DraggableReaderLayout;)Landroidx/customview/widget/ViewDragHelper;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Landroidx/customview/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$202(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z

    .line 176
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$200(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 177
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private settleReaderOut(Landroid/view/View;)V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    mul-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getWidth()I

    move-result v0

    .line 155
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    move v2, v0

    move v0, p1

    move p1, v2

    .line 157
    :goto_0
    iget-object v1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {v1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$500(Lcom/squareup/register/widgets/DraggableReaderLayout;)Landroidx/customview/widget/ViewDragHelper;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroidx/customview/widget/ViewDragHelper;->settleCapturedViewAt(II)Z

    .line 158
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$302(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z

    .line 159
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1, v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$202(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z

    .line 160
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1}, Landroidx/core/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .locals 0

    .line 104
    iget-object p3, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p3}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p1

    return p1

    .line 107
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getWidth()I

    move-result p1

    .line 108
    iget-object p3, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p3}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$100(Lcom/squareup/register/widgets/DraggableReaderLayout;)I

    move-result p3

    mul-int/lit8 p3, p3, -0x1

    .line 109
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
    .locals 1

    .line 114
    iget-object p3, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p3}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 115
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    mul-int/lit8 p3, p1, -0x1

    .line 117
    iget-object v0, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getHeight()I

    move-result v0

    sub-int/2addr v0, p1

    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$100(Lcom/squareup/register/widgets/DraggableReaderLayout;)I

    move-result p1

    add-int/2addr v0, p1

    .line 118
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1

    .line 120
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    return p1
.end method

.method public onViewCaptured(Landroid/view/View;I)V
    .locals 0

    .line 125
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$202(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z

    .line 126
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1, p2}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$302(Lcom/squareup/register/widgets/DraggableReaderLayout;Z)Z

    .line 127
    iget-object p1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$400(Lcom/squareup/register/widgets/DraggableReaderLayout;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    return-void
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .locals 3

    .line 133
    iget-object p2, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-static {p2}, Lcom/squareup/register/widgets/DraggableReaderLayout;->access$000(Lcom/squareup/register/widgets/DraggableReaderLayout;)Z

    move-result p2

    const/4 p3, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 134
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p2

    iget-object v1, p0, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->this$0:Lcom/squareup/register/widgets/DraggableReaderLayout;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/DraggableReaderLayout;->getHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    if-ge p2, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result p2

    if-lez p2, :cond_0

    :goto_0
    if-eqz p3, :cond_2

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->settleReaderOut(Landroid/view/View;)V

    goto :goto_1

    .line 142
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/register/widgets/DraggableReaderLayout$DragCallback;->settleReaderIn(Landroid/view/View;)V

    :goto_1
    return-void
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .locals 0

    .line 100
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    sget p2, Lcom/squareup/loggedout/R$id;->reader:I

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
