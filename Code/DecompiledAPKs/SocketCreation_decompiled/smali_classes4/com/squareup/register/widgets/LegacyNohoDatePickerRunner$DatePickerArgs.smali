.class public final Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;
.super Ljava/lang/Object;
.source "LegacyNohoDatePickerRunner.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DatePickerArgs"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyNohoDatePickerRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyNohoDatePickerRunner.kt\ncom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs\n*L\n1#1,111:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0014H\u0016R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
        "Landroid/os/Parcelable;",
        "token",
        "",
        "currentDate",
        "Lorg/threeten/bp/LocalDate;",
        "minDate",
        "maxDate",
        "allowClear",
        "",
        "(JLorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)V",
        "getAllowClear",
        "()Z",
        "getCurrentDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getMaxDate",
        "getMinDate",
        "getToken",
        "()J",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "dest",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion;


# instance fields
.field private final allowClear:Z

.field private final currentDate:Lorg/threeten/bp/LocalDate;

.field private final maxDate:Lorg/threeten/bp/LocalDate;

.field private final minDate:Lorg/threeten/bp/LocalDate;

.field private final token:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->Companion:Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion;

    .line 99
    new-instance v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)V
    .locals 1

    const-string v0, "currentDate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "minDate"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxDate"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->token:J

    iput-object p3, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->currentDate:Lorg/threeten/bp/LocalDate;

    iput-object p4, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->minDate:Lorg/threeten/bp/LocalDate;

    iput-object p5, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->maxDate:Lorg/threeten/bp/LocalDate;

    iput-boolean p6, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->allowClear:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getAllowClear()Z
    .locals 1

    .line 82
    iget-boolean v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->allowClear:Z

    return v0
.end method

.method public final getCurrentDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->currentDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMaxDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->maxDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMinDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->minDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getToken()J
    .locals 2

    .line 78
    iget-wide v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->token:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "dest"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-wide v0, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->token:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 90
    iget-object p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->currentDate:Lorg/threeten/bp/LocalDate;

    invoke-static {p1, p2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->minDate:Lorg/threeten/bp/LocalDate;

    invoke-static {p1, p2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V

    .line 92
    iget-object p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->maxDate:Lorg/threeten/bp/LocalDate;

    invoke-static {p1, p2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunnerKt;->access$writeLocalDate(Landroid/os/Parcel;Lorg/threeten/bp/LocalDate;)V

    .line 93
    iget-boolean p2, p0, Lcom/squareup/register/widgets/LegacyNohoDatePickerRunner$DatePickerArgs;->allowClear:Z

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeBooleanAsInt(Landroid/os/Parcel;Z)V

    return-void
.end method
