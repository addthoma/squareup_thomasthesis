.class final Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;
.super Ljava/lang/Object;
.source "NohoDatePickerDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/NohoDatePickerDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "wrapper",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 8

    const-string/jumbo v0, "wrapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;->getState()Lcom/squareup/register/widgets/NohoDatePickerDialogProps;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    sget v2, Lcom/squareup/widgets/pos/R$layout;->noho_date_picker_dialog_optional_year:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 35
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getCurrentDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    check-cast v3, Ljava/lang/Comparable;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v4

    check-cast v4, Ljava/lang/Comparable;

    invoke-static {v2, v3, v4}, Lkotlin/ranges/RangesKt;->coerceIn(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Lorg/threeten/bp/LocalDate;

    .line 36
    new-instance v3, Lkotlin/jvm/internal/Ref$IntRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$IntRef;-><init>()V

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v4

    iput v4, v3, Lkotlin/jvm/internal/Ref$IntRef;->element:I

    .line 39
    sget v4, Lcom/squareup/widgets/pos/R$id;->date_picker:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string/jumbo v5, "view.findViewById(R.id.date_picker)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/noho/NohoDatePicker;

    .line 40
    invoke-virtual {v4, v2}, Lcom/squareup/noho/NohoDatePicker;->setCurrentDate(Lorg/threeten/bp/LocalDate;)V

    .line 41
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/squareup/noho/NohoDatePicker;->setMinDate(Lorg/threeten/bp/LocalDate;)V

    .line 42
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/squareup/noho/NohoDatePicker;->setMaxDate(Lorg/threeten/bp/LocalDate;)V

    .line 44
    new-instance v2, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;

    invoke-direct {v2, v4, v3, v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$updateYearEnabled$1;-><init>(Lcom/squareup/noho/NohoDatePicker;Lkotlin/jvm/internal/Ref$IntRef;Lcom/squareup/register/widgets/NohoDatePickerDialogProps;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 64
    sget v3, Lcom/squareup/widgets/pos/R$id;->allow_year_check:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string/jumbo v5, "view.findViewById(R.id.allow_year_check)"

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lcom/squareup/noho/NohoCheckableRow;

    .line 65
    move-object v5, v3

    check-cast v5, Landroid/view/View;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getAllowNoYear()Z

    move-result v6

    invoke-static {v5, v6}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 66
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getAllowNoYear()Z

    move-result v5

    const/4 v6, 0x1

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getShouldShowYear()Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v5, 0x1

    .line 67
    :goto_1
    invoke-virtual {v3, v5}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 68
    new-instance v7, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$1;

    invoke-direct {v7, v2}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v7, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v3, v7}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 70
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v3, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 73
    invoke-virtual {v2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 75
    sget-object v2, Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;->MATCH_DIALOG:Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setDialogContentLayout(Lcom/squareup/widgets/dialog/internal/AlertController$DialogContentLayout;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 76
    invoke-virtual {v1, v6}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 77
    new-instance v2, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$1;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$1;-><init>(Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    .line 78
    new-instance v3, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;

    invoke-direct {v3, p1, v4}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$builder$2;-><init>(Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;Lcom/squareup/noho/NohoDatePicker;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 82
    invoke-virtual {v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogProps;->getAllowClear()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    sget v0, Lcom/squareup/widgets/pos/R$string;->date_picker_remove:I

    new-instance v2, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$2;-><init>(Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    goto :goto_2

    :cond_2
    const/high16 v0, 0x1040000

    .line 88
    new-instance v2, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$3;

    invoke-direct {v2, p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1$3;-><init>(Lcom/squareup/register/widgets/NohoDatePickerDialogScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    .line 93
    :goto_2
    invoke-virtual {v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/NohoDatePickerDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
