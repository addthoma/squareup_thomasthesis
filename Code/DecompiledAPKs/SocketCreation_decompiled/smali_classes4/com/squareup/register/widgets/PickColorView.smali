.class public Lcom/squareup/register/widgets/PickColorView;
.super Lcom/squareup/glyph/SquareGlyphView;
.source "PickColorView.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private checked:Z

.field private selector:Landroid/graphics/drawable/StateListDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .line 18
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0, p1, v0}, Lcom/squareup/glyph/SquareGlyphView;-><init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_shadow_radius:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dx:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/glyph/R$dimen;->glyph_shadow_dy:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 23
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/marin/R$color;->marin_text_shadow:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 24
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/register/widgets/PickColorView;->setGlyphShadow(IFFI)V

    .line 25
    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/PickColorView;->setGlyphColorRes(I)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_dim_translucent_pressed:I

    .line 28
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/StateListDrawable;

    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorView;->selector:Landroid/graphics/drawable/StateListDrawable;

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    .line 37
    invoke-super {p0}, Lcom/squareup/glyph/SquareGlyphView;->drawableStateChanged()V

    .line 38
    iget-object v0, p0, Lcom/squareup/register/widgets/PickColorView;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 39
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->invalidate()V

    return-void
.end method

.method public isChecked()Z
    .locals 1

    .line 57
    iget-boolean v0, p0, Lcom/squareup/register/widgets/PickColorView;->checked:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/PickColorView;->selector:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 45
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/register/widgets/PickColorView;->checked:Z

    if-nez v0, :cond_1

    return-void

    .line 46
    :cond_1
    invoke-super {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/glyph/SquareGlyphView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 32
    invoke-super {p0, p1, p2, p3, p4}, Lcom/squareup/glyph/SquareGlyphView;->onSizeChanged(IIII)V

    .line 33
    iget-object p3, p0, Lcom/squareup/register/widgets/PickColorView;->selector:Landroid/graphics/drawable/StateListDrawable;

    const/4 p4, 0x0

    invoke-virtual {p3, p4, p4, p1, p2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 50
    iget-boolean v0, p0, Lcom/squareup/register/widgets/PickColorView;->checked:Z

    if-eq v0, p1, :cond_0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/register/widgets/PickColorView;->checked:Z

    .line 52
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorView;->invalidate()V

    :cond_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/register/widgets/PickColorView;->checked:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/PickColorView;->setChecked(Z)V

    return-void
.end method
