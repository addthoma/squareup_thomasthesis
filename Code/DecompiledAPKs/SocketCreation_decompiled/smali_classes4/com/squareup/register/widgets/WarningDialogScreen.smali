.class public final Lcom/squareup/register/widgets/WarningDialogScreen;
.super Lcom/squareup/container/ContainerTreeKey;
.source "WarningDialogScreen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/register/widgets/WarningDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/WarningDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/WarningDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final cancelable:Z

.field private final name:Ljava/lang/String;

.field public final warning:Lcom/squareup/widgets/warning/Warning;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 71
    sget-object v0, Lcom/squareup/register/widgets/-$$Lambda$WarningDialogScreen$tdRkZmc44xC5-4pFcoQcu35BfzE;->INSTANCE:Lcom/squareup/register/widgets/-$$Lambda$WarningDialogScreen$tdRkZmc44xC5-4pFcoQcu35BfzE;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/register/widgets/WarningDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/widgets/warning/Warning;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->warning:Lcom/squareup/widgets/warning/Warning;

    const/4 p1, 0x1

    .line 33
    iput-boolean p1, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->cancelable:Z

    const/4 p1, 0x0

    .line 34
    iput-object p1, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->warning:Lcom/squareup/widgets/warning/Warning;

    .line 44
    iput-boolean p2, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->cancelable:Z

    .line 45
    iput-object p3, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->name:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/register/widgets/WarningDialogScreen;
    .locals 1

    .line 72
    const-class v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/widgets/warning/Warning;

    .line 73
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 67
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->warning:Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/register/widgets/WarningDialogScreen;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method
