.class public Lcom/squareup/register/widgets/card/CardEditorEditText;
.super Lcom/squareup/widgets/SensitiveEditText;
.source "CardEditorEditText.java"

# interfaces
.implements Lcom/squareup/text/OnInvalidContentListener;


# instance fields
.field private final shakeAnim:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/SensitiveEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance p1, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {p1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardEditorEditText;->shakeAnim:Landroid/view/animation/Animation;

    .line 21
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardEditorEditText;->shakeAnim:Landroid/view/animation/Animation;

    new-instance p2, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method


# virtual methods
.method public onInvalidContent()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardEditorEditText;->shakeAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/CardEditorEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
