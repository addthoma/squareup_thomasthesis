.class public interface abstract Lcom/squareup/register/widgets/card/OnCardListener;
.super Ljava/lang/Object;
.source "OnCardListener.java"


# virtual methods
.method public abstract onCardChanged(Lcom/squareup/register/widgets/card/PartialCard;)V
.end method

.method public abstract onCardInvalid(Lcom/squareup/Card$PanWarning;)V
.end method

.method public abstract onCardValid(Lcom/squareup/Card;)V
.end method

.method public abstract onChargeCard(Lcom/squareup/Card;)V
.end method

.method public abstract onPanValid(Lcom/squareup/Card;Z)Z
.end method
