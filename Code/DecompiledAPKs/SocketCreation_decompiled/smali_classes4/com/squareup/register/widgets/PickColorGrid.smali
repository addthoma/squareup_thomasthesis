.class public Lcom/squareup/register/widgets/PickColorGrid;
.super Lcom/squareup/register/widgets/RectangularGridLayout;
.source "PickColorGrid.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;
    }
.end annotation


# instance fields
.field private colorViewBackground:Landroid/graphics/drawable/Drawable;

.field private listener:Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;

.field private selectedColor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/RectangularGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->colorViewBackground:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public addColor(ILjava/lang/String;)V
    .locals 0

    .line 96
    invoke-static {p1}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public addColor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 109
    invoke-static {p1}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;)I

    move-result v0

    .line 110
    new-instance v1, Lcom/squareup/register/widgets/PickColorView;

    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/PickColorView;-><init>(Landroid/content/Context;)V

    .line 111
    iget-object v2, p0, Lcom/squareup/register/widgets/PickColorGrid;->colorViewBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 112
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 113
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 114
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/PickColorView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/PickColorView;->setBackgroundColor(I)V

    :goto_0
    const/4 v0, 0x0

    .line 118
    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/PickColorView;->setChecked(Z)V

    .line 119
    invoke-virtual {v1, p0}, Lcom/squareup/register/widgets/PickColorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    invoke-virtual {v1, p2}, Lcom/squareup/register/widgets/PickColorView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {p0, v1}, Lcom/squareup/register/widgets/PickColorGrid;->addView(Landroid/view/View;)V

    .line 122
    sget p2, Lcom/squareup/widgets/pos/R$id;->hex_color:I

    invoke-virtual {v1, p2, p1}, Lcom/squareup/register/widgets/PickColorView;->setTag(ILjava/lang/Object;)V

    return-void
.end method

.method public addColorsForCatalogObjects()V
    .locals 3

    .line 127
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 128
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_gray:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 129
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_light_green:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_light_green:I

    .line 130
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 131
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_green:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_green:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 132
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_cyan:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_cyan:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 133
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_blue:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_blue:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 135
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_purple:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_purple:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 136
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_pink:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_pink:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 137
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_red:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_red:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 138
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_yellow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_yellow:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    .line 139
    sget v1, Lcom/squareup/widgets/pos/R$color;->edit_item_brown:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/squareup/widgets/pos/R$string;->color_brown:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/squareup/register/widgets/PickColorGrid;->addColor(ILjava/lang/String;)V

    return-void
.end method

.method public clearColors()V
    .locals 0

    .line 143
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->removeAllViews()V

    return-void
.end method

.method public getSelectedColor()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 29
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_1

    .line 30
    move-object v0, p1

    check-cast v0, Landroid/widget/Checkable;

    .line 31
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 33
    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_3

    .line 39
    invoke-virtual {p0, v2}, Lcom/squareup/register/widgets/PickColorGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v3, p1, :cond_2

    .line 40
    instance-of v4, v3, Landroid/widget/Checkable;

    if-eqz v4, :cond_2

    .line 41
    check-cast v3, Landroid/widget/Checkable;

    .line 42
    invoke-interface {v3}, Landroid/widget/Checkable;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 43
    invoke-interface {v3, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 48
    :cond_3
    sget v0, Lcom/squareup/widgets/pos/R$id;->hex_color:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    .line 49
    iget-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->listener:Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;->onColorChanged(Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public setColorViewBackground(I)V
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->colorViewBackground:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setOnColorChangeListener(Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->listener:Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;

    return-void
.end method

.method public setSelectedColor(Ljava/lang/String;)V
    .locals 4

    .line 53
    iput-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/register/widgets/PickColorGrid;->getChildCount()I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    .line 56
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/PickColorGrid;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 57
    instance-of v2, v1, Landroid/widget/Checkable;

    if-eqz v2, :cond_0

    .line 58
    move-object v2, v1

    check-cast v2, Landroid/widget/Checkable;

    .line 60
    sget v3, Lcom/squareup/widgets/pos/R$id;->hex_color:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-interface {v2, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    iget-object p1, p0, Lcom/squareup/register/widgets/PickColorGrid;->listener:Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/squareup/register/widgets/PickColorGrid;->selectedColor:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;->onColorChanged(Ljava/lang/String;)V

    :cond_2
    return-void
.end method
