.class public final Lcom/squareup/redeemrewards/RedeemRewardCompositeViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RedeemRewardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardCompositeViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "redeemRewardViewFactory",
        "Lcom/squareup/redeemrewards/RedeemRewardViewFactory;",
        "redeemRewardByCodeViewFactory",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;",
        "addEligibleItemForCouponViewFactory",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;",
        "(Lcom/squareup/redeemrewards/RedeemRewardViewFactory;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardViewFactory;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeViewFactory;Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponViewFactory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "redeemRewardViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redeemRewardByCodeViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addEligibleItemForCouponViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 15
    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 16
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x1

    aput-object p2, v0, p1

    .line 17
    check-cast p3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x2

    aput-object p3, v0, p1

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
