.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $response:Lcom/squareup/loyalty/LoyaltyStatusResponse;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;Lcom/squareup/loyalty/LoyaltyStatusResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->$response:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    .line 244
    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    iget-object v1, v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 245
    iget-object v2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    iget-object v2, v2, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iget-object v3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    iget-object v3, v3, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->$response:Lcom/squareup/loyalty/LoyaltyStatusResponse;

    const-string v5, "response"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    invoke-static {v2, v3, v4}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$createInitialViewRewardsProps(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;)Lcom/squareup/redeemrewards/ViewRewardsProps;

    move-result-object v2

    .line 246
    iget-object v3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;

    iget-object v3, v3, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$this_loyaltyGetStatusForContact:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    invoke-virtual {v3}, Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;->getSearchTerm()Ljava/lang/String;

    move-result-object v3

    .line 243
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/redeemrewards/ViewRewardsProps;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
