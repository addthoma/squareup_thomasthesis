.class public final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRedeemRewardByCodeWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRedeemRewardByCodeWorkflow.kt\ncom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 6 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,250:1\n133#1:263\n133#1:269\n133#1:275\n133#1:281\n32#2,12:251\n149#3,5:264\n149#3,5:270\n149#3,5:276\n149#3,5:282\n149#3,5:287\n85#4:292\n240#5:293\n276#6:294\n*E\n*S KotlinDebug\n*F\n+ 1 RealRedeemRewardByCodeWorkflow.kt\ncom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow\n*L\n93#1:263\n102#1:269\n115#1:275\n125#1:281\n70#1,12:251\n93#1,5:264\n102#1,5:270\n115#1,5:276\n125#1,5:282\n133#1,5:287\n147#1:292\n147#1:293\n147#1:294\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002B/\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0008\u0010 \u001a\u00020!H\u0002J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0002J\u001f\u0010(\u001a\u00020\u00042\u0006\u0010)\u001a\u00020\u00032\u0008\u0010*\u001a\u0004\u0018\u00010+H\u0016\u00a2\u0006\u0002\u0010,J\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020#0.2\u0006\u0010$\u001a\u00020%H\u0002JW\u0010/\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010)\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u00042\u0012\u00100\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000501H\u0016\u00a2\u0006\u0002\u00102J\u0010\u00103\u001a\u00020+2\u0006\u0010$\u001a\u00020\u0004H\u0016J@\u00104\u001a$\u0012\u0004\u0012\u000205\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u000205`\n\"\n\u0008\u0000\u00106\u0018\u0001*\u000207*\u0002H6H\u0082\u0008\u00a2\u0006\u0002\u00108J \u00109\u001a\u000e\u0012\u0004\u0012\u00020;\u0012\u0004\u0012\u00020;0:*\u00020<2\u0006\u0010=\u001a\u00020;H\u0002R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "holdsCoupons",
        "Lcom/squareup/payment/TransactionDiscountAdapter;",
        "couponsServiceHelper",
        "Lcom/squareup/loyalty/CouponsServiceHelper;",
        "formatter",
        "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;)V",
        "getCouponsServiceHelper",
        "()Lcom/squareup/loyalty/CouponsServiceHelper;",
        "getFeatures",
        "()Lcom/squareup/settings/server/Features;",
        "getFormatter",
        "()Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
        "getHoldsCoupons",
        "()Lcom/squareup/payment/TransactionDiscountAdapter;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "alphanumericAllowed",
        "",
        "handleSuccess",
        "Lcom/squareup/redeemrewards/bycode/Action;",
        "state",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;",
        "response",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
        "lookupCode",
        "Lcom/squareup/workflow/Worker;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "asCard",
        "Lcom/squareup/container/PosLayering;",
        "ScreenT",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;",
        "toTitleAndSubtitle",
        "Lkotlin/Pair;",
        "",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;",
        "code",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final couponsServiceHelper:Lcom/squareup/loyalty/CouponsServiceHelper;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private final holdsCoupons:Lcom/squareup/payment/TransactionDiscountAdapter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holdsCoupons"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponsServiceHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->holdsCoupons:Lcom/squareup/payment/TransactionDiscountAdapter;

    iput-object p4, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->couponsServiceHelper:Lcom/squareup/loyalty/CouponsServiceHelper;

    iput-object p5, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    return-void
.end method

.method public static final synthetic access$handleSuccess(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;)Lcom/squareup/redeemrewards/bycode/Action;
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->handleSuccess(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;)Lcom/squareup/redeemrewards/bycode/Action;

    move-result-object p0

    return-object p0
.end method

.method private final alphanumericAllowed()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ALPHANUMERIC_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final synthetic asCard(Lcom/squareup/workflow/legacy/V2Screen;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ScreenT::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(TScreenT;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 288
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    const/4 v1, 0x4

    const-string v2, "ScreenT"

    .line 289
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/workflow/legacy/V2Screen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 290
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 288
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 133
    sget-object p1, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final handleSuccess(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;)Lcom/squareup/redeemrewards/bycode/Action;
    .locals 4

    .line 154
    sget-object v0, Lcom/squareup/loyalty/CouponsServiceHelper;->Companion:Lcom/squareup/loyalty/CouponsServiceHelper$Companion;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    const-string v2, "response.coupon"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/CouponsServiceHelper$Companion;->getSupportedCoupons(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    if-eqz v0, :cond_0

    .line 156
    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->holdsCoupons:Lcom/squareup/payment/TransactionDiscountAdapter;

    iget-object v2, v0, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const-string v3, "usableCoupon.coupon_id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/squareup/payment/TransactionDiscountAdapter;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    new-instance p2, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;->getCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1, v0}, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon;)V

    check-cast p2, Lcom/squareup/redeemrewards/bycode/Action;

    goto/16 :goto_4

    .line 159
    :cond_0
    new-instance v1, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;

    .line 160
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;->getCode()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 162
    sget-object p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyInCart;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyInCart;

    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    goto :goto_3

    .line 164
    :cond_1
    iget-object v0, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    goto :goto_0

    :cond_2
    move-object v0, v2

    :goto_0
    sget-object v3, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->REDEEMED:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    if-ne v0, v3, :cond_4

    iget-object v0, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    goto :goto_1

    :cond_3
    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_4

    .line 165
    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyRedeemed;

    iget-object p2, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    iget-object p2, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->redeemed_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string v2, "response.reason.redeemed_at"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyRedeemed;-><init>(Lcom/squareup/protos/client/ISO8601Date;)V

    move-object p2, v0

    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    goto :goto_3

    .line 167
    :cond_4
    iget-object v0, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->type:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    goto :goto_2

    :cond_5
    move-object v0, v2

    :goto_2
    sget-object v3, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;->EXPIRED:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason$ReasonType;

    if-ne v0, v3, :cond_7

    iget-object v0, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    if-eqz v0, :cond_6

    iget-object v2, v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    :cond_6
    if-eqz v2, :cond_7

    .line 168
    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Expired;

    iget-object p2, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    iget-object p2, p2, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;->expired_at:Lcom/squareup/protos/client/ISO8601Date;

    const-string v2, "response.reason.expired_at"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Expired;-><init>(Lcom/squareup/protos/client/ISO8601Date;)V

    move-object p2, v0

    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    goto :goto_3

    .line 170
    :cond_7
    sget-object p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$NoRewardExists;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$NoRewardExists;

    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    .line 159
    :goto_3
    invoke-direct {v1, p1, p2}, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;-><init>(Ljava/lang/String;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;)V

    move-object p2, v1

    check-cast p2, Lcom/squareup/redeemrewards/bycode/Action;

    :goto_4
    return-object p2
.end method

.method private final lookupCode(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/redeemrewards/bycode/Action;",
            ">;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->couponsServiceHelper:Lcom/squareup/loyalty/CouponsServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/CouponsServiceHelper;->lookupV2(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;-><init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "couponsServiceHelper.loo\u2026or)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 293
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 294
    const-class v0, Lcom/squareup/redeemrewards/bycode/Action;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method

.method private final toTitleAndSubtitle(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;Ljava/lang/String;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;",
            "Ljava/lang/String;",
            ")",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 177
    new-instance v0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;-><init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Ljava/lang/String;)V

    .line 183
    sget-object p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyInCart;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyInCart;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p1, Lkotlin/Pair;

    .line 184
    sget p2, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_already_in_cart_title:I

    invoke-virtual {v0, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->invoke(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    .line 183
    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 187
    :cond_0
    instance-of p2, p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyRedeemed;

    if-eqz p2, :cond_1

    new-instance p2, Lkotlin/Pair;

    .line 188
    sget v1, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_already_redeemed_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->invoke(I)Ljava/lang/String;

    move-result-object v0

    .line 189
    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyRedeemed;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$AlreadyRedeemed;->getRedeemedDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatCouponAlreadyRedeemedDate(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/String;

    move-result-object p1

    .line 187
    invoke-direct {p2, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    move-object p1, p2

    goto :goto_1

    .line 191
    :cond_1
    instance-of p2, p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Expired;

    if-eqz p2, :cond_2

    new-instance p2, Lkotlin/Pair;

    .line 192
    sget v1, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_expired_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->invoke(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Expired;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Expired;->getExpirationDate()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatCouponExpirationDate(Lcom/squareup/protos/client/ISO8601Date;)Ljava/lang/String;

    move-result-object p1

    .line 191
    invoke-direct {p2, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 195
    :cond_2
    sget-object p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$NoRewardExists;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$NoRewardExists;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    new-instance p1, Lkotlin/Pair;

    .line 196
    sget p2, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_not_found_title:I

    invoke-virtual {v0, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$toTitleAndSubtitle$1;->invoke(I)Ljava/lang/String;

    move-result-object p2

    .line 197
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_not_found_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 199
    :cond_3
    sget-object p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Error;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Error;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p1, Lkotlin/Pair;

    .line 200
    iget-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_failed:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 201
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_search_reward_not_found_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public final getCouponsServiceHelper()Lcom/squareup/loyalty/CouponsServiceHelper;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->couponsServiceHelper:Lcom/squareup/loyalty/CouponsServiceHelper;

    return-object v0
.end method

.method public final getFeatures()Lcom/squareup/settings/server/Features;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-object v0
.end method

.method public final getFormatter()Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    return-object v0
.end method

.method public final getHoldsCoupons()Lcom/squareup/payment/TransactionDiscountAdapter;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->holdsCoupons:Lcom/squareup/payment/TransactionDiscountAdapter;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 251
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 256
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 258
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 259
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 260
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 262
    :cond_3
    check-cast v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 70
    :cond_4
    new-instance p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;

    const-string p2, ""

    invoke-direct {p1, p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;-><init>(Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->render(Lkotlin/Unit;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
            "-",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 81
    instance-of v0, p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;

    const-string v1, ""

    if-eqz v0, :cond_0

    .line 82
    new-instance p3, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;

    .line 83
    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;->getCode()Ljava/lang/String;

    move-result-object p2

    .line 84
    invoke-direct {p0}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->alphanumericAllowed()Z

    move-result v0

    .line 85
    new-instance v2, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$1;

    invoke-direct {v2, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 86
    new-instance v3, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;-><init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 82
    invoke-direct {p3, p2, v0, v2, v3}, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;-><init>(Ljava/lang/String;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 265
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 266
    const-class p2, Lcom/squareup/redeemrewards/bycode/EnterRewardCodeScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 267
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 265
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 263
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 96
    :cond_0
    instance-of v0, p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    if-eqz v0, :cond_1

    .line 97
    move-object v0, p2

    check-cast v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    invoke-direct {p0, v0}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->lookupCode(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v2, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$3;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0, p2, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 99
    new-instance p2, Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen;

    .line 100
    new-instance p3, Lcom/squareup/util/ViewString$ResourceString;

    sget v0, Lcom/squareup/redeemrewards/bycode/impl/R$string;->enter_reward_code_title:I

    invoke-direct {p3, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p3, Lcom/squareup/util/ViewString;

    .line 101
    new-instance v0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$4;

    invoke-direct {v0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 99
    invoke-direct {p2, p3, v0}, Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 271
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 272
    const-class p3, Lcom/squareup/redeemrewards/bycode/LoadingRewardsScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 273
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 271
    invoke-direct {p1, p3, p2, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 269
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 105
    :cond_1
    instance-of p3, p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;

    if-eqz p3, :cond_2

    .line 106
    new-instance p3, Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen;

    .line 107
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    move-object v2, p2

    check-cast v2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatShortestDescription(Lcom/squareup/api/items/Discount;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 108
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 109
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/redeemrewards/bycode/impl/R$string;->coupon_redeem_reward_subtitle:I

    invoke-interface {v0, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 110
    invoke-virtual {v2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;->getCode()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string v5, "code"

    invoke-virtual {v0, v5, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 113
    new-instance v0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;

    invoke-direct {v0, p1, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$5;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 114
    new-instance p2, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$6;

    invoke-direct {p2, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$6;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v7, p2

    check-cast v7, Lkotlin/jvm/functions/Function0;

    move-object v2, p3

    .line 106
    invoke-direct/range {v2 .. v7}, Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 277
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 278
    const-class p2, Lcom/squareup/redeemrewards/bycode/DisplayRewardScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 279
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 277
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 275
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 118
    :cond_2
    instance-of p3, p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;

    if-eqz p3, :cond_3

    .line 119
    check-cast p2, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;->getReason()Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    move-result-object p3

    invoke-virtual {p2}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;->getCode()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p3, p2}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->toTitleAndSubtitle(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;Ljava/lang/String;)Lkotlin/Pair;

    move-result-object p2

    .line 120
    new-instance p3, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;

    .line 121
    invoke-virtual {p2}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122
    invoke-virtual {p2}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 123
    new-instance v2, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$7;

    invoke-direct {v2, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$7;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 124
    new-instance v3, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$8;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$8;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 120
    invoke-direct {p3, v0, p2, v2, v3}, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;-><init>(Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p3, Lcom/squareup/workflow/legacy/V2Screen;

    .line 283
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 284
    const-class p2, Lcom/squareup/redeemrewards/bycode/NoRewardScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 285
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 283
    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 281
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->snapshotState(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
