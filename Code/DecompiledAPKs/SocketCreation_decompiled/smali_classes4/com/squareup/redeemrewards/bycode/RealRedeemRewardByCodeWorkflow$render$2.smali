.class final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->render(Lkotlin/Unit;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "code",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 3

    const-string v0, "code"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/redeemrewards/bycode/impl/R$integer;->reward_code_length:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/redeemrewards/bycode/Action$LookupCode;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/bycode/Action$LookupCode;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$render$2;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
