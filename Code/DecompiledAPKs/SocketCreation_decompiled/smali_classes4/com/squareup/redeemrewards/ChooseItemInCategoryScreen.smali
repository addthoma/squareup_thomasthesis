.class public final Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;
.super Lcom/squareup/redeemrewards/InRedeemRewardScope;
.source "ChooseItemInCategoryScreen.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseItemInCategoryScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseItemInCategoryScreen.kt\ncom/squareup/redeemrewards/ChooseItemInCategoryScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,41:1\n43#2:42\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseItemInCategoryScreen.kt\ncom/squareup/redeemrewards/ChooseItemInCategoryScreen\n*L\n31#1:42\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\u0011B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0008\u001a\u00020\t8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\n\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;",
        "Lcom/squareup/redeemrewards/InRedeemRewardScope;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/container/LayoutScreen;",
        "redeemRewardsScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V",
        "isPersistent",
        "",
        "()Z",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Runner",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 1

    const-string v0, "redeemRewardsScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/InRedeemRewardScope;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    .line 31
    invoke-interface {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;->chooseItemInCategoryCoordinator()Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 28
    sget v0, Lcom/squareup/redeemrewards/R$layout;->crm_choose_reward_item:I

    return v0
.end method
