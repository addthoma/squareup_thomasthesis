.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)I
    .locals 1

    .line 233
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;->call(Lcom/squareup/loyalty/LoyaltyStatusResponse;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
