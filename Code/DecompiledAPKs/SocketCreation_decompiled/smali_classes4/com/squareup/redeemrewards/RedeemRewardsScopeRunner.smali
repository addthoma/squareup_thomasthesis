.class public final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;
.implements Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;
.implements Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;
.implements Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;
.implements Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Runner;
.implements Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,607:1\n1265#2,12:608\n1265#2,12:620\n1265#2,12:632\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner\n*L\n600#1,12:608\n604#1,12:620\n604#1,12:632\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d2\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007B\u00bb\u0001\u0008\u0001\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0013\u0012\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u0012\u0006\u0010.\u001a\u00020/\u0012\u0006\u00100\u001a\u000201\u0012\u0006\u00102\u001a\u000203\u00a2\u0006\u0002\u00104J\u0008\u0010^\u001a\u00020_H\u0016J\u0018\u0010`\u001a\u00020;2\u0006\u0010a\u001a\u00020b2\u0006\u0010c\u001a\u00020dH\u0016J\u0010\u0010e\u001a\u00020;2\u0006\u0010>\u001a\u00020@H\u0002J\u0010\u0010f\u001a\u00020;2\u0006\u0010g\u001a\u00020hH\u0016J\u0010\u0010i\u001a\u00020;2\u0006\u0010j\u001a\u000207H\u0016J\u0010\u0010k\u001a\u00020;2\u0006\u0010j\u001a\u000207H\u0016J\u0008\u0010l\u001a\u00020;H\u0016J\u0008\u0010m\u001a\u00020;H\u0016J\u0008\u0010n\u001a\u00020;H\u0016J\u0008\u0010o\u001a\u00020;H\u0016J\u0008\u0010p\u001a\u00020;H\u0016J\u0008\u0010q\u001a\u00020;H\u0016J\u0008\u0010r\u001a\u00020;H\u0016J\u0010\u0010r\u001a\u00020;2\u0006\u0010X\u001a\u00020HH\u0016J\u0008\u0010s\u001a\u00020;H\u0016J \u0010t\u001a\n\u0012\u0004\u0012\u00020H\u0018\u00010]2\u000e\u0010u\u001a\n\u0012\u0004\u0012\u000207\u0018\u00010]H\u0002J\"\u0010v\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020d0x0w2\u000c\u0010y\u001a\u0008\u0012\u0004\u0012\u00020H0]H\u0002J\"\u0010z\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020d0x0w2\u000c\u0010{\u001a\u0008\u0012\u0004\u0012\u00020H0]H\u0002J\u000e\u0010|\u001a\u0008\u0012\u0004\u0012\u00020=0}H\u0016J\u0008\u0010~\u001a\u00020@H\u0002J\u0016\u0010\u007f\u001a\u0008\u0012\u0004\u0012\u00020H0]2\u0006\u0010j\u001a\u000207H\u0002J\u0018\u0010\u0080\u0001\u001a\t\u0012\u0005\u0012\u00030\u0081\u00010N2\u0006\u0010j\u001a\u000207H\u0002J\u0013\u0010\u0082\u0001\u001a\u00020;2\u0008\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0016J\t\u0010\u0085\u0001\u001a\u00020;H\u0016J\u000e\u0010M\u001a\u0008\u0012\u0004\u0012\u00020O0NH\u0016J\u0012\u0010\u0086\u0001\u001a\u00020;2\u0007\u0010\u0087\u0001\u001a\u00020HH\u0016J\u000f\u0010\u0088\u0001\u001a\u0008\u0012\u0004\u0012\u00020S0NH\u0016J\t\u0010\u0089\u0001\u001a\u00020;H\u0002J\u0011\u0010\u008a\u0001\u001a\u00020;2\u0006\u0010>\u001a\u00020@H\u0016J\u0012\u0010\u008b\u0001\u001a\u00020;2\u0007\u0010\u008c\u0001\u001a\u00020HH\u0016J\t\u0010\u008d\u0001\u001a\u00020;H\u0016J\t\u0010\u008e\u0001\u001a\u00020;H\u0016J\t\u0010\u008f\u0001\u001a\u00020;H\u0002R\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u00105\u001a\u0010\u0012\u000c\u0012\n 8*\u0004\u0018\u0001070706X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00109\u001a\u0008\u0012\u0004\u0012\u00020;0:X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010<\u001a\u0010\u0012\u000c\u0012\n 8*\u0004\u0018\u00010=0=06X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010>\u001a&\u0012\u000c\u0012\n 8*\u0004\u0018\u00010@0@ 8*\u0012\u0012\u000c\u0012\n 8*\u0004\u0018\u00010@0@\u0018\u00010?0?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010A\u001a\u00020BX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008C\u0010D\"\u0004\u0008E\u0010FR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010G\u001a\u00020HX\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008I\u0010J\"\u0004\u0008K\u0010LR\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u000203X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010M\u001a\u0008\u0012\u0004\u0012\u00020O0NX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010P\u001a\u0008\u0012\u0004\u0012\u00020Q0NX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010R\u001a\u0008\u0012\u0004\u0012\u00020S0NX\u0082.\u00a2\u0006\u0002\n\u0000R2\u0010T\u001a&\u0012\u000c\u0012\n 8*\u0004\u0018\u00010H0H 8*\u0012\u0012\u000c\u0012\n 8*\u0004\u0018\u00010H0H\u0018\u00010U0UX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010V\u001a\u00020WX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010X\u001a&\u0012\u000c\u0012\n 8*\u0004\u0018\u00010H0H 8*\u0012\u0012\u000c\u0012\n 8*\u0004\u0018\u00010H0H\u0018\u00010?0?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010Y\u001a\u00020BX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010Z\u001a&\u0012\u000c\u0012\n 8*\u0004\u0018\u00010\\0\\ 8*\u0012\u0012\u000c\u0012\n 8*\u0004\u0018\u00010\\0\\\u0018\u00010]0[X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0090\u0001"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;",
        "Lmortar/Scoped;",
        "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
        "Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Runner;",
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;",
        "flow",
        "Lflow/Flow;",
        "res",
        "Lcom/squareup/util/Res;",
        "contactLoader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "couponService",
        "Lcom/squareup/loyalty/CouponsServiceHelper;",
        "loyaltyService",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "holdsCustomer",
        "Lcom/squareup/payment/crm/HoldsCustomer;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "loyaltyCalculator",
        "Lcom/squareup/loyalty/LoyaltyCalculator;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "couponDiscountFormatter",
        "Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;",
        "rewardAdapterHelper",
        "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "redeemRewardsFlow",
        "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "accountSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "libraryListEntryHandler",
        "Lcom/squareup/librarylist/LibraryListEntryHandler;",
        "loyaltyRulesFormatter",
        "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
        "(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/payment/Transaction;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)V",
        "appliedCoupon",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "kotlin.jvm.PlatformType",
        "autocloseLookupRewardByCode",
        "Lcom/squareup/util/RxWatchdog;",
        "",
        "chooseItemScreenData",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
        "contact",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "contactListScrollPosition",
        "",
        "getContactListScrollPosition",
        "()I",
        "setContactListScrollPosition",
        "(I)V",
        "contactLoaderSearchTerm",
        "",
        "getContactLoaderSearchTerm",
        "()Ljava/lang/String;",
        "setContactLoaderSearchTerm",
        "(Ljava/lang/String;)V",
        "onLookupRewardResult",
        "Lrx/Observable;",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "onPointsResponse",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "redeemPointsV2ScreenData",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        "redeemRewardTierClicked",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "redeemScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "rewardCode",
        "shortAnimTime",
        "supportedCatalogTypes",
        "",
        "Lcom/squareup/api/items/Item$Type;",
        "",
        "actionBarUsesBackArrow",
        "",
        "addSelectedRewardItemToCart",
        "itemThumbnail",
        "Landroid/widget/ImageView;",
        "selectedItem",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "addThisCustomer",
        "addToCartFromDialog",
        "context",
        "Landroid/content/Context;",
        "applyCouponFromCode",
        "coupon",
        "applyCustomerCoupon",
        "cancelItemSelection",
        "closeAddItemDialog",
        "closeChooseCustomerScreen",
        "closeDisplayRewardByCodeScreen",
        "closeDisplayRewardByCodeScreenOnRedeemReward",
        "closeDisplayRewardByCodeScreenOnSearchAgain",
        "closeLookupRewardByCodeScreen",
        "closeRedeemPointsScreen",
        "getAllCouponConstraints",
        "coupons",
        "getAllItemsInCategory",
        "Lrx/Single;",
        "",
        "categoryIds",
        "getAllItemsInVariation",
        "variationIds",
        "getChooseItemScreenData",
        "Lio/reactivex/Observable;",
        "getContact",
        "getCouponConstraints",
        "hasCouponApplied",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "redeemPoints",
        "couponDefinitionToken",
        "redeemRewardsV2ScreenData",
        "removeCustomerFromSale",
        "selectContact",
        "setSearchTerm",
        "term",
        "showAddItemDialog",
        "showLookupRewardByCodeScreen",
        "startChooseCustomerToAdd",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field private final autocloseLookupRewardByCode:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final contact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private contactListScrollPosition:I

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private contactLoaderSearchTerm:Ljava/lang/String;

.field private final couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private final couponService:Lcom/squareup/loyalty/CouponsServiceHelper;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field private final holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

.field private final libraryListEntryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

.field private final loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

.field private final loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

.field private final loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private onLookupRewardResult:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
            ">;"
        }
    .end annotation
.end field

.field private onPointsResponse:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private redeemPointsV2ScreenData:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardTierClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

.field private redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

.field private final res:Lcom/squareup/util/Res;

.field private final rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

.field private final rewardCode:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final shortAnimTime:I

.field private final supportedCatalogTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/loyalty/CouponsServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/checkout/HoldsCoupons;Lcom/squareup/payment/crm/HoldsCustomer;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/LoyaltyCalculator;Lcom/squareup/payment/Transaction;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)V
    .locals 16
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p7    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "flow"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactLoader"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponService"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyService"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "threadEnforcer"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holdsCoupons"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "holdsCustomer"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyCalculator"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponDiscountFormatter"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardAdapterHelper"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    move-object/from16 v6, p16

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redeemRewardsFlow"

    move-object/from16 v6, p17

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    move-object/from16 v7, p18

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    move-object/from16 v7, p19

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountSettings"

    move-object/from16 v7, p20

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListEntryHandler"

    move-object/from16 v7, p21

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyRulesFormatter"

    move-object/from16 v7, p22

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v7, p16

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object v3, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iput-object v4, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->couponService:Lcom/squareup/loyalty/CouponsServiceHelper;

    iput-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object v8, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iput-object v9, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iput-object v10, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object v11, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    iput-object v12, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v13, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iput-object v14, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    iput-object v15, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iput-object v7, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object v6, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    move-object/from16 v1, p18

    move-object/from16 v2, p19

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    move-object/from16 v1, p20

    move-object/from16 v2, p21

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->libraryListEntryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    move-object/from16 v1, p22

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    .line 130
    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->res:Lcom/squareup/util/Res;

    const/high16 v2, 0x10e0000

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v1

    iput v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->shortAnimTime:I

    .line 132
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->rewardCode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 133
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 134
    new-instance v1, Lcom/squareup/util/RxWatchdog;

    move-object/from16 v2, p6

    move-object/from16 v3, p7

    invoke-direct {v1, v2, v3}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->autocloseLookupRewardByCode:Lcom/squareup/util/RxWatchdog;

    const-string v1, ""

    .line 137
    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoaderSearchTerm:Ljava/lang/String;

    .line 142
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemRewardTierClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 143
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay2.create<Re\u2026emRewardItemScreenData>()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 144
    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay2.createDefault(EMPTY_COUPON)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 145
    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->accountSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->supportedCatalogTypes:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$addThisCustomer(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->addThisCustomer(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public static final synthetic access$getAllCouponConstraints(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getAllCouponConstraints(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAllItemsInCategory(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Lrx/Single;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getAllItemsInCategory(Ljava/util/List;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAllItemsInVariation(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Lrx/Single;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getAllItemsInVariation(Ljava/util/List;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getChooseItemScreenData$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCouponConstraints(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)Ljava/util/List;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getCouponConstraints(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCouponService$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/CouponsServiceHelper;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->couponService:Lcom/squareup/loyalty/CouponsServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lflow/Flow;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getHoldsCustomer$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/payment/crm/HoldsCustomer;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyRulesFormatter$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/LoyaltyRulesFormatter;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyRulesFormatter:Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyService$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/LoyaltyServiceHelper;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyService:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltySettings$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/LoyaltySettings;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-object p0
.end method

.method public static final synthetic access$getPointsTermsFormatter$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-object p0
.end method

.method public static final synthetic access$getRedeemScope$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/RedeemRewardsScope;
    .locals 1

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez p0, :cond_0

    const-string v0, "redeemScope"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getSupportedCatalogTypes$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Ljava/util/List;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->supportedCatalogTypes:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$hasCouponApplied(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)Lrx/Observable;
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->hasCouponApplied(Lcom/squareup/protos/client/coupons/Coupon;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$removeCustomerFromSale(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->removeCustomerFromSale()V

    return-void
.end method

.method public static final synthetic access$setRedeemScope$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    return-void
.end method

.method public static final synthetic access$startChooseCustomerToAdd(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->startChooseCustomerToAdd()V

    return-void
.end method

.method private final addThisCustomer(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 522
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_CUSTOMER_ADDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 523
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, v1}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 524
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private final getAllCouponConstraints(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 604
    check-cast p1, Ljava/lang/Iterable;

    .line 620
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 627
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 628
    check-cast v1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 604
    iget-object v1, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v1, v1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 629
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 631
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 632
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 639
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 640
    check-cast v1, Lcom/squareup/protos/client/coupons/ItemConstraint;

    .line 604
    iget-object v1, v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 641
    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_1

    .line 643
    :cond_1
    check-cast p1, Ljava/util/List;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method private final getAllItemsInCategory(Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Single<",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    .line 576
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { cogsLoca\u2026      matchingItems\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getAllItemsInVariation(Ljava/util/List;)Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lrx/Single<",
            "Ljava/util/Set<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation

    .line 542
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInVariation$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInVariation$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { cogsLoca\u2026      matchingItems\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 2

    .line 509
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "contact"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "contact.value"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method private final getCouponConstraints(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 600
    iget-object p1, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    const-string v0, "coupon.reward.item_constraint"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 608
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 615
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 616
    check-cast v1, Lcom/squareup/protos/client/coupons/ItemConstraint;

    .line 600
    iget-object v1, v1, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 617
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 619
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final hasCouponApplied(Lcom/squareup/protos/client/coupons/Coupon;)Lrx/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    .line 411
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/checkout/HoldsCoupons;->isCouponAddedToCart(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    new-instance v0, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 413
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 414
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/redeemrewards/R$string;->applied_reward_to_cart:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 415
    sget-object v2, Lcom/squareup/loyalty/ui/RewardWrapper$Type$AppliedToCart;->INSTANCE:Lcom/squareup/loyalty/ui/RewardWrapper$Type$AppliedToCart;

    check-cast v2, Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    .line 412
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/loyalty/ui/RewardWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/ui/RewardWrapper$Type;)V

    goto :goto_0

    .line 418
    :cond_0
    new-instance v0, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 419
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->couponDiscountFormatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;->formatName(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 420
    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/redeemrewards/R$string;->apply_reward_to_cart:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 421
    new-instance v3, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Clickable;

    new-instance v4, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;

    invoke-direct {v4, p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$hasCouponApplied$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/squareup/protos/client/coupons/Coupon;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-direct {v3, v4}, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Clickable;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    .line 418
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/loyalty/ui/RewardWrapper;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyalty/ui/RewardWrapper$Type;)V

    .line 410
    :goto_0
    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string v0, "Observable.just(\n       \u2026        )\n        }\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final removeCustomerFromSale()V
    .locals 4

    .line 529
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_REMOVE_CUSTOMER:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 530
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v1, v1}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 531
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private final startChooseCustomerToAdd()V
    .locals 6

    .line 512
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_ADD_CUSTOMER_FLOW_STARTED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 515
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    .line 516
    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    .line 517
    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {v2}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    .line 518
    const-class v4, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 515
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public actionBarUsesBackArrow()Z
    .locals 2

    .line 507
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v0, :cond_0

    const-string v1, "redeemScope"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScope;->getFirstScreenIsCustomerList$redeem_rewards_release()Z

    move-result v0

    return v0
.end method

.method public addSelectedRewardItemToCart(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 2

    const-string v0, "itemThumbnail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItem"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 452
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->libraryListEntryHandler:Lcom/squareup/librarylist/LibraryListEntryHandler;

    const-string v1, ""

    invoke-interface {v0, p1, p2, v1}, Lcom/squareup/librarylist/LibraryListEntryHandler;->itemClicked(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Ljava/lang/String;)V

    .line 453
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 p2, 0x5

    new-array p2, p2, [Ljava/lang/Class;

    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;

    const/4 v1, 0x0

    aput-object v0, p2, v1

    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    const/4 v1, 0x1

    aput-object v0, p2, v1

    .line 454
    const-class v0, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    const/4 v1, 0x2

    aput-object v0, p2, v1

    const-class v0, Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;

    const/4 v1, 0x3

    aput-object v0, p2, v1

    .line 455
    const-class v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;

    const/4 v1, 0x4

    aput-object v0, p2, v1

    .line 453
    invoke-static {p1, p2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 457
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public addToCartFromDialog(Landroid/content/Context;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;->getItemsData()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 444
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 445
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;->getItemsData()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 444
    invoke-virtual {p0, v0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->addSelectedRewardItemToCart(Landroid/widget/ImageView;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    goto :goto_0

    .line 447
    :cond_1
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v1, :cond_2

    const-string v2, "redeemScope"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public applyCouponFromCode(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 395
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->hasQualifyingItem(Lcom/squareup/protos/client/coupons/Coupon;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 398
    :cond_0
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 399
    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 400
    const-class v2, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    aput-object v2, v0, v1

    .line 398
    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public applyCustomerCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3

    const-string v0, "coupon"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_CHOOSE_COUPON:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 382
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 383
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-direct {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 384
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_ITEM_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->hasQualifyingItem(Lcom/squareup/protos/client/coupons/Coupon;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 387
    :cond_0
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 388
    const-class v2, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 389
    const-class v2, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    aput-object v2, v0, v1

    .line 387
    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public cancelItemSelection()V
    .locals 5

    .line 461
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {v0, v2}, Lcom/squareup/checkout/HoldsCoupons;->remove(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 463
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen;

    aput-object v4, v2, v3

    const-class v3, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;

    aput-object v3, v2, v1

    const/4 v1, 0x2

    .line 466
    const-class v3, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-class v3, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;

    aput-object v3, v2, v1

    .line 465
    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeAddItemDialog()V
    .locals 4

    .line 435
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {v0, v2}, Lcom/squareup/checkout/HoldsCoupons;->remove(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 437
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->getEMPTY_COUPON()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeChooseCustomerScreen()V
    .locals 2

    .line 367
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeDisplayRewardByCodeScreen()V
    .locals 2

    .line 489
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeDisplayRewardByCodeScreenOnRedeemReward()V
    .locals 4

    .line 497
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    .line 498
    const-class v2, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 499
    const-class v2, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 497
    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeDisplayRewardByCodeScreenOnSearchAgain()V
    .locals 4

    .line 493
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v2, :cond_0

    const-string v3, "redeemScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public closeLookupRewardByCodeScreen()V
    .locals 2

    .line 480
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeLookupRewardByCodeScreen(Ljava/lang/String;)V
    .locals 4

    const-string v0, "rewardCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->rewardCode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 485
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->autocloseLookupRewardByCode:Lcom/squareup/util/RxWatchdog;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iget v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->shortAnimTime:I

    int-to-long v1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public closeRedeemPointsScreen()V
    .locals 2

    .line 377
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getChooseItemScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
            ">;"
        }
    .end annotation

    .line 432
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->chooseItemScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public getContactListScrollPosition()I
    .locals 1

    .line 136
    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactListScrollPosition:I

    return v0
.end method

.method public getContactLoaderSearchTerm()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoaderSearchTerm:Ljava/lang/String;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "scope"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-static/range {p1 .. p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v2

    const-string v3, "RegisterTreeKey.get(scope)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/redeemrewards/RedeemRewardsScope;

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    .line 150
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3}, Lcom/squareup/crm/RolodexContactLoader;->setRestrictToSearchingOnly(Z)V

    .line 151
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    check-cast v2, Lmortar/Scoped;

    invoke-virtual {v1, v2}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 154
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->rewardCode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 155
    new-instance v3, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1;

    invoke-direct {v3, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    const/4 v3, 0x1

    .line 167
    invoke-virtual {v2, v3}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object v2

    .line 168
    invoke-virtual {v2, v3}, Lrx/observables/ConnectableObservable;->autoConnect(I)Lrx/Observable;

    move-result-object v2

    .line 169
    sget-object v5, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$2;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$2;

    check-cast v5, Lrx/functions/Func1;

    invoke-virtual {v2, v5}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    const-string v5, "rewardCode\n        .swit\u2026ookupRewardResult.EMPTY }"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onLookupRewardResult:Lrx/Observable;

    .line 172
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onLookupRewardResult:Lrx/Observable;

    if-nez v2, :cond_0

    const-string v5, "onLookupRewardResult"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v5, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;

    invoke-direct {v5, v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lmortar/MortarScope;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v1, v5}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 184
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v2, :cond_1

    const-string v5, "redeemScope"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/redeemrewards/RedeemRewardsScope;->getFirstScreenIsCustomerList$redeem_rewards_release()Z

    move-result v2

    if-nez v2, :cond_3

    .line 186
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {v5}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt;->access$getNO_CONTACT$p()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v5

    :goto_0
    invoke-virtual {v2, v5}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 190
    :cond_3
    invoke-static {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    .line 191
    invoke-static {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v4

    .line 193
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 195
    sget-object v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$4;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$4;

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    .line 196
    new-instance v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;

    invoke-direct {v6, v0, v2}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/jakewharton/rxrelay/BehaviorRelay;)V

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    .line 203
    iget-object v6, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$6;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$6;

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v6, v7}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v6

    .line 204
    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$7;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$7;

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v6, v7}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v6

    .line 203
    invoke-virtual {v5, v6}, Lrx/Observable;->mergeWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v5

    .line 205
    invoke-virtual {v5, v3}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object v5

    .line 206
    new-instance v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$8;

    invoke-direct {v6, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$8;-><init>(Lmortar/MortarScope;)V

    check-cast v6, Lrx/functions/Action1;

    invoke-virtual {v5, v3, v6}, Lrx/observables/ConnectableObservable;->autoConnect(ILrx/functions/Action1;)Lrx/Observable;

    move-result-object v5

    const-string v6, "contact\n        // If co\u2026.unsubscribeOnExit(sub) }"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onPointsResponse:Lrx/Observable;

    .line 209
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionTitle$1;

    invoke-direct {v6, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionTitle$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v8

    .line 217
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 218
    new-instance v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;

    invoke-direct {v6, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$actionBarAction$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v9

    .line 228
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->loyaltyCalculator:Lcom/squareup/loyalty/LoyaltyCalculator;

    iget-object v6, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v5, v6}, Lcom/squareup/loyalty/LoyaltyCalculator;->calculateCartPoints(Lcom/squareup/payment/Transaction;)Ljava/lang/Integer;

    move-result-object v5

    .line 227
    invoke-static {v5}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v5

    .line 229
    sget-object v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$cartPointTotal$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$cartPointTotal$1;

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v11

    .line 232
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onPointsResponse:Lrx/Observable;

    const-string v6, "onPointsResponse"

    if-nez v5, :cond_4

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 233
    :cond_4
    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$contactPointTotal$1;

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v5, v7}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v12

    .line 237
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$1;

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v5, v7}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    .line 238
    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$2;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$2;

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v12, v7}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v7

    .line 240
    new-instance v10, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$3;

    invoke-direct {v10, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$pointsMessage$3;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v10, Lrx/functions/Func3;

    .line 236
    invoke-static {v5, v7, v11, v10}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v13

    .line 256
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onPointsResponse:Lrx/Observable;

    if-nez v5, :cond_5

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 257
    :cond_5
    new-instance v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;

    invoke-direct {v7, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$coupons$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v5, v7}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v14

    .line 271
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    const-string v7, "contactPointTotal"

    .line 272
    invoke-static {v12, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    iget-object v7, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    .line 274
    new-instance v10, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$rewardTiers$1;

    invoke-direct {v10, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$rewardTiers$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 271
    invoke-virtual {v5, v12, v7, v10}, Lcom/squareup/loyalty/ui/RewardAdapterHelper;->createRewardWrapperList(Lrx/Observable;Lcom/squareup/checkout/HoldsCoupons;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object v15

    .line 277
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemRewardTierClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 279
    iget-object v7, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onPointsResponse:Lrx/Observable;

    if-nez v7, :cond_6

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 280
    :cond_6
    const-class v10, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {v7, v10}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v7

    const-string v10, "onPointsResponse\n       \u2026e(HasLoyalty::class.java)"

    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 281
    sget-object v10, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$1;

    check-cast v10, Lkotlin/jvm/functions/Function1;

    invoke-static {v7, v10}, Lcom/squareup/util/rx/RxKt;->mapNotNull(Lrx/Observable;Lkotlin/jvm/functions/Function1;)Lrx/Observable;

    move-result-object v7

    sget-object v10, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$2;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$2;

    check-cast v10, Lrx/functions/Func2;

    .line 278
    invoke-virtual {v5, v7, v10}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v5

    .line 282
    new-instance v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$3;

    invoke-direct {v7, v0, v4}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$3;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/jakewharton/rxrelay/BehaviorRelay;)V

    check-cast v7, Lrx/functions/Func1;

    invoke-virtual {v5, v7}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    .line 288
    invoke-virtual {v5, v3}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object v5

    .line 289
    new-instance v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$4;

    invoke-direct {v7, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$redeemPointsResponse$4;-><init>(Lmortar/MortarScope;)V

    check-cast v7, Lrx/functions/Action1;

    invoke-virtual {v5, v3, v7}, Lrx/observables/ConnectableObservable;->autoConnect(ILrx/functions/Action1;)Lrx/Observable;

    move-result-object v3

    const-string v5, "redeemPointsResponse"

    .line 291
    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$9;

    invoke-direct {v5, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$9;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v1, v5}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 298
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->appliedCoupon:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 299
    sget-object v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$10;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$10;

    check-cast v7, Lio/reactivex/functions/Predicate;

    invoke-virtual {v5, v7}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v5

    const-string v7, "appliedCoupon\n        .f\u2026 coupon != EMPTY_COUPON }"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    new-instance v7, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;

    invoke-direct {v7, v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$11;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lmortar/MortarScope;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-static {v5, v1, v7}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 326
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onPointsResponse:Lrx/Observable;

    if-nez v5, :cond_7

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;

    invoke-direct {v6, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v6, Lrx/functions/Func2;

    invoke-static {v5, v3, v6}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v3

    .line 342
    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v6, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$2;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$errorString$2;

    check-cast v6, Lrx/functions/Func1;

    invoke-virtual {v5, v6}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v5

    invoke-virtual {v3, v5}, Lrx/Observable;->mergeWith(Lrx/Observable;)Lrx/Observable;

    move-result-object v10

    .line 346
    check-cast v2, Lrx/Observable;

    check-cast v4, Lrx/Observable;

    sget-object v3, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$12;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$12;

    check-cast v3, Lrx/functions/Func2;

    invoke-static {v2, v4, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v7

    .line 355
    sget-object v2, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$13;

    check-cast v2, Lkotlin/jvm/functions/Function9;

    if-eqz v2, :cond_8

    new-instance v3, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt$sam$rx_functions_Func9$0;

    invoke-direct {v3, v2}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunnerKt$sam$rx_functions_Func9$0;-><init>(Lkotlin/jvm/functions/Function9;)V

    move-object v2, v3

    :cond_8
    move-object/from16 v16, v2

    check-cast v16, Lrx/functions/Func9;

    .line 345
    invoke-static/range {v7 .. v16}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func9;)Lrx/Observable;

    move-result-object v2

    const-string v3, "combineLatest(\n         \u2026ardsV2Screen::ScreenData)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemPointsV2ScreenData:Lrx/Observable;

    .line 357
    iget-object v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->autocloseLookupRewardByCode:Lcom/squareup/util/RxWatchdog;

    .line 358
    invoke-virtual {v2}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "autocloseLookupRewardByCode\n        .timeout()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    new-instance v3, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;

    invoke-direct {v3, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v1, v3}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLookupRewardResult()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
            ">;"
        }
    .end annotation

    .line 502
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onLookupRewardResult:Lrx/Observable;

    if-nez v0, :cond_0

    const-string v1, "onLookupRewardResult"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public redeemPoints(Ljava/lang/String;)V
    .locals 2

    const-string v0, "couponDefinitionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->LOYALTY_CART_POINTS_MODAL_APPLY_REWARD:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 406
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemRewardTierClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public redeemRewardsV2ScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 505
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemPointsV2ScreenData:Lrx/Observable;

    if-nez v0, :cond_0

    const-string v1, "redeemPointsV2ScreenData"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public selectContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    const-string v0, "contact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 373
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v1, :cond_0

    const-string v2, "redeemScope"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setContactListScrollPosition(I)V
    .locals 0

    .line 136
    iput p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactListScrollPosition:I

    return-void
.end method

.method public setContactLoaderSearchTerm(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->contactLoaderSearchTerm:Ljava/lang/String;

    return-void
.end method

.method public setSearchTerm(Ljava/lang/String;)V
    .locals 2

    const-string v0, "term"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/redeemrewards/log/SearchRewardsCustomerEvent;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/log/SearchRewardsCustomerEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 476
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->setContactLoaderSearchTerm(Ljava/lang/String;)V

    return-void
.end method

.method public showAddItemDialog()V
    .locals 4

    .line 428
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v2, :cond_0

    const-string v3, "redeemScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showLookupRewardByCodeScreen()V
    .locals 4

    .line 470
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CHECKOUT_REWARDS_USE_CODE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 471
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->redeemScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    if-nez v2, :cond_0

    const-string v3, "redeemScope"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
