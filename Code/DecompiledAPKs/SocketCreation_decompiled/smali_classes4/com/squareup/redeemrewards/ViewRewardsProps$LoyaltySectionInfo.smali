.class public final Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
.super Ljava/lang/Object;
.source "ViewRewardsProps.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/ViewRewardsProps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltySectionInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0006H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J7\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0001J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0006H\u00d6\u0001J\u0019\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000c\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;",
        "Landroid/os/Parcelable;",
        "currentPoints",
        "",
        "totalUsablePoints",
        "phoneToken",
        "",
        "rewardTierStates",
        "",
        "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
        "(IILjava/lang/String;Ljava/util/List;)V",
        "getCurrentPoints",
        "()I",
        "getPhoneToken",
        "()Ljava/lang/String;",
        "getRewardTierStates",
        "()Ljava/util/List;",
        "getTotalUsablePoints",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final currentPoints:I

.field private final phoneToken:Ljava/lang/String;

.field private final rewardTierStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
            ">;"
        }
    .end annotation
.end field

.field private final totalUsablePoints:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo$Creator;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo$Creator;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
            ">;)V"
        }
    .end annotation

    const-string v0, "phoneToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardTierStates"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    iput p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    iput-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;IILjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget p1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->copy(IILjava/lang/String;Ljava/util/List;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    return-object v0
.end method

.method public final copy(IILjava/lang/String;Ljava/util/List;)Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
            ">;)",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;"
        }
    .end annotation

    const-string v0, "phoneToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardTierStates"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;-><init>(IILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;

    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    iget v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    iget v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCurrentPoints()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    return v0
.end method

.method public final getPhoneToken()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getRewardTierStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    return-object v0
.end method

.method public final getTotalUsablePoints()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoyaltySectionInfo(currentPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", totalUsablePoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", phoneToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", rewardTierStates="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->currentPoints:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->totalUsablePoints:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->phoneToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsProps$LoyaltySectionInfo;->rewardTierStates:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/ViewRewardsProps$RewardTierState;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
