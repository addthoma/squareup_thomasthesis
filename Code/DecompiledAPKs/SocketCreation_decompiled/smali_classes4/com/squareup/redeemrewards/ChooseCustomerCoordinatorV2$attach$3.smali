.class public final Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ChooseCustomerCoordinatorV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseCustomerCoordinatorV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseCustomerCoordinatorV2.kt\ncom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,113:1\n45#2:114\n17#2,22:115\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseCustomerCoordinatorV2.kt\ncom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3\n*L\n56#1:114\n56#1,22:115\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "afterTextChanged",
        "",
        "editable",
        "Landroid/text/Editable;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    const-string v0, "editable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 114
    check-cast p1, Ljava/lang/CharSequence;

    .line 116
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, 0x0

    move v3, v0

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    if-gt v0, v3, :cond_5

    if-nez v4, :cond_0

    move v5, v0

    goto :goto_1

    :cond_0
    move v5, v3

    .line 121
    :goto_1
    invoke-interface {p1, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-gt v5, v6, :cond_1

    const/4 v5, 0x1

    goto :goto_2

    :cond_1
    const/4 v5, 0x0

    :goto_2
    if-nez v4, :cond_3

    if-nez v5, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    if-nez v5, :cond_4

    goto :goto_3

    :cond_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_5
    :goto_3
    add-int/2addr v3, v1

    .line 136
    invoke-interface {p1, v0, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p1

    .line 114
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->access$getContactLoader$p(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v0

    new-instance v2, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    xor-int/2addr v1, v3

    invoke-direct {v2, p1, v1}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;->this$0:Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    invoke-static {v0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->access$getRunner$p(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;->setSearchTerm(Ljava/lang/String;)V

    return-void
.end method
