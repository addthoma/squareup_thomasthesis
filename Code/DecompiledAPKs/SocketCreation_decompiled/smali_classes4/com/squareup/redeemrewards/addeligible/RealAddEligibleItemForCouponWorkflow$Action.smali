.class abstract Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;
.super Ljava/lang/Object;
.source "RealAddEligibleItemForCouponWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$Cancel;,
        Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$AddMatchingItem;,
        Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;,
        Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ShowChooseItemScreen;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddEligibleItemForCouponWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddEligibleItemForCouponWorkflow.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,167:1\n950#2:168\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddEligibleItemForCouponWorkflow.kt\ncom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action\n*L\n147#1:168\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "AddMatchingItem",
        "Cancel",
        "ItemsFetched",
        "ShowChooseItemScreen",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$Cancel;",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$AddMatchingItem;",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ShowChooseItemScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
            ">;)",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$Cancel;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$Cancel;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$Cancelled;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$Cancelled;

    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    return-object p1

    .line 142
    :cond_0
    instance-of v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;

    if-eqz v0, :cond_4

    .line 143
    move-object v0, p0

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;->getItems()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 147
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;->getItems()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 168
    new-instance v2, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$apply$$inlined$sortedBy$1;

    invoke-direct {v2}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$apply$$inlined$sortedBy$1;-><init>()V

    check-cast v2, Ljava/util/Comparator;

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v1

    .line 149
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;->getProps()Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;->getIncludeConfirmDialog()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemDialog;

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemDialog;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    goto :goto_0

    .line 152
    :cond_1
    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    goto :goto_0

    .line 145
    :cond_2
    new-instance v1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingAddItemDialog;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;->getItems()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-direct {v1, v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingAddItemDialog;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    goto :goto_0

    .line 144
    :cond_3
    sget-object v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$NoItemsFoundErrorDialog;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$NoItemsFoundErrorDialog;

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;

    .line 143
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 158
    :cond_4
    instance-of v0, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ShowChooseItemScreen;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;

    move-object v1, p0

    check-cast v1, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ShowChooseItemScreen;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ShowChooseItemScreen;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState$ShowingChooseEligibleItemScreen;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 160
    :cond_5
    instance-of p1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$AddMatchingItem;

    if-eqz p1, :cond_6

    new-instance p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;

    move-object v0, p0

    check-cast v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$AddMatchingItem;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$AddMatchingItem;->getItem()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput$MatchingItemAdded;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    check-cast p1, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    return-object p1

    :cond_6
    :goto_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponState;",
            "-",
            "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
