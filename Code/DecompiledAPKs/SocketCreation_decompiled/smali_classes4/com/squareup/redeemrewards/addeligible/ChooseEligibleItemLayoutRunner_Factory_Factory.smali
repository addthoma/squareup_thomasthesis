.class public final Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;
.super Ljava/lang/Object;
.source "ChooseEligibleItemLayoutRunner_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p7, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;"
        }
    .end annotation

    .line 57
    new-instance v8, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;-><init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;
    .locals 8

    .line 49
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/recycler/RecyclerFactory;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/DurationFormatter;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v7}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->newInstance(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner_Factory_Factory;->get()Lcom/squareup/redeemrewards/addeligible/ChooseEligibleItemLayoutRunner$Factory;

    move-result-object v0

    return-object v0
.end method
