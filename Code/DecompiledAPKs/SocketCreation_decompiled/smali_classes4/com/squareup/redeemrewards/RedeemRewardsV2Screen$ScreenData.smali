.class public final Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;
.super Ljava/lang/Object;
.source "RedeemRewardsV2Screen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsV2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u001f\u0008\u0086\u0008\u0018\u00002\u00020\u0001BY\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c6\u0003J\t\u0010#\u001a\u00020\u0005H\u00c6\u0003J\t\u0010$\u001a\u00020\nH\u00c6\u0003J\t\u0010%\u001a\u00020\nH\u00c6\u0003J\t\u0010&\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0003J\u000f\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0003Jo\u0010)\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00052\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00052\u000e\u0008\u0002\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u000e\u0008\u0002\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u00c6\u0001J\u0013\u0010*\u001a\u00020\u00032\u0008\u0010+\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010,\u001a\u00020\nH\u00d6\u0001J\t\u0010-\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0015R\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0011\u0010\u0008\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001aR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001a\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;",
        "",
        "loading",
        "",
        "title",
        "",
        "actionBarAction",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "errorMessage",
        "cartPoints",
        "",
        "contactPoints",
        "pointsMessage",
        "coupons",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "rewardTiers",
        "(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)V",
        "getActionBarAction",
        "()Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
        "getCartPoints",
        "()I",
        "getContactPoints",
        "getCoupons",
        "()Ljava/util/List;",
        "getErrorMessage",
        "()Ljava/lang/String;",
        "getLoading",
        "()Z",
        "getPointsMessage",
        "getRewardTiers",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

.field private final cartPoints:I

.field private final contactPoints:I

.field private final coupons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final errorMessage:Ljava/lang/String;

.field private final loading:Z

.field private final pointsMessage:Ljava/lang/String;

.field private final rewardTiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;)V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarAction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsMessage"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardTiers"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    iput-object p4, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    iput p5, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    iput p6, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    iput-object p7, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    iput-object p9, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget v6, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget v7, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->copy(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    return v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    return-object v0
.end method

.method public final component9()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;)",
            "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;"
        }
    .end annotation

    const-string v0, "title"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarAction"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsMessage"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "coupons"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardTiers"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    move-object v1, v0

    move v2, p1

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;-><init>(ZLjava/lang/String;Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;Ljava/lang/String;IILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    iget-boolean v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    iget v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    iget v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarAction()Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    return-object v0
.end method

.method public final getCartPoints()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    return v0
.end method

.method public final getContactPoints()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    return v0
.end method

.method public final getCoupons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    return-object v0
.end method

.method public final getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getLoading()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    return v0
.end method

.method public final getPointsMessage()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getRewardTiers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ScreenData(loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->loading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", actionBarAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->actionBarAction:Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ActionBarAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cartPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->cartPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", contactPoints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->contactPoints:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pointsMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->pointsMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", coupons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->coupons:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rewardTiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$ScreenData;->rewardTiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
