.class public final Lcom/squareup/redeemrewards/ViewRewardsScreen;
.super Ljava/lang/Object;
.source "ViewRewardsScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;,
        Lcom/squareup/redeemrewards/ViewRewardsScreen$AbstractRewardRow;,
        Lcom/squareup/redeemrewards/ViewRewardsScreen$LoyaltyRewardSection;,
        Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 !2\u00020\u0001:\u0004 !\"#B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\nH\u00c6\u0003J7\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00082\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ViewRewardsScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "title",
        "Lcom/squareup/util/ViewString;",
        "onBack",
        "Lkotlin/Function0;",
        "",
        "backButtonIsX",
        "",
        "type",
        "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;",
        "(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)V",
        "getBackButtonIsX",
        "()Z",
        "getOnBack",
        "()Lkotlin/jvm/functions/Function0;",
        "getTitle",
        "()Lcom/squareup/util/ViewString;",
        "getType",
        "()Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "AbstractRewardRow",
        "Companion",
        "LoyaltyRewardSection",
        "Type",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final backButtonIsX:Z

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final title:Lcom/squareup/util/ViewString;

.field private final type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->Companion:Lcom/squareup/redeemrewards/ViewRewardsScreen$Companion;

    .line 67
    const-class v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;",
            ")V"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iput-boolean p3, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    iput-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/redeemrewards/ViewRewardsScreen;Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;ILjava/lang/Object;)Lcom/squareup/redeemrewards/ViewRewardsScreen;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsScreen;->copy(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)Lcom/squareup/redeemrewards/ViewRewardsScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    return v0
.end method

.method public final component4()Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    return-object v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)Lcom/squareup/redeemrewards/ViewRewardsScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ViewString;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;Z",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;",
            ")",
            "Lcom/squareup/redeemrewards/ViewRewardsScreen;"
        }
    .end annotation

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/redeemrewards/ViewRewardsScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;ZLcom/squareup/redeemrewards/ViewRewardsScreen$Type;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    iget-boolean v1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    iget-object p1, p1, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBackButtonIsX()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    return v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getTitle()Lcom/squareup/util/ViewString;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final getType()Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewRewardsScreen(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->title:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", backButtonIsX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->backButtonIsX:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ViewRewardsScreen;->type:Lcom/squareup/redeemrewards/ViewRewardsScreen$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
