.class public final Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;
.super Ljava/lang/Object;
.source "RedeemRewardsScope_Module_ProvideConfigureItemHostFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/ConfigureItemHost;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final diningOptionCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionCompsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->module:Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;

    .line 45
    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->deviceProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionCompsProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DiningOptionCache;",
            ">;)",
            "Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideConfigureItemHost(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)Lcom/squareup/configure/item/ConfigureItemHost;
    .locals 0

    .line 72
    invoke-virtual/range {p0 .. p6}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;->provideConfigureItemHost(Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)Lcom/squareup/configure/item/ConfigureItemHost;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/configure/item/ConfigureItemHost;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/ConfigureItemHost;
    .locals 7

    .line 55
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->module:Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionCompsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/TransactionComps;

    iget-object v4, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    iget-object v5, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v6, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->diningOptionCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/ui/main/DiningOptionCache;

    invoke-static/range {v0 .. v6}, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->provideConfigureItemHost(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;Lcom/squareup/util/Device;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionComps;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/ui/main/DiningOptionCache;)Lcom/squareup/configure/item/ConfigureItemHost;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/RedeemRewardsScope_Module_ProvideConfigureItemHostFactory;->get()Lcom/squareup/configure/item/ConfigureItemHost;

    move-result-object v0

    return-object v0
.end method
