.class public final Lcom/squareup/redeemrewards/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final choose_contact_for_reward_title:I = 0x7f12041f

.field public static final loyalty_points_available:I = 0x7f120f3e

.field public static final loyalty_rewards_available:I = 0x7f120f6c

.field public static final loyalty_rewards_no_available:I = 0x7f120f6d

.field public static final redeem_rewards_add_customer:I = 0x7f1215fc

.field public static final redeem_rewards_contacts_error_message:I = 0x7f1215ff

.field public static final redeem_rewards_coupons_title:I = 0x7f121600

.field public static final redeem_rewards_failure_redeeming:I = 0x7f121602

.field public static final redeem_rewards_failure_returning:I = 0x7f121603

.field public static final redeem_rewards_no_available:I = 0x7f12160d

.field public static final redeem_rewards_redeem:I = 0x7f121610

.field public static final redeem_rewards_remove_customer:I = 0x7f121611

.field public static final redeem_rewards_undo:I = 0x7f121614

.field public static final redeem_rewards_use_code:I = 0x7f121615


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
