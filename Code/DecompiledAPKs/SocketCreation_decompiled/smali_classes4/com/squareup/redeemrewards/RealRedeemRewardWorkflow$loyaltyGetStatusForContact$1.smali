.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;
.super Ljava/lang/Object;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->loyaltyGetStatusForContact(Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "response",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contact:Lcom/squareup/protos/client/rolodex/Contact;

.field final synthetic $this_loyaltyGetStatusForContact:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$this_loyaltyGetStatusForContact:Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    instance-of v0, p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    new-instance v1, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1$1;-><init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;Lcom/squareup/loyalty/LoyaltyStatusResponse;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1, p1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 250
    :cond_0
    sget-object v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;->INSTANCE:Lcom/squareup/loyalty/LoyaltyStatusResponse$ErrorRetrievingLoyalty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    .line 251
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->$contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    const-string v1, "contact.contact_token"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getRes$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_contacts_error_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-static {p1, v0, v1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$lookupContactFailedAction(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$loyaltyGetStatusForContact$1;->apply(Lcom/squareup/loyalty/LoyaltyStatusResponse;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
