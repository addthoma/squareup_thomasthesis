.class public final Lcom/squareup/queue/sqlite/LocalPaymentConverter;
.super Ljava/lang/Object;
.source "LocalPaymentConverter.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
        "Lcom/squareup/queue/sqlite/LocalPaymentEntry;",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/tape/FileObjectQueue$Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tape/FileObjectQueue$Converter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    return-void
.end method

.method private taskMessage(Lcom/squareup/queue/retrofit/RetrofitTask;[BLjava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v0, "null"

    const-string v1, "non-null"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_1

    .line 89
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array p3, v2, [Ljava/lang/Object;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    aput-object v0, p3, v3

    const-string p2, "Unable to create entry from null task: bytes = %s"

    invoke-static {p1, p2, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 92
    :cond_1
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p3, v5, v3

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->idOf(Lcom/squareup/queue/retrofit/RetrofitTask;)Ljava/lang/String;

    move-result-object p3

    aput-object p3, v5, v2

    const/4 p3, 0x2

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->timeOf(Lcom/squareup/queue/retrofit/RetrofitTask;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v5, p3

    const/4 p1, 0x3

    if-nez p2, :cond_2

    goto :goto_1

    :cond_2
    move-object v0, v1

    :goto_1
    aput-object v0, v5, p1

    const-string p1, "%s: task id %s, task timestamp %d, bytes = %s"

    .line 92
    invoke-static {v4, p1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method idOf(Lcom/squareup/queue/retrofit/RetrofitTask;)Ljava/lang/String;
    .locals 2

    .line 73
    instance-of v0, p1, Lcom/squareup/queue/LocalPaymentRetrofitTask;

    const-string v1, "none"

    if-eqz v0, :cond_1

    .line 74
    check-cast p1, Lcom/squareup/queue/LocalPaymentRetrofitTask;

    invoke-interface {p1}, Lcom/squareup/queue/LocalPaymentRetrofitTask;->getUuid()Ljava/lang/String;

    move-result-object p1

    .line 75
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object p1, v1

    :cond_0
    return-object p1

    :cond_1
    return-object v1
.end method

.method timeOf(Lcom/squareup/queue/retrofit/RetrofitTask;)J
    .locals 2

    .line 81
    instance-of v0, p1, Lcom/squareup/queue/LocalPaymentRetrofitTask;

    if-eqz v0, :cond_0

    .line 82
    check-cast p1, Lcom/squareup/queue/LocalPaymentRetrofitTask;

    invoke-interface {p1}, Lcom/squareup/queue/LocalPaymentRetrofitTask;->getTime()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public toQueueEntries(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/queue/sqlite/LocalPaymentEntry;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/sqlite/LocalPaymentEntry;

    .line 61
    :try_start_0
    iget-object v2, p0, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/LocalPaymentEntry;->data()[B

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/tape/FileObjectQueue$Converter;->from([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/retrofit/RetrofitTask;

    if-eqz v2, :cond_0

    .line 63
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 66
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert entry to task"

    invoke-virtual {v1, v2}, Lcom/squareup/queue/sqlite/LocalPaymentEntry;->logAs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public toQueueEntry(Lcom/squareup/queue/sqlite/LocalPaymentEntry;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 3

    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/LocalPaymentEntry;->data()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/tape/FileObjectQueue$Converter;->from([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitTask;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert entry to task"

    invoke-virtual {p1, v2}, Lcom/squareup/queue/sqlite/LocalPaymentEntry;->logAs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic toQueueEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/queue/sqlite/LocalPaymentEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->toQueueEntry(Lcom/squareup/queue/sqlite/LocalPaymentEntry;)Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object p1

    return-object p1
.end method

.method public toStoreEntry(Lcom/squareup/queue/retrofit/RetrofitTask;)Lcom/squareup/queue/sqlite/LocalPaymentEntry;
    .locals 6

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->idOf(Lcom/squareup/queue/retrofit/RetrofitTask;)Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->timeOf(Lcom/squareup/queue/retrofit/RetrofitTask;)J

    move-result-wide v1

    const/4 v3, 0x0

    .line 38
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 39
    iget-object v5, p0, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-interface {v5, p1, v4}, Lcom/squareup/tape/FileObjectQueue$Converter;->toStream(Ljava/lang/Object;Ljava/io/OutputStream;)V

    .line 40
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 41
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/queue/sqlite/LocalPaymentEntry;->newLocalPaymentEntry(Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/LocalPaymentEntry;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v0

    .line 45
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert task to entry"

    invoke-direct {p0, p1, v3, v2}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->taskMessage(Lcom/squareup/queue/retrofit/RetrofitTask;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    .line 43
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert task to stream"

    invoke-direct {p0, p1, v3, v2}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->taskMessage(Lcom/squareup/queue/retrofit/RetrofitTask;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic toStoreEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/LocalPaymentConverter;->toStoreEntry(Lcom/squareup/queue/retrofit/RetrofitTask;)Lcom/squareup/queue/sqlite/LocalPaymentEntry;

    move-result-object p1

    return-object p1
.end method
