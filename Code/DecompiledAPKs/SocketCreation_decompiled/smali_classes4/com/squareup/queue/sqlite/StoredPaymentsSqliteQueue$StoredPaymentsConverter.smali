.class public final Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;
.super Ljava/lang/Object;
.source "StoredPaymentsSqliteQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoredPaymentsConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter<",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    return-void
.end method

.method private storedPaymentMessage(Lcom/squareup/payment/offline/StoredPayment;[BLjava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v0, "null"

    const-string v1, "non-null"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_1

    .line 150
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array p3, v2, [Ljava/lang/Object;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    aput-object v0, p3, v3

    const-string p2, "Unable to create entry from null stored payment: bytes = %s"

    invoke-static {p1, p2, p3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 154
    :cond_1
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p3, v5, v3

    .line 156
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object p3

    aput-object p3, v5, v2

    const/4 p3, 0x2

    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    aput-object p1, v5, p3

    const/4 p1, 0x3

    if-nez p2, :cond_2

    goto :goto_1

    :cond_2
    move-object v0, v1

    :goto_1
    aput-object v0, v5, p1

    const-string p1, "%s: stored payment unique key %s, stored payment timestamp %d, bytes = %s"

    .line 154
    invoke-static {v4, p1, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public toQueueEntries(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    .line 136
    :try_start_0
    iget-object v2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->data()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->from([B)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 138
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 141
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert entry to stored payment"

    invoke-virtual {v1, v2}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->logAs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    return-object v0
.end method

.method public toQueueEntry(Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)Lcom/squareup/payment/offline/StoredPayment;
    .locals 3

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->data()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->from([B)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v0

    .line 127
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert entry to stored payment"

    invoke-virtual {p1, v2}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->logAs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic toQueueEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 98
    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->toQueueEntry(Lcom/squareup/queue/sqlite/StoredPaymentsEntry;)Lcom/squareup/payment/offline/StoredPayment;

    move-result-object p1

    return-object p1
.end method

.method public toStoreEntry(Lcom/squareup/payment/offline/StoredPayment;)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
    .locals 4

    const/4 v0, 0x0

    .line 110
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 111
    iget-object v2, p0, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-virtual {v2, p1, v1}, Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;->toStream(Lcom/squareup/payment/offline/StoredPayment;Ljava/io/OutputStream;)V

    .line 112
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 113
    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/payment/offline/StoredPayment;->getTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3, v0}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;->newStoredPaymentsEntry(Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v1

    .line 118
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unable to convert stored payment to entry"

    invoke-direct {p0, p1, v0, v3}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->storedPaymentMessage(Lcom/squareup/payment/offline/StoredPayment;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v1

    .line 115
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unable to convert stored payment to stream"

    invoke-direct {p0, p1, v0, v3}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->storedPaymentMessage(Lcom/squareup/payment/offline/StoredPayment;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, p1, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic toStoreEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 98
    check-cast p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;->toStoreEntry(Lcom/squareup/payment/offline/StoredPayment;)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    move-result-object p1

    return-object p1
.end method
