.class public interface abstract Lcom/squareup/queue/sqlite/LocalPaymentModel;
.super Ljava/lang/Object;
.source "LocalPaymentModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/LocalPaymentModel$DeleteAllEntries;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$DeleteFirstEntry;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$InsertEntry;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$Factory;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$Mapper;,
        Lcom/squareup/queue/sqlite/LocalPaymentModel$Creator;
    }
.end annotation


# static fields
.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE local_payments_tasks (\n  -- Alias for ROWID.\n  _id INTEGER PRIMARY KEY,\n\n  /*\n   * Identifier associated with the local_payments_tasks represented by this entry (currently for logging purposes\n   * only).\n   */\n  entry_id TEXT NOT NULL,\n\n  -- Timestamp of the local_payments_tasks, in milliseconds since epoch.\n  timestamp_ms INTEGER NOT NULL CHECK (timestamp_ms >= 0),\n\n  -- Binary representation of the local_payments_tasks.\n  data BLOB NOT NULL\n)"

.field public static final DATA:Ljava/lang/String; = "data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ENTRY_ID:Ljava/lang/String; = "entry_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_NAME:Ljava/lang/String; = "local_payments_tasks"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIMESTAMP_MS:Ljava/lang/String; = "timestamp_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final _ID:Ljava/lang/String; = "_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract _id()Ljava/lang/Long;
.end method

.method public abstract data()[B
.end method

.method public abstract entry_id()Ljava/lang/String;
.end method

.method public abstract timestamp_ms()J
.end method
