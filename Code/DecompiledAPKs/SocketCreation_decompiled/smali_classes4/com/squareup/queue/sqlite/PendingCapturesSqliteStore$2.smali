.class final Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;
.super Lkotlin/jvm/internal/Lambda;
.source "PendingCapturesSqliteStore.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;-><init>(Landroid/app/Application;Lcom/squareup/util/Clock;Ljava/io/File;Lcom/squareup/sqlbrite3/SqlBrite;Lio/reactivex/Scheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lio/reactivex/Observable<",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00020\u0002H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "invoke",
        "(Ljava/lang/Integer;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $clock:Lcom/squareup/util/Clock;

.field final synthetic this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;


# direct methods
.method constructor <init>(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;Lcom/squareup/util/Clock;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    iput-object p2, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;->$clock:Lcom/squareup/util/Clock;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Integer;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 94
    sget-object p1, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->Companion:Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;

    invoke-virtual {p1}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;->getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;->$clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    const/16 v2, 0x7d0

    int-to-long v2, v2

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;->ripenedCount(J)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    move-result-object p1

    const-string v0, "FACTORY.ripenedCount(clo\u2026PENED_PAYMENT_MINIMUM_MS)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;->this$0:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;

    invoke-static {v0}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;->access$getDb$p(Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore;)Lcom/squareup/sqlbrite3/BriteDatabase;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;->getTables()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    check-cast p1, Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    .line 96
    sget-object v0, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2$1;->INSTANCE:Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lcom/squareup/sqlbrite3/QueryObservable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "db.createQuery(statement\u2026T_MAPPER)\n              }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 43
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/PendingCapturesSqliteStore$2;->invoke(Ljava/lang/Integer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
