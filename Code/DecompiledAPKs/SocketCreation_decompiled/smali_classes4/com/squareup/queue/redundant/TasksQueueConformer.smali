.class public Lcom/squareup/queue/redundant/TasksQueueConformer;
.super Lcom/squareup/queue/redundant/QueueConformer;
.source "TasksQueueConformer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/redundant/QueueConformer<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final tapeQueue:Lcom/squareup/tape/ObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/ObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/queue/redundant/RedundantRetrofitQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-virtual {p2}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getTapeQueue()Lcom/squareup/tape/FileObjectQueue;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getSqliteQueue()Lcom/squareup/queue/sqlite/shared/SqliteQueue;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/redundant/QueueConformer;-><init>(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V

    .line 31
    iput-object p1, p0, Lcom/squareup/queue/redundant/TasksQueueConformer;->features:Lcom/squareup/settings/server/Features;

    .line 32
    invoke-virtual {p2}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;->getTapeQueue()Lcom/squareup/tape/FileObjectQueue;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/redundant/TasksQueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/redundant/TasksQueueConformer;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PREFER_SQLITE_TASKS_QUEUE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/queue/redundant/TasksQueueConformer;->tapeQueue:Lcom/squareup/tape/ObjectQueue;

    invoke-interface {v0}, Lcom/squareup/tape/ObjectQueue;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/TasksQueueConformer;->finish()V

    goto :goto_0

    .line 39
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/queue/redundant/QueueConformer;->onEnterScope(Lmortar/MortarScope;)V

    :goto_0
    return-void
.end method
