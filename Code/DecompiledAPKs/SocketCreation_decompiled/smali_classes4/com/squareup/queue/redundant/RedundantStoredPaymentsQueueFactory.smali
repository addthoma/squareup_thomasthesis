.class public Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;
.super Lcom/squareup/queue/StoreAndForwardQueueFactory;
.source "RedundantStoredPaymentsQueueFactory.java"


# instance fields
.field private final context:Landroid/app/Application;

.field private final corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

.field private final fileScheduler:Lio/reactivex/Scheduler;

.field private final fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;Lcom/squareup/queue/CorruptQueueHelper;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0, p2, p4, p5}, Lcom/squareup/queue/StoreAndForwardQueueFactory;-><init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V

    .line 42
    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->context:Landroid/app/Application;

    .line 43
    iput-object p3, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->fileScheduler:Lio/reactivex/Scheduler;

    .line 44
    iput-object p4, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 45
    iput-object p6, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    return-void
.end method


# virtual methods
.method protected openQueueThrowing(Ljava/io/File;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/queue/ReadableFileObjectQueue;

    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/ReadableFileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 52
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object p1

    const-string v1, "Parent file"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/File;

    .line 54
    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->context:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->fileScheduler:Lio/reactivex/Scheduler;

    .line 55
    invoke-static {v1, p1, v2}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;->create(Landroid/app/Application;Ljava/io/File;Lio/reactivex/Scheduler;)Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;

    move-result-object p1

    .line 57
    new-instance v1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;

    iget-object v2, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-direct {v1, v2}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue$StoredPaymentsConverter;-><init>(Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V

    .line 58
    new-instance v2, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;

    invoke-direct {v2, p1, v1}, Lcom/squareup/queue/sqlite/shared/RealSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/SqliteQueueStore;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    .line 59
    new-instance v1, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    iget-object v3, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->converter:Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;

    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/StoredPaymentsSqliteStore;Lcom/squareup/queue/StoreAndForwardPaymentTaskConverter;)V

    .line 62
    new-instance p1, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;

    iget-object v2, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v3, p0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    invoke-direct {p1, v0, v1, v2, v3}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;-><init>(Lcom/squareup/queue/ReadableFileObjectQueue;Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/queue/CorruptQueueHelper;)V

    return-object p1
.end method
