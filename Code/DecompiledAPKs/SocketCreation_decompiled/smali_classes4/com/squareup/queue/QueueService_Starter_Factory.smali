.class public final Lcom/squareup/queue/QueueService_Starter_Factory;
.super Ljava/lang/Object;
.source "QueueService_Starter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/QueueService$Starter;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final foregroundServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/queue/QueueService_Starter_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/queue/QueueService_Starter_Factory;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueService_Starter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;)",
            "Lcom/squareup/queue/QueueService_Starter_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/queue/QueueService_Starter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueService_Starter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)Lcom/squareup/queue/QueueService$Starter;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/queue/QueueService$Starter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueService$Starter;-><init>(Landroid/app/Application;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/QueueService$Starter;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/queue/QueueService_Starter_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/queue/QueueService_Starter_Factory;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueService_Starter_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)Lcom/squareup/queue/QueueService$Starter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/queue/QueueService_Starter_Factory;->get()Lcom/squareup/queue/QueueService$Starter;

    move-result-object v0

    return-object v0
.end method
