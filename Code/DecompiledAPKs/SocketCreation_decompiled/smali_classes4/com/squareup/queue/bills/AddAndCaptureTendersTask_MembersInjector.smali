.class public final Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;
.super Ljava/lang/Object;
.source "AddAndCaptureTendersTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/bills/AddAndCaptureTendersTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final addTendersRequestServerIdsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;"
        }
    .end annotation
.end field

.field private final billCreationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field

.field private final captureTenderServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTenderCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->captureTenderServiceProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/LocalTenderCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/tenders/CaptureTenderService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/bills/AddAndCaptureTendersTask;",
            ">;"
        }
    .end annotation

    .line 78
    new-instance v11, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/AddAndCaptureTendersTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/settings/AddTendersRequestServerIds;",
            ">;)V"
        }
    .end annotation

    .line 104
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->addTendersRequestServerIds:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectBillCreationService(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/server/bills/BillCreationService;)V
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->billCreationService:Lcom/squareup/server/bills/BillCreationService;

    return-void
.end method

.method public static injectCaptureTenderService(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/server/tenders/CaptureTenderService;)V
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->captureTenderService:Lcom/squareup/server/tenders/CaptureTenderService;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/bills/AddAndCaptureTendersTask;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 98
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectLocalTenderCache(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/print/LocalTenderCache;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    return-void
.end method

.method public static injectTickets(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/tickets/Tickets;)V
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->tickets:Lcom/squareup/tickets/Tickets;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->lastLocalPaymentServerIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectLastLocalPaymentServerId(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/LocalSetting;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->addTendersRequestServerIdsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectAddTendersRequestServerIds(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/LocalSetting;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->localTenderCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/LocalTenderCache;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectLocalTenderCache(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/print/LocalTenderCache;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectTickets(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/tickets/Tickets;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectFeatures(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/settings/server/Features;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->captureTenderServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/tenders/CaptureTenderService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectCaptureTenderService(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/server/tenders/CaptureTenderService;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->billCreationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    invoke-static {p1, v0}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectBillCreationService(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;Lcom/squareup/server/bills/BillCreationService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask_MembersInjector;->injectMembers(Lcom/squareup/queue/bills/AddAndCaptureTendersTask;)V

    return-void
.end method
