.class final Lcom/squareup/queue/QueueRootModule$1;
.super Ljava/lang/Object;
.source "QueueRootModule.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/QueueRootModule;->provideCrossSessionStoreAndForwardTasksQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/RetrofitQueueFactory;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/retrofit/RetrofitQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue$Listener<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$ignoreEvents:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;


# direct methods
.method constructor <init>(Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule$1;->val$ignoreEvents:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p2, p0, Lcom/squareup/queue/QueueRootModule$1;->val$queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdd(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ")V"
        }
    .end annotation

    .line 114
    iget-object p1, p0, Lcom/squareup/queue/QueueRootModule$1;->val$ignoreEvents:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_0

    .line 115
    iget-object p1, p0, Lcom/squareup/queue/QueueRootModule$1;->val$queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string p2, "Task added to logged out queue"

    invoke-interface {p1, p2}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .locals 0

    .line 112
    check-cast p2, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/QueueRootModule$1;->onAdd(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
