.class public final Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvidePendingCapturesMonitorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/sqlite/PendingCapturesMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final redundantPendingCapturesQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;->redundantPendingCapturesQueueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePendingCapturesMonitor(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;)Lcom/squareup/queue/sqlite/PendingCapturesMonitor;
    .locals 1

    .line 38
    invoke-static {p0}, Lcom/squareup/queue/QueueModule;->providePendingCapturesMonitor(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;)Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/sqlite/PendingCapturesMonitor;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;->redundantPendingCapturesQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;

    invoke-static {v0}, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;->providePendingCapturesMonitor(Lcom/squareup/queue/redundant/RedundantRetrofitQueue;)Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvidePendingCapturesMonitorFactory;->get()Lcom/squareup/queue/sqlite/PendingCapturesMonitor;

    move-result-object v0

    return-object v0
.end method
