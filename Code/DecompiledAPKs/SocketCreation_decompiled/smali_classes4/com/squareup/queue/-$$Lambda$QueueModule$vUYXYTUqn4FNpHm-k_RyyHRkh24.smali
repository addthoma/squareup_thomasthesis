.class public final synthetic Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# instance fields
.field private final synthetic f$0:Landroid/app/Application;

.field private final synthetic f$1:Lcom/squareup/util/Clock;

.field private final synthetic f$2:Lio/reactivex/Scheduler;

.field private final synthetic f$3:Lcom/squareup/tape/FileObjectQueue$Converter;

.field private final synthetic f$4:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public synthetic constructor <init>(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$0:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$1:Lcom/squareup/util/Clock;

    iput-object p3, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$2:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$3:Lcom/squareup/tape/FileObjectQueue$Converter;

    iput-object p5, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$4:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$0:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$1:Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$2:Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$3:Lcom/squareup/tape/FileObjectQueue$Converter;

    iget-object v4, p0, Lcom/squareup/queue/-$$Lambda$QueueModule$vUYXYTUqn4FNpHm-k_RyyHRkh24;->f$4:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-object v5, p1

    check-cast v5, Ljava/io/File;

    invoke-static/range {v0 .. v5}, Lcom/squareup/queue/QueueModule;->lambda$localPaymentsSqliteQueueFactory$2(Landroid/app/Application;Lcom/squareup/util/Clock;Lio/reactivex/Scheduler;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/io/File;)Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    move-result-object p1

    return-object p1
.end method
