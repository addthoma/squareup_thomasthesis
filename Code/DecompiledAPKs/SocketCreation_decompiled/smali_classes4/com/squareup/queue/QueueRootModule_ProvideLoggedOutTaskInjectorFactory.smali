.class public final Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideLoggedOutTaskInjectorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tape/TaskInjector<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory$InstanceHolder;->access$000()Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideLoggedOutTaskInjector()Lcom/squareup/tape/TaskInjector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-static {}, Lcom/squareup/queue/QueueRootModule;->provideLoggedOutTaskInjector()Lcom/squareup/tape/TaskInjector;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tape/TaskInjector;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tape/TaskInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 20
    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;->provideLoggedOutTaskInjector()Lcom/squareup/tape/TaskInjector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutTaskInjectorFactory;->get()Lcom/squareup/tape/TaskInjector;

    move-result-object v0

    return-object v0
.end method
