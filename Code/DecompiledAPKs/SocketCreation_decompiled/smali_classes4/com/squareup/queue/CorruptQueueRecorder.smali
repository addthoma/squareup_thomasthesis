.class public Lcom/squareup/queue/CorruptQueueRecorder;
.super Ljava/lang/Object;
.source "CorruptQueueRecorder.java"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field protected final memCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected userId:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final usersWithCorruptQueues:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->usersWithCorruptQueues:Lcom/f2prateek/rx/preferences2/Preference;

    .line 66
    iput-object p2, p0, Lcom/squareup/queue/CorruptQueueRecorder;->analytics:Lcom/squareup/analytics/Analytics;

    .line 67
    new-instance p2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    invoke-direct {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(Ljava/util/Map;)V

    iput-object p2, p0, Lcom/squareup/queue/CorruptQueueRecorder;->memCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 68
    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->userId:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public hasCorruptQueue()Z
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/queue/CorruptQueueRecorder;->userId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 89
    :cond_0
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/queue/CorruptQueueRecorder;->memCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public recordCorruptQueue()V
    .locals 1

    const/4 v0, 0x0

    .line 97
    invoke-virtual {p0, v0}, Lcom/squareup/queue/CorruptQueueRecorder;->recordCorruptQueue(Ljava/lang/Throwable;)V

    return-void
.end method

.method public recordCorruptQueue(Ljava/lang/Throwable;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 106
    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->CORRUPT_QUEUE:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 109
    :cond_0
    iget-object p1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->userId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 110
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->memCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/squareup/queue/CorruptQueueRecorder;->memCache:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object p1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->usersWithCorruptQueues:Lcom/f2prateek/rx/preferences2/Preference;

    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p0, Lcom/squareup/queue/CorruptQueueRecorder;->memCache:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public setLoggedInUser(Ljava/lang/String;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/queue/CorruptQueueRecorder;->userId:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method
