.class Lcom/squareup/queue/OtherTenderTask$1;
.super Lcom/squareup/server/SquareCallback;
.source "OtherTenderTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/queue/OtherTenderTask;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/SquareCallback<",
        "Lcom/squareup/server/payment/CreatePaymentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/OtherTenderTask;

.field final synthetic val$callback:Lcom/squareup/server/SquareCallback;


# direct methods
.method constructor <init>(Lcom/squareup/queue/OtherTenderTask;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iput-object p2, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-direct {p0}, Lcom/squareup/server/SquareCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/server/payment/CreatePaymentResponse;)V
    .locals 6

    .line 158
    :try_start_0
    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v1, v1, Lcom/squareup/queue/OtherTenderTask;->localTenderCache:Lcom/squareup/print/LocalTenderCache;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v2, v2, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/payment/CreatePaymentResponse;->receipt_number:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/squareup/print/LocalTenderCache;->setLast(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v1, v1, Lcom/squareup/queue/OtherTenderTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 167
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v0, v0, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v0, v0, Lcom/squareup/queue/OtherTenderTask;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v1, v1, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    new-instance v2, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v4, v4, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 167
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v1, v1, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v1, v1, Lcom/squareup/queue/OtherTenderTask;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v2, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v2, v2, Lcom/squareup/queue/OtherTenderTask;->ticketId:Ljava/lang/String;

    new-instance v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {p1}, Lcom/squareup/server/payment/CreatePaymentResponse;->getPaymentId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/queue/OtherTenderTask$1;->this$0:Lcom/squareup/queue/OtherTenderTask;

    iget-object v5, v5, Lcom/squareup/queue/OtherTenderTask;->uuid:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2, v3}, Lcom/squareup/tickets/Tickets;->updateTerminalId(Ljava/lang/String;Lcom/squareup/protos/client/IdPair;)V

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v1, p1}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    .line 173
    throw v0
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/server/payment/CreatePaymentResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/OtherTenderTask$1;->call(Lcom/squareup/server/payment/CreatePaymentResponse;)V

    return-void
.end method

.method public clientError(Lcom/squareup/server/payment/CreatePaymentResponse;I)V
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    return-void
.end method

.method public bridge synthetic clientError(Ljava/lang/Object;I)V
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/server/payment/CreatePaymentResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/queue/OtherTenderTask$1;->clientError(Lcom/squareup/server/payment/CreatePaymentResponse;I)V

    return-void
.end method

.method public networkError()V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->networkError()V

    return-void
.end method

.method public serverError(I)V
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    return-void
.end method

.method public sessionExpired()V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    return-void
.end method

.method public unexpectedError(Ljava/lang/Throwable;)V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/queue/OtherTenderTask$1;->val$callback:Lcom/squareup/server/SquareCallback;

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->unexpectedError(Ljava/lang/Throwable;)V

    return-void
.end method
