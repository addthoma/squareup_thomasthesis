.class public Lcom/squareup/queue/DeclineReceipt;
.super Ljava/lang/Object;
.source "DeclineReceipt.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/DeclineReceipt$Builder;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final billId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final paymentId:Ljava/lang/String;

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final uniqueKey:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/DeclineReceipt$Builder;)V
    .locals 1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p1}, Lcom/squareup/queue/DeclineReceipt$Builder;->access$100(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->paymentId:Ljava/lang/String;

    .line 83
    invoke-static {p1}, Lcom/squareup/queue/DeclineReceipt$Builder;->access$200(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->billId:Ljava/lang/String;

    .line 84
    invoke-static {p1}, Lcom/squareup/queue/DeclineReceipt$Builder;->access$300(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/queue/DeclineReceipt$Builder;->access$300(Lcom/squareup/queue/DeclineReceipt$Builder;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/queue/DeclineReceipt;->uniqueKey:Ljava/lang/String;

    .line 86
    iget-object p1, p0, Lcom/squareup/queue/DeclineReceipt;->paymentId:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    iget-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->billId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr p1, v0

    const-string v0, "Either paymentId or billId must be set."

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/DeclineReceipt$Builder;Lcom/squareup/queue/DeclineReceipt$1;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/queue/DeclineReceipt;-><init>(Lcom/squareup/queue/DeclineReceipt$Builder;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->paymentService:Lcom/squareup/server/payment/PaymentService;

    iget-object v1, p0, Lcom/squareup/queue/DeclineReceipt;->paymentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/queue/DeclineReceipt;->billId:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/queue/DeclineReceipt;->uniqueKey:Ljava/lang/String;

    const/4 v3, 0x1

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lcom/squareup/server/payment/PaymentService;->declineReceipt(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/DeclineReceipt;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public getPaymentId()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->paymentId:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/DeclineReceipt;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 47
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/DeclineReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/DeclineReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeclineReceipt{paymentId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/DeclineReceipt;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", billId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/DeclineReceipt;->billId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", uniqueKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/DeclineReceipt;->uniqueKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
