.class Lcom/squareup/queue/TaskWatcher;
.super Ljava/lang/Object;
.source "TaskWatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/TaskWatcher$RepeatedStartException;
    }
.end annotation


# static fields
.field private static final REPEATED_START_EXCEPTION:Ljava/lang/Exception;

.field private static final REPEAT_THRESHOLD:I = 0xa


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final fileThreadScheduler:Lio/reactivex/Scheduler;

.field private final gson:Lcom/google/gson/Gson;

.field private final lastTaskCountSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final lastTaskRequiresRetry:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final lastTaskSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/queue/TaskWatcher$RepeatedStartException;

    invoke-direct {v0}, Lcom/squareup/queue/TaskWatcher$RepeatedStartException;-><init>()V

    sput-object v0, Lcom/squareup/queue/TaskWatcher;->REPEATED_START_EXCEPTION:Ljava/lang/Exception;

    return-void
.end method

.method constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/f2prateek/rx/preferences2/Preference;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;)V
    .locals 0
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/queue/TaskWatcher;->mainScheduler:Lio/reactivex/Scheduler;

    .line 44
    iput-object p2, p0, Lcom/squareup/queue/TaskWatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 45
    iput-object p3, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskSetting:Lcom/squareup/settings/LocalSetting;

    .line 46
    iput-object p4, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskCountSetting:Lcom/squareup/settings/LocalSetting;

    .line 47
    iput-object p5, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskRequiresRetry:Lcom/f2prateek/rx/preferences2/Preference;

    .line 48
    iput-object p6, p0, Lcom/squareup/queue/TaskWatcher;->gson:Lcom/google/gson/Gson;

    .line 49
    iput-object p7, p0, Lcom/squareup/queue/TaskWatcher;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 50
    iput-object p8, p0, Lcom/squareup/queue/TaskWatcher;->fileThreadScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private varargs logError(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .line 98
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 99
    iget-object p3, p0, Lcom/squareup/queue/TaskWatcher;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p3, v0, p2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 100
    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$startTask$0$TaskWatcher(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->gson:Lcom/google/gson/Gson;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/io/Serializable;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object p1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v2, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v2}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 64
    iget-object v6, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v6, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 65
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskCountSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, v5}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v3

    .line 69
    rem-int/lit8 v2, v0, 0xa

    if-nez v2, :cond_0

    .line 70
    sget-object v2, Lcom/squareup/queue/TaskWatcher;->REPEATED_START_EXCEPTION:Ljava/lang/Exception;

    new-array v1, v1, [Ljava/lang/Object;

    .line 71
    invoke-interface {p1}, Lcom/squareup/queue/retrofit/RetrofitTask;->secureCopyWithoutPIIForLogs()Ljava/lang/Object;

    move-result-object p1

    aput-object p1, v1, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v3

    const-string p1, "Task %s was executed %d times"

    .line 70
    invoke-direct {p0, v2, p1, v1}, Lcom/squareup/queue/TaskWatcher;->logError(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_0
    iget-object p1, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskCountSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :cond_1
    iget-object p1, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskCountSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v5}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskRequiresRetry:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void
.end method

.method startTask(Lcom/squareup/queue/retrofit/RetrofitTask;)Lio/reactivex/Completable;
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/queue/-$$Lambda$TaskWatcher$wqvTqoXmCyztRgfId7Qc2Dc0Ax0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/-$$Lambda$TaskWatcher$wqvTqoXmCyztRgfId7Qc2Dc0Ax0;-><init>(Lcom/squareup/queue/TaskWatcher;Lcom/squareup/queue/retrofit/RetrofitTask;)V

    .line 61
    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->fileThreadScheduler:Lio/reactivex/Scheduler;

    .line 80
    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->mainScheduler:Lio/reactivex/Scheduler;

    .line 85
    invoke-virtual {p1, v0}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method taskCompleted()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskSetting:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskRequiresRetry:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method taskRequiresRetry()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/queue/TaskWatcher;->lastTaskRequiresRetry:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method
