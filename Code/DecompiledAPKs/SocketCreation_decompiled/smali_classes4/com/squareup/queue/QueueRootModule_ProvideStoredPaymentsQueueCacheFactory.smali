.class public final Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideStoredPaymentsQueueCacheFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/QueueCache<",
        "Lcom/squareup/queue/StoredPaymentsQueue;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final queueFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;->queueFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ">;)",
            "Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;

    invoke-direct {v0, p0}, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStoredPaymentsQueueCache(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;",
            ")",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/queue/QueueRootModule;->provideStoredPaymentsQueueCache(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/QueueCache;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/QueueCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;->queueFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;

    invoke-static {v0}, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;->provideStoredPaymentsQueueCache(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueueFactory;)Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideStoredPaymentsQueueCacheFactory;->get()Lcom/squareup/queue/retrofit/QueueCache;

    move-result-object v0

    return-object v0
.end method
