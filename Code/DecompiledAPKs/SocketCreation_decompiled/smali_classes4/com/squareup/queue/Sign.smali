.class public Lcom/squareup/queue/Sign;
.super Ljava/lang/Object;
.source "Sign.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final billId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final paymentId:Ljava/lang/String;

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final signatureData:Ljava/lang/String;

.field final tenderId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/Sign;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iget-object v0, p1, Lcom/squareup/queue/Sign;->paymentId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/Sign;->paymentId:Ljava/lang/String;

    .line 40
    iget-object v0, p1, Lcom/squareup/queue/Sign;->billId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/Sign;->billId:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lcom/squareup/queue/Sign;->tenderId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/Sign;->tenderId:Ljava/lang/String;

    .line 42
    iget-object p1, p1, Lcom/squareup/queue/Sign;->signatureData:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p1, "REDACTED_signatureData"

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/queue/Sign;->signatureData:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/Sign;->paymentId:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/Sign;->billId:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/Sign;->tenderId:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lcom/squareup/queue/Sign;->signatureData:Ljava/lang/String;

    return-void

    .line 27
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Must set one and only one of paymentId and billId"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static payment(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/queue/Sign;
    .locals 2

    .line 22
    new-instance v0, Lcom/squareup/queue/Sign;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v1, p1}, Lcom/squareup/queue/Sign;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/Sign;->paymentService:Lcom/squareup/server/payment/PaymentService;

    iget-object v1, p0, Lcom/squareup/queue/Sign;->paymentId:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/queue/Sign;->billId:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/queue/Sign;->tenderId:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/queue/Sign;->signatureData:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/squareup/server/payment/PaymentService;->sign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lretrofit/mime/TypedOutput;Ljava/lang/String;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Sign;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 62
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/Sign;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Sign;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/queue/Sign;

    invoke-direct {v0, p0}, Lcom/squareup/queue/Sign;-><init>(Lcom/squareup/queue/Sign;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sign{paymentId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Sign;->paymentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", signatureData len "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Sign;->signatureData:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, "[null]"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
