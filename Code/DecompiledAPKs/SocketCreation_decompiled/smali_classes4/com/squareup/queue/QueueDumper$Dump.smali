.class public Lcom/squareup/queue/QueueDumper$Dump;
.super Ljava/lang/Object;
.source "QueueDumper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueDumper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Dump"
.end annotation


# instance fields
.field public final dumpId:Ljava/lang/String;

.field public final lastCaptureId:Ljava/lang/String;

.field public final lastLocalPaymentServerId:Ljava/lang/String;

.field public final merchantId:Ljava/lang/String;

.field public final qCaptures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/queue/QueueDumper$TaskEnvelope;",
            ">;"
        }
    .end annotation
.end field

.field public final qTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/queue/QueueDumper$TaskEnvelope;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/queue/QueueDumper$TaskEnvelope;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/queue/QueueDumper$TaskEnvelope;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/queue/QueueDumper$Dump;->dumpId:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/squareup/queue/QueueDumper$Dump;->lastCaptureId:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/squareup/queue/QueueDumper$Dump;->lastLocalPaymentServerId:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/squareup/queue/QueueDumper$Dump;->qCaptures:Ljava/util/List;

    .line 51
    iput-object p6, p0, Lcom/squareup/queue/QueueDumper$Dump;->qTasks:Ljava/util/List;

    .line 52
    iput-object p4, p0, Lcom/squareup/queue/QueueDumper$Dump;->merchantId:Ljava/lang/String;

    return-void
.end method
