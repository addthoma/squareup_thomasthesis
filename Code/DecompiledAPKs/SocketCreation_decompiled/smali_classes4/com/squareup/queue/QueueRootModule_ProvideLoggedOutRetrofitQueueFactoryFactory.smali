.class public final Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final converterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field

.field private final taskInjectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->taskInjectorProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->converterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;)",
            "Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLoggedOutRetrofitQueueFactory(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)",
            "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;"
        }
    .end annotation

    .line 45
    invoke-static {p0, p1}, Lcom/squareup/queue/QueueRootModule;->provideLoggedOutRetrofitQueueFactory(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->taskInjectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tape/TaskInjector;

    iget-object v1, p0, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->converterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-static {v0, v1}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->provideLoggedOutRetrofitQueueFactory(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideLoggedOutRetrofitQueueFactoryFactory;->get()Lcom/squareup/queue/retrofit/RetrofitQueueFactory;

    move-result-object v0

    return-object v0
.end method
