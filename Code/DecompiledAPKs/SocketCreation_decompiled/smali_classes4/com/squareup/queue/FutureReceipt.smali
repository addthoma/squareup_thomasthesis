.class public Lcom/squareup/queue/FutureReceipt;
.super Ljava/lang/Object;
.source "FutureReceipt.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final email:Ljava/lang/String;

.field private final emailId:Ljava/lang/String;

.field private final phone:Ljava/lang/String;

.field private final phoneId:Ljava/lang/String;

.field private final receiptDeclined:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/queue/FutureReceipt;->email:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/squareup/queue/FutureReceipt;->emailId:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/squareup/queue/FutureReceipt;->phone:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lcom/squareup/queue/FutureReceipt;->phoneId:Ljava/lang/String;

    .line 28
    iput-boolean p5, p0, Lcom/squareup/queue/FutureReceipt;->receiptDeclined:Z

    return-void
.end method


# virtual methods
.method public enqueue(Lcom/squareup/queue/retrofit/RetrofitQueue;Ljava/lang/String;)V
    .locals 3

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/FutureReceipt;->email:Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 40
    new-instance v0, Lcom/squareup/queue/EmailReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/EmailReceipt$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/queue/EmailReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->email:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/queue/EmailReceipt$Builder;->email(Ljava/lang/String;)Lcom/squareup/queue/EmailReceipt$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/queue/EmailReceipt$Builder;->build()Lcom/squareup/queue/EmailReceipt;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/FutureReceipt;->emailId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 43
    new-instance v0, Lcom/squareup/queue/EmailReceiptById$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/EmailReceiptById$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/queue/EmailReceiptById$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceiptById$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->emailId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/queue/EmailReceiptById$Builder;->emailId(Ljava/lang/String;)Lcom/squareup/queue/EmailReceiptById$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/queue/EmailReceiptById$Builder;->build()Lcom/squareup/queue/EmailReceiptById;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 47
    :goto_1
    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phone:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 48
    new-instance v0, Lcom/squareup/queue/SmsReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/SmsReceipt$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/queue/SmsReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phone:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/queue/SmsReceipt$Builder;->phone(Ljava/lang/String;)Lcom/squareup/queue/SmsReceipt$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/queue/SmsReceipt$Builder;->build()Lcom/squareup/queue/SmsReceipt;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_2

    .line 50
    :cond_2
    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phoneId:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 51
    new-instance v0, Lcom/squareup/queue/SmsReceiptById$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/SmsReceiptById$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/queue/SmsReceiptById$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phoneId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/queue/SmsReceiptById$Builder;->phoneId(Ljava/lang/String;)Lcom/squareup/queue/SmsReceiptById$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/queue/SmsReceiptById$Builder;->build()Lcom/squareup/queue/SmsReceiptById;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    move v1, v0

    :goto_2
    if-nez v1, :cond_5

    .line 58
    iget-boolean v0, p0, Lcom/squareup/queue/FutureReceipt;->receiptDeclined:Z

    if-eqz v0, :cond_4

    .line 59
    new-instance v0, Lcom/squareup/queue/DeclineReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/DeclineReceipt$Builder;-><init>()V

    invoke-virtual {v0, p2}, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/DeclineReceipt$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/queue/DeclineReceipt$Builder;->build()Lcom/squareup/queue/DeclineReceipt;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    goto :goto_3

    .line 61
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "We weren\'t told what to do with the receipt."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    :cond_5
    :goto_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FutureReceipt{emailAddress=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/FutureReceipt;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", emailId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->emailId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", phone=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phone:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", phoneId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/FutureReceipt;->phoneId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", receiptDeclined="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/queue/FutureReceipt;->receiptDeclined:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
