.class public final Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;
.super Ljava/lang/Object;
.source "QueueRootModule_ProvideSerializedConverterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/tape/SerializedConverter<",
        "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory$InstanceHolder;->access$000()Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideSerializedConverter()Lcom/squareup/tape/SerializedConverter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-static {}, Lcom/squareup/queue/QueueRootModule;->provideSerializedConverter()Lcom/squareup/tape/SerializedConverter;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tape/SerializedConverter;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/tape/SerializedConverter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/tape/SerializedConverter<",
            "Lcom/squareup/queue/StoreAndForwardQueueFactory$EnqueuedPaymentBytes;",
            ">;"
        }
    .end annotation

    .line 19
    invoke-static {}, Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;->provideSerializedConverter()Lcom/squareup/tape/SerializedConverter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/queue/QueueRootModule_ProvideSerializedConverterFactory;->get()Lcom/squareup/tape/SerializedConverter;

    move-result-object v0

    return-object v0
.end method
