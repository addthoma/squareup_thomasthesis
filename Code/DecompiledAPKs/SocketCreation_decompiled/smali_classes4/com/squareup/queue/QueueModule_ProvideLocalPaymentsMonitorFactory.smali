.class public final Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;
.super Ljava/lang/Object;
.source "QueueModule_ProvideLocalPaymentsMonitorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final monitorSqliteQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/MonitorSqliteQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/MonitorSqliteQueue;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;->monitorSqliteQueueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/sqlite/MonitorSqliteQueue;",
            ">;)",
            "Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocalPaymentsMonitor(Lcom/squareup/queue/sqlite/MonitorSqliteQueue;)Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;
    .locals 1

    .line 38
    invoke-static {p0}, Lcom/squareup/queue/QueueModule;->provideLocalPaymentsMonitor(Lcom/squareup/queue/sqlite/MonitorSqliteQueue;)Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;->monitorSqliteQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/sqlite/MonitorSqliteQueue;

    invoke-static {v0}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;->provideLocalPaymentsMonitor(Lcom/squareup/queue/sqlite/MonitorSqliteQueue;)Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/queue/QueueModule_ProvideLocalPaymentsMonitorFactory;->get()Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;

    move-result-object v0

    return-object v0
.end method
