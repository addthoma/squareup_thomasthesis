.class public Lcom/squareup/queue/QueueDumper;
.super Ljava/lang/Object;
.source "QueueDumper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/QueueDumper$Dump;,
        Lcom/squareup/queue/QueueDumper$TaskEnvelope;
    }
.end annotation


# instance fields
.field private final capturesQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final lastCaptureId:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tasksQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field private final userId:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Ljavax/inject/Provider;)V
    .locals 0
    .param p3    # Lcom/squareup/settings/LocalSetting;
        .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/queue/QueueDumper;->capturesQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 62
    iput-object p2, p0, Lcom/squareup/queue/QueueDumper;->tasksQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 63
    iput-object p3, p0, Lcom/squareup/queue/QueueDumper;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    .line 64
    iput-object p4, p0, Lcom/squareup/queue/QueueDumper;->lastCaptureId:Lcom/squareup/settings/LocalSetting;

    .line 65
    iput-object p5, p0, Lcom/squareup/queue/QueueDumper;->userId:Ljavax/inject/Provider;

    return-void
.end method

.method private copyTasks(Lcom/squareup/queue/retrofit/RetrofitQueue;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/queue/QueueDumper$TaskEnvelope;",
            ">;"
        }
    .end annotation

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 79
    invoke-interface {p1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->asList()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/retrofit/RetrofitTask;

    .line 80
    new-instance v2, Lcom/squareup/queue/QueueDumper$TaskEnvelope;

    invoke-interface {v1}, Lcom/squareup/queue/retrofit/RetrofitTask;->secureCopyWithoutPIIForLogs()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/squareup/queue/QueueDumper$TaskEnvelope;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public dump(Ljava/lang/String;)Lcom/squareup/queue/QueueDumper$Dump;
    .locals 8

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/QueueDumper;->capturesQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-direct {p0, v0}, Lcom/squareup/queue/QueueDumper;->copyTasks(Lcom/squareup/queue/retrofit/RetrofitQueue;)Ljava/util/List;

    move-result-object v4

    .line 70
    iget-object v0, p0, Lcom/squareup/queue/QueueDumper;->tasksQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-direct {p0, v0}, Lcom/squareup/queue/QueueDumper;->copyTasks(Lcom/squareup/queue/retrofit/RetrofitQueue;)Ljava/util/List;

    move-result-object v7

    .line 72
    new-instance v0, Lcom/squareup/queue/QueueDumper$Dump;

    iget-object v1, p0, Lcom/squareup/queue/QueueDumper;->lastCaptureId:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/queue/QueueDumper;->userId:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/queue/QueueDumper;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    .line 73
    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Ljava/lang/String;

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/QueueDumper$Dump;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method
