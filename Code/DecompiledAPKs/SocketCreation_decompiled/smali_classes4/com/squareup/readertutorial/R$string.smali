.class public final Lcom/squareup/readertutorial/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/readertutorial/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f1200d0

.field public static final r12_education_charge_message:I = 0x7f12151f

.field public static final r12_education_charge_title:I = 0x7f121520

.field public static final r12_education_dip_message:I = 0x7f121521

.field public static final r12_education_dip_title:I = 0x7f121522

.field public static final r12_education_start_button:I = 0x7f121523

.field public static final r12_education_start_message:I = 0x7f121524

.field public static final r12_education_start_title:I = 0x7f121525

.field public static final r12_education_swipe_message:I = 0x7f121526

.field public static final r12_education_swipe_message_au:I = 0x7f121527

.field public static final r12_education_swipe_title:I = 0x7f121528

.field public static final r12_education_swipe_title_au:I = 0x7f121529

.field public static final r12_education_tap_message:I = 0x7f12152a

.field public static final r12_education_tap_title:I = 0x7f12152b

.field public static final r12_education_video_message:I = 0x7f12152c

.field public static final r12_education_video_title:I = 0x7f12152d

.field public static final r12_education_welcome_button:I = 0x7f12152e

.field public static final r12_education_welcome_message_standard:I = 0x7f12152f

.field public static final r12_education_welcome_message_updating:I = 0x7f121530

.field public static final r12_education_welcome_title_standard:I = 0x7f121531

.field public static final r12_education_welcome_title_updating:I = 0x7f121532

.field public static final r12_video_option_connect:I = 0x7f121533

.field public static final r12_video_option_message:I = 0x7f121534

.field public static final r12_video_option_title:I = 0x7f121535

.field public static final r12_video_option_watch:I = 0x7f121536

.field public static final r6_first_time_video_subtitle:I = 0x7f121537

.field public static final r6_first_time_video_title:I = 0x7f121538

.field public static final speed_test_checking_connection:I = 0x7f121835

.field public static final speed_test_complete_message:I = 0x7f121836

.field public static final speed_test_complete_title:I = 0x7f121837

.field public static final speed_test_failure_message:I = 0x7f121838

.field public static final speed_test_failure_title:I = 0x7f121839

.field public static final speed_test_failure_url:I = 0x7f12183a

.field public static final speed_test_in_progress:I = 0x7f12183b

.field public static final speed_test_success_url:I = 0x7f12183c

.field public static final speed_test_support_link_text:I = 0x7f12183d

.field public static final stop_movie:I = 0x7f1218cc


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
