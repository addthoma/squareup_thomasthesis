.class final Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;
.super Ljava/lang/Object;
.source "ProgressBinder.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/progress/ProgressBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ProgressDialogFactory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\'\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/progress/Progress$ScreenData;",
        "Lcom/squareup/progress/Progress$ProgressComplete;",
        "Lcom/squareup/progress/ProgressScreen;",
        "(Lcom/squareup/progress/ProgressBinder;Lio/reactivex/Observable;)V",
        "getScreen",
        "()Lio/reactivex/Observable;",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "progress_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/progress/ProgressBinder;


# direct methods
.method public constructor <init>(Lcom/squareup/progress/ProgressBinder;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->this$0:Lcom/squareup/progress/ProgressBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->screen:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->inflate(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    .line 37
    new-instance v0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory$create$1;-><init>(Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;Landroid/view/View;)V

    check-cast v0, Lcom/squareup/coordinators/CoordinatorProvider;

    invoke-static {p1, v0}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    .line 43
    invoke-static {p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->createDialog(Landroid/view/View;)Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(ProgressDial\u2026nator.createDialog(view))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getScreen()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/progress/ProgressBinder$ProgressDialogFactory;->screen:Lio/reactivex/Observable;

    return-object v0
.end method
