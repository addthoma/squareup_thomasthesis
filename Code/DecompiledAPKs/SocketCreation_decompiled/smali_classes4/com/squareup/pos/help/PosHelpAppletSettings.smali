.class public final Lcom/squareup/pos/help/PosHelpAppletSettings;
.super Ljava/lang/Object;
.source "PosHelpAppletSettings.kt"

# interfaces
.implements Lcom/squareup/ui/help/api/HelpAppletSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u00a9\u0001\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0008\u0008\u0001\u0010(\u001a\u00020)\u00a2\u0006\u0002\u0010*J\u0008\u0010+\u001a\u00020)H\u0016J\u0008\u0010,\u001a\u00020-H\u0016J\u000e\u0010.\u001a\u0008\u0012\u0004\u0012\u0002000/H\u0016J\u000e\u00101\u001a\u0008\u0012\u0004\u0012\u00020302H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/pos/help/PosHelpAppletSettings;",
        "Lcom/squareup/ui/help/api/HelpAppletSettings;",
        "helpSectionListEntry",
        "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
        "tutorialsSectionEntry",
        "Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;",
        "ordersSectionListEntry",
        "Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;",
        "announcementsSectionListEntry",
        "Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;",
        "troubleshootingSectionEntry",
        "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;",
        "aboutSectionListEntry",
        "Lcom/squareup/ui/help/about/AboutSection$ListEntry;",
        "legalSectionListEntry",
        "Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;",
        "referralsSectionListEntry",
        "Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;",
        "whatsNewTutorial",
        "Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;",
        "firstPaymentTutorial",
        "Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;",
        "r12Tutorial",
        "Lcom/squareup/ui/help/tutorials/content/R12Tutorial;",
        "r6Tutorial",
        "Lcom/squareup/ui/help/tutorials/content/R6Tutorial;",
        "firstInvoiceTutorial",
        "Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;",
        "createItemTutorial",
        "Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;",
        "enrollLoyaltyCustomerTutorial",
        "Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;",
        "adjustPointsLoyaltyTutorial",
        "Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;",
        "redeemRewardsTutorial",
        "Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;",
        "feeTutorial",
        "Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;",
        "quickAmountsTutorial",
        "Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;",
        "registerVersionName",
        "",
        "(Lcom/squareup/ui/help/help/HelpSection$ListEntry;Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;Lcom/squareup/ui/help/about/AboutSection$ListEntry;Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;Lcom/squareup/ui/help/tutorials/content/R12Tutorial;Lcom/squareup/ui/help/tutorials/content/R6Tutorial;Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;Ljava/lang/String;)V",
        "aboutVersionString",
        "canClickLibraryLinks",
        "",
        "helpAppletSectionsEntries",
        "",
        "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
        "helpAppletTutorials",
        "",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "spos-help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final aboutSectionListEntry:Lcom/squareup/ui/help/about/AboutSection$ListEntry;

.field private final adjustPointsLoyaltyTutorial:Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;

.field private final announcementsSectionListEntry:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

.field private final createItemTutorial:Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;

.field private final enrollLoyaltyCustomerTutorial:Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;

.field private final feeTutorial:Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;

.field private final firstInvoiceTutorial:Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;

.field private final firstPaymentTutorial:Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

.field private final helpSectionListEntry:Lcom/squareup/ui/help/help/HelpSection$ListEntry;

.field private final legalSectionListEntry:Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;

.field private final ordersSectionListEntry:Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;

.field private final quickAmountsTutorial:Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

.field private final r12Tutorial:Lcom/squareup/ui/help/tutorials/content/R12Tutorial;

.field private final r6Tutorial:Lcom/squareup/ui/help/tutorials/content/R6Tutorial;

.field private final redeemRewardsTutorial:Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;

.field private final referralsSectionListEntry:Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;

.field private final registerVersionName:Ljava/lang/String;

.field private final troubleshootingSectionEntry:Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;

.field private final tutorialsSectionEntry:Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;

.field private final whatsNewTutorial:Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/help/HelpSection$ListEntry;Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;Lcom/squareup/ui/help/about/AboutSection$ListEntry;Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;Lcom/squareup/ui/help/tutorials/content/R12Tutorial;Lcom/squareup/ui/help/tutorials/content/R6Tutorial;Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;Ljava/lang/String;)V
    .locals 16
    .param p20    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/util/PosSdkVersionName;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "helpSectionListEntry"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialsSectionEntry"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersSectionListEntry"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "announcementsSectionListEntry"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "troubleshootingSectionEntry"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "aboutSectionListEntry"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "legalSectionListEntry"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referralsSectionListEntry"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "whatsNewTutorial"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstPaymentTutorial"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "r12Tutorial"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "r6Tutorial"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "firstInvoiceTutorial"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createItemTutorial"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enrollLoyaltyCustomerTutorial"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adjustPointsLoyaltyTutorial"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redeemRewardsTutorial"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "feeTutorial"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmountsTutorial"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "registerVersionName"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->helpSectionListEntry:Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    iput-object v2, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->tutorialsSectionEntry:Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;

    iput-object v3, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->ordersSectionListEntry:Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;

    iput-object v4, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->announcementsSectionListEntry:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

    iput-object v5, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->troubleshootingSectionEntry:Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;

    iput-object v6, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->aboutSectionListEntry:Lcom/squareup/ui/help/about/AboutSection$ListEntry;

    iput-object v7, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->legalSectionListEntry:Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;

    iput-object v8, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->referralsSectionListEntry:Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;

    iput-object v9, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->whatsNewTutorial:Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;

    iput-object v10, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->firstPaymentTutorial:Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    iput-object v11, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->r12Tutorial:Lcom/squareup/ui/help/tutorials/content/R12Tutorial;

    iput-object v12, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->r6Tutorial:Lcom/squareup/ui/help/tutorials/content/R6Tutorial;

    iput-object v13, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->firstInvoiceTutorial:Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;

    iput-object v14, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->createItemTutorial:Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->enrollLoyaltyCustomerTutorial:Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;

    iput-object v15, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->adjustPointsLoyaltyTutorial:Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->redeemRewardsTutorial:Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;

    iput-object v2, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->feeTutorial:Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->quickAmountsTutorial:Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    iput-object v2, v0, Lcom/squareup/pos/help/PosHelpAppletSettings;->registerVersionName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public aboutVersionString()Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->registerVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public canClickLibraryLinks()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public helpAppletSectionsEntries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
            ">;"
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    .line 54
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->helpSectionListEntry:Lcom/squareup/ui/help/help/HelpSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 55
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->tutorialsSectionEntry:Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 56
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->ordersSectionListEntry:Lcom/squareup/ui/help/orders/OrdersSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 57
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->referralsSectionListEntry:Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 58
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->announcementsSectionListEntry:Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 59
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->troubleshootingSectionEntry:Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 60
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->aboutSectionListEntry:Lcom/squareup/ui/help/about/AboutSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 61
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->legalSectionListEntry:Lcom/squareup/ui/help/legal/PosLegalSection$ListEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 53
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "unmodifiableList<HelpApp\u2026galSectionListEntry\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public helpAppletTutorials()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/ui/help/HelpAppletContent;

    .line 67
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->whatsNewTutorial:Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 68
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->feeTutorial:Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 69
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->firstPaymentTutorial:Lcom/squareup/ui/help/tutorials/content/FirstPaymentTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 70
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->r12Tutorial:Lcom/squareup/ui/help/tutorials/content/R12Tutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 71
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->r6Tutorial:Lcom/squareup/ui/help/tutorials/content/R6Tutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 72
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->firstInvoiceTutorial:Lcom/squareup/ui/help/tutorials/content/FirstInvoiceTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 73
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->createItemTutorial:Lcom/squareup/ui/help/tutorials/content/CreateItemTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 74
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->quickAmountsTutorial:Lcom/squareup/ui/help/tutorials/content/QuickAmountsTutorialEntry;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 75
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->enrollLoyaltyCustomerTutorial:Lcom/squareup/ui/help/tutorials/content/EnrollLoyaltyCustomerTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 76
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->adjustPointsLoyaltyTutorial:Lcom/squareup/ui/help/tutorials/content/AdjustPointsLoyaltyTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 77
    iget-object v1, p0, Lcom/squareup/pos/help/PosHelpAppletSettings;->redeemRewardsTutorial:Lcom/squareup/ui/help/tutorials/content/RedeemRewardsTutorial;

    check-cast v1, Lcom/squareup/ui/help/HelpAppletContent;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 66
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->unmodifiableList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "unmodifiableList<HelpApp\u2026deemRewardsTutorial\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
