.class final Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER;
.super Lcom/squareup/pos/loggedout/PosLearnMoreTour;
.source "PosLearnMoreTour.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pos/loggedout/PosLearnMoreTour;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "LEARN_MORE_READER"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPosLearnMoreTour.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PosLearnMoreTour.kt\ncom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER\n*L\n1#1,50:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0001\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/pos/loggedout/PosLearnMoreTour$LEARN_MORE_READER;",
        "Lcom/squareup/pos/loggedout/PosLearnMoreTour;",
        "addPages",
        "",
        "pages",
        "",
        "Lcom/squareup/tour/TourPage;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/pos/loggedout/PosLearnMoreTour;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public addPages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tour/TourPage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pages"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    check-cast p1, Ljava/util/Collection;

    .line 34
    sget v0, Lcom/squareup/loggedout/R$drawable;->tour_learn_more_reader:I

    .line 35
    sget v1, Lcom/squareup/pos/loggedout/R$string;->tour_learn_more_reader_title:I

    .line 36
    sget v2, Lcom/squareup/pos/loggedout/R$string;->tour_learn_more_reader_subtitle:I

    .line 33
    invoke-static {v0, v1, v2}, Lcom/squareup/tour/TourPage;->pageCenter(III)Lcom/squareup/tour/TourPage;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method
