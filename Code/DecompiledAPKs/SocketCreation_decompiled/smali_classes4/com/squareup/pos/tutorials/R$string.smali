.class public final Lcom/squareup/pos/tutorials/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pos/tutorials/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final first_invoice_tutorial_add_customer:I = 0x7f120ac3

.field public static final first_invoice_tutorial_add_customer_info:I = 0x7f120ac4

.field public static final first_invoice_tutorial_add_customer_info_save:I = 0x7f120ac5

.field public static final first_invoice_tutorial_add_item:I = 0x7f120ac6

.field public static final first_invoice_tutorial_add_item_info:I = 0x7f120ac7

.field public static final first_invoice_tutorial_add_item_info_save:I = 0x7f120ac8

.field public static final first_invoice_tutorial_choose_customer:I = 0x7f120ac9

.field public static final first_invoice_tutorial_choose_item:I = 0x7f120aca

.field public static final first_invoice_tutorial_create:I = 0x7f120acb

.field public static final first_invoice_tutorial_preview:I = 0x7f120acc

.field public static final first_invoice_tutorial_preview_confirm:I = 0x7f120acd

.field public static final loyalty_adjust_points_step_one_description:I = 0x7f120f05

.field public static final loyalty_adjust_points_step_one_title:I = 0x7f120f06

.field public static final loyalty_adjust_points_step_three_description:I = 0x7f120f07

.field public static final loyalty_adjust_points_step_three_title:I = 0x7f120f08

.field public static final loyalty_adjust_points_step_two_description:I = 0x7f120f09

.field public static final loyalty_adjust_points_step_two_title:I = 0x7f120f0a

.field public static final loyalty_enroll_step_one_description:I = 0x7f120f33

.field public static final loyalty_enroll_step_one_title:I = 0x7f120f34

.field public static final loyalty_enroll_step_three_description:I = 0x7f120f35

.field public static final loyalty_enroll_step_three_title:I = 0x7f120f36

.field public static final loyalty_enroll_step_two_description:I = 0x7f120f37

.field public static final loyalty_enroll_step_two_title:I = 0x7f120f38

.field public static final loyalty_redeem_rewards_step_one_description:I = 0x7f120f46

.field public static final loyalty_redeem_rewards_step_one_title:I = 0x7f120f47

.field public static final loyalty_redeem_rewards_step_three_description:I = 0x7f120f48

.field public static final loyalty_redeem_rewards_step_three_title:I = 0x7f120f49

.field public static final loyalty_redeem_rewards_step_two_description:I = 0x7f120f4a

.field public static final loyalty_redeem_rewards_step_two_title:I = 0x7f120f4b

.field public static final new_feature_automatic_reminders:I = 0x7f121067

.field public static final scroll_down_to_try_out_new_feature:I = 0x7f121786

.field public static final tutorial_fi_end_done:I = 0x7f121a70

.field public static final tutorial_fp_content_action_or_choose:I = 0x7f121a71

.field public static final tutorial_fp_content_action_or_enable:I = 0x7f121a72

.field public static final tutorial_fp_content_actions_connect:I = 0x7f121a73

.field public static final tutorial_fp_content_actions_dip_contactless:I = 0x7f121a74

.field public static final tutorial_fp_content_actions_dip_swipe:I = 0x7f121a75

.field public static final tutorial_fp_content_actions_dip_swipe_contactless:I = 0x7f121a76

.field public static final tutorial_fp_content_actions_dip_tap:I = 0x7f121a77

.field public static final tutorial_fp_content_actions_swipe:I = 0x7f121a78

.field public static final tutorial_fp_content_actions_swipe_dip:I = 0x7f121a79

.field public static final tutorial_fp_content_actions_swipe_dip_contactless:I = 0x7f121a7a

.field public static final tutorial_fp_content_actions_swipe_dip_tap:I = 0x7f121a7b

.field public static final tutorial_fp_content_actions_tap_dip:I = 0x7f121a7c

.field public static final tutorial_fp_content_actions_tap_dip_swipe:I = 0x7f121a7d

.field public static final tutorial_fp_content_cash_payment_screen:I = 0x7f121a7e

.field public static final tutorial_fp_content_charge_less:I = 0x7f121a7f

.field public static final tutorial_fp_content_charge_more:I = 0x7f121a80

.field public static final tutorial_fp_content_choose_or_create_ticket:I = 0x7f121a81

.field public static final tutorial_fp_content_cnp_entry:I = 0x7f121a82

.field public static final tutorial_fp_content_continue_payment:I = 0x7f121a83

.field public static final tutorial_fp_content_invoice:I = 0x7f121a84

.field public static final tutorial_fp_content_new_sale:I = 0x7f121a85

.field public static final tutorial_fp_content_receipt:I = 0x7f121a86

.field public static final tutorial_fp_content_sign:I = 0x7f121a87

.field public static final tutorial_fp_content_start:I = 0x7f121a88

.field public static final tutorial_fp_content_start_items_keypad_phone_landscape:I = 0x7f121a89

.field public static final tutorial_fp_content_start_items_keypad_phone_portrait:I = 0x7f121a8a

.field public static final tutorial_fp_content_start_items_keypad_tablet_landscape:I = 0x7f121a8b

.field public static final tutorial_fp_content_start_items_keypad_tablet_portrait:I = 0x7f121a8c

.field public static final tutorial_fp_content_start_items_library_no_items_phone_landscape:I = 0x7f121a8d

.field public static final tutorial_fp_content_start_items_library_no_items_phone_portrait:I = 0x7f121a8e

.field public static final tutorial_fp_content_start_items_library_no_items_tablet_landscape:I = 0x7f121a8f

.field public static final tutorial_fp_content_start_items_library_no_items_tablet_portrait:I = 0x7f121a90

.field public static final tutorial_fp_content_start_items_library_phone_landscape:I = 0x7f121a91

.field public static final tutorial_fp_content_start_items_library_phone_portrait:I = 0x7f121a92

.field public static final tutorial_fp_content_start_items_library_tablet_landscape:I = 0x7f121a93

.field public static final tutorial_fp_content_start_items_library_tablet_portrait:I = 0x7f121a94

.field public static final tutorial_fp_content_tap_cash_phone:I = 0x7f121a95

.field public static final tutorial_fp_content_tap_cash_tablet:I = 0x7f121a96

.field public static final tutorial_fp_content_tap_charge:I = 0x7f121a97

.field public static final tutorial_fp_content_tipping:I = 0x7f121a98

.field public static final tutorial_fp_end_content_failed_verify:I = 0x7f121a99

.field public static final tutorial_fp_end_content_history:I = 0x7f121a9a

.field public static final tutorial_fp_end_content_no_bank:I = 0x7f121a9b

.field public static final tutorial_fp_end_content_pending:I = 0x7f121a9c

.field public static final tutorial_fp_end_content_verified:I = 0x7f121a9d

.field public static final tutorial_fp_end_done:I = 0x7f121a9e

.field public static final tutorial_fp_end_link_bank_account:I = 0x7f121a9f

.field public static final tutorial_fp_end_link_history:I = 0x7f121aa0

.field public static final tutorial_fp_end_title:I = 0x7f121aa1

.field public static final tutorial_fp_exit_content:I = 0x7f121aa2

.field public static final tutorial_fp_exit_continue:I = 0x7f121aa3

.field public static final tutorial_fp_exit_end:I = 0x7f121aa4

.field public static final tutorial_fp_exit_title:I = 0x7f121aa5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
