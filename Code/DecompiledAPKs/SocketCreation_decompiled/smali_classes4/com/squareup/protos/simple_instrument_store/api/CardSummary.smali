.class public final Lcom/squareup/protos/simple_instrument_store/api/CardSummary;
.super Lcom/squareup/wire/Message;
.source "CardSummary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/simple_instrument_store/api/CardSummary$ProtoAdapter_CardSummary;,
        Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/simple_instrument_store/api/CardSummary;",
        "Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/simple_instrument_store/api/CardSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BIN:Ljava/lang/String; = ""

.field public static final DEFAULT_BRAND:Lcom/squareup/protos/simple_instrument_store/model/Brand;

.field public static final DEFAULT_CARDHOLDER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_FOUR:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.simple_instrument_store.model.Brand#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final cardholder_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.simple_instrument_store.model.ExpirationDate#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$ProtoAdapter_CardSummary;

    invoke-direct {v0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$ProtoAdapter_CardSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/model/Brand;->INVALID:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->DEFAULT_BRAND:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;)V
    .locals 7

    .line 86
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;-><init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/Brand;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;Lokio/ByteString;)V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    .line 93
    iput-object p2, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 114
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    iget-object v3, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    iget-object p1, p1, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    .line 121
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 126
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/simple_instrument_store/model/Brand;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 134
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->last_four:Ljava/lang/String;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->cardholder_name:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->bin:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->newBuilder()Lcom/squareup/protos/simple_instrument_store/api/CardSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", last_four=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    if-eqz v1, :cond_1

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->brand:Lcom/squareup/protos/simple_instrument_store/model/Brand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->cardholder_name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", cardholder_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->bin:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", bin=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    if-eqz v1, :cond_4

    const-string v1, ", expiration_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/api/CardSummary;->expiration_date:Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardSummary{"

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
