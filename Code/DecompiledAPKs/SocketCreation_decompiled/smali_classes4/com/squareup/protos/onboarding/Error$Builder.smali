.class public final Lcom/squareup/protos/onboarding/Error$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/onboarding/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/onboarding/Error;",
        "Lcom/squareup/protos/onboarding/Error$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public field:Ljava/lang/String;

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/onboarding/Error;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/onboarding/Error;

    iget-object v1, p0, Lcom/squareup/protos/onboarding/Error$Builder;->field:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/onboarding/Error$Builder;->message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/onboarding/Error;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/onboarding/Error$Builder;->build()Lcom/squareup/protos/onboarding/Error;

    move-result-object v0

    return-object v0
.end method

.method public field(Ljava/lang/String;)Lcom/squareup/protos/onboarding/Error$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/onboarding/Error$Builder;->field:Ljava/lang/String;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/protos/onboarding/Error$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/onboarding/Error$Builder;->message:Ljava/lang/String;

    return-object p0
.end method
