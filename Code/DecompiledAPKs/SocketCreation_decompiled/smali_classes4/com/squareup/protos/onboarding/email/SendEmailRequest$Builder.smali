.class public final Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendEmailRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/onboarding/email/SendEmailRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/onboarding/email/SendEmailRequest;",
        "Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email:Ljava/lang/String;

.field public locale:Ljava/lang/String;

.field public serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/onboarding/email/SendEmailRequest;
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/protos/onboarding/email/SendEmailRequest;

    iget-object v1, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->email:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->locale:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->serial_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/onboarding/email/SendEmailRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->build()Lcom/squareup/protos/onboarding/email/SendEmailRequest;

    move-result-object v0

    return-object v0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public locale(Ljava/lang/String;)Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->locale:Ljava/lang/String;

    return-object p0
.end method

.method public serial_number(Ljava/lang/String;)Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/onboarding/email/SendEmailRequest$Builder;->serial_number:Ljava/lang/String;

    return-object p0
.end method
