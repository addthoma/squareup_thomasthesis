.class public final enum Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
.super Ljava/lang/Enum;
.source "SignalFound.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/SignalFound;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SwipeDirection"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection$ProtoAdapter_SwipeDirection;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field public static final enum FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

.field public static final enum UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 865
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/4 v1, 0x0

    const-string v2, "FORWARD"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 867
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/4 v2, 0x1

    const-string v3, "BACKWARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 869
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/4 v3, 0x2

    const-string v4, "UNKNOWN_DIRECTION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 864
    sget-object v4, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    .line 871
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection$ProtoAdapter_SwipeDirection;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection$ProtoAdapter_SwipeDirection;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 876
    iput p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 886
    :cond_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->UNKNOWN_DIRECTION:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0

    .line 885
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->BACKWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0

    .line 884
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->FORWARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
    .locals 1

    .line 864
    const-class v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;
    .locals 1

    .line 864
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->$VALUES:[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    invoke-virtual {v0}, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 893
    iget v0, p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$SwipeDirection;->value:I

    return v0
.end method
