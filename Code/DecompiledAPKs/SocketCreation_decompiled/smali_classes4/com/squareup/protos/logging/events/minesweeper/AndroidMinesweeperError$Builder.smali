.class public final Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AndroidMinesweeperError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;",
        "Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;-><init>(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;->build()Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;

    move-result-object v0

    return-object v0
.end method

.method public type(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;)Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;
    .locals 0

    .line 89
    iput-object p1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    return-object p0
.end method
