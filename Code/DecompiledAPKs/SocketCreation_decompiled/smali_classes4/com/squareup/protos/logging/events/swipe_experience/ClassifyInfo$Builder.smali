.class public final Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClassifyInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public analyze_ffts_runtime:Ljava/lang/Integer;

.field public calc_spacings_and_variability_runtime:Ljava/lang/Integer;

.field public calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

.field public ffts_runtime:Ljava/lang/Integer;

.field public find_peaks_runtime:Ljava/lang/Integer;

.field public gen2_low_pass_filter_runtime:Ljava/lang/Integer;

.field public gen2_score:Ljava/lang/Float;

.field public link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

.field public low_pass_filter_runtime_in_us:Ljava/lang/Integer;

.field public m1_fast_score:Ljava/lang/Float;

.field public m1_slow_score:Ljava/lang/Float;

.field public normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

.field public o1_score:Ljava/lang/Float;

.field public peak_count:Ljava/lang/Integer;

.field public peak_variability:Ljava/lang/Float;

.field public runtime_in_us:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 356
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public analyze_ffts_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->analyze_ffts_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;
    .locals 2

    .line 492
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 323
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo;

    move-result-object v0

    return-object v0
.end method

.method public calc_spacings_and_variability_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 438
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_spacings_and_variability_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public calc_square_and_remove_mean_runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->calc_square_and_remove_mean_runtime_in_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public ffts_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 396
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->ffts_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public find_peaks_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->find_peaks_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public gen2_low_pass_filter_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_low_pass_filter_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public gen2_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 470
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->gen2_score:Ljava/lang/Float;

    return-object p0
.end method

.method public link_type(Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 363
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->link_type:Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    return-object p0
.end method

.method public low_pass_filter_runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 379
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->low_pass_filter_runtime_in_us:Ljava/lang/Integer;

    return-object p0
.end method

.method public m1_fast_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_fast_score:Ljava/lang/Float;

    return-object p0
.end method

.method public m1_slow_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 462
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->m1_slow_score:Ljava/lang/Float;

    return-object p0
.end method

.method public normalize_and_center_around_mean_runtime(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->normalize_and_center_around_mean_runtime:Ljava/lang/Integer;

    return-object p0
.end method

.method public o1_score(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 446
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->o1_score:Ljava/lang/Float;

    return-object p0
.end method

.method public peak_count(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 478
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public peak_variability(Ljava/lang/Float;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 486
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->peak_variability:Ljava/lang/Float;

    return-object p0
.end method

.method public runtime_in_us(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;
    .locals 0

    .line 371
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$Builder;->runtime_in_us:Ljava/lang/Integer;

    return-object p0
.end method
