.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "O1Packet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

.field public o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

.field public o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

.field public total_length:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;
    .locals 7

    .line 172
    new-instance v6, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    iget-object v3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    iget-object v4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1Packet;

    move-result-object v0

    return-object v0
.end method

.method public o1_flash_error(Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_flash_error:Lcom/squareup/protos/logging/events/swipe_experience/O1FlashError;

    return-object p0
.end method

.method public o1_general_error(Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_general_error:Lcom/squareup/protos/logging/events/swipe_experience/O1GeneralError;

    return-object p0
.end method

.method public o1_successful_swipe(Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->o1_successful_swipe:Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    return-object p0
.end method

.method public total_length(Ljava/lang/Integer;)Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1Packet$Builder;->total_length:Ljava/lang/Integer;

    return-object p0
.end method
