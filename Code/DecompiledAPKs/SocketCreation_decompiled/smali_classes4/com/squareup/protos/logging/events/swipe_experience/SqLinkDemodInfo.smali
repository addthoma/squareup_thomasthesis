.class public final Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;
.super Lcom/squareup/wire/Message;
.source "SqLinkDemodInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$ProtoAdapter_SqLinkDemodInfo;,
        Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;,
        Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;,
        Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DECONVOLVE_BIT_ERRORS:Ljava/lang/Integer;

.field public static final DEFAULT_DECONVOLVE_NUM_BITS:Ljava/lang/Integer;

.field public static final DEFAULT_DECONVOLVE_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_DEMODULATE_PACKETS_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_DEPRECATED_RUNTIME_IN_US:Ljava/lang/Integer;

.field public static final DEFAULT_FIND_PREAMBLE_FREQ_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_FIRST_FIND_SYNC_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_INVERTED:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_FAST:Ljava/lang/Boolean;

.field public static final DEFAULT_LAST4_STATUS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

.field public static final DEFAULT_LOW_PASS_FILTER_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_PACKET_FREQUENCY0:Ljava/lang/Float;

.field public static final DEFAULT_PACKET_FREQUENCY1:Ljava/lang/Float;

.field public static final DEFAULT_PACKET_FREQUENCY2:Ljava/lang/Float;

.field public static final DEFAULT_PACKET_FREQUENCY3:Ljava/lang/Float;

.field public static final DEFAULT_PACKET_FREQUENCY4:Ljava/lang/Float;

.field public static final DEFAULT_PREAMBLE_FREQ:Ljava/lang/Float;

.field public static final DEFAULT_REST_FIND_SYNC_RUNTIME:Ljava/lang/Integer;

.field public static final DEFAULT_RESULT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

.field public static final DEFAULT_SYNC_INDEX0:Ljava/lang/Integer;

.field public static final DEFAULT_SYNC_INDEX1:Ljava/lang/Integer;

.field public static final DEFAULT_SYNC_INDEX2:Ljava/lang/Integer;

.field public static final DEFAULT_SYNC_INDEX3:Ljava/lang/Integer;

.field public static final DEFAULT_SYNC_INDEX4:Ljava/lang/Integer;

.field public static final DEFAULT_VITERBI_RUNTIME:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final deconvolve_bit_errors:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x18
    .end annotation
.end field

.field public final deconvolve_num_bits:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x19
    .end annotation
.end field

.field public final deconvolve_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xa
    .end annotation
.end field

.field public final demodulate_packets_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final deprecated_runtime_in_us:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final find_preamble_freq_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final first_find_sync_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final inverted:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final is_fast:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SqLinkDemodInfo$Last4Status#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final low_pass_filter_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final packet_frequency0:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x13
    .end annotation
.end field

.field public final packet_frequency1:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x14
    .end annotation
.end field

.field public final packet_frequency2:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x15
    .end annotation
.end field

.field public final packet_frequency3:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x16
    .end annotation
.end field

.field public final packet_frequency4:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0x17
    .end annotation
.end field

.field public final preamble_freq:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#FLOAT"
        tag = 0xd
    .end annotation
.end field

.field public final rest_find_sync_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.swipe_experience.SqLinkDemodInfo$DemodResult#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final sync_index0:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xe
    .end annotation
.end field

.field public final sync_index1:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xf
    .end annotation
.end field

.field public final sync_index2:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x10
    .end annotation
.end field

.field public final sync_index3:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x11
    .end annotation
.end field

.field public final sync_index4:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x12
    .end annotation
.end field

.field public final viterbi_runtime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$ProtoAdapter_SqLinkDemodInfo;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$ProtoAdapter_SqLinkDemodInfo;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->SUCCESS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_RESULT:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 34
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;->VALID:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_LAST4_STATUS:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_IS_FAST:Ljava/lang/Boolean;

    .line 38
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_DEPRECATED_RUNTIME_IN_US:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_LOW_PASS_FILTER_RUNTIME:Ljava/lang/Integer;

    .line 42
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_FIND_PREAMBLE_FREQ_RUNTIME:Ljava/lang/Integer;

    .line 44
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_FIRST_FIND_SYNC_RUNTIME:Ljava/lang/Integer;

    .line 46
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_REST_FIND_SYNC_RUNTIME:Ljava/lang/Integer;

    .line 48
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_DEMODULATE_PACKETS_RUNTIME:Ljava/lang/Integer;

    .line 50
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_DECONVOLVE_RUNTIME:Ljava/lang/Integer;

    .line 52
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_VITERBI_RUNTIME:Ljava/lang/Integer;

    .line 54
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_INVERTED:Ljava/lang/Boolean;

    const/4 v1, 0x0

    .line 56
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PREAMBLE_FREQ:Ljava/lang/Float;

    .line 58
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_SYNC_INDEX0:Ljava/lang/Integer;

    .line 60
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_SYNC_INDEX1:Ljava/lang/Integer;

    .line 62
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_SYNC_INDEX2:Ljava/lang/Integer;

    .line 64
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_SYNC_INDEX3:Ljava/lang/Integer;

    .line 66
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_SYNC_INDEX4:Ljava/lang/Integer;

    .line 68
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PACKET_FREQUENCY0:Ljava/lang/Float;

    .line 70
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PACKET_FREQUENCY1:Ljava/lang/Float;

    .line 72
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PACKET_FREQUENCY2:Ljava/lang/Float;

    .line 74
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PACKET_FREQUENCY3:Ljava/lang/Float;

    .line 76
    sput-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_PACKET_FREQUENCY4:Ljava/lang/Float;

    .line 78
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_DECONVOLVE_BIT_ERRORS:Ljava/lang/Integer;

    .line 80
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->DEFAULT_DECONVOLVE_NUM_BITS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;Lokio/ByteString;)V
    .locals 1

    .line 312
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 313
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 314
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    .line 315
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->is_fast:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    .line 316
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deprecated_runtime_in_us:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    .line 317
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->low_pass_filter_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    .line 318
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->find_preamble_freq_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    .line 319
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->first_find_sync_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    .line 320
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->rest_find_sync_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    .line 321
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->demodulate_packets_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    .line 322
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    .line 323
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->viterbi_runtime:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    .line 324
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->inverted:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    .line 325
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->preamble_freq:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    .line 326
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index0:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    .line 327
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index1:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    .line 328
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index2:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    .line 329
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index3:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    .line 330
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index4:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    .line 331
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency0:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    .line 332
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency1:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    .line 333
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency2:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    .line 334
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency3:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    .line 335
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency4:Ljava/lang/Float;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    .line 336
    iget-object p2, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_bit_errors:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    .line 337
    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_num_bits:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 375
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 376
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    .line 377
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 378
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    .line 379
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    .line 380
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    .line 381
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    .line 382
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    .line 383
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    .line 384
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    .line 385
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    .line 386
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    .line 387
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    .line 388
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    .line 389
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    .line 390
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    .line 391
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    .line 392
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    .line 393
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    .line 394
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    .line 395
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    .line 396
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    .line 397
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    .line 398
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    .line 399
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    .line 400
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    .line 401
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    .line 402
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 407
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_19

    .line 409
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 414
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 415
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 416
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 417
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 418
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 419
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 420
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 421
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 422
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 423
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 424
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 425
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 426
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 427
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 428
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 429
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 430
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 431
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 432
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Float;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 433
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 434
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_18
    add-int/2addr v0, v2

    .line 435
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_19
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;
    .locals 2

    .line 342
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;-><init>()V

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->is_fast:Ljava/lang/Boolean;

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deprecated_runtime_in_us:Ljava/lang/Integer;

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->low_pass_filter_runtime:Ljava/lang/Integer;

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->find_preamble_freq_runtime:Ljava/lang/Integer;

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->first_find_sync_runtime:Ljava/lang/Integer;

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->rest_find_sync_runtime:Ljava/lang/Integer;

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->demodulate_packets_runtime:Ljava/lang/Integer;

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_runtime:Ljava/lang/Integer;

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->viterbi_runtime:Ljava/lang/Integer;

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->inverted:Ljava/lang/Boolean;

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->preamble_freq:Ljava/lang/Float;

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index0:Ljava/lang/Integer;

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index1:Ljava/lang/Integer;

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index2:Ljava/lang/Integer;

    .line 359
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index3:Ljava/lang/Integer;

    .line 360
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->sync_index4:Ljava/lang/Integer;

    .line 361
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency0:Ljava/lang/Float;

    .line 362
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency1:Ljava/lang/Float;

    .line 363
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency2:Ljava/lang/Float;

    .line 364
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency3:Ljava/lang/Float;

    .line 365
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->packet_frequency4:Ljava/lang/Float;

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_bit_errors:Ljava/lang/Integer;

    .line 367
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->deconvolve_num_bits:Ljava/lang/Integer;

    .line 368
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 442
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 443
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    if-eqz v1, :cond_0

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->result:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 444
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    if-eqz v1, :cond_1

    const-string v1, ", last4_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->last4_status:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 445
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", is_fast="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->is_fast:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 446
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", deprecated_runtime_in_us="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deprecated_runtime_in_us:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 447
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", low_pass_filter_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->low_pass_filter_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 448
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", find_preamble_freq_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->find_preamble_freq_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 449
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", first_find_sync_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->first_find_sync_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 450
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", rest_find_sync_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->rest_find_sync_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 451
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", demodulate_packets_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->demodulate_packets_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 452
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", deconvolve_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 453
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    const-string v1, ", viterbi_runtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->viterbi_runtime:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 454
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", inverted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->inverted:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 455
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    if-eqz v1, :cond_c

    const-string v1, ", preamble_freq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->preamble_freq:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 456
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    const-string v1, ", sync_index0="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index0:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 457
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    const-string v1, ", sync_index1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index1:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 458
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    const-string v1, ", sync_index2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index2:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 459
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    const-string v1, ", sync_index3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index3:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 460
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    const-string v1, ", sync_index4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->sync_index4:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 461
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    if-eqz v1, :cond_12

    const-string v1, ", packet_frequency0="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency0:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 462
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    if-eqz v1, :cond_13

    const-string v1, ", packet_frequency1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency1:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 463
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    if-eqz v1, :cond_14

    const-string v1, ", packet_frequency2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency2:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 464
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    if-eqz v1, :cond_15

    const-string v1, ", packet_frequency3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency3:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 465
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    if-eqz v1, :cond_16

    const-string v1, ", packet_frequency4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->packet_frequency4:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 466
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    if-eqz v1, :cond_17

    const-string v1, ", deconvolve_bit_errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_bit_errors:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 467
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    const-string v1, ", deconvolve_num_bits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;->deconvolve_num_bits:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_18
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SqLinkDemodInfo{"

    .line 468
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
