.class public final Lcom/squareup/protos/cogs/EntityDescriptor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EntityDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/cogs/EntityDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/cogs/EntityDescriptor;",
        "Lcom/squareup/protos/cogs/EntityDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public index:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/cogs/IndexDescriptor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 80
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/cogs/EntityDescriptor$Builder;->index:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/cogs/EntityDescriptor;
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/protos/cogs/EntityDescriptor;

    iget-object v1, p0, Lcom/squareup/protos/cogs/EntityDescriptor$Builder;->index:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/cogs/EntityDescriptor;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/cogs/EntityDescriptor$Builder;->build()Lcom/squareup/protos/cogs/EntityDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public index(Ljava/util/List;)Lcom/squareup/protos/cogs/EntityDescriptor$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/cogs/IndexDescriptor;",
            ">;)",
            "Lcom/squareup/protos/cogs/EntityDescriptor$Builder;"
        }
    .end annotation

    .line 84
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/cogs/EntityDescriptor$Builder;->index:Ljava/util/List;

    return-object p0
.end method
