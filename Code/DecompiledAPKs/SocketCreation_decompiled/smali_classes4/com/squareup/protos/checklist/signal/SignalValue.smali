.class public final Lcom/squareup/protos/checklist/signal/SignalValue;
.super Lcom/squareup/wire/Message;
.source "SignalValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/signal/SignalValue$ProtoAdapter_SignalValue;,
        Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/checklist/signal/SignalValue;",
        "Lcom/squareup/protos/checklist/signal/SignalValue$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/signal/SignalValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EXT_ADDED_AUTHORIZED_REPRESENTATIVE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ADDED_A_LOCATION:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ADDED_MOBILE_STAFF:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ADDED_MORE_NEXT_STEPS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ALLOWED_REACTIVATION:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final DEFAULT_EXT_CARD_PAYMENT_SUCCESS_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_EXT_CREATED_AN_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_CREATED_BAR_CODE_LABEL:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_CUSTOMIZED_RECEIPTS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_DOWNLOADED_THE_INVOICES_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ENABLED_ITEM_FOR_SALE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ENABLED_TWO_FACTOR_AUTH:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ENABLE_PICKUP_AND_DELIVERY:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_ENGAGED_CUSTOMERS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_HAS_A_DEVICE_USAGE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_HAS_RETAIL_APP_USAGE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_IMPORTED_INVENTORY:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_MANAGED_EMPLOYEES:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_PUBLISHED_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_PUBLISHED_MERCHANT_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_REFERRAL_ACTIVATED:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

.field public static final DEFAULT_EXT_REVIEWED_BUSINESS_DETAILS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SAVED_A_MENU_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SAW_SECURE_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SENT_AN_INVOICE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SENT_A_DEVICE_CODE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SET_CLOSE_OF_DAY:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SET_UP_AN_RST_GRID_LAYOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SET_UP_A_FLOOR_PLAN:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SET_UP_TAXES:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SIGNUP_BUSINESS_CATEGORY:Ljava/lang/String; = ""

.field public static final DEFAULT_EXT_SIGNUP_BUSINESS_SUB_CATEGORY:Ljava/lang/String; = ""

.field public static final DEFAULT_EXT_SIGNUP_EGIFT_CARD:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

.field public static final DEFAULT_EXT_SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

.field public static final DEFAULT_EXT_SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

.field public static final DEFAULT_EXT_SIGNUP_VARIANT:Ljava/lang/String; = ""

.field public static final DEFAULT_EXT_STARTED_BIG_COMMERCE_RETAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_TOOK_A_CARD_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_TOOK_A_CASH_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_TOOK_A_CNP_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_UPDATED_SECURE_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_BUSINESS_FULFILLMENT_OPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_CUSTOMER_FEEDBACK:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_DISCOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_INVOICE_SETTINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_ONLINE_STORE_SELECTOR:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_ONLINE_STORE_SETUP:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_OTHER_SERVICES:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_REPORTS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_SOFTWARE_OVERVIEW:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_STAND_PAGE:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_VENDORS:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VIEWED_WEEBLY_OST:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VISITED_SELLER_COMMUNITY:Ljava/lang/Boolean;

.field public static final DEFAULT_EXT_VISITED_SQUARE_SHOP:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final ext_added_a_location:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1e
    .end annotation
.end field

.field public final ext_added_authorized_representative:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3a
    .end annotation
.end field

.field public final ext_added_mobile_staff:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final ext_added_more_next_steps:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final ext_allowed_reactivation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final ext_apps_with_device_code_usages:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x35
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.BankAccountLinkProgress#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final ext_card_payment_success_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x10
    .end annotation
.end field

.field public final ext_cardless_free_trial_features:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.Feature#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x28
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final ext_completed_action_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x34
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_completed_product_interests:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x29
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_created_an_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final ext_created_bar_code_label:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x44
    .end annotation
.end field

.field public final ext_customized_receipts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x21
    .end annotation
.end field

.field public final ext_dismissed_action_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_dismissed_action_item_time:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.ActionItemDismissal#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1b
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActionItemDismissal;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_downloaded_the_invoices_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x42
    .end annotation
.end field

.field public final ext_enable_pickup_and_delivery:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x45
    .end annotation
.end field

.field public final ext_enabled_item_for_sale:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final ext_enabled_two_factor_auth:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final ext_engaged_customers:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x23
    .end annotation
.end field

.field public final ext_has_a_device_usage:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final ext_has_paid_for_features:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.Feature#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2c
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_has_retail_app_usage:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2b
    .end annotation
.end field

.field public final ext_imported_inventory:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x43
    .end annotation
.end field

.field public final ext_managed_employees:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1f
    .end annotation
.end field

.field public final ext_next_step:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.common.NextStep#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/common/NextStep;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_published_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final ext_published_merchant_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final ext_recommended_products:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x26
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_referral_activated:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.ActivationStatus#ADAPTER"
        tag = 0x2e
    .end annotation
.end field

.field public final ext_register_activation_state_history:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.ActivationStatus#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2d
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/ActivationStatus;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_reviewed_business_details:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1d
    .end annotation
.end field

.field public final ext_saved_a_menu_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x33
    .end annotation
.end field

.field public final ext_saw_secure_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2f
    .end annotation
.end field

.field public final ext_sent_a_device_code:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final ext_sent_an_invoice:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final ext_set_close_of_day:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2a
    .end annotation
.end field

.field public final ext_set_up_a_floor_plan:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x31
    .end annotation
.end field

.field public final ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x30
    .end annotation
.end field

.field public final ext_set_up_taxes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x32
    .end annotation
.end field

.field public final ext_signup_business_category:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final ext_signup_business_sub_category:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x17
    .end annotation
.end field

.field public final ext_signup_egift_card:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x37
    .end annotation
.end field

.field public final ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.EstimatedEmployeeCount#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.EstimatedRevenue#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.EstimatedUsageType#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final ext_signup_variant:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x36
    .end annotation
.end field

.field public final ext_started_big_commerce_retail:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x40
    .end annotation
.end field

.field public final ext_subscribed_features:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.checklist.signal.Feature#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x27
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/checklist/signal/Feature;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_took_a_card_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final ext_took_a_cash_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final ext_took_a_cnp_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final ext_updated_secure_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x39
    .end annotation
.end field

.field public final ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final ext_viewed_customer_feedback:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x20
    .end annotation
.end field

.field public final ext_viewed_discounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3b
    .end annotation
.end field

.field public final ext_viewed_invoice_settings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x41
    .end annotation
.end field

.field public final ext_viewed_online_store_selector:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3f
    .end annotation
.end field

.field public final ext_viewed_online_store_setup:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3e
    .end annotation
.end field

.field public final ext_viewed_other_services:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x22
    .end annotation
.end field

.field public final ext_viewed_reports:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final ext_viewed_software_overview:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x38
    .end annotation
.end field

.field public final ext_viewed_stand_page:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final ext_viewed_vendors:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3c
    .end annotation
.end field

.field public final ext_viewed_weebly_ost:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3d
    .end annotation
.end field

.field public final ext_visited_seller_community:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x24
    .end annotation
.end field

.field public final ext_visited_square_shop:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x25
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalValue$ProtoAdapter_SignalValue;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/SignalValue$ProtoAdapter_SignalValue;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 33
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_REPORTS:Ljava/lang/Boolean;

    .line 35
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_TOOK_A_CASH_PAYMENT:Ljava/lang/Boolean;

    .line 37
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_TOOK_A_CARD_PAYMENT:Ljava/lang/Boolean;

    .line 39
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SENT_AN_INVOICE:Ljava/lang/Boolean;

    .line 41
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_CREATED_AN_ITEM:Ljava/lang/Boolean;

    .line 43
    sget-object v2, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->NONE:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    sput-object v2, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_BANK_ACCOUNT_LINK_PROGRESS:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 45
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_TOOK_A_CNP_PAYMENT:Ljava/lang/Boolean;

    .line 47
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ALLOWED_REACTIVATION:Ljava/lang/Boolean;

    .line 49
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_HAS_A_DEVICE_USAGE:Ljava/lang/Boolean;

    .line 51
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_PUBLISHED_MERCHANT_PROFILE:Ljava/lang/Boolean;

    .line 53
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_STAND_PAGE:Ljava/lang/Boolean;

    .line 55
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ADDED_MOBILE_STAFF:Ljava/lang/Boolean;

    .line 57
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ADDED_MORE_NEXT_STEPS:Ljava/lang/Boolean;

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_CARD_PAYMENT_SUCCESS_COUNT:Ljava/lang/Integer;

    .line 61
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_PUBLISHED_ITEM:Ljava/lang/Boolean;

    .line 63
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ENABLED_ITEM_FOR_SALE:Ljava/lang/Boolean;

    .line 65
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_BUSINESS_FULFILLMENT_OPTIONS:Ljava/lang/Boolean;

    .line 67
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_REFERRAL_ACTIVATED:Ljava/lang/Boolean;

    .line 69
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ENABLED_TWO_FACTOR_AUTH:Ljava/lang/Boolean;

    .line 75
    sget-object v0, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->REVENUE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SIGNUP_ESTIMATED_REVENUE:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 77
    sget-object v0, Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;->EMPLOYEE_COUNT_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SIGNUP_ESTIMATED_EMPLOYEES:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    .line 79
    sget-object v0, Lcom/squareup/protos/checklist/signal/EstimatedUsageType;->USAGE_DO_NOT_USE:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SIGNUP_ESTIMATED_USAGE:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    .line 81
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SENT_A_DEVICE_CODE:Ljava/lang/Boolean;

    .line 83
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_REVIEWED_BUSINESS_DETAILS:Ljava/lang/Boolean;

    .line 85
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ADDED_A_LOCATION:Ljava/lang/Boolean;

    .line 87
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_MANAGED_EMPLOYEES:Ljava/lang/Boolean;

    .line 89
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_CUSTOMER_FEEDBACK:Ljava/lang/Boolean;

    .line 91
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_CUSTOMIZED_RECEIPTS:Ljava/lang/Boolean;

    .line 93
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_OTHER_SERVICES:Ljava/lang/Boolean;

    .line 95
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ENGAGED_CUSTOMERS:Ljava/lang/Boolean;

    .line 97
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VISITED_SELLER_COMMUNITY:Ljava/lang/Boolean;

    .line 99
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VISITED_SQUARE_SHOP:Ljava/lang/Boolean;

    .line 101
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SET_CLOSE_OF_DAY:Ljava/lang/Boolean;

    .line 103
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_HAS_RETAIL_APP_USAGE:Ljava/lang/Boolean;

    .line 105
    sget-object v0, Lcom/squareup/protos/checklist/signal/ActivationStatus;->DO_NOT_USE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    sput-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_REGISTER_ACTIVATION_STATE:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 107
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SAW_SECURE_PROFILE:Ljava/lang/Boolean;

    .line 109
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SET_UP_AN_RST_GRID_LAYOUT:Ljava/lang/Boolean;

    .line 111
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SET_UP_A_FLOOR_PLAN:Ljava/lang/Boolean;

    .line 113
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SET_UP_TAXES:Ljava/lang/Boolean;

    .line 115
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SAVED_A_MENU_ITEM:Ljava/lang/Boolean;

    .line 119
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_SIGNUP_EGIFT_CARD:Ljava/lang/Boolean;

    .line 121
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_SOFTWARE_OVERVIEW:Ljava/lang/Boolean;

    .line 123
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_UPDATED_SECURE_PROFILE:Ljava/lang/Boolean;

    .line 125
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ADDED_AUTHORIZED_REPRESENTATIVE:Ljava/lang/Boolean;

    .line 127
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_DISCOUNTS:Ljava/lang/Boolean;

    .line 129
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_VENDORS:Ljava/lang/Boolean;

    .line 131
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_WEEBLY_OST:Ljava/lang/Boolean;

    .line 133
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_ONLINE_STORE_SETUP:Ljava/lang/Boolean;

    .line 135
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_ONLINE_STORE_SELECTOR:Ljava/lang/Boolean;

    .line 137
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_STARTED_BIG_COMMERCE_RETAIL:Ljava/lang/Boolean;

    .line 139
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_VIEWED_INVOICE_SETTINGS:Ljava/lang/Boolean;

    .line 141
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_DOWNLOADED_THE_INVOICES_APP:Ljava/lang/Boolean;

    .line 143
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_IMPORTED_INVENTORY:Ljava/lang/Boolean;

    .line 145
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_CREATED_BAR_CODE_LABEL:Ljava/lang/Boolean;

    .line 147
    sput-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->DEFAULT_EXT_ENABLE_PICKUP_AND_DELIVERY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/checklist/signal/SignalValue$Builder;Lokio/ByteString;)V
    .locals 1

    .line 784
    sget-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 785
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_reports:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    .line 786
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    .line 787
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_card_payment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    .line 788
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_an_invoice:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    .line 789
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_an_item:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    .line 790
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 791
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    .line 792
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_allowed_reactivation:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    .line 793
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_a_device_usage:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    .line 794
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_merchant_profile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    .line 795
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_stand_page:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    .line 796
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_mobile_staff:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    .line 797
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_more_next_steps:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    .line 798
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item:Ljava/util/List;

    const-string v0, "ext_dismissed_action_item"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    .line 799
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_card_payment_success_count:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    .line 800
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_item:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    .line 801
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    .line 802
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    .line 803
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_referral_activated:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    .line 804
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    .line 805
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_category:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    .line 806
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_sub_category:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    .line 807
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 808
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    .line 809
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    .line 810
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item_time:Ljava/util/List;

    const-string v0, "ext_dismissed_action_item_time"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    .line 811
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_a_device_code:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    .line 812
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_reviewed_business_details:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    .line 813
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_a_location:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    .line 814
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_managed_employees:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    .line 815
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    .line 816
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_customized_receipts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    .line 817
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_other_services:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    .line 818
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_engaged_customers:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    .line 819
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_seller_community:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    .line 820
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_square_shop:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    .line 821
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_recommended_products:Ljava/util/List;

    const-string v0, "ext_recommended_products"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    .line 822
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_subscribed_features:Ljava/util/List;

    const-string v0, "ext_subscribed_features"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    .line 823
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_cardless_free_trial_features:Ljava/util/List;

    const-string v0, "ext_cardless_free_trial_features"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    .line 824
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_product_interests:Ljava/util/List;

    const-string v0, "ext_completed_product_interests"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    .line 825
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_close_of_day:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    .line 826
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    .line 827
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_paid_for_features:Ljava/util/List;

    const-string v0, "ext_has_paid_for_features"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    .line 828
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state_history:Ljava/util/List;

    const-string v0, "ext_register_activation_state_history"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    .line 829
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 830
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saw_secure_profile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    .line 831
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    .line 832
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    .line 833
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_taxes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    .line 834
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    .line 835
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_action_item:Ljava/util/List;

    const-string v0, "ext_completed_action_item"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    .line 836
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_apps_with_device_code_usages:Ljava/util/List;

    const-string v0, "ext_apps_with_device_code_usages"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    .line 837
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_variant:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    .line 838
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_egift_card:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    .line 839
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_software_overview:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    .line 840
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_updated_secure_profile:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    .line 841
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_authorized_representative:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    .line 842
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_discounts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    .line 843
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_vendors:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    .line 844
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    .line 845
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    .line 846
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    .line 847
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    .line 848
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    .line 849
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    .line 850
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_imported_inventory:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    .line 851
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_bar_code_label:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    .line 852
    iget-object p2, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    .line 853
    iget-object p1, p1, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_next_step:Ljava/util/List;

    const-string p2, "ext_next_step"

    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 935
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/checklist/signal/SignalValue;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 936
    :cond_1
    check-cast p1, Lcom/squareup/protos/checklist/signal/SignalValue;

    .line 937
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/checklist/signal/SignalValue;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    .line 938
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    .line 939
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    .line 940
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    .line 941
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    .line 942
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 943
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    .line 944
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    .line 945
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    .line 946
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    .line 947
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    .line 948
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    .line 949
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    .line 950
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    .line 951
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    .line 952
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    .line 953
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    .line 954
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    .line 955
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    .line 956
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    .line 957
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    .line 958
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    .line 959
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 960
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    .line 961
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    .line 962
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    .line 963
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    .line 964
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    .line 965
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    .line 966
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    .line 967
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    .line 968
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    .line 969
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    .line 970
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    .line 971
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    .line 972
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    .line 973
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    .line 974
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    .line 975
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    .line 976
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    .line 977
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    .line 978
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    .line 979
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    .line 980
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    .line 981
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 982
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    .line 983
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    .line 984
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    .line 985
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    .line 986
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    .line 987
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    .line 988
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    .line 989
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    .line 990
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    .line 991
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    .line 992
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    .line 993
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    .line 994
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    .line 995
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    .line 996
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    .line 997
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    .line 998
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    .line 999
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    .line 1000
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    .line 1001
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    .line 1002
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    .line 1003
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    .line 1004
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    .line 1005
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    .line 1006
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1011
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3a

    .line 1013
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalValue;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1014
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1015
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1016
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1017
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1018
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1019
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1020
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1021
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1022
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1023
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1024
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1025
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1026
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1027
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1028
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1029
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1030
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1031
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1032
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1033
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1034
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1035
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1036
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/signal/EstimatedRevenue;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1037
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1038
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/signal/EstimatedUsageType;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1039
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1040
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1041
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1042
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1043
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1044
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1045
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1046
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1047
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1048
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1049
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1050
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1051
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1052
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1053
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1054
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1055
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1056
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1057
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1058
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/squareup/protos/checklist/signal/ActivationStatus;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1059
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1060
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1061
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1062
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1063
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1064
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1065
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1066
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1067
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1068
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2c

    :cond_2c
    const/4 v1, 0x0

    :goto_2c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1069
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2d

    :cond_2d
    const/4 v1, 0x0

    :goto_2d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1070
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2e

    :cond_2e
    const/4 v1, 0x0

    :goto_2e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1071
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2f

    :cond_2f
    const/4 v1, 0x0

    :goto_2f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1072
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    if-eqz v1, :cond_30

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_30

    :cond_30
    const/4 v1, 0x0

    :goto_30
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1073
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    if-eqz v1, :cond_31

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_31

    :cond_31
    const/4 v1, 0x0

    :goto_31
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1074
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    if-eqz v1, :cond_32

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_32

    :cond_32
    const/4 v1, 0x0

    :goto_32
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1075
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    if-eqz v1, :cond_33

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_33

    :cond_33
    const/4 v1, 0x0

    :goto_33
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1076
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    if-eqz v1, :cond_34

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_34

    :cond_34
    const/4 v1, 0x0

    :goto_34
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1077
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_35

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_35

    :cond_35
    const/4 v1, 0x0

    :goto_35
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1078
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_36

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_36

    :cond_36
    const/4 v1, 0x0

    :goto_36
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1079
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_37

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_37

    :cond_37
    const/4 v1, 0x0

    :goto_37
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1080
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    if-eqz v1, :cond_38

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_38

    :cond_38
    const/4 v1, 0x0

    :goto_38
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1081
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    if-eqz v1, :cond_39

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_39
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 1082
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1083
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/checklist/signal/SignalValue$Builder;
    .locals 2

    .line 858
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;-><init>()V

    .line 859
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_reports:Ljava/lang/Boolean;

    .line 860
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    .line 861
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_card_payment:Ljava/lang/Boolean;

    .line 862
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_an_invoice:Ljava/lang/Boolean;

    .line 863
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_an_item:Ljava/lang/Boolean;

    .line 864
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 865
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    .line 866
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_allowed_reactivation:Ljava/lang/Boolean;

    .line 867
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_a_device_usage:Ljava/lang/Boolean;

    .line 868
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_merchant_profile:Ljava/lang/Boolean;

    .line 869
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_stand_page:Ljava/lang/Boolean;

    .line 870
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_mobile_staff:Ljava/lang/Boolean;

    .line 871
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_more_next_steps:Ljava/lang/Boolean;

    .line 872
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item:Ljava/util/List;

    .line 873
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_card_payment_success_count:Ljava/lang/Integer;

    .line 874
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_published_item:Ljava/lang/Boolean;

    .line 875
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    .line 876
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    .line 877
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_referral_activated:Ljava/lang/Boolean;

    .line 878
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    .line 879
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_category:Ljava/lang/String;

    .line 880
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_business_sub_category:Ljava/lang/String;

    .line 881
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    .line 882
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    .line 883
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    .line 884
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_dismissed_action_item_time:Ljava/util/List;

    .line 885
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_sent_a_device_code:Ljava/lang/Boolean;

    .line 886
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_reviewed_business_details:Ljava/lang/Boolean;

    .line 887
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_a_location:Ljava/lang/Boolean;

    .line 888
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_managed_employees:Ljava/lang/Boolean;

    .line 889
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    .line 890
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_customized_receipts:Ljava/lang/Boolean;

    .line 891
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_other_services:Ljava/lang/Boolean;

    .line 892
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_engaged_customers:Ljava/lang/Boolean;

    .line 893
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_seller_community:Ljava/lang/Boolean;

    .line 894
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_visited_square_shop:Ljava/lang/Boolean;

    .line 895
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_recommended_products:Ljava/util/List;

    .line 896
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_subscribed_features:Ljava/util/List;

    .line 897
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_cardless_free_trial_features:Ljava/util/List;

    .line 898
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_product_interests:Ljava/util/List;

    .line 899
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_close_of_day:Ljava/lang/Boolean;

    .line 900
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    .line 901
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_has_paid_for_features:Ljava/util/List;

    .line 902
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state_history:Ljava/util/List;

    .line 903
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    .line 904
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saw_secure_profile:Ljava/lang/Boolean;

    .line 905
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    .line 906
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    .line 907
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_set_up_taxes:Ljava/lang/Boolean;

    .line 908
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    .line 909
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_completed_action_item:Ljava/util/List;

    .line 910
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_apps_with_device_code_usages:Ljava/util/List;

    .line 911
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_variant:Ljava/lang/String;

    .line 912
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_signup_egift_card:Ljava/lang/Boolean;

    .line 913
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_software_overview:Ljava/lang/Boolean;

    .line 914
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_updated_secure_profile:Ljava/lang/Boolean;

    .line 915
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_added_authorized_representative:Ljava/lang/Boolean;

    .line 916
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_discounts:Ljava/lang/Boolean;

    .line 917
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_vendors:Ljava/lang/Boolean;

    .line 918
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    .line 919
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    .line 920
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    .line 921
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    .line 922
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    .line 923
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    .line 924
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_imported_inventory:Ljava/lang/Boolean;

    .line 925
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_created_bar_code_label:Ljava/lang/Boolean;

    .line 926
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    .line 927
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->ext_next_step:Ljava/util/List;

    .line 928
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/checklist/signal/SignalValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/signal/SignalValue;->newBuilder()Lcom/squareup/protos/checklist/signal/SignalValue$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1090
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1091
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", ext_viewed_reports="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_reports:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1092
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", ext_took_a_cash_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cash_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1093
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", ext_took_a_card_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_card_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1094
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", ext_sent_an_invoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_an_invoice:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1095
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", ext_created_an_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_an_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1096
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    if-eqz v1, :cond_5

    const-string v1, ", ext_bank_account_link_progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_bank_account_link_progress:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1097
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", ext_took_a_cnp_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_took_a_cnp_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1098
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", ext_allowed_reactivation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_allowed_reactivation:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1099
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", ext_has_a_device_usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_a_device_usage:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1100
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", ext_published_merchant_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_merchant_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1101
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", ext_viewed_stand_page="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_stand_page:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1102
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", ext_added_mobile_staff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_mobile_staff:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1103
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", ext_added_more_next_steps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_more_next_steps:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1104
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", ext_dismissed_action_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1105
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    const-string v1, ", ext_card_payment_success_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_card_payment_success_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1106
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", ext_published_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_published_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1107
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", ext_enabled_item_for_sale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_item_for_sale:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1108
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", ext_viewed_business_fulfillment_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_business_fulfillment_options:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1109
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", ext_referral_activated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_referral_activated:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1110
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", ext_enabled_two_factor_auth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enabled_two_factor_auth:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1111
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    if-eqz v1, :cond_14

    const-string v1, ", ext_signup_business_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1112
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", ext_signup_business_sub_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_business_sub_category:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1113
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    if-eqz v1, :cond_16

    const-string v1, ", ext_signup_estimated_revenue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_revenue:Lcom/squareup/protos/checklist/signal/EstimatedRevenue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1114
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    if-eqz v1, :cond_17

    const-string v1, ", ext_signup_estimated_employees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_employees:Lcom/squareup/protos/checklist/signal/EstimatedEmployeeCount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1115
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    if-eqz v1, :cond_18

    const-string v1, ", ext_signup_estimated_usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_estimated_usage:Lcom/squareup/protos/checklist/signal/EstimatedUsageType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1116
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    const-string v1, ", ext_dismissed_action_item_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_dismissed_action_item_time:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1117
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", ext_sent_a_device_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_sent_a_device_code:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1118
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", ext_reviewed_business_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_reviewed_business_details:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1119
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", ext_added_a_location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_a_location:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1120
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    const-string v1, ", ext_managed_employees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_managed_employees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1121
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", ext_viewed_customer_feedback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_customer_feedback:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1122
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", ext_customized_receipts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_customized_receipts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1123
    :cond_1f
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    const-string v1, ", ext_viewed_other_services="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_other_services:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1124
    :cond_20
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    const-string v1, ", ext_engaged_customers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_engaged_customers:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1125
    :cond_21
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", ext_visited_seller_community="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_seller_community:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1126
    :cond_22
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", ext_visited_square_shop="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_visited_square_shop:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1127
    :cond_23
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_24

    const-string v1, ", ext_recommended_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_recommended_products:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1128
    :cond_24
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_25

    const-string v1, ", ext_subscribed_features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_subscribed_features:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1129
    :cond_25
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_26

    const-string v1, ", ext_cardless_free_trial_features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_cardless_free_trial_features:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1130
    :cond_26
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_27

    const-string v1, ", ext_completed_product_interests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_product_interests:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1131
    :cond_27
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    const-string v1, ", ext_set_close_of_day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_close_of_day:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1132
    :cond_28
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    const-string v1, ", ext_has_retail_app_usage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_retail_app_usage:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1133
    :cond_29
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2a

    const-string v1, ", ext_has_paid_for_features="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_has_paid_for_features:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1134
    :cond_2a
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2b

    const-string v1, ", ext_register_activation_state_history="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state_history:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1135
    :cond_2b
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    if-eqz v1, :cond_2c

    const-string v1, ", ext_register_activation_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_register_activation_state:Lcom/squareup/protos/checklist/signal/ActivationStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1136
    :cond_2c
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    const-string v1, ", ext_saw_secure_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saw_secure_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1137
    :cond_2d
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    const-string v1, ", ext_set_up_an_rst_grid_layout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_an_rst_grid_layout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1138
    :cond_2e
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    if-eqz v1, :cond_2f

    const-string v1, ", ext_set_up_a_floor_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_a_floor_plan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1139
    :cond_2f
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    if-eqz v1, :cond_30

    const-string v1, ", ext_set_up_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_set_up_taxes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1140
    :cond_30
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_31

    const-string v1, ", ext_saved_a_menu_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_saved_a_menu_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1141
    :cond_31
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_32

    const-string v1, ", ext_completed_action_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_completed_action_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1142
    :cond_32
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_33

    const-string v1, ", ext_apps_with_device_code_usages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_apps_with_device_code_usages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1143
    :cond_33
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    if-eqz v1, :cond_34

    const-string v1, ", ext_signup_variant="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_variant:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1144
    :cond_34
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_35

    const-string v1, ", ext_signup_egift_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_signup_egift_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1145
    :cond_35
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    if-eqz v1, :cond_36

    const-string v1, ", ext_viewed_software_overview="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_software_overview:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1146
    :cond_36
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_37

    const-string v1, ", ext_updated_secure_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_updated_secure_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1147
    :cond_37
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    if-eqz v1, :cond_38

    const-string v1, ", ext_added_authorized_representative="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_added_authorized_representative:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1148
    :cond_38
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_39

    const-string v1, ", ext_viewed_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1149
    :cond_39
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    if-eqz v1, :cond_3a

    const-string v1, ", ext_viewed_vendors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_vendors:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1150
    :cond_3a
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    if-eqz v1, :cond_3b

    const-string v1, ", ext_viewed_weebly_ost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_weebly_ost:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1151
    :cond_3b
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    if-eqz v1, :cond_3c

    const-string v1, ", ext_viewed_online_store_setup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_setup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1152
    :cond_3c
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    if-eqz v1, :cond_3d

    const-string v1, ", ext_viewed_online_store_selector="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_online_store_selector:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1153
    :cond_3d
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    if-eqz v1, :cond_3e

    const-string v1, ", ext_started_big_commerce_retail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_started_big_commerce_retail:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1154
    :cond_3e
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_3f

    const-string v1, ", ext_viewed_invoice_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_viewed_invoice_settings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1155
    :cond_3f
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_40

    const-string v1, ", ext_downloaded_the_invoices_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_downloaded_the_invoices_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1156
    :cond_40
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_41

    const-string v1, ", ext_imported_inventory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_imported_inventory:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1157
    :cond_41
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    if-eqz v1, :cond_42

    const-string v1, ", ext_created_bar_code_label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_created_bar_code_label:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1158
    :cond_42
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    if-eqz v1, :cond_43

    const-string v1, ", ext_enable_pickup_and_delivery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_enable_pickup_and_delivery:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1159
    :cond_43
    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_44

    const-string v1, ", ext_next_step="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/checklist/signal/SignalValue;->ext_next_step:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_44
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SignalValue{"

    .line 1160
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
