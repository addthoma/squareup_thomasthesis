.class public final enum Lcom/squareup/protos/checklist/common/ActionItemName;
.super Ljava/lang/Enum;
.source "ActionItemName.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/common/ActionItemName$ProtoAdapter_ActionItemName;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ACTIVATE_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADD_A_LOCATION:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum BUY_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum BUY_POS_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum COMPLETE_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CONNECT_OTHER_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CONNECT_WEEBLY:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CREATE_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CREATE_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CUSTOMIZE_HOMEPAGE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CUSTOMIZE_INVOICE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum CUSTOMIZE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum DOWNLOAD_THE_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum DOWNLOAD_THE_INVOICES_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum DOWNLOAD_THE_RETAIL_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum DOWNLOAD_THE_RST_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ENGAGE_CUSTOMERS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ENTER_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ENTER_UK_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum FINISH_ACTIVATING_PROVISIONAL:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum IMPORT_INVENTORY:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum INELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum INTEGRATE_ECOMMERCE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum INTEGRATE_ECOMMERCE_INTERESTED:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum MANAGE_EMPLOYEES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum ORDER_FREE_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum PAIR_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum REVIEW_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SEE_SECURE_PROFILE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SELL_WITH_SQUARE_FOR_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SEND_AN_INVOICE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_ITEM_COSTS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_DISCOUNTS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_ECOMMERCE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_FLOOR_PLAN:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_ITEMS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_ONLINE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_RETAIL_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_TAXES:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SET_UP_VENDORS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SIGN_UP_EGIFT_CARD:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum SUBSCRIBE_TO_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum TAKE_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum TAKE_FIRST_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum TAKE_FIRST_RETAIL_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum TAKE_FIRST_X2_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum TRACK_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum UPDATE_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum VIEW_BOOKING_SITE:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum VIEW_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum VIEW_SALES_REPORTS:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum VIEW_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/common/ActionItemName;

.field public static final enum VISIT_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/common/ActionItemName;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 13
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v2, 0x1

    const-string v3, "FINISH_ACTIVATING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 15
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v3, 0x2

    const-string v4, "DOWNLOAD_THE_APP"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 17
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v4, 0x3

    const-string v5, "TAKE_FIRST_PAYMENT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 19
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v5, 0x4

    const-string v6, "TAKE_A_CASH_PAYMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v6, 0x5

    const-string v7, "BUY_HARDWARE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v7, 0x6

    const-string v8, "LINK_YOUR_BANK_ACCOUNT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/4 v8, 0x7

    const-string v9, "SET_UP_ITEMS"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ITEMS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 27
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v9, 0x8

    const-string v10, "SET_UP_DEVICES"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v10, 0x9

    const-string v11, "UPDATE_BUSINESS_DETAILS"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->UPDATE_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 31
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v11, 0xa

    const-string v12, "ADD_A_LOCATION"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ADD_A_LOCATION:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 33
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v12, 0xb

    const-string v13, "MANAGE_EMPLOYEES"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->MANAGE_EMPLOYEES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 35
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v13, 0xc

    const-string v14, "CUSTOMIZE_RECEIPTS"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 37
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v14, 0xd

    const-string v15, "TRACK_CUSTOMER_FEEDBACK"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->TRACK_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 39
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v15, 0xe

    const-string v14, "ENGAGE_CUSTOMERS"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENGAGE_CUSTOMERS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 41
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v14, "CONNECT_OTHER_SERVICES"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_OTHER_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 43
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "VIEW_SALES_REPORTS"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SALES_REPORTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 45
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "CUSTOMIZE_HOMEPAGE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_HOMEPAGE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 47
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ACTIVATE_TWO_FACTOR_AUTH"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ACTIVATE_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 49
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SEE_SECURE_PROFILE"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SEE_SECURE_PROFILE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 51
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "VISIT_SELLER_COMMUNITY"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->VISIT_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 53
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_CLOSE_OF_DAY"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 55
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_ITEM_COSTS"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_ITEM_COSTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 57
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SUBSCRIBE_TO_RETAIL"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SUBSCRIBE_TO_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 59
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "DOWNLOAD_THE_RETAIL_APP"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RETAIL_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 61
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ELIGIBLE_FOR_1099K_TAX_FORM_2017"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 63
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "INELIGIBLE_FOR_1099K_TAX_FORM_2017"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->INELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 65
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "COMPLETE_1099K_TAX_FORM_2017"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->COMPLETE_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 67
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ENTER_UK_DIRECTOR_INFO"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_UK_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 69
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "INTEGRATE_ECOMMERCE"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 71
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "INTEGRATE_ECOMMERCE_INTERESTED"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE_INTERESTED:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 73
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SAVED_A_MENU_ITEM"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 75
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "DOWNLOAD_THE_RST_APP"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RST_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 77
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_RST_GRID_LAYOUT"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 79
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_FLOOR_PLAN"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_FLOOR_PLAN:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 81
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_TAXES"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 83
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "TAKE_FIRST_RETAIL_PAYMENT"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_RETAIL_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 85
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_RETAIL_DEVICES"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RETAIL_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 87
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "TAKE_FIRST_X2_PAYMENT"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_X2_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 89
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "BUY_POS_HARDWARE"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_POS_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 91
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SIGN_UP_EGIFT_CARD"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SIGN_UP_EGIFT_CARD:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 93
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "VIEW_SOFTWARE_OVERVIEW"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 95
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "REVIEW_SETTINGS"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->REVIEW_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 97
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_DISCOUNTS"

    const/16 v14, 0x2b

    const/16 v15, 0x2b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DISCOUNTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 99
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_VENDORS"

    const/16 v14, 0x2c

    const/16 v15, 0x2c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_VENDORS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 101
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "CONNECT_WEEBLY"

    const/16 v14, 0x2d

    const/16 v15, 0x2d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_WEEBLY:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 103
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_ONLINE_STORE"

    const/16 v14, 0x2e

    const/16 v15, 0x2e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ONLINE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 105
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SET_UP_ECOMMERCE_STORE"

    const/16 v14, 0x2f

    const/16 v15, 0x2f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ECOMMERCE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 107
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "FINISH_ACTIVATING_PROVISIONAL"

    const/16 v14, 0x30

    const/16 v15, 0x30

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING_PROVISIONAL:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 109
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "CUSTOMIZE_INVOICE_RECEIPTS"

    const/16 v14, 0x31

    const/16 v15, 0x31

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_INVOICE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 111
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SEND_AN_INVOICE"

    const/16 v14, 0x32

    const/16 v15, 0x32

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SEND_AN_INVOICE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 113
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "VIEW_INVOICE_SETTINGS"

    const/16 v14, 0x33

    const/16 v15, 0x33

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 115
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "DOWNLOAD_THE_INVOICES_APP"

    const/16 v14, 0x34

    const/16 v15, 0x34

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_INVOICES_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 117
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "IMPORT_INVENTORY"

    const/16 v14, 0x35

    const/16 v15, 0x35

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->IMPORT_INVENTORY:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 119
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "SELL_WITH_SQUARE_FOR_RETAIL"

    const/16 v14, 0x36

    const/16 v15, 0x36

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->SELL_WITH_SQUARE_FOR_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 121
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "CREATE_BAR_CODE_LABEL"

    const/16 v14, 0x37

    const/16 v15, 0x37

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 123
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "VIEW_BOOKING_SITE"

    const/16 v14, 0x38

    const/16 v15, 0x38

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_BOOKING_SITE:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 125
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ORDER_FREE_READER"

    const/16 v14, 0x39

    const/16 v15, 0x39

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ORDER_FREE_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 127
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "CREATE_SERVICES"

    const/16 v14, 0x3a

    const/16 v15, 0x3a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 129
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "PAIR_READER"

    const/16 v14, 0x3b

    const/16 v15, 0x3b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->PAIR_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 131
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ENABLE_PICKUP_AND_DELIVERY"

    const/16 v14, 0x3c

    const/16 v15, 0x3c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 133
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    const-string v13, "ENTER_DIRECTOR_INFO"

    const/16 v14, 0x3d

    const/16 v15, 0x3d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/checklist/common/ActionItemName;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v0, 0x3e

    new-array v0, v0, [Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 10
    sget-object v13, Lcom/squareup/protos/checklist/common/ActionItemName;->UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ITEMS:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->UPDATE_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ADD_A_LOCATION:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->MANAGE_EMPLOYEES:Lcom/squareup/protos/checklist/common/ActionItemName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->TRACK_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ENGAGE_CUSTOMERS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_OTHER_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SALES_REPORTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_HOMEPAGE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ACTIVATE_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SEE_SECURE_PROFILE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->VISIT_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_ITEM_COSTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SUBSCRIBE_TO_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RETAIL_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->INELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->COMPLETE_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_UK_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE_INTERESTED:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RST_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_FLOOR_PLAN:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_RETAIL_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RETAIL_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_X2_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_POS_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SIGN_UP_EGIFT_CARD:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->REVIEW_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DISCOUNTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_VENDORS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_WEEBLY:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ONLINE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ECOMMERCE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING_PROVISIONAL:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_INVOICE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SEND_AN_INVOICE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_INVOICES_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->IMPORT_INVENTORY:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->SELL_WITH_SQUARE_FOR_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_BOOKING_SITE:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ORDER_FREE_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->PAIR_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->$VALUES:[Lcom/squareup/protos/checklist/common/ActionItemName;

    .line 135
    new-instance v0, Lcom/squareup/protos/checklist/common/ActionItemName$ProtoAdapter_ActionItemName;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/common/ActionItemName$ProtoAdapter_ActionItemName;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    iput p3, p0, Lcom/squareup/protos/checklist/common/ActionItemName;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/common/ActionItemName;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 209
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 208
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENABLE_PICKUP_AND_DELIVERY:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 207
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->PAIR_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 206
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 205
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ORDER_FREE_READER:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 204
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_BOOKING_SITE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 203
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CREATE_BAR_CODE_LABEL:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 202
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SELL_WITH_SQUARE_FOR_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 201
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->IMPORT_INVENTORY:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 200
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_INVOICES_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 199
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_INVOICE_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 198
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SEND_AN_INVOICE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 197
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_INVOICE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 196
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING_PROVISIONAL:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 195
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ECOMMERCE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 194
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ONLINE_STORE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 193
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_WEEBLY:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 192
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_VENDORS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 191
    :pswitch_12
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DISCOUNTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 190
    :pswitch_13
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->REVIEW_SETTINGS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 189
    :pswitch_14
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SOFTWARE_OVERVIEW:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 188
    :pswitch_15
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SIGN_UP_EGIFT_CARD:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 187
    :pswitch_16
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_POS_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 186
    :pswitch_17
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_X2_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 185
    :pswitch_18
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RETAIL_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 184
    :pswitch_19
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_RETAIL_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 183
    :pswitch_1a
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_TAXES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 182
    :pswitch_1b
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_FLOOR_PLAN:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 181
    :pswitch_1c
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_RST_GRID_LAYOUT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 180
    :pswitch_1d
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RST_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 179
    :pswitch_1e
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SAVED_A_MENU_ITEM:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 178
    :pswitch_1f
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE_INTERESTED:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 177
    :pswitch_20
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->INTEGRATE_ECOMMERCE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 176
    :pswitch_21
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENTER_UK_DIRECTOR_INFO:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 175
    :pswitch_22
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->COMPLETE_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 174
    :pswitch_23
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->INELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 173
    :pswitch_24
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ELIGIBLE_FOR_1099K_TAX_FORM_2017:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 172
    :pswitch_25
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_RETAIL_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 171
    :pswitch_26
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SUBSCRIBE_TO_RETAIL:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 170
    :pswitch_27
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_ITEM_COSTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 169
    :pswitch_28
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_CLOSE_OF_DAY:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 168
    :pswitch_29
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->VISIT_SELLER_COMMUNITY:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 167
    :pswitch_2a
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SEE_SECURE_PROFILE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 166
    :pswitch_2b
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ACTIVATE_TWO_FACTOR_AUTH:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 165
    :pswitch_2c
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_HOMEPAGE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 164
    :pswitch_2d
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->VIEW_SALES_REPORTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 163
    :pswitch_2e
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CONNECT_OTHER_SERVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 162
    :pswitch_2f
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ENGAGE_CUSTOMERS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 161
    :pswitch_30
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->TRACK_CUSTOMER_FEEDBACK:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 160
    :pswitch_31
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->CUSTOMIZE_RECEIPTS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 159
    :pswitch_32
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->MANAGE_EMPLOYEES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 158
    :pswitch_33
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->ADD_A_LOCATION:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 157
    :pswitch_34
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->UPDATE_BUSINESS_DETAILS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 156
    :pswitch_35
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_DEVICES:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 155
    :pswitch_36
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->SET_UP_ITEMS:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 154
    :pswitch_37
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->LINK_YOUR_BANK_ACCOUNT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 153
    :pswitch_38
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->BUY_HARDWARE:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 152
    :pswitch_39
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_A_CASH_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 151
    :pswitch_3a
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->TAKE_FIRST_PAYMENT:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 150
    :pswitch_3b
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->DOWNLOAD_THE_APP:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 149
    :pswitch_3c
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->FINISH_ACTIVATING:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    .line 148
    :pswitch_3d
    sget-object p0, Lcom/squareup/protos/checklist/common/ActionItemName;->UNKNOWN:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3d
        :pswitch_3c
        :pswitch_3b
        :pswitch_3a
        :pswitch_39
        :pswitch_38
        :pswitch_37
        :pswitch_36
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/common/ActionItemName;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/common/ActionItemName;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/checklist/common/ActionItemName;->$VALUES:[Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/common/ActionItemName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/squareup/protos/checklist/common/ActionItemName;->value:I

    return v0
.end method
