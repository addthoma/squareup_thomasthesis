.class public final Lcom/squareup/protos/jedi/service/ComponentItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/ComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/ComponentItem;",
        "Lcom/squareup/protos/jedi/service/ComponentItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public show_other:Ljava/lang/String;

.field public text:Ljava/lang/String;

.field public type:Lcom/squareup/protos/jedi/service/ComponentItemType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/ComponentItem;
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/protos/jedi/service/ComponentItem;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->label:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->text:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->show_other:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->type:Lcom/squareup/protos/jedi/service/ComponentItemType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/ComponentItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/jedi/service/ComponentItemType;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->build()Lcom/squareup/protos/jedi/service/ComponentItem;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/ComponentItem$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->label:Ljava/lang/String;

    return-object p0
.end method

.method public show_other(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/ComponentItem$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->show_other:Ljava/lang/String;

    return-object p0
.end method

.method public text(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/ComponentItem$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->text:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/jedi/service/ComponentItemType;)Lcom/squareup/protos/jedi/service/ComponentItem$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/ComponentItem$Builder;->type:Lcom/squareup/protos/jedi/service/ComponentItemType;

    return-object p0
.end method
