.class public final Lcom/squareup/protos/jedi/service/SearchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/SearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/SearchResponse;",
        "Lcom/squareup/protos/jedi/service/SearchResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Ljava/lang/Integer;

.field public error_message:Ljava/lang/String;

.field public panel:Lcom/squareup/protos/jedi/service/Panel;

.field public success:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/jedi/service/SearchResponse;
    .locals 7

    .line 150
    new-instance v6, Lcom/squareup/protos/jedi/service/SearchResponse;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->success:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->error_message:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->code:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/jedi/service/SearchResponse;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->build()Lcom/squareup/protos/jedi/service/SearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public code(Ljava/lang/Integer;)Lcom/squareup/protos/jedi/service/SearchResponse$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->code:Ljava/lang/Integer;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/SearchResponse$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public panel(Lcom/squareup/protos/jedi/service/Panel;)Lcom/squareup/protos/jedi/service/SearchResponse$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->panel:Lcom/squareup/protos/jedi/service/Panel;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/protos/jedi/service/SearchResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/SearchResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method
