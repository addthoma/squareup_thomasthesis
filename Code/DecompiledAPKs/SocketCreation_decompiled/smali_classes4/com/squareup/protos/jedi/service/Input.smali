.class public final Lcom/squareup/protos/jedi/service/Input;
.super Lcom/squareup/wire/Message;
.source "Input.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/jedi/service/Input$ProtoAdapter_Input;,
        Lcom/squareup/protos/jedi/service/Input$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/jedi/service/Input;",
        "Lcom/squareup/protos/jedi/service/Input$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_KIND:Lcom/squareup/protos/jedi/service/InputKind;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final kind:Lcom/squareup/protos/jedi/service/InputKind;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.jedi.service.InputKind#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/jedi/service/Input$ProtoAdapter_Input;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/Input$ProtoAdapter_Input;-><init>()V

    sput-object v0, Lcom/squareup/protos/jedi/service/Input;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    sput-object v0, Lcom/squareup/protos/jedi/service/Input;->DEFAULT_KIND:Lcom/squareup/protos/jedi/service/InputKind;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/jedi/service/InputKind;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 49
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/jedi/service/Input;-><init>(Lcom/squareup/protos/jedi/service/InputKind;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/jedi/service/InputKind;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/protos/jedi/service/Input;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 54
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    .line 55
    iput-object p2, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 72
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/jedi/service/Input;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 73
    :cond_1
    check-cast p1, Lcom/squareup/protos/jedi/service/Input;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Input;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/Input;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    .line 75
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Input;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/InputKind;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 88
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/jedi/service/Input$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/jedi/service/Input$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/Input$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/Input$Builder;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/Input$Builder;->name:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/jedi/service/Input$Builder;->value:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Input;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/Input$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/Input;->newBuilder()Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    if-eqz v1, :cond_0

    const-string v1, ", kind="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->kind:Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/Input;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Input{"

    .line 99
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
