.class final Lcom/squareup/protos/jedi/service/ComponentKind$ProtoAdapter_ComponentKind;
.super Lcom/squareup/wire/EnumAdapter;
.source "ComponentKind.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/ComponentKind;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ComponentKind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/jedi/service/ComponentKind;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 183
    const-class v0, Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;
    .locals 0

    .line 188
    invoke-static {p1}, Lcom/squareup/protos/jedi/service/ComponentKind;->fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 181
    invoke-virtual {p0, p1}, Lcom/squareup/protos/jedi/service/ComponentKind$ProtoAdapter_ComponentKind;->fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object p1

    return-object p1
.end method
