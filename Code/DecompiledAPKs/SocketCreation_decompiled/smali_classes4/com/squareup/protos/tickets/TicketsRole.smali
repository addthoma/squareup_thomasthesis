.class public final enum Lcom/squareup/protos/tickets/TicketsRole;
.super Ljava/lang/Enum;
.source "TicketsRole.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/tickets/TicketsRole$ProtoAdapter_TicketsRole;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/tickets/TicketsRole;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/tickets/TicketsRole;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/tickets/TicketsRole;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CUSTOMER:Lcom/squareup/protos/tickets/TicketsRole;

.field public static final enum MERCHANT:Lcom/squareup/protos/tickets/TicketsRole;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 15
    new-instance v0, Lcom/squareup/protos/tickets/TicketsRole;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "MERCHANT"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/tickets/TicketsRole;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/tickets/TicketsRole;->MERCHANT:Lcom/squareup/protos/tickets/TicketsRole;

    .line 17
    new-instance v0, Lcom/squareup/protos/tickets/TicketsRole;

    const/4 v3, 0x2

    const-string v4, "CUSTOMER"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/tickets/TicketsRole;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/tickets/TicketsRole;->CUSTOMER:Lcom/squareup/protos/tickets/TicketsRole;

    new-array v0, v3, [Lcom/squareup/protos/tickets/TicketsRole;

    .line 14
    sget-object v3, Lcom/squareup/protos/tickets/TicketsRole;->MERCHANT:Lcom/squareup/protos/tickets/TicketsRole;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/tickets/TicketsRole;->CUSTOMER:Lcom/squareup/protos/tickets/TicketsRole;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/tickets/TicketsRole;->$VALUES:[Lcom/squareup/protos/tickets/TicketsRole;

    .line 19
    new-instance v0, Lcom/squareup/protos/tickets/TicketsRole$ProtoAdapter_TicketsRole;

    invoke-direct {v0}, Lcom/squareup/protos/tickets/TicketsRole$ProtoAdapter_TicketsRole;-><init>()V

    sput-object v0, Lcom/squareup/protos/tickets/TicketsRole;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/squareup/protos/tickets/TicketsRole;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/tickets/TicketsRole;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 33
    :cond_0
    sget-object p0, Lcom/squareup/protos/tickets/TicketsRole;->CUSTOMER:Lcom/squareup/protos/tickets/TicketsRole;

    return-object p0

    .line 32
    :cond_1
    sget-object p0, Lcom/squareup/protos/tickets/TicketsRole;->MERCHANT:Lcom/squareup/protos/tickets/TicketsRole;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/tickets/TicketsRole;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/protos/tickets/TicketsRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/tickets/TicketsRole;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/tickets/TicketsRole;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/protos/tickets/TicketsRole;->$VALUES:[Lcom/squareup/protos/tickets/TicketsRole;

    invoke-virtual {v0}, [Lcom/squareup/protos/tickets/TicketsRole;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/tickets/TicketsRole;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/squareup/protos/tickets/TicketsRole;->value:I

    return v0
.end method
