.class public final Lcom/squareup/protos/common/client/Product;
.super Lcom/squareup/wire/Message;
.source "Product.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;,
        Lcom/squareup/protos/common/client/Product$BuildType;,
        Lcom/squareup/protos/common/client/Product$Application;,
        Lcom/squareup/protos/common/client/Product$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/client/Product;",
        "Lcom/squareup/protos/common/client/Product$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/client/Product;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION:Lcom/squareup/protos/common/client/Product$Application;

.field public static final DEFAULT_BUILD_TYPE:Lcom/squareup/protos/common/client/Product$BuildType;

.field public static final DEFAULT_DEVICE_ADVERTISING_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICE_INSTALLATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICE_TRACKING_ENABLED:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application:Lcom/squareup/protos/common/client/Product$Application;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Product$Application#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final build_type:Lcom/squareup/protos/common/client/Product$BuildType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Product$BuildType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final device_advertising_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final device_installation_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final device_tracking_enabled:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final version:Lcom/squareup/protos/common/client/Version;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.client.Version#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Product$ProtoAdapter_Product;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/client/Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/common/client/Product$Application;->REGISTER:Lcom/squareup/protos/common/client/Product$Application;

    sput-object v0, Lcom/squareup/protos/common/client/Product;->DEFAULT_APPLICATION:Lcom/squareup/protos/common/client/Product$Application;

    .line 31
    sget-object v0, Lcom/squareup/protos/common/client/Product$BuildType;->RELEASE:Lcom/squareup/protos/common/client/Product$BuildType;

    sput-object v0, Lcom/squareup/protos/common/client/Product;->DEFAULT_BUILD_TYPE:Lcom/squareup/protos/common/client/Product$BuildType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/client/Product$Application;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Product$BuildType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 77
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/common/client/Product;-><init>(Lcom/squareup/protos/common/client/Product$Application;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Product$BuildType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/client/Product$Application;Lcom/squareup/protos/common/client/Version;Lcom/squareup/protos/common/client/Product$BuildType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/protos/common/client/Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    .line 85
    iput-object p2, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    .line 86
    iput-object p3, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 87
    iput-object p4, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    .line 88
    iput-object p5, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    .line 89
    iput-object p6, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/client/Product;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/client/Product;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    .line 116
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 121
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Product$Application;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Version;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/client/Product$BuildType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 130
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/client/Product$Builder;
    .locals 2

    .line 94
    new-instance v0, Lcom/squareup/protos/common/client/Product$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/client/Product$Builder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->application:Lcom/squareup/protos/common/client/Product$Application;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->version:Lcom/squareup/protos/common/client/Version;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->device_advertising_id:Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->device_tracking_enabled:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/common/client/Product$Builder;->device_installation_id:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Product;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/client/Product$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Product;->newBuilder()Lcom/squareup/protos/common/client/Product$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    if-eqz v1, :cond_0

    const-string v1, ", application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->application:Lcom/squareup/protos/common/client/Product$Application;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    if-eqz v1, :cond_1

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->version:Lcom/squareup/protos/common/client/Version;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    if-eqz v1, :cond_2

    const-string v1, ", build_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->build_type:Lcom/squareup/protos/common/client/Product$BuildType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", device_advertising_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_advertising_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", device_tracking_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_tracking_enabled:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", device_installation_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Product;->device_installation_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Product{"

    .line 144
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
