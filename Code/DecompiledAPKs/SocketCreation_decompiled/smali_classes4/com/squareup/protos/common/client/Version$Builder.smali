.class public final Lcom/squareup/protos/common/client/Version$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Version.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/client/Version;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/client/Version;",
        "Lcom/squareup/protos/common/client/Version$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public build_id:Ljava/lang/String;

.field public major:Ljava/lang/Integer;

.field public minor:Ljava/lang/Integer;

.field public prerelease:Ljava/lang/Integer;

.field public prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

.field public revision:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/client/Version;
    .locals 9

    .line 210
    new-instance v8, Lcom/squareup/protos/common/client/Version;

    iget-object v1, p0, Lcom/squareup/protos/common/client/Version$Builder;->major:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/common/client/Version$Builder;->minor:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/common/client/Version$Builder;->revision:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    iget-object v5, p0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/common/client/Version$Builder;->build_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/common/client/Version;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/common/client/Version$PrereleaseType;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/common/client/Version$Builder;->build()Lcom/squareup/protos/common/client/Version;

    move-result-object v0

    return-object v0
.end method

.method public build_id(Ljava/lang/String;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->build_id:Ljava/lang/String;

    return-object p0
.end method

.method public major(Ljava/lang/Integer;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->major:Ljava/lang/Integer;

    return-object p0
.end method

.method public minor(Ljava/lang/Integer;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->minor:Ljava/lang/Integer;

    return-object p0
.end method

.method public prerelease(Ljava/lang/Integer;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease:Ljava/lang/Integer;

    return-object p0
.end method

.method public prerelease_type(Lcom/squareup/protos/common/client/Version$PrereleaseType;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->prerelease_type:Lcom/squareup/protos/common/client/Version$PrereleaseType;

    return-object p0
.end method

.method public revision(Ljava/lang/Integer;)Lcom/squareup/protos/common/client/Version$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/common/client/Version$Builder;->revision:Ljava/lang/Integer;

    return-object p0
.end method
