.class public final Lcom/squareup/protos/common/validation/Range$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Range.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/validation/Range;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/validation/Range;",
        "Lcom/squareup/protos/common/validation/Range$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public max:Ljava/lang/Double;

.field public min:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/validation/Range;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/common/validation/Range;

    iget-object v1, p0, Lcom/squareup/protos/common/validation/Range$Builder;->min:Ljava/lang/Double;

    iget-object v2, p0, Lcom/squareup/protos/common/validation/Range$Builder;->max:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/validation/Range;-><init>(Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/common/validation/Range$Builder;->build()Lcom/squareup/protos/common/validation/Range;

    move-result-object v0

    return-object v0
.end method

.method public max(Ljava/lang/Double;)Lcom/squareup/protos/common/validation/Range$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/common/validation/Range$Builder;->max:Ljava/lang/Double;

    return-object p0
.end method

.method public min(Ljava/lang/Double;)Lcom/squareup/protos/common/validation/Range$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/common/validation/Range$Builder;->min:Ljava/lang/Double;

    return-object p0
.end method
