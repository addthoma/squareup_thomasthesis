.class public final Lcom/squareup/protos/common/tipping/TippingPreferences;
.super Lcom/squareup/wire/Message;
.source "TippingPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;,
        Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/tipping/TippingPreferences;",
        "Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/tipping/TippingPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

.field public static final DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Ljava/lang/Double;

.field public static final DEFAULT_USE_SMART_TIPPING:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_manual_tip_entry:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final client_calculated_tip_option:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public final manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final manual_tip_entry_max_percentage:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        tag = 0x9
    .end annotation
.end field

.field public final manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final smart_tipping_over_threshold_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public final smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final smart_tipping_under_threshold_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public final tipping_options:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.tipping.TipOption#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;"
        }
    .end annotation
.end field

.field public final use_smart_tipping:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TippingPreferences$ProtoAdapter_TippingPreferences;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->DEFAULT_USE_SMART_TIPPING:Ljava/lang/Boolean;

    .line 40
    sput-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->DEFAULT_ALLOW_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 42
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->DEFAULT_MANUAL_TIP_ENTRY_MAX_PERCENTAGE:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Double;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;)V"
        }
    .end annotation

    .line 163
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/common/tipping/TippingPreferences;-><init>(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/Double;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/Double;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/tipping/TipOption;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 172
    sget-object v0, Lcom/squareup/protos/common/tipping/TippingPreferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    .line 174
    iput-object p2, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    const-string p1, "smart_tipping_under_threshold_options"

    .line 175
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    const-string p1, "smart_tipping_over_threshold_options"

    .line 176
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    const-string p1, "tipping_options"

    .line 177
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    .line 178
    iput-object p6, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 179
    iput-object p7, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 180
    iput-object p8, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 181
    iput-object p9, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    const-string p1, "client_calculated_tip_option"

    .line 182
    invoke-static {p1, p10}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 205
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 206
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/tipping/TippingPreferences;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 210
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 211
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    .line 212
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    iget-object v3, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    .line 217
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 222
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;
    .locals 2

    .line 187
    new-instance v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->use_smart_tipping:Ljava/lang/Boolean;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_under_threshold_options:Ljava/util/List;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->smart_tipping_over_threshold_options:Ljava/util/List;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->tipping_options:Ljava/util/List;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->allow_manual_tip_entry:Ljava/lang/Boolean;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->client_calculated_tip_option:Ljava/util/List;

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TippingPreferences;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TippingPreferences;->newBuilder()Lcom/squareup/protos/common/tipping/TippingPreferences$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_smart_tipping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->use_smart_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", smart_tipping_threshold_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_threshold_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", smart_tipping_under_threshold_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_under_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", smart_tipping_over_threshold_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->smart_tipping_over_threshold_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", tipping_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->tipping_options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 248
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", allow_manual_tip_entry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->allow_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", manual_tip_entry_smallest_max_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_smallest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", manual_tip_entry_largest_max_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_largest_max_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    if-eqz v1, :cond_8

    const-string v1, ", manual_tip_entry_max_percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->manual_tip_entry_max_percentage:Ljava/lang/Double;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", client_calculated_tip_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/tipping/TippingPreferences;->client_calculated_tip_option:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TippingPreferences{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
