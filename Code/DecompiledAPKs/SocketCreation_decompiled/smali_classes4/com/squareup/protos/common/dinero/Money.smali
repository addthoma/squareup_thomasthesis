.class public final Lcom/squareup/protos/common/dinero/Money;
.super Lcom/squareup/wire/Message;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/dinero/Money$ProtoAdapter_Money;,
        Lcom/squareup/protos/common/dinero/Money$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/common/dinero/Money;",
        "Lcom/squareup/protos/common/dinero/Money$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/dinero/Money;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CENTS:Ljava/lang/Long;

.field public static final DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/dinero/CurrencyCode;

.field public static final DEFAULT_OBSOLETE_CURRENCY_CODE:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

.field private static final serialVersionUID:J


# instance fields
.field public final OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.OBSOLETE_CurrencyCode#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final cents:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.CurrencyCode#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/common/dinero/Money$ProtoAdapter_Money;

    invoke-direct {v0}, Lcom/squareup/protos/common/dinero/Money$ProtoAdapter_Money;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 32
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/common/dinero/Money;->DEFAULT_CENTS:Ljava/lang/Long;

    .line 34
    sget-object v0, Lcom/squareup/protos/common/dinero/CurrencyCode;->USD:Lcom/squareup/protos/common/dinero/CurrencyCode;

    sput-object v0, Lcom/squareup/protos/common/dinero/Money;->DEFAULT_CURRENCY_CODE:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 36
    sget-object v0, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->USD:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    sput-object v0, Lcom/squareup/protos/common/dinero/Money;->DEFAULT_OBSOLETE_CURRENCY_CODE:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/dinero/CurrencyCode;Lcom/squareup/api/items/OBSOLETE_CurrencyCode;)V
    .locals 1

    .line 67
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/common/dinero/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/dinero/CurrencyCode;Lcom/squareup/api/items/OBSOLETE_CurrencyCode;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/dinero/CurrencyCode;Lcom/squareup/api/items/OBSOLETE_CurrencyCode;Lokio/ByteString;)V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/protos/common/dinero/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/common/dinero/Money;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/protos/common/dinero/Money;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/common/dinero/Money;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    iget-object v3, p1, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    iget-object p1, p1, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    .line 96
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 101
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/CurrencyCode;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/OBSOLETE_CurrencyCode;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 107
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/common/dinero/Money$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/common/dinero/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/dinero/Money$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/common/dinero/Money$Builder;->cents:Ljava/lang/Long;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    iput-object v1, v0, Lcom/squareup/protos/common/dinero/Money$Builder;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    iput-object v1, v0, Lcom/squareup/protos/common/dinero/Money$Builder;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/dinero/Money$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money;->newBuilder()Lcom/squareup/protos/common/dinero/Money$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", cents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    if-eqz v1, :cond_1

    const-string v1, ", currency_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    if-eqz v1, :cond_2

    const-string v1, ", OBSOLETE_currency_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Money{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
