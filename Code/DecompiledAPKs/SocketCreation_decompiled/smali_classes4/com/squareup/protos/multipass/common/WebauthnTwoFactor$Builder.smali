.class public final Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "WebauthnTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;",
        "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public challenge:Lokio/ByteString;

.field public credential_id:Lokio/ByteString;

.field public encrypted_data:Lokio/ByteString;

.field public person_token:Ljava/lang/String;

.field public public_key_credential:Ljava/lang/String;

.field public signature:Lokio/ByteString;

.field public signature_count:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 203
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
    .locals 10

    .line 266
    new-instance v9, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    iget-object v1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->credential_id:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature_count:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->encrypted_data:Lokio/ByteString;

    iget-object v5, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->public_key_credential:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->challenge:Lokio/ByteString;

    iget-object v7, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->person_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;-><init>(Lokio/ByteString;Ljava/lang/Long;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    move-result-object v0

    return-object v0
.end method

.method public challenge(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->challenge:Lokio/ByteString;

    return-object p0
.end method

.method public credential_id(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->credential_id:Lokio/ByteString;

    return-object p0
.end method

.method public encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->encrypted_data:Lokio/ByteString;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public public_key_credential(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 244
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->public_key_credential:Ljava/lang/String;

    return-object p0
.end method

.method public signature(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature:Lokio/ByteString;

    return-object p0
.end method

.method public signature_count(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature_count:Ljava/lang/Long;

    return-object p0
.end method
