.class final Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "TwoFactorDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/TwoFactorDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TwoFactorDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 224
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 251
    new-instance v0, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;-><init>()V

    .line 252
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 253
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 262
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 260
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 259
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->created_at(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 258
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth(Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 257
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->set_trusted_device(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 256
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms(Lcom/squareup/protos/multipass/common/SmsTwoFactor;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 255
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    goto :goto_0

    .line 266
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 267
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 222
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 240
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 241
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 242
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 243
    sget-object v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 244
    sget-object v0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 245
    sget-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 246
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 222
    check-cast p2, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/TwoFactorDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)I
    .locals 4

    .line 229
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->set_trusted_device:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 230
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->created_at:Ljava/lang/Long;

    const/4 v3, 0x5

    .line 231
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    const/4 v3, 0x2

    .line 232
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    const/4 v3, 0x4

    .line 233
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    const/4 v3, 0x6

    .line 234
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 222
    check-cast p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;->encodedSize(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;
    .locals 2

    .line 272
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails;->newBuilder()Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;

    move-result-object p1

    .line 273
    iget-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    iput-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->sms:Lcom/squareup/protos/multipass/common/SmsTwoFactor;

    .line 274
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    iput-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->googleauth:Lcom/squareup/protos/multipass/common/GoogleauthTwoFactor;

    .line 275
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    iput-object v0, p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->webauthn:Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    .line 276
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$Builder;->build()Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 222
    check-cast p1, Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/TwoFactorDetails$ProtoAdapter_TwoFactorDetails;->redact(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    move-result-object p1

    return-object p1
.end method
