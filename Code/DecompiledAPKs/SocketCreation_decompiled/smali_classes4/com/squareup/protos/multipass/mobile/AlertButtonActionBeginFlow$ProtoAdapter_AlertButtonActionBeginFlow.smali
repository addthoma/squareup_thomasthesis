.class final Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AlertButtonActionBeginFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AlertButtonActionBeginFlow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 157
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;-><init>()V

    .line 175
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 176
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 187
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 180
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->flow(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 182
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 191
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 192
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 168
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 169
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 155
    check-cast p2, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)I
    .locals 3

    .line 162
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Flow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 163
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;->encodedSize(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
    .locals 0

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;

    move-result-object p1

    .line 198
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 199
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$Builder;->build()Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow$ProtoAdapter_AlertButtonActionBeginFlow;->redact(Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    move-result-object p1

    return-object p1
.end method
