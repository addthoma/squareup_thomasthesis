.class public final Lcom/squareup/protos/deposits/BalanceActivityFailure;
.super Lcom/squareup/wire/Message;
.source "BalanceActivityFailure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;,
        Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/deposits/BalanceActivityFailure;",
        "Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/deposits/BalanceActivityFailure;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ELIGIBILITY_BLOCKER:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

.field public static final DEFAULT_MONEY_MOVING_BLOCKER:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

.field private static final serialVersionUID:J


# instance fields
.field public final eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.ledger.service.EligibilityBlocker#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.deposits.BalanceActivityFailureMetadata#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.ledger.service.MoneyMovingBlocker#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;-><init>()V

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/ledger/service/EligibilityBlocker;->UNKNOWN:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->DEFAULT_ELIGIBILITY_BLOCKER:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    .line 28
    sget-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->DEFAULT_UNKNOWN:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    sput-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->DEFAULT_MONEY_MOVING_BLOCKER:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/ledger/service/EligibilityBlocker;Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;)V
    .locals 1

    .line 63
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/deposits/BalanceActivityFailure;-><init>(Lcom/squareup/protos/ledger/service/EligibilityBlocker;Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/ledger/service/EligibilityBlocker;Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    .line 71
    iput-object p2, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 72
    iput-object p3, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 88
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    iget-object v3, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 92
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    iget-object p1, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    .line 93
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 98
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/ledger/service/EligibilityBlocker;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    .line 80
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    iput-object v1, v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->newBuilder()Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    if-eqz v1, :cond_0

    const-string v1, ", eligibility_blocker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    if-eqz v1, :cond_1

    const-string v1, ", money_moving_blocker="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    if-eqz v1, :cond_2

    const-string v1, ", failure_metadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BalanceActivityFailure{"

    .line 115
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
