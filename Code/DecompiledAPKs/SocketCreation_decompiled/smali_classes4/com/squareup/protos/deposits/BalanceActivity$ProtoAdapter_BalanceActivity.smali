.class final Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BalanceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BalanceActivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/BalanceActivity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 522
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/BalanceActivity;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 567
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;-><init>()V

    .line 568
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 569
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 608
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 606
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto :goto_0

    .line 605
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->external_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto :goto_0

    .line 604
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->client_ip(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto :goto_0

    .line 598
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/deposits/BalanceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/deposits/BalanceType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->balance_type(Lcom/squareup/protos/deposits/BalanceType;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 600
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 595
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->sync_id(Ljava/lang/Long;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto :goto_0

    .line 594
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->pagination_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto :goto_0

    .line 588
    :pswitch_6
    :try_start_1
    sget-object v4, Lcom/squareup/protos/deposits/BalanceActivity$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/deposits/BalanceActivity$State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->state(Lcom/squareup/protos/deposits/BalanceActivity$State;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 590
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 585
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 584
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 578
    :pswitch_9
    :try_start_2
    sget-object v4, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 580
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 575
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/ledger/service/FeeStructure;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure(Lcom/squareup/protos/ledger/service/FeeStructure;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 574
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 573
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 572
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination(Lcom/squareup/protos/deposits/BalanceActivityEntityView;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 571
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source(Lcom/squareup/protos/deposits/BalanceActivityEntityView;)Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    goto/16 :goto_0

    .line 612
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 613
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivity;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 520
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/BalanceActivity;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/BalanceActivity;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 547
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 548
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 549
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 550
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 551
    sget-object v0, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 552
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 553
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 554
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 555
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivity$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 556
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 557
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 558
    sget-object v0, Lcom/squareup/protos/deposits/BalanceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 559
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 560
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 561
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 562
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 520
    check-cast p2, Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/BalanceActivity;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/BalanceActivity;)I
    .locals 4

    .line 527
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    const/4 v3, 0x2

    .line 528
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 529
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->fee_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 530
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    const/4 v3, 0x5

    .line 531
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->type:Lcom/squareup/protos/deposits/BalanceActivityType;

    const/4 v3, 0x6

    .line 532
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->token:Ljava/lang/String;

    const/4 v3, 0x7

    .line 533
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0x8

    .line 534
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivity$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->state:Lcom/squareup/protos/deposits/BalanceActivity$State;

    const/16 v3, 0x9

    .line 535
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->pagination_token:Ljava/lang/String;

    const/16 v3, 0xa

    .line 536
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->sync_id:Ljava/lang/Long;

    const/16 v3, 0xb

    .line 537
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->balance_type:Lcom/squareup/protos/deposits/BalanceType;

    const/16 v3, 0xc

    .line 538
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->client_ip:Ljava/lang/String;

    const/16 v3, 0xd

    .line 539
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->external_token:Ljava/lang/String;

    const/16 v3, 0xe

    .line 540
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivity;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/16 v3, 0xf

    .line 541
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 542
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivity;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 520
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;->encodedSize(Lcom/squareup/protos/deposits/BalanceActivity;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/BalanceActivity;)Lcom/squareup/protos/deposits/BalanceActivity;
    .locals 2

    .line 618
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivity;->newBuilder()Lcom/squareup/protos/deposits/BalanceActivity$Builder;

    move-result-object p1

    .line 619
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 620
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityView;

    .line 621
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 622
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_amount:Lcom/squareup/protos/common/Money;

    .line 623
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/ledger/service/FeeStructure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/ledger/service/FeeStructure;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->fee_structure:Lcom/squareup/protos/ledger/service/FeeStructure;

    .line 624
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 625
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    .line 626
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 627
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivity$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivity;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 520
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivity$ProtoAdapter_BalanceActivity;->redact(Lcom/squareup/protos/deposits/BalanceActivity;)Lcom/squareup/protos/deposits/BalanceActivity;

    move-result-object p1

    return-object p1
.end method
