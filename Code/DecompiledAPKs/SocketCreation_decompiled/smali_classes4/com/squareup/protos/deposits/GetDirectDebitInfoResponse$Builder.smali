.class public final Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetDirectDebitInfoResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_owner_email:Ljava/lang/String;

.field public account_owner_first_name:Ljava/lang/String;

.field public account_owner_last_name:Ljava/lang/String;

.field public bank_name:Ljava/lang/String;

.field public business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

.field public direct_debit_reference_id:Ljava/lang/Long;

.field public error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 190
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_owner_email(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_email:Ljava/lang/String;

    return-object p0
.end method

.method public account_owner_first_name(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_first_name:Ljava/lang/String;

    return-object p0
.end method

.method public account_owner_last_name(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_last_name:Ljava/lang/String;

    return-object p0
.end method

.method public bank_name(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->bank_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;
    .locals 10

    .line 245
    new-instance v9, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;

    iget-object v1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    iget-object v2, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_first_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_last_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->account_owner_email:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iget-object v6, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->direct_debit_reference_id:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->bank_name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;-><init>(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 175
    invoke-virtual {p0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public business_address(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    return-object p0
.end method

.method public direct_debit_reference_id(Ljava/lang/Long;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->direct_debit_reference_id:Ljava/lang/Long;

    return-object p0
.end method

.method public error(Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;)Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Builder;->error:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$GetDirectDebitInfoError;

    return-object p0
.end method
