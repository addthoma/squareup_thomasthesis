.class public final Lcom/squareup/protos/agenda/ClientMessage;
.super Lcom/squareup/wire/Message;
.source "ClientMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/ClientMessage$ProtoAdapter_ClientMessage;,
        Lcom/squareup/protos/agenda/ClientMessage$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/agenda/ClientMessage;",
        "Lcom/squareup/protos/agenda/ClientMessage$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/ClientMessage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOM_CLIENT_MESSAGE_BODY:Ljava/lang/String; = ""

.field public static final DEFAULT_SEND_CUSTOM_CLIENT_MESSAGE_BODY_TO_CLIENT:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_EMAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_SEND_SMS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final custom_client_message_body:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final send_custom_client_message_body_to_client:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final send_email:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final send_sms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/agenda/ClientMessage$ProtoAdapter_ClientMessage;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ClientMessage$ProtoAdapter_ClientMessage;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/ClientMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/agenda/ClientMessage;->DEFAULT_SEND_SMS:Ljava/lang/Boolean;

    .line 29
    sput-object v0, Lcom/squareup/protos/agenda/ClientMessage;->DEFAULT_SEND_EMAIL:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/agenda/ClientMessage;->DEFAULT_SEND_CUSTOM_CLIENT_MESSAGE_BODY_TO_CLIENT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6

    .line 80
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/ClientMessage;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/protos/agenda/ClientMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    .line 88
    iput-object p3, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    .line 89
    iput-object p4, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 106
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/agenda/ClientMessage;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 107
    :cond_1
    check-cast p1, Lcom/squareup/protos/agenda/ClientMessage;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientMessage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/agenda/ClientMessage;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    .line 112
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 117
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 119
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientMessage;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 124
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/agenda/ClientMessage$Builder;
    .locals 2

    .line 94
    new-instance v0, Lcom/squareup/protos/agenda/ClientMessage$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/ClientMessage$Builder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->custom_client_message_body:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_sms:Ljava/lang/Boolean;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_email:Ljava/lang/Boolean;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/agenda/ClientMessage$Builder;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientMessage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/agenda/ClientMessage$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientMessage;->newBuilder()Lcom/squareup/protos/agenda/ClientMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", custom_client_message_body="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->custom_client_message_body:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", send_sms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_sms:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 134
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", send_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", send_custom_client_message_body_to_client="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientMessage;->send_custom_client_message_body_to_client:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClientMessage{"

    .line 136
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
