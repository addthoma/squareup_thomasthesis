.class public final Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SendConfirmationMessageResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;",
        "Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/agenda/ResponseStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;

    iget-object v1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;-><init>(Lcom/squareup/protos/agenda/ResponseStatus;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;->build()Lcom/squareup/protos/agenda/SendConfirmationMessageResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/agenda/ResponseStatus;)Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/agenda/SendConfirmationMessageResponse$Builder;->status:Lcom/squareup/protos/agenda/ResponseStatus;

    return-object p0
.end method
