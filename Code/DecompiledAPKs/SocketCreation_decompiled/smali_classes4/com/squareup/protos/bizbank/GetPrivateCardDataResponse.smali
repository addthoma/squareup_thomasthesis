.class public final Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;
.super Lcom/squareup/wire/Message;
.source "GetPrivateCardDataResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$ProtoAdapter_GetPrivateCardDataResponse;,
        Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;,
        Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ERROR:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

.field private static final serialVersionUID:J


# instance fields
.field public final error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.GetPrivateCardDataResponse$Error#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.PrivateCardData#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$ProtoAdapter_GetPrivateCardDataResponse;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$ProtoAdapter_GetPrivateCardDataResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;->DO_NOT_USE:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    sput-object v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->DEFAULT_ERROR:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/PrivateCardData;Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;)V
    .locals 1

    .line 41
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;-><init>(Lcom/squareup/protos/bizbank/PrivateCardData;Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/PrivateCardData;Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;Lokio/ByteString;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 47
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    .line 50
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    .line 51
    iput-object p2, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    return-void

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of private_card_info, error may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 66
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 67
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    .line 69
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    .line 70
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 75
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/PrivateCardData;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 79
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 80
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;-><init>()V

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    .line 58
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->newBuilder()Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    if-eqz v1, :cond_0

    const-string v1, ", private_card_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    if-eqz v1, :cond_1

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetPrivateCardDataResponse{"

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
