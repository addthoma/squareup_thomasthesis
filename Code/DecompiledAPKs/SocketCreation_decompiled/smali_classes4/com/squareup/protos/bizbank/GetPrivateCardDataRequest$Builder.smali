.class public final Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetPrivateCardDataRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->card_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public card_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->card_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
