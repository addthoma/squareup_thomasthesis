.class final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetUnifiedActivityDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Response"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 223
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 242
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;-><init>()V

    .line 243
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 244
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 249
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 247
    :cond_0
    sget-object v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    goto :goto_0

    .line 246
    :cond_1
    sget-object v3, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    goto :goto_0

    .line 253
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 254
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 221
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 235
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 236
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 237
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 221
    check-cast p2, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)I
    .locals 4

    .line 228
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    const/4 v3, 0x2

    .line 229
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 221
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;->encodedSize(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
    .locals 2

    .line 259
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;->newBuilder()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;

    move-result-object p1

    .line 260
    iget-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bizbank/UnifiedActivity;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 261
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    .line 262
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 263
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 221
    check-cast p1, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$ProtoAdapter_Response;->redact(Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    move-result-object p1

    return-object p1
.end method
