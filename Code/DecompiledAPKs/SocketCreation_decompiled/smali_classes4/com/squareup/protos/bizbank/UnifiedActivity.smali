.class public final Lcom/squareup/protos/bizbank/UnifiedActivity;
.super Lcom/squareup/wire/Message;
.source "UnifiedActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/UnifiedActivity$ProtoAdapter_UnifiedActivity;,
        Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;,
        Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;,
        Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;,
        Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTIVITY_STATE:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final DEFAULT_ACTIVITY_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_ACTIVITY_TYPE:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

.field public static final DEFAULT_CARD_LAST_FOUR:Ljava/lang/String; = ""

.field public static final DEFAULT_DECLINE_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivity$ActivityState#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final activity_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivity$ActivityType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final balance:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final card_last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final decline_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final latest_event_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivity$MerchantImage#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final transaction_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ProtoAdapter_UnifiedActivity;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivity$ProtoAdapter_UnifiedActivity;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->UNKNOWN:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->DEFAULT_ACTIVITY_TYPE:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 37
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->DEFAULT_ACTIVITY_STATE:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    .line 161
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/bizbank/UnifiedActivity;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 168
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    .line 170
    iput-object p2, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 171
    iput-object p3, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 172
    iput-object p4, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 173
    iput-object p5, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    .line 174
    iput-object p6, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    .line 175
    iput-object p7, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    .line 176
    iput-object p8, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    .line 177
    iput-object p9, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    .line 178
    iput-object p10, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    .line 179
    iput-object p11, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    .line 180
    iput-object p12, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 205
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 206
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    .line 213
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    .line 219
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 224
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_c

    .line 226
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_b
    add-int/2addr v0, v2

    .line 239
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_c
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;
    .locals 2

    .line 185
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_token:Ljava/lang/String;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->description:Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->balance:Lcom/squareup/protos/common/Money;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->transaction_token:Ljava/lang/String;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->unit_token:Ljava/lang/String;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->decline_reason:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->card_last_four:Ljava/lang/String;

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivity;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", activity_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", latest_event_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-eqz v1, :cond_2

    const-string v1, ", activity_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    if-eqz v1, :cond_3

    const-string v1, ", activity_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 253
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->balance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    if-eqz v1, :cond_7

    const-string v1, ", merchant_image="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->merchant_image:Lcom/squareup/protos/bizbank/UnifiedActivity$MerchantImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", transaction_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->transaction_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", decline_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->decline_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", card_last_four="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivity;->card_last_four:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UnifiedActivity{"

    .line 259
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
