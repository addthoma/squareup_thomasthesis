.class public final Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetUnifiedActivityDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;",
        "Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

.field public unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 196
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;
    .locals 4

    .line 217
    new-instance v0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 191
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->build()Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response;

    move-result-object v0

    return-object v0
.end method

.method public unified_activity(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    return-object p0
.end method

.method public unified_activity_details(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/bizbank/GetUnifiedActivityDetailsResponse$Response$Builder;->unified_activity_details:Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    return-object p0
.end method
