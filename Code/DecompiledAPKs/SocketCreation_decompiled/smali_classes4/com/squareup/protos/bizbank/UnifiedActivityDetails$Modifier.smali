.class public final Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
.super Lcom/squareup/wire/Message;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Modifier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;,
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;,
        Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MODIFIER_AMOUNT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFIER_TYPE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

.field private static final serialVersionUID:J


# instance fields
.field public final modifier_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final modifier_amount_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bizbank.UnifiedActivityDetails$Modifier$ModifierType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 208
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 212
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->DO_NOT_USE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->DEFAULT_MODIFIER_TYPE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 1

    .line 245
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 250
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 252
    iput-object p2, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    .line 253
    iput-object p3, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 269
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 270
    :cond_1
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    .line 271
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 272
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    .line 273
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    .line 274
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 279
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 281
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 285
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    .locals 2

    .line 258
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;-><init>()V

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    .line 260
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    .line 261
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount_description:Ljava/lang/String;

    .line 262
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    if-eqz v1, :cond_0

    const-string v1, ", modifier_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", modifier_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", modifier_amount_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Modifier{"

    .line 296
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
