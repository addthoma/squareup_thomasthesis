.class public final Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnlineStoreItemData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/items/OnlineStoreItemData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/items/OnlineStoreItemData;",
        "Lcom/squareup/protos/items/OnlineStoreItemData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

.field public event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

.field public opted_out_for_auto_sharing:Ljava/lang/Boolean;

.field public pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

.field public type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/items/OnlineStoreItemData;
    .locals 9

    .line 195
    new-instance v8, Lcom/squareup/protos/items/OnlineStoreItemData;

    iget-object v1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->version:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    iget-object v3, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    iget-object v4, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    iget-object v5, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    iget-object v6, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/items/OnlineStoreItemData;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;Lcom/squareup/protos/items/OnlineStoreEventData;Lcom/squareup/protos/items/OnlineStoreDonationData;Lcom/squareup/protos/items/OnlineStorePickupData;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->build()Lcom/squareup/protos/items/OnlineStoreItemData;

    move-result-object v0

    return-object v0
.end method

.method public donation_data(Lcom/squareup/protos/items/OnlineStoreDonationData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    return-object p0
.end method

.method public event_data(Lcom/squareup/protos/items/OnlineStoreEventData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    return-object p0
.end method

.method public opted_out_for_auto_sharing(Ljava/lang/Boolean;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public pickup_data(Lcom/squareup/protos/items/OnlineStorePickupData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
