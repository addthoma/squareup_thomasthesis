.class public final Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/devicesettings/external/DeviceCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/devicesettings/external/DeviceCode;",
        "Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public code:Ljava/lang/String;

.field public created_at:Ljava/lang/String;

.field public device_id:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public pair_by:Ljava/lang/String;

.field public paired_at:Ljava/lang/String;

.field public product_type:Lcom/squareup/protos/devicesettings/ProductType;

.field public status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

.field public status_changed_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 295
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/devicesettings/external/DeviceCode;
    .locals 14

    .line 410
    new-instance v13, Lcom/squareup/protos/devicesettings/external/DeviceCode;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->code:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->device_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    iget-object v6, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->location_id:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    iget-object v8, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->pair_by:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->created_at:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status_changed_at:Ljava/lang/String;

    iget-object v11, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->paired_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/devicesettings/external/DeviceCode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Ljava/lang/String;Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 272
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->build()Lcom/squareup/protos/devicesettings/external/DeviceCode;

    move-result-object v0

    return-object v0
.end method

.method public code(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->code:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 384
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 304
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 354
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 314
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public pair_by(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 374
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->pair_by:Ljava/lang/String;

    return-object p0
.end method

.method public paired_at(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->paired_at:Ljava/lang/String;

    return-object p0
.end method

.method public product_type(Lcom/squareup/protos/devicesettings/ProductType;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status:Lcom/squareup/protos/devicesettings/external/DeviceCode$Status;

    return-object p0
.end method

.method public status_changed_at(Ljava/lang/String;)Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/DeviceCode$Builder;->status_changed_at:Ljava/lang/String;

    return-object p0
.end method
