.class public final Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SearchCustomersResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public customers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Customer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 81
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;->customers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;
    .locals 3

    .line 92
    new-instance v0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;->customers:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;->build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse;

    move-result-object v0

    return-object v0
.end method

.method public customers(Ljava/util/List;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Customer;",
            ">;)",
            "Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;"
        }
    .end annotation

    .line 85
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersResponse$Builder;->customers:Ljava/util/List;

    return-object p0
.end method
