.class public final enum Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
.super Ljava/lang/Enum;
.source "Document.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Document;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DocumentType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType$ProtoAdapter_DocumentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

.field public static final enum FUTURE_RECEIVABLES_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

.field public static final enum LOAN_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    const/4 v1, 0x0

    const-string v2, "DT_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->DT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 141
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    const/4 v2, 0x1

    const-string v3, "FUTURE_RECEIVABLES_AGREEMENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->FUTURE_RECEIVABLES_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 146
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    const/4 v3, 0x2

    const-string v4, "LOAN_AGREEMENT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->LOAN_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 135
    sget-object v4, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->DT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->FUTURE_RECEIVABLES_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->LOAN_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    .line 148
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType$ProtoAdapter_DocumentType;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType$ProtoAdapter_DocumentType;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 152
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 153
    iput p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 163
    :cond_0
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->LOAN_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object p0

    .line 162
    :cond_1
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->FUTURE_RECEIVABLES_AGREEMENT:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object p0

    .line 161
    :cond_2
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->DT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
    .locals 1

    .line 135
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Document$DocumentType;->value:I

    return v0
.end method
