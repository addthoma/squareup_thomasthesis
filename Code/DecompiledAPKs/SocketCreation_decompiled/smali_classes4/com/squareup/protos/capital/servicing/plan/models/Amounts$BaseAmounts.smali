.class public final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;
.super Lcom/squareup/wire/Message;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BaseAmounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$ProtoAdapter_BaseAmounts;,
        Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final paid_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final remaining_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 171
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$ProtoAdapter_BaseAmounts;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$ProtoAdapter_BaseAmounts;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 188
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 192
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    .line 194
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 209
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 210
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 213
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 218
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 223
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;
    .locals 2

    .line 199
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;-><init>()V

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 170
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", paid_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", remaining_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$BaseAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BaseAmounts{"

    .line 233
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
