.class public final Lcom/squareup/protos/capital/servicing/plan/models/Payer;
.super Lcom/squareup/wire/Message;
.source "Payer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;,
        Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
        "Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Payer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_CAN_DEBIT_SQ_BALANCE_LINKED_ACCOUNT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_PRIMARY:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final business_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final can_debit_sq_balance_linked_account:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final instruments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.instruments.models.InstrumentSummary#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final is_primary:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$ProtoAdapter_Payer;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->DEFAULT_IS_PRIMARY:Ljava/lang/Boolean;

    .line 39
    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->DEFAULT_CAN_DEBIT_SQ_BALANCE_LINKED_ACCOUNT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;)V"
        }
    .end annotation

    .line 94
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/servicing/instruments/models/InstrumentSummary;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 99
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 100
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    .line 102
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    .line 103
    iput-object p4, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    const-string p1, "instruments"

    .line 104
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 122
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 123
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    .line 125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    .line 126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    .line 127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    .line 129
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 134
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 136
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;
    .locals 2

    .line 109
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;-><init>()V

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->customer_id:Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->is_primary:Ljava/lang/Boolean;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->business_name:Ljava/lang/String;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->instruments:Ljava/util/List;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Payer$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_primary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->is_primary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_debit_sq_balance_linked_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->can_debit_sq_balance_linked_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 153
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", business_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", instruments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/Payer;->instruments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Payer{"

    .line 155
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
