.class public final Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;
.super Lcom/squareup/wire/Message;
.source "RepaymentTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PortionOfSales"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$ProtoAdapter_PortionOfSales;,
        Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;",
        "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HOLD_RATE_BPS:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final hold_rate_bps:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final minimum_payment_money:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$ProtoAdapter_PortionOfSales;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$ProtoAdapter_PortionOfSales;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 59
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 65
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 81
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 82
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    .line 85
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 90
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 95
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->hold_rate_bps:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", hold_rate_bps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->hold_rate_bps:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", minimum_payment_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->minimum_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PortionOfSales{"

    .line 105
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
