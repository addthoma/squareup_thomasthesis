.class public final Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;
.super Lcom/squareup/wire/Message;
.source "PreviewOfferFlow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;,
        Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_OFFER_FLOW_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REVIEW_TYPE:Lcom/squareup/protos/capital/external/business/models/ReviewType;

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final offer_flow_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.ReviewType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.external.business.models.ApplicationStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$ProtoAdapter_PreviewOfferFlow;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 26
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->AS_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->DEFAULT_STATUS:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 28
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/ReviewType;->RT_DO_NOT_USE:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    sput-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->DEFAULT_REVIEW_TYPE:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;Lcom/squareup/protos/capital/external/business/models/ReviewType;)V
    .locals 1

    .line 58
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;-><init>(Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;Lcom/squareup/protos/capital/external/business/models/ReviewType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;Lcom/squareup/protos/capital/external/business/models/ReviewType;Lokio/ByteString;)V
    .locals 1

    .line 63
    sget-object v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    iget-object v3, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/capital/external/business/models/ReviewType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 98
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->offer_flow_id:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    iput-object v1, v0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", offer_flow_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->offer_flow_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    if-eqz v1, :cond_1

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->status:Lcom/squareup/protos/capital/external/business/models/ApplicationStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    if-eqz v1, :cond_2

    const-string v1, ", review_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;->review_type:Lcom/squareup/protos/capital/external/business/models/ReviewType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PreviewOfferFlow{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
