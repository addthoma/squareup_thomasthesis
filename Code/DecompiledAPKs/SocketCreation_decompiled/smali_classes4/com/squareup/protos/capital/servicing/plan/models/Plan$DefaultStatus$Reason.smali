.class public final enum Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;
.super Ljava/lang/Enum;
.source "Plan.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason$ProtoAdapter_Reason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BANKRUPTCY_DEBT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum BANK_REQUEST:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum BUSINESS_CONTROL:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum BUSINESS_NAME:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum BUSINESS_OPERATIONS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum BUSINESS_SOLD:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum CARD_PROCESSING:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum COOPERATION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum DR_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum INSPECTION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

.field public static final enum NO_TRANSFER:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 972
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v1, 0x0

    const-string v2, "DR_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->DR_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 974
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v2, 0x1

    const-string v3, "NO_TRANSFER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->NO_TRANSFER:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 976
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v3, 0x2

    const-string v4, "CARD_PROCESSING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->CARD_PROCESSING:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 978
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v4, 0x3

    const-string v5, "BUSINESS_OPERATIONS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_OPERATIONS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 980
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v5, 0x4

    const-string v6, "BUSINESS_NAME"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_NAME:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 982
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v6, 0x5

    const-string v7, "BUSINESS_CONTROL"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_CONTROL:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 984
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v7, 0x6

    const-string v8, "BUSINESS_SOLD"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_SOLD:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 986
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/4 v8, 0x7

    const-string v9, "BANK_REQUEST"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANK_REQUEST:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 988
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/16 v9, 0x8

    const-string v10, "COOPERATION"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->COOPERATION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 990
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/16 v10, 0x9

    const-string v11, "INSPECTION"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->INSPECTION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 992
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/16 v11, 0xa

    const-string v12, "BANKRUPTCY_DEBT_DISCHARGED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANKRUPTCY_DEBT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 971
    sget-object v12, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->DR_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->NO_TRANSFER:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->CARD_PROCESSING:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_OPERATIONS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_NAME:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_CONTROL:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_SOLD:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANK_REQUEST:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->COOPERATION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->INSPECTION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANKRUPTCY_DEBT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    .line 994
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason$ProtoAdapter_Reason;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason$ProtoAdapter_Reason;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 999
    iput p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 1017
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANKRUPTCY_DEBT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1016
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->INSPECTION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1015
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->COOPERATION:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1014
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BANK_REQUEST:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1013
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_SOLD:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1012
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_CONTROL:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1011
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_NAME:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1010
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->BUSINESS_OPERATIONS:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1009
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->CARD_PROCESSING:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1008
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->NO_TRANSFER:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    .line 1007
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->DR_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;
    .locals 1

    .line 971
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;
    .locals 1

    .line 971
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1024
    iget v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Reason;->value:I

    return v0
.end method
