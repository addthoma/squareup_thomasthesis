.class public final Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;
.super Lcom/squareup/wire/Message;
.source "SupportResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ProtoAdapter_SupportResource;,
        Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;,
        Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
        "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PHONE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

.field public static final DEFAULT_URL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.capital.servicing.plan.models.SupportResource$ResourceType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ProtoAdapter_SupportResource;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ProtoAdapter_SupportResource;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;->SR_RT_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->DEFAULT_TYPE:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 65
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;-><init>(Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 70
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    return-void

    .line 71
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of url, phone may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 91
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 92
    :cond_1
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;

    .line 93
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    .line 94
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    .line 95
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    .line 96
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 101
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 107
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->url:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->phone:Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->type:Lcom/squareup/protos/capital/servicing/plan/models/SupportResource$ResourceType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", phone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/plan/models/SupportResource;->phone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SupportResource{"

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
