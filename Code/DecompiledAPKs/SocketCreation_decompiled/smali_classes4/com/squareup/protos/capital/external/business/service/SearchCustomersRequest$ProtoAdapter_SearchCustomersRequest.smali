.class final Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$ProtoAdapter_SearchCustomersRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SearchCustomersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SearchCustomersRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 103
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    new-instance v0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;-><init>()V

    .line 121
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 122
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 126
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 124
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->partner_ids:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$ProtoAdapter_SearchCustomersRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;->partner_ids:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 115
    invoke-virtual {p2}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 101
    check-cast p2, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$ProtoAdapter_SearchCustomersRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)I
    .locals 3

    .line 108
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;->partner_ids:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 109
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 101
    check-cast p1, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$ProtoAdapter_SearchCustomersRequest;->encodedSize(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;
    .locals 0

    .line 136
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;->newBuilder()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 138
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$Builder;->build()Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 101
    check-cast p1, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest$ProtoAdapter_SearchCustomersRequest;->redact(Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;)Lcom/squareup/protos/capital/external/business/service/SearchCustomersRequest;

    move-result-object p1

    return-object p1
.end method
