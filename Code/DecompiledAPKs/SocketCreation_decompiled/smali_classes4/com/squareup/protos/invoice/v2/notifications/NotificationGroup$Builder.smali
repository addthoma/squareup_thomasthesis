.class public final Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group:Ljava/lang/String;

.field public group_label:Ljava/lang/String;

.field public notification_setting:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 118
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->notification_setting:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;
    .locals 5

    .line 145
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->group:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->group_label:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->notification_setting:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup;

    move-result-object v0

    return-object v0
.end method

.method public group(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->group:Ljava/lang/String;

    return-object p0
.end method

.method public group_label(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;
    .locals 0

    .line 133
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->group_label:Ljava/lang/String;

    return-object p0
.end method

.method public notification_setting(Ljava/util/List;)Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;",
            ">;)",
            "Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;"
        }
    .end annotation

    .line 138
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/NotificationGroup$Builder;->notification_setting:Ljava/util/List;

    return-object p0
.end method
