.class final Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$ProtoAdapter_NotificationSetting;
.super Lcom/squareup/wire/ProtoAdapter;
.source "NotificationSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_NotificationSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 195
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 220
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;-><init>()V

    .line 221
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 222
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 244
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 237
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->push_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 239
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 229
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->email_setting(Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 231
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 226
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_description(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;

    goto :goto_0

    .line 225
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category_label(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;

    goto :goto_0

    .line 224
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->category(Ljava/lang/String;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;

    goto :goto_0

    .line 248
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 249
    invoke-virtual {v0}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 193
    invoke-virtual {p0, p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$ProtoAdapter_NotificationSetting;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 210
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 211
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category_label:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 212
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category_description:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 213
    sget-object v0, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 214
    sget-object v0, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 215
    invoke-virtual {p2}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 193
    check-cast p2, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$ProtoAdapter_NotificationSetting;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)I
    .locals 4

    .line 200
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category_label:Ljava/lang/String;

    const/4 v3, 0x2

    .line 201
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->category_description:Ljava/lang/String;

    const/4 v3, 0x3

    .line 202
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->email_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    const/4 v3, 0x4

    .line 203
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->push_setting:Lcom/squareup/protos/invoice/v2/notifications/PreferenceSetting;

    const/4 v3, 0x5

    .line 204
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 193
    check-cast p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$ProtoAdapter_NotificationSetting;->encodedSize(Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;
    .locals 0

    .line 254
    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;->newBuilder()Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;

    move-result-object p1

    .line 255
    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 256
    invoke-virtual {p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 193
    check-cast p1, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting$ProtoAdapter_NotificationSetting;->redact(Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;)Lcom/squareup/protos/invoice/v2/notifications/NotificationSetting;

    move-result-object p1

    return-object p1
.end method
