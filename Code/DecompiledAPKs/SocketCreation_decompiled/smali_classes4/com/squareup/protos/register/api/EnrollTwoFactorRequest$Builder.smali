.class public final Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "EnrollTwoFactorRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotence_token:Ljava/lang/String;

.field public two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;

    iget-object v1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    iget-object v2, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->idempotence_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;-><init>(Lcom/squareup/protos/multipass/common/TwoFactorDetails;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->build()Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotence_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->idempotence_token:Ljava/lang/String;

    return-object p0
.end method

.method public two_factor_details(Lcom/squareup/protos/multipass/common/TwoFactorDetails;)Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/register/api/EnrollTwoFactorRequest$Builder;->two_factor_details:Lcom/squareup/protos/multipass/common/TwoFactorDetails;

    return-object p0
.end method
