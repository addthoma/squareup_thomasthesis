.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;
.super Lcom/squareup/wire/Message;
.source "CustomReportAggregateOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$ProtoAdapter_CustomReportAggregateOptions;,
        Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCLUDE_BILL_EVENT_TOKENS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_GIFT_CARD_ITEMIZATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_PARTIAL_PAYMENT_DETAILS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final include_bill_event_tokens:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final include_gift_card_itemizations:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final include_partial_payment_details:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$ProtoAdapter_CustomReportAggregateOptions;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$ProtoAdapter_CustomReportAggregateOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->DEFAULT_INCLUDE_GIFT_CARD_ITEMIZATIONS:Ljava/lang/Boolean;

    .line 28
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->DEFAULT_INCLUDE_BILL_EVENT_TOKENS:Ljava/lang/Boolean;

    .line 30
    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->DEFAULT_INCLUDE_PARTIAL_PAYMENT_DETAILS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 74
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    .line 83
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 99
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 100
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    .line 103
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;-><init>()V

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_gift_card_itemizations:Ljava/lang/Boolean;

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_bill_event_tokens:Ljava/lang/Boolean;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->include_partial_payment_details:Ljava/lang/Boolean;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", include_gift_card_itemizations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_gift_card_itemizations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", include_bill_event_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_bill_event_tokens:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", include_partial_payment_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->include_partial_payment_details:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomReportAggregateOptions{"

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
