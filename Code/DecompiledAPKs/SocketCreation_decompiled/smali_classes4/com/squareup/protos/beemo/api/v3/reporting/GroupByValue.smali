.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final DEFAULT_FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

.field public static final DEFAULT_FULFILLMENT_STATE:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final DEFAULT_FULFILLMENT_TYPE:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final DEFAULT_GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

.field public static final DEFAULT_HOUR_OF_DAY:Ljava/lang/Integer;

.field public static final DEFAULT_ORDER_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

.field public static final DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final DEFAULT_RISK_EVALUATION_ACTION:Lcom/squareup/protos/riskevaluation/external/Action;

.field public static final DEFAULT_RISK_EVALUATION_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

.field public static final DEFAULT_RISK_EVALUATION_RULE:Ljava/lang/String; = ""

.field public static final DEFAULT_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

.field public static final DEFAULT_TRANSACTION_COMPLETED_AT_MS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Employee#ADAPTER"
        tag = 0x32
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ActivityTypeDetails#ADAPTER"
        tag = 0x1e
    .end annotation
.end field

.field public final bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$BillNote#ADAPTER"
        tag = 0x2c
    .end annotation
.end field

.field public final business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$BusinessDayDetails#ADAPTER"
        tag = 0x1d
    .end annotation
.end field

.field public final card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$CardDetails#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$CompReason#ADAPTER"
        tag = 0x22
    .end annotation
.end field

.field public final contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ContactDetails#ADAPTER"
        tag = 0x38
    .end annotation
.end field

.field public final conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ConversationalMode#ADAPTER"
        tag = 0x2b
    .end annotation
.end field

.field public final day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TimeRange#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DayOfWeek#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Device#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DeviceCredential#ADAPTER"
        tag = 0x26
    .end annotation
.end field

.field public final dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiningOption#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Discount#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiscountAdjustmentTypeDetails#ADAPTER"
        tag = 0x21
    .end annotation
.end field

.field public final employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Employee#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$EmployeeRole#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$EventTypeDetails#ADAPTER"
        tag = 0x20
    .end annotation
.end field

.field public final fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$FeePlan#ADAPTER"
        tag = 0x30
    .end annotation
.end field

.field public final fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$FeePlanTypeDetails#ADAPTER"
        tag = 0x31
    .end annotation
.end field

.field public final fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$FeeType#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v4.FulfillmentRecipient#ADAPTER"
        tag = 0x3f
    .end annotation
.end field

.field public final fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$State#ADAPTER"
        tag = 0x3e
    .end annotation
.end field

.field public final fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentType#ADAPTER"
        tag = 0x3d
    .end annotation
.end field

.field public final gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$GiftCard#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$GiftCardActivityType#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TimeRange#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final hour_of_day:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x16
    .end annotation
.end field

.field public final item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Item#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemCategory#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemVariation#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemizationNote#ADAPTER"
        tag = 0x27
    .end annotation
.end field

.field public final itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemizationTypeDetails#ADAPTER"
        tag = 0x36
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Menu#ADAPTER"
        tag = 0x2e
    .end annotation
.end field

.field public final mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$MobileStaff#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Modifier#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ModifierSet#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TimeRange#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final order_display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3c
    .end annotation
.end field

.field public final order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$OrderSourceName#ADAPTER"
        tag = 0x3a
    .end annotation
.end field

.field public final payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$PaymentMethod#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$PaymentSourceName#ADAPTER"
        tag = 0x35
    .end annotation
.end field

.field public final product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Product#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ReceiptDetails#ADAPTER"
        tag = 0x28
    .end annotation
.end field

.field public final restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$RestaurantCoverDetails#ADAPTER"
        tag = 0x34
    .end annotation
.end field

.field public final risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.riskevaluation.external.Action#ADAPTER"
        tag = 0x41
    .end annotation
.end field

.field public final risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.riskevaluation.external.Level#ADAPTER"
        tag = 0x3b
    .end annotation
.end field

.field public final risk_evaluation_rule:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x42
    .end annotation
.end field

.field public final subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Subunit#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$SurchargeNameDetails#ADAPTER"
        tag = 0x39
    .end annotation
.end field

.field public final surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$SurchargeTypeDetails#ADAPTER"
        tag = 0x37
    .end annotation
.end field

.field public final tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Tax#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Taxable#ADAPTER"
        tag = 0x14
    .end annotation
.end field

.field public final tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TenderNote#ADAPTER"
        tag = 0x2d
    .end annotation
.end field

.field public final ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TicketGroup#ADAPTER"
        tag = 0x25
    .end annotation
.end field

.field public final ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TicketName#ADAPTER"
        tag = 0x29
    .end annotation
.end field

.field public final ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TicketTemplate#ADAPTER"
        tag = 0x33
    .end annotation
.end field

.field public final time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TimeRange#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final transaction_completed_at_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2f
    .end annotation
.end field

.field public final transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TransactionTotal#ADAPTER"
        tag = 0x2a
    .end annotation
.end field

.field public final unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$UniqueItemization#ADAPTER"
        tag = 0x24
    .end annotation
.end field

.field public final unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$UnitPrice#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$Vendor#ADAPTER"
        tag = 0x40
    .end annotation
.end field

.field public final virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$VirtualRegisterDetails#ADAPTER"
        tag = 0x1c
    .end annotation
.end field

.field public final void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$VoidReason#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$TimeRange#ADAPTER"
        tag = 0x17
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ProtoAdapter_GroupByValue;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 43
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_TRANSACTION_COMPLETED_AT_MS:Ljava/lang/Long;

    .line 45
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 47
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->SWIPED:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_FEE_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 49
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 51
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_TAXABLE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 53
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SUNDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_DAY_OF_WEEK:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v0, 0x0

    .line 55
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_HOUR_OF_DAY:Ljava/lang/Integer;

    .line 57
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;->NO_ACTIVITY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_GIFT_CARD_ACTIVITY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    .line 59
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_RISK_EVALUATION_LEVEL:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 61
    sget-object v0, Lcom/squareup/protos/riskevaluation/external/Action;->DO_NOT_USE:Lcom/squareup/protos/riskevaluation/external/Action;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_RISK_EVALUATION_ACTION:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 67
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_FULFILLMENT_TYPE:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 69
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->DEFAULT_FULFILLMENT_STATE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;Lokio/ByteString;)V
    .locals 1

    .line 480
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 481
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_completed_at_ms:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    .line 482
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 483
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 484
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 485
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 486
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    .line 487
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    .line 488
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 489
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 490
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    .line 491
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    .line 492
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 493
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 494
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    .line 495
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 496
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 497
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 498
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    .line 499
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 500
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 501
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 502
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 503
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 504
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 505
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour_of_day:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    .line 506
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 507
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    .line 508
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    .line 509
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    .line 510
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    .line 511
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    .line 512
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    .line 513
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    .line 514
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    .line 515
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    .line 516
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    .line 517
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    .line 518
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    .line 519
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    .line 520
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    .line 521
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    .line 522
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    .line 523
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    .line 524
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    .line 525
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    .line 526
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    .line 527
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    .line 528
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    .line 529
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    .line 530
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    .line 531
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    .line 532
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    .line 533
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    .line 534
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    .line 535
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    .line 536
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    .line 537
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    .line 538
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    .line 539
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 540
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 541
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_rule:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    .line 542
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_display_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    .line 543
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 544
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 545
    iget-object p2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    .line 546
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 625
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 626
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    .line 627
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    .line 628
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 629
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 630
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 631
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 632
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    .line 633
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    .line 634
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 635
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 636
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    .line 637
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    .line 638
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 639
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 640
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    .line 641
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 642
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 643
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 644
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    .line 645
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 646
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 647
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 648
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 649
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 650
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 651
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    .line 652
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 653
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    .line 654
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    .line 655
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    .line 656
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    .line 657
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    .line 658
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    .line 659
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    .line 660
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    .line 661
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    .line 662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    .line 663
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    .line 664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    .line 665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    .line 666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    .line 667
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    .line 668
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    .line 669
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    .line 670
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    .line 671
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    .line 672
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    .line 673
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    .line 674
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    .line 675
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    .line 676
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    .line 677
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    .line 678
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    .line 679
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    .line 680
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    .line 681
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    .line 682
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    .line 683
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    .line 684
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    .line 685
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 686
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 687
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    .line 688
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    .line 689
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 690
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 691
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    .line 692
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    .line 693
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 698
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_42

    .line 700
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 701
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 702
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 703
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 710
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 711
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 712
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 713
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 714
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 715
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 716
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 717
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 718
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 719
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 720
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 721
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 722
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 723
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 724
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 725
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 726
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 727
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 728
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 729
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 730
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 731
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 732
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 733
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 734
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 735
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 736
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 737
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 738
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 739
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 740
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 741
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 742
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 743
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 744
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 745
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;->hashCode()I

    move-result v1

    goto :goto_2c

    :cond_2c
    const/4 v1, 0x0

    :goto_2c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 746
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    if-eqz v1, :cond_2d

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;->hashCode()I

    move-result v1

    goto :goto_2d

    :cond_2d
    const/4 v1, 0x0

    :goto_2d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 747
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;->hashCode()I

    move-result v1

    goto :goto_2e

    :cond_2e
    const/4 v1, 0x0

    :goto_2e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 748
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;->hashCode()I

    move-result v1

    goto :goto_2f

    :cond_2f
    const/4 v1, 0x0

    :goto_2f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 749
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    if-eqz v1, :cond_30

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;->hashCode()I

    move-result v1

    goto :goto_30

    :cond_30
    const/4 v1, 0x0

    :goto_30
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 750
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    if-eqz v1, :cond_31

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;->hashCode()I

    move-result v1

    goto :goto_31

    :cond_31
    const/4 v1, 0x0

    :goto_31
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 751
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    if-eqz v1, :cond_32

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;->hashCode()I

    move-result v1

    goto :goto_32

    :cond_32
    const/4 v1, 0x0

    :goto_32
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 752
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    if-eqz v1, :cond_33

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;->hashCode()I

    move-result v1

    goto :goto_33

    :cond_33
    const/4 v1, 0x0

    :goto_33
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 753
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    if-eqz v1, :cond_34

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;->hashCode()I

    move-result v1

    goto :goto_34

    :cond_34
    const/4 v1, 0x0

    :goto_34
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 754
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    if-eqz v1, :cond_35

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->hashCode()I

    move-result v1

    goto :goto_35

    :cond_35
    const/4 v1, 0x0

    :goto_35
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 755
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    if-eqz v1, :cond_36

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;->hashCode()I

    move-result v1

    goto :goto_36

    :cond_36
    const/4 v1, 0x0

    :goto_36
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 756
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    if-eqz v1, :cond_37

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;->hashCode()I

    move-result v1

    goto :goto_37

    :cond_37
    const/4 v1, 0x0

    :goto_37
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 757
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    if-eqz v1, :cond_38

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;->hashCode()I

    move-result v1

    goto :goto_38

    :cond_38
    const/4 v1, 0x0

    :goto_38
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 758
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    if-eqz v1, :cond_39

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;->hashCode()I

    move-result v1

    goto :goto_39

    :cond_39
    const/4 v1, 0x0

    :goto_39
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 759
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    if-eqz v1, :cond_3a

    invoke-virtual {v1}, Lcom/squareup/protos/riskevaluation/external/Level;->hashCode()I

    move-result v1

    goto :goto_3a

    :cond_3a
    const/4 v1, 0x0

    :goto_3a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 760
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    if-eqz v1, :cond_3b

    invoke-virtual {v1}, Lcom/squareup/protos/riskevaluation/external/Action;->hashCode()I

    move-result v1

    goto :goto_3b

    :cond_3b
    const/4 v1, 0x0

    :goto_3b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 761
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    if-eqz v1, :cond_3c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3c

    :cond_3c
    const/4 v1, 0x0

    :goto_3c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 762
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    if-eqz v1, :cond_3d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3d

    :cond_3d
    const/4 v1, 0x0

    :goto_3d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 763
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v1, :cond_3e

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->hashCode()I

    move-result v1

    goto :goto_3e

    :cond_3e
    const/4 v1, 0x0

    :goto_3e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 764
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_3f

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->hashCode()I

    move-result v1

    goto :goto_3f

    :cond_3f
    const/4 v1, 0x0

    :goto_3f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 765
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    if-eqz v1, :cond_40

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_40

    :cond_40
    const/4 v1, 0x0

    :goto_40
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 766
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    if-eqz v1, :cond_41

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;->hashCode()I

    move-result v2

    :cond_41
    add-int/2addr v0, v2

    .line 767
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_42
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;
    .locals 2

    .line 551
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 552
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_completed_at_ms:Ljava/lang/Long;

    .line 553
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 554
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 555
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 556
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    .line 557
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    .line 558
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    .line 559
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 560
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 561
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    .line 562
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    .line 563
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    .line 564
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    .line 565
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    .line 566
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 567
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 568
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 569
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    .line 570
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 571
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 572
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    .line 573
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 574
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    .line 575
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 576
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->hour_of_day:Ljava/lang/Integer;

    .line 577
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    .line 578
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    .line 579
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    .line 580
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    .line 581
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    .line 582
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    .line 583
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    .line 584
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    .line 585
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    .line 586
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    .line 587
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    .line 588
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    .line 589
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    .line 590
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    .line 591
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    .line 592
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    .line 593
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    .line 594
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    .line 595
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    .line 596
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    .line 597
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    .line 598
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    .line 599
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    .line 600
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    .line 601
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    .line 602
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    .line 603
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    .line 604
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    .line 605
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    .line 606
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    .line 607
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    .line 608
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    .line 609
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    .line 610
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    .line 611
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    .line 612
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->risk_evaluation_rule:Ljava/lang/String;

    .line 613
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->order_display_name:Ljava/lang/String;

    .line 614
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 615
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 616
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    .line 617
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    .line 618
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 774
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 775
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", transaction_completed_at_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_completed_at_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 776
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    if-eqz v1, :cond_1

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 777
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    if-eqz v1, :cond_2

    const-string v1, ", item_category="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 778
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v1, :cond_3

    const-string v1, ", employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 779
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    if-eqz v1, :cond_4

    const-string v1, ", acting_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->acting_employee:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 780
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    if-eqz v1, :cond_5

    const-string v1, ", employee_role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->employee_role:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EmployeeRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 781
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    if-eqz v1, :cond_6

    const-string v1, ", device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 782
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v1, :cond_7

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 783
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v1, :cond_8

    const-string v1, ", item_variation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 784
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    if-eqz v1, :cond_9

    const-string v1, ", mobile_staff="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->mobile_staff:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$MobileStaff;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 785
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    if-eqz v1, :cond_a

    const-string v1, ", subunit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->subunit:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Subunit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 786
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    if-eqz v1, :cond_b

    const-string v1, ", modifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 787
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    if-eqz v1, :cond_c

    const-string v1, ", payment_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_method:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 788
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    if-eqz v1, :cond_d

    const-string v1, ", modifier_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->modifier_set:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 789
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_e

    const-string v1, ", hour="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 790
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_f

    const-string v1, ", time_window="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->time_window:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 791
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v1, :cond_10

    const-string v1, ", unit_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 792
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    if-eqz v1, :cond_11

    const-string v1, ", tax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tax:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Tax;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 793
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_12

    const-string v1, ", day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 794
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_13

    const-string v1, ", month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->month:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 795
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    if-eqz v1, :cond_14

    const-string v1, ", fee_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeeType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 796
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    if-eqz v1, :cond_15

    const-string v1, ", product="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->product:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 797
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    if-eqz v1, :cond_16

    const-string v1, ", taxable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->taxable:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Taxable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 798
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    if-eqz v1, :cond_17

    const-string v1, ", day_of_week="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->day_of_week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 799
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    const-string v1, ", hour_of_day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->hour_of_day:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 800
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    if-eqz v1, :cond_19

    const-string v1, ", week="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->week:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 801
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    if-eqz v1, :cond_1a

    const-string v1, ", dining_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->dining_option:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiningOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 802
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    if-eqz v1, :cond_1b

    const-string v1, ", gift_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 803
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    if-eqz v1, :cond_1c

    const-string v1, ", gift_card_activity_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->gift_card_activity_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$GiftCardActivityType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 804
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    if-eqz v1, :cond_1d

    const-string v1, ", card_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->card_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CardDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 805
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    if-eqz v1, :cond_1e

    const-string v1, ", virtual_register_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->virtual_register_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VirtualRegisterDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 806
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    if-eqz v1, :cond_1f

    const-string v1, ", business_day_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->business_day_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BusinessDayDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 807
    :cond_1f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    if-eqz v1, :cond_20

    const-string v1, ", activity_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->activity_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 808
    :cond_20
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    if-eqz v1, :cond_21

    const-string v1, ", void_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->void_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$VoidReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 809
    :cond_21
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    if-eqz v1, :cond_22

    const-string v1, ", event_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->event_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$EventTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 810
    :cond_22
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    if-eqz v1, :cond_23

    const-string v1, ", discount_adjustment_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->discount_adjustment_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 811
    :cond_23
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    if-eqz v1, :cond_24

    const-string v1, ", comp_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->comp_reason:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$CompReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 812
    :cond_24
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    if-eqz v1, :cond_25

    const-string v1, ", unique_itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->unique_itemization:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 813
    :cond_25
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    if-eqz v1, :cond_26

    const-string v1, ", ticket_group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_group:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 814
    :cond_26
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    if-eqz v1, :cond_27

    const-string v1, ", device_credential="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->device_credential:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 815
    :cond_27
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    if-eqz v1, :cond_28

    const-string v1, ", itemization_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationNote;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 816
    :cond_28
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    if-eqz v1, :cond_29

    const-string v1, ", receipt_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->receipt_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 817
    :cond_29
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    if-eqz v1, :cond_2a

    const-string v1, ", ticket_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 818
    :cond_2a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    if-eqz v1, :cond_2b

    const-string v1, ", transaction_total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->transaction_total:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 819
    :cond_2b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    if-eqz v1, :cond_2c

    const-string v1, ", conversational_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->conversational_mode:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ConversationalMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 820
    :cond_2c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    if-eqz v1, :cond_2d

    const-string v1, ", bill_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->bill_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 821
    :cond_2d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    if-eqz v1, :cond_2e

    const-string v1, ", tender_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->tender_note:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TenderNote;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 822
    :cond_2e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    if-eqz v1, :cond_2f

    const-string v1, ", menu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->menu:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Menu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 823
    :cond_2f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    if-eqz v1, :cond_30

    const-string v1, ", fee_plan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlan;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 824
    :cond_30
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    if-eqz v1, :cond_31

    const-string v1, ", fee_plan_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fee_plan_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$FeePlanTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 825
    :cond_31
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    if-eqz v1, :cond_32

    const-string v1, ", ticket_template="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->ticket_template:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TicketTemplate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 826
    :cond_32
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    if-eqz v1, :cond_33

    const-string v1, ", restaurant_cover_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->restaurant_cover_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$RestaurantCoverDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 827
    :cond_33
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    if-eqz v1, :cond_34

    const-string v1, ", payment_source_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->payment_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentSourceName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 828
    :cond_34
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    if-eqz v1, :cond_35

    const-string v1, ", itemization_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->itemization_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 829
    :cond_35
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    if-eqz v1, :cond_36

    const-string v1, ", surcharge_type_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_type_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeTypeDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 830
    :cond_36
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    if-eqz v1, :cond_37

    const-string v1, ", contact_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->contact_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ContactDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 831
    :cond_37
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    if-eqz v1, :cond_38

    const-string v1, ", surcharge_name_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->surcharge_name_details:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$SurchargeNameDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 832
    :cond_38
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    if-eqz v1, :cond_39

    const-string v1, ", order_source_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_source_name:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$OrderSourceName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 833
    :cond_39
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    if-eqz v1, :cond_3a

    const-string v1, ", risk_evaluation_level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_level:Lcom/squareup/protos/riskevaluation/external/Level;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 834
    :cond_3a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    if-eqz v1, :cond_3b

    const-string v1, ", risk_evaluation_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_action:Lcom/squareup/protos/riskevaluation/external/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 835
    :cond_3b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    if-eqz v1, :cond_3c

    const-string v1, ", risk_evaluation_rule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->risk_evaluation_rule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 836
    :cond_3c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    if-eqz v1, :cond_3d

    const-string v1, ", order_display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->order_display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 837
    :cond_3d
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v1, :cond_3e

    const-string v1, ", fulfillment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 838
    :cond_3e
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_3f

    const-string v1, ", fulfillment_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 839
    :cond_3f
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    if-eqz v1, :cond_40

    const-string v1, ", fulfillment_recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->fulfillment_recipient:Lcom/squareup/protos/beemo/api/v4/FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 840
    :cond_40
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    if-eqz v1, :cond_41

    const-string v1, ", vendor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;->vendor:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Vendor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_41
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GroupByValue{"

    .line 841
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
