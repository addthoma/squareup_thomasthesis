.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public activity_type:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4550
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public activity_type(Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;
    .locals 0

    .line 4554
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;->activity_type:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;
    .locals 3

    .line 4560
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;->activity_type:Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;-><init>(Lcom/squareup/protos/beemo/itemizations/api/v3/ActivityType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4547
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ActivityTypeDetails;

    move-result-object v0

    return-object v0
.end method
