.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DiscountAdjustmentTypeDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$ProtoAdapter_DiscountAdjustmentTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field private static final serialVersionUID:J


# instance fields
.field public final discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiscountAdjustmentType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4908
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$ProtoAdapter_DiscountAdjustmentTypeDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$ProtoAdapter_DiscountAdjustmentTypeDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4912
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->DEFAULT_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;)V
    .locals 1

    .line 4924
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;Lokio/ByteString;)V
    .locals 1

    .line 4929
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4930
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4944
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4945
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;

    .line 4946
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4947
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 4952
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 4954
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4955
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 4956
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;
    .locals 2

    .line 4935
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;-><init>()V

    .line 4936
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 4937
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4907
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4963
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4964
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    if-eqz v1, :cond_0

    const-string v1, ", discount_adjustment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentTypeDetails;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DiscountAdjustmentTypeDetails{"

    .line 4965
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
