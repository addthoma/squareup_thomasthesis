.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Discount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$ProtoAdapter_Discount;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

.field private static final serialVersionUID:J


# instance fields
.field public final discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$DiscountAdjustmentType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.NameOrTranslationType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1330
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$ProtoAdapter_Discount;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$ProtoAdapter_Discount;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1334
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->DISCOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->DEFAULT_DISCOUNT_ADJUSTMENT_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;)V
    .locals 1

    .line 1356
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;Lokio/ByteString;)V
    .locals 1

    .line 1361
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1362
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 1363
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1378
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1379
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;

    .line 1380
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 1381
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 1382
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1387
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1389
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1390
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1391
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1392
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;
    .locals 2

    .line 1368
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;-><init>()V

    .line 1369
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 1370
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    .line 1371
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1329
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1400
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_0

    const-string v1, ", name_or_translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1401
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    if-eqz v1, :cond_1

    const-string v1, ", discount_adjustment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Discount;->discount_adjustment_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountAdjustmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Discount{"

    .line 1402
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
