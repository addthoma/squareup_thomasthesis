.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public receipt_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6472
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;
    .locals 3

    .line 6482
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;->receipt_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6469
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails;

    move-result-object v0

    return-object v0
.end method

.method public receipt_number(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;
    .locals 0

    .line 6476
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ReceiptDetails$Builder;->receipt_number:Ljava/lang/String;

    return-object p0
.end method
