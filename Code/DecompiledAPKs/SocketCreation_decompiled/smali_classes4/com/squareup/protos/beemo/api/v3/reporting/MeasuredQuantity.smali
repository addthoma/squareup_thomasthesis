.class public final Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;
.super Lcom/squareup/wire/Message;
.source "MeasuredQuantity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$ProtoAdapter_MeasuredQuantity;,
        Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = "0"

.field private static final serialVersionUID:J


# instance fields
.field public final default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.TranslationType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.MeasurementUnit#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$ProtoAdapter_MeasuredQuantity;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$ProtoAdapter_MeasuredQuantity;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->DEFAULT_DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 56
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 59
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 61
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-void

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of defined_measurement_unit, default_measurement_unit may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 77
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 78
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    .line 80
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 82
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 87
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 93
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->quantity:Ljava/lang/String;

    .line 68
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    .line 70
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v1, :cond_1

    const-string v1, ", defined_measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    if-eqz v1, :cond_2

    const-string v1, ", default_measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MeasuredQuantity{"

    .line 104
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
