.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
.super Lcom/squareup/wire/Message;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;,
        Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FILTER_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

.field public static final DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field private static final serialVersionUID:J


# instance fields
.field public final filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.Filter$FilterMode#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByType#ADAPTER"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final group_by_value:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;"
        }
    .end annotation
.end field

.field public final grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupingType#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final reporting_group_value:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.ReportingGroupValue#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$ProtoAdapter_Filter;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->NONE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->DEFAULT_GROUP_BY_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 30
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->INCLUDE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->DEFAULT_FILTER_MODE:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ">;)V"
        }
    .end annotation

    .line 85
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 91
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const-string p1, "group_by_value"

    .line 93
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    const-string p1, "reporting_group_value"

    .line 96
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 114
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    .line 118
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    .line 121
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 126
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    if-eqz v1, :cond_0

    const-string v1, ", group_by_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", group_by_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->group_by_value:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    if-eqz v1, :cond_2

    const-string v1, ", filter_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    if-eqz v1, :cond_3

    const-string v1, ", grouping_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", reporting_group_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->reporting_group_value:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Filter{"

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
