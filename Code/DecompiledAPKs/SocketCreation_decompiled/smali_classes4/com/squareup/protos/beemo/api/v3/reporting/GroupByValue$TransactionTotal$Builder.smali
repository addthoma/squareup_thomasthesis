.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public total_collected_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6706
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;
    .locals 3

    .line 6720
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6703
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal;

    move-result-object v0

    return-object v0
.end method

.method public total_collected_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;
    .locals 0

    .line 6714
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TransactionTotal$Builder;->total_collected_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
