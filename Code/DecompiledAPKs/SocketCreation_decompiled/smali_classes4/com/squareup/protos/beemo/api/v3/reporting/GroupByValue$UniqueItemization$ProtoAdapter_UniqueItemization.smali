.class final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UniqueItemization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5953
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5982
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;-><init>()V

    .line 5983
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5984
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 5994
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5992
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5991
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5990
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5989
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    goto :goto_0

    .line 5988
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    goto :goto_0

    .line 5987
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    goto :goto_0

    .line 5986
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    goto :goto_0

    .line 5998
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5999
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5951
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5970
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5971
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5972
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5973
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5974
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5975
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5976
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5977
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5951
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)I
    .locals 4

    .line 5958
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    const/4 v3, 0x2

    .line 5959
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    const/4 v3, 0x3

    .line 5960
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    const/4 v3, 0x4

    .line 5961
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5962
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->modifier_details:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5963
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->tax_details:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5964
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->discount_details:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5965
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5951
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;
    .locals 2

    .line 6004
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;

    move-result-object p1

    .line 6005
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_category:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemCategory;

    .line 6006
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    .line 6007
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->item_variation:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemVariation;

    .line 6008
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->unit_price:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    .line 6009
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->modifier_details:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 6010
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->tax_details:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$TaxDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 6011
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->discount_details:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DiscountDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 6012
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 6013
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5951
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization$ProtoAdapter_UniqueItemization;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UniqueItemization;

    move-result-object p1

    return-object p1
.end method
