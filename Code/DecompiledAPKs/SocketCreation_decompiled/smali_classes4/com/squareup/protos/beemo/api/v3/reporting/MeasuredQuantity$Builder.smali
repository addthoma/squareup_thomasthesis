.class public final Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MeasuredQuantity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

.field public defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

.field public quantity:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->quantity:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/protos/beemo/translation_types/TranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity;

    move-result-object v0

    return-object v0
.end method

.method public default_measurement_unit(Lcom/squareup/protos/beemo/translation_types/TranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    const/4 p1, 0x0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object p0
.end method

.method public defined_measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->defined_measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 p1, 0x0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->default_measurement_unit:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/MeasuredQuantity$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method
