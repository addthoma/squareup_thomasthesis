.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Product"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product$ProtoAdapter_Product;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum EXTERNAL_API:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum INVOICES:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum MARKET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum ORDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

.field public static final enum REGISTER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 3664
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_PRODUCT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3666
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v2, 0x1

    const-string v3, "REGISTER"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->REGISTER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3668
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v3, 0x2

    const-string v4, "MARKET"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->MARKET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3670
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v4, 0x3

    const-string v5, "ORDER"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ORDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3672
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v5, 0x4

    const-string v6, "EXTERNAL_API"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->EXTERNAL_API:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3674
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v6, 0x5

    const-string v7, "APPOINTMENTS"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->APPOINTMENTS:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3676
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v7, 0x6

    const-string v8, "INVOICES"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->INVOICES:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3663
    sget-object v8, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->REGISTER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->MARKET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ORDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->EXTERNAL_API:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->APPOINTMENTS:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->INVOICES:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    .line 3678
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product$ProtoAdapter_Product;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product$ProtoAdapter_Product;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3682
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3683
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3697
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->INVOICES:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3696
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->APPOINTMENTS:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3695
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->EXTERNAL_API:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3694
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->ORDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3693
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->MARKET:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3692
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->REGISTER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    .line 3691
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->DEFAULT_PRODUCT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;
    .locals 1

    .line 3663
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;
    .locals 1

    .line 3663
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3704
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Product;->value:I

    return v0
.end method
