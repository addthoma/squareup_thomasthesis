.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ItemizationTypeDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ProtoAdapter_ItemizationTypeDetails;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

.field private static final serialVersionUID:J


# instance fields
.field public final itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v3.reporting.GroupByValue$ItemizationTypeDetails$ItemizationType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7911
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ProtoAdapter_ItemizationTypeDetails;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ProtoAdapter_ItemizationTypeDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7915
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->UNKNOWN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->DEFAULT_ITEMIZATION_TYPE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;)V
    .locals 1

    .line 7924
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;Lokio/ByteString;)V
    .locals 1

    .line 7928
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7929
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7943
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7944
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    .line 7945
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7946
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 7951
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 7953
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7954
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 7955
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;
    .locals 2

    .line 7934
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;-><init>()V

    .line 7935
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    .line 7936
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7910
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7962
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7963
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    if-eqz v1, :cond_0

    const-string v1, ", itemization_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemizationTypeDetails{"

    .line 7964
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
