.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_token:Ljava/lang/String;

.field public inside_range_total_collected:Ljava/lang/Long;

.field public total_collected:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 288
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->bill_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;
    .locals 5

    .line 314
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->bill_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->total_collected:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->inside_range_total_collected:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 281
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail;

    move-result-object v0

    return-object v0
.end method

.method public inside_range_total_collected(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->inside_range_total_collected:Ljava/lang/Long;

    return-object p0
.end method

.method public total_collected(Ljava/lang/Long;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse$PartialPaymentDetail$Builder;->total_collected:Ljava/lang/Long;

    return-object p0
.end method
