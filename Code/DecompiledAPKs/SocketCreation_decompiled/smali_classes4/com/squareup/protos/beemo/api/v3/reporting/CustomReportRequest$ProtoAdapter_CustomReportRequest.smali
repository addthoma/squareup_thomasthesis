.class final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CustomReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CustomReportRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 230
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 257
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;-><init>()V

    .line 258
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 259
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/4 v4, 0x7

    if-eq v3, v4, :cond_2

    const/16 v4, 0x9

    if-eq v3, v4, :cond_1

    const/16 v4, 0xa

    if-eq v3, v4, :cond_0

    .line 275
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 273
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 272
    :cond_1
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    goto :goto_0

    .line 271
    :cond_2
    sget-object v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    goto :goto_0

    .line 270
    :cond_3
    iget-object v3, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    :cond_4
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 266
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 261
    :cond_5
    sget-object v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    goto :goto_0

    .line 279
    :cond_6
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 280
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 247
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 248
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 249
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 250
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 251
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 252
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 228
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)I
    .locals 4

    .line 235
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 236
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->group_by_type:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 237
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->filter:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    const/4 v3, 0x7

    .line 238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    const/16 v3, 0x9

    .line 239
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 240
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->grouping_type:Ljava/util/List;

    const/16 v3, 0xa

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 228
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
    .locals 2

    .line 285
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 286
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    .line 287
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 288
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequestFlags;

    .line 289
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->aggregate_options:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportAggregateOptions;

    .line 290
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->grouping_type:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 291
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 292
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 228
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$ProtoAdapter_CustomReportRequest;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    return-object p1
.end method
