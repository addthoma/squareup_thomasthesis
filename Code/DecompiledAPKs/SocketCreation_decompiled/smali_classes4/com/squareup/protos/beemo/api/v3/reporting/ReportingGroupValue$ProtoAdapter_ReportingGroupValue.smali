.class final Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$ProtoAdapter_ReportingGroupValue;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReportingGroupValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReportingGroupValue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 97
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;-><init>()V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 116
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 120
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 118
    :cond_0
    sget-object v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 125
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$ProtoAdapter_ReportingGroupValue;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$ProtoAdapter_ReportingGroupValue;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)I
    .locals 3

    .line 102
    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 103
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$ProtoAdapter_ReportingGroupValue;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;
    .locals 2

    .line 130
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;

    move-result-object p1

    .line 131
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->reporting_group_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 132
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue$ProtoAdapter_ReportingGroupValue;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    move-result-object p1

    return-object p1
.end method
