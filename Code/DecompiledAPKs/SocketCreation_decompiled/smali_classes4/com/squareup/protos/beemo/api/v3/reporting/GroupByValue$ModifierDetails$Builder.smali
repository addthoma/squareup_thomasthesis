.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

.field public single_quantity_applied_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5231
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;
    .locals 4

    .line 5246
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5226
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails;

    move-result-object v0

    return-object v0
.end method

.method public modifier(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;
    .locals 0

    .line 5235
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->modifier:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    return-object p0
.end method

.method public single_quantity_applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;
    .locals 0

    .line 5240
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ModifierDetails$Builder;->single_quantity_applied_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
