.class public final Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/protos/connect/v2/resources/Card;

.field public entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 779
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
    .locals 5

    .line 814
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;-><init>(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;Lcom/squareup/protos/connect/v2/resources/Card;Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 772
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/connect/v2/resources/Card;)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
    .locals 0

    .line 798
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
    .locals 0

    .line 808
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
    .locals 0

    .line 790
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0
.end method
