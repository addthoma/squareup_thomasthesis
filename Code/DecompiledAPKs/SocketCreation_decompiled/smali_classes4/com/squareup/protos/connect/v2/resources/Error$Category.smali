.class public final enum Lcom/squareup/protos/connect/v2/resources/Error$Category;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Error$Category$ProtoAdapter_Category;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Error$Category;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Error$Category;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum API_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final enum AUTHENTICATION_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final enum INVALID_REQUEST_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final enum PAYMENT_METHOD_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final enum RATE_LIMIT_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

.field public static final enum REFUND_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 230
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "API_ERROR"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->API_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 238
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v3, 0x2

    const-string v4, "AUTHENTICATION_ERROR"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->AUTHENTICATION_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 246
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v4, 0x3

    const-string v5, "INVALID_REQUEST_ERROR"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->INVALID_REQUEST_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 254
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v5, 0x4

    const-string v6, "RATE_LIMIT_ERROR"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->RATE_LIMIT_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 263
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v6, 0x5

    const-string v7, "PAYMENT_METHOD_ERROR"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->PAYMENT_METHOD_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 270
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    const/4 v7, 0x6

    const-string v8, "REFUND_ERROR"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/connect/v2/resources/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->REFUND_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    new-array v0, v7, [Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 224
    sget-object v7, Lcom/squareup/protos/connect/v2/resources/Error$Category;->API_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Category;->AUTHENTICATION_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Category;->INVALID_REQUEST_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Category;->RATE_LIMIT_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Category;->PAYMENT_METHOD_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error$Category;->REFUND_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Category;

    .line 272
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Error$Category$ProtoAdapter_Category;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Error$Category$ProtoAdapter_Category;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 277
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Error$Category;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 290
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->REFUND_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    .line 289
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->PAYMENT_METHOD_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    .line 288
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->RATE_LIMIT_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    .line 287
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->INVALID_REQUEST_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    .line 286
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->AUTHENTICATION_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    .line 285
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->API_ERROR:Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Error$Category;
    .locals 1

    .line 224
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Error$Category;
    .locals 1

    .line 224
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Error$Category;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Error$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Error$Category;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 297
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Error$Category;->value:I

    return v0
.end method
