.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogModifierOverride.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_id:Ljava/lang/String;

.field public on_by_default:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;
    .locals 4

    .line 123
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;->modifier_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;->on_by_default:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride;

    move-result-object v0

    return-object v0
.end method

.method public modifier_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;->modifier_id:Ljava/lang/String;

    return-object p0
.end method

.method public on_by_default(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierOverride$Builder;->on_by_default:Ljava/lang/Boolean;

    return-object p0
.end method
