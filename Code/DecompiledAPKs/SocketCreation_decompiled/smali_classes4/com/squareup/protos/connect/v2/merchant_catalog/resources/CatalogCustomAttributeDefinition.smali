.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;
.super Lcom/squareup/wire/Message;
.source "CatalogCustomAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$ProtoAdapter_CatalogCustomAttributeDefinition;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_VISIBILITY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_USAGE_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SELLER_VISIBILITY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final allowed_object_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogObjectType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field

.field public final app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$AppVisibility#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final custom_attribute_usage_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xb
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$SelectionConfig#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$SellerVisibility#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.inventory.resources.SourceApplication#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$StringConfig#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$ProtoAdapter_CatalogCustomAttributeDefinition;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$ProtoAdapter_CatalogCustomAttributeDefinition;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;->ATTRIBUTE_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 36
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->SELLER_VISIBILITY_READ_WRITE_VALUES:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->DEFAULT_SELLER_VISIBILITY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 38
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;->APP_VISIBILITY_HIDDEN:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->DEFAULT_APP_VISIBILITY:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->DEFAULT_CUSTOM_ATTRIBUTE_USAGE_COUNT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 163
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 173
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    .line 174
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    .line 175
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    const-string p1, "allowed_object_types"

    .line 176
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    .line 177
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 178
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    .line 179
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    .line 180
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    .line 181
    iput-object p10, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    .line 182
    iput-object p11, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 206
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 207
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    .line 208
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    .line 212
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    .line 213
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 214
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    .line 215
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    .line 216
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    .line 219
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 224
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 226
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 238
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;
    .locals 2

    .line 187
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;-><init>()V

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->name:Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->description:Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->allowed_object_types:Ljava/util/List;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->custom_attribute_usage_count:Ljava/lang/Integer;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->key:Ljava/lang/String;

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    if-eqz v1, :cond_3

    const-string v1, ", source_application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->source_application:Lcom/squareup/protos/connect/v2/inventory/resources/SourceApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", allowed_object_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->allowed_object_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 251
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    if-eqz v1, :cond_5

    const-string v1, ", seller_visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->seller_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SellerVisibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    if-eqz v1, :cond_6

    const-string v1, ", app_visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->app_visibility:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$AppVisibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 253
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    if-eqz v1, :cond_7

    const-string v1, ", string_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->string_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$StringConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 254
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    if-eqz v1, :cond_8

    const-string v1, ", selection_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->selection_config:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$SelectionConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 255
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", custom_attribute_usage_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->custom_attribute_usage_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 256
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogCustomAttributeDefinition{"

    .line 257
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
