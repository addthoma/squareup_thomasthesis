.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UnitType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType$ProtoAdapter_UnitType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INVALID_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_AREA:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_CUSTOM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_GENERIC:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

.field public static final enum TYPE_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 1089
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v1, 0x0

    const-string v2, "INVALID_TYPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->INVALID_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1096
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v2, 0x1

    const-string v3, "TYPE_CUSTOM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_CUSTOM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1103
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v3, 0x2

    const-string v4, "TYPE_AREA"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_AREA:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1110
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v4, 0x3

    const-string v5, "TYPE_LENGTH"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1117
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v5, 0x4

    const-string v6, "TYPE_VOLUME"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1124
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v6, 0x5

    const-string v7, "TYPE_WEIGHT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1131
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/4 v7, 0x7

    const/4 v8, 0x6

    const-string v9, "TYPE_TIME"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1138
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const-string v9, "TYPE_GENERIC"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_GENERIC:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1088
    sget-object v9, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->INVALID_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_CUSTOM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_AREA:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_GENERIC:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    .line 1140
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType$ProtoAdapter_UnitType;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType$ProtoAdapter_UnitType;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1145
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 1159
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1160
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_GENERIC:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1158
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_WEIGHT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1157
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1156
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_LENGTH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1155
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_AREA:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1154
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->TYPE_CUSTOM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    .line 1153
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->INVALID_TYPE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;
    .locals 1

    .line 1088
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;
    .locals 1

    .line 1088
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1167
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$UnitType;->value:I

    return v0
.end method
