.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchUpsertCatalogObjectsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public batches:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;",
            ">;"
        }
    .end annotation
.end field

.field public idempotency_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 135
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 136
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->batches:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public batches(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectBatch;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;"
        }
    .end annotation

    .line 183
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->batches:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;
    .locals 4

    .line 190
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->batches:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;-><init>(Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method
