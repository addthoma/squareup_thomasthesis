.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ItemVariationLocationOverrides.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public inventory_alert_threshold:Ljava/lang/Long;

.field public inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

.field public location_id:Ljava/lang/String;

.field public price_description:Ljava/lang/String;

.field public price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

.field public track_inventory:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 212
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;
    .locals 10

    .line 285
    new-instance v9, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->location_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->track_inventory:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 197
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    move-result-object v0

    return-object v0
.end method

.method public inventory_alert_threshold(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_alert_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description:Ljava/lang/String;

    return-object p0
.end method

.method public price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    return-object p0
.end method

.method public track_inventory(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->track_inventory:Ljava/lang/Boolean;

    return-object p0
.end method
