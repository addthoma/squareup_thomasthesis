.class public final Lcom/squareup/protos/connect/v2/resources/Tender;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$ProtoAdapter_Tender;,
        Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;,
        Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;,
        Lcom/squareup/protos/connect/v2/resources/Tender$Type;,
        Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Tender;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TRANSACTION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final additional_recipients:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.AdditionalRecipient#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;",
            ">;"
        }
    .end annotation
.end field

.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$CardDetails#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$CashDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final payment_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field

.field public final processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$RiskEvaluation#ADAPTER"
        tag = 0x11
    .end annotation
.end field

.field public final source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$PaymentSource#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$Type#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$ProtoAdapter_Tender;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$ProtoAdapter_Tender;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 46
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->CARD:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Tender$Builder;Lokio/ByteString;)V
    .locals 1

    .line 240
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 241
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    .line 242
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    .line 243
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->location_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    .line 244
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->transaction_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    .line 245
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    .line 246
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    .line 247
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 248
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 249
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 250
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->customer_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    .line 251
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 252
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    .line 253
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    .line 254
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->additional_recipients:Ljava/util/List;

    const-string v0, "additional_recipients"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    .line 255
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    .line 256
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    .line 257
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->payment_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 287
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 288
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender;

    .line 289
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    .line 290
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    .line 291
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    .line 292
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    .line 293
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    .line 294
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    .line 295
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 296
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 297
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 298
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    .line 299
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 300
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    .line 301
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    .line 302
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    .line 303
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    .line 304
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    .line 305
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    .line 306
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 311
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_10

    .line 313
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Type;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 326
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 327
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 328
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 329
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 330
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_f
    add-int/2addr v0, v2

    .line 331
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_10
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$Builder;
    .locals 2

    .line 262
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;-><init>()V

    .line 263
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->id:Ljava/lang/String;

    .line 264
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->uid:Ljava/lang/String;

    .line 265
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->location_id:Ljava/lang/String;

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->transaction_id:Ljava/lang/String;

    .line 267
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->created_at:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->note:Ljava/lang/String;

    .line 269
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 270
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->customer_id:Ljava/lang/String;

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->additional_recipients:Ljava/util/List;

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    .line 278
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    .line 279
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->payment_id:Ljava/lang/String;

    .line 280
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 346
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 347
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_8

    const-string v1, ", processing_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 348
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    if-eqz v1, :cond_a

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->type:Lcom/squareup/protos/connect/v2/resources/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 350
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    if-eqz v1, :cond_b

    const-string v1, ", card_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->card_details:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 351
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    if-eqz v1, :cond_c

    const-string v1, ", cash_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->cash_details:Lcom/squareup/protos/connect/v2/resources/Tender$CashDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 352
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", additional_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->additional_recipients:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 353
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    if-eqz v1, :cond_e

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->source:Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 354
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    if-eqz v1, :cond_f

    const-string v1, ", risk_evaluation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->risk_evaluation:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 355
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", payment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender;->payment_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tender{"

    .line 356
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
