.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;
.super Ljava/lang/Enum;
.source "DiscountTargetScope.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope$ProtoAdapter_DiscountTargetScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DISCOUNT_TARGET_SCOPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

.field public static final enum LINE_ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

.field public static final enum WHOLE_PURCHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    const/4 v1, 0x0

    const-string v2, "DISCOUNT_TARGET_SCOPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->DISCOUNT_TARGET_SCOPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 24
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    const/4 v2, 0x1

    const-string v3, "LINE_ITEM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->LINE_ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    const/4 v3, 0x2

    const-string v4, "WHOLE_PURCHASE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 16
    sget-object v4, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->DISCOUNT_TARGET_SCOPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->LINE_ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope$ProtoAdapter_DiscountTargetScope;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope$ProtoAdapter_DiscountTargetScope;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 48
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object p0

    .line 47
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->LINE_ITEM:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object p0

    .line 46
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->DISCOUNT_TARGET_SCOPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/DiscountTargetScope;->value:I

    return v0
.end method
