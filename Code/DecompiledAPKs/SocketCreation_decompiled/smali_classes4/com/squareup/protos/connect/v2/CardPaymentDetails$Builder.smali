.class public final Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CardPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CardPaymentDetails;",
        "Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_cryptogram:Ljava/lang/String;

.field public application_identifier:Ljava/lang/String;

.field public application_name:Ljava/lang/String;

.field public auth_result_code:Ljava/lang/String;

.field public avs_status:Ljava/lang/String;

.field public card:Lcom/squareup/protos/connect/v2/resources/Card;

.field public cvv_status:Ljava/lang/String;

.field public device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

.field public entry_method:Ljava/lang/String;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public pan_fidelius_token:Ljava/lang/String;

.field public statement_description:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public verification_method:Ljava/lang/String;

.field public verification_results:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 380
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 381
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public application_cryptogram(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 469
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_cryptogram:Ljava/lang/String;

    return-object p0
.end method

.method public application_identifier(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 453
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_identifier:Ljava/lang/String;

    return-object p0
.end method

.method public application_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 461
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_name:Ljava/lang/String;

    return-object p0
.end method

.method public auth_result_code(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 445
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->auth_result_code:Ljava/lang/String;

    return-object p0
.end method

.method public avs_status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 434
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->avs_status:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/CardPaymentDetails;
    .locals 20

    move-object/from16 v0, p0

    .line 541
    new-instance v18, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->status:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v4, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->entry_method:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->cvv_status:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->avs_status:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->auth_result_code:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_identifier:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_name:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_cryptogram:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_method:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_results:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->statement_description:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    iget-object v15, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->pan_fidelius_token:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Card;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/DeviceDetails;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 349
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/connect/v2/resources/Card;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 401
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    return-object p0
.end method

.method public cvv_status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 423
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->cvv_status:Ljava/lang/String;

    return-object p0
.end method

.method public device_details(Lcom/squareup/protos/connect/v2/DeviceDetails;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 514
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    return-object p0
.end method

.method public entry_method(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 412
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->entry_method:Ljava/lang/String;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;"
        }
    .end annotation

    .line 534
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 535
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public pan_fidelius_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 526
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->pan_fidelius_token:Ljava/lang/String;

    return-object p0
.end method

.method public statement_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 504
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->statement_description:Ljava/lang/String;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->status:Ljava/lang/String;

    return-object p0
.end method

.method public verification_method(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 480
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_method:Ljava/lang/String;

    return-object p0
.end method

.method public verification_results(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;
    .locals 0

    .line 491
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_results:Ljava/lang/String;

    return-object p0
.end method
