.class final Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$ProtoAdapter_PaymentSource;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PaymentSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1283
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1302
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;-><init>()V

    .line 1303
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1304
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1309
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1307
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->application_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;

    goto :goto_0

    .line 1306
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->application_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;

    goto :goto_0

    .line 1313
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1314
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1281
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$ProtoAdapter_PaymentSource;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1295
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->application_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1296
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->application_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1297
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1281
    check-cast p2, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$ProtoAdapter_PaymentSource;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)I
    .locals 4

    .line 1288
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->application_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->application_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1290
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1281
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$ProtoAdapter_PaymentSource;->encodedSize(Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;
    .locals 0

    .line 1319
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;

    move-result-object p1

    .line 1320
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1321
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1281
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource$ProtoAdapter_PaymentSource;->redact(Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;)Lcom/squareup/protos/connect/v2/resources/Tender$PaymentSource;

    move-result-object p1

    return-object p1
.end method
