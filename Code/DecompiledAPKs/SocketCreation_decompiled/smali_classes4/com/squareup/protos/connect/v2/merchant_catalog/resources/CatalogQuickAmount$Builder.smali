.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuickAmount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/connect/v2/common/Money;

.field public ordinal:Ljava/lang/Long;

.field public score:Ljava/lang/Long;

.field public type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;
    .locals 7

    .line 197
    new-instance v6, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->amount:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->score:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->ordinal:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount;

    move-result-object v0

    return-object v0
.end method

.method public ordinal(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->ordinal:Ljava/lang/Long;

    return-object p0
.end method

.method public score(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->score:Ljava/lang/Long;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmount$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountType;

    return-object p0
.end method
