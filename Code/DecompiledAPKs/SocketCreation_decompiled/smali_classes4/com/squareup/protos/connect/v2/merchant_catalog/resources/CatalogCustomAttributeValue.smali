.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;
.super Lcom/squareup/wire/Message;
.source "CatalogCustomAttributeValue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$ProtoAdapter_CatalogCustomAttributeValue;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BOOLEAN_VALUE:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_DEFINITION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INT_VALUE:Ljava/lang/Long;

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_STRING_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final boolean_value:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final custom_attribute_definition_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final int_value:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final number_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final selection_id_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final string_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogCustomAttributeDefinition$Type#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$ProtoAdapter_CatalogCustomAttributeValue;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$ProtoAdapter_CatalogCustomAttributeValue;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->DEFAULT_INT_VALUE:Ljava/lang/Long;

    .line 38
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;->ATTRIBUTE_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->DEFAULT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    const/4 v0, 0x0

    .line 42
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->DEFAULT_BOOLEAN_VALUE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 134
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 141
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    .line 144
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    .line 145
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    .line 146
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 147
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    .line 148
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    const-string p1, "selection_id_values"

    .line 149
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    .line 150
    iput-object p9, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 172
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 173
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    .line 175
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    .line 176
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    .line 177
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    .line 178
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 179
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    .line 180
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    .line 181
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    .line 182
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    .line 183
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 188
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 200
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;
    .locals 2

    .line 155
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;-><init>()V

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->name:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->string_value:Ljava/lang/String;

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->int_value:Ljava/lang/Long;

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->custom_attribute_definition_id:Ljava/lang/String;

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->number_value:Ljava/lang/String;

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->boolean_value:Ljava/lang/Boolean;

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->selection_id_values:Ljava/util/List;

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->key:Ljava/lang/String;

    .line 165
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", string_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->string_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", int_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->int_value:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", custom_attribute_definition_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->custom_attribute_definition_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    if-eqz v1, :cond_4

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 213
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", number_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->number_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", boolean_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->boolean_value:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 215
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", selection_id_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->selection_id_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 216
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogCustomAttributeValue{"

    .line 217
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
