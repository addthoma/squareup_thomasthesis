.class public final enum Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

.field public static final enum CAPTURED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

.field public static final enum FAILED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

.field public static final enum VOIDED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 827
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "AUTHORIZED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->AUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 832
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    const/4 v3, 0x2

    const-string v4, "CAPTURED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->CAPTURED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 837
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    const/4 v4, 0x3

    const-string v5, "VOIDED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->VOIDED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 842
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    const/4 v5, 0x4

    const-string v6, "FAILED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->FAILED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    new-array v0, v5, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 823
    sget-object v5, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->AUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->CAPTURED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->VOIDED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->FAILED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 844
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 848
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 849
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 860
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->FAILED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0

    .line 859
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->VOIDED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0

    .line 858
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->CAPTURED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0

    .line 857
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->AUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;
    .locals 1

    .line 823
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;
    .locals 1

    .line 823
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 867
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->value:I

    return v0
.end method
