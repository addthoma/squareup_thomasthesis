.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogV1Id.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_v1_id:Ljava/lang/String;

.field public location_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 103
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;
    .locals 4

    .line 124
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;->catalog_v1_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;->location_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;

    move-result-object v0

    return-object v0
.end method

.method public catalog_v1_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;->catalog_v1_id:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method
