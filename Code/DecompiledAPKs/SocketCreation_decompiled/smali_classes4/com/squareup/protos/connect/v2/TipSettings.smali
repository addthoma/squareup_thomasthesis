.class public final Lcom/squareup/protos/connect/v2/TipSettings;
.super Lcom/squareup/wire/Message;
.source "TipSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/TipSettings$ProtoAdapter_TipSettings;,
        Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/TipSettings;",
        "Lcom/squareup/protos/connect/v2/TipSettings$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/TipSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_TIPPING:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_TIP_FIELD:Ljava/lang/Boolean;

.field public static final DEFAULT_SEPARATE_TIP_SCREEN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_tipping:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final custom_tip_field:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final separate_tip_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final tip_percentages:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/connect/v2/TipSettings$ProtoAdapter_TipSettings;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/TipSettings$ProtoAdapter_TipSettings;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->DEFAULT_ALLOW_TIPPING:Ljava/lang/Boolean;

    .line 29
    sput-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->DEFAULT_SEPARATE_TIP_SCREEN:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->DEFAULT_CUSTOM_TIP_FIELD:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 82
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/TipSettings;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 87
    sget-object v0, Lcom/squareup/protos/connect/v2/TipSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    .line 89
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    .line 90
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    const-string p1, "tip_percentages"

    .line 91
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/TipSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/TipSettings;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TipSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/TipSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    .line 114
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 119
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TipSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/TipSettings$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/TipSettings$Builder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->allow_tipping:Ljava/lang/Boolean;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->separate_tip_screen:Ljava/lang/Boolean;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->custom_tip_field:Ljava/lang/Boolean;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->tip_percentages:Ljava/util/List;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TipSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/TipSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TipSettings;->newBuilder()Lcom/squareup/protos/connect/v2/TipSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", allow_tipping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->allow_tipping:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", separate_tip_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->separate_tip_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", custom_tip_field="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->custom_tip_field:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tip_percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/TipSettings;->tip_percentages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TipSettings{"

    .line 138
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
