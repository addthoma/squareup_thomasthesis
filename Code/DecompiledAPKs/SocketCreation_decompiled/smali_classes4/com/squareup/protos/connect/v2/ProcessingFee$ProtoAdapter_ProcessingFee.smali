.class final Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/ProcessingFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProcessingFee"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/ProcessingFee;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 176
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/ProcessingFee;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/ProcessingFee;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 197
    new-instance v0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;-><init>()V

    .line 198
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 199
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 205
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 203
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    goto :goto_0

    .line 202
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    goto :goto_0

    .line 201
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->effective_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    goto :goto_0

    .line 209
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 210
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->build()Lcom/squareup/protos/connect/v2/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/ProcessingFee;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 189
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 190
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 191
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 192
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    check-cast p2, Lcom/squareup/protos/connect/v2/ProcessingFee;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/ProcessingFee;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/ProcessingFee;)I
    .locals 4

    .line 181
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    const/4 v3, 0x2

    .line 182
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x3

    .line 183
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 174
    check-cast p1, Lcom/squareup/protos/connect/v2/ProcessingFee;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;->encodedSize(Lcom/squareup/protos/connect/v2/ProcessingFee;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/ProcessingFee;)Lcom/squareup/protos/connect/v2/ProcessingFee;
    .locals 2

    .line 215
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ProcessingFee;->newBuilder()Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    move-result-object p1

    .line 216
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 217
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 218
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->build()Lcom/squareup/protos/connect/v2/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 174
    check-cast p1, Lcom/squareup/protos/connect/v2/ProcessingFee;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;->redact(Lcom/squareup/protos/connect/v2/ProcessingFee;)Lcom/squareup/protos/connect/v2/ProcessingFee;

    move-result-object p1

    return-object p1
.end method
