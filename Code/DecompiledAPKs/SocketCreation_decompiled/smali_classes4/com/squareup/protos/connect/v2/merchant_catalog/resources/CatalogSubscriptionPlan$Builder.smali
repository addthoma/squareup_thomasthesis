.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogSubscriptionPlan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public price_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
