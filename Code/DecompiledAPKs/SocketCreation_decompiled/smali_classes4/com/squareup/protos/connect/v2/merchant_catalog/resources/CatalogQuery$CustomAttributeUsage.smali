.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;
.super Lcom/squareup/wire/Message;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomAttributeUsage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$ProtoAdapter_CustomAttributeUsage;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HAS_VALUE:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final custom_attribute_definition_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final has_value:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2729
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$ProtoAdapter_CustomAttributeUsage;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$ProtoAdapter_CustomAttributeUsage;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x1

    .line 2733
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->DEFAULT_HAS_VALUE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 2757
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;-><init>(Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 2762
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "custom_attribute_definition_ids"

    .line 2763
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2764
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2779
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2780
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;

    .line 2781
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2782
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    .line 2783
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 2788
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 2790
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2791
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2792
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2793
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;
    .locals 2

    .line 2769
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;-><init>()V

    .line 2770
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2771
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;->has_value:Ljava/lang/Boolean;

    .line 2772
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2728
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2801
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", custom_attribute_definition_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2802
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", has_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$CustomAttributeUsage;->has_value:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomAttributeUsage{"

    .line 2803
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
