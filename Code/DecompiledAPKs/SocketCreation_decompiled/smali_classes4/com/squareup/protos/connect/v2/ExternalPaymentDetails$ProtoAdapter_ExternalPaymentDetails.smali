.class final Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ExternalPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ExternalPaymentDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 231
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 254
    new-instance v0, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;-><init>()V

    .line 255
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 256
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 263
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 261
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    goto :goto_0

    .line 260
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    goto :goto_0

    .line 259
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    goto :goto_0

    .line 258
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    goto :goto_0

    .line 267
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 268
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 245
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 246
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 247
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 248
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 249
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    check-cast p2, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)I
    .locals 4

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->type:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source:Ljava/lang/String;

    const/4 v3, 0x2

    .line 237
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x4

    .line 239
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;->encodedSize(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
    .locals 2

    .line 273
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;

    move-result-object p1

    .line 274
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->source_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 275
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 276
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails$ProtoAdapter_ExternalPaymentDetails;->redact(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    move-result-object p1

    return-object p1
.end method
