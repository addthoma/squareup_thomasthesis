.class public final Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreatePaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest;",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accept_partial_authorization:Ljava/lang/Boolean;

.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public autocomplete:Ljava/lang/Boolean;

.field public auxiliary_info:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AuxiliaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public buyer_email_address:Ljava/lang/String;

.field public card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

.field public cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

.field public create_order:Ljava/lang/Boolean;

.field public customer_id:Ljava/lang/String;

.field public delay_action:Ljava/lang/String;

.field public delay_duration:Ljava/lang/String;

.field public employee_id:Ljava/lang/String;

.field public external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

.field public geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

.field public idempotency_key:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public order_id:Ljava/lang/String;

.field public payment_config_id:Ljava/lang/String;

.field public payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

.field public reference_id:Ljava/lang/String;

.field public requested_statement_description:Ljava/lang/String;

.field public revenue_association_tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public source_id:Ljava/lang/String;

.field public statement_description_identifier:Ljava/lang/String;

.field public tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public terminal_checkout_id:Ljava/lang/String;

.field public tip_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public verification_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 825
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 826
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->revenue_association_tags:Ljava/util/List;

    .line 827
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept_partial_authorization(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1147
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->accept_partial_authorization:Ljava/lang/Boolean;

    return-object p0
.end method

.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 887
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public app_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 946
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public autocomplete(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1012
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->autocomplete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public auxiliary_info(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AuxiliaryInfo;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;"
        }
    .end annotation

    .line 1254
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1255
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    return-object p0
.end method

.method public billing_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1167
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
    .locals 2

    .line 1281
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;-><init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 760
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object v0

    return-object v0
.end method

.method public buyer_email_address(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1157
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->buyer_email_address:Ljava/lang/String;

    return-object p0
.end method

.method public card_present_options(Lcom/squareup/protos/connect/v2/CardPresentOptions;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 854
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    return-object p0
.end method

.method public cash_details(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1265
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    return-object p0
.end method

.method public create_order(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1031
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->create_order:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1056
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public delay_action(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 997
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_action:Ljava/lang/String;

    return-object p0
.end method

.method public delay_duration(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 981
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_duration:Ljava/lang/String;

    return-object p0
.end method

.method public employee_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1090
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public external_details(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1275
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    return-object p0
.end method

.method public geo_location(Lcom/squareup/protos/connect/v2/GeoLocation;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1241
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    return-object p0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 870
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1080
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1189
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1044
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method

.method public payment_config_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1069
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_config_id:Ljava/lang/String;

    return-object p0
.end method

.method public payment_fees_spec(Lcom/squareup/protos/connect/v2/PaymentFeesSpec;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 961
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1104
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public requested_statement_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1218
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->requested_statement_description:Ljava/lang/String;

    return-object p0
.end method

.method public revenue_association_tags(Ljava/util/List;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;"
        }
    .end annotation

    .line 1229
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1230
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->revenue_association_tags:Ljava/util/List;

    return-object p0
.end method

.method public shipping_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1177
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public source_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 841
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id:Ljava/lang/String;

    return-object p0
.end method

.method public statement_description_identifier(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1207
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->statement_description_identifier:Ljava/lang/String;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 921
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public terminal_checkout_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1128
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->terminal_checkout_id:Ljava/lang/String;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 904
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public verification_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 0

    .line 1118
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->verification_token:Ljava/lang/String;

    return-object p0
.end method
