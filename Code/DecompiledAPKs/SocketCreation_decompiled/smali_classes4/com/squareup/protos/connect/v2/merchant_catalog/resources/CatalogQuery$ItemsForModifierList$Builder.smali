.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_list_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1415
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1416
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;->modifier_list_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;
    .locals 3

    .line 1430
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;->modifier_list_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1412
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList;

    move-result-object v0

    return-object v0
.end method

.method public modifier_list_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;"
        }
    .end annotation

    .line 1423
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1424
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$ItemsForModifierList$Builder;->modifier_list_ids:Ljava/util/List;

    return-object p0
.end method
