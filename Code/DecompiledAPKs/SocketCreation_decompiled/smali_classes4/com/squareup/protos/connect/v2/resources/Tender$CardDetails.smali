.class public final Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;
.super Lcom/squareup/wire/Message;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$ProtoAdapter_CardDetails;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;,
        Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;",
        "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final card:Lcom/squareup/protos/connect/v2/resources/Card;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Card#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$CardDetails$EntryMethod#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender$CardDetails$Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 676
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$ProtoAdapter_CardDetails;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$ProtoAdapter_CardDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 680
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->AUTHORIZED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->DEFAULT_STATUS:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 682
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->SWIPED:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->DEFAULT_ENTRY_METHOD:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;Lcom/squareup/protos/connect/v2/resources/Card;Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;)V
    .locals 1

    .line 718
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;-><init>(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;Lcom/squareup/protos/connect/v2/resources/Card;Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;Lcom/squareup/protos/connect/v2/resources/Card;Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;Lokio/ByteString;)V
    .locals 1

    .line 723
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 724
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 725
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 726
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 742
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 743
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;

    .line 744
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 745
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 746
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 747
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 752
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 754
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 755
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 756
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Card;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 757
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 758
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;
    .locals 2

    .line 731
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;-><init>()V

    .line 732
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    .line 733
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 734
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    .line 735
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 675
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 765
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 766
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->status:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 767
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v1, :cond_1

    const-string v1, ", card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 768
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    if-eqz v1, :cond_2

    const-string v1, ", entry_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails;->entry_method:Lcom/squareup/protos/connect/v2/resources/Tender$CardDetails$EntryMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardDetails{"

    .line 769
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
