.class public final enum Lcom/squareup/protos/connect/v2/inventory/resources/Product;
.super Ljava/lang/Enum;
.source "Product.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/inventory/resources/Product$ProtoAdapter_Product;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/inventory/resources/Product;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/inventory/resources/Product;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum APPOINTMENTS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum BILLING:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum DASHBOARD:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum EXTERNAL_API:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum INVOICES:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum ITEM_LIBRARY_IMPORT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum ONLINE_STORE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum OTHER:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum PAYROLL:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum PRODUCT_DO_NOT_USE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

.field public static final enum SQUARE_POS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 16
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v1, 0x0

    const-string v2, "PRODUCT_DO_NOT_USE"

    const/4 v3, -0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PRODUCT_DO_NOT_USE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v2, 0x1

    const-string v3, "SQUARE_POS"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->SQUARE_POS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v3, 0x2

    const-string v4, "EXTERNAL_API"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->EXTERNAL_API:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v4, 0x3

    const-string v5, "BILLING"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->BILLING:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 36
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v5, 0x4

    const-string v6, "APPOINTMENTS"

    invoke-direct {v0, v6, v5, v4}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->APPOINTMENTS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 41
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v6, 0x5

    const-string v7, "INVOICES"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->INVOICES:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 46
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v7, 0x6

    const-string v8, "ONLINE_STORE"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ONLINE_STORE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 51
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/4 v8, 0x7

    const-string v9, "PAYROLL"

    invoke-direct {v0, v9, v8, v7}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PAYROLL:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 56
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/16 v9, 0x8

    const-string v10, "DASHBOARD"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->DASHBOARD:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 61
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/16 v10, 0x9

    const-string v11, "ITEM_LIBRARY_IMPORT"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ITEM_LIBRARY_IMPORT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 66
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/16 v11, 0xa

    const-string v12, "OTHER"

    invoke-direct {v0, v12, v11, v8}, Lcom/squareup/protos/connect/v2/inventory/resources/Product;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->OTHER:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 15
    sget-object v12, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PRODUCT_DO_NOT_USE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->SQUARE_POS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->EXTERNAL_API:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->BILLING:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->APPOINTMENTS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->INVOICES:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ONLINE_STORE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PAYROLL:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->DASHBOARD:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ITEM_LIBRARY_IMPORT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->OTHER:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->$VALUES:[Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    .line 68
    new-instance v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product$ProtoAdapter_Product;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/inventory/resources/Product$ProtoAdapter_Product;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 73
    iput p3, p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/inventory/resources/Product;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 90
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ITEM_LIBRARY_IMPORT:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 89
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->DASHBOARD:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 91
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->OTHER:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 88
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PAYROLL:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 87
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->ONLINE_STORE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 86
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->INVOICES:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 85
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->APPOINTMENTS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 84
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->BILLING:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 83
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->EXTERNAL_API:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 82
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->SQUARE_POS:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    .line 81
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->PRODUCT_DO_NOT_USE:Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/inventory/resources/Product;
    .locals 1

    .line 15
    const-class v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/inventory/resources/Product;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->$VALUES:[Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/inventory/resources/Product;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/inventory/resources/Product;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 98
    iget v0, p0, Lcom/squareup/protos/connect/v2/inventory/resources/Product;->value:I

    return v0
.end method
