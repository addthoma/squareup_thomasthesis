.class public final Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;
.super Lcom/squareup/wire/Message;
.source "AckNextTerminalCheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$ProtoAdapter_AckNextTerminalCheckoutResponse;,
        Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;",
        "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.TerminalCheckout#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final errors:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Error#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$ProtoAdapter_AckNextTerminalCheckoutResponse;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$ProtoAdapter_AckNextTerminalCheckoutResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/TerminalCheckout;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
            ")V"
        }
    .end annotation

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/TerminalCheckout;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/TerminalCheckout;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;",
            "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 55
    sget-object v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "errors"

    .line 56
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    .line 57
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 72
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 73
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    .line 75
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    .line 76
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 81
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/TerminalCheckout;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 86
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;-><init>()V

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;->errors:Ljava/util/List;

    .line 64
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->newBuilder()Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->errors:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    if-eqz v1, :cond_1

    const-string v1, ", checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AckNextTerminalCheckoutResponse;->checkout:Lcom/squareup/protos/connect/v2/TerminalCheckout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AckNextTerminalCheckoutResponse{"

    .line 96
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
