.class public final Lcom/squareup/protos/precog/Signal;
.super Lcom/squareup/wire/Message;
.source "Signal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/precog/Signal$ProtoAdapter_Signal;,
        Lcom/squareup/protos/precog/Signal$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/precog/Signal;",
        "Lcom/squareup/protos/precog/Signal$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/precog/Signal;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ANONYMOUS_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ONBOARDING_ENTRY_POINT_VALUE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

.field public static final DEFAULT_PRODUCT_INTENT_VALUE:Lcom/squareup/protos/precog/ProductIntent;

.field public static final DEFAULT_SIGNAL_NAME:Lcom/squareup/protos/precog/SignalName;

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIANT_VALUE:Lcom/squareup/protos/precog/Variant;

.field private static final serialVersionUID:J


# instance fields
.field public final anonymous_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.precog.OnboardingEntryPoint#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final product_intent_value:Lcom/squareup/protos/precog/ProductIntent;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.precog.ProductIntent#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.precog.ProductInterestWeight#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final signal_name:Lcom/squareup/protos/precog/SignalName;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.precog.SignalName#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final variant_value:Lcom/squareup/protos/precog/Variant;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.precog.Variant#ADAPTER"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/precog/Signal$ProtoAdapter_Signal;

    invoke-direct {v0}, Lcom/squareup/protos/precog/Signal$ProtoAdapter_Signal;-><init>()V

    sput-object v0, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/protos/precog/SignalName;->SIGNAL_UNKNOWN:Lcom/squareup/protos/precog/SignalName;

    sput-object v0, Lcom/squareup/protos/precog/Signal;->DEFAULT_SIGNAL_NAME:Lcom/squareup/protos/precog/SignalName;

    .line 33
    sget-object v0, Lcom/squareup/protos/precog/ProductIntent;->PRODUCT_INTENT_UNKNOWN:Lcom/squareup/protos/precog/ProductIntent;

    sput-object v0, Lcom/squareup/protos/precog/Signal;->DEFAULT_PRODUCT_INTENT_VALUE:Lcom/squareup/protos/precog/ProductIntent;

    .line 35
    sget-object v0, Lcom/squareup/protos/precog/OnboardingEntryPoint;->OEP_UNKNOWN:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    sput-object v0, Lcom/squareup/protos/precog/Signal;->DEFAULT_ONBOARDING_ENTRY_POINT_VALUE:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 37
    sget-object v0, Lcom/squareup/protos/precog/Variant;->VARIANT_UNKNOWN:Lcom/squareup/protos/precog/Variant;

    sput-object v0, Lcom/squareup/protos/precog/Signal;->DEFAULT_VARIANT_VALUE:Lcom/squareup/protos/precog/Variant;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/precog/SignalName;Lcom/squareup/protos/precog/ProductIntent;Lcom/squareup/protos/precog/OnboardingEntryPoint;Lcom/squareup/protos/precog/ProductInterestWeight;Lcom/squareup/protos/precog/Variant;)V
    .locals 10

    .line 92
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/precog/Signal;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/precog/SignalName;Lcom/squareup/protos/precog/ProductIntent;Lcom/squareup/protos/precog/OnboardingEntryPoint;Lcom/squareup/protos/precog/ProductInterestWeight;Lcom/squareup/protos/precog/Variant;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/precog/SignalName;Lcom/squareup/protos/precog/ProductIntent;Lcom/squareup/protos/precog/OnboardingEntryPoint;Lcom/squareup/protos/precog/ProductInterestWeight;Lcom/squareup/protos/precog/Variant;Lokio/ByteString;)V
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/protos/precog/Signal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p9, 0x0

    new-array p9, p9, [Ljava/lang/Object;

    .line 100
    invoke-static {p5, p6, p7, p8, p9}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p9

    const/4 v0, 0x1

    if-gt p9, v0, :cond_0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    .line 107
    iput-object p5, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 108
    iput-object p6, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 109
    iput-object p7, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    .line 110
    iput-object p8, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    return-void

    .line 101
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of product_intent_value, onboarding_entry_point_value, product_interest_weight_value, variant_value may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 131
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/precog/Signal;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 132
    :cond_1
    check-cast p1, Lcom/squareup/protos/precog/Signal;

    .line 133
    invoke-virtual {p0}, Lcom/squareup/protos/precog/Signal;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/precog/Signal;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    iget-object v3, p1, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    iget-object p1, p1, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    .line 141
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 146
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/precog/Signal;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/precog/SignalName;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/precog/ProductIntent;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/precog/OnboardingEntryPoint;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/precog/ProductInterestWeight;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/precog/Variant;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 157
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/precog/Signal$Builder;
    .locals 2

    .line 115
    new-instance v0, Lcom/squareup/protos/precog/Signal$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/precog/Signal$Builder;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->name:Ljava/lang/String;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->value:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->anonymous_token:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->signal_name:Lcom/squareup/protos/precog/SignalName;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    iput-object v1, v0, Lcom/squareup/protos/precog/Signal$Builder;->variant_value:Lcom/squareup/protos/precog/Variant;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/precog/Signal;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/precog/Signal$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/precog/Signal;->newBuilder()Lcom/squareup/protos/precog/Signal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", anonymous_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->anonymous_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    if-eqz v1, :cond_3

    const-string v1, ", signal_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->signal_name:Lcom/squareup/protos/precog/SignalName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    if-eqz v1, :cond_4

    const-string v1, ", product_intent_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_intent_value:Lcom/squareup/protos/precog/ProductIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 170
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    if-eqz v1, :cond_5

    const-string v1, ", onboarding_entry_point_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->onboarding_entry_point_value:Lcom/squareup/protos/precog/OnboardingEntryPoint;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 171
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    if-eqz v1, :cond_6

    const-string v1, ", product_interest_weight_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->product_interest_weight_value:Lcom/squareup/protos/precog/ProductInterestWeight;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    if-eqz v1, :cond_7

    const-string v1, ", variant_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/precog/Signal;->variant_value:Lcom/squareup/protos/precog/Variant;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Signal{"

    .line 173
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
