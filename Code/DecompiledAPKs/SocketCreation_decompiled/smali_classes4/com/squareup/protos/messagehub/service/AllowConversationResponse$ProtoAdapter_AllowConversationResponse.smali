.class final Lcom/squareup/protos/messagehub/service/AllowConversationResponse$ProtoAdapter_AllowConversationResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AllowConversationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/AllowConversationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AllowConversationResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messagehub/service/AllowConversationResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 187
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 212
    new-instance v0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;-><init>()V

    .line 213
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 214
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 222
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->custom_issue_fields:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/messagehub/service/CustomIssueField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 219
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_subtitle(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    goto :goto_0

    .line 218
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->help_tip_title(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    goto :goto_0

    .line 217
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->view_title(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    goto :goto_0

    .line 216
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->allow(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 227
    invoke-virtual {v0}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$ProtoAdapter_AllowConversationResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 202
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->allow:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 203
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->view_title:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_title:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 205
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_subtitle:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 206
    sget-object v0, Lcom/squareup/protos/messagehub/service/CustomIssueField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->custom_issue_fields:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 207
    invoke-virtual {p2}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    check-cast p2, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$ProtoAdapter_AllowConversationResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)I
    .locals 4

    .line 192
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->allow:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->view_title:Ljava/lang/String;

    const/4 v3, 0x2

    .line 193
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_title:Ljava/lang/String;

    const/4 v3, 0x3

    .line 194
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->help_tip_subtitle:Ljava/lang/String;

    const/4 v3, 0x4

    .line 195
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 196
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->custom_issue_fields:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 185
    check-cast p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$ProtoAdapter_AllowConversationResponse;->encodedSize(Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse;
    .locals 2

    .line 232
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;->newBuilder()Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;

    move-result-object p1

    .line 233
    iget-object v0, p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->custom_issue_fields:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 234
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 235
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 185
    check-cast p1, Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/AllowConversationResponse$ProtoAdapter_AllowConversationResponse;->redact(Lcom/squareup/protos/messagehub/service/AllowConversationResponse;)Lcom/squareup/protos/messagehub/service/AllowConversationResponse;

    move-result-object p1

    return-object p1
.end method
