.class public final Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AllowConversationRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/AllowConversationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messagehub/service/AllowConversationRequest;",
        "Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_code:Ljava/lang/String;

.field public language_code:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messagehub/service/AllowConversationRequest;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest;

    iget-object v1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->language_code:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->country_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/messagehub/service/AllowConversationRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->build()Lcom/squareup/protos/messagehub/service/AllowConversationRequest;

    move-result-object v0

    return-object v0
.end method

.method public country_code(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->language_code:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/protos/messagehub/service/AllowConversationRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
