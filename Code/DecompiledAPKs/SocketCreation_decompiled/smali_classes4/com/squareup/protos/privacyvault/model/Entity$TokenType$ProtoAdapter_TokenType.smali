.class final Lcom/squareup/protos/privacyvault/model/Entity$TokenType$ProtoAdapter_TokenType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/privacyvault/model/Entity$TokenType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 283
    const-class v0, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/privacyvault/model/Entity$TokenType;
    .locals 0

    .line 288
    invoke-static {p1}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType;->fromValue(I)Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 281
    invoke-virtual {p0, p1}, Lcom/squareup/protos/privacyvault/model/Entity$TokenType$ProtoAdapter_TokenType;->fromValue(I)Lcom/squareup/protos/privacyvault/model/Entity$TokenType;

    move-result-object p1

    return-object p1
.end method
