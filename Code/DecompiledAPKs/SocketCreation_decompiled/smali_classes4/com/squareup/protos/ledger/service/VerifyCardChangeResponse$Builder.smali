.class public final Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyCardChangeResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;
    .locals 4

    .line 127
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;

    iget-object v1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    iget-object v2, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;-><init>(Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->build()Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;

    move-result-object v0

    return-object v0
.end method

.method public response_code(Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;)Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->response_code:Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$ResponseCode;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
