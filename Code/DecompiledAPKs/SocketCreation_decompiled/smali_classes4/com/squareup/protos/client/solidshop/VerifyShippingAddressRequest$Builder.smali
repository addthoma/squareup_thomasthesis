.class public final Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyShippingAddressRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public first_name:Ljava/lang/String;

.field public last_name:Ljava/lang/String;

.field public phone:Lcom/squareup/protos/common/location/Phone;

.field public save:Ljava/lang/Boolean;

.field public shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
    .locals 8

    .line 204
    new-instance v7, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v2, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone:Lcom/squareup/protos/common/location/Phone;

    iget-object v5, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->save:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/location/Phone;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object v0

    return-object v0
.end method

.method public first_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name:Ljava/lang/String;

    return-object p0
.end method

.method public last_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name:Ljava/lang/String;

    return-object p0
.end method

.method public phone(Lcom/squareup/protos/common/location/Phone;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone:Lcom/squareup/protos/common/location/Phone;

    return-object p0
.end method

.method public save(Ljava/lang/Boolean;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->save:Ljava/lang/Boolean;

    return-object p0
.end method

.method public shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method
