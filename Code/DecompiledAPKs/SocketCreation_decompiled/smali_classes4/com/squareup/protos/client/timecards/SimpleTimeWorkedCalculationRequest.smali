.class public final Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;
.super Lcom/squareup/wire/Message;
.source "SimpleTimeWorkedCalculationRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;,
        Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;",
        "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCLUDE_OPEN_TIMECARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_NEXT_CURSOR:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final date_range:Lcom/squareup/protos/client/timecards/DateRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.DateRange#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTimeInterval#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final include_open_timecards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final next_cursor:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.MerchantEmployeeRequestFilter#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$ProtoAdapter_SimpleTimeWorkedCalculationRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 66
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->DEFAULT_INCLUDE_OPEN_TIMECARDS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/DateRange;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 7

    .line 116
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;-><init>(Lcom/squareup/protos/client/timecards/DateRange;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/DateRange;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 122
    sget-object v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    .line 124
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 125
    iput-object p3, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    .line 126
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    .line 127
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 145
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 146
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    .line 152
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 157
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 159
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/DateRange;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTimeInterval;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 165
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;
    .locals 2

    .line 132
    new-instance v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    .line 136
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->next_cursor:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->include_open_timecards:Ljava/lang/Boolean;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->newBuilder()Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    if-eqz v1, :cond_0

    const-string v1, ", date_range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_range:Lcom/squareup/protos/client/timecards/DateRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    if-eqz v1, :cond_1

    const-string v1, ", date_time_interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->date_time_interval:Lcom/squareup/protos/common/time/DateTimeInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    if-eqz v1, :cond_2

    const-string v1, ", request_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->request_filter:Lcom/squareup/protos/client/timecards/MerchantEmployeeRequestFilter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", next_cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->next_cursor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", include_open_timecards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/SimpleTimeWorkedCalculationRequest;->include_open_timecards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SimpleTimeWorkedCalculationRequest{"

    .line 178
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
