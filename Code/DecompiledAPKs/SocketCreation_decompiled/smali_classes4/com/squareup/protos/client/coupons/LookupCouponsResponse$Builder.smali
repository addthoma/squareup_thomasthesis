.class public final Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LookupCouponsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public coupon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation
.end field

.field public reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 100
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->coupon:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
    .locals 4

    .line 119
    new-instance v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->coupon:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object v0

    return-object v0
.end method

.method public coupon(Ljava/util/List;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;"
        }
    .end annotation

    .line 104
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->coupon:Ljava/util/List;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->reason:Lcom/squareup/protos/client/coupons/LookupCouponsResponse$LookupFailureReason;

    return-object p0
.end method
