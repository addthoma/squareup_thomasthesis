.class public final enum Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;
.super Ljava/lang/Enum;
.source "LoyaltyRequestError.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Code"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code$ProtoAdapter_Code;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CODE_CANNOT_DELETE_LOYALTY_ACCOUNT:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_CONTACT_ALREADY_HAS_LOYALTY:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_ENTITY_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_ENTITY_FORBIDDEN:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_ENTITY_IDENTICAL:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_ENTITY_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_ENTITY_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_FIELD_REQUIRED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_MAPPING_ALREADY_USED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

.field public static final enum CODE_UNAUTHORIZED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 222
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v1, 0x0

    const-string v2, "CODE_INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 224
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v2, 0x1

    const-string v3, "CODE_FIELD_REQUIRED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_FIELD_REQUIRED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 226
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v3, 0x2

    const-string v4, "CODE_ENTITY_NOT_FOUND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 228
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v4, 0x3

    const-string v5, "CODE_MAPPING_ALREADY_USED"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_MAPPING_ALREADY_USED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 230
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v5, 0x4

    const-string v6, "CODE_CONTACT_ALREADY_HAS_LOYALTY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CONTACT_ALREADY_HAS_LOYALTY:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 232
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v6, 0x5

    const-string v7, "CODE_CANNOT_DELETE_LOYALTY_ACCOUNT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CANNOT_DELETE_LOYALTY_ACCOUNT:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 234
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v7, 0x6

    const-string v8, "CODE_ENTITY_FORBIDDEN"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_FORBIDDEN:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 236
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/4 v8, 0x7

    const-string v9, "CODE_UNAUTHORIZED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_UNAUTHORIZED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 238
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/16 v9, 0x8

    const-string v10, "CODE_ENTITY_INVALID"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 240
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/16 v10, 0x9

    const-string v11, "CODE_ENTITY_IDENTICAL"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_IDENTICAL:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 242
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/16 v11, 0xa

    const-string v12, "CODE_ENTITY_DELETED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 218
    sget-object v12, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_FIELD_REQUIRED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_MAPPING_ALREADY_USED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CONTACT_ALREADY_HAS_LOYALTY:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CANNOT_DELETE_LOYALTY_ACCOUNT:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_FORBIDDEN:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_UNAUTHORIZED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_IDENTICAL:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    .line 244
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code$ProtoAdapter_Code;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code$ProtoAdapter_Code;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 249
    iput p3, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 267
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_DELETED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 266
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_IDENTICAL:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 265
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 264
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_UNAUTHORIZED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 263
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_FORBIDDEN:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 262
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CANNOT_DELETE_LOYALTY_ACCOUNT:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 261
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_CONTACT_ALREADY_HAS_LOYALTY:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 260
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_MAPPING_ALREADY_USED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 259
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_ENTITY_NOT_FOUND:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 258
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_FIELD_REQUIRED:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    .line 257
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->CODE_INVALID:Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;
    .locals 1

    .line 218
    const-class v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;
    .locals 1

    .line 218
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->$VALUES:[Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 274
    iget v0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError$Code;->value:I

    return v0
.end method
