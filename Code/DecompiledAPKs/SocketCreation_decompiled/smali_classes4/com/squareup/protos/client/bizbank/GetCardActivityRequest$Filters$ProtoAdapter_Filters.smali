.class final Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCardActivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Filters"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 490
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 517
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;-><init>()V

    .line 518
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 519
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 556
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 554
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    goto :goto_0

    .line 553
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    goto :goto_0

    .line 547
    :pswitch_2
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_transaction_states:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 549
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 539
    :pswitch_3
    :try_start_1
    iget-object v4, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_transaction_states:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 541
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 531
    :pswitch_4
    :try_start_2
    iget-object v4, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 533
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 523
    :pswitch_5
    :try_start_3
    iget-object v4, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_activity_types:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v4

    .line 525
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 560
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 561
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 488
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 506
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 507
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 508
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 509
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 510
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 511
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 512
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 488
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)I
    .locals 4

    .line 495
    sget-object v0, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 496
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 497
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 498
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 499
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x6

    .line 500
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 501
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 488
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;->encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
    .locals 2

    .line 566
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    move-result-object p1

    .line 567
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 568
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 569
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 570
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 488
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;->redact(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object p1

    return-object p1
.end method
