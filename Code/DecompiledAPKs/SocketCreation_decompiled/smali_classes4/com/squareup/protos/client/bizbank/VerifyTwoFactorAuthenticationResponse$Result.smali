.class public final enum Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;
.super Ljava/lang/Enum;
.source "VerifyTwoFactorAuthenticationResponse.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result$ProtoAdapter_Result;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

.field public static final enum EXPIRED_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

.field public static final enum INVALID_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

.field public static final enum VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 105
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    .line 107
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v2, 0x1

    const-string v3, "VERIFIED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    .line 109
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v3, 0x2

    const-string v4, "INVALID_TOKEN"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    .line 111
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v4, 0x3

    const-string v5, "EXPIRED_TOKEN"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    .line 104
    sget-object v5, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    .line 113
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result$ProtoAdapter_Result;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result$ProtoAdapter_Result;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 117
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 118
    iput p3, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 129
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object p0

    .line 128
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object p0

    .line 127
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object p0

    .line 126
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->DO_NOT_USE:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;
    .locals 1

    .line 104
    const-class v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->$VALUES:[Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 136
    iget v0, p0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->value:I

    return v0
.end method
