.class public final Lcom/squareup/protos/client/invoice/Invoice;
.super Lcom/squareup/wire/Message;
.source "Invoice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/Invoice$ProtoAdapter_Invoice;,
        Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;,
        Lcom/squareup/protos/client/invoice/Invoice$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTOMATIC_REMINDERS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_ENTERED_AUTOMATIC_CHARGE_ENROLL_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_ENTERED_INSTRUMENT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_ENTERED_SHIPPING_ADDRESS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_INSTRUMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_INVOICE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_INVOICE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final additional_recipient_email:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        redacted = true
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final attachment:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.FileAttachmentMetadata#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation
.end field

.field public final automatic_reminder_config:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceReminderConfig#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x16
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation
.end field

.field public final automatic_reminders_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final buyer_entered_instrument_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final cart:Lcom/squareup/protos/client/bills/Cart;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final creator_details:Lcom/squareup/protos/client/CreatorDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.CreatorDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final due_on:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x6
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final instrument_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final invoice_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final merchant_invoice_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final payer:Lcom/squareup/protos/client/invoice/InvoiceContact;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceContact#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.Invoice$PaymentMethod#ADAPTER"
        tag = 0xe
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final payment_request:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.PaymentRequest#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x17
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x12
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final version:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$ProtoAdapter_Invoice;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/Invoice$ProtoAdapter_Invoice;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    .line 48
    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    sput-object v1, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_PAYMENT_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 52
    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_BUYER_ENTERED_INSTRUMENT_ENABLED:Ljava/lang/Boolean;

    .line 54
    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_BUYER_ENTERED_SHIPPING_ADDRESS_ENABLED:Ljava/lang/Boolean;

    .line 56
    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_BUYER_ENTERED_AUTOMATIC_CHARGE_ENROLL_ENABLED:Ljava/lang/Boolean;

    .line 58
    sput-object v0, Lcom/squareup/protos/client/invoice/Invoice;->DEFAULT_AUTOMATIC_REMINDERS_ENABLED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lokio/ByteString;)V
    .locals 1

    .line 276
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 277
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 278
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    .line 279
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 280
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    .line 281
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    .line 282
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 283
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 284
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 285
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    .line 286
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    const-string v0, "additional_recipient_email"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    .line 287
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 288
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->version:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    .line 289
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 290
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    const-string v0, "attachment"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    .line 291
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->instrument_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    .line 292
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 293
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    .line 294
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    .line 295
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    .line 296
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 297
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    const-string v0, "automatic_reminder_config"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    .line 298
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const-string p2, "payment_request"

    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 333
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/Invoice;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 334
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    .line 335
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 336
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    .line 337
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 338
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    .line 339
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    .line 340
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 341
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 342
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 343
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    .line 344
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    .line 345
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 346
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    .line 347
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 348
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    .line 349
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    .line 350
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 351
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    .line 352
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    .line 353
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    .line 354
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 355
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    .line 356
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 357
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 362
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_12

    .line 364
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 365
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 366
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 367
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 368
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 369
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 370
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 371
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 372
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceContact;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 373
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 374
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 375
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 376
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 377
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 378
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 379
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 380
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 381
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 382
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 383
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 384
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 385
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 386
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 2

    .line 303
    new-instance v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;-><init>()V

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description:Ljava/lang/String;

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->merchant_invoice_number:Ljava/lang/String;

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->invoice_name:Ljava/lang/String;

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    .line 315
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->version:Ljava/lang/String;

    .line 316
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    .line 317
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    .line 318
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->instrument_token:Ljava/lang/String;

    .line 319
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    .line 320
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    .line 321
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    .line 322
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    .line 323
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled:Ljava/lang/Boolean;

    .line 324
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 325
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    .line 326
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", description=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_2

    const-string v1, ", scheduled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 398
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", merchant_invoice_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", invoice_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_5

    const-string v1, ", due_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->due_on:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 401
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v1, :cond_6

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 402
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_7

    const-string v1, ", payer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 403
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 404
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", additional_recipient_email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 405
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    if-eqz v1, :cond_a

    const-string v1, ", creator_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 406
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eqz v1, :cond_c

    const-string v1, ", payment_method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_method:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 408
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 409
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", instrument_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->instrument_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", buyer_entered_instrument_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_instrument_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 411
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_10

    const-string v1, ", scheduled_at_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at_time:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 412
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", buyer_entered_shipping_address_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 413
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", buyer_entered_automatic_charge_enroll_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 414
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", automatic_reminders_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminders_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 415
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, ", automatic_reminder_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->automatic_reminder_config:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 416
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    const-string v1, ", payment_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_15
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Invoice{"

    .line 417
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
