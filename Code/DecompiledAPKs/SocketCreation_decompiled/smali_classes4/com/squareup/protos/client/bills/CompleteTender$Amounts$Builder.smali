.class public final Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CompleteTender$Amounts;",
        "Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public tip_money:Lcom/squareup/protos/common/Money;

.field public tip_percentage:Ljava/lang/String;

.field public total_charged_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 263
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;
    .locals 5

    .line 296
    new-instance v0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 256
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object v0

    return-object v0
.end method

.method public tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
    .locals 0

    .line 290
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage:Ljava/lang/String;

    return-object p0
.end method

.method public total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
